
CREATE OR REPLACE FUNCTION vaxiom.update_search_name() 
RETURNS TRIGGER 
LANGUAGE plpgsql AS
$BODY$ 
	BEGIN
		new.search_name = remove_special_chars(CONCAT_WS(' ',new.first_name, new.middle_name, new.last_name, new.greeting));
		RETURN NEW;
	END;
$BODY$
;
	
DROP TRIGGER IF EXISTS person_before_insert ON vaxiom.persons;
CREATE TRIGGER person_before_insert
  BEFORE INSERT
  ON vaxiom.persons
  FOR EACH ROW
  EXECUTE PROCEDURE vaxiom.update_search_name();
 
 DROP TRIGGER IF EXISTS person_before_update ON vaxiom.persons;
 CREATE TRIGGER person_before_update
  BEFORE UPDATE
  ON vaxiom.persons
  FOR EACH ROW
  EXECUTE PROCEDURE vaxiom.update_search_name();
