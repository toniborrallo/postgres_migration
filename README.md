# README #

Custom process to migrate vaxiom DB from MySQL (Petcona  v5.7) to PostgreSQL (v12.2)

## Prerequisites

* pgloader: THe porcess make uses of the [pgloader](https://github.com/dimitri/pgloader) application 
* Acces to the source: MySQL DB server
* Acces to the target: PostgreSQL DB server

## How to run the process

To execute the procces we need to configure the connections to the source and targe databases and execute the script mysql2postgres. This process will fire all the subprocesses to execute the migration.

THis process execute the migration in several steps:

* 1. Check connectivity woth the source and target db
* 2. Execute pgloader to create the migrate the daabase structure (no data)
* 3. Migate data from source DB to target DB, to avoid memory problems we split this step in sub-steps, copying tables starting with letter A-D, E-H, I-L, M-P. Q-T and UZ.
* 4. Create vía sql the triggers, functions, views.

### Setting up the connections

There are a several files to modify the connection to adjust to the new environtment, all the files with the extension .load

The fisrt one could be vaxiom2postgres_create.load

load database
     from      mysql://<user>:<password>@localhost:3306/vaxiom
     into postgresql://<user>:<password>@localhost:5432/postgres
	 
Modify user, password and (if needed) the host and ports

And just it!

### Final step

Just type: $>mysql2postgres





