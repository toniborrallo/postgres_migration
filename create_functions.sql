CREATE OR REPLACE FUNCTION vaxiom.get_patient_by_transaction(_transaction_id numeric)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
-- Convetions:  All the variables starts with '_'  to avoid ambiguity with column or table names
DECLARE _transaction_dtype text;
declare _receivable_id numeric;
declare _res numeric;
BEGIN
   -- NEW.time_stamp_ = now();
   -- RETURN NEW;
  SELECT dtype, receivable_id into _transaction_dtype,_receivable_id FROM transactions WHERE id = _transaction_id;


    IF _receivable_id IS NOT NULL THEN
         SELECT patient_id into _res FROM vaxiom.receivables WHERE id = _receivable_id;
    ELSEIF _transaction_dtype = 'payment' THEN
         SELECT p.patient_id into _res
         FROM vaxiom.transactions t
             LEFT JOIN vaxiom.applied_payments ap ON ap.id=t.id
             LEFT JOIN vaxiom.payments p ON p.id=ap.payment_id
         WHERE t.id = _transaction_id ;
     END IF;

  return _res;
END;
$function$
;

CREATE OR REPLACE FUNCTION vaxiom.get_patient_by_payment_account(_account_id NUMERIC)
RETURNS numeric
 LANGUAGE plpgsql
AS $function$
-- Convetions:  All the variables starts with '_'  to avoid ambiguity with column or table names
DECLARE _res numeric;
 BEGIN

     SELECT DISTINCT p.id into _res
     FROM vaxiom.payment_accounts pa
 		JOIN vaxiom.person_payment_accounts ppa on ppa.id = pa.id
         LEFT JOIN rvaxiom.elationships re on re.to_person = ppa.payer_person_id
         LEFT JOIN vaxiom.patients p on (p.person_id = re.from_person or p.person_id = ppa.payer_person_id)
         WHERE pa.id = _account_id AND p.id IS NOT NULL LIMIT 1;

        RETURN _res;
  END;
  $function$
;

CREATE OR REPLACE FUNCTION vaxiom.get_transaction_description(_transaction_id NUMERIC)
RETURNS varchar(1024)
 LANGUAGE plpgsql
AS $function$
	DECLARE _result varchar(1024);
	BEGIN
     SELECT
       case when rec.id is not null then 'Declined'
   		else case when ap.id is not null then
   			case when p.payment_type = 'CreditCardPayment' then 'Credit Card'
   			     when p.payment_type = 'CashPayment' then 'Cash'
   			     when p.payment_type = 'InsurancePayment' then 'Insurance'
   			     when p.payment_type = 'AutoPayment' then 'Auto Payment'
   			     when p.payment_type = 'TransferPayment' then 'Transfer Payment'
   			     when p.payment_type = 'CheckPayment' then
   					case when p.type_info = 'CARE_CREDIT' then 'Check (Care Credit)'
   					     when p.type_info = 'INSURANCE' then 'Check (Insurance)'
   						 when p.type_info = 'INSURANCE_EFT' then 'Check (Insurance EFT)'
   						 when p.type_info = 'MEDICAID' then 'Check (Medicaid)'
   						 when p.type_info = 'MEDICAID_EFT' then 'Check (Medicaid EFT)'
   						 when p.type_info = 'MEDICAID_NH' then 'Check (Medicaid NH)'
   						 when p.type_info = 'MEDICAID_VERMONT' then 'Check (Medicaid Vermont)'
   						 when p.type_info = 'MONEY_ORDER' then 'Check (Money Order)'
   						 when p.type_info = 'ORTHOBANC' then 'Check (Orthobanc)'
   						 when p.type_info = 'MANUAL_ORTHOTRACK_ACH_CC' then 'Check (Manual Orthotrac ACH/CC)'
   						 when p.type_info = 'VANCO' then 'Check (Vanco)'
   						 when p.type_info = 'CHECK' then 'Check'
   						 else concat('Check (', p.type_info, ')')
   				    end
   			else 'Payment'
   			end
   		  when da.id is not null then
   		       case when t.amount > 0 then 'Migration'
   		       else case when dd.id is not null then dd.name
   		       		else case when da.discount_adjustment_type = 'TxFeeDiscount' then 'Treatment Discount'
   		       		          when da.discount_adjustment_type = 'PersonPayerDiscount' then 'Payer Discount'
   		       		          else 'Discount'
   		       		     end
   		            end
   		       end
   		  when dra.id is not null then 'Discount Reverse'
   		  when ind.id is not null then 'In Network Discount'
   		  when pp.id is not null then 'Payment Plan'
   		  when cta.id is not null then 'Change Transfer'
   		  when ppr.id is not null then 'Payment Plan Rollback'
   		  when wa.id is not null then 'Write Off'
   		  when tr.id is not null then 'Refund'
   		  when mfa.id is not null then 'Migration Fix'
   		  when fa.id is not null then 'Fix Adjustment'
   		  when mfc.id is not null then CASE WHEN mfc.name is not NULL then mfc.name else 'Misc Fee'end
   		  when tc.id is not null then 'Transfer Charge'
   		  when tfc.id is not null then 'Treatment Fee'
   		  when ic.id is not null then 'Installment Charge'
   		  when rf.id is not null then 'Retail Fee'
   		  when lfc.id is not null then 'Late Fee'
   		  when cor.id is not null then 'Correction'
   		   else t.dtype
       end
   		end INTO _result
       from vaxiom.transactions t
       left join vaxiom.applied_payments ap on ap.id = t.id
       left join vaxiom.payments p on p.id = ap.payment_id
       left join vaxiom.charge_transfer_adjustments cta on cta.id = t.id
       left join vaxiom.discount_adjustments da on da.id = t.id
       left join vaxiom.discounts dd on dd.id = da.discount_id
       left join vaxiom.discount_reverse_adjustments dra on dra.id = t.id
       left join vaxiom.fix_adjustments fa on fa.id = t.id
       left join vaxiom.in_network_discounts ind on ind.id = t.id
      	left join vaxiom.migration_fix_adjustments mfa on mfa.id = t.id
      	left join vaxiom.payment_plans pp on pp.id = t.id
      	left join vaxiom.payment_plan_rollbacks ppr on ppr.id = t.id
      	left join vaxiom.refunds tr on tr.id = t.id
      	left join vaxiom.writeoff_adjustments wa on wa.id = t.id
      	left join vaxiom.payment_correction_details cor on cor.id = t.id

      	left join vaxiom.tx_fee_charges tfc on tfc.id = t.id
      	left join vaxiom.transfer_charges tc on tc.id = t.id
      	left join vaxiom.retail_fees rf on rf.id = t.id
      	left join vaxiom.misc_fee_charges mfc on mfc.id = t.id
      	left join vaxiom.late_fee_charges lfc on lfc.id = t.id
      	left join vaxiom.installment_charges ic on ic.id = t.id
      	left join vaxiom.receivables rec on rec.id = t.receivable_id and rec.status='DECLINED'
      	where t.id = _transaction_id;

      	return _result;
END;
$function$
;

CREATE OR REPLACE FUNCTION vaxiom.remove_special_chars(_str VARCHAR(255))
RETURNS VARCHAR(255)
 LANGUAGE plpgsql
AS $function$
	BEGIN
	  -- Replace all non_alfanumeric symbol for ' ' <white space>, 'g' indicates all the occurrences
      RETURN regexp_replace(_str,'[^[:alnum:]]+',' ','g');
	END;
$function$
;

CREATE OR REPLACE FUNCTION vaxiom.checksumverhoeff(num numeric, calcchecksum integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
DECLARE
        d CHAR(100) := '0123456789123406789523401789563401289567401239567859876043216598710432765982104387659321049876543210';
        p CHAR(80) := '01234567891576283094580379614289160435279453126870428657390127938064157046913258';
        inv CHAR(10) := '0432156789';
        c integer := 0;
        len integer;
        m integer;
        i integer := 0;
        n VARCHAR(255);
BEGIN
        /* Start Processing */
        n := REVERSE(num::varchar);
        len := LENGTH(n);

        WHILE (i < len) LOOP
                IF calcChecksum = 1 THEN
                                /* Do the CalcChecksum */
                                m := substring(p,(((i+1)%8)*10)+ substring(n,i+1,1)::integer+1,1)::integer;
                ELSE
                                /* Do the Checksum */
                                m := substring(p,((i%8)*10)+ substring(n,i+1,1)::integer+1,1)::integer;
                END IF;
                c := substring(d,(c*10+m+1),1)::integer;
                i:=i+1;
        END LOOP;

        IF (calcChecksum = 1) THEN
                                /* Do the CalcChecksum */
                                c := substring(inv,c+1,1)::integer;
                END IF;
        RETURN c;
END
$function$
;
