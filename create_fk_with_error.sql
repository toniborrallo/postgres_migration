ALTER TABLE vaxiom.enrollment_form_beneficiary ALTER COLUMN enrollment_form_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.enrollment_form_beneficiary ADD CONSTRAINT enrollment_form_beneficiary_ibfk_1 FOREIGN KEY(enrollment_form_id) REFERENCES vaxiom.enrollment_form(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE vaxiom.enrollment_form_benefits ALTER COLUMN enrollment_form_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.enrollment_form_benefits ADD CONSTRAINT enrollment_form_benefits_ibfk_1 FOREIGN KEY(enrollment_form_id) REFERENCES vaxiom.enrollment_form(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE vaxiom.enrollment_form_dependents ALTER COLUMN enrollment_form_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.enrollment_form_dependents ADD CONSTRAINT enrollment_form_dependents_ibfk_1 FOREIGN KEY(enrollment_form_id) REFERENCES vaxiom.enrollment_form(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE vaxiom.enrollment_form_dependents_benefits ALTER COLUMN enrollment_form_benefit_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.enrollment_form_dependents_benefits ADD CONSTRAINT enrollment_form_dependents_benefits_ibfk_1 FOREIGN KEY(enrollment_form_benefit_id) REFERENCES vaxiom.enrollment_form_benefits(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE vaxiom.enrollment_form_dependents_benefits ALTER COLUMN enrollment_form_dependent_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.enrollment_form_dependents_benefits ADD CONSTRAINT enrollment_form_dependents_benefits_ibfk_2 FOREIGN KEY(enrollment_form_dependent_id) REFERENCES vaxiom.enrollment_form_dependents(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE vaxiom.enrollment_form_personal_data ALTER COLUMN enrollment_form_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.enrollment_form_personal_data ADD CONSTRAINT enrollment_form_personal_data_ibfk_1 FOREIGN KEY(enrollment_form_id) REFERENCES vaxiom.enrollment_form(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE vaxiom.migration_map ALTER COLUMN merged_to_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.migration_map ADD CONSTRAINT migration_map_ibfk_1 FOREIGN KEY(merged_to_id) REFERENCES vaxiom.migration_map(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE vaxiom.previous_enrollment_dependents ALTER COLUMN previous_enrollment_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.previous_enrollment_dependents ADD CONSTRAINT previous_enrollment_dependents_ibfk_1 FOREIGN KEY(previous_enrollment_id) REFERENCES vaxiom.previous_enrollment_form(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE vaxiom.previous_enrollment_form_benefits ALTER COLUMN enrollment_form_id SET DATA TYPE bigint ;
ALTER TABLE vaxiom.previous_enrollment_form_benefits ADD CONSTRAINT previous_enrollment_form_benefits_ibfk_1 FOREIGN KEY(enrollment_form_id) REFERENCES vaxiom.previous_enrollment_form(id) ON UPDATE RESTRICT ON DELETE RESTRICT;

