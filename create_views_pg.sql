create
or replace
view vaxiom.all_resources as
 select
   r.id as id,
   r.dtype as dtype,
   r.organization_id as location_id,
   (case
       when (r.dtype = 'employee') then concat(p.first_name, ' ', p.last_name)
       when (r.dtype = 'chair') then c.code
       when (r.dtype = 'office') then o.name
       when (r.dtype = 'equipment') then eq.name
       else 'unknown'
   end) as display_name,
   r.resource_type_id as resource_type_id,
   rt.name as resource_type_name
from
   (((((((vaxiom.resources r
join vaxiom.resource_types rt on
   ((rt.id = r.resource_type_id)))
left join vaxiom.employee_resources er on
   ((er.id = r.id)))
left join vaxiom.employment_contracts ec on
   ((ec.id = er.employment_contract_id)))
left join vaxiom.persons p on
   ((p.id = ec.person_id)))
left join vaxiom.chairs c on
   ((c.id = r.id)))
left join vaxiom.equipment eq on
   ((eq.id = r.id)))
join vaxiom.sys_organizations o on
   ((o.id = r.organization_id)));


create
or replace
view vaxiom.employee_enrollment_coverage_by_individual_view as
  	select
      ed.id as dependent_id,
      ed.employee_enrollment_id as employee_enrollment_id,
      ep.first_name as dependent_first_name,
      ep.last_name as dependent_last_name,
      ep.date_of_birth as dependent_date_of_birth,
      ep.relationship as relationship,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'MEDICAL')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_medical,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'VISION')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_vision,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'DENTAL')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_dental,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'BL')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_bl,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'BL_ADND')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_bl_adnd,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'STD')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_std,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'LTD')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_ltd,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'VL')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_vl,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'VL_ADND')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_vl_adnd,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'CL')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_cl,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'FSA')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_fsa,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'CANCER')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_cancer,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'ACCIDENT')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_accident,
      bit_or(
      (
          case
          when ((eb.benefit_type = 'HOSPITALIZATION')
          and ((eb.is_waived = '0')
          or (eb.benefit_enrollment_status = 'NONE'))) then 1
          else 0
      end)) as is_hospitalization,
      bit_or(
      (
          case
          when 'HSA' in (
              select sai.type
          from
              ((vaxiom.employee_benefit ebe
          join vaxiom.insurance_plan_saving_account sai on
              ((ebe.insurance_plan_id = sai.insurance_plan_id)))
          join vaxiom.benefit_saving_account_values bsavi on
              ((bsavi.insurance_plan_saving_account_id = sai.id)))
          where
              ((ebe.employee_enrollment_id = eb.employee_enrollment_id)
              and ((case when ebe.is_waived is null then true else false end)
              or (ebe.is_waived = '0'))
              and ((case when bsavi.waived is null then true else false end)
              or (bsavi.waived = '0')))) then 1
          else 0
      end)) as is_hsa,
      bit_or(
      (
          case
          when 'EMPLOYEE' in (
              select sai.type
          from
              ((vaxiom.employee_benefit ebe
          join vaxiom.benefit_saving_account_values bsavi on
              ((ebe.id = bsavi.employee_benefit_id)))
          join vaxiom.insurance_plan_saving_account sai on
              ((bsavi.insurance_plan_saving_account_id = sai.id)))
          where
              ((ebe.employee_enrollment_id = eb.employee_enrollment_id)
              and (bsavi.waived = '0'))) then 1
          else 0
      end)) as is_hc_fsa,
      bit_or(
      (
          case
          when 'DEPENDENT' in (
              select sai.type
          from
              ((vaxiom.employee_benefit ebe
          join vaxiom.benefit_saving_account_values bsavi on
              ((ebe.id = bsavi.employee_benefit_id)))
          join vaxiom.insurance_plan_saving_account sai on
              ((bsavi.insurance_plan_saving_account_id = sai.id)))
          where
              ((ebe.employee_enrollment_id = eb.employee_enrollment_id)
              and (bsavi.waived = '0'))) then 1
          else 0
      end)) as is_dc_fsa,

      sum(
        CASE
            WHEN eb.benefit_type::text = 'BL'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS bl_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'BL_ADND'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS bl_adnd_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'STD'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS std_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'LTD'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS ltd_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'VL'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS vl_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'VL_ADND'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS vl_adnd_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'CL'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS cl_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'CANCER'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS cancer_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'ACCIDENT'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS accident_benefit_amount,
    sum(
        CASE
            WHEN eb.benefit_type::text = 'HOSPITALIZATION'::text AND (eb.is_waived = false OR eb.benefit_enrollment_status::text = 'NONE'::text) THEN ev.benefit_amout
            ELSE 0
        END)::decimal(30, 10) AS hospitalization_benefit_amount

      /*,
      sum((case when ((eb.benefit_type = 'BL') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0 end))::decimal(30, 10) as bl_benefit_amount,
      sum((case when ((eb.benefit_type = 'BL_ADND') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0 end))::decimal(30, 10) as bl_adnd_benefit_amount,
      sum((case when ((eb.benefit_type = 'STD') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0 end))::decimal(30, 10) as std_benefit_amount,
      sum((case when ((eb.benefit_type = 'LTD') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0 end))::decimal(30, 10) as ltd_benefit_amount,
      sum((case when ((eb.benefit_type = 'VL') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0 end))::decimal(30, 10) as vl_benefit_amount,
      sum((case when ((eb.benefit_type = 'VL_ADND') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0 end))::decimal(30, 10) as vl_adnd_benefit_amount,
      sum((case when ((eb.benefit_type = 'CL') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0 end))::decimal(30, 10) as cl_benefit_amount,
      sum((case when ((eb.benefit_type = 'CANCER') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0 end))::decimal(30, 10) as cancer_benefit_amount,
      sum((case when ((eb.benefit_type = 'ACCIDENT') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout) else 0 end))::decimal(30, 10) as accident_benefit_amount,
      sum((case when ((eb.benefit_type = 'HOSPITALIZATION') and ((eb.is_waived = '0') or (eb.benefit_enrollment_status = 'NONE')) then ev.benefit_amout else 0) end))::decimal(30, 10) as hospitalization_benefit_amount
  		*/
  from
      (((((vaxiom.employee_dependent ed
  left join vaxiom.employee_enrollment_person ep on
      ((ep.id = ed.employee_enrollment_person_id)))
  left join vaxiom.employee_insured ei on
      ((ed.id = ei.employee_dependent_id)))
  left join vaxiom.employee_benefit eb on
      ((eb.id = ei.employee_benefit_id)))
  left join vaxiom.insurance_plan ip on
      ((eb.insurance_plan_id = ip.id)))
  left join vaxiom.insured_benefit_values ev on
      ((ei.id = ev.employee_insured_id)))
  group by
      ed.id,
      ep.first_name,
      ep.last_name,
      ed.employee_enrollment_id,
      ep.date_of_birth,
      ep.relationship;

create
or replace
view vaxiom.employee_enrollment_elected_benefit_view as
   	select
       eb.id as id,
       eb.employee_enrollment_id as employee_enrollment_id,
       eb.benefit_type as benefit_type,
       eb.post_tax_rate as post_tax_rate,
       eb.pre_tax_rate as pre_tax_rate,
       eb.employer_rate as employer_rate,
       eb.covarage_level as covereage,
       ip.plan_name as plan_name,
       ip.carrier as carrier,
       eb.is_waived as is_waived,
       ip.summary_plan_pdf as summary_plan_pdf,
       ip.summary_plan_pdf_file_name as summary_plan_pdf_file_name,
       'GENERAL' as TYPE,
       eb.benefit_enrollment_status as benefit_enrollment_status
   from
       ((((vaxiom.employee_benefit eb
   left join vaxiom.insurance_plan ip on
       ((eb.insurance_plan_id = ip.id)))
   left join vaxiom.employee_insured ei on
       ((eb.id = ei.employee_benefit_id)))
   left join vaxiom.employee_dependent ed on
       ((ei.employee_dependent_id = ed.id)))
   left join vaxiom.employee_enrollment_person ep on
       ((ep.id = ed.employee_enrollment_person_id)))
   where
       ((eb.is_waived = '0')
       and (eb.benefit_type <> 'FSA'))
   group by
       eb.id,
       eb.employee_enrollment_id,
       eb.benefit_type,
       eb.post_tax_rate,
       eb.pre_tax_rate,
       eb.employer_rate,
       covereage,
       ip.plan_name,
       ip.carrier,
       eb.is_waived,
       ip.summary_plan_pdf,
       ip.summary_plan_pdf_file_name,
       eb.benefit_enrollment_status
   union select
       sav.id as id,
       eb.employee_enrollment_id as employee_enrollment_id,
       (case
           when (sa.type = 'HSA') then 'HSA'
           when (sa.type = 'DEPENDENT') then 'DCFSA'
           when (sa.type = 'EMPLOYEE') then 'HCFSA'
           when (sa.type = 'BONUS_UP') then 'BONUS_UP'
       end) as name,
       sav.post_tax_rate as post_tax_rate,
       sav.pre_tax_rate as pre_tax_rate,
       sav.employer_rate as employer_rate,
       'Enrolled' as Enrolled,
       ip.plan_name as plan_name,
       ip.carrier as carrier,
       sav.waived as waived,
       sa.summary_plan_pdf as summary_plan_pdf,
       sa.summary_plan_pdf_file_name as summary_plan_pdf_file_name,
       sa.type as TYPE,
       eb.benefit_enrollment_status as benefit_enrollment_status
   from
       (((vaxiom.benefit_saving_account_values sav
   left join vaxiom.insurance_plan_saving_account sa on
       ((sa.id = sav.insurance_plan_saving_account_id)))
   left join vaxiom.insurance_plan ip on
       ((ip.id = sa.insurance_plan_id)))
   left join vaxiom.employee_benefit eb on
       ((eb.id = sav.employee_benefit_id)));

create
or replace
view vaxiom.employee_enrollment_view as select
    efse.source_type as source_type,
    ee.employment_contracts_id as employment_contract_id,
    ee.first_name as first_name,
    ee.last_name as last_name,
    (
        select cp.number
    from
        (((vaxiom.persons per
    join vaxiom.contact_method_associations cma on
        (((cma.person_id = per.id)
        and (cma.contact_order = 1))))
    join vaxiom.contact_methods cm on
        (((cm.id = cma.contact_method_id)
        and (cm.dtype = 'phone'))))
    join vaxiom.contact_phones cp on
        ((cp.id = cm.id)))
    where
        (per.id = p.id)
    limit 1) as phone_number,
    (
        select ce.address
    from
        (((vaxiom.persons per
    join vaxiom.contact_method_associations cma on
        (((cma.person_id = per.id)
        and (cma.contact_order = 1))))
    join vaxiom.contact_methods cm on
        (((cm.id = cma.contact_method_id)
        and (cm.dtype = 'email'))))
    join vaxiom.contact_emails ce on
        ((ce.id = cm.id)))
    where
        (per.id = p.id)
    limit 1) as email,
    (
        select legal_entity.legal_name
    from
        vaxiom.legal_entity
    where
        (legal_entity.id = e.legal_entity_id)) as group,
    (
        select legal_entity.legal_name
    from
        vaxiom.legal_entity
    where
        (legal_entity.id = ec.legal_entity_id)) as employer_name,
    ec.legal_entity_id as employer_id,
    ee.fill_date as fill_date,
    ee.status as status,
    e.year as year,
    ee.id as employee_enrollment_id,
    e.id as enrollment_id,
    eea.file_id as file_id,
    eea.file_name as file_name
from
    (((((vaxiom.employee_enrollment ee
join vaxiom.enrollment_form_source_event efse on
    ((ee.enrollment_form_source_event_id = efse.id)))
join vaxiom.employment_contracts ec on
    (((ee.employment_contracts_id = ec.id)
    and (case when ec.employment_end_date is null then true else false end))))
join vaxiom.enrollment e on
    ((e.id = ee.enrollment_id)))
join vaxiom.employee_enrollment_attachment eea on
    ((ee.id = eea.employee_enrollment_id)))
join vaxiom.persons p on
    (((ec.person_id = p.id)
    and (p.portal_user_id is not null))));


create
or replace
view vaxiom.employment_contract_enrollment_group_view as select
        ec.id as id,
        eg.name as eg_name,
        egl.date_of_occurance as eg_last_change,
        egl.commnet as eg_last_change_note,
        bg.name as bg_name
    from
        (((vaxiom.employment_contracts ec
    left join vaxiom.employment_contract_enrollment_group_log egl on
        ((ec.id = egl.employment_contracts_id)))
    left join vaxiom.enrollment_group eg on
        ((ec.enrollment_group_id = eg.id)))
    left join vaxiom.legal_entity_billing_group bg on
        ((bg.id = ec.billing_group_id)))
    where
        ((
            select temp.id
        from
            vaxiom.employment_contract_enrollment_group_log temp
        where
            (temp.employment_contracts_id = ec.id)
        order by
            temp.id desc
        limit 1) = egl.id);



create
or replace
view vaxiom.employment_contract_view as select
            ec.id as id,
            ec.employment_end_date as employment_end_date,
            ec.employment_start_date as employment_start_date,
            p.first_name as first_name,
            p.middle_name as middle_name,
            p.last_name as last_name,
            p.greeting as greeting,
            p.title as title,
            p.prefix as prefix,
            p.suffix as suffix,
            ec.human_id as human_id,
            le.legal_name as employer_name,
            le.id as employer_id,
            jt.name as job_title,
            division.name as division,
            department.name as department,
            eg.name as enrollment_group,
            ec.job_status as job_status,
            ec.deleted as deleted
        from
            ((((((vaxiom.employment_contracts ec
        left join vaxiom.persons p on
            ((p.id = ec.person_id)))
        left join vaxiom.legal_entity le on
            ((le.id = ec.legal_entity_id)))
        left join vaxiom.sys_organizations division on
            ((division.id = ec.division_id)))
        left join vaxiom.sys_organizations department on
            ((department.id = ec.department_id)))
        left join vaxiom.enrollment_group eg on
            ((eg.id = ec.enrollment_group_id)))
        left join vaxiom.jobtitle jt on
            ((jt.id = ec.jobtitle_id)));

create
or replace
view vaxiom.enrollment_master_view_base as select
             le.id as legal_entity_id,
             le.legal_name as legal_name,
             en.id as id,
             en.year as year,
             en.oe_start as oe_start,
             en.oe_end as oe_end,
             en.effective_coverage_start as effective_coverage_start,
             le.legal_name as owner,
             0 as is_participating,
             count(distinct ec.id) as employee_no,
             count((case when (ee.status = 'DRAFT') then ee.status else null end)) as draft_enrollment,
             count((case when (ee.status = 'FINISHED') then ee.status else null end)) as finished_enrollment,
             count((case when (ee.status = 'APPROVED') then ee.status else null end)) as approved_enrollment,
             count((case when (ee.status = 'DENIED') then ee.status else null end)) as denied_enrollment,
             count((case when (ee.status = 'EFFECTIVE') then ee.status else null end)) as effective_enrollment
         from
             (((vaxiom.legal_entity le
         left join vaxiom.employment_contracts ec on
             ((ec.legal_entity_id = le.id)))
         left join vaxiom.enrollment en on
             ((en.legal_entity_id = le.id)))
         left join vaxiom.employee_enrollment ee on
             (((ec.id = ee.employment_contracts_id)
             and (ee.enrollment_id = en.id))))
         where
             (en.id is not null)
         group by
             le.id,
             le.legal_name,
             en.id,
             en.year,
             en.oe_start,
             en.oe_end,
             le.legal_name,
             en.effective_coverage_start,
             is_participating
         union select
             le.id as legal_entity_id,
             le.legal_name as legal_name,
             en.id as id,
             en.year as year,
             en.oe_start as oe_start,
             en.oe_end as oe_end,
             en.effective_coverage_start as effective_coverage_start,
             owner.legal_name as owner,
             1 as is_participating,
             count(distinct ec.id) as employee_no,
             count((case when (ee.status = 'DRAFT') then ee.status else null end)) as draft_enrollment,
             count((case when (ee.status = 'FINISHED') then ee.status else null end)) as finished_enrollment,
             count((case when (ee.status = 'APPROVED') then ee.status else null end)) as approved_enrollment,
             count((case when (ee.status = 'DENIED') then ee.status else null end)) as denied_enrollment,
             count((case when (ee.status = 'EFFECTIVE') then ee.status else null end)) as effective_enrollment
         from
             (((((vaxiom.legal_entity le
         left join vaxiom.legal_entities_participating_enrollment lep on
             ((lep.legal_entity_id = le.id)))
         left join vaxiom.enrollment en on
             ((lep.enrollment_id = en.id)))
         left join vaxiom.employment_contracts ec on
             ((ec.legal_entity_id = le.id)))
         left join vaxiom.legal_entity owner on
             ((en.legal_entity_id = owner.id)))
         left join vaxiom.employee_enrollment ee on
             (((ec.id = ee.employment_contracts_id)
             and (ee.enrollment_id = en.id))))
         where
             (owner.legal_name is not null)
         group by
             le.id,
             le.legal_name,
             en.id,
             en.year,
             en.oe_start,
             en.oe_end,
             owner.legal_name,
             en.effective_coverage_start,
             is_participating;

create
or replace
view vaxiom.enrollment_master_view as select
                 ag.legal_entity_id as legal_entity_id,
                 ag.legal_name as legal_name,
                 ag.id as id,
                 ag.year as year,
                 ag.oe_start as oe_start,
                 ag.oe_end as oe_end,
                 ag.effective_coverage_start as effective_coverage_start,
                 ag.owner as owner,
                 ag.is_participating as is_participating,
                 ag.is_participating as is_participating_support,
                 ag.employee_no as employee_no,
                 ag.draft_enrollment as draft_enrollment,
                 ag.finished_enrollment as finished_enrollment,
                 ag.approved_enrollment as approved_enrollment,
                 ag.denied_enrollment as denied_enrollment,
                 ag.effective_enrollment as effective_enrollment,
                 count(distinct (case when (ipe.status in ('NEW', 'RENEWED')) then ip.benefit_type else null end)) as benefit_no,
                 count((case when (ipe.status in ('NEW', 'RENEWED')) then ip.id else null end)) as plan_on,
                 e.status as status,
                 e.expiration_date as expiration_date,
                 cast(e.sys_last_modified as date) as last_modified,
                 concat(concat(p.first_name, ' '), p.last_name) as last_modified_by
             from
                 (((((vaxiom.enrollment_master_view_base ag
             left join vaxiom.insurance_plan_has_enrollment ipe on
                 ((ipe.enrollment_id = ag.id)))
             left join vaxiom.insurance_plan ip on
                 ((ipe.insurance_plan_id = ip.id)))
             join vaxiom.enrollment e on
                 ((ag.id = e.id)))
             join vaxiom.sys_users u on
                 ((u.id = e.sys_last_modified_by)))
             left join vaxiom.persons p on
                 ((p.core_user_id = u.id)))
             group by
                 ag.id,
                 ag.year,
                 ag.oe_start,
                 ag.oe_end,
                 ag.legal_name,
                 ag.employee_no,
                 ag.draft_enrollment,
                 ag.finished_enrollment,
                 ag.approved_enrollment,
                 ag.denied_enrollment,
                 ag.effective_enrollment,
                 ag.legal_entity_id,
                 ag.owner,
                 ag.is_participating,
                 ag.effective_coverage_start,
                 e.status,
                 e.expiration_date,
                e.sys_last_modified,
                p.first_name,
                p.last_name;

create or replace  view vaxiom.employment_contract_rehire_view as
select

    ec.id as id,
    ec.employment_start_date as employment_start_date,

    ec.employment_end_date as employment_end_date,

    p.title as title,

    p.first_name as first_name,

    p.middle_name as middle_name,

    p.last_name as last_name,

    p.prefix as prefix,

    p.suffix as suffix,

    p.ssn as ssn,

    p.id_number as id_number,

    ec.human_id as human_id,

    le.legal_name as employer_name,

    le.id as employer_id,

    jt.name as job_title,

    postal.zip as zip,

    postal.city as city,

    postal.state as state

from  vaxiom.persons p

join vaxiom.employment_contracts ec on  p.id = ec.person_id  and (p.core_user_id is not null)

join vaxiom.legal_entity le on ec.legal_entity_id = le.id

left join vaxiom.jobtitle jt on  ec.jobtitle_id = jt.id

left join (

select cpa.zip, cpa.city, cpa.state, cma.person_id
from vaxiom.contact_methods cm
inner join vaxiom.contact_method_associations cma on  cma.contact_method_id = cm.id and cm.dtype='postal'
inner  join vaxiom.contact_postal_addresses cpa  on cpa.id =cma.contact_method_id

    ) as postal on postal.person_id = ec.person_id


where
ec.job_status = 'TERMINATED'
and (not exists (select 1 from vaxiom.employment_contracts ec2 where ec2.person_id = ec.person_id and ec2.employment_start_date > ec.employment_start_date))
