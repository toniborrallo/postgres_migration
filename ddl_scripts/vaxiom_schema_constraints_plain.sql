--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

-- Started on 2020-03-10 15:12:20 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;



--
-- TOC entry 6505 (class 2606 OID 892291)
-- Name: activiti_process_settings idx_889332_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.activiti_process_settings
    ADD CONSTRAINT idx_889332_primary PRIMARY KEY (id);


--
-- TOC entry 6507 (class 2606 OID 892292)
-- Name: activiti_process_settings_property idx_889335_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.activiti_process_settings_property
    ADD CONSTRAINT idx_889335_primary PRIMARY KEY (activiti_process_settings_id, property);


--
-- TOC entry 6509 (class 2606 OID 892293)
-- Name: act_evt_log idx_889343_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_evt_log
    ADD CONSTRAINT idx_889343_primary PRIMARY KEY (log_nr_);


--
-- TOC entry 6512 (class 2606 OID 892294)
-- Name: act_ge_bytearray idx_889351_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ge_bytearray
    ADD CONSTRAINT idx_889351_primary PRIMARY KEY (id_);


--
-- TOC entry 6514 (class 2606 OID 892295)
-- Name: act_ge_property idx_889357_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ge_property
    ADD CONSTRAINT idx_889357_primary PRIMARY KEY (name_);


--
-- TOC entry 6520 (class 2606 OID 892296)
-- Name: act_hi_actinst idx_889360_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_actinst
    ADD CONSTRAINT idx_889360_primary PRIMARY KEY (id_);


--
-- TOC entry 6522 (class 2606 OID 892297)
-- Name: act_hi_attachment idx_889367_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_attachment
    ADD CONSTRAINT idx_889367_primary PRIMARY KEY (id_);


--
-- TOC entry 6524 (class 2606 OID 892298)
-- Name: act_hi_comment idx_889373_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_comment
    ADD CONSTRAINT idx_889373_primary PRIMARY KEY (id_);


--
-- TOC entry 6531 (class 2606 OID 892299)
-- Name: act_hi_detail idx_889379_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_detail
    ADD CONSTRAINT idx_889379_primary PRIMARY KEY (id_);


--
-- TOC entry 6536 (class 2606 OID 892300)
-- Name: act_hi_identitylink idx_889385_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_identitylink
    ADD CONSTRAINT idx_889385_primary PRIMARY KEY (id_);


--
-- TOC entry 6540 (class 2606 OID 892301)
-- Name: act_hi_procinst idx_889391_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_procinst
    ADD CONSTRAINT idx_889391_primary PRIMARY KEY (id_);


--
-- TOC entry 6544 (class 2606 OID 892302)
-- Name: act_hi_taskinst idx_889398_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_taskinst
    ADD CONSTRAINT idx_889398_primary PRIMARY KEY (id_);


--
-- TOC entry 6549 (class 2606 OID 892303)
-- Name: act_hi_varinst idx_889405_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_varinst
    ADD CONSTRAINT idx_889405_primary PRIMARY KEY (id_);


--
-- TOC entry 6551 (class 2606 OID 892304)
-- Name: act_id_group idx_889411_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_group
    ADD CONSTRAINT idx_889411_primary PRIMARY KEY (id_);


--
-- TOC entry 6553 (class 2606 OID 892305)
-- Name: act_id_info idx_889417_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_info
    ADD CONSTRAINT idx_889417_primary PRIMARY KEY (id_);


--
-- TOC entry 6556 (class 2606 OID 892306)
-- Name: act_id_membership idx_889423_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_membership
    ADD CONSTRAINT idx_889423_primary PRIMARY KEY (user_id_, group_id_);


--
-- TOC entry 6558 (class 2606 OID 892307)
-- Name: act_id_user idx_889426_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_user
    ADD CONSTRAINT idx_889426_primary PRIMARY KEY (id_);


--
-- TOC entry 6560 (class 2606 OID 892308)
-- Name: act_re_deployment idx_889432_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_deployment
    ADD CONSTRAINT idx_889432_primary PRIMARY KEY (id_);


--
-- TOC entry 6565 (class 2606 OID 892309)
-- Name: act_re_model idx_889439_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_model
    ADD CONSTRAINT idx_889439_primary PRIMARY KEY (id_);


--
-- TOC entry 6568 (class 2606 OID 892310)
-- Name: act_re_procdef idx_889446_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_procdef
    ADD CONSTRAINT idx_889446_primary PRIMARY KEY (id_);


--
-- TOC entry 6572 (class 2606 OID 892311)
-- Name: act_ru_event_subscr idx_889453_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_event_subscr
    ADD CONSTRAINT idx_889453_primary PRIMARY KEY (id_);


--
-- TOC entry 6579 (class 2606 OID 892312)
-- Name: act_ru_execution idx_889461_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT idx_889461_primary PRIMARY KEY (id_);


--
-- TOC entry 6586 (class 2606 OID 892313)
-- Name: act_ru_identitylink idx_889468_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_identitylink
    ADD CONSTRAINT idx_889468_primary PRIMARY KEY (id_);


--
-- TOC entry 6589 (class 2606 OID 892314)
-- Name: act_ru_job idx_889474_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_job
    ADD CONSTRAINT idx_889474_primary PRIMARY KEY (id_);


--
-- TOC entry 6595 (class 2606 OID 892315)
-- Name: act_ru_task idx_889481_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_task
    ADD CONSTRAINT idx_889481_primary PRIMARY KEY (id_);


--
-- TOC entry 6601 (class 2606 OID 892316)
-- Name: act_ru_variable idx_889488_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_variable
    ADD CONSTRAINT idx_889488_primary PRIMARY KEY (id_);


--
-- TOC entry 6604 (class 2606 OID 892317)
-- Name: added_report_type idx_889494_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.added_report_type
    ADD CONSTRAINT idx_889494_primary PRIMARY KEY (id);


--
-- TOC entry 6606 (class 2606 OID 892318)
-- Name: answers idx_889497_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.answers
    ADD CONSTRAINT idx_889497_primary PRIMARY KEY (id);


--
-- TOC entry 6609 (class 2606 OID 892319)
-- Name: application_properties idx_889503_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.application_properties
    ADD CONSTRAINT idx_889503_primary PRIMARY KEY (organization_id, message_key);


--
-- TOC entry 6612 (class 2606 OID 892320)
-- Name: applied_payments idx_889509_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.applied_payments
    ADD CONSTRAINT idx_889509_primary PRIMARY KEY (id);


--
-- TOC entry 6616 (class 2606 OID 892321)
-- Name: appointments idx_889512_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT idx_889512_primary PRIMARY KEY (id);


--
-- TOC entry 6623 (class 2606 OID 892322)
-- Name: appointment_audit_log idx_889521_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_audit_log
    ADD CONSTRAINT idx_889521_primary PRIMARY KEY (id);


--
-- TOC entry 6632 (class 2606 OID 892323)
-- Name: appointment_bookings idx_889524_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT idx_889524_primary PRIMARY KEY (id);


--
-- TOC entry 6638 (class 2606 OID 892324)
-- Name: appointment_booking_resources idx_889531_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_resources
    ADD CONSTRAINT idx_889531_primary PRIMARY KEY (id);


--
-- TOC entry 6642 (class 2606 OID 892325)
-- Name: appointment_booking_state_changes idx_889538_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_state_changes
    ADD CONSTRAINT idx_889538_primary PRIMARY KEY (id);


--
-- TOC entry 6646 (class 2606 OID 892326)
-- Name: appointment_field_values idx_889544_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_field_values
    ADD CONSTRAINT idx_889544_primary PRIMARY KEY (appointment_id, field_option_id, field_definition_id);


--
-- TOC entry 6648 (class 2606 OID 892327)
-- Name: appointment_procedures idx_889547_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_procedures
    ADD CONSTRAINT idx_889547_primary PRIMARY KEY (appointment_id, procedure_id, pos);


--
-- TOC entry 6653 (class 2606 OID 892328)
-- Name: appointment_templates idx_889554_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT idx_889554_primary PRIMARY KEY (id);


--
-- TOC entry 6657 (class 2606 OID 892329)
-- Name: appointment_template_procedures idx_889561_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_template_procedures
    ADD CONSTRAINT idx_889561_primary PRIMARY KEY (appointment_template_id, pos);


--
-- TOC entry 6664 (class 2606 OID 892330)
-- Name: appointment_types idx_889564_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_types
    ADD CONSTRAINT idx_889564_primary PRIMARY KEY (id);


--
-- TOC entry 6666 (class 2606 OID 892331)
-- Name: appointment_types_property idx_889574_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_types_property
    ADD CONSTRAINT idx_889574_primary PRIMARY KEY (appointment_type_id, property);


--
-- TOC entry 6670 (class 2606 OID 892332)
-- Name: appointment_type_daily_restrictions idx_889580_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_type_daily_restrictions
    ADD CONSTRAINT idx_889580_primary PRIMARY KEY (id);


--
-- TOC entry 6673 (class 2606 OID 892333)
-- Name: appt_type_template_restrictions idx_889584_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appt_type_template_restrictions
    ADD CONSTRAINT idx_889584_primary PRIMARY KEY (id);


--
-- TOC entry 6677 (class 2606 OID 892334)
-- Name: audit_events idx_889588_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.audit_events
    ADD CONSTRAINT idx_889588_primary PRIMARY KEY (id);


--
-- TOC entry 6679 (class 2606 OID 892335)
-- Name: audit_logs idx_889594_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.audit_logs
    ADD CONSTRAINT idx_889594_primary PRIMARY KEY (id);


--
-- TOC entry 6682 (class 2606 OID 892336)
-- Name: autopay_invoice_changes idx_889597_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.autopay_invoice_changes
    ADD CONSTRAINT idx_889597_primary PRIMARY KEY (id);


--
-- TOC entry 6685 (class 2606 OID 892337)
-- Name: bank_accounts idx_889603_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.bank_accounts
    ADD CONSTRAINT idx_889603_primary PRIMARY KEY (id);


--
-- TOC entry 6691 (class 2606 OID 892338)
-- Name: benefit_saving_account_values idx_889609_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.benefit_saving_account_values
    ADD CONSTRAINT idx_889609_primary PRIMARY KEY (id);


--
-- TOC entry 6693 (class 2606 OID 892339)
-- Name: bluefin_credentials idx_889612_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.bluefin_credentials
    ADD CONSTRAINT idx_889612_primary PRIMARY KEY (organization_id);


--
-- TOC entry 6695 (class 2606 OID 892340)
-- Name: cancelled_receivables idx_889621_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.cancelled_receivables
    ADD CONSTRAINT idx_889621_primary PRIMARY KEY (receivable_id);


--
-- TOC entry 6698 (class 2606 OID 892341)
-- Name: cephx_requests idx_889624_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.cephx_requests
    ADD CONSTRAINT idx_889624_primary PRIMARY KEY (id);


--
-- TOC entry 6700 (class 2606 OID 892342)
-- Name: chairs idx_889628_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chairs
    ADD CONSTRAINT idx_889628_primary PRIMARY KEY (id);


--
-- TOC entry 6704 (class 2606 OID 892343)
-- Name: chair_allocations idx_889635_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chair_allocations
    ADD CONSTRAINT idx_889635_primary PRIMARY KEY (id);


--
-- TOC entry 6707 (class 2606 OID 892344)
-- Name: charge_transfer_adjustments idx_889639_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.charge_transfer_adjustments
    ADD CONSTRAINT idx_889639_primary PRIMARY KEY (id);


--
-- TOC entry 6710 (class 2606 OID 892345)
-- Name: checks idx_889642_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.checks
    ADD CONSTRAINT idx_889642_primary PRIMARY KEY (id);


--
-- TOC entry 6713 (class 2606 OID 892346)
-- Name: check_types idx_889645_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.check_types
    ADD CONSTRAINT idx_889645_primary PRIMARY KEY (id);


--
-- TOC entry 6715 (class 2606 OID 892347)
-- Name: claim_events idx_889648_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_events
    ADD CONSTRAINT idx_889648_primary PRIMARY KEY (id);


--
-- TOC entry 6720 (class 2606 OID 892348)
-- Name: claim_requests idx_889654_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT idx_889654_primary PRIMARY KEY (id);


--
-- TOC entry 6728 (class 2606 OID 892349)
-- Name: collection_labels idx_889666_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT idx_889666_primary PRIMARY KEY (patient_id, payment_account_id);


--
-- TOC entry 6731 (class 2606 OID 892350)
-- Name: collection_label_agencies idx_889669_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_agencies
    ADD CONSTRAINT idx_889669_primary PRIMARY KEY (id);


--
-- TOC entry 6737 (class 2606 OID 892351)
-- Name: collection_label_history idx_889672_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT idx_889672_primary PRIMARY KEY (id);


--
-- TOC entry 6741 (class 2606 OID 892352)
-- Name: collection_label_templates idx_889675_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_templates
    ADD CONSTRAINT idx_889675_primary PRIMARY KEY (id);


--
-- TOC entry 6745 (class 2606 OID 892353)
-- Name: communication_preferences idx_889681_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.communication_preferences
    ADD CONSTRAINT idx_889681_primary PRIMARY KEY (id);


--
-- TOC entry 6748 (class 2606 OID 892354)
-- Name: contact_emails idx_889684_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_emails
    ADD CONSTRAINT idx_889684_primary PRIMARY KEY (id);


--
-- TOC entry 6750 (class 2606 OID 892355)
-- Name: contact_methods idx_889687_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_methods
    ADD CONSTRAINT idx_889687_primary PRIMARY KEY (id);


--
-- TOC entry 6755 (class 2606 OID 892356)
-- Name: contact_method_associations idx_889691_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_method_associations
    ADD CONSTRAINT idx_889691_primary PRIMARY KEY (id);


--
-- TOC entry 6758 (class 2606 OID 892357)
-- Name: contact_phones idx_889694_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_phones
    ADD CONSTRAINT idx_889694_primary PRIMARY KEY (id);


--
-- TOC entry 6762 (class 2606 OID 892358)
-- Name: contact_postal_addresses idx_889697_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_postal_addresses
    ADD CONSTRAINT idx_889697_primary PRIMARY KEY (id);


--
-- TOC entry 6764 (class 2606 OID 892359)
-- Name: contact_recently_opened_items idx_889703_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_recently_opened_items
    ADD CONSTRAINT idx_889703_primary PRIMARY KEY (id);


--
-- TOC entry 6766 (class 2606 OID 892360)
-- Name: contact_website idx_889706_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_website
    ADD CONSTRAINT idx_889706_primary PRIMARY KEY (id);


--
-- TOC entry 6770 (class 2606 OID 892361)
-- Name: correction_types idx_889709_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types
    ADD CONSTRAINT idx_889709_primary PRIMARY KEY (id);


--
-- TOC entry 6773 (class 2606 OID 892362)
-- Name: correction_types_locations idx_889715_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_locations
    ADD CONSTRAINT idx_889715_primary PRIMARY KEY (correction_type_id, organization_id);


--
-- TOC entry 6776 (class 2606 OID 892363)
-- Name: correction_types_payment_options idx_889718_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_payment_options
    ADD CONSTRAINT idx_889718_primary PRIMARY KEY (correction_types_id, payment_options_id);


--
-- TOC entry 6779 (class 2606 OID 892364)
-- Name: county idx_889721_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.county
    ADD CONSTRAINT idx_889721_primary PRIMARY KEY (id);


--
-- TOC entry 6782 (class 2606 OID 892365)
-- Name: daily_transactions idx_889724_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.daily_transactions
    ADD CONSTRAINT idx_889724_primary PRIMARY KEY (id);


--
-- TOC entry 6785 (class 2606 OID 892366)
-- Name: data_lock idx_889731_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.data_lock
    ADD CONSTRAINT idx_889731_primary PRIMARY KEY (id);


--
-- TOC entry 6789 (class 2606 OID 892367)
-- Name: day_schedules idx_889734_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedules
    ADD CONSTRAINT idx_889734_primary PRIMARY KEY (id);


--
-- TOC entry 6793 (class 2606 OID 892368)
-- Name: day_schedule_appt_slots idx_889741_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_appt_slots
    ADD CONSTRAINT idx_889741_primary PRIMARY KEY (day_schedule_id, appt_type_id, start_min);


--
-- TOC entry 6795 (class 2606 OID 892369)
-- Name: day_schedule_break_hours idx_889744_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_break_hours
    ADD CONSTRAINT idx_889744_primary PRIMARY KEY (day_schedule_id, end_min, start_min);


--
-- TOC entry 6797 (class 2606 OID 892370)
-- Name: day_schedule_office_hours idx_889747_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_office_hours
    ADD CONSTRAINT idx_889747_primary PRIMARY KEY (day_schedule_id, end_min, start_min);


--
-- TOC entry 6800 (class 2606 OID 892371)
-- Name: default_appointment_templates idx_889750_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.default_appointment_templates
    ADD CONSTRAINT idx_889750_primary PRIMARY KEY (id);


--
-- TOC entry 6804 (class 2606 OID 892372)
-- Name: dentalchart_additional_markings idx_889753_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_additional_markings
    ADD CONSTRAINT idx_889753_primary PRIMARY KEY (id);


--
-- TOC entry 6806 (class 2606 OID 892373)
-- Name: dentalchart_edit_log idx_889756_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_edit_log
    ADD CONSTRAINT idx_889756_primary PRIMARY KEY (id);


--
-- TOC entry 6809 (class 2606 OID 892374)
-- Name: dentalchart_marking idx_889759_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_marking
    ADD CONSTRAINT idx_889759_primary PRIMARY KEY (id);


--
-- TOC entry 6812 (class 2606 OID 892375)
-- Name: dentalchart_materials idx_889762_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_materials
    ADD CONSTRAINT idx_889762_primary PRIMARY KEY (id);


--
-- TOC entry 6815 (class 2606 OID 892376)
-- Name: dentalchart_snapshot idx_889768_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_snapshot
    ADD CONSTRAINT idx_889768_primary PRIMARY KEY (id);


--
-- TOC entry 6819 (class 2606 OID 892377)
-- Name: diagnosis idx_889774_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis
    ADD CONSTRAINT idx_889774_primary PRIMARY KEY (id);


--
-- TOC entry 6821 (class 2606 OID 892378)
-- Name: diagnosis_field_values idx_889780_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_field_values
    ADD CONSTRAINT idx_889780_primary PRIMARY KEY (diagnosis_id, field_key);


--
-- TOC entry 6826 (class 2606 OID 892379)
-- Name: diagnosis_letters idx_889786_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT idx_889786_primary PRIMARY KEY (id);


--
-- TOC entry 6831 (class 2606 OID 892380)
-- Name: diagnosis_templates idx_889789_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_templates
    ADD CONSTRAINT idx_889789_primary PRIMARY KEY (id);


--
-- TOC entry 6835 (class 2606 OID 892381)
-- Name: discounts idx_889795_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discounts
    ADD CONSTRAINT idx_889795_primary PRIMARY KEY (id);


--
-- TOC entry 6838 (class 2606 OID 892382)
-- Name: discount_adjustments idx_889802_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_adjustments
    ADD CONSTRAINT idx_889802_primary PRIMARY KEY (id);


--
-- TOC entry 6841 (class 2606 OID 892383)
-- Name: discount_reverse_adjustments idx_889805_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_reverse_adjustments
    ADD CONSTRAINT idx_889805_primary PRIMARY KEY (id);


--
-- TOC entry 6843 (class 2606 OID 892384)
-- Name: dock_item_descriptors idx_889808_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_descriptors
    ADD CONSTRAINT idx_889808_primary PRIMARY KEY (id);


--
-- TOC entry 6845 (class 2606 OID 892385)
-- Name: dock_item_features_events idx_889811_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_events
    ADD CONSTRAINT idx_889811_primary PRIMARY KEY (id);


--
-- TOC entry 6848 (class 2606 OID 892386)
-- Name: dock_item_features_images idx_889817_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_images
    ADD CONSTRAINT idx_889817_primary PRIMARY KEY (id);


--
-- TOC entry 6850 (class 2606 OID 892387)
-- Name: dock_item_features_messages idx_889820_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_messages
    ADD CONSTRAINT idx_889820_primary PRIMARY KEY (id);


--
-- TOC entry 6853 (class 2606 OID 892388)
-- Name: dock_item_features_note idx_889823_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_note
    ADD CONSTRAINT idx_889823_primary PRIMARY KEY (id);


--
-- TOC entry 6855 (class 2606 OID 892389)
-- Name: dock_item_features_tasks idx_889826_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_tasks
    ADD CONSTRAINT idx_889826_primary PRIMARY KEY (id);


--
-- TOC entry 6857 (class 2606 OID 892390)
-- Name: document_recently_opened_items idx_889832_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_recently_opened_items
    ADD CONSTRAINT idx_889832_primary PRIMARY KEY (id);


--
-- TOC entry 6860 (class 2606 OID 892391)
-- Name: document_templates idx_889835_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_templates
    ADD CONSTRAINT idx_889835_primary PRIMARY KEY (id);


--
-- TOC entry 6865 (class 2606 OID 892392)
-- Name: document_tree_nodes idx_889842_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_tree_nodes
    ADD CONSTRAINT idx_889842_primary PRIMARY KEY (id);


--
-- TOC entry 6867 (class 2606 OID 892393)
-- Name: eeo idx_889850_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.eeo
    ADD CONSTRAINT idx_889850_primary PRIMARY KEY (id);


--
-- TOC entry 6871 (class 2606 OID 892394)
-- Name: employee_beneficiary idx_889853_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary
    ADD CONSTRAINT idx_889853_primary PRIMARY KEY (id);


--
-- TOC entry 6874 (class 2606 OID 892395)
-- Name: employee_beneficiary_group idx_889856_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary_group
    ADD CONSTRAINT idx_889856_primary PRIMARY KEY (id);


--
-- TOC entry 6879 (class 2606 OID 892396)
-- Name: employee_benefit idx_889859_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_benefit
    ADD CONSTRAINT idx_889859_primary PRIMARY KEY (id);


--
-- TOC entry 6883 (class 2606 OID 892397)
-- Name: employee_dependent idx_889867_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_dependent
    ADD CONSTRAINT idx_889867_primary PRIMARY KEY (id);


--
-- TOC entry 6888 (class 2606 OID 892398)
-- Name: employee_enrollment idx_889873_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment
    ADD CONSTRAINT idx_889873_primary PRIMARY KEY (id);


--
-- TOC entry 6891 (class 2606 OID 892399)
-- Name: employee_enrollment_attachment idx_889882_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_attachment
    ADD CONSTRAINT idx_889882_primary PRIMARY KEY (id);


--
-- TOC entry 6894 (class 2606 OID 892400)
-- Name: employee_enrollment_event idx_889888_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_event
    ADD CONSTRAINT idx_889888_primary PRIMARY KEY (id);


--
-- TOC entry 6897 (class 2606 OID 892401)
-- Name: employee_enrollment_person idx_889894_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_person
    ADD CONSTRAINT idx_889894_primary PRIMARY KEY (id);


--
-- TOC entry 6902 (class 2606 OID 892402)
-- Name: employee_insured idx_889900_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_insured
    ADD CONSTRAINT idx_889900_primary PRIMARY KEY (id);


--
-- TOC entry 6904 (class 2606 OID 892403)
-- Name: employee_professional_relationships idx_889904_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_professional_relationships
    ADD CONSTRAINT idx_889904_primary PRIMARY KEY (id);


--
-- TOC entry 6908 (class 2606 OID 892404)
-- Name: employee_resources idx_889907_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_resources
    ADD CONSTRAINT idx_889907_primary PRIMARY KEY (id);


--
-- TOC entry 6915 (class 2606 OID 892405)
-- Name: employers idx_889911_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT idx_889911_primary PRIMARY KEY (id);


--
-- TOC entry 6927 (class 2606 OID 892406)
-- Name: employment_contracts idx_889914_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT idx_889914_primary PRIMARY KEY (id);


--
-- TOC entry 6930 (class 2606 OID 892407)
-- Name: employment_contract_emergency_contact idx_889921_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_emergency_contact
    ADD CONSTRAINT idx_889921_primary PRIMARY KEY (id);


--
-- TOC entry 6933 (class 2606 OID 892408)
-- Name: employment_contract_enrollment_group_log idx_889924_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_enrollment_group_log
    ADD CONSTRAINT idx_889924_primary PRIMARY KEY (id);


--
-- TOC entry 6936 (class 2606 OID 892409)
-- Name: employment_contract_job_status_log idx_889930_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_job_status_log
    ADD CONSTRAINT idx_889930_primary PRIMARY KEY (id);


--
-- TOC entry 6940 (class 2606 OID 892410)
-- Name: employment_contract_location_group idx_889933_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_location_group
    ADD CONSTRAINT idx_889933_primary PRIMARY KEY (id);


--
-- TOC entry 6944 (class 2606 OID 892411)
-- Name: employment_contract_permissions idx_889936_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_permissions
    ADD CONSTRAINT idx_889936_primary PRIMARY KEY (id);


--
-- TOC entry 6948 (class 2606 OID 892412)
-- Name: employment_contract_role idx_889939_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_role
    ADD CONSTRAINT idx_889939_primary PRIMARY KEY (id);


--
-- TOC entry 6951 (class 2606 OID 892413)
-- Name: employment_contract_salary_log idx_889942_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_salary_log
    ADD CONSTRAINT idx_889942_primary PRIMARY KEY (id);


--
-- TOC entry 6954 (class 2606 OID 892414)
-- Name: enrollment idx_889948_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment
    ADD CONSTRAINT idx_889948_primary PRIMARY KEY (id);


--
-- TOC entry 6959 (class 2606 OID 892415)
-- Name: enrollment_form idx_889956_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form
    ADD CONSTRAINT idx_889956_primary PRIMARY KEY (id);


--
-- TOC entry 6962 (class 2606 OID 892416)
-- Name: enrollment_form_beneficiary idx_889962_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_beneficiary
    ADD CONSTRAINT idx_889962_primary PRIMARY KEY (id);


--
-- TOC entry 6965 (class 2606 OID 892417)
-- Name: enrollment_form_benefits idx_889971_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_benefits
    ADD CONSTRAINT idx_889971_primary PRIMARY KEY (id);


--
-- TOC entry 6968 (class 2606 OID 892418)
-- Name: enrollment_form_dependents idx_889980_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_dependents
    ADD CONSTRAINT idx_889980_primary PRIMARY KEY (id);


--
-- TOC entry 6972 (class 2606 OID 892419)
-- Name: enrollment_form_dependents_benefits idx_889989_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_dependents_benefits
    ADD CONSTRAINT idx_889989_primary PRIMARY KEY (id);


--
-- TOC entry 6975 (class 2606 OID 892420)
-- Name: enrollment_form_personal_data idx_889998_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_personal_data
    ADD CONSTRAINT idx_889998_primary PRIMARY KEY (id);


--
-- TOC entry 6977 (class 2606 OID 892421)
-- Name: enrollment_form_source_event idx_890005_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_source_event
    ADD CONSTRAINT idx_890005_primary PRIMARY KEY (id);


--
-- TOC entry 6979 (class 2606 OID 892422)
-- Name: enrollment_group idx_890011_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_group
    ADD CONSTRAINT idx_890011_primary PRIMARY KEY (id);


--
-- TOC entry 6983 (class 2606 OID 892423)
-- Name: enrollment_groups_enrollment_legal_entity idx_890014_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_groups_enrollment_legal_entity
    ADD CONSTRAINT idx_890014_primary PRIMARY KEY (enrollment_group_id, legal_entity_id, enrollment_id);


--
-- TOC entry 6986 (class 2606 OID 892424)
-- Name: enrollment_group_enrollment idx_890017_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_group_enrollment
    ADD CONSTRAINT idx_890017_primary PRIMARY KEY (enrollment_group_id, enrollment_id);


--
-- TOC entry 6989 (class 2606 OID 892425)
-- Name: enrollment_schedule_billing_groups idx_890020_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_schedule_billing_groups
    ADD CONSTRAINT idx_890020_primary PRIMARY KEY (enrollment_id, legal_entity_id, carrier);


--
-- TOC entry 6991 (class 2606 OID 892426)
-- Name: equipment idx_890023_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.equipment
    ADD CONSTRAINT idx_890023_primary PRIMARY KEY (id);


--
-- TOC entry 6994 (class 2606 OID 892427)
-- Name: external_offices idx_890027_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.external_offices
    ADD CONSTRAINT idx_890027_primary PRIMARY KEY (id);


--
-- TOC entry 6997 (class 2606 OID 892428)
-- Name: financial_settings idx_890030_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.financial_settings
    ADD CONSTRAINT idx_890030_primary PRIMARY KEY (id);


--
-- TOC entry 6999 (class 2606 OID 892429)
-- Name: fix_adjustments idx_890036_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.fix_adjustments
    ADD CONSTRAINT idx_890036_primary PRIMARY KEY (id);


--
-- TOC entry 7002 (class 2606 OID 892430)
-- Name: hradmin_enrollment idx_890062_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.hradmin_enrollment
    ADD CONSTRAINT idx_890062_primary PRIMARY KEY (id);


--
-- TOC entry 7004 (class 2606 OID 892431)
-- Name: hr_admin_config idx_890065_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.hr_admin_config
    ADD CONSTRAINT idx_890065_primary PRIMARY KEY (id);


--
-- TOC entry 7007 (class 2606 OID 892432)
-- Name: image_series idx_890068_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.image_series
    ADD CONSTRAINT idx_890068_primary PRIMARY KEY (id);


--
-- TOC entry 7010 (class 2606 OID 892433)
-- Name: image_series_types idx_890071_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.image_series_types
    ADD CONSTRAINT idx_890071_primary PRIMARY KEY (id);


--
-- TOC entry 7013 (class 2606 OID 892434)
-- Name: installment_charges idx_890074_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.installment_charges
    ADD CONSTRAINT idx_890074_primary PRIMARY KEY (id);


--
-- TOC entry 7020 (class 2606 OID 892435)
-- Name: insurance_billing_centers idx_890077_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT idx_890077_primary PRIMARY KEY (id);


--
-- TOC entry 7028 (class 2606 OID 892436)
-- Name: insurance_claims_setup idx_890083_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT idx_890083_primary PRIMARY KEY (id);


--
-- TOC entry 7030 (class 2606 OID 892437)
-- Name: insurance_codes idx_890087_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_codes
    ADD CONSTRAINT idx_890087_primary PRIMARY KEY (id);


--
-- TOC entry 7035 (class 2606 OID 892438)
-- Name: insurance_code_val idx_890093_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_code_val
    ADD CONSTRAINT idx_890093_primary PRIMARY KEY (id);


--
-- TOC entry 7042 (class 2606 OID 892439)
-- Name: insurance_companies idx_890096_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT idx_890096_primary PRIMARY KEY (id);


--
-- TOC entry 7044 (class 2606 OID 892440)
-- Name: insurance_company_payments idx_890104_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_payments
    ADD CONSTRAINT idx_890104_primary PRIMARY KEY (insurance_company_id);


--
-- TOC entry 7048 (class 2606 OID 892441)
-- Name: insurance_company_phone_associations idx_890107_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_phone_associations
    ADD CONSTRAINT idx_890107_primary PRIMARY KEY (id);


--
-- TOC entry 7052 (class 2606 OID 892442)
-- Name: insurance_payments idx_890111_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payments
    ADD CONSTRAINT idx_890111_primary PRIMARY KEY (id);


--
-- TOC entry 7056 (class 2606 OID 892443)
-- Name: insurance_payment_accounts idx_890117_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_accounts
    ADD CONSTRAINT idx_890117_primary PRIMARY KEY (id);


--
-- TOC entry 7059 (class 2606 OID 892444)
-- Name: insurance_payment_corrections idx_890120_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_corrections
    ADD CONSTRAINT idx_890120_primary PRIMARY KEY (id);


--
-- TOC entry 7062 (class 2606 OID 892445)
-- Name: insurance_payment_movements idx_890123_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_movements
    ADD CONSTRAINT idx_890123_primary PRIMARY KEY (id);


--
-- TOC entry 7064 (class 2606 OID 892446)
-- Name: insurance_payment_refunds idx_890129_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_refunds
    ADD CONSTRAINT idx_890129_primary PRIMARY KEY (id);


--
-- TOC entry 7067 (class 2606 OID 892447)
-- Name: insurance_payment_transfers idx_890132_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_transfers
    ADD CONSTRAINT idx_890132_primary PRIMARY KEY (id);


--
-- TOC entry 7070 (class 2606 OID 892448)
-- Name: insurance_plan idx_890135_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan
    ADD CONSTRAINT idx_890135_primary PRIMARY KEY (id);


--
-- TOC entry 7072 (class 2606 OID 892449)
-- Name: insurance_plan_adandd idx_890142_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_adandd
    ADD CONSTRAINT idx_890142_primary PRIMARY KEY (id);


--
-- TOC entry 7075 (class 2606 OID 892450)
-- Name: insurance_plan_age_and_gender_banded_rate idx_890145_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_and_gender_banded_rate
    ADD CONSTRAINT idx_890145_primary PRIMARY KEY (id);


--
-- TOC entry 7078 (class 2606 OID 892451)
-- Name: insurance_plan_age_banded_rate idx_890148_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_banded_rate
    ADD CONSTRAINT idx_890148_primary PRIMARY KEY (id);


--
-- TOC entry 7080 (class 2606 OID 892452)
-- Name: insurance_plan_age_reduction idx_890151_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_reduction
    ADD CONSTRAINT idx_890151_primary PRIMARY KEY (id);


--
-- TOC entry 7082 (class 2606 OID 892453)
-- Name: insurance_plan_age_reduction_detail idx_890154_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_reduction_detail
    ADD CONSTRAINT idx_890154_primary PRIMARY KEY (id);


--
-- TOC entry 7085 (class 2606 OID 892454)
-- Name: insurance_plan_benefit_amount idx_890157_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_benefit_amount
    ADD CONSTRAINT idx_890157_primary PRIMARY KEY (id);


--
-- TOC entry 7087 (class 2606 OID 892455)
-- Name: insurance_plan_children_rate idx_890160_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_children_rate
    ADD CONSTRAINT idx_890160_primary PRIMARY KEY (id);


--
-- TOC entry 7089 (class 2606 OID 892456)
-- Name: insurance_plan_complex_benefit_amount idx_890163_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_complex_benefit_amount
    ADD CONSTRAINT idx_890163_primary PRIMARY KEY (id);


--
-- TOC entry 7092 (class 2606 OID 892457)
-- Name: insurance_plan_complex_benefit_amount_item idx_890166_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_complex_benefit_amount_item
    ADD CONSTRAINT idx_890166_primary PRIMARY KEY (id);


--
-- TOC entry 7094 (class 2606 OID 892458)
-- Name: insurance_plan_composition_detailed_rate idx_890169_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_composition_detailed_rate
    ADD CONSTRAINT idx_890169_primary PRIMARY KEY (id);


--
-- TOC entry 7097 (class 2606 OID 892459)
-- Name: insurance_plan_composition_rate idx_890172_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_composition_rate
    ADD CONSTRAINT idx_890172_primary PRIMARY KEY (id);


--
-- TOC entry 7100 (class 2606 OID 892460)
-- Name: insurance_plan_employers idx_890175_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_employers
    ADD CONSTRAINT idx_890175_primary PRIMARY KEY (insurance_plan_id, employer_id);


--
-- TOC entry 7103 (class 2606 OID 892461)
-- Name: insurance_plan_fixed_limit idx_890178_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_fixed_limit
    ADD CONSTRAINT idx_890178_primary PRIMARY KEY (id, alert_limit);


--
-- TOC entry 7105 (class 2606 OID 892462)
-- Name: insurance_plan_guaranteed_increase idx_890183_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_guaranteed_increase
    ADD CONSTRAINT idx_890183_primary PRIMARY KEY (id);


--
-- TOC entry 7107 (class 2606 OID 892463)
-- Name: insurance_plan_guaranteed_issue idx_890186_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_guaranteed_issue
    ADD CONSTRAINT idx_890186_primary PRIMARY KEY (id);


--
-- TOC entry 7111 (class 2606 OID 892464)
-- Name: insurance_plan_has_enrollment idx_890189_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment
    ADD CONSTRAINT idx_890189_primary PRIMARY KEY (id);


--
-- TOC entry 7115 (class 2606 OID 892465)
-- Name: insurance_plan_has_enrollment_has_enrollment_group idx_890193_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment_has_enrollment_group
    ADD CONSTRAINT idx_890193_primary PRIMARY KEY (insurance_plan_has_enrollment_id, enrollment_group_id);


--
-- TOC entry 7118 (class 2606 OID 892466)
-- Name: insurance_plan_limits idx_890196_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_limits
    ADD CONSTRAINT idx_890196_primary PRIMARY KEY (id);


--
-- TOC entry 7120 (class 2606 OID 892467)
-- Name: insurance_plan_medical_connection idx_890199_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_medical_connection
    ADD CONSTRAINT idx_890199_primary PRIMARY KEY (id);


--
-- TOC entry 7122 (class 2606 OID 892468)
-- Name: insurance_plan_misc idx_890202_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_misc
    ADD CONSTRAINT idx_890202_primary PRIMARY KEY (id);


--
-- TOC entry 7125 (class 2606 OID 892469)
-- Name: insurance_plan_rates idx_890205_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_rates
    ADD CONSTRAINT idx_890205_primary PRIMARY KEY (id);


--
-- TOC entry 7128 (class 2606 OID 892470)
-- Name: insurance_plan_saving_account idx_890208_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_saving_account
    ADD CONSTRAINT idx_890208_primary PRIMARY KEY (id);


--
-- TOC entry 7132 (class 2606 OID 892471)
-- Name: insurance_plan_separate_age_banded_rate idx_890217_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_separate_age_banded_rate
    ADD CONSTRAINT idx_890217_primary PRIMARY KEY (id);


--
-- TOC entry 7134 (class 2606 OID 892472)
-- Name: insurance_plan_simple_benefit_amount idx_890220_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_simple_benefit_amount
    ADD CONSTRAINT idx_890220_primary PRIMARY KEY (id);


--
-- TOC entry 7137 (class 2606 OID 892473)
-- Name: insurance_plan_slider_limit idx_890223_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_slider_limit
    ADD CONSTRAINT idx_890223_primary PRIMARY KEY (id);


--
-- TOC entry 7140 (class 2606 OID 892474)
-- Name: insurance_plan_summary_fields idx_890226_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_summary_fields
    ADD CONSTRAINT idx_890226_primary PRIMARY KEY (id);


--
-- TOC entry 7142 (class 2606 OID 892475)
-- Name: insurance_plan_summary_fields_default idx_890232_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_summary_fields_default
    ADD CONSTRAINT idx_890232_primary PRIMARY KEY (id);


--
-- TOC entry 7144 (class 2606 OID 892476)
-- Name: insurance_plan_term_disability_benefit_amount idx_890238_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_term_disability_benefit_amount
    ADD CONSTRAINT idx_890238_primary PRIMARY KEY (id);


--
-- TOC entry 7149 (class 2606 OID 892477)
-- Name: insurance_subscriptions idx_890241_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_subscriptions
    ADD CONSTRAINT idx_890241_primary PRIMARY KEY (id);


--
-- TOC entry 7153 (class 2606 OID 892478)
-- Name: insured idx_890249_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured
    ADD CONSTRAINT idx_890249_primary PRIMARY KEY (id);


--
-- TOC entry 7156 (class 2606 OID 892479)
-- Name: insured_benefit_values idx_890253_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured_benefit_values
    ADD CONSTRAINT idx_890253_primary PRIMARY KEY (id);


--
-- TOC entry 7158 (class 2606 OID 892480)
-- Name: invitations idx_890256_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invitations
    ADD CONSTRAINT idx_890256_primary PRIMARY KEY (id);


--
-- TOC entry 7162 (class 2606 OID 892481)
-- Name: invoice_export_queue idx_890259_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_export_queue
    ADD CONSTRAINT idx_890259_primary PRIMARY KEY (id);


--
-- TOC entry 7166 (class 2606 OID 892482)
-- Name: invoice_import_export_log idx_890262_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_import_export_log
    ADD CONSTRAINT idx_890262_primary PRIMARY KEY (id);


--
-- TOC entry 7169 (class 2606 OID 892483)
-- Name: in_network_discounts idx_890265_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.in_network_discounts
    ADD CONSTRAINT idx_890265_primary PRIMARY KEY (id);


--
-- TOC entry 7174 (class 2606 OID 892484)
-- Name: jobtitle idx_890268_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle
    ADD CONSTRAINT idx_890268_primary PRIMARY KEY (id);


--
-- TOC entry 7178 (class 2606 OID 892485)
-- Name: jobtitle_group idx_890271_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle_group
    ADD CONSTRAINT idx_890271_primary PRIMARY KEY (id);


--
-- TOC entry 7180 (class 2606 OID 892486)
-- Name: late_fee_charges idx_890274_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.late_fee_charges
    ADD CONSTRAINT idx_890274_primary PRIMARY KEY (id);


--
-- TOC entry 7184 (class 2606 OID 892487)
-- Name: legal_entities_participating_enrollment idx_890277_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entities_participating_enrollment
    ADD CONSTRAINT idx_890277_primary PRIMARY KEY (legal_entity_id, enrollment_id);


--
-- TOC entry 7189 (class 2606 OID 892488)
-- Name: legal_entity idx_890280_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity
    ADD CONSTRAINT idx_890280_primary PRIMARY KEY (id);


--
-- TOC entry 7192 (class 2606 OID 892489)
-- Name: legal_entity_address idx_890290_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_address
    ADD CONSTRAINT idx_890290_primary PRIMARY KEY (id);


--
-- TOC entry 7195 (class 2606 OID 892490)
-- Name: legal_entity_address_attachment idx_890293_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_address_attachment
    ADD CONSTRAINT idx_890293_primary PRIMARY KEY (id);


--
-- TOC entry 7198 (class 2606 OID 892491)
-- Name: legal_entity_billing_group idx_890299_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group
    ADD CONSTRAINT idx_890299_primary PRIMARY KEY (id);


--
-- TOC entry 7201 (class 2606 OID 892492)
-- Name: legal_entity_billing_group_has_sys_organizations idx_890302_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group_has_sys_organizations
    ADD CONSTRAINT idx_890302_primary PRIMARY KEY (id);


--
-- TOC entry 7204 (class 2606 OID 892493)
-- Name: legal_entity_dba idx_890305_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_dba
    ADD CONSTRAINT idx_890305_primary PRIMARY KEY (id);


--
-- TOC entry 7209 (class 2606 OID 892494)
-- Name: legal_entity_insurance_benefit idx_890308_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit
    ADD CONSTRAINT idx_890308_primary PRIMARY KEY (id);


--
-- TOC entry 7212 (class 2606 OID 892495)
-- Name: legal_entity_insurance_benefit_has_division idx_890311_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit_has_division
    ADD CONSTRAINT idx_890311_primary PRIMARY KEY (legal_entity_insurance_benefit_id, division_id);


--
-- TOC entry 7215 (class 2606 OID 892496)
-- Name: legal_entity_npi idx_890314_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_npi
    ADD CONSTRAINT idx_890314_primary PRIMARY KEY (id);


--
-- TOC entry 7218 (class 2606 OID 892497)
-- Name: legal_entity_operational_structures idx_890317_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_operational_structures
    ADD CONSTRAINT idx_890317_primary PRIMARY KEY (id);


--
-- TOC entry 7221 (class 2606 OID 892498)
-- Name: legal_entity_owner idx_890320_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_owner
    ADD CONSTRAINT idx_890320_primary PRIMARY KEY (id);


--
-- TOC entry 7225 (class 2606 OID 892499)
-- Name: legal_entity_tin idx_890323_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_tin
    ADD CONSTRAINT idx_890323_primary PRIMARY KEY (id);


--
-- TOC entry 7229 (class 2606 OID 892500)
-- Name: lightbarcalls idx_890326_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.lightbarcalls
    ADD CONSTRAINT idx_890326_primary PRIMARY KEY (id);


--
-- TOC entry 7234 (class 2606 OID 892501)
-- Name: location_access_keys idx_890329_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.location_access_keys
    ADD CONSTRAINT idx_890329_primary PRIMARY KEY (id);


--
-- TOC entry 7237 (class 2606 OID 892502)
-- Name: medical_history idx_890335_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history
    ADD CONSTRAINT idx_890335_primary PRIMARY KEY (id);


--
-- TOC entry 7241 (class 2606 OID 892503)
-- Name: medical_history_medications idx_890430_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_medications
    ADD CONSTRAINT idx_890430_primary PRIMARY KEY (id);


--
-- TOC entry 7244 (class 2606 OID 892504)
-- Name: medical_history_patient_relatives_crowding idx_890435_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_patient_relatives_crowding
    ADD CONSTRAINT idx_890435_primary PRIMARY KEY (id);


--
-- TOC entry 7246 (class 2606 OID 892505)
-- Name: medications idx_890439_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medications
    ADD CONSTRAINT idx_890439_primary PRIMARY KEY (id);


--
-- TOC entry 7251 (class 2606 OID 892506)
-- Name: merchant_reports idx_890443_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.merchant_reports
    ADD CONSTRAINT idx_890443_primary PRIMARY KEY (id);


--
-- TOC entry 7255 (class 2606 OID 892507)
-- Name: migrated_answers idx_890449_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_answers
    ADD CONSTRAINT idx_890449_primary PRIMARY KEY (id);


--
-- TOC entry 7278 (class 2606 OID 892508)
-- Name: migrated_patients_comments idx_890485_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_patients_comments
    ADD CONSTRAINT idx_890485_primary PRIMARY KEY (id);


--
-- TOC entry 7282 (class 2606 OID 892509)
-- Name: migrated_patient_answers idx_890491_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_patient_answers
    ADD CONSTRAINT idx_890491_primary PRIMARY KEY (id);


--
-- TOC entry 7284 (class 2606 OID 892510)
-- Name: migrated_patient_documents idx_890494_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_patient_documents
    ADD CONSTRAINT idx_890494_primary PRIMARY KEY (id);


--
-- TOC entry 7288 (class 2606 OID 892511)
-- Name: migrated_patient_image_series idx_890497_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_patient_image_series
    ADD CONSTRAINT idx_890497_primary PRIMARY KEY (id);


--
-- TOC entry 7297 (class 2606 OID 892512)
-- Name: migrated_questionnaire idx_890506_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_questionnaire
    ADD CONSTRAINT idx_890506_primary PRIMARY KEY (id);


--
-- TOC entry 7303 (class 2606 OID 892513)
-- Name: migrated_questions idx_890509_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_questions
    ADD CONSTRAINT idx_890509_primary PRIMARY KEY (id);


--
-- TOC entry 7308 (class 2606 OID 892514)
-- Name: migrated_relations idx_890512_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_relations
    ADD CONSTRAINT idx_890512_primary PRIMARY KEY (relation_id);


--
-- TOC entry 7311 (class 2606 OID 892515)
-- Name: migrated_schedule_templates idx_890515_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_schedule_templates
    ADD CONSTRAINT idx_890515_primary PRIMARY KEY (id);


--
-- TOC entry 7321 (class 2606 OID 892516)
-- Name: migrated_workschedule idx_890530_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_workschedule
    ADD CONSTRAINT idx_890530_primary PRIMARY KEY (id);


--
-- TOC entry 7325 (class 2606 OID 892517)
-- Name: migration_fix_adjustments idx_890536_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migration_fix_adjustments
    ADD CONSTRAINT idx_890536_primary PRIMARY KEY (id);


--
-- TOC entry 7330 (class 2606 OID 892518)
-- Name: migration_map idx_890541_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migration_map
    ADD CONSTRAINT idx_890541_primary PRIMARY KEY (id);


--
-- TOC entry 7334 (class 2606 OID 892519)
-- Name: misc_fee_charges idx_890546_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.misc_fee_charges
    ADD CONSTRAINT idx_890546_primary PRIMARY KEY (id);


--
-- TOC entry 7337 (class 2606 OID 892520)
-- Name: misc_fee_charge_templates idx_890551_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.misc_fee_charge_templates
    ADD CONSTRAINT idx_890551_primary PRIMARY KEY (id);


--
-- TOC entry 7339 (class 2606 OID 892521)
-- Name: my_reports idx_890555_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports
    ADD CONSTRAINT idx_890555_primary PRIMARY KEY (id);


--
-- TOC entry 7341 (class 2606 OID 892522)
-- Name: my_reports_columns idx_890558_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports_columns
    ADD CONSTRAINT idx_890558_primary PRIMARY KEY (id);


--
-- TOC entry 7344 (class 2606 OID 892523)
-- Name: my_reports_filters idx_890561_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports_filters
    ADD CONSTRAINT idx_890561_primary PRIMARY KEY (id);


--
-- TOC entry 7349 (class 2606 OID 892524)
-- Name: network_fee_sheets idx_890567_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_fee_sheets
    ADD CONSTRAINT idx_890567_primary PRIMARY KEY (id);


--
-- TOC entry 7353 (class 2606 OID 892525)
-- Name: network_sheet_fee idx_890570_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_sheet_fee
    ADD CONSTRAINT idx_890570_primary PRIMARY KEY (id);


--
-- TOC entry 7355 (class 2606 OID 892526)
-- Name: notebooks idx_890573_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.notebooks
    ADD CONSTRAINT idx_890573_primary PRIMARY KEY (id);


--
-- TOC entry 7358 (class 2606 OID 892527)
-- Name: notebook_notes idx_890576_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.notebook_notes
    ADD CONSTRAINT idx_890576_primary PRIMARY KEY (id);


--
-- TOC entry 7360 (class 2606 OID 892528)
-- Name: notes idx_890582_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.notes
    ADD CONSTRAINT idx_890582_primary PRIMARY KEY (id);


--
-- TOC entry 7365 (class 2606 OID 892529)
-- Name: odt_chairs idx_890588_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chairs
    ADD CONSTRAINT idx_890588_primary PRIMARY KEY (id);


--
-- TOC entry 7369 (class 2606 OID 892530)
-- Name: odt_chair_allocations idx_890592_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chair_allocations
    ADD CONSTRAINT idx_890592_primary PRIMARY KEY (id);


--
-- TOC entry 7372 (class 2606 OID 892531)
-- Name: od_chair_break_hours idx_890596_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.od_chair_break_hours
    ADD CONSTRAINT idx_890596_primary PRIMARY KEY (od_chair_id, end_min, start_min);


--
-- TOC entry 7374 (class 2606 OID 892532)
-- Name: offices idx_890599_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.offices
    ADD CONSTRAINT idx_890599_primary PRIMARY KEY (id);


--
-- TOC entry 7377 (class 2606 OID 892533)
-- Name: office_days idx_890607_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_days
    ADD CONSTRAINT idx_890607_primary PRIMARY KEY (id);


--
-- TOC entry 7383 (class 2606 OID 892534)
-- Name: office_day_chairs idx_890614_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_chairs
    ADD CONSTRAINT idx_890614_primary PRIMARY KEY (id);


--
-- TOC entry 7386 (class 2606 OID 892535)
-- Name: office_day_templates idx_890617_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_templates
    ADD CONSTRAINT idx_890617_primary PRIMARY KEY (id);


--
-- TOC entry 7388 (class 2606 OID 892536)
-- Name: ortho_coverages idx_890624_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.ortho_coverages
    ADD CONSTRAINT idx_890624_primary PRIMARY KEY (id);


--
-- TOC entry 7390 (class 2606 OID 892537)
-- Name: ortho_insured idx_890631_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.ortho_insured
    ADD CONSTRAINT idx_890631_primary PRIMARY KEY (id);


--
-- TOC entry 7395 (class 2606 OID 892538)
-- Name: other_professional_relationships idx_890634_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.other_professional_relationships
    ADD CONSTRAINT idx_890634_primary PRIMARY KEY (id);


--
-- TOC entry 7401 (class 2606 OID 892539)
-- Name: patients idx_890637_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT idx_890637_primary PRIMARY KEY (id);


--
-- TOC entry 7405 (class 2606 OID 892540)
-- Name: patient_images idx_890644_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images
    ADD CONSTRAINT idx_890644_primary PRIMARY KEY (id);


--
-- TOC entry 7410 (class 2606 OID 892541)
-- Name: patient_imageseries idx_890650_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_imageseries
    ADD CONSTRAINT idx_890650_primary PRIMARY KEY (id);


--
-- TOC entry 7414 (class 2606 OID 892542)
-- Name: patient_images_layouts idx_890654_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images_layouts
    ADD CONSTRAINT idx_890654_primary PRIMARY KEY (id);


--
-- TOC entry 7416 (class 2606 OID 892543)
-- Name: patient_images_types idx_890660_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images_types
    ADD CONSTRAINT idx_890660_primary PRIMARY KEY (id);


--
-- TOC entry 7421 (class 2606 OID 892544)
-- Name: patient_insurance_plans idx_890666_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_insurance_plans
    ADD CONSTRAINT idx_890666_primary PRIMARY KEY (id);


--
-- TOC entry 7424 (class 2606 OID 892545)
-- Name: patient_ledger_history idx_890672_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history
    ADD CONSTRAINT idx_890672_primary PRIMARY KEY (id);


--
-- TOC entry 7428 (class 2606 OID 892546)
-- Name: patient_ledger_history_overdue_receivables idx_890678_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history_overdue_receivables
    ADD CONSTRAINT idx_890678_primary PRIMARY KEY (receivable_id, patient_ledger_history_id);


--
-- TOC entry 7431 (class 2606 OID 892547)
-- Name: patient_locations idx_890681_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_locations
    ADD CONSTRAINT idx_890681_primary PRIMARY KEY (patient_id, location_id);


--
-- TOC entry 7435 (class 2606 OID 892548)
-- Name: patient_person_referrals idx_890684_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_person_referrals
    ADD CONSTRAINT idx_890684_primary PRIMARY KEY (id, patient_id, person_id);


--
-- TOC entry 7438 (class 2606 OID 892549)
-- Name: patient_recently_opened_items idx_890687_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_recently_opened_items
    ADD CONSTRAINT idx_890687_primary PRIMARY KEY (id);


--
-- TOC entry 7442 (class 2606 OID 892550)
-- Name: patient_template_referrals idx_890690_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_template_referrals
    ADD CONSTRAINT idx_890690_primary PRIMARY KEY (id);


--
-- TOC entry 7451 (class 2606 OID 892551)
-- Name: payments idx_890696_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT idx_890696_primary PRIMARY KEY (id);


--
-- TOC entry 7456 (class 2606 OID 892552)
-- Name: payment_accounts idx_890703_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_accounts
    ADD CONSTRAINT idx_890703_primary PRIMARY KEY (id);


--
-- TOC entry 7459 (class 2606 OID 892553)
-- Name: payment_correction_details idx_890707_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_correction_details
    ADD CONSTRAINT idx_890707_primary PRIMARY KEY (id);


--
-- TOC entry 7462 (class 2606 OID 892554)
-- Name: payment_options idx_890713_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_options
    ADD CONSTRAINT idx_890713_primary PRIMARY KEY (id);


--
-- TOC entry 7466 (class 2606 OID 892555)
-- Name: payment_plans idx_890716_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT idx_890716_primary PRIMARY KEY (id);


--
-- TOC entry 7470 (class 2606 OID 892556)
-- Name: payment_plan_rollbacks idx_890724_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plan_rollbacks
    ADD CONSTRAINT idx_890724_primary PRIMARY KEY (id);


--
-- TOC entry 7474 (class 2606 OID 892557)
-- Name: payment_types idx_890727_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types
    ADD CONSTRAINT idx_890727_primary PRIMARY KEY (id);


--
-- TOC entry 7477 (class 2606 OID 892558)
-- Name: payment_types_locations idx_890731_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types_locations
    ADD CONSTRAINT idx_890731_primary PRIMARY KEY (payment_type_id, organization_id);


--
-- TOC entry 7480 (class 2606 OID 892559)
-- Name: permissions_permission idx_890734_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_permission
    ADD CONSTRAINT idx_890734_primary PRIMARY KEY (id);


--
-- TOC entry 7482 (class 2606 OID 892560)
-- Name: permissions_user_role idx_890737_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role
    ADD CONSTRAINT idx_890737_primary PRIMARY KEY (id);


--
-- TOC entry 7485 (class 2606 OID 892561)
-- Name: permissions_user_role_departament idx_890740_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_departament
    ADD CONSTRAINT idx_890740_primary PRIMARY KEY (permissions_user_role_id, department_id);


--
-- TOC entry 7488 (class 2606 OID 892562)
-- Name: permissions_user_role_job_title idx_890743_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_job_title
    ADD CONSTRAINT idx_890743_primary PRIMARY KEY (permissions_user_role_id, job_title_id);


--
-- TOC entry 7491 (class 2606 OID 892563)
-- Name: permissions_user_role_permission idx_890746_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_permission
    ADD CONSTRAINT idx_890746_primary PRIMARY KEY (id, permissions_permission_id);


--
-- TOC entry 7496 (class 2606 OID 892564)
-- Name: permission_user_role_location idx_890749_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permission_user_role_location
    ADD CONSTRAINT idx_890749_primary PRIMARY KEY (user_id, location_id);


--
-- TOC entry 7504 (class 2606 OID 892565)
-- Name: persons idx_890752_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT idx_890752_primary PRIMARY KEY (id);


--
-- TOC entry 7508 (class 2606 OID 892566)
-- Name: person_image_files idx_890759_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_image_files
    ADD CONSTRAINT idx_890759_primary PRIMARY KEY (id);


--
-- TOC entry 7512 (class 2606 OID 892567)
-- Name: person_payment_accounts idx_890765_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_payment_accounts
    ADD CONSTRAINT idx_890765_primary PRIMARY KEY (id);


--
-- TOC entry 7516 (class 2606 OID 892568)
-- Name: previous_enrollment_form idx_890776_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form
    ADD CONSTRAINT idx_890776_primary PRIMARY KEY (id);


--
-- TOC entry 7519 (class 2606 OID 892569)
-- Name: previous_enrollment_form_benefits idx_890782_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form_benefits
    ADD CONSTRAINT idx_890782_primary PRIMARY KEY (id);


--
-- TOC entry 7523 (class 2606 OID 892570)
-- Name: procedures idx_890789_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedures
    ADD CONSTRAINT idx_890789_primary PRIMARY KEY (id);


--
-- TOC entry 7525 (class 2606 OID 892571)
-- Name: procedure_additional_resource_requirements idx_890795_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_additional_resource_requirements
    ADD CONSTRAINT idx_890795_primary PRIMARY KEY (procedure_id, resource_type_id, qty);


--
-- TOC entry 7528 (class 2606 OID 892572)
-- Name: procedure_steps idx_890798_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_steps
    ADD CONSTRAINT idx_890798_primary PRIMARY KEY (id);


--
-- TOC entry 7531 (class 2606 OID 892573)
-- Name: procedure_step_additional_resource_requirements idx_890802_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_additional_resource_requirements
    ADD CONSTRAINT idx_890802_primary PRIMARY KEY (step_id, resource_type_id, qty);


--
-- TOC entry 7534 (class 2606 OID 892574)
-- Name: procedure_step_durations idx_890805_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_durations
    ADD CONSTRAINT idx_890805_primary PRIMARY KEY (id);


--
-- TOC entry 7539 (class 2606 OID 892575)
-- Name: professional_relationships idx_890809_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.professional_relationships
    ADD CONSTRAINT idx_890809_primary PRIMARY KEY (id);


--
-- TOC entry 7543 (class 2606 OID 892576)
-- Name: provider_license idx_890813_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_license
    ADD CONSTRAINT idx_890813_primary PRIMARY KEY (id);


--
-- TOC entry 7546 (class 2606 OID 892577)
-- Name: provider_network_insurance_companies idx_890816_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_network_insurance_companies
    ADD CONSTRAINT idx_890816_primary PRIMARY KEY (insurance_company_id, network_fee_sheet_id);


--
-- TOC entry 7548 (class 2606 OID 892578)
-- Name: provider_specialty idx_890819_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_specialty
    ADD CONSTRAINT idx_890819_primary PRIMARY KEY (id);


--
-- TOC entry 7550 (class 2606 OID 892579)
-- Name: question idx_890822_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.question
    ADD CONSTRAINT idx_890822_primary PRIMARY KEY (id);


--
-- TOC entry 7554 (class 2606 OID 892580)
-- Name: questionnaire idx_890825_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.questionnaire
    ADD CONSTRAINT idx_890825_primary PRIMARY KEY (id);


--
-- TOC entry 7558 (class 2606 OID 892581)
-- Name: questionnaire_answers idx_890828_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.questionnaire_answers
    ADD CONSTRAINT idx_890828_primary PRIMARY KEY (id);


--
-- TOC entry 7562 (class 2606 OID 892582)
-- Name: reachify_users idx_890834_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.reachify_users
    ADD CONSTRAINT idx_890834_primary PRIMARY KEY (id);


--
-- TOC entry 7569 (class 2606 OID 892583)
-- Name: receivables idx_890840_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT idx_890840_primary PRIMARY KEY (id);


--
-- TOC entry 7574 (class 2606 OID 892584)
-- Name: recently_opened_items idx_890843_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.recently_opened_items
    ADD CONSTRAINT idx_890843_primary PRIMARY KEY (id);


--
-- TOC entry 7576 (class 2606 OID 892585)
-- Name: referral_list_values idx_890846_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_list_values
    ADD CONSTRAINT idx_890846_primary PRIMARY KEY (referral_template_id, pos);


--
-- TOC entry 7578 (class 2606 OID 892586)
-- Name: referral_templates idx_890852_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_templates
    ADD CONSTRAINT idx_890852_primary PRIMARY KEY (id);


--
-- TOC entry 7582 (class 2606 OID 892587)
-- Name: refunds idx_890859_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.refunds
    ADD CONSTRAINT idx_890859_primary PRIMARY KEY (id);


--
-- TOC entry 7584 (class 2606 OID 892588)
-- Name: refund_details idx_890862_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.refund_details
    ADD CONSTRAINT idx_890862_primary PRIMARY KEY (id);


--
-- TOC entry 7588 (class 2606 OID 892589)
-- Name: relationships idx_890868_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.relationships
    ADD CONSTRAINT idx_890868_primary PRIMARY KEY (id);


--
-- TOC entry 7594 (class 2606 OID 892590)
-- Name: remote_authentication_tokens idx_890874_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.remote_authentication_tokens
    ADD CONSTRAINT idx_890874_primary PRIMARY KEY (id);


--
-- TOC entry 7597 (class 2606 OID 892591)
-- Name: resources idx_890877_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resources
    ADD CONSTRAINT idx_890877_primary PRIMARY KEY (id);


--
-- TOC entry 7603 (class 2606 OID 892592)
-- Name: resource_types idx_890881_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resource_types
    ADD CONSTRAINT idx_890881_primary PRIMARY KEY (id);


--
-- TOC entry 7605 (class 2606 OID 892593)
-- Name: retail_fees idx_890890_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_fees
    ADD CONSTRAINT idx_890890_primary PRIMARY KEY (id);


--
-- TOC entry 7609 (class 2606 OID 892594)
-- Name: retail_items idx_890893_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_items
    ADD CONSTRAINT idx_890893_primary PRIMARY KEY (id);


--
-- TOC entry 7611 (class 2606 OID 892595)
-- Name: reversed_payments idx_890897_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.reversed_payments
    ADD CONSTRAINT idx_890897_primary PRIMARY KEY (payment_id);


--
-- TOC entry 7613 (class 2606 OID 892596)
-- Name: scheduled_task idx_890903_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduled_task
    ADD CONSTRAINT idx_890903_primary PRIMARY KEY (task_type, parent_company_id);


--
-- TOC entry 7617 (class 2606 OID 892597)
-- Name: schedule_conflict_permission_bypass idx_890907_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_conflict_permission_bypass
    ADD CONSTRAINT idx_890907_primary PRIMARY KEY (id);


--
-- TOC entry 7621 (class 2606 OID 892598)
-- Name: schedule_notes idx_890910_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_notes
    ADD CONSTRAINT idx_890910_primary PRIMARY KEY (id);


--
-- TOC entry 7624 (class 2606 OID 892599)
-- Name: scheduling_preferences idx_890915_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferences
    ADD CONSTRAINT idx_890915_primary PRIMARY KEY (id);


--
-- TOC entry 7628 (class 2606 OID 892600)
-- Name: scheduling_preferred_employees idx_890921_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferred_employees
    ADD CONSTRAINT idx_890921_primary PRIMARY KEY (preference_id, employee_id);


--
-- TOC entry 7630 (class 2606 OID 892601)
-- Name: schema_version idx_890924_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schema_version
    ADD CONSTRAINT idx_890924_primary PRIMARY KEY (version);


--
-- TOC entry 7638 (class 2606 OID 892602)
-- Name: selected_appointment_templates idx_890931_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates
    ADD CONSTRAINT idx_890931_primary PRIMARY KEY (id);


--
-- TOC entry 7643 (class 2606 OID 892603)
-- Name: selected_appointment_templates_office_day_calendar_view idx_890934_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates_office_day_calendar_view
    ADD CONSTRAINT idx_890934_primary PRIMARY KEY (user_id, location_id, appointment_template_id);


--
-- TOC entry 7648 (class 2606 OID 892604)
-- Name: simultaneous_appointment_values idx_890943_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.simultaneous_appointment_values
    ADD CONSTRAINT idx_890943_primary PRIMARY KEY (id);


--
-- TOC entry 7652 (class 2606 OID 892605)
-- Name: sms idx_890946_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sms
    ADD CONSTRAINT idx_890946_primary PRIMARY KEY (id);


--
-- TOC entry 7654 (class 2606 OID 892606)
-- Name: sms_part idx_890952_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sms_part
    ADD CONSTRAINT idx_890952_primary PRIMARY KEY (id);


--
-- TOC entry 7660 (class 2606 OID 892607)
-- Name: statements idx_890958_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.statements
    ADD CONSTRAINT idx_890958_primary PRIMARY KEY (id);


--
-- TOC entry 7662 (class 2606 OID 892608)
-- Name: sys_i18n idx_890967_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_i18n
    ADD CONSTRAINT idx_890967_primary PRIMARY KEY (locale, key);


--
-- TOC entry 7664 (class 2606 OID 892609)
-- Name: sys_identifiers idx_890973_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_identifiers
    ADD CONSTRAINT idx_890973_primary PRIMARY KEY (space);


--
-- TOC entry 7669 (class 2606 OID 892610)
-- Name: sys_organizations idx_890976_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organizations
    ADD CONSTRAINT idx_890976_primary PRIMARY KEY (id);


--
-- TOC entry 7672 (class 2606 OID 892611)
-- Name: sys_organization_address idx_890983_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address
    ADD CONSTRAINT idx_890983_primary PRIMARY KEY (id);


--
-- TOC entry 7675 (class 2606 OID 892612)
-- Name: sys_organization_address_has_sys_organizations idx_890986_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address_has_sys_organizations
    ADD CONSTRAINT idx_890986_primary PRIMARY KEY (id);


--
-- TOC entry 7678 (class 2606 OID 892613)
-- Name: sys_organization_attachment idx_890989_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_attachment
    ADD CONSTRAINT idx_890989_primary PRIMARY KEY (id);


--
-- TOC entry 7682 (class 2606 OID 892614)
-- Name: sys_organization_contact_method idx_890995_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_contact_method
    ADD CONSTRAINT idx_890995_primary PRIMARY KEY (id);


--
-- TOC entry 7685 (class 2606 OID 892615)
-- Name: sys_organization_department_data idx_890999_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_department_data
    ADD CONSTRAINT idx_890999_primary PRIMARY KEY (id);


--
-- TOC entry 7689 (class 2606 OID 892616)
-- Name: sys_organization_division_data idx_891002_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_data
    ADD CONSTRAINT idx_891002_primary PRIMARY KEY (id);


--
-- TOC entry 7692 (class 2606 OID 892617)
-- Name: sys_organization_division_has_department idx_891005_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_has_department
    ADD CONSTRAINT idx_891005_primary PRIMARY KEY (id);


--
-- TOC entry 7696 (class 2606 OID 892618)
-- Name: sys_organization_location_group idx_891008_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group
    ADD CONSTRAINT idx_891008_primary PRIMARY KEY (id);


--
-- TOC entry 7699 (class 2606 OID 892619)
-- Name: sys_organization_location_group_has_sys_organizations idx_891011_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group_has_sys_organizations
    ADD CONSTRAINT idx_891011_primary PRIMARY KEY (sys_organization_location_group_id, sys_organizations_id);


--
-- TOC entry 7702 (class 2606 OID 892620)
-- Name: sys_organization_settings idx_891014_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_settings
    ADD CONSTRAINT idx_891014_primary PRIMARY KEY (id);


--
-- TOC entry 7705 (class 2606 OID 892621)
-- Name: sys_patient_portal_users idx_891020_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_patient_portal_users
    ADD CONSTRAINT idx_891020_primary PRIMARY KEY (id);


--
-- TOC entry 7709 (class 2606 OID 892622)
-- Name: sys_portal_users idx_891032_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_portal_users
    ADD CONSTRAINT idx_891032_primary PRIMARY KEY (id);


--
-- TOC entry 7713 (class 2606 OID 892623)
-- Name: sys_users idx_891042_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_users
    ADD CONSTRAINT idx_891042_primary PRIMARY KEY (id);


--
-- TOC entry 7720 (class 2606 OID 892624)
-- Name: tasks idx_891058_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT idx_891058_primary PRIMARY KEY (id);


--
-- TOC entry 7726 (class 2606 OID 892625)
-- Name: task_baskets idx_891065_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_baskets
    ADD CONSTRAINT idx_891065_primary PRIMARY KEY (id);


--
-- TOC entry 7728 (class 2606 OID 892626)
-- Name: task_basket_subscribers idx_891071_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_basket_subscribers
    ADD CONSTRAINT idx_891071_primary PRIMARY KEY (task_basket_id, user_id);


--
-- TOC entry 7732 (class 2606 OID 892627)
-- Name: task_contacts idx_891074_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_contacts
    ADD CONSTRAINT idx_891074_primary PRIMARY KEY (task_id, contact_method_id);


--
-- TOC entry 7735 (class 2606 OID 892628)
-- Name: task_events idx_891077_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_events
    ADD CONSTRAINT idx_891077_primary PRIMARY KEY (id);


--
-- TOC entry 7741 (class 2606 OID 892629)
-- Name: temporary_images idx_891083_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temporary_images
    ADD CONSTRAINT idx_891083_primary PRIMARY KEY (id);


--
-- TOC entry 7746 (class 2606 OID 892630)
-- Name: temp_accounts idx_891089_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT idx_891089_primary PRIMARY KEY (id);


--
-- TOC entry 7750 (class 2606 OID 892631)
-- Name: third_party_account idx_891092_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.third_party_account
    ADD CONSTRAINT idx_891092_primary PRIMARY KEY (id);


--
-- TOC entry 7752 (class 2606 OID 892632)
-- Name: toothchart_edit_log idx_891098_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_edit_log
    ADD CONSTRAINT idx_891098_primary PRIMARY KEY (id);


--
-- TOC entry 7755 (class 2606 OID 892633)
-- Name: toothchart_note idx_891101_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_note
    ADD CONSTRAINT idx_891101_primary PRIMARY KEY (id);


--
-- TOC entry 7759 (class 2606 OID 892634)
-- Name: toothchart_snapshot idx_891104_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_snapshot
    ADD CONSTRAINT idx_891104_primary PRIMARY KEY (id);


--
-- TOC entry 7761 (class 2606 OID 892635)
-- Name: tooth_marking idx_891110_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tooth_marking
    ADD CONSTRAINT idx_891110_primary PRIMARY KEY (id);


--
-- TOC entry 7765 (class 2606 OID 892636)
-- Name: transactions idx_891113_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transactions
    ADD CONSTRAINT idx_891113_primary PRIMARY KEY (id);


--
-- TOC entry 7770 (class 2606 OID 892637)
-- Name: transfer_charges idx_891118_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transfer_charges
    ADD CONSTRAINT idx_891118_primary PRIMARY KEY (id);


--
-- TOC entry 7774 (class 2606 OID 892638)
-- Name: txs idx_891121_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT idx_891121_primary PRIMARY KEY (id);


--
-- TOC entry 7779 (class 2606 OID 892639)
-- Name: txs_state_changes idx_891131_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs_state_changes
    ADD CONSTRAINT idx_891131_primary PRIMARY KEY (id);


--
-- TOC entry 7783 (class 2606 OID 892640)
-- Name: tx_cards idx_891137_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_cards
    ADD CONSTRAINT idx_891137_primary PRIMARY KEY (id);


--
-- TOC entry 7787 (class 2606 OID 892641)
-- Name: tx_card_field_definitions idx_891141_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_definitions
    ADD CONSTRAINT idx_891141_primary PRIMARY KEY (id);


--
-- TOC entry 7791 (class 2606 OID 892642)
-- Name: tx_card_field_options idx_891149_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_options
    ADD CONSTRAINT idx_891149_primary PRIMARY KEY (id);


--
-- TOC entry 7793 (class 2606 OID 892643)
-- Name: tx_card_field_types idx_891154_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_types
    ADD CONSTRAINT idx_891154_primary PRIMARY KEY (id);


--
-- TOC entry 7795 (class 2606 OID 892644)
-- Name: tx_card_templates idx_891162_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_templates
    ADD CONSTRAINT idx_891162_primary PRIMARY KEY (id);


--
-- TOC entry 7799 (class 2606 OID 892645)
-- Name: tx_categories idx_891165_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_categories
    ADD CONSTRAINT idx_891165_primary PRIMARY KEY (id);


--
-- TOC entry 7802 (class 2606 OID 892646)
-- Name: tx_category_coverages idx_891168_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_coverages
    ADD CONSTRAINT idx_891168_primary PRIMARY KEY (id);


--
-- TOC entry 7806 (class 2606 OID 892647)
-- Name: tx_category_insured idx_891171_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_insured
    ADD CONSTRAINT idx_891171_primary PRIMARY KEY (id);


--
-- TOC entry 7809 (class 2606 OID 892648)
-- Name: tx_contracts idx_891174_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_contracts
    ADD CONSTRAINT idx_891174_primary PRIMARY KEY (id);


--
-- TOC entry 7813 (class 2606 OID 892649)
-- Name: tx_fee_charges idx_891180_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_fee_charges
    ADD CONSTRAINT idx_891180_primary PRIMARY KEY (id);


--
-- TOC entry 7820 (class 2606 OID 892650)
-- Name: tx_payers idx_891183_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT idx_891183_primary PRIMARY KEY (id);


--
-- TOC entry 7824 (class 2606 OID 892651)
-- Name: tx_plans idx_891187_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plans
    ADD CONSTRAINT idx_891187_primary PRIMARY KEY (id);


--
-- TOC entry 7829 (class 2606 OID 892652)
-- Name: tx_plan_appointments idx_891192_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT idx_891192_primary PRIMARY KEY (id);


--
-- TOC entry 7835 (class 2606 OID 892653)
-- Name: tx_plan_appointment_procedures idx_891197_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointment_procedures
    ADD CONSTRAINT idx_891197_primary PRIMARY KEY (appointment_id, procedure_id, pos);


--
-- TOC entry 7839 (class 2606 OID 892654)
-- Name: tx_plan_groups idx_891204_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_groups
    ADD CONSTRAINT idx_891204_primary PRIMARY KEY (id);


--
-- TOC entry 7842 (class 2606 OID 892655)
-- Name: tx_plan_notes idx_891208_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_notes
    ADD CONSTRAINT idx_891208_primary PRIMARY KEY (id);


--
-- TOC entry 7845 (class 2606 OID 892656)
-- Name: tx_plan_note_groups idx_891215_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_note_groups
    ADD CONSTRAINT idx_891215_primary PRIMARY KEY (id);


--
-- TOC entry 7850 (class 2606 OID 892657)
-- Name: tx_plan_templates idx_891222_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT idx_891222_primary PRIMARY KEY (id);


--
-- TOC entry 7855 (class 2606 OID 892658)
-- Name: tx_plan_template_appointments idx_891228_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT idx_891228_primary PRIMARY KEY (id);


--
-- TOC entry 7860 (class 2606 OID 892659)
-- Name: tx_plan_template_appointment_procedures idx_891233_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointment_procedures
    ADD CONSTRAINT idx_891233_primary PRIMARY KEY (appointment_id, procedure_id, pos);


--
-- TOC entry 7863 (class 2606 OID 892660)
-- Name: tx_statuses idx_891240_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_statuses
    ADD CONSTRAINT idx_891240_primary PRIMARY KEY (id);


--
-- TOC entry 7867 (class 2606 OID 892661)
-- Name: unapplied_payments idx_891244_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.unapplied_payments
    ADD CONSTRAINT idx_891244_primary PRIMARY KEY (id);


--
-- TOC entry 7872 (class 2606 OID 892662)
-- Name: week_templates idx_891253_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT idx_891253_primary PRIMARY KEY (id);


--
-- TOC entry 7883 (class 2606 OID 892663)
-- Name: work_hours idx_891256_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hours
    ADD CONSTRAINT idx_891256_primary PRIMARY KEY (id);


--
-- TOC entry 7886 (class 2606 OID 892664)
-- Name: work_hour_comments idx_891260_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hour_comments
    ADD CONSTRAINT idx_891260_primary PRIMARY KEY (id);


--
-- TOC entry 7890 (class 2606 OID 892665)
-- Name: work_schedule_days idx_891266_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_schedule_days
    ADD CONSTRAINT idx_891266_primary PRIMARY KEY (id);


--
-- TOC entry 7894 (class 2606 OID 892666)
-- Name: writeoff_adjustments idx_891269_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.writeoff_adjustments
    ADD CONSTRAINT idx_891269_primary PRIMARY KEY (id);


--
-- TOC entry 6503 (class 1259 OID 891279)
-- Name: idx_889332_organization_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889332_organization_node_id ON vaxiom.activiti_process_settings USING btree (organization_node_id);


--
-- TOC entry 6510 (class 1259 OID 891304)
-- Name: idx_889351_act_fk_bytearr_depl; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889351_act_fk_bytearr_depl ON vaxiom.act_ge_bytearray USING btree (deployment_id_);


--
-- TOC entry 6515 (class 1259 OID 891276)
-- Name: idx_889360_act_idx_hi_act_inst_end; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889360_act_idx_hi_act_inst_end ON vaxiom.act_hi_actinst USING btree (end_time_);


--
-- TOC entry 6516 (class 1259 OID 891272)
-- Name: idx_889360_act_idx_hi_act_inst_exec; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889360_act_idx_hi_act_inst_exec ON vaxiom.act_hi_actinst USING btree (execution_id_, act_id_);


--
-- TOC entry 6517 (class 1259 OID 891277)
-- Name: idx_889360_act_idx_hi_act_inst_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889360_act_idx_hi_act_inst_procinst ON vaxiom.act_hi_actinst USING btree (proc_inst_id_, act_id_);


--
-- TOC entry 6518 (class 1259 OID 891273)
-- Name: idx_889360_act_idx_hi_act_inst_start; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889360_act_idx_hi_act_inst_start ON vaxiom.act_hi_actinst USING btree (start_time_);


--
-- TOC entry 6525 (class 1259 OID 891290)
-- Name: idx_889379_act_idx_hi_detail_act_inst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_act_inst ON vaxiom.act_hi_detail USING btree (act_inst_id_);


--
-- TOC entry 6526 (class 1259 OID 891313)
-- Name: idx_889379_act_idx_hi_detail_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_name ON vaxiom.act_hi_detail USING btree (name_);


--
-- TOC entry 6527 (class 1259 OID 891291)
-- Name: idx_889379_act_idx_hi_detail_proc_inst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_proc_inst ON vaxiom.act_hi_detail USING btree (proc_inst_id_);


--
-- TOC entry 6528 (class 1259 OID 891288)
-- Name: idx_889379_act_idx_hi_detail_task_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_task_id ON vaxiom.act_hi_detail USING btree (task_id_);


--
-- TOC entry 6529 (class 1259 OID 891285)
-- Name: idx_889379_act_idx_hi_detail_time; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_time ON vaxiom.act_hi_detail USING btree (time_);


--
-- TOC entry 6532 (class 1259 OID 891287)
-- Name: idx_889385_act_idx_hi_ident_lnk_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889385_act_idx_hi_ident_lnk_procinst ON vaxiom.act_hi_identitylink USING btree (proc_inst_id_);


--
-- TOC entry 6533 (class 1259 OID 891283)
-- Name: idx_889385_act_idx_hi_ident_lnk_task; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889385_act_idx_hi_ident_lnk_task ON vaxiom.act_hi_identitylink USING btree (task_id_);


--
-- TOC entry 6534 (class 1259 OID 891299)
-- Name: idx_889385_act_idx_hi_ident_lnk_user; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889385_act_idx_hi_ident_lnk_user ON vaxiom.act_hi_identitylink USING btree (user_id_);


--
-- TOC entry 6537 (class 1259 OID 891300)
-- Name: idx_889391_act_idx_hi_pro_i_buskey; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889391_act_idx_hi_pro_i_buskey ON vaxiom.act_hi_procinst USING btree (business_key_);


--
-- TOC entry 6538 (class 1259 OID 891294)
-- Name: idx_889391_act_idx_hi_pro_inst_end; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889391_act_idx_hi_pro_inst_end ON vaxiom.act_hi_procinst USING btree (end_time_);


--
-- TOC entry 6541 (class 1259 OID 891301)
-- Name: idx_889391_proc_inst_id_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889391_proc_inst_id_ ON vaxiom.act_hi_procinst USING btree (proc_inst_id_);


--
-- TOC entry 6542 (class 1259 OID 891297)
-- Name: idx_889398_act_idx_hi_task_inst_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889398_act_idx_hi_task_inst_procinst ON vaxiom.act_hi_taskinst USING btree (proc_inst_id_);


--
-- TOC entry 6545 (class 1259 OID 891296)
-- Name: idx_889405_act_idx_hi_procvar_name_type; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889405_act_idx_hi_procvar_name_type ON vaxiom.act_hi_varinst USING btree (name_, var_type_);


--
-- TOC entry 6546 (class 1259 OID 891298)
-- Name: idx_889405_act_idx_hi_procvar_proc_inst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889405_act_idx_hi_procvar_proc_inst ON vaxiom.act_hi_varinst USING btree (proc_inst_id_);


--
-- TOC entry 6547 (class 1259 OID 891292)
-- Name: idx_889405_act_idx_hi_procvar_task_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889405_act_idx_hi_procvar_task_id ON vaxiom.act_hi_varinst USING btree (task_id_);


--
-- TOC entry 6554 (class 1259 OID 891310)
-- Name: idx_889423_act_fk_memb_group; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889423_act_fk_memb_group ON vaxiom.act_id_membership USING btree (group_id_);


--
-- TOC entry 6561 (class 1259 OID 891306)
-- Name: idx_889439_act_fk_model_deployment; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889439_act_fk_model_deployment ON vaxiom.act_re_model USING btree (deployment_id_);


--
-- TOC entry 6562 (class 1259 OID 891307)
-- Name: idx_889439_act_fk_model_source; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889439_act_fk_model_source ON vaxiom.act_re_model USING btree (editor_source_value_id_);


--
-- TOC entry 6563 (class 1259 OID 891309)
-- Name: idx_889439_act_fk_model_source_extra; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889439_act_fk_model_source_extra ON vaxiom.act_re_model USING btree (editor_source_extra_value_id_);


--
-- TOC entry 6566 (class 1259 OID 891322)
-- Name: idx_889446_act_uniq_procdef; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889446_act_uniq_procdef ON vaxiom.act_re_procdef USING btree (key_, version_, tenant_id_);


--
-- TOC entry 6569 (class 1259 OID 891316)
-- Name: idx_889453_act_fk_event_exec; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889453_act_fk_event_exec ON vaxiom.act_ru_event_subscr USING btree (execution_id_);


--
-- TOC entry 6570 (class 1259 OID 891321)
-- Name: idx_889453_act_idx_event_subscr_config_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889453_act_idx_event_subscr_config_ ON vaxiom.act_ru_event_subscr USING btree (configuration_);


--
-- TOC entry 6573 (class 1259 OID 891323)
-- Name: idx_889461_act_fk_exe_parent; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_fk_exe_parent ON vaxiom.act_ru_execution USING btree (parent_id_);


--
-- TOC entry 6574 (class 1259 OID 891319)
-- Name: idx_889461_act_fk_exe_procdef; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_fk_exe_procdef ON vaxiom.act_ru_execution USING btree (proc_def_id_);


--
-- TOC entry 6575 (class 1259 OID 891317)
-- Name: idx_889461_act_fk_exe_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_fk_exe_procinst ON vaxiom.act_ru_execution USING btree (proc_inst_id_);


--
-- TOC entry 6576 (class 1259 OID 891318)
-- Name: idx_889461_act_fk_exe_super; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_fk_exe_super ON vaxiom.act_ru_execution USING btree (super_exec_);


--
-- TOC entry 6577 (class 1259 OID 891320)
-- Name: idx_889461_act_idx_exec_buskey; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_idx_exec_buskey ON vaxiom.act_ru_execution USING btree (business_key_);


--
-- TOC entry 6580 (class 1259 OID 891332)
-- Name: idx_889468_act_fk_idl_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_fk_idl_procinst ON vaxiom.act_ru_identitylink USING btree (proc_inst_id_);


--
-- TOC entry 6581 (class 1259 OID 891326)
-- Name: idx_889468_act_fk_tskass_task; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_fk_tskass_task ON vaxiom.act_ru_identitylink USING btree (task_id_);


--
-- TOC entry 6582 (class 1259 OID 891327)
-- Name: idx_889468_act_idx_athrz_procedef; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_idx_athrz_procedef ON vaxiom.act_ru_identitylink USING btree (proc_def_id_);


--
-- TOC entry 6583 (class 1259 OID 891331)
-- Name: idx_889468_act_idx_ident_lnk_group; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_idx_ident_lnk_group ON vaxiom.act_ru_identitylink USING btree (group_id_);


--
-- TOC entry 6584 (class 1259 OID 891355)
-- Name: idx_889468_act_idx_ident_lnk_user; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_idx_ident_lnk_user ON vaxiom.act_ru_identitylink USING btree (user_id_);


--
-- TOC entry 6587 (class 1259 OID 891333)
-- Name: idx_889474_act_fk_job_exception; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889474_act_fk_job_exception ON vaxiom.act_ru_job USING btree (exception_stack_id_);


--
-- TOC entry 6590 (class 1259 OID 891329)
-- Name: idx_889481_act_fk_task_exe; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889481_act_fk_task_exe ON vaxiom.act_ru_task USING btree (execution_id_);


--
-- TOC entry 6591 (class 1259 OID 891330)
-- Name: idx_889481_act_fk_task_procdef; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889481_act_fk_task_procdef ON vaxiom.act_ru_task USING btree (proc_def_id_);


--
-- TOC entry 6592 (class 1259 OID 891325)
-- Name: idx_889481_act_fk_task_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889481_act_fk_task_procinst ON vaxiom.act_ru_task USING btree (proc_inst_id_);


--
-- TOC entry 6593 (class 1259 OID 891344)
-- Name: idx_889481_act_idx_task_create; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889481_act_idx_task_create ON vaxiom.act_ru_task USING btree (create_time_);


--
-- TOC entry 6596 (class 1259 OID 891338)
-- Name: idx_889488_act_fk_var_bytearray; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889488_act_fk_var_bytearray ON vaxiom.act_ru_variable USING btree (bytearray_id_);


--
-- TOC entry 6597 (class 1259 OID 891342)
-- Name: idx_889488_act_fk_var_exe; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889488_act_fk_var_exe ON vaxiom.act_ru_variable USING btree (execution_id_);


--
-- TOC entry 6598 (class 1259 OID 891366)
-- Name: idx_889488_act_fk_var_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889488_act_fk_var_procinst ON vaxiom.act_ru_variable USING btree (proc_inst_id_);


--
-- TOC entry 6599 (class 1259 OID 891348)
-- Name: idx_889488_act_idx_variable_task_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889488_act_idx_variable_task_id ON vaxiom.act_ru_variable USING btree (task_id_);


--
-- TOC entry 6602 (class 1259 OID 891339)
-- Name: idx_889494_owner; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889494_owner ON vaxiom.added_report_type USING btree (owner, report_type);


--
-- TOC entry 6607 (class 1259 OID 891335)
-- Name: idx_889497_questionid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889497_questionid ON vaxiom.answers USING btree (question_id);


--
-- TOC entry 6610 (class 1259 OID 891347)
-- Name: idx_889509_payment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889509_payment_id ON vaxiom.applied_payments USING btree (payment_id);


--
-- TOC entry 6613 (class 1259 OID 891353)
-- Name: idx_889512_next_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889512_next_appointment_id ON vaxiom.appointments USING btree (next_appointment_id);


--
-- TOC entry 6614 (class 1259 OID 891375)
-- Name: idx_889512_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_patient_id ON vaxiom.appointments USING btree (patient_id);


--
-- TOC entry 6617 (class 1259 OID 891354)
-- Name: idx_889512_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_sys_created_at ON vaxiom.appointments USING btree (sys_created_at);


--
-- TOC entry 6618 (class 1259 OID 891350)
-- Name: idx_889512_tx_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_tx_id ON vaxiom.appointments USING btree (tx_id);


--
-- TOC entry 6619 (class 1259 OID 891351)
-- Name: idx_889512_tx_plan_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_tx_plan_appointment_id ON vaxiom.appointments USING btree (tx_plan_appointment_id);


--
-- TOC entry 6620 (class 1259 OID 891357)
-- Name: idx_889512_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_type_id ON vaxiom.appointments USING btree (type_id);


--
-- TOC entry 6621 (class 1259 OID 891346)
-- Name: idx_889521_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889521_appointment_id ON vaxiom.appointment_audit_log USING btree (appointment_id);


--
-- TOC entry 6624 (class 1259 OID 891358)
-- Name: idx_889524_appointment_bookings_local_start_date; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_appointment_bookings_local_start_date ON vaxiom.appointment_bookings USING btree (local_start_date);


--
-- TOC entry 6625 (class 1259 OID 891359)
-- Name: idx_889524_appointment_bookings_state; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_appointment_bookings_state ON vaxiom.appointment_bookings USING btree (state);


--
-- TOC entry 6626 (class 1259 OID 891365)
-- Name: idx_889524_appointment_bookings_state_provider; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_appointment_bookings_state_provider ON vaxiom.appointment_bookings USING btree (state, provider_id);


--
-- TOC entry 6627 (class 1259 OID 891383)
-- Name: idx_889524_appointment_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_appointment_index ON vaxiom.appointment_bookings USING btree (appointment_id);


--
-- TOC entry 6628 (class 1259 OID 891373)
-- Name: idx_889524_assistant_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_assistant_id ON vaxiom.appointment_bookings USING btree (assistant_id);


--
-- TOC entry 6629 (class 1259 OID 891364)
-- Name: idx_889524_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_chair_id ON vaxiom.appointment_bookings USING btree (chair_id);


--
-- TOC entry 6630 (class 1259 OID 891361)
-- Name: idx_889524_fk_appointment_bookings_chairs; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_fk_appointment_bookings_chairs ON vaxiom.appointment_bookings USING btree (seated_chair_id);


--
-- TOC entry 6633 (class 1259 OID 891367)
-- Name: idx_889524_provider_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_provider_id ON vaxiom.appointment_bookings USING btree (provider_id);


--
-- TOC entry 6634 (class 1259 OID 891356)
-- Name: idx_889524_start_time_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_start_time_index ON vaxiom.appointment_bookings USING btree (start_time);


--
-- TOC entry 6635 (class 1259 OID 891371)
-- Name: idx_889524_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_sys_created_at ON vaxiom.appointment_bookings USING btree (sys_created_at);


--
-- TOC entry 6636 (class 1259 OID 891370)
-- Name: idx_889531_appointment_booking_resources_ibfk_2; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889531_appointment_booking_resources_ibfk_2 ON vaxiom.appointment_booking_resources USING btree (booking_id);


--
-- TOC entry 6639 (class 1259 OID 891374)
-- Name: idx_889531_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889531_resource_id ON vaxiom.appointment_booking_resources USING btree (resource_id);


--
-- TOC entry 6640 (class 1259 OID 891392)
-- Name: idx_889538_booking_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889538_booking_id ON vaxiom.appointment_booking_state_changes USING btree (booking_id);


--
-- TOC entry 6643 (class 1259 OID 891376)
-- Name: idx_889544_field_definition_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889544_field_definition_id ON vaxiom.appointment_field_values USING btree (field_definition_id);


--
-- TOC entry 6644 (class 1259 OID 891372)
-- Name: idx_889544_field_option_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889544_field_option_id ON vaxiom.appointment_field_values USING btree (field_option_id);


--
-- TOC entry 6649 (class 1259 OID 891368)
-- Name: idx_889547_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889547_procedure_id ON vaxiom.appointment_procedures USING btree (procedure_id);


--
-- TOC entry 6650 (class 1259 OID 891382)
-- Name: idx_889554_appointment_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889554_appointment_type_id ON vaxiom.appointment_templates USING btree (appointment_type_id);


--
-- TOC entry 6651 (class 1259 OID 891380)
-- Name: idx_889554_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889554_organization_id ON vaxiom.appointment_templates USING btree (organization_id, appointment_type_id);


--
-- TOC entry 6654 (class 1259 OID 891384)
-- Name: idx_889554_primary_assistant_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889554_primary_assistant_type_id ON vaxiom.appointment_templates USING btree (primary_assistant_type_id);


--
-- TOC entry 6655 (class 1259 OID 891401)
-- Name: idx_889554_primary_provider_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889554_primary_provider_type_id ON vaxiom.appointment_templates USING btree (primary_provider_type_id);


--
-- TOC entry 6658 (class 1259 OID 891388)
-- Name: idx_889561_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889561_procedure_id ON vaxiom.appointment_template_procedures USING btree (procedure_id);


--
-- TOC entry 6659 (class 1259 OID 891386)
-- Name: idx_889564_appointment_types_full_name_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889564_appointment_types_full_name_idx ON vaxiom.appointment_types USING btree (full_name);


--
-- TOC entry 6660 (class 1259 OID 891385)
-- Name: idx_889564_appointment_types_migrated; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889564_appointment_types_migrated ON vaxiom.appointment_types USING btree (migrated);


--
-- TOC entry 6661 (class 1259 OID 891387)
-- Name: idx_889564_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889564_name ON vaxiom.appointment_types USING btree (name);


--
-- TOC entry 6662 (class 1259 OID 891379)
-- Name: idx_889564_parent_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889564_parent_id ON vaxiom.appointment_types USING btree (parent_id);


--
-- TOC entry 6667 (class 1259 OID 891391)
-- Name: idx_889580_appointment_type_daily_restrictions_date_location_ty; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889580_appointment_type_daily_restrictions_date_location_ty ON vaxiom.appointment_type_daily_restrictions USING btree (local_date, location_id, type_id);


--
-- TOC entry 6668 (class 1259 OID 891395)
-- Name: idx_889580_location_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889580_location_id_fk ON vaxiom.appointment_type_daily_restrictions USING btree (location_id);


--
-- TOC entry 6671 (class 1259 OID 891419)
-- Name: idx_889580_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889580_type_id ON vaxiom.appointment_type_daily_restrictions USING btree (type_id, local_date, location_id);


--
-- TOC entry 6674 (class 1259 OID 891396)
-- Name: idx_889584_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889584_template_id ON vaxiom.appt_type_template_restrictions USING btree (template_id, type_id);


--
-- TOC entry 6675 (class 1259 OID 891393)
-- Name: idx_889584_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889584_type_id ON vaxiom.appt_type_template_restrictions USING btree (type_id);


--
-- TOC entry 6680 (class 1259 OID 891408)
-- Name: idx_889594_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889594_user_id ON vaxiom.audit_logs USING btree (user_id);


--
-- TOC entry 6683 (class 1259 OID 891405)
-- Name: idx_889597_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889597_receivable_id ON vaxiom.autopay_invoice_changes USING btree (receivable_id);


--
-- TOC entry 6686 (class 1259 OID 891425)
-- Name: idx_889609_employee_benefit_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889609_employee_benefit_id ON vaxiom.benefit_saving_account_values USING btree (employee_benefit_id, insurance_plan_saving_account_id);


--
-- TOC entry 6687 (class 1259 OID 891428)
-- Name: idx_889609_fk_benefit_saving_account_values_employee_beneficiar; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889609_fk_benefit_saving_account_values_employee_beneficiar ON vaxiom.benefit_saving_account_values USING btree (employee_beneficiary_group_id);


--
-- TOC entry 6688 (class 1259 OID 891409)
-- Name: idx_889609_fk_benefit_saving_accout_values_employee_benefit1_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889609_fk_benefit_saving_accout_values_employee_benefit1_id ON vaxiom.benefit_saving_account_values USING btree (employee_benefit_id);


--
-- TOC entry 6689 (class 1259 OID 891404)
-- Name: idx_889609_fk_benefit_saving_accout_values_insurance_plan_savin; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889609_fk_benefit_saving_accout_values_insurance_plan_savin ON vaxiom.benefit_saving_account_values USING btree (insurance_plan_saving_account_id);


--
-- TOC entry 6696 (class 1259 OID 891418)
-- Name: idx_889624_patient_image_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889624_patient_image_id ON vaxiom.cephx_requests USING btree (patient_image_id);


--
-- TOC entry 6701 (class 1259 OID 891416)
-- Name: idx_889635_chair_allocations_chair_id_primary_resource_ca_date; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889635_chair_allocations_chair_id_primary_resource_ca_date ON vaxiom.chair_allocations USING btree (chair_id, ca_date, primary_resource);


--
-- TOC entry 6702 (class 1259 OID 891435)
-- Name: idx_889635_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889635_chair_id ON vaxiom.chair_allocations USING btree (chair_id);


--
-- TOC entry 6705 (class 1259 OID 891420)
-- Name: idx_889635_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889635_resource_id ON vaxiom.chair_allocations USING btree (resource_id);


--
-- TOC entry 6708 (class 1259 OID 891412)
-- Name: idx_889642_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889642_organization_id ON vaxiom.checks USING btree (organization_id);


--
-- TOC entry 6711 (class 1259 OID 891417)
-- Name: idx_889642_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889642_type_id ON vaxiom.checks USING btree (type_id);


--
-- TOC entry 6716 (class 1259 OID 891422)
-- Name: idx_889648_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889648_receivable_id ON vaxiom.claim_events USING btree (receivable_id);


--
-- TOC entry 6717 (class 1259 OID 891423)
-- Name: idx_889654_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889654_location_id ON vaxiom.claim_requests USING btree (location_id);


--
-- TOC entry 6718 (class 1259 OID 891446)
-- Name: idx_889654_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889654_patient_id ON vaxiom.claim_requests USING btree (patient_id);


--
-- TOC entry 6721 (class 1259 OID 891431)
-- Name: idx_889654_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889654_receivable_id ON vaxiom.claim_requests USING btree (receivable_id);


--
-- TOC entry 6722 (class 1259 OID 891430)
-- Name: idx_889654_treatment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889654_treatment_id ON vaxiom.claim_requests USING btree (treatment_id);


--
-- TOC entry 6723 (class 1259 OID 891426)
-- Name: idx_889660_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889660_location_id ON vaxiom.collections_cache USING btree (location_id, date);


--
-- TOC entry 6724 (class 1259 OID 891439)
-- Name: idx_889666_collection_label_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889666_collection_label_template_id ON vaxiom.collection_labels USING btree (collection_label_template_id);


--
-- TOC entry 6725 (class 1259 OID 891424)
-- Name: idx_889666_last_modified_by; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889666_last_modified_by ON vaxiom.collection_labels USING btree (last_modified_by);


--
-- TOC entry 6726 (class 1259 OID 891438)
-- Name: idx_889666_payment_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889666_payment_account_id ON vaxiom.collection_labels USING btree (payment_account_id);


--
-- TOC entry 6729 (class 1259 OID 891434)
-- Name: idx_889669_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889669_parent_company_id ON vaxiom.collection_label_agencies USING btree (parent_company_id);


--
-- TOC entry 6732 (class 1259 OID 891458)
-- Name: idx_889672_created_by; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889672_created_by ON vaxiom.collection_label_history USING btree (created_by);


--
-- TOC entry 6733 (class 1259 OID 891457)
-- Name: idx_889672_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889672_patient_id ON vaxiom.collection_label_history USING btree (patient_id);


--
-- TOC entry 6734 (class 1259 OID 891440)
-- Name: idx_889672_payment_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889672_payment_account_id ON vaxiom.collection_label_history USING btree (payment_account_id);


--
-- TOC entry 6735 (class 1259 OID 891444)
-- Name: idx_889672_previous_collection_label_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889672_previous_collection_label_template_id ON vaxiom.collection_label_history USING btree (previous_collection_label_template_id);


--
-- TOC entry 6738 (class 1259 OID 891450)
-- Name: idx_889675_agency_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889675_agency_id ON vaxiom.collection_label_templates USING btree (agency_id);


--
-- TOC entry 6739 (class 1259 OID 891436)
-- Name: idx_889675_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889675_parent_company_id ON vaxiom.collection_label_templates USING btree (parent_company_id);


--
-- TOC entry 6742 (class 1259 OID 891442)
-- Name: idx_889681_contact_method_association_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889681_contact_method_association_id ON vaxiom.communication_preferences USING btree (contact_method_association_id);


--
-- TOC entry 6743 (class 1259 OID 891445)
-- Name: idx_889681_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889681_patient_id ON vaxiom.communication_preferences USING btree (patient_id);


--
-- TOC entry 6746 (class 1259 OID 891468)
-- Name: idx_889684_address; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889684_address ON vaxiom.contact_emails USING btree (address);


--
-- TOC entry 6751 (class 1259 OID 891454)
-- Name: idx_889691_contact_method_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889691_contact_method_id ON vaxiom.contact_method_associations USING btree (contact_method_id);


--
-- TOC entry 6752 (class 1259 OID 891453)
-- Name: idx_889691_error_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889691_error_location_id ON vaxiom.contact_method_associations USING btree (error_location_id);


--
-- TOC entry 6753 (class 1259 OID 891461)
-- Name: idx_889691_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889691_person_id ON vaxiom.contact_method_associations USING btree (person_id);


--
-- TOC entry 6756 (class 1259 OID 891459)
-- Name: idx_889694_number; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889694_number ON vaxiom.contact_phones USING btree (number);


--
-- TOC entry 6759 (class 1259 OID 891456)
-- Name: idx_889697_address_line1_city_zip_state; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889697_address_line1_city_zip_state ON vaxiom.contact_postal_addresses USING btree (address_line1, city, zip, state);


--
-- TOC entry 6760 (class 1259 OID 891455)
-- Name: idx_889697_contact_postal_addresses_city; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889697_contact_postal_addresses_city ON vaxiom.contact_postal_addresses USING btree (city);


--
-- TOC entry 6767 (class 1259 OID 891464)
-- Name: idx_889709_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889709_name ON vaxiom.correction_types USING btree (name, parent_company_id);


--
-- TOC entry 6768 (class 1259 OID 891463)
-- Name: idx_889709_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889709_parent_company_id ON vaxiom.correction_types USING btree (parent_company_id);


--
-- TOC entry 6771 (class 1259 OID 891460)
-- Name: idx_889715_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889715_organization_id ON vaxiom.correction_types_locations USING btree (organization_id);


--
-- TOC entry 6774 (class 1259 OID 891465)
-- Name: idx_889718_correction_types_payment_options_ibfk_2; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889718_correction_types_payment_options_ibfk_2 ON vaxiom.correction_types_payment_options USING btree (payment_options_id);


--
-- TOC entry 6777 (class 1259 OID 891467)
-- Name: idx_889721_county_state_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889721_county_state_index ON vaxiom.county USING btree (state);


--
-- TOC entry 6780 (class 1259 OID 891493)
-- Name: idx_889724_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889724_location_id ON vaxiom.daily_transactions USING btree (location_id, date);


--
-- TOC entry 6783 (class 1259 OID 891475)
-- Name: idx_889731_data_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889731_data_id ON vaxiom.data_lock USING btree (data_id);


--
-- TOC entry 6786 (class 1259 OID 891481)
-- Name: idx_889731_session_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889731_session_id ON vaxiom.data_lock USING btree (session_id, user_id, data_id);


--
-- TOC entry 6787 (class 1259 OID 891471)
-- Name: idx_889734_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889734_organization_id_fk ON vaxiom.day_schedules USING btree (organization_id);


--
-- TOC entry 6790 (class 1259 OID 891478)
-- Name: idx_889734_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889734_resource_id ON vaxiom.day_schedules USING btree (resource_id);


--
-- TOC entry 6791 (class 1259 OID 891476)
-- Name: idx_889741_appt_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889741_appt_type_id ON vaxiom.day_schedule_appt_slots USING btree (appt_type_id);


--
-- TOC entry 6798 (class 1259 OID 891483)
-- Name: idx_889750_location_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889750_location_unique ON vaxiom.default_appointment_templates USING btree (location_id);


--
-- TOC entry 6801 (class 1259 OID 891485)
-- Name: idx_889750_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889750_template_id ON vaxiom.default_appointment_templates USING btree (template_id);


--
-- TOC entry 6802 (class 1259 OID 891490)
-- Name: idx_889753_marking_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889753_marking_id_fk ON vaxiom.dentalchart_additional_markings USING btree (marking_id);


--
-- TOC entry 6807 (class 1259 OID 891489)
-- Name: idx_889756_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889756_snapshot_id ON vaxiom.dentalchart_edit_log USING btree (snapshot_id);


--
-- TOC entry 6810 (class 1259 OID 891492)
-- Name: idx_889759_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889759_snapshot_id ON vaxiom.dentalchart_marking USING btree (snapshot_id);


--
-- TOC entry 6813 (class 1259 OID 891517)
-- Name: idx_889768_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889768_appointment_id ON vaxiom.dentalchart_snapshot USING btree (appointment_id);


--
-- TOC entry 6816 (class 1259 OID 891497)
-- Name: idx_889774_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889774_appointment_id ON vaxiom.diagnosis USING btree (appointment_id);


--
-- TOC entry 6817 (class 1259 OID 891496)
-- Name: idx_889774_diagnosis_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889774_diagnosis_template_id ON vaxiom.diagnosis USING btree (diagnosis_template_id);


--
-- TOC entry 6822 (class 1259 OID 891507)
-- Name: idx_889786_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889786_address_id ON vaxiom.diagnosis_letters USING btree (address_id);


--
-- TOC entry 6823 (class 1259 OID 891501)
-- Name: idx_889786_diagnosis_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889786_diagnosis_id ON vaxiom.diagnosis_letters USING btree (diagnosis_id);


--
-- TOC entry 6824 (class 1259 OID 891498)
-- Name: idx_889786_document_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889786_document_template_id ON vaxiom.diagnosis_letters USING btree (document_template_id);


--
-- TOC entry 6827 (class 1259 OID 891530)
-- Name: idx_889786_recipient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889786_recipient_id ON vaxiom.diagnosis_letters USING btree (recipient_id);


--
-- TOC entry 6828 (class 1259 OID 891528)
-- Name: idx_889789_appointment_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889789_appointment_type_id ON vaxiom.diagnosis_templates USING btree (appointment_type_id);


--
-- TOC entry 6829 (class 1259 OID 891504)
-- Name: idx_889789_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889789_parent_company_id ON vaxiom.diagnosis_templates USING btree (parent_company_id);


--
-- TOC entry 6832 (class 1259 OID 891506)
-- Name: idx_889789_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889789_tx_category_id ON vaxiom.diagnosis_templates USING btree (tx_category_id);


--
-- TOC entry 6833 (class 1259 OID 891510)
-- Name: idx_889795_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889795_organization_id ON vaxiom.discounts USING btree (organization_id);


--
-- TOC entry 6836 (class 1259 OID 891519)
-- Name: idx_889802_discount_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889802_discount_id ON vaxiom.discount_adjustments USING btree (discount_id);


--
-- TOC entry 6839 (class 1259 OID 891509)
-- Name: idx_889805_discount_adjustment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889805_discount_adjustment_id ON vaxiom.discount_reverse_adjustments USING btree (discount_adjustment_id);


--
-- TOC entry 6846 (class 1259 OID 891514)
-- Name: idx_889817_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889817_patient_id ON vaxiom.dock_item_features_images USING btree (patient_id);


--
-- TOC entry 6851 (class 1259 OID 891521)
-- Name: idx_889823_note_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889823_note_id ON vaxiom.dock_item_features_note USING btree (note_id);


--
-- TOC entry 6858 (class 1259 OID 891520)
-- Name: idx_889835_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889835_organization_id ON vaxiom.document_templates USING btree (organization_id, name);


--
-- TOC entry 6861 (class 1259 OID 891551)
-- Name: idx_889842_ix_document_tree_nodes_file_uid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889842_ix_document_tree_nodes_file_uid ON vaxiom.document_tree_nodes USING btree (file_uid);


--
-- TOC entry 6862 (class 1259 OID 891548)
-- Name: idx_889842_ix_document_tree_nodes_parent_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889842_ix_document_tree_nodes_parent_id ON vaxiom.document_tree_nodes USING btree (parent_id);


--
-- TOC entry 6863 (class 1259 OID 891525)
-- Name: idx_889842_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889842_patient_id ON vaxiom.document_tree_nodes USING btree (patient_id);


--
-- TOC entry 6868 (class 1259 OID 891532)
-- Name: idx_889853_fk_employee_benebitiary_employee_benefitiary_group1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889853_fk_employee_benebitiary_employee_benefitiary_group1_ ON vaxiom.employee_beneficiary USING btree (employee_beneficiary_group_id);


--
-- TOC entry 6869 (class 1259 OID 891523)
-- Name: idx_889853_fk_employee_benebitiary_employee_enrollment_person1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889853_fk_employee_benebitiary_employee_enrollment_person1_ ON vaxiom.employee_beneficiary USING btree (employee_enrollment_person_id);


--
-- TOC entry 6872 (class 1259 OID 891533)
-- Name: idx_889856_fk_employee_benefitiary_group_employee_enrollment1_i; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889856_fk_employee_benefitiary_group_employee_enrollment1_i ON vaxiom.employee_beneficiary_group USING btree (employee_enrollment_id);


--
-- TOC entry 6875 (class 1259 OID 891537)
-- Name: idx_889859_benefit_type; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889859_benefit_type ON vaxiom.employee_benefit USING btree (benefit_type, employee_enrollment_id);


--
-- TOC entry 6876 (class 1259 OID 891558)
-- Name: idx_889859_fk_employee_benefit_employee_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889859_fk_employee_benefit_employee_enrollment1_idx ON vaxiom.employee_benefit USING btree (employee_enrollment_id);


--
-- TOC entry 6877 (class 1259 OID 891559)
-- Name: idx_889859_fk_employee_benefit_insurance_palan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889859_fk_employee_benefit_insurance_palan1_idx ON vaxiom.employee_benefit USING btree (insurance_plan_id);


--
-- TOC entry 6880 (class 1259 OID 891535)
-- Name: idx_889867_fk_employee_dependent_employee_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889867_fk_employee_dependent_employee_enrollment1_idx ON vaxiom.employee_dependent USING btree (employee_enrollment_id);


--
-- TOC entry 6881 (class 1259 OID 891540)
-- Name: idx_889867_fk_employee_dependet_employee_enrollment_person1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889867_fk_employee_dependet_employee_enrollment_person1_idx ON vaxiom.employee_dependent USING btree (employee_enrollment_person_id);


--
-- TOC entry 6884 (class 1259 OID 891536)
-- Name: idx_889873_fk_employee_enrollment_employment_contracts1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889873_fk_employee_enrollment_employment_contracts1_idx ON vaxiom.employee_enrollment USING btree (employment_contracts_id);


--
-- TOC entry 6885 (class 1259 OID 891555)
-- Name: idx_889873_fk_employee_enrollment_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889873_fk_employee_enrollment_enrollment1_idx ON vaxiom.employee_enrollment USING btree (enrollment_id);


--
-- TOC entry 6886 (class 1259 OID 891549)
-- Name: idx_889873_fk_employee_enrollment_enrollment_form_source_event1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889873_fk_employee_enrollment_enrollment_form_source_event1 ON vaxiom.employee_enrollment USING btree (enrollment_form_source_event_id);


--
-- TOC entry 6889 (class 1259 OID 891545)
-- Name: idx_889882_fk_employee_enrollment_attachment_employee_enrollmen; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889882_fk_employee_enrollment_attachment_employee_enrollmen ON vaxiom.employee_enrollment_attachment USING btree (employee_enrollment_id);


--
-- TOC entry 6892 (class 1259 OID 891570)
-- Name: idx_889888_fk_employee_enrollment_event_employee_enrollment1_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889888_fk_employee_enrollment_event_employee_enrollment1_id ON vaxiom.employee_enrollment_event USING btree (employee_enrollment_id);


--
-- TOC entry 6895 (class 1259 OID 891546)
-- Name: idx_889894_fk_employee_enrollment_person_employee_enrollment1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889894_fk_employee_enrollment_person_employee_enrollment1 ON vaxiom.employee_enrollment_person USING btree (employee_enrollment_id);


--
-- TOC entry 6898 (class 1259 OID 891553)
-- Name: idx_889900_fk_employee_insured_employee_benefit1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889900_fk_employee_insured_employee_benefit1_idx ON vaxiom.employee_insured USING btree (employee_benefit_id);


--
-- TOC entry 6899 (class 1259 OID 891547)
-- Name: idx_889900_fk_employee_insured_employee_benefitiary_group1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889900_fk_employee_insured_employee_benefitiary_group1_idx ON vaxiom.employee_insured USING btree (employee_beneficiary_group_id);


--
-- TOC entry 6900 (class 1259 OID 891565)
-- Name: idx_889900_fk_employee_insured_employee_dependet1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889900_fk_employee_insured_employee_dependet1_idx ON vaxiom.employee_insured USING btree (employee_dependent_id);


--
-- TOC entry 6905 (class 1259 OID 891557)
-- Name: idx_889904_provider_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889904_provider_id ON vaxiom.employee_professional_relationships USING btree (provider_id);


--
-- TOC entry 6906 (class 1259 OID 891576)
-- Name: idx_889907_employment_contract_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889907_employment_contract_id ON vaxiom.employee_resources USING btree (employment_contract_id);


--
-- TOC entry 6909 (class 1259 OID 891552)
-- Name: idx_889911_contact_email_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_contact_email_id ON vaxiom.employers USING btree (contact_email_id);


--
-- TOC entry 6910 (class 1259 OID 891562)
-- Name: idx_889911_contact_phone_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_contact_phone_id ON vaxiom.employers USING btree (contact_phone_id);


--
-- TOC entry 6911 (class 1259 OID 891560)
-- Name: idx_889911_contact_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_contact_postal_address_id ON vaxiom.employers USING btree (contact_postal_address_id);


--
-- TOC entry 6912 (class 1259 OID 891569)
-- Name: idx_889911_master_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_master_id ON vaxiom.employers USING btree (master_id);


--
-- TOC entry 6913 (class 1259 OID 891554)
-- Name: idx_889911_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_organization_id ON vaxiom.employers USING btree (organization_id);


--
-- TOC entry 6916 (class 1259 OID 891571)
-- Name: idx_889914_fk_employment_contracts_enrollment_group1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_enrollment_group1_idx ON vaxiom.employment_contracts USING btree (enrollment_group_id);


--
-- TOC entry 6917 (class 1259 OID 891566)
-- Name: idx_889914_fk_employment_contracts_jobtitle1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_jobtitle1_idx ON vaxiom.employment_contracts USING btree (jobtitle_id);


--
-- TOC entry 6918 (class 1259 OID 891567)
-- Name: idx_889914_fk_employment_contracts_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_legal_entity1_idx ON vaxiom.employment_contracts USING btree (legal_entity_id);


--
-- TOC entry 6919 (class 1259 OID 891585)
-- Name: idx_889914_fk_employment_contracts_legal_entity_billing_group1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_legal_entity_billing_group1_ ON vaxiom.employment_contracts USING btree (billing_group_id);


--
-- TOC entry 6920 (class 1259 OID 891575)
-- Name: idx_889914_fk_employment_contracts_sys_organization_address1_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_sys_organization_address1_id ON vaxiom.employment_contracts USING btree (physical_address_id);


--
-- TOC entry 6921 (class 1259 OID 891587)
-- Name: idx_889914_fk_employment_contracts_sys_organizations1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_sys_organizations1_idx ON vaxiom.employment_contracts USING btree (department_id);


--
-- TOC entry 6922 (class 1259 OID 891561)
-- Name: idx_889914_fk_employment_contracts_sys_organizations2_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_sys_organizations2_idx ON vaxiom.employment_contracts USING btree (division_id);


--
-- TOC entry 6923 (class 1259 OID 891572)
-- Name: idx_889914_human_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889914_human_id ON vaxiom.employment_contracts USING btree (legacy_id);


--
-- TOC entry 6924 (class 1259 OID 891578)
-- Name: idx_889914_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_organization_id_fk ON vaxiom.employment_contracts USING btree (organization_id);


--
-- TOC entry 6925 (class 1259 OID 891563)
-- Name: idx_889914_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_person_id ON vaxiom.employment_contracts USING btree (person_id);


--
-- TOC entry 6928 (class 1259 OID 891581)
-- Name: idx_889921_fk_employee_emergency_contact_employment_contracts1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889921_fk_employee_emergency_contact_employment_contracts1_ ON vaxiom.employment_contract_emergency_contact USING btree (employment_contracts_id);


--
-- TOC entry 6931 (class 1259 OID 891574)
-- Name: idx_889924_fk_employment_contract_enrollment_group_log_employme; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889924_fk_employment_contract_enrollment_group_log_employme ON vaxiom.employment_contract_enrollment_group_log USING btree (employment_contracts_id);


--
-- TOC entry 6934 (class 1259 OID 891598)
-- Name: idx_889930_fk_employement_contract_job_status_log_employment_co; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889930_fk_employement_contract_job_status_log_employment_co ON vaxiom.employment_contract_job_status_log USING btree (employment_contracts_id);


--
-- TOC entry 6937 (class 1259 OID 891594)
-- Name: idx_889933_emploment_contract_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889933_emploment_contract_fk ON vaxiom.employment_contract_location_group USING btree (employment_contract_id);


--
-- TOC entry 6938 (class 1259 OID 891582)
-- Name: idx_889933_location_group_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889933_location_group_fk ON vaxiom.employment_contract_location_group USING btree (location_group_id);


--
-- TOC entry 6941 (class 1259 OID 891583)
-- Name: idx_889936_ecperm_emploment_contract_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889936_ecperm_emploment_contract_fk ON vaxiom.employment_contract_permissions USING btree (employment_contract_id);


--
-- TOC entry 6942 (class 1259 OID 891597)
-- Name: idx_889936_ecperm_permission_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889936_ecperm_permission_fk ON vaxiom.employment_contract_permissions USING btree (permissions_permission_id);


--
-- TOC entry 6945 (class 1259 OID 891589)
-- Name: idx_889939_ecrole_user_role_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889939_ecrole_user_role_fk ON vaxiom.employment_contract_role USING btree (permissions_user_role_id);


--
-- TOC entry 6946 (class 1259 OID 891584)
-- Name: idx_889939_emploment_contract_fk_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889939_emploment_contract_fk_idx ON vaxiom.employment_contract_role USING btree (employment_contract_id);


--
-- TOC entry 6949 (class 1259 OID 891609)
-- Name: idx_889942_fk_employment_contract_salary_log_employment_cont_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889942_fk_employment_contract_salary_log_employment_cont_id ON vaxiom.employment_contract_salary_log USING btree (employment_contracts_id);


--
-- TOC entry 6952 (class 1259 OID 891603)
-- Name: idx_889948_fk_enrollment_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889948_fk_enrollment_legal_entity1_idx ON vaxiom.enrollment USING btree (legal_entity_id);


--
-- TOC entry 6955 (class 1259 OID 891596)
-- Name: idx_889948_uq_year_lagal_entity; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889948_uq_year_lagal_entity ON vaxiom.enrollment USING btree (year, legal_entity_id);


--
-- TOC entry 6956 (class 1259 OID 891591)
-- Name: idx_889956_employee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889956_employee_id ON vaxiom.enrollment_form USING btree (employee_id);


--
-- TOC entry 6957 (class 1259 OID 891605)
-- Name: idx_889956_enrollment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889956_enrollment_id ON vaxiom.enrollment_form USING btree (enrollment_id);


--
-- TOC entry 6960 (class 1259 OID 891599)
-- Name: idx_889962_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889962_enrollment_form_id ON vaxiom.enrollment_form_beneficiary USING btree (enrollment_form_id);


--
-- TOC entry 6963 (class 1259 OID 891623)
-- Name: idx_889971_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889971_enrollment_form_id ON vaxiom.enrollment_form_benefits USING btree (enrollment_form_id);


--
-- TOC entry 6966 (class 1259 OID 891606)
-- Name: idx_889980_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889980_enrollment_form_id ON vaxiom.enrollment_form_dependents USING btree (enrollment_form_id);


--
-- TOC entry 6969 (class 1259 OID 891604)
-- Name: idx_889989_enrollment_form_benefit_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889989_enrollment_form_benefit_id ON vaxiom.enrollment_form_dependents_benefits USING btree (enrollment_form_benefit_id);


--
-- TOC entry 6970 (class 1259 OID 891610)
-- Name: idx_889989_enrollment_form_dependent_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889989_enrollment_form_dependent_id ON vaxiom.enrollment_form_dependents_benefits USING btree (enrollment_form_dependent_id);


--
-- TOC entry 6973 (class 1259 OID 891617)
-- Name: idx_889998_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889998_enrollment_form_id ON vaxiom.enrollment_form_personal_data USING btree (enrollment_form_id);


--
-- TOC entry 6980 (class 1259 OID 891632)
-- Name: idx_890014_fk_eg_enrollment; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890014_fk_eg_enrollment ON vaxiom.enrollment_groups_enrollment_legal_entity USING btree (enrollment_id);


--
-- TOC entry 6981 (class 1259 OID 891634)
-- Name: idx_890014_fk_egle_legal_entity; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890014_fk_egle_legal_entity ON vaxiom.enrollment_groups_enrollment_legal_entity USING btree (legal_entity_id);


--
-- TOC entry 6984 (class 1259 OID 891622)
-- Name: idx_890017_enrollment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890017_enrollment_id ON vaxiom.enrollment_group_enrollment USING btree (enrollment_id);


--
-- TOC entry 6987 (class 1259 OID 891619)
-- Name: idx_890020_legal_entity_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890020_legal_entity_id ON vaxiom.enrollment_schedule_billing_groups USING btree (legal_entity_id);


--
-- TOC entry 6992 (class 1259 OID 891624)
-- Name: idx_890027_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890027_postal_address_id ON vaxiom.external_offices USING btree (postal_address_id);


--
-- TOC entry 6995 (class 1259 OID 891616)
-- Name: idx_890030_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890030_organization_id ON vaxiom.financial_settings USING btree (organization_id);


--
-- TOC entry 7000 (class 1259 OID 891692)
-- Name: idx_890040_idx_gf10219; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890040_idx_gf10219 ON vaxiom.gf10219 USING btree (appointment_id);


--
-- TOC entry 7005 (class 1259 OID 891629)
-- Name: idx_890068_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890068_organization_id ON vaxiom.image_series USING btree (organization_id);


--
-- TOC entry 7008 (class 1259 OID 891636)
-- Name: idx_890068_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890068_type_id ON vaxiom.image_series USING btree (type_id);


--
-- TOC entry 7011 (class 1259 OID 891628)
-- Name: idx_890074_payment_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890074_payment_plan_id ON vaxiom.installment_charges USING btree (payment_plan_id);


--
-- TOC entry 7014 (class 1259 OID 891653)
-- Name: idx_890077_contact_email_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_contact_email_id ON vaxiom.insurance_billing_centers USING btree (contact_email_id);


--
-- TOC entry 7015 (class 1259 OID 891654)
-- Name: idx_890077_contact_phone_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_contact_phone_id ON vaxiom.insurance_billing_centers USING btree (contact_phone_id);


--
-- TOC entry 7016 (class 1259 OID 891701)
-- Name: idx_890077_contact_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_contact_postal_address_id ON vaxiom.insurance_billing_centers USING btree (contact_postal_address_id);


--
-- TOC entry 7017 (class 1259 OID 891640)
-- Name: idx_890077_master_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_master_id ON vaxiom.insurance_billing_centers USING btree (master_id);


--
-- TOC entry 7018 (class 1259 OID 891635)
-- Name: idx_890077_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_organization_id ON vaxiom.insurance_billing_centers USING btree (organization_id);


--
-- TOC entry 7021 (class 1259 OID 891631)
-- Name: idx_890083_fk_billing_entity_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_billing_entity_idx ON vaxiom.insurance_claims_setup USING btree (billing_entity_id);


--
-- TOC entry 7022 (class 1259 OID 891646)
-- Name: idx_890083_fk_insurance_claims_setup_legal_entity_npi1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_legal_entity_npi1_idx ON vaxiom.insurance_claims_setup USING btree (legal_entity_npi_id);


--
-- TOC entry 7023 (class 1259 OID 891642)
-- Name: idx_890083_fk_insurance_claims_setup_legal_entity_tin1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_legal_entity_tin1_idx ON vaxiom.insurance_claims_setup USING btree (tin_id);


--
-- TOC entry 7024 (class 1259 OID 891637)
-- Name: idx_890083_fk_insurance_claims_setup_sys_organization_address1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_sys_organization_address1_ ON vaxiom.insurance_claims_setup USING btree (sys_organization_address_id);


--
-- TOC entry 7025 (class 1259 OID 891663)
-- Name: idx_890083_fk_insurance_claims_setup_sys_organization_contact_m; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_sys_organization_contact_m ON vaxiom.insurance_claims_setup USING btree (sys_organization_contact_method_id);


--
-- TOC entry 7026 (class 1259 OID 891638)
-- Name: idx_890083_fk_insurance_claims_setup_sys_organizations_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_sys_organizations_idx ON vaxiom.insurance_claims_setup USING btree (sys_organizations_id);


--
-- TOC entry 7031 (class 1259 OID 891650)
-- Name: idx_890093_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890093_insurance_code_id ON vaxiom.insurance_code_val USING btree (insurance_code_id);


--
-- TOC entry 7032 (class 1259 OID 891645)
-- Name: idx_890093_insurance_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890093_insurance_company_id ON vaxiom.insurance_code_val USING btree (insurance_company_id);


--
-- TOC entry 7033 (class 1259 OID 891647)
-- Name: idx_890093_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890093_organization_id_fk ON vaxiom.insurance_code_val USING btree (organization_id);


--
-- TOC entry 7036 (class 1259 OID 891656)
-- Name: idx_890096_am_email_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_am_email_address_id ON vaxiom.insurance_companies USING btree (am_email_address_id);


--
-- TOC entry 7037 (class 1259 OID 891652)
-- Name: idx_890096_am_phone_number_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_am_phone_number_id ON vaxiom.insurance_companies USING btree (am_phone_number_id);


--
-- TOC entry 7038 (class 1259 OID 891649)
-- Name: idx_890096_master_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_master_id ON vaxiom.insurance_companies USING btree (master_id);


--
-- TOC entry 7039 (class 1259 OID 891648)
-- Name: idx_890096_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_organization_id ON vaxiom.insurance_companies USING btree (organization_id);


--
-- TOC entry 7040 (class 1259 OID 891673)
-- Name: idx_890096_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_postal_address_id ON vaxiom.insurance_companies USING btree (postal_address_id);


--
-- TOC entry 7045 (class 1259 OID 891660)
-- Name: idx_890107_contact_method_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890107_contact_method_id ON vaxiom.insurance_company_phone_associations USING btree (contact_method_id);


--
-- TOC entry 7046 (class 1259 OID 891655)
-- Name: idx_890107_insurance_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890107_insurance_company_id ON vaxiom.insurance_company_phone_associations USING btree (insurance_company_id);


--
-- TOC entry 7049 (class 1259 OID 891651)
-- Name: idx_890111_fk_insurancepaymentid_insurancecompanies; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890111_fk_insurancepaymentid_insurancecompanies ON vaxiom.insurance_payments USING btree (insurance_company_id);


--
-- TOC entry 7050 (class 1259 OID 891666)
-- Name: idx_890111_fk_paymenttypeid_paymenttypes; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890111_fk_paymenttypeid_paymenttypes ON vaxiom.insurance_payments USING btree (payment_type_id);


--
-- TOC entry 7053 (class 1259 OID 891661)
-- Name: idx_890117_insurance_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890117_insurance_company_id ON vaxiom.insurance_payment_accounts USING btree (insurance_company_id);


--
-- TOC entry 7054 (class 1259 OID 891657)
-- Name: idx_890117_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890117_organization_id ON vaxiom.insurance_payment_accounts USING btree (organization_id, insurance_company_id);


--
-- TOC entry 7057 (class 1259 OID 891685)
-- Name: idx_890120_fk_insurancepaymentcorrectionscorrectiontypeid_corre; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890120_fk_insurancepaymentcorrectionscorrectiontypeid_corre ON vaxiom.insurance_payment_corrections USING btree (correction_type_id);


--
-- TOC entry 7060 (class 1259 OID 891670)
-- Name: idx_890123_fk_insurancepaymentmovementid_insurancepaymentid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890123_fk_insurancepaymentmovementid_insurancepaymentid ON vaxiom.insurance_payment_movements USING btree (insurance_payment_id);


--
-- TOC entry 7065 (class 1259 OID 891659)
-- Name: idx_890132_fk_insurancepaymenttransferpaymentid_paymentid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890132_fk_insurancepaymenttransferpaymentid_paymentid ON vaxiom.insurance_payment_transfers USING btree (payment_id);


--
-- TOC entry 7068 (class 1259 OID 891671)
-- Name: idx_890135_fk_insurance_plan_sys_organizations1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890135_fk_insurance_plan_sys_organizations1_idx ON vaxiom.insurance_plan USING btree (sys_organizations_id);


--
-- TOC entry 7073 (class 1259 OID 891693)
-- Name: idx_890145_fk_insurance_plan_age_and_gender_banded_rate_insuran; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890145_fk_insurance_plan_age_and_gender_banded_rate_insuran ON vaxiom.insurance_plan_age_and_gender_banded_rate USING btree (id);


--
-- TOC entry 7076 (class 1259 OID 891739)
-- Name: idx_890148_fk_insurance_plan_age_banded_rate_insurance_plan_rat; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890148_fk_insurance_plan_age_banded_rate_insurance_plan_rat ON vaxiom.insurance_plan_age_banded_rate USING btree (id);


--
-- TOC entry 7083 (class 1259 OID 891668)
-- Name: idx_890157_fk_insurance_plan_benefit_amount_insurance_plan2_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890157_fk_insurance_plan_benefit_amount_insurance_plan2_idx ON vaxiom.insurance_plan_benefit_amount USING btree (insurance_plan_id);


--
-- TOC entry 7090 (class 1259 OID 891677)
-- Name: idx_890166_fk_insurance_plan_complex_benefit_amount_item_insura; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890166_fk_insurance_plan_complex_benefit_amount_item_insura ON vaxiom.insurance_plan_complex_benefit_amount_item USING btree (insurance_plan_complex_benefit_amount_id);


--
-- TOC entry 7095 (class 1259 OID 891750)
-- Name: idx_890172_fk_insurance_plan_composition_rate_insurance_plan_ra; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890172_fk_insurance_plan_composition_rate_insurance_plan_ra ON vaxiom.insurance_plan_composition_rate USING btree (id);


--
-- TOC entry 7098 (class 1259 OID 891686)
-- Name: idx_890175_employer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890175_employer_id ON vaxiom.insurance_plan_employers USING btree (employer_id);


--
-- TOC entry 7101 (class 1259 OID 891680)
-- Name: idx_890178_fk_fixedlimits_insurance_limits1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890178_fk_fixedlimits_insurance_limits1_idx ON vaxiom.insurance_plan_fixed_limit USING btree (insurance_limits_id);


--
-- TOC entry 7108 (class 1259 OID 891691)
-- Name: idx_890189_fk_insurance_plan_has_enrollment_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890189_fk_insurance_plan_has_enrollment_enrollment1_idx ON vaxiom.insurance_plan_has_enrollment USING btree (enrollment_id);


--
-- TOC entry 7109 (class 1259 OID 891714)
-- Name: idx_890189_fk_insurance_plan_has_enrollment_insurance_plan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890189_fk_insurance_plan_has_enrollment_insurance_plan1_idx ON vaxiom.insurance_plan_has_enrollment USING btree (insurance_plan_id);


--
-- TOC entry 7112 (class 1259 OID 891761)
-- Name: idx_890189_uq_insurance_plan_enrollment; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890189_uq_insurance_plan_enrollment ON vaxiom.insurance_plan_has_enrollment USING btree (insurance_plan_id, enrollment_id);


--
-- TOC entry 7113 (class 1259 OID 891694)
-- Name: idx_890193_fk_insurance_plan_has_enrollment_has_enrollment_grou; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890193_fk_insurance_plan_has_enrollment_has_enrollment_grou ON vaxiom.insurance_plan_has_enrollment_has_enrollment_group USING btree (insurance_plan_has_enrollment_id);


--
-- TOC entry 7116 (class 1259 OID 891690)
-- Name: idx_890196_fk_insurance_limits_insurance_palan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890196_fk_insurance_limits_insurance_palan1_idx ON vaxiom.insurance_plan_limits USING btree (insurance_plan_id);


--
-- TOC entry 7123 (class 1259 OID 891702)
-- Name: idx_890205_fk_insurance_plan_rates_insurance_plan_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890205_fk_insurance_plan_rates_insurance_plan_idx ON vaxiom.insurance_plan_rates USING btree (insurance_plan_id);


--
-- TOC entry 7126 (class 1259 OID 891730)
-- Name: idx_890208_fk_insurace_plan_saving_account_insurance_plan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890208_fk_insurace_plan_saving_account_insurance_plan1_idx ON vaxiom.insurance_plan_saving_account USING btree (insurance_plan_id);


--
-- TOC entry 7129 (class 1259 OID 891713)
-- Name: idx_890214_year_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890214_year_index ON vaxiom.insurance_plan_saving_accounts_limits USING btree (year);


--
-- TOC entry 7130 (class 1259 OID 891706)
-- Name: idx_890217_fk_separate_age_banded_rate_insurance_plan_rates_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890217_fk_separate_age_banded_rate_insurance_plan_rates_idx ON vaxiom.insurance_plan_separate_age_banded_rate USING btree (id);


--
-- TOC entry 7135 (class 1259 OID 891721)
-- Name: idx_890223_fk_slider_limits_insurance_limits1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890223_fk_slider_limits_insurance_limits1_idx ON vaxiom.insurance_plan_slider_limit USING btree (insurance_limits_id);


--
-- TOC entry 7138 (class 1259 OID 891715)
-- Name: idx_890226_fk_insurance_plan_summary_fields_insurance_plan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890226_fk_insurance_plan_summary_fields_insurance_plan1_idx ON vaxiom.insurance_plan_summary_fields USING btree (insurance_plan_id);


--
-- TOC entry 7145 (class 1259 OID 891778)
-- Name: idx_890241_employer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890241_employer_id ON vaxiom.insurance_subscriptions USING btree (employer_id);


--
-- TOC entry 7146 (class 1259 OID 891724)
-- Name: idx_890241_insurance_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890241_insurance_plan_id ON vaxiom.insurance_subscriptions USING btree (insurance_plan_id);


--
-- TOC entry 7147 (class 1259 OID 891717)
-- Name: idx_890241_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890241_person_id ON vaxiom.insurance_subscriptions USING btree (person_id);


--
-- TOC entry 7150 (class 1259 OID 891712)
-- Name: idx_890249_insurance_subscription_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890249_insurance_subscription_id ON vaxiom.insured USING btree (insurance_subscription_id);


--
-- TOC entry 7151 (class 1259 OID 891732)
-- Name: idx_890249_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890249_patient_id ON vaxiom.insured USING btree (patient_id);


--
-- TOC entry 7154 (class 1259 OID 891727)
-- Name: idx_890253_fk_insured_rate_amount_employee_insured1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890253_fk_insured_rate_amount_employee_insured1_idx ON vaxiom.insured_benefit_values USING btree (employee_insured_id);


--
-- TOC entry 7159 (class 1259 OID 891751)
-- Name: idx_890259_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890259_account_id ON vaxiom.invoice_export_queue USING btree (account_id);


--
-- TOC entry 7160 (class 1259 OID 891787)
-- Name: idx_890259_invoice_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890259_invoice_id ON vaxiom.invoice_export_queue USING btree (invoice_id);


--
-- TOC entry 7163 (class 1259 OID 891728)
-- Name: idx_890262_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890262_account_id ON vaxiom.invoice_import_export_log USING btree (account_id);


--
-- TOC entry 7164 (class 1259 OID 891731)
-- Name: idx_890262_invoice_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890262_invoice_id ON vaxiom.invoice_import_export_log USING btree (invoice_id);


--
-- TOC entry 7167 (class 1259 OID 891743)
-- Name: idx_890265_network_sheet_fee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890265_network_sheet_fee_id ON vaxiom.in_network_discounts USING btree (network_sheet_fee_id);


--
-- TOC entry 7170 (class 1259 OID 891737)
-- Name: idx_890268_fk_jobtitle_jobtitle_group1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890268_fk_jobtitle_jobtitle_group1_idx ON vaxiom.jobtitle USING btree (jobtitle_group_id);


--
-- TOC entry 7171 (class 1259 OID 891734)
-- Name: idx_890268_fk_jobtitle_sys_organizations1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890268_fk_jobtitle_sys_organizations1_idx ON vaxiom.jobtitle USING btree (sys_organizations_id);


--
-- TOC entry 7172 (class 1259 OID 891758)
-- Name: idx_890268_idx_name_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890268_idx_name_unique ON vaxiom.jobtitle USING btree (name, sys_organizations_id, jobtitle_group_id);


--
-- TOC entry 7175 (class 1259 OID 891799)
-- Name: idx_890271_fk_jobtitle_group_eeo1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890271_fk_jobtitle_group_eeo1_idx ON vaxiom.jobtitle_group USING btree (eeo_id);


--
-- TOC entry 7176 (class 1259 OID 891744)
-- Name: idx_890271_idx_name_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890271_idx_name_unique ON vaxiom.jobtitle_group USING btree (name, eeo_id);


--
-- TOC entry 7181 (class 1259 OID 891735)
-- Name: idx_890277_fk_legal_entity_has_enrollment_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890277_fk_legal_entity_has_enrollment_enrollment1_idx ON vaxiom.legal_entities_participating_enrollment USING btree (enrollment_id);


--
-- TOC entry 7182 (class 1259 OID 891754)
-- Name: idx_890277_fk_legal_entity_has_enrollment_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890277_fk_legal_entity_has_enrollment_legal_entity1_idx ON vaxiom.legal_entities_participating_enrollment USING btree (legal_entity_id);


--
-- TOC entry 7185 (class 1259 OID 891747)
-- Name: idx_890280_fk_employer_sys_organizations1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890280_fk_employer_sys_organizations1_idx ON vaxiom.legal_entity USING btree (sys_organizations_id);


--
-- TOC entry 7186 (class 1259 OID 891746)
-- Name: idx_890280_fk_legal_entity_county1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890280_fk_legal_entity_county1_idx ON vaxiom.legal_entity USING btree (county_id);


--
-- TOC entry 7187 (class 1259 OID 891767)
-- Name: idx_890280_fk_legal_entity_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890280_fk_legal_entity_legal_entity1_idx ON vaxiom.legal_entity USING btree (parent_id);


--
-- TOC entry 7190 (class 1259 OID 891808)
-- Name: idx_890290_fk_legal_entity_address_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890290_fk_legal_entity_address_legal_entity1_idx ON vaxiom.legal_entity_address USING btree (legal_entity_id);


--
-- TOC entry 7193 (class 1259 OID 891749)
-- Name: idx_890293_fk_legal_entity_address_attachment_legal_entity_addr; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890293_fk_legal_entity_address_attachment_legal_entity_addr ON vaxiom.legal_entity_address_attachment USING btree (legal_entity_address_id);


--
-- TOC entry 7196 (class 1259 OID 891745)
-- Name: idx_890299_fk_legal_entity_billing_group_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890299_fk_legal_entity_billing_group_legal_entity1_idx ON vaxiom.legal_entity_billing_group USING btree (legal_entity_id);


--
-- TOC entry 7199 (class 1259 OID 891760)
-- Name: idx_890302_fk_legal_entity_billing_group_has_sys_organizations_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890302_fk_legal_entity_billing_group_has_sys_organizations_ ON vaxiom.legal_entity_billing_group_has_sys_organizations USING btree (sys_organizations_id);


--
-- TOC entry 7202 (class 1259 OID 891777)
-- Name: idx_890305_fk_legal_entity_dba_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890305_fk_legal_entity_dba_legal_entity1_idx ON vaxiom.legal_entity_dba USING btree (legal_entity_id);


--
-- TOC entry 7205 (class 1259 OID 891818)
-- Name: idx_890308_fk_legal_entity_insurance_benefit_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890308_fk_legal_entity_insurance_benefit_legal_entity1_idx ON vaxiom.legal_entity_insurance_benefit USING btree (benefits_holder);


--
-- TOC entry 7206 (class 1259 OID 891764)
-- Name: idx_890308_fk_legal_entity_insurance_benefit_legal_entity2_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890308_fk_legal_entity_insurance_benefit_legal_entity2_idx ON vaxiom.legal_entity_insurance_benefit USING btree (legal_entity_id);


--
-- TOC entry 7207 (class 1259 OID 891759)
-- Name: idx_890308_fk_legal_entity_insurance_benefit_sys_users1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890308_fk_legal_entity_insurance_benefit_sys_users1_idx ON vaxiom.legal_entity_insurance_benefit USING btree (broker_id);


--
-- TOC entry 7210 (class 1259 OID 891757)
-- Name: idx_890311_fk_legal_entity_insurance_benefit_has_sys_organizati; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890311_fk_legal_entity_insurance_benefit_has_sys_organizati ON vaxiom.legal_entity_insurance_benefit_has_division USING btree (legal_entity_insurance_benefit_id);


--
-- TOC entry 7213 (class 1259 OID 891770)
-- Name: idx_890314_fk_legal_entity_npi_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890314_fk_legal_entity_npi_legal_entity1_idx ON vaxiom.legal_entity_npi USING btree (legal_entity_id);


--
-- TOC entry 7216 (class 1259 OID 891788)
-- Name: idx_890317_fk_legal_entity_operational_structures_legal_entity1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890317_fk_legal_entity_operational_structures_legal_entity1 ON vaxiom.legal_entity_operational_structures USING btree (legal_entity_id);


--
-- TOC entry 7219 (class 1259 OID 891828)
-- Name: idx_890320_fk_legal_entity_owner_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890320_fk_legal_entity_owner_legal_entity1_idx ON vaxiom.legal_entity_owner USING btree (legal_entity_id);


--
-- TOC entry 7222 (class 1259 OID 891769)
-- Name: idx_890323_legal_entity_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890323_legal_entity_id ON vaxiom.legal_entity_tin USING btree (legal_entity_id);


--
-- TOC entry 7223 (class 1259 OID 891775)
-- Name: idx_890323_legal_entity_id_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890323_legal_entity_id_idx ON vaxiom.legal_entity_tin USING btree (id);


--
-- TOC entry 7226 (class 1259 OID 891793)
-- Name: idx_890326_booking_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890326_booking_id ON vaxiom.lightbarcalls USING btree (booking_id);


--
-- TOC entry 7227 (class 1259 OID 891782)
-- Name: idx_890326_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890326_chair_id ON vaxiom.lightbarcalls USING btree (chair_id);


--
-- TOC entry 7230 (class 1259 OID 891776)
-- Name: idx_890326_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890326_resource_id ON vaxiom.lightbarcalls USING btree (resource_id);


--
-- TOC entry 7231 (class 1259 OID 891798)
-- Name: idx_890329_access_key; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890329_access_key ON vaxiom.location_access_keys USING btree (access_key);


--
-- TOC entry 7232 (class 1259 OID 891805)
-- Name: idx_890329_organization_id_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890329_organization_id_unique ON vaxiom.location_access_keys USING btree (organization_id);


--
-- TOC entry 7235 (class 1259 OID 891784)
-- Name: idx_890335_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890335_patient_id ON vaxiom.medical_history USING btree (patient_id);


--
-- TOC entry 7238 (class 1259 OID 891786)
-- Name: idx_890430_medical_history_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890430_medical_history_id ON vaxiom.medical_history_medications USING btree (medical_history_id);


--
-- TOC entry 7239 (class 1259 OID 891781)
-- Name: idx_890430_medication_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890430_medication_id ON vaxiom.medical_history_medications USING btree (medication_id);


--
-- TOC entry 7242 (class 1259 OID 891792)
-- Name: idx_890435_fk_medicalhistoryid_medicalhistory; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890435_fk_medicalhistoryid_medicalhistory ON vaxiom.medical_history_patient_relatives_crowding USING btree (medical_history_id);


--
-- TOC entry 7247 (class 1259 OID 891809)
-- Name: idx_890443_check_date_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890443_check_date_index ON vaxiom.merchant_reports USING btree (check_date);


--
-- TOC entry 7248 (class 1259 OID 891814)
-- Name: idx_890443_fk_merchant_reports_sys_organizations; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890443_fk_merchant_reports_sys_organizations ON vaxiom.merchant_reports USING btree (location_id);


--
-- TOC entry 7249 (class 1259 OID 891846)
-- Name: idx_890443_merchant_report_success; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890443_merchant_report_success ON vaxiom.merchant_reports USING btree (success);


--
-- TOC entry 7252 (class 1259 OID 891791)
-- Name: idx_890449_answerguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890449_answerguid ON vaxiom.migrated_answers USING btree (answer_guid);


--
-- TOC entry 7253 (class 1259 OID 891797)
-- Name: idx_890449_answerid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890449_answerid ON vaxiom.migrated_answers USING btree (answer_id);


--
-- TOC entry 7256 (class 1259 OID 891815)
-- Name: idx_890449_questionguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890449_questionguid ON vaxiom.migrated_answers USING btree (question_guid);


--
-- TOC entry 7257 (class 1259 OID 891803)
-- Name: idx_890452_guidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890452_guidindex ON vaxiom.migrated_appointments USING btree (appointment_guid);


--
-- TOC entry 7258 (class 1259 OID 891802)
-- Name: idx_890452_idindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890452_idindex ON vaxiom.migrated_appointments USING btree (appointment_id);


--
-- TOC entry 7259 (class 1259 OID 891796)
-- Name: idx_890455_fieldguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890455_fieldguidindex ON vaxiom.migrated_appointment_fields USING btree (fields_guid);


--
-- TOC entry 7260 (class 1259 OID 891820)
-- Name: idx_890455_fieldidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890455_fieldidindex ON vaxiom.migrated_appointment_fields USING btree (appointment_id);


--
-- TOC entry 7261 (class 1259 OID 891825)
-- Name: idx_890458_typeguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890458_typeguidindex ON vaxiom.migrated_appointment_types USING btree (appointment_type_guid);


--
-- TOC entry 7262 (class 1259 OID 891858)
-- Name: idx_890458_typeidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890458_typeidindex ON vaxiom.migrated_appointment_types USING btree (appointment_type_id);


--
-- TOC entry 7263 (class 1259 OID 891806)
-- Name: idx_890461_chairguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890461_chairguidindex ON vaxiom.migrated_chairs USING btree (chair_guid);


--
-- TOC entry 7264 (class 1259 OID 891800)
-- Name: idx_890461_chairidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890461_chairidindex ON vaxiom.migrated_chairs USING btree (chair_id);


--
-- TOC entry 7265 (class 1259 OID 891816)
-- Name: idx_890464_claimguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890464_claimguid ON vaxiom.migrated_claim USING btree (claim_guid);


--
-- TOC entry 7266 (class 1259 OID 891801)
-- Name: idx_890470_employeeguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890470_employeeguidindex ON vaxiom.migrated_employees USING btree (employee_guid);


--
-- TOC entry 7267 (class 1259 OID 891824)
-- Name: idx_890470_employeeidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890470_employeeidindex ON vaxiom.migrated_employees USING btree (employee_id);


--
-- TOC entry 7268 (class 1259 OID 891813)
-- Name: idx_890473_companyguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890473_companyguidindex ON vaxiom.migrated_insurance_companies USING btree (company_guid);


--
-- TOC entry 7269 (class 1259 OID 891811)
-- Name: idx_890473_companyidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890473_companyidindex ON vaxiom.migrated_insurance_companies USING btree (company_id);


--
-- TOC entry 7270 (class 1259 OID 891807)
-- Name: idx_890476_planguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890476_planguid ON vaxiom.migrated_insurance_plans USING btree (plan_guid);


--
-- TOC entry 7271 (class 1259 OID 891830)
-- Name: idx_890476_planid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890476_planid ON vaxiom.migrated_insurance_plans USING btree (plan_id);


--
-- TOC entry 7272 (class 1259 OID 891840)
-- Name: idx_890479_locationguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890479_locationguid ON vaxiom.migrated_locations USING btree (location_guid);


--
-- TOC entry 7273 (class 1259 OID 891867)
-- Name: idx_890479_locationid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890479_locationid ON vaxiom.migrated_locations USING btree (location_id);


--
-- TOC entry 7274 (class 1259 OID 891817)
-- Name: idx_890482_patientguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890482_patientguidindex ON vaxiom.migrated_patients USING btree (patient_guid);


--
-- TOC entry 7275 (class 1259 OID 891810)
-- Name: idx_890485_commentguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890485_commentguidindex ON vaxiom.migrated_patients_comments USING btree (comment_guid);


--
-- TOC entry 7276 (class 1259 OID 891829)
-- Name: idx_890485_commentidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890485_commentidindex ON vaxiom.migrated_patients_comments USING btree (comment_id);


--
-- TOC entry 7279 (class 1259 OID 891835)
-- Name: idx_890491_patientanswerguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890491_patientanswerguid ON vaxiom.migrated_patient_answers USING btree (patient_answer_guid);


--
-- TOC entry 7280 (class 1259 OID 891823)
-- Name: idx_890491_patientanswerid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890491_patientanswerid ON vaxiom.migrated_patient_answers USING btree (patient_answer_id);


--
-- TOC entry 7285 (class 1259 OID 891841)
-- Name: idx_890497_imageseriesguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890497_imageseriesguidindex ON vaxiom.migrated_patient_image_series USING btree (image_series_guid);


--
-- TOC entry 7286 (class 1259 OID 891850)
-- Name: idx_890497_imageseriesidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890497_imageseriesidindex ON vaxiom.migrated_patient_image_series USING btree (image_series_id);


--
-- TOC entry 7289 (class 1259 OID 891827)
-- Name: idx_890500_paymentguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890500_paymentguidindex ON vaxiom.migrated_payment_accounts USING btree (payment_guid);


--
-- TOC entry 7290 (class 1259 OID 891822)
-- Name: idx_890500_paymentidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890500_paymentidindex ON vaxiom.migrated_payment_accounts USING btree (payment_id);


--
-- TOC entry 7291 (class 1259 OID 891839)
-- Name: idx_890503_migrated_persons_person_guid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890503_migrated_persons_person_guid ON vaxiom.migrated_persons USING btree (person_guid);


--
-- TOC entry 7292 (class 1259 OID 891826)
-- Name: idx_890503_migrated_persons_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890503_migrated_persons_person_id ON vaxiom.migrated_persons USING btree (person_id);


--
-- TOC entry 7293 (class 1259 OID 891843)
-- Name: idx_890503_person_guid_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890503_person_guid_index ON vaxiom.migrated_persons USING btree (person_guid);


--
-- TOC entry 7294 (class 1259 OID 891833)
-- Name: idx_890503_person_id_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890503_person_id_index ON vaxiom.migrated_persons USING btree (person_id);


--
-- TOC entry 7295 (class 1259 OID 891832)
-- Name: idx_890506_patientguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890506_patientguid ON vaxiom.migrated_questionnaire USING btree (patient_guid);


--
-- TOC entry 7298 (class 1259 OID 891852)
-- Name: idx_890506_questionnaireguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890506_questionnaireguid ON vaxiom.migrated_questionnaire USING btree (questionnaire_guid);


--
-- TOC entry 7299 (class 1259 OID 891859)
-- Name: idx_890506_questionnaireid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890506_questionnaireid ON vaxiom.migrated_questionnaire USING btree (questionnaire_id);


--
-- TOC entry 7300 (class 1259 OID 891890)
-- Name: idx_890506_tx_card_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890506_tx_card_id ON vaxiom.migrated_questionnaire USING btree (tx_card_id);


--
-- TOC entry 7301 (class 1259 OID 891836)
-- Name: idx_890509_patient_guid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890509_patient_guid ON vaxiom.migrated_questions USING btree (patient_guid);


--
-- TOC entry 7304 (class 1259 OID 891837)
-- Name: idx_890509_question_guid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890509_question_guid ON vaxiom.migrated_questions USING btree (question_guid);


--
-- TOC entry 7305 (class 1259 OID 891854)
-- Name: idx_890509_question_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890509_question_id ON vaxiom.migrated_questions USING btree (question_id);


--
-- TOC entry 7306 (class 1259 OID 891849)
-- Name: idx_890509_questionnaire_guid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890509_questionnaire_guid ON vaxiom.migrated_questions USING btree (questionnaire_guid);


--
-- TOC entry 7309 (class 1259 OID 891844)
-- Name: idx_890512_relationguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890512_relationguidindex ON vaxiom.migrated_relations USING btree (relation_guid);


--
-- TOC entry 7312 (class 1259 OID 891863)
-- Name: idx_890515_templateguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890515_templateguid ON vaxiom.migrated_schedule_templates USING btree (template_guid);


--
-- TOC entry 7313 (class 1259 OID 891872)
-- Name: idx_890515_templateid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890515_templateid ON vaxiom.migrated_schedule_templates USING btree (template_id);


--
-- TOC entry 7314 (class 1259 OID 891899)
-- Name: idx_890521_invoiceguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890521_invoiceguidindex ON vaxiom.migrated_transactions USING btree (transaction_guid);


--
-- TOC entry 7315 (class 1259 OID 891848)
-- Name: idx_890521_invoiceidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890521_invoiceidindex ON vaxiom.migrated_transactions USING btree (transaction_id);


--
-- TOC entry 7316 (class 1259 OID 891845)
-- Name: idx_890524_nodeguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890524_nodeguid ON vaxiom.migrated_treatment_notes USING btree (note_guid);


--
-- TOC entry 7317 (class 1259 OID 891856)
-- Name: idx_890524_nodeid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890524_nodeid ON vaxiom.migrated_treatment_notes USING btree (note_id);


--
-- TOC entry 7318 (class 1259 OID 891847)
-- Name: idx_890527_txcardguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890527_txcardguidindex ON vaxiom.migrated_txcards USING btree (txcard_guid);


--
-- TOC entry 7319 (class 1259 OID 891864)
-- Name: idx_890527_txcardidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890527_txcardidindex ON vaxiom.migrated_txcards USING btree (txcard_id);


--
-- TOC entry 7322 (class 1259 OID 891861)
-- Name: idx_890530_workscheduleguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890530_workscheduleguid ON vaxiom.migrated_workschedule USING btree (workschedule_guid);


--
-- TOC entry 7323 (class 1259 OID 891853)
-- Name: idx_890530_workscheduleid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890530_workscheduleid ON vaxiom.migrated_workschedule USING btree (workschedule_id);


--
-- TOC entry 7326 (class 1259 OID 891885)
-- Name: idx_890541_loose_search_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890541_loose_search_index ON vaxiom.migration_map USING btree (source_key, source_name, target_table);


--
-- TOC entry 7327 (class 1259 OID 891910)
-- Name: idx_890541_merged_to_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890541_merged_to_id ON vaxiom.migration_map USING btree (merged_to_id);


--
-- TOC entry 7328 (class 1259 OID 891860)
-- Name: idx_890541_migration_map_search_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890541_migration_map_search_index ON vaxiom.migration_map USING btree (source_key, source_type, source_name, target_table);


--
-- TOC entry 7331 (class 1259 OID 891868)
-- Name: idx_890541_source_key; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890541_source_key ON vaxiom.migration_map USING btree (source_key, source_name, target_id, target_table);


--
-- TOC entry 7332 (class 1259 OID 891855)
-- Name: idx_890541_target_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890541_target_id ON vaxiom.migration_map USING btree (target_id);


--
-- TOC entry 7335 (class 1259 OID 891871)
-- Name: idx_890551_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890551_organization_id ON vaxiom.misc_fee_charge_templates USING btree (organization_id);


--
-- TOC entry 7342 (class 1259 OID 891897)
-- Name: idx_890558_report_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890558_report_id ON vaxiom.my_reports_columns USING btree (report_id);


--
-- TOC entry 7345 (class 1259 OID 891874)
-- Name: idx_890561_report_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890561_report_id ON vaxiom.my_reports_filters USING btree (report_id);


--
-- TOC entry 7346 (class 1259 OID 891869)
-- Name: idx_890567_employment_contract_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890567_employment_contract_id ON vaxiom.network_fee_sheets USING btree (employment_contract_id);


--
-- TOC entry 7347 (class 1259 OID 891878)
-- Name: idx_890567_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890567_organization_id ON vaxiom.network_fee_sheets USING btree (organization_id, employment_contract_id);


--
-- TOC entry 7350 (class 1259 OID 891886)
-- Name: idx_890570_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890570_insurance_code_id ON vaxiom.network_sheet_fee USING btree (insurance_code_id);


--
-- TOC entry 7351 (class 1259 OID 891882)
-- Name: idx_890570_network_fee_sheet_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890570_network_fee_sheet_id ON vaxiom.network_sheet_fee USING btree (network_fee_sheet_id);


--
-- TOC entry 7356 (class 1259 OID 891894)
-- Name: idx_890576_notebook_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890576_notebook_id ON vaxiom.notebook_notes USING btree (notebook_id);


--
-- TOC entry 7361 (class 1259 OID 891884)
-- Name: idx_890582_target_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890582_target_id ON vaxiom.notes USING btree (target_id);


--
-- TOC entry 7362 (class 1259 OID 891880)
-- Name: idx_890588_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890588_chair_id ON vaxiom.odt_chairs USING btree (chair_id);


--
-- TOC entry 7363 (class 1259 OID 891889)
-- Name: idx_890588_day_schedule_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890588_day_schedule_id ON vaxiom.odt_chairs USING btree (day_schedule_id);


--
-- TOC entry 7366 (class 1259 OID 891896)
-- Name: idx_890588_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890588_template_id ON vaxiom.odt_chairs USING btree (template_id, chair_id, day_schedule_id, valid_from);


--
-- TOC entry 7367 (class 1259 OID 891893)
-- Name: idx_890592_odt_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890592_odt_chair_id ON vaxiom.odt_chair_allocations USING btree (odt_chair_id, resource_id);


--
-- TOC entry 7370 (class 1259 OID 891888)
-- Name: idx_890592_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890592_resource_id ON vaxiom.odt_chair_allocations USING btree (resource_id);


--
-- TOC entry 7375 (class 1259 OID 891941)
-- Name: idx_890607_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890607_location_id ON vaxiom.office_days USING btree (location_id, o_date);


--
-- TOC entry 7378 (class 1259 OID 891891)
-- Name: idx_890607_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890607_template_id ON vaxiom.office_days USING btree (template_id);


--
-- TOC entry 7379 (class 1259 OID 891898)
-- Name: idx_890614_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890614_chair_id ON vaxiom.office_day_chairs USING btree (chair_id);


--
-- TOC entry 7380 (class 1259 OID 891887)
-- Name: idx_890614_odt_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890614_odt_chair_id ON vaxiom.office_day_chairs USING btree (odt_chair_id);


--
-- TOC entry 7381 (class 1259 OID 891907)
-- Name: idx_890614_office_day_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890614_office_day_id ON vaxiom.office_day_chairs USING btree (office_day_id, chair_id);


--
-- TOC entry 7384 (class 1259 OID 891903)
-- Name: idx_890617_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890617_location_id ON vaxiom.office_day_templates USING btree (location_id);


--
-- TOC entry 7391 (class 1259 OID 891952)
-- Name: idx_890634_employee_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890634_employee_type_id ON vaxiom.other_professional_relationships USING btree (employee_type_id);


--
-- TOC entry 7392 (class 1259 OID 891906)
-- Name: idx_890634_external_office_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890634_external_office_id ON vaxiom.other_professional_relationships USING btree (external_office_id);


--
-- TOC entry 7393 (class 1259 OID 891902)
-- Name: idx_890634_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890634_person_id ON vaxiom.other_professional_relationships USING btree (person_id);


--
-- TOC entry 7396 (class 1259 OID 891901)
-- Name: idx_890637_heard_from_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890637_heard_from_patient_id ON vaxiom.patients USING btree (heard_from_patient_id);


--
-- TOC entry 7397 (class 1259 OID 891918)
-- Name: idx_890637_heard_from_professional_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890637_heard_from_professional_id ON vaxiom.patients USING btree (heard_from_professional_id);


--
-- TOC entry 7398 (class 1259 OID 891916)
-- Name: idx_890637_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890637_organization_id ON vaxiom.patients USING btree (organization_id, human_id);


--
-- TOC entry 7399 (class 1259 OID 891912)
-- Name: idx_890637_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890637_person_id ON vaxiom.patients USING btree (person_id);


--
-- TOC entry 7402 (class 1259 OID 891925)
-- Name: idx_890644_imageseries_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890644_imageseries_id ON vaxiom.patient_images USING btree (imageseries_id);


--
-- TOC entry 7403 (class 1259 OID 891942)
-- Name: idx_890644_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890644_patient_id ON vaxiom.patient_images USING btree (patient_id);


--
-- TOC entry 7406 (class 1259 OID 891919)
-- Name: idx_890644_slot_key; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890644_slot_key ON vaxiom.patient_images USING btree (slot_key);


--
-- TOC entry 7407 (class 1259 OID 891915)
-- Name: idx_890650_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890650_appointment_id ON vaxiom.patient_imageseries USING btree (appointment_id);


--
-- TOC entry 7408 (class 1259 OID 891917)
-- Name: idx_890650_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890650_patient_id ON vaxiom.patient_imageseries USING btree (patient_id);


--
-- TOC entry 7411 (class 1259 OID 891932)
-- Name: idx_890650_treatment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890650_treatment_id ON vaxiom.patient_imageseries USING btree (treatment_id);


--
-- TOC entry 7412 (class 1259 OID 891928)
-- Name: idx_890654_organization_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890654_organization_node_id ON vaxiom.patient_images_layouts USING btree (organization_node_id);


--
-- TOC entry 7417 (class 1259 OID 891939)
-- Name: idx_890666_insurance_billing_center_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890666_insurance_billing_center_id ON vaxiom.patient_insurance_plans USING btree (insurance_billing_center_id);


--
-- TOC entry 7418 (class 1259 OID 891962)
-- Name: idx_890666_insurance_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890666_insurance_company_id ON vaxiom.patient_insurance_plans USING btree (insurance_company_id);


--
-- TOC entry 7419 (class 1259 OID 891971)
-- Name: idx_890666_master_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890666_master_id ON vaxiom.patient_insurance_plans USING btree (master_id);


--
-- TOC entry 7422 (class 1259 OID 891926)
-- Name: idx_890672_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890672_patient_id ON vaxiom.patient_ledger_history USING btree (patient_id);


--
-- TOC entry 7425 (class 1259 OID 891924)
-- Name: idx_890672_tx_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890672_tx_location_id ON vaxiom.patient_ledger_history USING btree (tx_location_id);


--
-- TOC entry 7426 (class 1259 OID 891943)
-- Name: idx_890678_patient_ledger_history_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890678_patient_ledger_history_id ON vaxiom.patient_ledger_history_overdue_receivables USING btree (patient_ledger_history_id);


--
-- TOC entry 7429 (class 1259 OID 891934)
-- Name: idx_890681_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890681_location_id ON vaxiom.patient_locations USING btree (location_id);


--
-- TOC entry 7432 (class 1259 OID 891951)
-- Name: idx_890684_patient_person_referrals_ibfk_1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890684_patient_person_referrals_ibfk_1 ON vaxiom.patient_person_referrals USING btree (patient_id);


--
-- TOC entry 7433 (class 1259 OID 891978)
-- Name: idx_890684_patient_person_referrals_ibfk_2; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890684_patient_person_referrals_ibfk_2 ON vaxiom.patient_person_referrals USING btree (person_id);


--
-- TOC entry 7436 (class 1259 OID 891940)
-- Name: idx_890687_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890687_patient_id ON vaxiom.patient_recently_opened_items USING btree (patient_id);


--
-- TOC entry 7439 (class 1259 OID 891936)
-- Name: idx_890690_patient_template_referrals_ibfk_1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890690_patient_template_referrals_ibfk_1 ON vaxiom.patient_template_referrals USING btree (patient_id);


--
-- TOC entry 7440 (class 1259 OID 891935)
-- Name: idx_890690_patient_template_referrals_ibfk_3; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890690_patient_template_referrals_ibfk_3 ON vaxiom.patient_template_referrals USING btree (referral_template_id);


--
-- TOC entry 7443 (class 1259 OID 891949)
-- Name: idx_890696_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_account_id ON vaxiom.payments USING btree (account_id);


--
-- TOC entry 7444 (class 1259 OID 891945)
-- Name: idx_890696_applied_payment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_applied_payment_id ON vaxiom.payments USING btree (applied_payment_id);


--
-- TOC entry 7445 (class 1259 OID 891944)
-- Name: idx_890696_billing_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_billing_address_id ON vaxiom.payments USING btree (billing_address_id);


--
-- TOC entry 7446 (class 1259 OID 891963)
-- Name: idx_890696_fk_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_fk_patient_id ON vaxiom.payments USING btree (patient_id);


--
-- TOC entry 7447 (class 1259 OID 891993)
-- Name: idx_890696_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_organization_id ON vaxiom.payments USING btree (organization_id);


--
-- TOC entry 7448 (class 1259 OID 891950)
-- Name: idx_890696_payment_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_payment_type_id ON vaxiom.payments USING btree (payment_type_id);


--
-- TOC entry 7449 (class 1259 OID 891990)
-- Name: idx_890696_payments_ibfk_8; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_payments_ibfk_8 ON vaxiom.payments USING btree (insurance_company_payment_id);


--
-- TOC entry 7452 (class 1259 OID 891947)
-- Name: idx_890696_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_receivable_id ON vaxiom.payments USING btree (receivable_id);


--
-- TOC entry 7453 (class 1259 OID 891946)
-- Name: idx_890696_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_sys_created_at ON vaxiom.payments USING btree (sys_created_at);


--
-- TOC entry 7454 (class 1259 OID 891964)
-- Name: idx_890703_account_type; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890703_account_type ON vaxiom.payment_accounts USING btree (account_type);


--
-- TOC entry 7457 (class 1259 OID 891960)
-- Name: idx_890707_correction_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890707_correction_type_id ON vaxiom.payment_correction_details USING btree (correction_type_id);


--
-- TOC entry 7460 (class 1259 OID 891973)
-- Name: idx_890707_transaction_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890707_transaction_id ON vaxiom.payment_correction_details USING btree (transaction_id);


--
-- TOC entry 7463 (class 1259 OID 892000)
-- Name: idx_890716_bank_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890716_bank_account_id ON vaxiom.payment_plans USING btree (bank_account_id);


--
-- TOC entry 7464 (class 1259 OID 891958)
-- Name: idx_890716_invoice_contact_method_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890716_invoice_contact_method_fk ON vaxiom.payment_plans USING btree (invoice_contact_method_id);


--
-- TOC entry 7467 (class 1259 OID 891955)
-- Name: idx_890716_token_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890716_token_location_id ON vaxiom.payment_plans USING btree (token_location_id);


--
-- TOC entry 7468 (class 1259 OID 891956)
-- Name: idx_890724_payment_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890724_payment_plan_id ON vaxiom.payment_plan_rollbacks USING btree (payment_plan_id);


--
-- TOC entry 7471 (class 1259 OID 891970)
-- Name: idx_890727_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890727_name ON vaxiom.payment_types USING btree (name, parent_company_id);


--
-- TOC entry 7472 (class 1259 OID 891972)
-- Name: idx_890727_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890727_parent_company_id ON vaxiom.payment_types USING btree (parent_company_id);


--
-- TOC entry 7475 (class 1259 OID 891986)
-- Name: idx_890731_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890731_organization_id ON vaxiom.payment_types_locations USING btree (organization_id);


--
-- TOC entry 7478 (class 1259 OID 892009)
-- Name: idx_890734_permissions_ibfk_1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890734_permissions_ibfk_1_idx ON vaxiom.permissions_permission USING btree (permission_group_id);


--
-- TOC entry 7483 (class 1259 OID 891969)
-- Name: idx_890740_department_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890740_department_fk ON vaxiom.permissions_user_role_departament USING btree (department_id);


--
-- TOC entry 7486 (class 1259 OID 891984)
-- Name: idx_890743_jobtitle_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890743_jobtitle_fk ON vaxiom.permissions_user_role_job_title USING btree (job_title_id);


--
-- TOC entry 7489 (class 1259 OID 891981)
-- Name: idx_890746_permission_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890746_permission_fk ON vaxiom.permissions_user_role_permission USING btree (permissions_permission_id);


--
-- TOC entry 7492 (class 1259 OID 891999)
-- Name: idx_890746_user_role_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890746_user_role_fk ON vaxiom.permissions_user_role_permission USING btree (permissions_user_role_id);


--
-- TOC entry 7493 (class 1259 OID 892025)
-- Name: idx_890749_fk_permission_user_role_location_permissions_user_ro; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890749_fk_permission_user_role_location_permissions_user_ro ON vaxiom.permission_user_role_location USING btree (permission_user_role_id);


--
-- TOC entry 7494 (class 1259 OID 892018)
-- Name: idx_890749_fk_permission_user_role_location_sys_organizations; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890749_fk_permission_user_role_location_sys_organizations ON vaxiom.permission_user_role_location USING btree (location_id);


--
-- TOC entry 7497 (class 1259 OID 891975)
-- Name: idx_890752_core_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890752_core_user_id ON vaxiom.persons USING btree (core_user_id);


--
-- TOC entry 7498 (class 1259 OID 891979)
-- Name: idx_890752_fullname_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_fullname_id ON vaxiom.persons USING btree (first_name, last_name);


--
-- TOC entry 7499 (class 1259 OID 891982)
-- Name: idx_890752_global_person_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_global_person_id_fk ON vaxiom.persons USING btree (global_person_id);


--
-- TOC entry 7500 (class 1259 OID 891995)
-- Name: idx_890752_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_organization_id_fk ON vaxiom.persons USING btree (organization_id);


--
-- TOC entry 7501 (class 1259 OID 891994)
-- Name: idx_890752_patient_portal_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_patient_portal_user_id ON vaxiom.persons USING btree (patient_portal_user_id);


--
-- TOC entry 7502 (class 1259 OID 891992)
-- Name: idx_890752_portal_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890752_portal_user_id ON vaxiom.persons USING btree (portal_user_id);


--
-- TOC entry 7505 (class 1259 OID 892010)
-- Name: idx_890752_search_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_search_name ON vaxiom.persons USING gin (to_tsvector('simple'::regconfig, (search_name)::text));


--
-- TOC entry 7506 (class 1259 OID 892035)
-- Name: idx_890759_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890759_person_id ON vaxiom.person_image_files USING btree (person_id);


--
-- TOC entry 7509 (class 1259 OID 891987)
-- Name: idx_890765_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890765_organization_id ON vaxiom.person_payment_accounts USING btree (organization_id, payer_person_id);


--
-- TOC entry 7510 (class 1259 OID 891985)
-- Name: idx_890765_payer_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890765_payer_person_id ON vaxiom.person_payment_accounts USING btree (payer_person_id);


--
-- TOC entry 7513 (class 1259 OID 891991)
-- Name: idx_890768_previous_enrollment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890768_previous_enrollment_id ON vaxiom.previous_enrollment_dependents USING btree (previous_enrollment_id);


--
-- TOC entry 7514 (class 1259 OID 892008)
-- Name: idx_890776_employee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890776_employee_id ON vaxiom.previous_enrollment_form USING btree (employee_id);


--
-- TOC entry 7517 (class 1259 OID 892005)
-- Name: idx_890782_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890782_enrollment_form_id ON vaxiom.previous_enrollment_form_benefits USING btree (enrollment_form_id);


--
-- TOC entry 7520 (class 1259 OID 892019)
-- Name: idx_890789_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890789_insurance_code_id ON vaxiom.procedures USING btree (insurance_code_id);


--
-- TOC entry 7521 (class 1259 OID 892046)
-- Name: idx_890789_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890789_organization_id_fk ON vaxiom.procedures USING btree (organization_id);


--
-- TOC entry 7526 (class 1259 OID 892002)
-- Name: idx_890795_resource_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890795_resource_type_id ON vaxiom.procedure_additional_resource_requirements USING btree (resource_type_id);


--
-- TOC entry 7529 (class 1259 OID 892001)
-- Name: idx_890798_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890798_procedure_id ON vaxiom.procedure_steps USING btree (procedure_id);


--
-- TOC entry 7532 (class 1259 OID 892015)
-- Name: idx_890802_resource_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890802_resource_type_id ON vaxiom.procedure_step_additional_resource_requirements USING btree (resource_type_id);


--
-- TOC entry 7535 (class 1259 OID 892011)
-- Name: idx_890805_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890805_resource_id ON vaxiom.procedure_step_durations USING btree (resource_id);


--
-- TOC entry 7536 (class 1259 OID 892033)
-- Name: idx_890805_step_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890805_step_id ON vaxiom.procedure_step_durations USING btree (step_id);


--
-- TOC entry 7537 (class 1259 OID 892056)
-- Name: idx_890809_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890809_patient_id ON vaxiom.professional_relationships USING btree (patient_id);


--
-- TOC entry 7540 (class 1259 OID 892007)
-- Name: idx_890813_fk_provider_license_employment_contracts1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890813_fk_provider_license_employment_contracts1_idx ON vaxiom.provider_license USING btree (employment_contracts_id);


--
-- TOC entry 7541 (class 1259 OID 892013)
-- Name: idx_890813_fk_provider_license_provider_specialty1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890813_fk_provider_license_provider_specialty1_idx ON vaxiom.provider_license USING btree (provider_specialty_id);


--
-- TOC entry 7544 (class 1259 OID 892012)
-- Name: idx_890816_network_fee_sheet_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890816_network_fee_sheet_id ON vaxiom.provider_network_insurance_companies USING btree (network_fee_sheet_id);


--
-- TOC entry 7551 (class 1259 OID 892021)
-- Name: idx_890822_questionnaireid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890822_questionnaireid ON vaxiom.question USING btree (questionnaire_id);


--
-- TOC entry 7552 (class 1259 OID 892044)
-- Name: idx_890825_patientid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890825_patientid ON vaxiom.questionnaire USING btree (patient_id);


--
-- TOC entry 7555 (class 1259 OID 892062)
-- Name: idx_890825_txcardid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890825_txcardid ON vaxiom.questionnaire USING btree (tx_card_id);


--
-- TOC entry 7556 (class 1259 OID 892017)
-- Name: idx_890828_answer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890828_answer_id ON vaxiom.questionnaire_answers USING btree (answer_id);


--
-- TOC entry 7559 (class 1259 OID 892020)
-- Name: idx_890828_question_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890828_question_id ON vaxiom.questionnaire_answers USING btree (question_id);


--
-- TOC entry 7560 (class 1259 OID 892022)
-- Name: idx_890834_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890834_patient_id ON vaxiom.reachify_users USING btree (patient_id);


--
-- TOC entry 7563 (class 1259 OID 892034)
-- Name: idx_890840_insured_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_insured_id ON vaxiom.receivables USING btree (insured_id);


--
-- TOC entry 7564 (class 1259 OID 892036)
-- Name: idx_890840_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_organization_id ON vaxiom.receivables USING btree (organization_id);


--
-- TOC entry 7565 (class 1259 OID 892031)
-- Name: idx_890840_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890840_parent_company_id ON vaxiom.receivables USING btree (parent_company_id, human_id);


--
-- TOC entry 7566 (class 1259 OID 892057)
-- Name: idx_890840_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_patient_id ON vaxiom.receivables USING btree (patient_id);


--
-- TOC entry 7567 (class 1259 OID 892079)
-- Name: idx_890840_payment_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_payment_account_id ON vaxiom.receivables USING btree (payment_account_id);


--
-- TOC entry 7570 (class 1259 OID 892029)
-- Name: idx_890840_primary_prsn_payer_inv_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_primary_prsn_payer_inv_id ON vaxiom.receivables USING btree (primary_prsn_payer_inv_id);


--
-- TOC entry 7571 (class 1259 OID 892032)
-- Name: idx_890840_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_sys_created_at ON vaxiom.receivables USING btree (sys_created_at);


--
-- TOC entry 7572 (class 1259 OID 892045)
-- Name: idx_890840_treatment_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_treatment_index ON vaxiom.receivables USING btree (treatment_id);


--
-- TOC entry 7579 (class 1259 OID 892047)
-- Name: idx_890852_referral_templates_ibfk_1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890852_referral_templates_ibfk_1 ON vaxiom.referral_templates USING btree (organization_id);


--
-- TOC entry 7580 (class 1259 OID 892041)
-- Name: idx_890852_referral_templates_ibfk_2; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890852_referral_templates_ibfk_2 ON vaxiom.referral_templates USING btree (location_id);


--
-- TOC entry 7585 (class 1259 OID 892091)
-- Name: idx_890862_transaction_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890862_transaction_id ON vaxiom.refund_details USING btree (transaction_id);


--
-- TOC entry 7586 (class 1259 OID 892039)
-- Name: idx_890868_from_person; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890868_from_person ON vaxiom.relationships USING btree (from_person, to_person, type);


--
-- TOC entry 7589 (class 1259 OID 892055)
-- Name: idx_890868_relationships_from_to_person_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890868_relationships_from_to_person_idx ON vaxiom.relationships USING btree (from_person, to_person);


--
-- TOC entry 7590 (class 1259 OID 892040)
-- Name: idx_890868_role; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890868_role ON vaxiom.relationships USING btree (role);


--
-- TOC entry 7591 (class 1259 OID 892069)
-- Name: idx_890868_to_person; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890868_to_person ON vaxiom.relationships USING btree (to_person);


--
-- TOC entry 7592 (class 1259 OID 892053)
-- Name: idx_890868_uc_relationship; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890868_uc_relationship ON vaxiom.relationships USING btree (from_person, to_person);


--
-- TOC entry 7595 (class 1259 OID 892052)
-- Name: idx_890877_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890877_organization_id_fk ON vaxiom.resources USING btree (organization_id);


--
-- TOC entry 7598 (class 1259 OID 892108)
-- Name: idx_890877_resource_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890877_resource_type_id ON vaxiom.resources USING btree (resource_type_id);


--
-- TOC entry 7599 (class 1259 OID 892100)
-- Name: idx_890877_resources_ifbk_3; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890877_resources_ifbk_3 ON vaxiom.resources USING btree (permissions_user_role_id);


--
-- TOC entry 7600 (class 1259 OID 892051)
-- Name: idx_890881_abbreviation; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890881_abbreviation ON vaxiom.resource_types USING btree (abbreviation, dtype);


--
-- TOC entry 7601 (class 1259 OID 892054)
-- Name: idx_890881_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890881_name ON vaxiom.resource_types USING btree (name, dtype);


--
-- TOC entry 7606 (class 1259 OID 892077)
-- Name: idx_890890_retail_item_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890890_retail_item_id ON vaxiom.retail_fees USING btree (retail_item_id);


--
-- TOC entry 7607 (class 1259 OID 892063)
-- Name: idx_890893_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890893_organization_id ON vaxiom.retail_items USING btree (organization_id);


--
-- TOC entry 7614 (class 1259 OID 892110)
-- Name: idx_890903_scheduled_task_ibfk_1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890903_scheduled_task_ibfk_1 ON vaxiom.scheduled_task USING btree (parent_company_id);


--
-- TOC entry 7615 (class 1259 OID 892121)
-- Name: idx_890907_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890907_location_id ON vaxiom.schedule_conflict_permission_bypass USING btree (location_id);


--
-- TOC entry 7618 (class 1259 OID 892064)
-- Name: idx_890910_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890910_chair_id ON vaxiom.schedule_notes USING btree (chair_id);


--
-- TOC entry 7619 (class 1259 OID 892075)
-- Name: idx_890910_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890910_organization_id_fk ON vaxiom.schedule_notes USING btree (organization_id);


--
-- TOC entry 7622 (class 1259 OID 892087)
-- Name: idx_890915_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890915_patient_id ON vaxiom.scheduling_preferences USING btree (patient_id);


--
-- TOC entry 7625 (class 1259 OID 892081)
-- Name: idx_890918_scheduling_preferences_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890918_scheduling_preferences_id ON vaxiom.scheduling_preferences_week_days USING btree (scheduling_preferences_id);


--
-- TOC entry 7626 (class 1259 OID 892072)
-- Name: idx_890921_employee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890921_employee_id ON vaxiom.scheduling_preferred_employees USING btree (employee_id);


--
-- TOC entry 7631 (class 1259 OID 892130)
-- Name: idx_890924_schema_version_ir_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890924_schema_version_ir_idx ON vaxiom.schema_version USING btree (installed_rank);


--
-- TOC entry 7632 (class 1259 OID 892086)
-- Name: idx_890924_schema_version_s_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890924_schema_version_s_idx ON vaxiom.schema_version USING btree (success);


--
-- TOC entry 7633 (class 1259 OID 892074)
-- Name: idx_890924_schema_version_vr_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890924_schema_version_vr_idx ON vaxiom.schema_version USING btree (version_rank);


--
-- TOC entry 7634 (class 1259 OID 892085)
-- Name: idx_890931_appointment_template_id_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890931_appointment_template_id_idx ON vaxiom.selected_appointment_templates USING btree (appointment_template_id);


--
-- TOC entry 7635 (class 1259 OID 892071)
-- Name: idx_890931_location_id_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890931_location_id_idx ON vaxiom.selected_appointment_templates USING btree (location_id);


--
-- TOC entry 7636 (class 1259 OID 892097)
-- Name: idx_890931_location_id_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890931_location_id_unique ON vaxiom.selected_appointment_templates USING btree (user_id, location_id, appointment_template_id);


--
-- TOC entry 7639 (class 1259 OID 892092)
-- Name: idx_890934_fk_appointment_template; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890934_fk_appointment_template ON vaxiom.selected_appointment_templates_office_day_calendar_view USING btree (appointment_template_id);


--
-- TOC entry 7640 (class 1259 OID 892082)
-- Name: idx_890934_fk_location_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890934_fk_location_idx ON vaxiom.selected_appointment_templates_office_day_calendar_view USING btree (location_id);


--
-- TOC entry 7641 (class 1259 OID 892111)
-- Name: idx_890934_fk_user_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890934_fk_user_idx ON vaxiom.selected_appointment_templates_office_day_calendar_view USING btree (user_id);


--
-- TOC entry 7644 (class 1259 OID 892139)
-- Name: idx_890937_location_access_key_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890937_location_access_key_id ON vaxiom.selfcheckin_settings USING btree (location_access_key_id);


--
-- TOC entry 7645 (class 1259 OID 892098)
-- Name: idx_890940_appointment_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890940_appointment_template_id ON vaxiom.selfcheckin_settings_forbidden_types USING btree (appointment_template_id);


--
-- TOC entry 7646 (class 1259 OID 892084)
-- Name: idx_890940_location_access_key_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890940_location_access_key_id ON vaxiom.selfcheckin_settings_forbidden_types USING btree (location_access_key_id, appointment_template_id);


--
-- TOC entry 7649 (class 1259 OID 892080)
-- Name: idx_890943_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890943_resource_id ON vaxiom.simultaneous_appointment_values USING btree (resource_id, type_id);


--
-- TOC entry 7650 (class 1259 OID 892106)
-- Name: idx_890943_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890943_type_id ON vaxiom.simultaneous_appointment_values USING btree (type_id);


--
-- TOC entry 7655 (class 1259 OID 892093)
-- Name: idx_890952_provider_message_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890952_provider_message_id ON vaxiom.sms_part USING btree (provider_message_id);


--
-- TOC entry 7656 (class 1259 OID 892124)
-- Name: idx_890952_sms_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890952_sms_id ON vaxiom.sms_part USING btree (sms_id);


--
-- TOC entry 7657 (class 1259 OID 892141)
-- Name: idx_890958_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890958_organization_id ON vaxiom.statements USING btree (organization_id);


--
-- TOC entry 7658 (class 1259 OID 892149)
-- Name: idx_890958_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890958_patient_id ON vaxiom.statements USING btree (patient_id);


--
-- TOC entry 7665 (class 1259 OID 892090)
-- Name: idx_890976_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890976_name ON vaxiom.sys_organizations USING btree (name, parent_id);


--
-- TOC entry 7666 (class 1259 OID 892120)
-- Name: idx_890976_parent_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890976_parent_id ON vaxiom.sys_organizations USING btree (parent_id);


--
-- TOC entry 7667 (class 1259 OID 892103)
-- Name: idx_890976_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890976_postal_address_id ON vaxiom.sys_organizations USING btree (postal_address_id);


--
-- TOC entry 7670 (class 1259 OID 892105)
-- Name: idx_890983_fk_sys_organisation_address_contact_postal_addresses; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890983_fk_sys_organisation_address_contact_postal_addresses ON vaxiom.sys_organization_address USING btree (contact_postal_addresses_id);


--
-- TOC entry 7673 (class 1259 OID 892152)
-- Name: idx_890986_fk_sys_organisation_address_has_sys_organizations_sy; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890986_fk_sys_organisation_address_has_sys_organizations_sy ON vaxiom.sys_organization_address_has_sys_organizations USING btree (sys_organizations_id);


--
-- TOC entry 7676 (class 1259 OID 892104)
-- Name: idx_890989_fk_sys_organization_attachment_sys_organizations1_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890989_fk_sys_organization_attachment_sys_organizations1_id ON vaxiom.sys_organization_attachment USING btree (sys_organizations_id);


--
-- TOC entry 7679 (class 1259 OID 892102)
-- Name: idx_890995_fk_sys_organisation_contact_method_contact_methods1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890995_fk_sys_organisation_contact_method_contact_methods1_ ON vaxiom.sys_organization_contact_method USING btree (contact_methods_id);


--
-- TOC entry 7680 (class 1259 OID 892129)
-- Name: idx_890995_fk_sys_organisation_contact_method_sys_organizations; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890995_fk_sys_organisation_contact_method_sys_organizations ON vaxiom.sys_organization_contact_method USING btree (sys_organizations_id);


--
-- TOC entry 7683 (class 1259 OID 892122)
-- Name: idx_890999_fk_sys_organisation_departament_data_sys_organizatio; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890999_fk_sys_organisation_departament_data_sys_organizatio ON vaxiom.sys_organization_department_data USING btree (sys_organizations_id);


--
-- TOC entry 7686 (class 1259 OID 892143)
-- Name: idx_891002_fk_sys_organisation_devision_data_sys_organizations1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891002_fk_sys_organisation_devision_data_sys_organizations1 ON vaxiom.sys_organization_division_data USING btree (sys_organizations_id);


--
-- TOC entry 7687 (class 1259 OID 892162)
-- Name: idx_891002_fk_sys_organization_division_data_legal_entity_npi1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891002_fk_sys_organization_division_data_legal_entity_npi1_ ON vaxiom.sys_organization_division_data USING btree (legal_entity_npi_id);


--
-- TOC entry 7690 (class 1259 OID 892113)
-- Name: idx_891005_fk_sys_organizations_has_sys_organizations_sys_organ; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891005_fk_sys_organizations_has_sys_organizations_sys_organ ON vaxiom.sys_organization_division_has_department USING btree (division_tree_node_id);


--
-- TOC entry 7693 (class 1259 OID 892109)
-- Name: idx_891005_uk_sys_organization_division_has_department; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891005_uk_sys_organization_division_has_department ON vaxiom.sys_organization_division_has_department USING btree (division_tree_node_id, department_id);


--
-- TOC entry 7694 (class 1259 OID 892138)
-- Name: idx_891008_fk_sys_organization_location_group_sys_organizations; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891008_fk_sys_organization_location_group_sys_organizations ON vaxiom.sys_organization_location_group USING btree (sys_organizations_id);


--
-- TOC entry 7697 (class 1259 OID 892127)
-- Name: idx_891011_fk_sys_organization_location_group_has_sys_organizat; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891011_fk_sys_organization_location_group_has_sys_organizat ON vaxiom.sys_organization_location_group_has_sys_organizations USING btree (sys_organization_location_group_id);


--
-- TOC entry 7700 (class 1259 OID 892173)
-- Name: idx_891014_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891014_organization_id ON vaxiom.sys_organization_settings USING btree (organization_id);


--
-- TOC entry 7703 (class 1259 OID 892137)
-- Name: idx_891020_login_email; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891020_login_email ON vaxiom.sys_patient_portal_users USING btree (login_email);


--
-- TOC entry 7706 (class 1259 OID 892146)
-- Name: idx_891020_token; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891020_token ON vaxiom.sys_patient_portal_users USING btree (token);


--
-- TOC entry 7707 (class 1259 OID 892118)
-- Name: idx_891032_login_email; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891032_login_email ON vaxiom.sys_portal_users USING btree (login_email);


--
-- TOC entry 7710 (class 1259 OID 892134)
-- Name: idx_891032_token; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891032_token ON vaxiom.sys_portal_users USING btree (token);


--
-- TOC entry 7711 (class 1259 OID 892142)
-- Name: idx_891042_login_email; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891042_login_email ON vaxiom.sys_users USING btree (login_email);


--
-- TOC entry 7714 (class 1259 OID 892163)
-- Name: idx_891058_assignee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_assignee_id ON vaxiom.tasks USING btree (assignee_id);


--
-- TOC entry 7715 (class 1259 OID 892184)
-- Name: idx_891058_document_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_document_template_id ON vaxiom.tasks USING btree (document_template_id);


--
-- TOC entry 7716 (class 1259 OID 892189)
-- Name: idx_891058_document_tree_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_document_tree_node_id ON vaxiom.tasks USING btree (document_tree_node_id);


--
-- TOC entry 7717 (class 1259 OID 892148)
-- Name: idx_891058_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_organization_id ON vaxiom.tasks USING btree (organization_id);


--
-- TOC entry 7718 (class 1259 OID 892135)
-- Name: idx_891058_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_patient_id ON vaxiom.tasks USING btree (patient_id);


--
-- TOC entry 7721 (class 1259 OID 892128)
-- Name: idx_891058_provider_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_provider_id ON vaxiom.tasks USING btree (provider_id);


--
-- TOC entry 7722 (class 1259 OID 892159)
-- Name: idx_891058_recipient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_recipient_id ON vaxiom.tasks USING btree (recipient_id);


--
-- TOC entry 7723 (class 1259 OID 892144)
-- Name: idx_891058_task_basket_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_task_basket_id ON vaxiom.tasks USING btree (task_basket_id);


--
-- TOC entry 7724 (class 1259 OID 892151)
-- Name: idx_891065_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891065_organization_id ON vaxiom.task_baskets USING btree (organization_id);


--
-- TOC entry 7729 (class 1259 OID 892194)
-- Name: idx_891071_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891071_user_id ON vaxiom.task_basket_subscribers USING btree (user_id);


--
-- TOC entry 7730 (class 1259 OID 892202)
-- Name: idx_891074_contact_method_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891074_contact_method_id ON vaxiom.task_contacts USING btree (contact_method_id);


--
-- TOC entry 7733 (class 1259 OID 892145)
-- Name: idx_891077_assignee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891077_assignee_id ON vaxiom.task_events USING btree (assignee_id);


--
-- TOC entry 7736 (class 1259 OID 892140)
-- Name: idx_891077_task_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891077_task_id ON vaxiom.task_events USING btree (task_id);


--
-- TOC entry 7737 (class 1259 OID 892170)
-- Name: idx_891083_organization_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891083_organization_node_id ON vaxiom.temporary_images USING btree (organization_node_id);


--
-- TOC entry 7738 (class 1259 OID 892155)
-- Name: idx_891083_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891083_patient_id ON vaxiom.temporary_images USING btree (patient_id);


--
-- TOC entry 7739 (class 1259 OID 892161)
-- Name: idx_891083_patient_image_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891083_patient_image_id ON vaxiom.temporary_images USING btree (patient_image_id);


--
-- TOC entry 7742 (class 1259 OID 892185)
-- Name: idx_891089_insurance_payer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891089_insurance_payer_id ON vaxiom.temp_accounts USING btree (insurance_payer_id);


--
-- TOC entry 7743 (class 1259 OID 892204)
-- Name: idx_891089_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891089_organization_id ON vaxiom.temp_accounts USING btree (organization_id);


--
-- TOC entry 7744 (class 1259 OID 892213)
-- Name: idx_891089_person_payer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891089_person_payer_id ON vaxiom.temp_accounts USING btree (person_payer_id);


--
-- TOC entry 7747 (class 1259 OID 892157)
-- Name: idx_891089_tx_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891089_tx_plan_id ON vaxiom.temp_accounts USING btree (tx_plan_id);


--
-- TOC entry 7748 (class 1259 OID 892176)
-- Name: idx_891092_organization_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891092_organization_node_id ON vaxiom.third_party_account USING btree (organization_node_id);


--
-- TOC entry 7753 (class 1259 OID 892164)
-- Name: idx_891098_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891098_snapshot_id ON vaxiom.toothchart_edit_log USING btree (snapshot_id);


--
-- TOC entry 7756 (class 1259 OID 892172)
-- Name: idx_891101_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891101_snapshot_id ON vaxiom.toothchart_note USING btree (snapshot_id);


--
-- TOC entry 7757 (class 1259 OID 892199)
-- Name: idx_891104_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891104_appointment_id ON vaxiom.toothchart_snapshot USING btree (appointment_id);


--
-- TOC entry 7762 (class 1259 OID 892182)
-- Name: idx_891110_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891110_snapshot_id ON vaxiom.tooth_marking USING btree (snapshot_id);


--
-- TOC entry 7763 (class 1259 OID 892167)
-- Name: idx_891113_dtype; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891113_dtype ON vaxiom.transactions USING btree (dtype);


--
-- TOC entry 7766 (class 1259 OID 892166)
-- Name: idx_891113_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891113_receivable_id ON vaxiom.transactions USING btree (receivable_id);


--
-- TOC entry 7767 (class 1259 OID 892191)
-- Name: idx_891113_sys_created; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891113_sys_created ON vaxiom.transactions USING btree (sys_created);


--
-- TOC entry 7768 (class 1259 OID 892175)
-- Name: idx_891118_charge_transfer_adjustment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891118_charge_transfer_adjustment_id ON vaxiom.transfer_charges USING btree (charge_transfer_adjustment_id);


--
-- TOC entry 7771 (class 1259 OID 892181)
-- Name: idx_891121_migrated; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_migrated ON vaxiom.txs USING btree (migrated);


--
-- TOC entry 7772 (class 1259 OID 892209)
-- Name: idx_891121_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_organization_id ON vaxiom.txs USING btree (organization_id);


--
-- TOC entry 7775 (class 1259 OID 892234)
-- Name: idx_891121_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_sys_created_at ON vaxiom.txs USING btree (sys_created_at);


--
-- TOC entry 7776 (class 1259 OID 892195)
-- Name: idx_891121_tx_card_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_tx_card_id ON vaxiom.txs USING btree (tx_card_id);


--
-- TOC entry 7777 (class 1259 OID 892179)
-- Name: idx_891121_tx_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_tx_plan_id ON vaxiom.txs USING btree (tx_plan_id);


--
-- TOC entry 7780 (class 1259 OID 892177)
-- Name: idx_891131_treatment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891131_treatment_id ON vaxiom.txs_state_changes USING btree (treatment_id);


--
-- TOC entry 7781 (class 1259 OID 892200)
-- Name: idx_891137_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891137_patient_id ON vaxiom.tx_cards USING btree (patient_id);


--
-- TOC entry 7784 (class 1259 OID 892192)
-- Name: idx_891137_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891137_tx_category_id ON vaxiom.tx_cards USING btree (tx_category_id);


--
-- TOC entry 7785 (class 1259 OID 892193)
-- Name: idx_891141_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891141_organization_id_fk ON vaxiom.tx_card_field_definitions USING btree (organization_id);


--
-- TOC entry 7788 (class 1259 OID 892241)
-- Name: idx_891141_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891141_type_id ON vaxiom.tx_card_field_definitions USING btree (type_id);


--
-- TOC entry 7789 (class 1259 OID 892243)
-- Name: idx_891149_field_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891149_field_type_id ON vaxiom.tx_card_field_options USING btree (field_type_id);


--
-- TOC entry 7796 (class 1259 OID 892186)
-- Name: idx_891162_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891162_tx_category_id ON vaxiom.tx_card_templates USING btree (tx_category_id);


--
-- TOC entry 7797 (class 1259 OID 892211)
-- Name: idx_891162_tx_plan_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891162_tx_plan_template_id ON vaxiom.tx_card_templates USING btree (tx_plan_template_id);


--
-- TOC entry 7800 (class 1259 OID 892203)
-- Name: idx_891168_insurance_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891168_insurance_plan_id ON vaxiom.tx_category_coverages USING btree (insurance_plan_id);


--
-- TOC entry 7803 (class 1259 OID 892231)
-- Name: idx_891168_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891168_tx_category_id ON vaxiom.tx_category_coverages USING btree (tx_category_id);


--
-- TOC entry 7804 (class 1259 OID 892252)
-- Name: idx_891171_insured_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891171_insured_id ON vaxiom.tx_category_insured USING btree (insured_id);


--
-- TOC entry 7807 (class 1259 OID 892215)
-- Name: idx_891171_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891171_tx_category_id ON vaxiom.tx_category_insured USING btree (tx_category_id);


--
-- TOC entry 7810 (class 1259 OID 892220)
-- Name: idx_891174_tx_payer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891174_tx_payer_id ON vaxiom.tx_contracts USING btree (tx_payer_id);


--
-- TOC entry 7811 (class 1259 OID 892198)
-- Name: idx_891180_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891180_insurance_code_id ON vaxiom.tx_fee_charges USING btree (insurance_code_id);


--
-- TOC entry 7814 (class 1259 OID 892207)
-- Name: idx_891180_tx_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891180_tx_plan_id ON vaxiom.tx_fee_charges USING btree (tx_plan_id);


--
-- TOC entry 7815 (class 1259 OID 892214)
-- Name: idx_891183_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_account_id ON vaxiom.tx_payers USING btree (account_id);


--
-- TOC entry 7816 (class 1259 OID 892217)
-- Name: idx_891183_contact_method_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_contact_method_id ON vaxiom.tx_payers USING btree (contact_method_id);


--
-- TOC entry 7817 (class 1259 OID 892239)
-- Name: idx_891183_insured_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_insured_id ON vaxiom.tx_payers USING btree (insured_id);


--
-- TOC entry 7818 (class 1259 OID 892266)
-- Name: idx_891183_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_person_id ON vaxiom.tx_payers USING btree (person_id);


--
-- TOC entry 7821 (class 1259 OID 892226)
-- Name: idx_891183_tx_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_tx_id ON vaxiom.tx_payers USING btree (tx_id);


--
-- TOC entry 7822 (class 1259 OID 892212)
-- Name: idx_891187_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891187_insurance_code_id ON vaxiom.tx_plans USING btree (insurance_code_id);


--
-- TOC entry 7825 (class 1259 OID 892208)
-- Name: idx_891187_tx_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891187_tx_id ON vaxiom.tx_plans USING btree (tx_id);


--
-- TOC entry 7826 (class 1259 OID 892230)
-- Name: idx_891187_tx_plan_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891187_tx_plan_template_id ON vaxiom.tx_plans USING btree (tx_plan_template_id);


--
-- TOC entry 7827 (class 1259 OID 892218)
-- Name: idx_891192_next_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_next_appointment_id ON vaxiom.tx_plan_appointments USING btree (next_appointment_id);


--
-- TOC entry 7830 (class 1259 OID 892229)
-- Name: idx_891192_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_sys_created_at ON vaxiom.tx_plan_appointments USING btree (sys_created_at);


--
-- TOC entry 7831 (class 1259 OID 892249)
-- Name: idx_891192_tx_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_tx_plan_id ON vaxiom.tx_plan_appointments USING btree (tx_plan_id);


--
-- TOC entry 7832 (class 1259 OID 892275)
-- Name: idx_891192_tx_plan_template_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_tx_plan_template_appointment_id ON vaxiom.tx_plan_appointments USING btree (tx_plan_template_appointment_id);


--
-- TOC entry 7833 (class 1259 OID 892280)
-- Name: idx_891192_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_type_id ON vaxiom.tx_plan_appointments USING btree (type_id);


--
-- TOC entry 7836 (class 1259 OID 892223)
-- Name: idx_891197_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891197_procedure_id ON vaxiom.tx_plan_appointment_procedures USING btree (procedure_id);


--
-- TOC entry 7837 (class 1259 OID 892237)
-- Name: idx_891204_pc_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891204_pc_id ON vaxiom.tx_plan_groups USING btree (pc_id);


--
-- TOC entry 7840 (class 1259 OID 892238)
-- Name: idx_891208_diagnosis_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891208_diagnosis_id ON vaxiom.tx_plan_notes USING btree (diagnosis_id);


--
-- TOC entry 7843 (class 1259 OID 892236)
-- Name: idx_891208_tx_card_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891208_tx_card_id ON vaxiom.tx_plan_notes USING btree (tx_card_id);


--
-- TOC entry 7846 (class 1259 OID 892259)
-- Name: idx_891215_tx_plan_note_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891215_tx_plan_note_id ON vaxiom.tx_plan_note_groups USING btree (tx_plan_note_id);


--
-- TOC entry 7847 (class 1259 OID 892283)
-- Name: idx_891222_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891222_insurance_code_id ON vaxiom.tx_plan_templates USING btree (insurance_code_id);


--
-- TOC entry 7848 (class 1259 OID 892281)
-- Name: idx_891222_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891222_organization_id_fk ON vaxiom.tx_plan_templates USING btree (organization_id);


--
-- TOC entry 7851 (class 1259 OID 892235)
-- Name: idx_891222_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891222_tx_category_id ON vaxiom.tx_plan_templates USING btree (tx_category_id);


--
-- TOC entry 7852 (class 1259 OID 892248)
-- Name: idx_891222_tx_plan_templates_ibfk_4; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891222_tx_plan_templates_ibfk_4 ON vaxiom.tx_plan_templates USING btree (tx_plan_group_id);


--
-- TOC entry 7853 (class 1259 OID 892233)
-- Name: idx_891228_next_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891228_next_appointment_id ON vaxiom.tx_plan_template_appointments USING btree (next_appointment_id);


--
-- TOC entry 7856 (class 1259 OID 892244)
-- Name: idx_891228_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891228_sys_created_at ON vaxiom.tx_plan_template_appointments USING btree (sys_created_at);


--
-- TOC entry 7857 (class 1259 OID 892250)
-- Name: idx_891228_tx_plan_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891228_tx_plan_template_id ON vaxiom.tx_plan_template_appointments USING btree (tx_plan_template_id);


--
-- TOC entry 7858 (class 1259 OID 892254)
-- Name: idx_891228_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891228_type_id ON vaxiom.tx_plan_template_appointments USING btree (type_id);


--
-- TOC entry 7861 (class 1259 OID 892286)
-- Name: idx_891233_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891233_procedure_id ON vaxiom.tx_plan_template_appointment_procedures USING btree (procedure_id);


--
-- TOC entry 7864 (class 1259 OID 892262)
-- Name: idx_891240_unique_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891240_unique_index ON vaxiom.tx_statuses USING btree (parent_company_id, type);


--
-- TOC entry 7865 (class 1259 OID 892246)
-- Name: idx_891244_applied_payment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891244_applied_payment_id ON vaxiom.unapplied_payments USING btree (applied_payment_id);


--
-- TOC entry 7868 (class 1259 OID 892245)
-- Name: idx_891253_fri; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_fri ON vaxiom.week_templates USING btree (fri);


--
-- TOC entry 7869 (class 1259 OID 892255)
-- Name: idx_891253_mon; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_mon ON vaxiom.week_templates USING btree (mon);


--
-- TOC entry 7870 (class 1259 OID 892258)
-- Name: idx_891253_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_organization_id_fk ON vaxiom.week_templates USING btree (organization_id);


--
-- TOC entry 7873 (class 1259 OID 892263)
-- Name: idx_891253_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_resource_id ON vaxiom.week_templates USING btree (resource_id);


--
-- TOC entry 7874 (class 1259 OID 892278)
-- Name: idx_891253_sat; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_sat ON vaxiom.week_templates USING btree (sat);


--
-- TOC entry 7875 (class 1259 OID 892284)
-- Name: idx_891253_sun; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_sun ON vaxiom.week_templates USING btree (sun);


--
-- TOC entry 7876 (class 1259 OID 892279)
-- Name: idx_891253_thu; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_thu ON vaxiom.week_templates USING btree (thu);


--
-- TOC entry 7877 (class 1259 OID 892274)
-- Name: idx_891253_tue; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_tue ON vaxiom.week_templates USING btree (tue);


--
-- TOC entry 7878 (class 1259 OID 892260)
-- Name: idx_891253_wed; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_wed ON vaxiom.week_templates USING btree (wed);


--
-- TOC entry 7879 (class 1259 OID 892267)
-- Name: idx_891256_core_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891256_core_user_id ON vaxiom.work_hours USING btree (core_user_id);


--
-- TOC entry 7880 (class 1259 OID 892256)
-- Name: idx_891256_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891256_location_id ON vaxiom.work_hours USING btree (location_id);


--
-- TOC entry 7881 (class 1259 OID 892264)
-- Name: idx_891256_modified_by_admin; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891256_modified_by_admin ON vaxiom.work_hours USING btree (modified_by_admin);


--
-- TOC entry 7884 (class 1259 OID 892272)
-- Name: idx_891260_core_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891260_core_user_id ON vaxiom.work_hour_comments USING btree (core_user_id);


--
-- TOC entry 7887 (class 1259 OID 892285)
-- Name: idx_891260_work_hours_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891260_work_hours_id ON vaxiom.work_hour_comments USING btree (work_hours_id);


--
-- TOC entry 7888 (class 1259 OID 892287)
-- Name: idx_891266_day_schedule_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891266_day_schedule_id ON vaxiom.work_schedule_days USING btree (day_schedule_id);


--
-- TOC entry 7891 (class 1259 OID 892276)
-- Name: idx_891266_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891266_resource_id ON vaxiom.work_schedule_days USING btree (resource_id);


--
-- TOC entry 7892 (class 1259 OID 892271)
-- Name: idx_891266_work_schedule_days_date_resource; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891266_work_schedule_days_date_resource ON vaxiom.work_schedule_days USING btree (ws_date, resource_id);


--
-- TOC entry 8468 (class 2620 OID 895533)
-- Name: act_evt_log on_update_current_timestamp; Type: TRIGGER; Schema: vaxiom; Owner: postgres
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON vaxiom.act_evt_log FOR EACH ROW EXECUTE PROCEDURE vaxiom.on_update_current_timestamp_act_evt_log();


--
-- TOC entry 8469 (class 2620 OID 895535)
-- Name: act_re_deployment on_update_current_timestamp; Type: TRIGGER; Schema: vaxiom; Owner: postgres
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON vaxiom.act_re_deployment FOR EACH ROW EXECUTE PROCEDURE vaxiom.on_update_current_timestamp_act_re_deployment();


--
-- TOC entry 8470 (class 2620 OID 895537)
-- Name: act_ru_task on_update_current_timestamp; Type: TRIGGER; Schema: vaxiom; Owner: postgres
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON vaxiom.act_ru_task FOR EACH ROW EXECUTE PROCEDURE vaxiom.on_update_current_timestamp_act_ru_task();


--
-- TOC entry 7908 (class 2606 OID 892732)
-- Name: act_ru_identitylink act_fk_athrz_procedef; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_identitylink
    ADD CONSTRAINT act_fk_athrz_procedef FOREIGN KEY (proc_def_id_) REFERENCES vaxiom.act_re_procdef(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7897 (class 2606 OID 892677)
-- Name: act_ge_bytearray act_fk_bytearr_depl; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ge_bytearray
    ADD CONSTRAINT act_fk_bytearr_depl FOREIGN KEY (deployment_id_) REFERENCES vaxiom.act_re_deployment(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7903 (class 2606 OID 892707)
-- Name: act_ru_event_subscr act_fk_event_exec; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_event_subscr
    ADD CONSTRAINT act_fk_event_exec FOREIGN KEY (execution_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7904 (class 2606 OID 892712)
-- Name: act_ru_execution act_fk_exe_parent; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT act_fk_exe_parent FOREIGN KEY (parent_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7905 (class 2606 OID 892717)
-- Name: act_ru_execution act_fk_exe_procdef; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT act_fk_exe_procdef FOREIGN KEY (proc_def_id_) REFERENCES vaxiom.act_re_procdef(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7906 (class 2606 OID 892722)
-- Name: act_ru_execution act_fk_exe_procinst; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT act_fk_exe_procinst FOREIGN KEY (proc_inst_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 7907 (class 2606 OID 892727)
-- Name: act_ru_execution act_fk_exe_super; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT act_fk_exe_super FOREIGN KEY (super_exec_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7909 (class 2606 OID 892737)
-- Name: act_ru_identitylink act_fk_idl_procinst; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_identitylink
    ADD CONSTRAINT act_fk_idl_procinst FOREIGN KEY (proc_inst_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7911 (class 2606 OID 892747)
-- Name: act_ru_job act_fk_job_exception; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_job
    ADD CONSTRAINT act_fk_job_exception FOREIGN KEY (exception_stack_id_) REFERENCES vaxiom.act_ge_bytearray(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7898 (class 2606 OID 892682)
-- Name: act_id_membership act_fk_memb_group; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_membership
    ADD CONSTRAINT act_fk_memb_group FOREIGN KEY (group_id_) REFERENCES vaxiom.act_id_group(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7899 (class 2606 OID 892687)
-- Name: act_id_membership act_fk_memb_user; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_membership
    ADD CONSTRAINT act_fk_memb_user FOREIGN KEY (user_id_) REFERENCES vaxiom.act_id_user(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7900 (class 2606 OID 892692)
-- Name: act_re_model act_fk_model_deployment; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_model
    ADD CONSTRAINT act_fk_model_deployment FOREIGN KEY (deployment_id_) REFERENCES vaxiom.act_re_deployment(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7901 (class 2606 OID 892697)
-- Name: act_re_model act_fk_model_source; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_model
    ADD CONSTRAINT act_fk_model_source FOREIGN KEY (editor_source_value_id_) REFERENCES vaxiom.act_ge_bytearray(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7902 (class 2606 OID 892702)
-- Name: act_re_model act_fk_model_source_extra; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_model
    ADD CONSTRAINT act_fk_model_source_extra FOREIGN KEY (editor_source_extra_value_id_) REFERENCES vaxiom.act_ge_bytearray(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7912 (class 2606 OID 892752)
-- Name: act_ru_task act_fk_task_exe; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_task
    ADD CONSTRAINT act_fk_task_exe FOREIGN KEY (execution_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7913 (class 2606 OID 892757)
-- Name: act_ru_task act_fk_task_procdef; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_task
    ADD CONSTRAINT act_fk_task_procdef FOREIGN KEY (proc_def_id_) REFERENCES vaxiom.act_re_procdef(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7914 (class 2606 OID 892762)
-- Name: act_ru_task act_fk_task_procinst; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_task
    ADD CONSTRAINT act_fk_task_procinst FOREIGN KEY (proc_inst_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7910 (class 2606 OID 892742)
-- Name: act_ru_identitylink act_fk_tskass_task; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_identitylink
    ADD CONSTRAINT act_fk_tskass_task FOREIGN KEY (task_id_) REFERENCES vaxiom.act_ru_task(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7915 (class 2606 OID 892767)
-- Name: act_ru_variable act_fk_var_bytearray; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_variable
    ADD CONSTRAINT act_fk_var_bytearray FOREIGN KEY (bytearray_id_) REFERENCES vaxiom.act_ge_bytearray(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7916 (class 2606 OID 892772)
-- Name: act_ru_variable act_fk_var_exe; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_variable
    ADD CONSTRAINT act_fk_var_exe FOREIGN KEY (execution_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7917 (class 2606 OID 892777)
-- Name: act_ru_variable act_fk_var_procinst; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_variable
    ADD CONSTRAINT act_fk_var_procinst FOREIGN KEY (proc_inst_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7895 (class 2606 OID 892667)
-- Name: activiti_process_settings activiti_process_settings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.activiti_process_settings
    ADD CONSTRAINT activiti_process_settings_ibfk_1 FOREIGN KEY (organization_node_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7896 (class 2606 OID 892672)
-- Name: activiti_process_settings_property activiti_process_settings_property_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.activiti_process_settings_property
    ADD CONSTRAINT activiti_process_settings_property_ibfk_1 FOREIGN KEY (activiti_process_settings_id) REFERENCES vaxiom.activiti_process_settings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7918 (class 2606 OID 892782)
-- Name: application_properties application_properties_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.application_properties
    ADD CONSTRAINT application_properties_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7919 (class 2606 OID 892787)
-- Name: applied_payments applied_payments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.applied_payments
    ADD CONSTRAINT applied_payments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7920 (class 2606 OID 892792)
-- Name: applied_payments applied_payments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.applied_payments
    ADD CONSTRAINT applied_payments_ibfk_2 FOREIGN KEY (payment_id) REFERENCES vaxiom.payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7927 (class 2606 OID 892827)
-- Name: appointment_audit_log appointment_audit_log_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_audit_log
    ADD CONSTRAINT appointment_audit_log_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7928 (class 2606 OID 892832)
-- Name: appointment_audit_log appointment_audit_log_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_audit_log
    ADD CONSTRAINT appointment_audit_log_ibfk_2 FOREIGN KEY (id) REFERENCES vaxiom.audit_logs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7935 (class 2606 OID 892867)
-- Name: appointment_booking_resources appointment_booking_resources_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_resources
    ADD CONSTRAINT appointment_booking_resources_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7936 (class 2606 OID 892872)
-- Name: appointment_booking_resources appointment_booking_resources_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_resources
    ADD CONSTRAINT appointment_booking_resources_ibfk_2 FOREIGN KEY (booking_id) REFERENCES vaxiom.appointment_bookings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7937 (class 2606 OID 892877)
-- Name: appointment_booking_state_changes appointment_booking_state_changes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_state_changes
    ADD CONSTRAINT appointment_booking_state_changes_ibfk_1 FOREIGN KEY (booking_id) REFERENCES vaxiom.appointment_bookings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7929 (class 2606 OID 892837)
-- Name: appointment_bookings appointment_bookings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_1 FOREIGN KEY (chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7930 (class 2606 OID 892842)
-- Name: appointment_bookings appointment_bookings_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_2 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7931 (class 2606 OID 892847)
-- Name: appointment_bookings appointment_bookings_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_3 FOREIGN KEY (provider_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7932 (class 2606 OID 892852)
-- Name: appointment_bookings appointment_bookings_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_4 FOREIGN KEY (assistant_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7933 (class 2606 OID 892857)
-- Name: appointment_bookings appointment_bookings_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_5 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7938 (class 2606 OID 892882)
-- Name: appointment_field_values appointment_field_values_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_field_values
    ADD CONSTRAINT appointment_field_values_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7939 (class 2606 OID 892887)
-- Name: appointment_field_values appointment_field_values_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_field_values
    ADD CONSTRAINT appointment_field_values_ibfk_2 FOREIGN KEY (field_option_id) REFERENCES vaxiom.tx_card_field_options(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7940 (class 2606 OID 892892)
-- Name: appointment_field_values appointment_field_values_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_field_values
    ADD CONSTRAINT appointment_field_values_ibfk_3 FOREIGN KEY (field_definition_id) REFERENCES vaxiom.tx_card_field_definitions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7941 (class 2606 OID 892897)
-- Name: appointment_procedures appointment_procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_procedures
    ADD CONSTRAINT appointment_procedures_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7942 (class 2606 OID 892902)
-- Name: appointment_procedures appointment_procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_procedures
    ADD CONSTRAINT appointment_procedures_ibfk_2 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8336 (class 2606 OID 894872)
-- Name: selected_appointment_templates appointment_template_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates
    ADD CONSTRAINT appointment_template_id FOREIGN KEY (appointment_template_id) REFERENCES vaxiom.appointment_templates(id);


--
-- TOC entry 7947 (class 2606 OID 892927)
-- Name: appointment_template_procedures appointment_template_procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_template_procedures
    ADD CONSTRAINT appointment_template_procedures_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7948 (class 2606 OID 892932)
-- Name: appointment_template_procedures appointment_template_procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_template_procedures
    ADD CONSTRAINT appointment_template_procedures_ibfk_2 FOREIGN KEY (appointment_template_id) REFERENCES vaxiom.appointment_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7943 (class 2606 OID 892907)
-- Name: appointment_templates appointment_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT appointment_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7944 (class 2606 OID 892912)
-- Name: appointment_templates appointment_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT appointment_templates_ibfk_2 FOREIGN KEY (appointment_type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7945 (class 2606 OID 892917)
-- Name: appointment_templates appointment_templates_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT appointment_templates_ibfk_3 FOREIGN KEY (primary_provider_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7946 (class 2606 OID 892922)
-- Name: appointment_templates appointment_templates_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT appointment_templates_ibfk_4 FOREIGN KEY (primary_assistant_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7951 (class 2606 OID 892947)
-- Name: appointment_type_daily_restrictions appointment_type_daily_restrictions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_type_daily_restrictions
    ADD CONSTRAINT appointment_type_daily_restrictions_ibfk_1 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7952 (class 2606 OID 892952)
-- Name: appointment_type_daily_restrictions appointment_type_daily_restrictions_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_type_daily_restrictions
    ADD CONSTRAINT appointment_type_daily_restrictions_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7949 (class 2606 OID 892937)
-- Name: appointment_types appointment_types_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_types
    ADD CONSTRAINT appointment_types_ibfk_1 FOREIGN KEY (parent_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7950 (class 2606 OID 892942)
-- Name: appointment_types_property appointment_types_property_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_types_property
    ADD CONSTRAINT appointment_types_property_ibfk_1 FOREIGN KEY (appointment_type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7921 (class 2606 OID 892797)
-- Name: appointments appointments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7922 (class 2606 OID 892802)
-- Name: appointments appointments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7923 (class 2606 OID 892807)
-- Name: appointments appointments_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_3 FOREIGN KEY (next_appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7924 (class 2606 OID 892812)
-- Name: appointments appointments_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_4 FOREIGN KEY (tx_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7925 (class 2606 OID 892817)
-- Name: appointments appointments_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_5 FOREIGN KEY (tx_plan_appointment_id) REFERENCES vaxiom.tx_plan_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7926 (class 2606 OID 892822)
-- Name: appointments appointments_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_6 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7953 (class 2606 OID 892957)
-- Name: appt_type_template_restrictions appt_type_template_restrictions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appt_type_template_restrictions
    ADD CONSTRAINT appt_type_template_restrictions_ibfk_1 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7954 (class 2606 OID 892962)
-- Name: appt_type_template_restrictions appt_type_template_restrictions_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appt_type_template_restrictions
    ADD CONSTRAINT appt_type_template_restrictions_ibfk_2 FOREIGN KEY (template_id) REFERENCES vaxiom.office_day_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7955 (class 2606 OID 892967)
-- Name: audit_logs audit_logs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.audit_logs
    ADD CONSTRAINT audit_logs_ibfk_1 FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7956 (class 2606 OID 892972)
-- Name: autopay_invoice_changes autopay_invoice_changes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.autopay_invoice_changes
    ADD CONSTRAINT autopay_invoice_changes_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7960 (class 2606 OID 892992)
-- Name: bluefin_credentials bluefin_credentials_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.bluefin_credentials
    ADD CONSTRAINT bluefin_credentials_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7961 (class 2606 OID 892997)
-- Name: cancelled_receivables cancelled_receivables_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.cancelled_receivables
    ADD CONSTRAINT cancelled_receivables_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7962 (class 2606 OID 893002)
-- Name: cephx_requests cephx_requests_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.cephx_requests
    ADD CONSTRAINT cephx_requests_ibfk_1 FOREIGN KEY (patient_image_id) REFERENCES vaxiom.patient_images(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7964 (class 2606 OID 893012)
-- Name: chair_allocations chair_allocations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chair_allocations
    ADD CONSTRAINT chair_allocations_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7965 (class 2606 OID 893017)
-- Name: chair_allocations chair_allocations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chair_allocations
    ADD CONSTRAINT chair_allocations_ibfk_2 FOREIGN KEY (chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7963 (class 2606 OID 893007)
-- Name: chairs chairs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chairs
    ADD CONSTRAINT chairs_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7966 (class 2606 OID 893022)
-- Name: charge_transfer_adjustments charge_transfer_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.charge_transfer_adjustments
    ADD CONSTRAINT charge_transfer_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7967 (class 2606 OID 893027)
-- Name: checks checks_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.checks
    ADD CONSTRAINT checks_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7968 (class 2606 OID 893032)
-- Name: checks checks_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.checks
    ADD CONSTRAINT checks_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.check_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7969 (class 2606 OID 893037)
-- Name: claim_events claim_events_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_events
    ADD CONSTRAINT claim_events_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7970 (class 2606 OID 893042)
-- Name: claim_requests claim_requests_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT claim_requests_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7971 (class 2606 OID 893047)
-- Name: claim_requests claim_requests_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT claim_requests_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7972 (class 2606 OID 893052)
-- Name: claim_requests claim_requests_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT claim_requests_ibfk_3 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7973 (class 2606 OID 893057)
-- Name: claim_requests claim_requests_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT claim_requests_ibfk_4 FOREIGN KEY (treatment_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7978 (class 2606 OID 893082)
-- Name: collection_label_agencies collection_label_agencies_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_agencies
    ADD CONSTRAINT collection_label_agencies_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7979 (class 2606 OID 893087)
-- Name: collection_label_history collection_label_history_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT collection_label_history_ibfk_1 FOREIGN KEY (created_by) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7980 (class 2606 OID 893092)
-- Name: collection_label_history collection_label_history_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT collection_label_history_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7981 (class 2606 OID 893097)
-- Name: collection_label_history collection_label_history_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT collection_label_history_ibfk_3 FOREIGN KEY (payment_account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7982 (class 2606 OID 893102)
-- Name: collection_label_history collection_label_history_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT collection_label_history_ibfk_4 FOREIGN KEY (previous_collection_label_template_id) REFERENCES vaxiom.collection_label_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7983 (class 2606 OID 893107)
-- Name: collection_label_templates collection_label_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_templates
    ADD CONSTRAINT collection_label_templates_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7984 (class 2606 OID 893112)
-- Name: collection_label_templates collection_label_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_templates
    ADD CONSTRAINT collection_label_templates_ibfk_2 FOREIGN KEY (agency_id) REFERENCES vaxiom.collection_label_agencies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7974 (class 2606 OID 893062)
-- Name: collection_labels collection_labels_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT collection_labels_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7975 (class 2606 OID 893067)
-- Name: collection_labels collection_labels_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT collection_labels_ibfk_2 FOREIGN KEY (payment_account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7976 (class 2606 OID 893072)
-- Name: collection_labels collection_labels_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT collection_labels_ibfk_3 FOREIGN KEY (collection_label_template_id) REFERENCES vaxiom.collection_label_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7977 (class 2606 OID 893077)
-- Name: collection_labels collection_labels_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT collection_labels_ibfk_4 FOREIGN KEY (last_modified_by) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7985 (class 2606 OID 893117)
-- Name: communication_preferences communication_preferences_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.communication_preferences
    ADD CONSTRAINT communication_preferences_ibfk_1 FOREIGN KEY (contact_method_association_id) REFERENCES vaxiom.contact_method_associations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7986 (class 2606 OID 893122)
-- Name: communication_preferences communication_preferences_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.communication_preferences
    ADD CONSTRAINT communication_preferences_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7987 (class 2606 OID 893127)
-- Name: contact_emails contact_emails_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_emails
    ADD CONSTRAINT contact_emails_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7988 (class 2606 OID 893132)
-- Name: contact_method_associations contact_method_associations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_method_associations
    ADD CONSTRAINT contact_method_associations_ibfk_1 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7989 (class 2606 OID 893137)
-- Name: contact_method_associations contact_method_associations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_method_associations
    ADD CONSTRAINT contact_method_associations_ibfk_2 FOREIGN KEY (contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7990 (class 2606 OID 893142)
-- Name: contact_method_associations contact_method_associations_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_method_associations
    ADD CONSTRAINT contact_method_associations_ibfk_3 FOREIGN KEY (error_location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7991 (class 2606 OID 893147)
-- Name: contact_phones contact_phones_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_phones
    ADD CONSTRAINT contact_phones_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7992 (class 2606 OID 893152)
-- Name: contact_postal_addresses contact_postal_addresses_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_postal_addresses
    ADD CONSTRAINT contact_postal_addresses_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7993 (class 2606 OID 893157)
-- Name: contact_recently_opened_items contact_recently_opened_items_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_recently_opened_items
    ADD CONSTRAINT contact_recently_opened_items_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.recently_opened_items(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7995 (class 2606 OID 893167)
-- Name: correction_types correction_types_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types
    ADD CONSTRAINT correction_types_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7996 (class 2606 OID 893172)
-- Name: correction_types_locations correction_types_locations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_locations
    ADD CONSTRAINT correction_types_locations_ibfk_1 FOREIGN KEY (correction_type_id) REFERENCES vaxiom.correction_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7997 (class 2606 OID 893177)
-- Name: correction_types_locations correction_types_locations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_locations
    ADD CONSTRAINT correction_types_locations_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7998 (class 2606 OID 893182)
-- Name: correction_types_payment_options correction_types_payment_options_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_payment_options
    ADD CONSTRAINT correction_types_payment_options_ibfk_1 FOREIGN KEY (correction_types_id) REFERENCES vaxiom.correction_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7999 (class 2606 OID 893187)
-- Name: correction_types_payment_options correction_types_payment_options_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_payment_options
    ADD CONSTRAINT correction_types_payment_options_ibfk_2 FOREIGN KEY (payment_options_id) REFERENCES vaxiom.payment_options(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8000 (class 2606 OID 893192)
-- Name: daily_transactions daily_transactions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.daily_transactions
    ADD CONSTRAINT daily_transactions_ibfk_1 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8003 (class 2606 OID 893207)
-- Name: day_schedule_break_hours day_schedule_break_hours_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_break_hours
    ADD CONSTRAINT day_schedule_break_hours_ibfk_1 FOREIGN KEY (day_schedule_id) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8004 (class 2606 OID 893212)
-- Name: day_schedule_office_hours day_schedule_office_hours_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_office_hours
    ADD CONSTRAINT day_schedule_office_hours_ibfk_1 FOREIGN KEY (day_schedule_id) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8001 (class 2606 OID 893197)
-- Name: day_schedules day_schedules_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedules
    ADD CONSTRAINT day_schedules_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8002 (class 2606 OID 893202)
-- Name: day_schedules day_schedules_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedules
    ADD CONSTRAINT day_schedules_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8005 (class 2606 OID 893217)
-- Name: default_appointment_templates default_appointment_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.default_appointment_templates
    ADD CONSTRAINT default_appointment_templates_ibfk_1 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8006 (class 2606 OID 893222)
-- Name: default_appointment_templates default_appointment_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.default_appointment_templates
    ADD CONSTRAINT default_appointment_templates_ibfk_2 FOREIGN KEY (template_id) REFERENCES vaxiom.appointment_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8007 (class 2606 OID 893227)
-- Name: dentalchart_additional_markings dentalchart_additional_markings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_additional_markings
    ADD CONSTRAINT dentalchart_additional_markings_ibfk_1 FOREIGN KEY (marking_id) REFERENCES vaxiom.dentalchart_marking(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8008 (class 2606 OID 893232)
-- Name: dentalchart_edit_log dentalchart_edit_log_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_edit_log
    ADD CONSTRAINT dentalchart_edit_log_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.audit_logs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8009 (class 2606 OID 893237)
-- Name: dentalchart_edit_log dentalchart_edit_log_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_edit_log
    ADD CONSTRAINT dentalchart_edit_log_ibfk_2 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.dentalchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8010 (class 2606 OID 893242)
-- Name: dentalchart_marking dentalchart_marking_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_marking
    ADD CONSTRAINT dentalchart_marking_ibfk_1 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.dentalchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8011 (class 2606 OID 893247)
-- Name: dentalchart_snapshot dentalchart_snapshot_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_snapshot
    ADD CONSTRAINT dentalchart_snapshot_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8270 (class 2606 OID 894542)
-- Name: permissions_user_role_departament department_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_departament
    ADD CONSTRAINT department_fk FOREIGN KEY (department_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8014 (class 2606 OID 893262)
-- Name: diagnosis_field_values diagnosis_field_values_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_field_values
    ADD CONSTRAINT diagnosis_field_values_ibfk_1 FOREIGN KEY (diagnosis_id) REFERENCES vaxiom.diagnosis(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8012 (class 2606 OID 893252)
-- Name: diagnosis diagnosis_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis
    ADD CONSTRAINT diagnosis_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8013 (class 2606 OID 893257)
-- Name: diagnosis diagnosis_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis
    ADD CONSTRAINT diagnosis_ibfk_2 FOREIGN KEY (diagnosis_template_id) REFERENCES vaxiom.diagnosis_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8015 (class 2606 OID 893267)
-- Name: diagnosis_letters diagnosis_letters_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT diagnosis_letters_ibfk_1 FOREIGN KEY (diagnosis_id) REFERENCES vaxiom.diagnosis(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8016 (class 2606 OID 893272)
-- Name: diagnosis_letters diagnosis_letters_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT diagnosis_letters_ibfk_2 FOREIGN KEY (document_template_id) REFERENCES vaxiom.document_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8017 (class 2606 OID 893277)
-- Name: diagnosis_letters diagnosis_letters_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT diagnosis_letters_ibfk_3 FOREIGN KEY (recipient_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8018 (class 2606 OID 893282)
-- Name: diagnosis_letters diagnosis_letters_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT diagnosis_letters_ibfk_4 FOREIGN KEY (address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8019 (class 2606 OID 893287)
-- Name: diagnosis_templates diagnosis_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_templates
    ADD CONSTRAINT diagnosis_templates_ibfk_1 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8020 (class 2606 OID 893292)
-- Name: diagnosis_templates diagnosis_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_templates
    ADD CONSTRAINT diagnosis_templates_ibfk_2 FOREIGN KEY (appointment_type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8021 (class 2606 OID 893297)
-- Name: diagnosis_templates diagnosis_templates_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_templates
    ADD CONSTRAINT diagnosis_templates_ibfk_3 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8023 (class 2606 OID 893307)
-- Name: discount_adjustments discount_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_adjustments
    ADD CONSTRAINT discount_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8024 (class 2606 OID 893312)
-- Name: discount_adjustments discount_adjustments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_adjustments
    ADD CONSTRAINT discount_adjustments_ibfk_2 FOREIGN KEY (discount_id) REFERENCES vaxiom.discounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8025 (class 2606 OID 893317)
-- Name: discount_reverse_adjustments discount_reverse_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_reverse_adjustments
    ADD CONSTRAINT discount_reverse_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8026 (class 2606 OID 893322)
-- Name: discount_reverse_adjustments discount_reverse_adjustments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_reverse_adjustments
    ADD CONSTRAINT discount_reverse_adjustments_ibfk_2 FOREIGN KEY (discount_adjustment_id) REFERENCES vaxiom.discount_adjustments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8022 (class 2606 OID 893302)
-- Name: discounts discounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discounts
    ADD CONSTRAINT discounts_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8027 (class 2606 OID 893327)
-- Name: dock_item_features_events dock_item_features_events_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_events
    ADD CONSTRAINT dock_item_features_events_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8028 (class 2606 OID 893332)
-- Name: dock_item_features_images dock_item_features_images_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_images
    ADD CONSTRAINT dock_item_features_images_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8029 (class 2606 OID 893337)
-- Name: dock_item_features_images dock_item_features_images_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_images
    ADD CONSTRAINT dock_item_features_images_ibfk_2 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8030 (class 2606 OID 893342)
-- Name: dock_item_features_messages dock_item_features_messages_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_messages
    ADD CONSTRAINT dock_item_features_messages_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8031 (class 2606 OID 893347)
-- Name: dock_item_features_note dock_item_features_note_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_note
    ADD CONSTRAINT dock_item_features_note_ibfk_1 FOREIGN KEY (note_id) REFERENCES vaxiom.notebook_notes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8032 (class 2606 OID 893352)
-- Name: dock_item_features_note dock_item_features_note_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_note
    ADD CONSTRAINT dock_item_features_note_ibfk_2 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8033 (class 2606 OID 893357)
-- Name: dock_item_features_tasks dock_item_features_tasks_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_tasks
    ADD CONSTRAINT dock_item_features_tasks_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8034 (class 2606 OID 893362)
-- Name: document_recently_opened_items document_recently_opened_items_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_recently_opened_items
    ADD CONSTRAINT document_recently_opened_items_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.recently_opened_items(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8035 (class 2606 OID 893367)
-- Name: document_templates document_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_templates
    ADD CONSTRAINT document_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8036 (class 2606 OID 893372)
-- Name: document_tree_nodes document_tree_nodes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_tree_nodes
    ADD CONSTRAINT document_tree_nodes_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8075 (class 2606 OID 893567)
-- Name: employment_contract_permissions ecperm_emploment_contract_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_permissions
    ADD CONSTRAINT ecperm_emploment_contract_fk FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8076 (class 2606 OID 893572)
-- Name: employment_contract_permissions ecperm_permission_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_permissions
    ADD CONSTRAINT ecperm_permission_fk FOREIGN KEY (permissions_permission_id) REFERENCES vaxiom.permissions_permission(id);


--
-- TOC entry 8077 (class 2606 OID 893577)
-- Name: employment_contract_role ecrole_emploment_contract_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_role
    ADD CONSTRAINT ecrole_emploment_contract_fk FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8078 (class 2606 OID 893582)
-- Name: employment_contract_role ecrole_user_role_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_role
    ADD CONSTRAINT ecrole_user_role_fk FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id);


--
-- TOC entry 8073 (class 2606 OID 893557)
-- Name: employment_contract_location_group emploment_contract_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_location_group
    ADD CONSTRAINT emploment_contract_fk FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8053 (class 2606 OID 893457)
-- Name: employee_professional_relationships employee_professional_relationships_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_professional_relationships
    ADD CONSTRAINT employee_professional_relationships_ibfk_1 FOREIGN KEY (provider_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8054 (class 2606 OID 893462)
-- Name: employee_resources employee_resources_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_resources
    ADD CONSTRAINT employee_resources_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 8055 (class 2606 OID 893467)
-- Name: employee_resources employee_resources_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_resources
    ADD CONSTRAINT employee_resources_ibfk_2 FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8056 (class 2606 OID 893472)
-- Name: employers employers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_1 FOREIGN KEY (contact_postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8057 (class 2606 OID 893477)
-- Name: employers employers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_2 FOREIGN KEY (contact_phone_id) REFERENCES vaxiom.contact_phones(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8058 (class 2606 OID 893482)
-- Name: employers employers_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_3 FOREIGN KEY (contact_email_id) REFERENCES vaxiom.contact_emails(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8059 (class 2606 OID 893487)
-- Name: employers employers_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_4 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8060 (class 2606 OID 893492)
-- Name: employers employers_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_5 FOREIGN KEY (master_id) REFERENCES vaxiom.employers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8061 (class 2606 OID 893497)
-- Name: employment_contracts employment_contracts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT employment_contracts_ibfk_1 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8062 (class 2606 OID 893502)
-- Name: employment_contracts employment_contracts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT employment_contracts_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8081 (class 2606 OID 893597)
-- Name: enrollment_form enrollment_form_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form
    ADD CONSTRAINT enrollment_form_ibfk_1 FOREIGN KEY (employee_id) REFERENCES vaxiom.employment_contracts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8082 (class 2606 OID 893602)
-- Name: enrollment_form enrollment_form_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form
    ADD CONSTRAINT enrollment_form_ibfk_2 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.hradmin_enrollment(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8086 (class 2606 OID 893622)
-- Name: enrollment_group_enrollment enrollment_group_enrollment_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_group_enrollment
    ADD CONSTRAINT enrollment_group_enrollment_ibfk_1 FOREIGN KEY (enrollment_group_id) REFERENCES vaxiom.enrollment_group(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8087 (class 2606 OID 893627)
-- Name: enrollment_group_enrollment enrollment_group_enrollment_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_group_enrollment
    ADD CONSTRAINT enrollment_group_enrollment_ibfk_2 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8083 (class 2606 OID 893607)
-- Name: enrollment_groups_enrollment_legal_entity enrollment_groups_enrollment_legal_entity_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_groups_enrollment_legal_entity
    ADD CONSTRAINT enrollment_groups_enrollment_legal_entity_ibfk_1 FOREIGN KEY (enrollment_group_id) REFERENCES vaxiom.enrollment_group(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8084 (class 2606 OID 893612)
-- Name: enrollment_groups_enrollment_legal_entity enrollment_groups_enrollment_legal_entity_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_groups_enrollment_legal_entity
    ADD CONSTRAINT enrollment_groups_enrollment_legal_entity_ibfk_2 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8085 (class 2606 OID 893617)
-- Name: enrollment_groups_enrollment_legal_entity enrollment_groups_enrollment_legal_entity_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_groups_enrollment_legal_entity
    ADD CONSTRAINT enrollment_groups_enrollment_legal_entity_ibfk_3 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8088 (class 2606 OID 893632)
-- Name: enrollment_schedule_billing_groups enrollment_schedule_billing_groups_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_schedule_billing_groups
    ADD CONSTRAINT enrollment_schedule_billing_groups_ibfk_1 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8089 (class 2606 OID 893637)
-- Name: enrollment_schedule_billing_groups enrollment_schedule_billing_groups_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_schedule_billing_groups
    ADD CONSTRAINT enrollment_schedule_billing_groups_ibfk_2 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8090 (class 2606 OID 893642)
-- Name: equipment equipment_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.equipment
    ADD CONSTRAINT equipment_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8091 (class 2606 OID 893647)
-- Name: external_offices external_offices_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.external_offices
    ADD CONSTRAINT external_offices_ibfk_1 FOREIGN KEY (postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8092 (class 2606 OID 893652)
-- Name: financial_settings financial_settings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.financial_settings
    ADD CONSTRAINT financial_settings_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8093 (class 2606 OID 893657)
-- Name: fix_adjustments fix_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.fix_adjustments
    ADD CONSTRAINT fix_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7934 (class 2606 OID 892862)
-- Name: appointment_bookings fk_appointment_bookings_chairs; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT fk_appointment_bookings_chairs FOREIGN KEY (seated_chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8339 (class 2606 OID 894887)
-- Name: selected_appointment_templates_office_day_calendar_view fk_appointment_template; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates_office_day_calendar_view
    ADD CONSTRAINT fk_appointment_template FOREIGN KEY (appointment_template_id) REFERENCES vaxiom.appointment_templates(id);


--
-- TOC entry 7957 (class 2606 OID 892977)
-- Name: benefit_saving_account_values fk_benefit_saving_account_values_employee_beneficiary_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.benefit_saving_account_values
    ADD CONSTRAINT fk_benefit_saving_account_values_employee_beneficiary_group1 FOREIGN KEY (employee_beneficiary_group_id) REFERENCES vaxiom.employee_beneficiary_group(id);


--
-- TOC entry 7958 (class 2606 OID 892982)
-- Name: benefit_saving_account_values fk_benefit_saving_accout_values_employee_benefit1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.benefit_saving_account_values
    ADD CONSTRAINT fk_benefit_saving_accout_values_employee_benefit1 FOREIGN KEY (employee_benefit_id) REFERENCES vaxiom.employee_benefit(id);


--
-- TOC entry 7959 (class 2606 OID 892987)
-- Name: benefit_saving_account_values fk_benefit_saving_accout_values_insurance_plan_saving_account1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.benefit_saving_account_values
    ADD CONSTRAINT fk_benefit_saving_accout_values_insurance_plan_saving_account1 FOREIGN KEY (insurance_plan_saving_account_id) REFERENCES vaxiom.insurance_plan_saving_account(id);


--
-- TOC entry 8103 (class 2606 OID 893707)
-- Name: insurance_claims_setup fk_billing_entity; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_billing_entity FOREIGN KEY (billing_entity_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 7994 (class 2606 OID 893162)
-- Name: contact_website fk_contact_website_contact_methods1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_website
    ADD CONSTRAINT fk_contact_website_contact_methods1 FOREIGN KEY (id) REFERENCES vaxiom.contact_methods(id);


--
-- TOC entry 8037 (class 2606 OID 893377)
-- Name: employee_beneficiary fk_employee_benebitiary_employee_benefitiary_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary
    ADD CONSTRAINT fk_employee_benebitiary_employee_benefitiary_group1 FOREIGN KEY (employee_beneficiary_group_id) REFERENCES vaxiom.employee_beneficiary_group(id);


--
-- TOC entry 8038 (class 2606 OID 893382)
-- Name: employee_beneficiary fk_employee_benebitiary_employee_enrollment_person1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary
    ADD CONSTRAINT fk_employee_benebitiary_employee_enrollment_person1 FOREIGN KEY (employee_enrollment_person_id) REFERENCES vaxiom.employee_enrollment_person(id);


--
-- TOC entry 8040 (class 2606 OID 893392)
-- Name: employee_benefit fk_employee_benefit_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_benefit
    ADD CONSTRAINT fk_employee_benefit_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8041 (class 2606 OID 893397)
-- Name: employee_benefit fk_employee_benefit_insurance_palan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_benefit
    ADD CONSTRAINT fk_employee_benefit_insurance_palan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8039 (class 2606 OID 893387)
-- Name: employee_beneficiary_group fk_employee_benefitiary_group_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary_group
    ADD CONSTRAINT fk_employee_benefitiary_group_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8042 (class 2606 OID 893402)
-- Name: employee_dependent fk_employee_dependent_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_dependent
    ADD CONSTRAINT fk_employee_dependent_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8043 (class 2606 OID 893407)
-- Name: employee_dependent fk_employee_dependet_employee_enrollment_person1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_dependent
    ADD CONSTRAINT fk_employee_dependet_employee_enrollment_person1 FOREIGN KEY (employee_enrollment_person_id) REFERENCES vaxiom.employee_enrollment_person(id);


--
-- TOC entry 8070 (class 2606 OID 893542)
-- Name: employment_contract_emergency_contact fk_employee_emergency_contact_employment_contracts1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_emergency_contact
    ADD CONSTRAINT fk_employee_emergency_contact_employment_contracts1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8047 (class 2606 OID 893427)
-- Name: employee_enrollment_attachment fk_employee_enrollment_attachment_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_attachment
    ADD CONSTRAINT fk_employee_enrollment_attachment_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8044 (class 2606 OID 893412)
-- Name: employee_enrollment fk_employee_enrollment_employment_contracts1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment
    ADD CONSTRAINT fk_employee_enrollment_employment_contracts1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8045 (class 2606 OID 893417)
-- Name: employee_enrollment fk_employee_enrollment_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment
    ADD CONSTRAINT fk_employee_enrollment_enrollment1 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id);


--
-- TOC entry 8046 (class 2606 OID 893422)
-- Name: employee_enrollment fk_employee_enrollment_enrollment_form_source_event1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment
    ADD CONSTRAINT fk_employee_enrollment_enrollment_form_source_event1 FOREIGN KEY (enrollment_form_source_event_id) REFERENCES vaxiom.enrollment_form_source_event(id);


--
-- TOC entry 8048 (class 2606 OID 893432)
-- Name: employee_enrollment_event fk_employee_enrollment_event_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_event
    ADD CONSTRAINT fk_employee_enrollment_event_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8049 (class 2606 OID 893437)
-- Name: employee_enrollment_person fk_employee_enrollment_person_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_person
    ADD CONSTRAINT fk_employee_enrollment_person_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8050 (class 2606 OID 893442)
-- Name: employee_insured fk_employee_insured_employee_benefit1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_insured
    ADD CONSTRAINT fk_employee_insured_employee_benefit1 FOREIGN KEY (employee_benefit_id) REFERENCES vaxiom.employee_benefit(id);


--
-- TOC entry 8051 (class 2606 OID 893447)
-- Name: employee_insured fk_employee_insured_employee_benefitiary_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_insured
    ADD CONSTRAINT fk_employee_insured_employee_benefitiary_group1 FOREIGN KEY (employee_beneficiary_group_id) REFERENCES vaxiom.employee_beneficiary_group(id);


--
-- TOC entry 8052 (class 2606 OID 893452)
-- Name: employee_insured fk_employee_insured_employee_dependet1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_insured
    ADD CONSTRAINT fk_employee_insured_employee_dependet1 FOREIGN KEY (employee_dependent_id) REFERENCES vaxiom.employee_dependent(id);


--
-- TOC entry 8072 (class 2606 OID 893552)
-- Name: employment_contract_job_status_log fk_employement_contract_job_status_log_employment_contracts1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_job_status_log
    ADD CONSTRAINT fk_employement_contract_job_status_log_employment_contracts1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8170 (class 2606 OID 894042)
-- Name: legal_entity fk_employer_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity
    ADD CONSTRAINT fk_employer_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8071 (class 2606 OID 893547)
-- Name: employment_contract_enrollment_group_log fk_employment_contract_enrollment_group_log_employment_contra1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_enrollment_group_log
    ADD CONSTRAINT fk_employment_contract_enrollment_group_log_employment_contra1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8079 (class 2606 OID 893587)
-- Name: employment_contract_salary_log fk_employment_contract_salary_log_employment_contra1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_salary_log
    ADD CONSTRAINT fk_employment_contract_salary_log_employment_contra1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8063 (class 2606 OID 893507)
-- Name: employment_contracts fk_employment_contracts_enrollment_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_enrollment_group1 FOREIGN KEY (enrollment_group_id) REFERENCES vaxiom.enrollment_group(id);


--
-- TOC entry 8064 (class 2606 OID 893512)
-- Name: employment_contracts fk_employment_contracts_jobtitle1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_jobtitle1 FOREIGN KEY (jobtitle_id) REFERENCES vaxiom.jobtitle(id);


--
-- TOC entry 8065 (class 2606 OID 893517)
-- Name: employment_contracts fk_employment_contracts_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8066 (class 2606 OID 893522)
-- Name: employment_contracts fk_employment_contracts_legal_entity_billing_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_legal_entity_billing_group1 FOREIGN KEY (billing_group_id) REFERENCES vaxiom.legal_entity_billing_group(id);


--
-- TOC entry 8069 (class 2606 OID 893537)
-- Name: employment_contracts fk_employment_contracts_sys_organization_address1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_sys_organization_address1 FOREIGN KEY (physical_address_id) REFERENCES vaxiom.sys_organization_address(id);


--
-- TOC entry 8067 (class 2606 OID 893527)
-- Name: employment_contracts fk_employment_contracts_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_sys_organizations1 FOREIGN KEY (department_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8068 (class 2606 OID 893532)
-- Name: employment_contracts fk_employment_contracts_sys_organizations2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_sys_organizations2 FOREIGN KEY (division_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8080 (class 2606 OID 893592)
-- Name: enrollment fk_enrollment_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment
    ADD CONSTRAINT fk_enrollment_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8140 (class 2606 OID 893892)
-- Name: insurance_plan_fixed_limit fk_fixedlimits_insurance_limits1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_fixed_limit
    ADD CONSTRAINT fk_fixedlimits_insurance_limits1 FOREIGN KEY (insurance_limits_id) REFERENCES vaxiom.insurance_plan_limits(id);


--
-- TOC entry 8147 (class 2606 OID 893927)
-- Name: insurance_plan_saving_account fk_insurace_plan_saving_account_insurance_plan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_saving_account
    ADD CONSTRAINT fk_insurace_plan_saving_account_insurance_plan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8104 (class 2606 OID 893712)
-- Name: insurance_claims_setup fk_insurance_claims_setup_legal_entity_npi1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_legal_entity_npi1 FOREIGN KEY (legal_entity_npi_id) REFERENCES vaxiom.legal_entity_npi(id);


--
-- TOC entry 8105 (class 2606 OID 893717)
-- Name: insurance_claims_setup fk_insurance_claims_setup_legal_entity_tin1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_legal_entity_tin1 FOREIGN KEY (tin_id) REFERENCES vaxiom.legal_entity_tin(id);


--
-- TOC entry 8107 (class 2606 OID 893727)
-- Name: insurance_claims_setup fk_insurance_claims_setup_sys_organization_address1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_sys_organization_address1 FOREIGN KEY (sys_organization_address_id) REFERENCES vaxiom.sys_organization_address(id);


--
-- TOC entry 8108 (class 2606 OID 893732)
-- Name: insurance_claims_setup fk_insurance_claims_setup_sys_organization_contact_method1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_sys_organization_contact_method1 FOREIGN KEY (sys_organization_contact_method_id) REFERENCES vaxiom.sys_organization_contact_method(id);


--
-- TOC entry 8106 (class 2606 OID 893722)
-- Name: insurance_claims_setup fk_insurance_claims_setup_sys_organizations; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_sys_organizations FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8145 (class 2606 OID 893917)
-- Name: insurance_plan_limits fk_insurance_limits_insurance_palan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_limits
    ADD CONSTRAINT fk_insurance_limits_insurance_palan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8131 (class 2606 OID 893847)
-- Name: insurance_plan_age_and_gender_banded_rate fk_insurance_plan_age_and_gender_banded_rate_insurance_plan_r1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_and_gender_banded_rate
    ADD CONSTRAINT fk_insurance_plan_age_and_gender_banded_rate_insurance_plan_r1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8132 (class 2606 OID 893852)
-- Name: insurance_plan_age_banded_rate fk_insurance_plan_age_banded_rate_insurance_plan_rates1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_banded_rate
    ADD CONSTRAINT fk_insurance_plan_age_banded_rate_insurance_plan_rates1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8133 (class 2606 OID 893857)
-- Name: insurance_plan_benefit_amount fk_insurance_plan_benefit_amount_insurance_plan2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_benefit_amount
    ADD CONSTRAINT fk_insurance_plan_benefit_amount_insurance_plan2 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8134 (class 2606 OID 893862)
-- Name: insurance_plan_complex_benefit_amount fk_insurance_plan_complex_benefit_amount_insurance_plan_benef2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_complex_benefit_amount
    ADD CONSTRAINT fk_insurance_plan_complex_benefit_amount_insurance_plan_benef2 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_benefit_amount(id);


--
-- TOC entry 8135 (class 2606 OID 893867)
-- Name: insurance_plan_complex_benefit_amount_item fk_insurance_plan_complex_benefit_amount_item_insurance_plan_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_complex_benefit_amount_item
    ADD CONSTRAINT fk_insurance_plan_complex_benefit_amount_item_insurance_plan_1 FOREIGN KEY (insurance_plan_complex_benefit_amount_id) REFERENCES vaxiom.insurance_plan_complex_benefit_amount(id);


--
-- TOC entry 8136 (class 2606 OID 893872)
-- Name: insurance_plan_composition_detailed_rate fk_insurance_plan_composition_detailed_rate_insurance_plan_rate; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_composition_detailed_rate
    ADD CONSTRAINT fk_insurance_plan_composition_detailed_rate_insurance_plan_rate FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8137 (class 2606 OID 893877)
-- Name: insurance_plan_composition_rate fk_insurance_plan_composition_rate_insurance_plan_rates1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_composition_rate
    ADD CONSTRAINT fk_insurance_plan_composition_rate_insurance_plan_rates1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8141 (class 2606 OID 893897)
-- Name: insurance_plan_has_enrollment fk_insurance_plan_has_enrollment_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment
    ADD CONSTRAINT fk_insurance_plan_has_enrollment_enrollment1 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id);


--
-- TOC entry 8143 (class 2606 OID 893907)
-- Name: insurance_plan_has_enrollment_has_enrollment_group fk_insurance_plan_has_enrollment_has_enrollment_group_enrollm1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment_has_enrollment_group
    ADD CONSTRAINT fk_insurance_plan_has_enrollment_has_enrollment_group_enrollm1 FOREIGN KEY (enrollment_group_id) REFERENCES vaxiom.enrollment_group(id);


--
-- TOC entry 8144 (class 2606 OID 893912)
-- Name: insurance_plan_has_enrollment_has_enrollment_group fk_insurance_plan_has_enrollment_has_enrollment_group_insuran1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment_has_enrollment_group
    ADD CONSTRAINT fk_insurance_plan_has_enrollment_has_enrollment_group_insuran1 FOREIGN KEY (insurance_plan_has_enrollment_id) REFERENCES vaxiom.insurance_plan_has_enrollment(id);


--
-- TOC entry 8142 (class 2606 OID 893902)
-- Name: insurance_plan_has_enrollment fk_insurance_plan_has_enrollment_insurance_plan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment
    ADD CONSTRAINT fk_insurance_plan_has_enrollment_insurance_plan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8146 (class 2606 OID 893922)
-- Name: insurance_plan_rates fk_insurance_plan_rates_insurance_plan; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_rates
    ADD CONSTRAINT fk_insurance_plan_rates_insurance_plan FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8149 (class 2606 OID 893937)
-- Name: insurance_plan_simple_benefit_amount fk_insurance_plan_simple_benefit_amount_insurance_plan_benefi1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_simple_benefit_amount
    ADD CONSTRAINT fk_insurance_plan_simple_benefit_amount_insurance_plan_benefi1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_benefit_amount(id);


--
-- TOC entry 8151 (class 2606 OID 893947)
-- Name: insurance_plan_summary_fields fk_insurance_plan_summary_fields_insurance_plan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_summary_fields
    ADD CONSTRAINT fk_insurance_plan_summary_fields_insurance_plan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8152 (class 2606 OID 893952)
-- Name: insurance_plan_term_disability_benefit_amount fk_insurance_plan_term_disabilility_benefit_amount_insurance_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_term_disability_benefit_amount
    ADD CONSTRAINT fk_insurance_plan_term_disabilility_benefit_amount_insurance_1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_benefit_amount(id);


--
-- TOC entry 8125 (class 2606 OID 893817)
-- Name: insurance_payment_corrections fk_insurancepaymentcorrectionscorrectiontypeid_correctiontypesi; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_corrections
    ADD CONSTRAINT fk_insurancepaymentcorrectionscorrectiontypeid_correctiontypesi FOREIGN KEY (correction_type_id) REFERENCES vaxiom.correction_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8126 (class 2606 OID 893822)
-- Name: insurance_payment_corrections fk_insurancepaymentcorrectionsid_insurancepaymentmovementsid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_corrections
    ADD CONSTRAINT fk_insurancepaymentcorrectionsid_insurancepaymentmovementsid FOREIGN KEY (id) REFERENCES vaxiom.insurance_payment_movements(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8120 (class 2606 OID 893792)
-- Name: insurance_payments fk_insurancepaymentid_insurancecompanies; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payments
    ADD CONSTRAINT fk_insurancepaymentid_insurancecompanies FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8127 (class 2606 OID 893827)
-- Name: insurance_payment_movements fk_insurancepaymentmovementid_insurancepaymentid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_movements
    ADD CONSTRAINT fk_insurancepaymentmovementid_insurancepaymentid FOREIGN KEY (insurance_payment_id) REFERENCES vaxiom.insurance_payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8128 (class 2606 OID 893832)
-- Name: insurance_payment_refunds fk_insurancepaymentrefundsid_insurancepaymentmovementsid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_refunds
    ADD CONSTRAINT fk_insurancepaymentrefundsid_insurancepaymentmovementsid FOREIGN KEY (id) REFERENCES vaxiom.insurance_payment_movements(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8129 (class 2606 OID 893837)
-- Name: insurance_payment_transfers fk_insurancepaymenttransferid_insurancepaymentmovementsid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_transfers
    ADD CONSTRAINT fk_insurancepaymenttransferid_insurancepaymentmovementsid FOREIGN KEY (id) REFERENCES vaxiom.insurance_payment_movements(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8130 (class 2606 OID 893842)
-- Name: insurance_payment_transfers fk_insurancepaymenttransferpaymentid_paymentid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_transfers
    ADD CONSTRAINT fk_insurancepaymenttransferpaymentid_paymentid FOREIGN KEY (payment_id) REFERENCES vaxiom.payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8158 (class 2606 OID 893982)
-- Name: insured_benefit_values fk_insured_rate_amount_employee_insured1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured_benefit_values
    ADD CONSTRAINT fk_insured_rate_amount_employee_insured1 FOREIGN KEY (employee_insured_id) REFERENCES vaxiom.employee_insured(id);


--
-- TOC entry 8166 (class 2606 OID 894022)
-- Name: jobtitle_group fk_jobtitle_group_eeo1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle_group
    ADD CONSTRAINT fk_jobtitle_group_eeo1 FOREIGN KEY (eeo_id) REFERENCES vaxiom.eeo(id);


--
-- TOC entry 8164 (class 2606 OID 894012)
-- Name: jobtitle fk_jobtitle_jobtitle_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle
    ADD CONSTRAINT fk_jobtitle_jobtitle_group1 FOREIGN KEY (jobtitle_group_id) REFERENCES vaxiom.jobtitle_group(id);


--
-- TOC entry 8165 (class 2606 OID 894017)
-- Name: jobtitle fk_jobtitle_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle
    ADD CONSTRAINT fk_jobtitle_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8174 (class 2606 OID 894062)
-- Name: legal_entity_address_attachment fk_legal_entity_address_attachment_legal_entity_address1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_address_attachment
    ADD CONSTRAINT fk_legal_entity_address_attachment_legal_entity_address1 FOREIGN KEY (legal_entity_address_id) REFERENCES vaxiom.legal_entity_address(id);


--
-- TOC entry 8173 (class 2606 OID 894057)
-- Name: legal_entity_address fk_legal_entity_address_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_address
    ADD CONSTRAINT fk_legal_entity_address_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8176 (class 2606 OID 894072)
-- Name: legal_entity_billing_group_has_sys_organizations fk_legal_entity_billing_group_has_sys_organizations_legal_enti1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group_has_sys_organizations
    ADD CONSTRAINT fk_legal_entity_billing_group_has_sys_organizations_legal_enti1 FOREIGN KEY (legal_entity_billing_group_id) REFERENCES vaxiom.legal_entity_billing_group(id);


--
-- TOC entry 8177 (class 2606 OID 894077)
-- Name: legal_entity_billing_group_has_sys_organizations fk_legal_entity_billing_group_has_sys_organizations_sys_organi1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group_has_sys_organizations
    ADD CONSTRAINT fk_legal_entity_billing_group_has_sys_organizations_sys_organi1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8175 (class 2606 OID 894067)
-- Name: legal_entity_billing_group fk_legal_entity_billing_group_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group
    ADD CONSTRAINT fk_legal_entity_billing_group_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8171 (class 2606 OID 894047)
-- Name: legal_entity fk_legal_entity_county1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity
    ADD CONSTRAINT fk_legal_entity_county1 FOREIGN KEY (county_id) REFERENCES vaxiom.county(id);


--
-- TOC entry 8178 (class 2606 OID 894082)
-- Name: legal_entity_dba fk_legal_entity_dba_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_dba
    ADD CONSTRAINT fk_legal_entity_dba_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8168 (class 2606 OID 894032)
-- Name: legal_entities_participating_enrollment fk_legal_entity_has_enrollment_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entities_participating_enrollment
    ADD CONSTRAINT fk_legal_entity_has_enrollment_enrollment1 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id);


--
-- TOC entry 8169 (class 2606 OID 894037)
-- Name: legal_entities_participating_enrollment fk_legal_entity_has_enrollment_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entities_participating_enrollment
    ADD CONSTRAINT fk_legal_entity_has_enrollment_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8182 (class 2606 OID 894102)
-- Name: legal_entity_insurance_benefit_has_division fk_legal_entity_insurance_benefit_has_sys_organizations_legal1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit_has_division
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_has_sys_organizations_legal1 FOREIGN KEY (legal_entity_insurance_benefit_id) REFERENCES vaxiom.legal_entity_insurance_benefit(id);


--
-- TOC entry 8183 (class 2606 OID 894107)
-- Name: legal_entity_insurance_benefit_has_division fk_legal_entity_insurance_benefit_has_sys_organizations_sys_o1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit_has_division
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_has_sys_organizations_sys_o1 FOREIGN KEY (division_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8179 (class 2606 OID 894087)
-- Name: legal_entity_insurance_benefit fk_legal_entity_insurance_benefit_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_legal_entity1 FOREIGN KEY (benefits_holder) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8180 (class 2606 OID 894092)
-- Name: legal_entity_insurance_benefit fk_legal_entity_insurance_benefit_legal_entity2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_legal_entity2 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8181 (class 2606 OID 894097)
-- Name: legal_entity_insurance_benefit fk_legal_entity_insurance_benefit_sys_users1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_sys_users1 FOREIGN KEY (broker_id) REFERENCES vaxiom.sys_users(id);


--
-- TOC entry 8172 (class 2606 OID 894052)
-- Name: legal_entity fk_legal_entity_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity
    ADD CONSTRAINT fk_legal_entity_legal_entity1 FOREIGN KEY (parent_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8184 (class 2606 OID 894112)
-- Name: legal_entity_npi fk_legal_entity_npi_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_npi
    ADD CONSTRAINT fk_legal_entity_npi_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8185 (class 2606 OID 894117)
-- Name: legal_entity_operational_structures fk_legal_entity_operational_structures_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_operational_structures
    ADD CONSTRAINT fk_legal_entity_operational_structures_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8186 (class 2606 OID 894122)
-- Name: legal_entity_owner fk_legal_entity_owner_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_owner
    ADD CONSTRAINT fk_legal_entity_owner_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8340 (class 2606 OID 894892)
-- Name: selected_appointment_templates_office_day_calendar_view fk_location; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates_office_day_calendar_view
    ADD CONSTRAINT fk_location FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8195 (class 2606 OID 894167)
-- Name: medical_history_patient_relatives_crowding fk_medicalhistoryid_medicalhistory; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_patient_relatives_crowding
    ADD CONSTRAINT fk_medicalhistoryid_medicalhistory FOREIGN KEY (medical_history_id) REFERENCES vaxiom.medical_history(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8196 (class 2606 OID 894172)
-- Name: merchant_reports fk_merchant_reports_sys_organizations; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.merchant_reports
    ADD CONSTRAINT fk_merchant_reports_sys_organizations FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8250 (class 2606 OID 894442)
-- Name: payments fk_patient_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT fk_patient_id FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8121 (class 2606 OID 893797)
-- Name: insurance_payments fk_paymenttypeid_paymenttypes; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payments
    ADD CONSTRAINT fk_paymenttypeid_paymenttypes FOREIGN KEY (payment_type_id) REFERENCES vaxiom.payment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8276 (class 2606 OID 894572)
-- Name: permission_user_role_location fk_permission_user_role_location_permissions_user_role; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permission_user_role_location
    ADD CONSTRAINT fk_permission_user_role_location_permissions_user_role FOREIGN KEY (permission_user_role_id) REFERENCES vaxiom.permissions_user_role(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 8277 (class 2606 OID 894577)
-- Name: permission_user_role_location fk_permission_user_role_location_sys_organizations; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permission_user_role_location
    ADD CONSTRAINT fk_permission_user_role_location_sys_organizations FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 8278 (class 2606 OID 894582)
-- Name: permission_user_role_location fk_permission_user_role_location_sys_users; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permission_user_role_location
    ADD CONSTRAINT fk_permission_user_role_location_sys_users FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 8299 (class 2606 OID 894687)
-- Name: provider_license fk_provider_license_employment_contracts1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_license
    ADD CONSTRAINT fk_provider_license_employment_contracts1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8300 (class 2606 OID 894692)
-- Name: provider_license fk_provider_license_provider_specialty1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_license
    ADD CONSTRAINT fk_provider_license_provider_specialty1 FOREIGN KEY (provider_specialty_id) REFERENCES vaxiom.provider_specialty(id);


--
-- TOC entry 8148 (class 2606 OID 893932)
-- Name: insurance_plan_separate_age_banded_rate fk_separate_age_banded_rate_insurance_plan_rates1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_separate_age_banded_rate
    ADD CONSTRAINT fk_separate_age_banded_rate_insurance_plan_rates1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8150 (class 2606 OID 893942)
-- Name: insurance_plan_slider_limit fk_slider_limits_insurance_limits1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_slider_limit
    ADD CONSTRAINT fk_slider_limits_insurance_limits1 FOREIGN KEY (insurance_limits_id) REFERENCES vaxiom.insurance_plan_limits(id);


--
-- TOC entry 8352 (class 2606 OID 894952)
-- Name: sys_organization_address fk_sys_organisation_address_contact_postal_addresses1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address
    ADD CONSTRAINT fk_sys_organisation_address_contact_postal_addresses1 FOREIGN KEY (contact_postal_addresses_id) REFERENCES vaxiom.contact_postal_addresses(id);


--
-- TOC entry 8353 (class 2606 OID 894957)
-- Name: sys_organization_address_has_sys_organizations fk_sys_organisation_address_has_sys_organizations_sys_organis1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address_has_sys_organizations
    ADD CONSTRAINT fk_sys_organisation_address_has_sys_organizations_sys_organis1 FOREIGN KEY (sys_organisation_address_id) REFERENCES vaxiom.sys_organization_address(id);


--
-- TOC entry 8354 (class 2606 OID 894962)
-- Name: sys_organization_address_has_sys_organizations fk_sys_organisation_address_has_sys_organizations_sys_organiz1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address_has_sys_organizations
    ADD CONSTRAINT fk_sys_organisation_address_has_sys_organizations_sys_organiz1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8356 (class 2606 OID 894972)
-- Name: sys_organization_contact_method fk_sys_organisation_contact_method_contact_methods1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_contact_method
    ADD CONSTRAINT fk_sys_organisation_contact_method_contact_methods1 FOREIGN KEY (contact_methods_id) REFERENCES vaxiom.contact_methods(id);


--
-- TOC entry 8357 (class 2606 OID 894977)
-- Name: sys_organization_contact_method fk_sys_organisation_contact_method_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_contact_method
    ADD CONSTRAINT fk_sys_organisation_contact_method_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8358 (class 2606 OID 894982)
-- Name: sys_organization_department_data fk_sys_organisation_departament_data_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_department_data
    ADD CONSTRAINT fk_sys_organisation_departament_data_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8359 (class 2606 OID 894987)
-- Name: sys_organization_division_data fk_sys_organisation_devision_data_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_data
    ADD CONSTRAINT fk_sys_organisation_devision_data_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8355 (class 2606 OID 894967)
-- Name: sys_organization_attachment fk_sys_organization_attachment_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_attachment
    ADD CONSTRAINT fk_sys_organization_attachment_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8360 (class 2606 OID 894992)
-- Name: sys_organization_division_data fk_sys_organization_division_data_legal_entity_npi1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_data
    ADD CONSTRAINT fk_sys_organization_division_data_legal_entity_npi1 FOREIGN KEY (legal_entity_npi_id) REFERENCES vaxiom.legal_entity_npi(id);


--
-- TOC entry 8364 (class 2606 OID 895012)
-- Name: sys_organization_location_group_has_sys_organizations fk_sys_organization_location_group_has_sys_organizations_sys_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group_has_sys_organizations
    ADD CONSTRAINT fk_sys_organization_location_group_has_sys_organizations_sys_1 FOREIGN KEY (sys_organization_location_group_id) REFERENCES vaxiom.sys_organization_location_group(id);


--
-- TOC entry 8365 (class 2606 OID 895017)
-- Name: sys_organization_location_group_has_sys_organizations fk_sys_organization_location_group_has_sys_organizations_sys_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group_has_sys_organizations
    ADD CONSTRAINT fk_sys_organization_location_group_has_sys_organizations_sys_2 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8363 (class 2606 OID 895007)
-- Name: sys_organization_location_group fk_sys_organization_location_group_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group
    ADD CONSTRAINT fk_sys_organization_location_group_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8361 (class 2606 OID 894997)
-- Name: sys_organization_division_has_department fk_sys_organizations_has_sys_organizations_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_has_department
    ADD CONSTRAINT fk_sys_organizations_has_sys_organizations_sys_organizations1 FOREIGN KEY (division_tree_node_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8362 (class 2606 OID 895002)
-- Name: sys_organization_division_has_department fk_sys_organizations_has_sys_organizations_sys_organizations2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_has_department
    ADD CONSTRAINT fk_sys_organizations_has_sys_organizations_sys_organizations2 FOREIGN KEY (department_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8341 (class 2606 OID 894897)
-- Name: selected_appointment_templates_office_day_calendar_view fk_user; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates_office_day_calendar_view
    ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id);


--
-- TOC entry 8094 (class 2606 OID 893662)
-- Name: image_series image_series_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.image_series
    ADD CONSTRAINT image_series_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8095 (class 2606 OID 893667)
-- Name: image_series image_series_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.image_series
    ADD CONSTRAINT image_series_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.image_series_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8162 (class 2606 OID 894002)
-- Name: in_network_discounts in_network_discounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.in_network_discounts
    ADD CONSTRAINT in_network_discounts_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8163 (class 2606 OID 894007)
-- Name: in_network_discounts in_network_discounts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.in_network_discounts
    ADD CONSTRAINT in_network_discounts_ibfk_2 FOREIGN KEY (network_sheet_fee_id) REFERENCES vaxiom.network_sheet_fee(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8096 (class 2606 OID 893672)
-- Name: installment_charges installment_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.installment_charges
    ADD CONSTRAINT installment_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8097 (class 2606 OID 893677)
-- Name: installment_charges installment_charges_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.installment_charges
    ADD CONSTRAINT installment_charges_ibfk_2 FOREIGN KEY (payment_plan_id) REFERENCES vaxiom.payment_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8098 (class 2606 OID 893682)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_1 FOREIGN KEY (contact_postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8099 (class 2606 OID 893687)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_2 FOREIGN KEY (contact_phone_id) REFERENCES vaxiom.contact_phones(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8100 (class 2606 OID 893692)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_3 FOREIGN KEY (contact_email_id) REFERENCES vaxiom.contact_emails(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8101 (class 2606 OID 893697)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_4 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8102 (class 2606 OID 893702)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_5 FOREIGN KEY (master_id) REFERENCES vaxiom.insurance_billing_centers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8109 (class 2606 OID 893737)
-- Name: insurance_code_val insurance_code_val_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_code_val
    ADD CONSTRAINT insurance_code_val_ibfk_1 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8110 (class 2606 OID 893742)
-- Name: insurance_code_val insurance_code_val_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_code_val
    ADD CONSTRAINT insurance_code_val_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8111 (class 2606 OID 893747)
-- Name: insurance_code_val insurance_code_val_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_code_val
    ADD CONSTRAINT insurance_code_val_ibfk_3 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8117 (class 2606 OID 893777)
-- Name: insurance_company_payments insurance_companies_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_payments
    ADD CONSTRAINT insurance_companies_fk FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8112 (class 2606 OID 893752)
-- Name: insurance_companies insurance_companies_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_1 FOREIGN KEY (am_phone_number_id) REFERENCES vaxiom.contact_phones(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8113 (class 2606 OID 893757)
-- Name: insurance_companies insurance_companies_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_2 FOREIGN KEY (am_email_address_id) REFERENCES vaxiom.contact_emails(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8114 (class 2606 OID 893762)
-- Name: insurance_companies insurance_companies_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_3 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8115 (class 2606 OID 893767)
-- Name: insurance_companies insurance_companies_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_4 FOREIGN KEY (postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8116 (class 2606 OID 893772)
-- Name: insurance_companies insurance_companies_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_5 FOREIGN KEY (master_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8118 (class 2606 OID 893782)
-- Name: insurance_company_phone_associations insurance_company_phone_associations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_phone_associations
    ADD CONSTRAINT insurance_company_phone_associations_ibfk_1 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8119 (class 2606 OID 893787)
-- Name: insurance_company_phone_associations insurance_company_phone_associations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_phone_associations
    ADD CONSTRAINT insurance_company_phone_associations_ibfk_2 FOREIGN KEY (contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8122 (class 2606 OID 893802)
-- Name: insurance_payment_accounts insurance_payment_accounts_fk_pay_acount; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_accounts
    ADD CONSTRAINT insurance_payment_accounts_fk_pay_acount FOREIGN KEY (id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8123 (class 2606 OID 893807)
-- Name: insurance_payment_accounts insurance_payment_accounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_accounts
    ADD CONSTRAINT insurance_payment_accounts_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8124 (class 2606 OID 893812)
-- Name: insurance_payment_accounts insurance_payment_accounts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_accounts
    ADD CONSTRAINT insurance_payment_accounts_ibfk_2 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8138 (class 2606 OID 893882)
-- Name: insurance_plan_employers insurance_plan_employers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_employers
    ADD CONSTRAINT insurance_plan_employers_ibfk_1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.patient_insurance_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8139 (class 2606 OID 893887)
-- Name: insurance_plan_employers insurance_plan_employers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_employers
    ADD CONSTRAINT insurance_plan_employers_ibfk_2 FOREIGN KEY (employer_id) REFERENCES vaxiom.employers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8153 (class 2606 OID 893957)
-- Name: insurance_subscriptions insurance_subscriptions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_subscriptions
    ADD CONSTRAINT insurance_subscriptions_ibfk_1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.patient_insurance_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8154 (class 2606 OID 893962)
-- Name: insurance_subscriptions insurance_subscriptions_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_subscriptions
    ADD CONSTRAINT insurance_subscriptions_ibfk_2 FOREIGN KEY (employer_id) REFERENCES vaxiom.employers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8155 (class 2606 OID 893967)
-- Name: insurance_subscriptions insurance_subscriptions_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_subscriptions
    ADD CONSTRAINT insurance_subscriptions_ibfk_3 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8156 (class 2606 OID 893972)
-- Name: insured insured_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured
    ADD CONSTRAINT insured_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8157 (class 2606 OID 893977)
-- Name: insured insured_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured
    ADD CONSTRAINT insured_ibfk_2 FOREIGN KEY (insurance_subscription_id) REFERENCES vaxiom.insurance_subscriptions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8159 (class 2606 OID 893987)
-- Name: invoice_export_queue invoice_export_queue_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_export_queue
    ADD CONSTRAINT invoice_export_queue_ibfk_2 FOREIGN KEY (account_id) REFERENCES vaxiom.third_party_account(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8160 (class 2606 OID 893992)
-- Name: invoice_import_export_log invoice_import_export_log_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_import_export_log
    ADD CONSTRAINT invoice_import_export_log_ibfk_1 FOREIGN KEY (account_id) REFERENCES vaxiom.third_party_account(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8161 (class 2606 OID 893997)
-- Name: invoice_import_export_log invoice_import_export_log_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_import_export_log
    ADD CONSTRAINT invoice_import_export_log_ibfk_3 FOREIGN KEY (id) REFERENCES vaxiom.audit_logs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8272 (class 2606 OID 894552)
-- Name: permissions_user_role_job_title jobtitle_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_job_title
    ADD CONSTRAINT jobtitle_fk FOREIGN KEY (job_title_id) REFERENCES vaxiom.jobtitle(id);


--
-- TOC entry 8167 (class 2606 OID 894027)
-- Name: late_fee_charges late_fee_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.late_fee_charges
    ADD CONSTRAINT late_fee_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8187 (class 2606 OID 894127)
-- Name: legal_entity_tin legal_entity_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_tin
    ADD CONSTRAINT legal_entity_id FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8188 (class 2606 OID 894132)
-- Name: lightbarcalls lightbarcalls_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.lightbarcalls
    ADD CONSTRAINT lightbarcalls_ibfk_1 FOREIGN KEY (chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8189 (class 2606 OID 894137)
-- Name: lightbarcalls lightbarcalls_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.lightbarcalls
    ADD CONSTRAINT lightbarcalls_ibfk_2 FOREIGN KEY (booking_id) REFERENCES vaxiom.appointment_bookings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8190 (class 2606 OID 894142)
-- Name: lightbarcalls lightbarcalls_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.lightbarcalls
    ADD CONSTRAINT lightbarcalls_ibfk_3 FOREIGN KEY (resource_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8191 (class 2606 OID 894147)
-- Name: location_access_keys location_access_keys_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.location_access_keys
    ADD CONSTRAINT location_access_keys_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8074 (class 2606 OID 893562)
-- Name: employment_contract_location_group location_group_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_location_group
    ADD CONSTRAINT location_group_fk FOREIGN KEY (location_group_id) REFERENCES vaxiom.sys_organization_location_group(id);


--
-- TOC entry 8337 (class 2606 OID 894877)
-- Name: selected_appointment_templates location_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates
    ADD CONSTRAINT location_id FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8192 (class 2606 OID 894152)
-- Name: medical_history medical_history_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history
    ADD CONSTRAINT medical_history_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8193 (class 2606 OID 894157)
-- Name: medical_history_medications medical_history_medications_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_medications
    ADD CONSTRAINT medical_history_medications_ibfk_1 FOREIGN KEY (medication_id) REFERENCES vaxiom.medications(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8194 (class 2606 OID 894162)
-- Name: medical_history_medications medical_history_medications_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_medications
    ADD CONSTRAINT medical_history_medications_ibfk_2 FOREIGN KEY (medical_history_id) REFERENCES vaxiom.medical_history(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8197 (class 2606 OID 894177)
-- Name: migration_fix_adjustments migration_fix_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migration_fix_adjustments
    ADD CONSTRAINT migration_fix_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8199 (class 2606 OID 894187)
-- Name: misc_fee_charge_templates misc_fee_charge_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.misc_fee_charge_templates
    ADD CONSTRAINT misc_fee_charge_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8198 (class 2606 OID 894182)
-- Name: misc_fee_charges misc_fee_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.misc_fee_charges
    ADD CONSTRAINT misc_fee_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8200 (class 2606 OID 894192)
-- Name: my_reports_columns my_reports_columns_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports_columns
    ADD CONSTRAINT my_reports_columns_ibfk_1 FOREIGN KEY (report_id) REFERENCES vaxiom.my_reports(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8201 (class 2606 OID 894197)
-- Name: my_reports_filters my_reports_filters_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports_filters
    ADD CONSTRAINT my_reports_filters_ibfk_1 FOREIGN KEY (report_id) REFERENCES vaxiom.my_reports(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8202 (class 2606 OID 894202)
-- Name: network_fee_sheets network_fee_sheets_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_fee_sheets
    ADD CONSTRAINT network_fee_sheets_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8203 (class 2606 OID 894207)
-- Name: network_fee_sheets network_fee_sheets_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_fee_sheets
    ADD CONSTRAINT network_fee_sheets_ibfk_2 FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8204 (class 2606 OID 894212)
-- Name: network_sheet_fee network_sheet_fee_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_sheet_fee
    ADD CONSTRAINT network_sheet_fee_ibfk_1 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8205 (class 2606 OID 894217)
-- Name: network_sheet_fee network_sheet_fee_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_sheet_fee
    ADD CONSTRAINT network_sheet_fee_ibfk_2 FOREIGN KEY (network_fee_sheet_id) REFERENCES vaxiom.network_fee_sheets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8206 (class 2606 OID 894222)
-- Name: notebook_notes notebook_notes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.notebook_notes
    ADD CONSTRAINT notebook_notes_ibfk_1 FOREIGN KEY (notebook_id) REFERENCES vaxiom.notebooks(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8212 (class 2606 OID 894252)
-- Name: od_chair_break_hours od_chair_break_hours_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.od_chair_break_hours
    ADD CONSTRAINT od_chair_break_hours_ibfk_1 FOREIGN KEY (od_chair_id) REFERENCES vaxiom.office_day_chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8210 (class 2606 OID 894242)
-- Name: odt_chair_allocations odt_chair_allocations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chair_allocations
    ADD CONSTRAINT odt_chair_allocations_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8211 (class 2606 OID 894247)
-- Name: odt_chair_allocations odt_chair_allocations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chair_allocations
    ADD CONSTRAINT odt_chair_allocations_ibfk_2 FOREIGN KEY (odt_chair_id) REFERENCES vaxiom.odt_chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8207 (class 2606 OID 894227)
-- Name: odt_chairs odt_chairs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chairs
    ADD CONSTRAINT odt_chairs_ibfk_1 FOREIGN KEY (template_id) REFERENCES vaxiom.office_day_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8208 (class 2606 OID 894232)
-- Name: odt_chairs odt_chairs_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chairs
    ADD CONSTRAINT odt_chairs_ibfk_2 FOREIGN KEY (day_schedule_id) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8209 (class 2606 OID 894237)
-- Name: odt_chairs odt_chairs_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chairs
    ADD CONSTRAINT odt_chairs_ibfk_3 FOREIGN KEY (chair_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8216 (class 2606 OID 894272)
-- Name: office_day_chairs office_day_chairs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_chairs
    ADD CONSTRAINT office_day_chairs_ibfk_1 FOREIGN KEY (office_day_id) REFERENCES vaxiom.office_days(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8217 (class 2606 OID 894277)
-- Name: office_day_chairs office_day_chairs_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_chairs
    ADD CONSTRAINT office_day_chairs_ibfk_2 FOREIGN KEY (odt_chair_id) REFERENCES vaxiom.odt_chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8218 (class 2606 OID 894282)
-- Name: office_day_chairs office_day_chairs_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_chairs
    ADD CONSTRAINT office_day_chairs_ibfk_3 FOREIGN KEY (chair_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8219 (class 2606 OID 894287)
-- Name: office_day_templates office_day_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_templates
    ADD CONSTRAINT office_day_templates_ibfk_1 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8214 (class 2606 OID 894262)
-- Name: office_days office_days_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_days
    ADD CONSTRAINT office_days_ibfk_1 FOREIGN KEY (template_id) REFERENCES vaxiom.office_day_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8215 (class 2606 OID 894267)
-- Name: office_days office_days_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_days
    ADD CONSTRAINT office_days_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8213 (class 2606 OID 894257)
-- Name: offices offices_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.offices
    ADD CONSTRAINT offices_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8220 (class 2606 OID 894292)
-- Name: ortho_coverages ortho_coverages_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.ortho_coverages
    ADD CONSTRAINT ortho_coverages_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.tx_category_coverages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8221 (class 2606 OID 894297)
-- Name: ortho_insured ortho_insured_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.ortho_insured
    ADD CONSTRAINT ortho_insured_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.tx_category_insured(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8222 (class 2606 OID 894302)
-- Name: other_professional_relationships other_professional_relationships_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.other_professional_relationships
    ADD CONSTRAINT other_professional_relationships_ibfk_1 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8223 (class 2606 OID 894307)
-- Name: other_professional_relationships other_professional_relationships_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.other_professional_relationships
    ADD CONSTRAINT other_professional_relationships_ibfk_2 FOREIGN KEY (external_office_id) REFERENCES vaxiom.external_offices(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8224 (class 2606 OID 894312)
-- Name: other_professional_relationships other_professional_relationships_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.other_professional_relationships
    ADD CONSTRAINT other_professional_relationships_ibfk_3 FOREIGN KEY (employee_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8448 (class 2606 OID 895432)
-- Name: tx_statuses parent_company_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_statuses
    ADD CONSTRAINT parent_company_id FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8229 (class 2606 OID 894337)
-- Name: patient_images patient_images_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images
    ADD CONSTRAINT patient_images_ibfk_1 FOREIGN KEY (imageseries_id) REFERENCES vaxiom.patient_imageseries(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8230 (class 2606 OID 894342)
-- Name: patient_images patient_images_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images
    ADD CONSTRAINT patient_images_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8234 (class 2606 OID 894362)
-- Name: patient_images_layouts patient_images_layouts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images_layouts
    ADD CONSTRAINT patient_images_layouts_ibfk_1 FOREIGN KEY (organization_node_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8231 (class 2606 OID 894347)
-- Name: patient_imageseries patient_imageseries_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_imageseries
    ADD CONSTRAINT patient_imageseries_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8232 (class 2606 OID 894352)
-- Name: patient_imageseries patient_imageseries_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_imageseries
    ADD CONSTRAINT patient_imageseries_ibfk_2 FOREIGN KEY (treatment_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8233 (class 2606 OID 894357)
-- Name: patient_imageseries patient_imageseries_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_imageseries
    ADD CONSTRAINT patient_imageseries_ibfk_3 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8235 (class 2606 OID 894367)
-- Name: patient_insurance_plans patient_insurance_plans_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_insurance_plans
    ADD CONSTRAINT patient_insurance_plans_ibfk_2 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8236 (class 2606 OID 894372)
-- Name: patient_insurance_plans patient_insurance_plans_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_insurance_plans
    ADD CONSTRAINT patient_insurance_plans_ibfk_3 FOREIGN KEY (insurance_billing_center_id) REFERENCES vaxiom.insurance_billing_centers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8237 (class 2606 OID 894377)
-- Name: patient_insurance_plans patient_insurance_plans_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_insurance_plans
    ADD CONSTRAINT patient_insurance_plans_ibfk_4 FOREIGN KEY (master_id) REFERENCES vaxiom.patient_insurance_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8238 (class 2606 OID 894382)
-- Name: patient_ledger_history patient_ledger_history_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history
    ADD CONSTRAINT patient_ledger_history_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8239 (class 2606 OID 894387)
-- Name: patient_ledger_history patient_ledger_history_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history
    ADD CONSTRAINT patient_ledger_history_ibfk_2 FOREIGN KEY (tx_location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8240 (class 2606 OID 894392)
-- Name: patient_ledger_history_overdue_receivables patient_ledger_history_overdue_receivables_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history_overdue_receivables
    ADD CONSTRAINT patient_ledger_history_overdue_receivables_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8241 (class 2606 OID 894397)
-- Name: patient_ledger_history_overdue_receivables patient_ledger_history_overdue_receivables_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history_overdue_receivables
    ADD CONSTRAINT patient_ledger_history_overdue_receivables_ibfk_2 FOREIGN KEY (patient_ledger_history_id) REFERENCES vaxiom.patient_ledger_history(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8242 (class 2606 OID 894402)
-- Name: patient_locations patient_locations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_locations
    ADD CONSTRAINT patient_locations_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8243 (class 2606 OID 894407)
-- Name: patient_locations patient_locations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_locations
    ADD CONSTRAINT patient_locations_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8244 (class 2606 OID 894412)
-- Name: patient_person_referrals patient_person_referrals_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_person_referrals
    ADD CONSTRAINT patient_person_referrals_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8245 (class 2606 OID 894417)
-- Name: patient_person_referrals patient_person_referrals_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_person_referrals
    ADD CONSTRAINT patient_person_referrals_ibfk_2 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8246 (class 2606 OID 894422)
-- Name: patient_recently_opened_items patient_recently_opened_items_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_recently_opened_items
    ADD CONSTRAINT patient_recently_opened_items_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.recently_opened_items(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8247 (class 2606 OID 894427)
-- Name: patient_recently_opened_items patient_recently_opened_items_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_recently_opened_items
    ADD CONSTRAINT patient_recently_opened_items_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8248 (class 2606 OID 894432)
-- Name: patient_template_referrals patient_template_referrals_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_template_referrals
    ADD CONSTRAINT patient_template_referrals_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8249 (class 2606 OID 894437)
-- Name: patient_template_referrals patient_template_referrals_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_template_referrals
    ADD CONSTRAINT patient_template_referrals_ibfk_3 FOREIGN KEY (referral_template_id) REFERENCES vaxiom.referral_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8225 (class 2606 OID 894317)
-- Name: patients patients_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT patients_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8226 (class 2606 OID 894322)
-- Name: patients patients_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT patients_ibfk_2 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8227 (class 2606 OID 894327)
-- Name: patients patients_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT patients_ibfk_3 FOREIGN KEY (heard_from_patient_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8228 (class 2606 OID 894332)
-- Name: patients patients_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT patients_ibfk_4 FOREIGN KEY (heard_from_professional_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8259 (class 2606 OID 894487)
-- Name: payment_correction_details payment_correction_details_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_correction_details
    ADD CONSTRAINT payment_correction_details_ibfk_1 FOREIGN KEY (transaction_id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8260 (class 2606 OID 894492)
-- Name: payment_correction_details payment_correction_details_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_correction_details
    ADD CONSTRAINT payment_correction_details_ibfk_2 FOREIGN KEY (correction_type_id) REFERENCES vaxiom.correction_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8265 (class 2606 OID 894517)
-- Name: payment_plan_rollbacks payment_plan_rollbacks_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plan_rollbacks
    ADD CONSTRAINT payment_plan_rollbacks_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8266 (class 2606 OID 894522)
-- Name: payment_plan_rollbacks payment_plan_rollbacks_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plan_rollbacks
    ADD CONSTRAINT payment_plan_rollbacks_ibfk_2 FOREIGN KEY (payment_plan_id) REFERENCES vaxiom.payment_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8261 (class 2606 OID 894497)
-- Name: payment_plans payment_plans_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT payment_plans_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8262 (class 2606 OID 894502)
-- Name: payment_plans payment_plans_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT payment_plans_ibfk_2 FOREIGN KEY (bank_account_id) REFERENCES vaxiom.bank_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8263 (class 2606 OID 894507)
-- Name: payment_plans payment_plans_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT payment_plans_ibfk_3 FOREIGN KEY (invoice_contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8264 (class 2606 OID 894512)
-- Name: payment_plans payment_plans_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT payment_plans_ibfk_4 FOREIGN KEY (token_location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8267 (class 2606 OID 894527)
-- Name: payment_types payment_types_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types
    ADD CONSTRAINT payment_types_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8268 (class 2606 OID 894532)
-- Name: payment_types_locations payment_types_locations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types_locations
    ADD CONSTRAINT payment_types_locations_ibfk_1 FOREIGN KEY (payment_type_id) REFERENCES vaxiom.payment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8269 (class 2606 OID 894537)
-- Name: payment_types_locations payment_types_locations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types_locations
    ADD CONSTRAINT payment_types_locations_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8251 (class 2606 OID 894447)
-- Name: payments payments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_1 FOREIGN KEY (account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8252 (class 2606 OID 894452)
-- Name: payments payments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_2 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8253 (class 2606 OID 894457)
-- Name: payments payments_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_3 FOREIGN KEY (billing_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8254 (class 2606 OID 894462)
-- Name: payments payments_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_4 FOREIGN KEY (applied_payment_id) REFERENCES vaxiom.applied_payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8255 (class 2606 OID 894467)
-- Name: payments payments_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_5 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8256 (class 2606 OID 894472)
-- Name: payments payments_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_6 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8257 (class 2606 OID 894477)
-- Name: payments payments_ibfk_7; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_7 FOREIGN KEY (payment_type_id) REFERENCES vaxiom.payment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8258 (class 2606 OID 894482)
-- Name: payments payments_ibfk_8; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_8 FOREIGN KEY (insurance_company_payment_id) REFERENCES vaxiom.insurance_company_payments(insurance_company_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8274 (class 2606 OID 894562)
-- Name: permissions_user_role_permission permission_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_permission
    ADD CONSTRAINT permission_fk FOREIGN KEY (permissions_permission_id) REFERENCES vaxiom.permissions_permission(id);


--
-- TOC entry 8271 (class 2606 OID 894547)
-- Name: permissions_user_role_departament permissions_user_role_dep_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_departament
    ADD CONSTRAINT permissions_user_role_dep_fk FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id);


--
-- TOC entry 8273 (class 2606 OID 894557)
-- Name: permissions_user_role_job_title permissions_user_role_jt_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_job_title
    ADD CONSTRAINT permissions_user_role_jt_fk FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id);


--
-- TOC entry 8284 (class 2606 OID 894612)
-- Name: person_image_files person_image_files_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_image_files
    ADD CONSTRAINT person_image_files_ibfk_1 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8285 (class 2606 OID 894617)
-- Name: person_payment_accounts person_payment_accounts_fk_pay_acount; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_payment_accounts
    ADD CONSTRAINT person_payment_accounts_fk_pay_acount FOREIGN KEY (id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8286 (class 2606 OID 894622)
-- Name: person_payment_accounts person_payment_accounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_payment_accounts
    ADD CONSTRAINT person_payment_accounts_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8287 (class 2606 OID 894627)
-- Name: person_payment_accounts person_payment_accounts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_payment_accounts
    ADD CONSTRAINT person_payment_accounts_ibfk_2 FOREIGN KEY (payer_person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8279 (class 2606 OID 894587)
-- Name: persons persons_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_1 FOREIGN KEY (core_user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8280 (class 2606 OID 894592)
-- Name: persons persons_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_2 FOREIGN KEY (portal_user_id) REFERENCES vaxiom.sys_portal_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8281 (class 2606 OID 894597)
-- Name: persons persons_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_3 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8282 (class 2606 OID 894602)
-- Name: persons persons_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_4 FOREIGN KEY (global_person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8283 (class 2606 OID 894607)
-- Name: persons persons_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_5 FOREIGN KEY (patient_portal_user_id) REFERENCES vaxiom.sys_patient_portal_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8288 (class 2606 OID 894632)
-- Name: previous_enrollment_form previous_enrollment_form_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form
    ADD CONSTRAINT previous_enrollment_form_ibfk_1 FOREIGN KEY (employee_id) REFERENCES vaxiom.employment_contracts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8291 (class 2606 OID 894647)
-- Name: procedure_additional_resource_requirements procedure_additional_resource_requirements_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_additional_resource_requirements
    ADD CONSTRAINT procedure_additional_resource_requirements_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8292 (class 2606 OID 894652)
-- Name: procedure_additional_resource_requirements procedure_additional_resource_requirements_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_additional_resource_requirements
    ADD CONSTRAINT procedure_additional_resource_requirements_ibfk_2 FOREIGN KEY (resource_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8294 (class 2606 OID 894662)
-- Name: procedure_step_additional_resource_requirements procedure_step_additional_resource_requirements_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_additional_resource_requirements
    ADD CONSTRAINT procedure_step_additional_resource_requirements_ibfk_1 FOREIGN KEY (step_id) REFERENCES vaxiom.procedure_steps(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8295 (class 2606 OID 894667)
-- Name: procedure_step_additional_resource_requirements procedure_step_additional_resource_requirements_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_additional_resource_requirements
    ADD CONSTRAINT procedure_step_additional_resource_requirements_ibfk_2 FOREIGN KEY (resource_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8296 (class 2606 OID 894672)
-- Name: procedure_step_durations procedure_step_durations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_durations
    ADD CONSTRAINT procedure_step_durations_ibfk_1 FOREIGN KEY (step_id) REFERENCES vaxiom.procedure_steps(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8297 (class 2606 OID 894677)
-- Name: procedure_step_durations procedure_step_durations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_durations
    ADD CONSTRAINT procedure_step_durations_ibfk_2 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8293 (class 2606 OID 894657)
-- Name: procedure_steps procedure_steps_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_steps
    ADD CONSTRAINT procedure_steps_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8289 (class 2606 OID 894637)
-- Name: procedures procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedures
    ADD CONSTRAINT procedures_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8290 (class 2606 OID 894642)
-- Name: procedures procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedures
    ADD CONSTRAINT procedures_ibfk_2 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8298 (class 2606 OID 894682)
-- Name: professional_relationships professional_relationships_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.professional_relationships
    ADD CONSTRAINT professional_relationships_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8301 (class 2606 OID 894697)
-- Name: provider_network_insurance_companies provider_network_insurance_companies_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_network_insurance_companies
    ADD CONSTRAINT provider_network_insurance_companies_ibfk_1 FOREIGN KEY (network_fee_sheet_id) REFERENCES vaxiom.network_fee_sheets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8302 (class 2606 OID 894702)
-- Name: provider_network_insurance_companies provider_network_insurance_companies_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_network_insurance_companies
    ADD CONSTRAINT provider_network_insurance_companies_ibfk_2 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8303 (class 2606 OID 894707)
-- Name: questionnaire_answers questionnaire_answers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.questionnaire_answers
    ADD CONSTRAINT questionnaire_answers_ibfk_1 FOREIGN KEY (question_id) REFERENCES vaxiom.question(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8304 (class 2606 OID 894712)
-- Name: questionnaire_answers questionnaire_answers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.questionnaire_answers
    ADD CONSTRAINT questionnaire_answers_ibfk_2 FOREIGN KEY (answer_id) REFERENCES vaxiom.answers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8305 (class 2606 OID 894717)
-- Name: reachify_users reachify_users_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.reachify_users
    ADD CONSTRAINT reachify_users_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8306 (class 2606 OID 894722)
-- Name: receivables receivables_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_1 FOREIGN KEY (treatment_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8307 (class 2606 OID 894727)
-- Name: receivables receivables_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8308 (class 2606 OID 894732)
-- Name: receivables receivables_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_3 FOREIGN KEY (payment_account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8309 (class 2606 OID 894737)
-- Name: receivables receivables_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_4 FOREIGN KEY (insured_id) REFERENCES vaxiom.insured(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8310 (class 2606 OID 894742)
-- Name: receivables receivables_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_5 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8311 (class 2606 OID 894747)
-- Name: receivables receivables_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_6 FOREIGN KEY (primary_prsn_payer_inv_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8312 (class 2606 OID 894752)
-- Name: receivables receivables_ibfk_7; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_7 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8313 (class 2606 OID 894757)
-- Name: receivables receivables_ibfk_8; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_8 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8367 (class 2606 OID 895027)
-- Name: tasks recipient_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT recipient_id FOREIGN KEY (recipient_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8314 (class 2606 OID 894762)
-- Name: referral_list_values referral_list_values_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_list_values
    ADD CONSTRAINT referral_list_values_ibfk_1 FOREIGN KEY (referral_template_id) REFERENCES vaxiom.referral_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8315 (class 2606 OID 894767)
-- Name: referral_templates referral_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_templates
    ADD CONSTRAINT referral_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8316 (class 2606 OID 894772)
-- Name: referral_templates referral_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_templates
    ADD CONSTRAINT referral_templates_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8318 (class 2606 OID 894782)
-- Name: refund_details refund_details_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.refund_details
    ADD CONSTRAINT refund_details_ibfk_1 FOREIGN KEY (transaction_id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8317 (class 2606 OID 894777)
-- Name: refunds refunds_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.refunds
    ADD CONSTRAINT refunds_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8319 (class 2606 OID 894787)
-- Name: relationships relationships_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.relationships
    ADD CONSTRAINT relationships_ibfk_1 FOREIGN KEY (from_person) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8320 (class 2606 OID 894792)
-- Name: relationships relationships_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.relationships
    ADD CONSTRAINT relationships_ibfk_2 FOREIGN KEY (to_person) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8321 (class 2606 OID 894797)
-- Name: resources resources_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resources
    ADD CONSTRAINT resources_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8322 (class 2606 OID 894802)
-- Name: resources resources_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resources
    ADD CONSTRAINT resources_ibfk_2 FOREIGN KEY (resource_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8323 (class 2606 OID 894807)
-- Name: resources resources_ifbk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resources
    ADD CONSTRAINT resources_ifbk_3 FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8324 (class 2606 OID 894812)
-- Name: retail_fees retail_fees_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_fees
    ADD CONSTRAINT retail_fees_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8325 (class 2606 OID 894817)
-- Name: retail_fees retail_fees_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_fees
    ADD CONSTRAINT retail_fees_ibfk_2 FOREIGN KEY (retail_item_id) REFERENCES vaxiom.retail_items(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8326 (class 2606 OID 894822)
-- Name: retail_items retail_items_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_items
    ADD CONSTRAINT retail_items_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8327 (class 2606 OID 894827)
-- Name: reversed_payments reversed_payments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.reversed_payments
    ADD CONSTRAINT reversed_payments_ibfk_1 FOREIGN KEY (payment_id) REFERENCES vaxiom.payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8329 (class 2606 OID 894837)
-- Name: schedule_conflict_permission_bypass schedule_conflict_permission_bypass_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_conflict_permission_bypass
    ADD CONSTRAINT schedule_conflict_permission_bypass_ibfk_1 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8330 (class 2606 OID 894842)
-- Name: schedule_notes schedule_notes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_notes
    ADD CONSTRAINT schedule_notes_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8331 (class 2606 OID 894847)
-- Name: schedule_notes schedule_notes_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_notes
    ADD CONSTRAINT schedule_notes_ibfk_2 FOREIGN KEY (chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8328 (class 2606 OID 894832)
-- Name: scheduled_task scheduled_task_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduled_task
    ADD CONSTRAINT scheduled_task_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8332 (class 2606 OID 894852)
-- Name: scheduling_preferences scheduling_preferences_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferences
    ADD CONSTRAINT scheduling_preferences_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8333 (class 2606 OID 894857)
-- Name: scheduling_preferences_week_days scheduling_preferences_week_days_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferences_week_days
    ADD CONSTRAINT scheduling_preferences_week_days_ibfk_1 FOREIGN KEY (scheduling_preferences_id) REFERENCES vaxiom.scheduling_preferences(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8334 (class 2606 OID 894862)
-- Name: scheduling_preferred_employees scheduling_preferred_employees_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferred_employees
    ADD CONSTRAINT scheduling_preferred_employees_ibfk_1 FOREIGN KEY (preference_id) REFERENCES vaxiom.scheduling_preferences(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8335 (class 2606 OID 894867)
-- Name: scheduling_preferred_employees scheduling_preferred_employees_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferred_employees
    ADD CONSTRAINT scheduling_preferred_employees_ibfk_2 FOREIGN KEY (employee_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8343 (class 2606 OID 894907)
-- Name: selfcheckin_settings_forbidden_types selfcheckin_settings_forbidden_types_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selfcheckin_settings_forbidden_types
    ADD CONSTRAINT selfcheckin_settings_forbidden_types_ibfk_1 FOREIGN KEY (location_access_key_id) REFERENCES vaxiom.location_access_keys(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 8344 (class 2606 OID 894912)
-- Name: selfcheckin_settings_forbidden_types selfcheckin_settings_forbidden_types_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selfcheckin_settings_forbidden_types
    ADD CONSTRAINT selfcheckin_settings_forbidden_types_ibfk_2 FOREIGN KEY (appointment_template_id) REFERENCES vaxiom.appointment_templates(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 8342 (class 2606 OID 894902)
-- Name: selfcheckin_settings selfcheckin_settings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selfcheckin_settings
    ADD CONSTRAINT selfcheckin_settings_ibfk_1 FOREIGN KEY (location_access_key_id) REFERENCES vaxiom.location_access_keys(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 8345 (class 2606 OID 894917)
-- Name: simultaneous_appointment_values simultaneous_appointment_values_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.simultaneous_appointment_values
    ADD CONSTRAINT simultaneous_appointment_values_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8346 (class 2606 OID 894922)
-- Name: simultaneous_appointment_values simultaneous_appointment_values_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.simultaneous_appointment_values
    ADD CONSTRAINT simultaneous_appointment_values_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8347 (class 2606 OID 894927)
-- Name: sms_part sms_part_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sms_part
    ADD CONSTRAINT sms_part_ibfk_1 FOREIGN KEY (sms_id) REFERENCES vaxiom.sms(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8348 (class 2606 OID 894932)
-- Name: statements statements_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.statements
    ADD CONSTRAINT statements_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8349 (class 2606 OID 894937)
-- Name: statements statements_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.statements
    ADD CONSTRAINT statements_ibfk_3 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8366 (class 2606 OID 895022)
-- Name: sys_organization_settings sys_organization_settings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_settings
    ADD CONSTRAINT sys_organization_settings_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8350 (class 2606 OID 894942)
-- Name: sys_organizations sys_organizations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organizations
    ADD CONSTRAINT sys_organizations_ibfk_1 FOREIGN KEY (postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8351 (class 2606 OID 894947)
-- Name: sys_organizations sys_organizations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organizations
    ADD CONSTRAINT sys_organizations_ibfk_2 FOREIGN KEY (parent_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8376 (class 2606 OID 895072)
-- Name: task_basket_subscribers task_basket_subscribers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_basket_subscribers
    ADD CONSTRAINT task_basket_subscribers_ibfk_1 FOREIGN KEY (task_basket_id) REFERENCES vaxiom.task_baskets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8377 (class 2606 OID 895077)
-- Name: task_basket_subscribers task_basket_subscribers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_basket_subscribers
    ADD CONSTRAINT task_basket_subscribers_ibfk_2 FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8375 (class 2606 OID 895067)
-- Name: task_baskets task_baskets_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_baskets
    ADD CONSTRAINT task_baskets_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8378 (class 2606 OID 895082)
-- Name: task_contacts task_contacts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_contacts
    ADD CONSTRAINT task_contacts_ibfk_1 FOREIGN KEY (task_id) REFERENCES vaxiom.tasks(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8379 (class 2606 OID 895087)
-- Name: task_contacts task_contacts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_contacts
    ADD CONSTRAINT task_contacts_ibfk_2 FOREIGN KEY (contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8380 (class 2606 OID 895092)
-- Name: task_events task_events_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_events
    ADD CONSTRAINT task_events_ibfk_1 FOREIGN KEY (assignee_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8381 (class 2606 OID 895097)
-- Name: task_events task_events_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_events
    ADD CONSTRAINT task_events_ibfk_2 FOREIGN KEY (task_id) REFERENCES vaxiom.tasks(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8368 (class 2606 OID 895032)
-- Name: tasks tasks_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8369 (class 2606 OID 895037)
-- Name: tasks tasks_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_2 FOREIGN KEY (document_template_id) REFERENCES vaxiom.document_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8370 (class 2606 OID 895042)
-- Name: tasks tasks_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_3 FOREIGN KEY (document_tree_node_id) REFERENCES vaxiom.document_tree_nodes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8371 (class 2606 OID 895047)
-- Name: tasks tasks_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_4 FOREIGN KEY (provider_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8372 (class 2606 OID 895052)
-- Name: tasks tasks_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_5 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8373 (class 2606 OID 895057)
-- Name: tasks tasks_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_6 FOREIGN KEY (assignee_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8374 (class 2606 OID 895062)
-- Name: tasks tasks_ibfk_7; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_7 FOREIGN KEY (task_basket_id) REFERENCES vaxiom.task_baskets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8385 (class 2606 OID 895117)
-- Name: temp_accounts temp_accounts_fk_pay_acount; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_fk_pay_acount FOREIGN KEY (id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8386 (class 2606 OID 895122)
-- Name: temp_accounts temp_accounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8387 (class 2606 OID 895127)
-- Name: temp_accounts temp_accounts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_ibfk_2 FOREIGN KEY (person_payer_id) REFERENCES vaxiom.tx_payers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8388 (class 2606 OID 895132)
-- Name: temp_accounts temp_accounts_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_ibfk_3 FOREIGN KEY (insurance_payer_id) REFERENCES vaxiom.tx_payers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8389 (class 2606 OID 895137)
-- Name: temp_accounts temp_accounts_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_ibfk_4 FOREIGN KEY (tx_plan_id) REFERENCES vaxiom.tx_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8382 (class 2606 OID 895102)
-- Name: temporary_images temporary_images_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temporary_images
    ADD CONSTRAINT temporary_images_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8383 (class 2606 OID 895107)
-- Name: temporary_images temporary_images_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temporary_images
    ADD CONSTRAINT temporary_images_ibfk_2 FOREIGN KEY (patient_image_id) REFERENCES vaxiom.patient_images(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8384 (class 2606 OID 895112)
-- Name: temporary_images temporary_images_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temporary_images
    ADD CONSTRAINT temporary_images_ibfk_3 FOREIGN KEY (organization_node_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8390 (class 2606 OID 895142)
-- Name: third_party_account third_party_account_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.third_party_account
    ADD CONSTRAINT third_party_account_ibfk_1 FOREIGN KEY (organization_node_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8395 (class 2606 OID 895167)
-- Name: tooth_marking tooth_marking_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tooth_marking
    ADD CONSTRAINT tooth_marking_ibfk_1 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.toothchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8391 (class 2606 OID 895147)
-- Name: toothchart_edit_log toothchart_edit_log_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_edit_log
    ADD CONSTRAINT toothchart_edit_log_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.audit_logs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8392 (class 2606 OID 895152)
-- Name: toothchart_edit_log toothchart_edit_log_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_edit_log
    ADD CONSTRAINT toothchart_edit_log_ibfk_2 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.toothchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8393 (class 2606 OID 895157)
-- Name: toothchart_note toothchart_note_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_note
    ADD CONSTRAINT toothchart_note_ibfk_1 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.toothchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8394 (class 2606 OID 895162)
-- Name: toothchart_snapshot toothchart_snapshot_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_snapshot
    ADD CONSTRAINT toothchart_snapshot_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8396 (class 2606 OID 895172)
-- Name: transactions transactions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transactions
    ADD CONSTRAINT transactions_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8397 (class 2606 OID 895177)
-- Name: transfer_charges transfer_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transfer_charges
    ADD CONSTRAINT transfer_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8398 (class 2606 OID 895182)
-- Name: transfer_charges transfer_charges_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transfer_charges
    ADD CONSTRAINT transfer_charges_ibfk_2 FOREIGN KEY (charge_transfer_adjustment_id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8406 (class 2606 OID 895222)
-- Name: tx_card_field_definitions tx_card_field_definitions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_definitions
    ADD CONSTRAINT tx_card_field_definitions_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8407 (class 2606 OID 895227)
-- Name: tx_card_field_definitions tx_card_field_definitions_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_definitions
    ADD CONSTRAINT tx_card_field_definitions_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.tx_card_field_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8408 (class 2606 OID 895232)
-- Name: tx_card_field_options tx_card_field_options_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_options
    ADD CONSTRAINT tx_card_field_options_ibfk_1 FOREIGN KEY (field_type_id) REFERENCES vaxiom.tx_card_field_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8409 (class 2606 OID 895237)
-- Name: tx_card_templates tx_card_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_templates
    ADD CONSTRAINT tx_card_templates_ibfk_1 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8410 (class 2606 OID 895242)
-- Name: tx_card_templates tx_card_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_templates
    ADD CONSTRAINT tx_card_templates_ibfk_2 FOREIGN KEY (tx_plan_template_id) REFERENCES vaxiom.tx_plan_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8404 (class 2606 OID 895212)
-- Name: tx_cards tx_cards_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_cards
    ADD CONSTRAINT tx_cards_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8405 (class 2606 OID 895217)
-- Name: tx_cards tx_cards_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_cards
    ADD CONSTRAINT tx_cards_ibfk_2 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8411 (class 2606 OID 895247)
-- Name: tx_category_coverages tx_category_coverages_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_coverages
    ADD CONSTRAINT tx_category_coverages_ibfk_1 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8412 (class 2606 OID 895252)
-- Name: tx_category_coverages tx_category_coverages_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_coverages
    ADD CONSTRAINT tx_category_coverages_ibfk_2 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.patient_insurance_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8413 (class 2606 OID 895257)
-- Name: tx_category_insured tx_category_insured_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_insured
    ADD CONSTRAINT tx_category_insured_ibfk_1 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8414 (class 2606 OID 895262)
-- Name: tx_category_insured tx_category_insured_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_insured
    ADD CONSTRAINT tx_category_insured_ibfk_2 FOREIGN KEY (insured_id) REFERENCES vaxiom.insured(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8415 (class 2606 OID 895267)
-- Name: tx_contracts tx_contracts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_contracts
    ADD CONSTRAINT tx_contracts_ibfk_1 FOREIGN KEY (tx_payer_id) REFERENCES vaxiom.tx_payers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8416 (class 2606 OID 895272)
-- Name: tx_fee_charges tx_fee_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_fee_charges
    ADD CONSTRAINT tx_fee_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8417 (class 2606 OID 895277)
-- Name: tx_fee_charges tx_fee_charges_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_fee_charges
    ADD CONSTRAINT tx_fee_charges_ibfk_2 FOREIGN KEY (tx_plan_id) REFERENCES vaxiom.tx_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8418 (class 2606 OID 895282)
-- Name: tx_fee_charges tx_fee_charges_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_fee_charges
    ADD CONSTRAINT tx_fee_charges_ibfk_3 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8419 (class 2606 OID 895287)
-- Name: tx_payers tx_payers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_1 FOREIGN KEY (tx_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8420 (class 2606 OID 895292)
-- Name: tx_payers tx_payers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_2 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8421 (class 2606 OID 895297)
-- Name: tx_payers tx_payers_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_3 FOREIGN KEY (contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8422 (class 2606 OID 895302)
-- Name: tx_payers tx_payers_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_4 FOREIGN KEY (insured_id) REFERENCES vaxiom.insured(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8423 (class 2606 OID 895307)
-- Name: tx_payers tx_payers_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_5 FOREIGN KEY (account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8432 (class 2606 OID 895352)
-- Name: tx_plan_appointment_procedures tx_plan_appointment_procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointment_procedures
    ADD CONSTRAINT tx_plan_appointment_procedures_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8433 (class 2606 OID 895357)
-- Name: tx_plan_appointment_procedures tx_plan_appointment_procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointment_procedures
    ADD CONSTRAINT tx_plan_appointment_procedures_ibfk_2 FOREIGN KEY (appointment_id) REFERENCES vaxiom.tx_plan_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8427 (class 2606 OID 895327)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_1 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8428 (class 2606 OID 895332)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_2 FOREIGN KEY (tx_plan_id) REFERENCES vaxiom.tx_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8429 (class 2606 OID 895337)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_3 FOREIGN KEY (next_appointment_id) REFERENCES vaxiom.tx_plan_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8430 (class 2606 OID 895342)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_4 FOREIGN KEY (tx_plan_template_appointment_id) REFERENCES vaxiom.tx_plan_template_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8431 (class 2606 OID 895347)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_5 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8434 (class 2606 OID 895362)
-- Name: tx_plan_groups tx_plan_groups_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_groups
    ADD CONSTRAINT tx_plan_groups_ibfk_1 FOREIGN KEY (pc_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8437 (class 2606 OID 895377)
-- Name: tx_plan_note_groups tx_plan_note_groups_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_note_groups
    ADD CONSTRAINT tx_plan_note_groups_ibfk_1 FOREIGN KEY (tx_plan_note_id) REFERENCES vaxiom.tx_plan_notes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8435 (class 2606 OID 895367)
-- Name: tx_plan_notes tx_plan_notes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_notes
    ADD CONSTRAINT tx_plan_notes_ibfk_1 FOREIGN KEY (tx_card_id) REFERENCES vaxiom.tx_cards(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8436 (class 2606 OID 895372)
-- Name: tx_plan_notes tx_plan_notes_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_notes
    ADD CONSTRAINT tx_plan_notes_ibfk_2 FOREIGN KEY (diagnosis_id) REFERENCES vaxiom.diagnosis(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8446 (class 2606 OID 895422)
-- Name: tx_plan_template_appointment_procedures tx_plan_template_appointment_procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointment_procedures
    ADD CONSTRAINT tx_plan_template_appointment_procedures_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8447 (class 2606 OID 895427)
-- Name: tx_plan_template_appointment_procedures tx_plan_template_appointment_procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointment_procedures
    ADD CONSTRAINT tx_plan_template_appointment_procedures_ibfk_2 FOREIGN KEY (appointment_id) REFERENCES vaxiom.tx_plan_template_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8442 (class 2606 OID 895402)
-- Name: tx_plan_template_appointments tx_plan_template_appointments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT tx_plan_template_appointments_ibfk_1 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8443 (class 2606 OID 895407)
-- Name: tx_plan_template_appointments tx_plan_template_appointments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT tx_plan_template_appointments_ibfk_2 FOREIGN KEY (tx_plan_template_id) REFERENCES vaxiom.tx_plan_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8444 (class 2606 OID 895412)
-- Name: tx_plan_template_appointments tx_plan_template_appointments_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT tx_plan_template_appointments_ibfk_3 FOREIGN KEY (next_appointment_id) REFERENCES vaxiom.tx_plan_template_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8445 (class 2606 OID 895417)
-- Name: tx_plan_template_appointments tx_plan_template_appointments_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT tx_plan_template_appointments_ibfk_4 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8438 (class 2606 OID 895382)
-- Name: tx_plan_templates tx_plan_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT tx_plan_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8439 (class 2606 OID 895387)
-- Name: tx_plan_templates tx_plan_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT tx_plan_templates_ibfk_2 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8440 (class 2606 OID 895392)
-- Name: tx_plan_templates tx_plan_templates_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT tx_plan_templates_ibfk_3 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8441 (class 2606 OID 895397)
-- Name: tx_plan_templates tx_plan_templates_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT tx_plan_templates_ibfk_4 FOREIGN KEY (tx_plan_group_id) REFERENCES vaxiom.tx_plan_groups(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8424 (class 2606 OID 895312)
-- Name: tx_plans tx_plans_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plans
    ADD CONSTRAINT tx_plans_ibfk_1 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8425 (class 2606 OID 895317)
-- Name: tx_plans tx_plans_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plans
    ADD CONSTRAINT tx_plans_ibfk_2 FOREIGN KEY (tx_plan_template_id) REFERENCES vaxiom.tx_plan_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8426 (class 2606 OID 895322)
-- Name: tx_plans tx_plans_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plans
    ADD CONSTRAINT tx_plans_ibfk_3 FOREIGN KEY (tx_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8399 (class 2606 OID 895187)
-- Name: txs txs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT txs_ibfk_1 FOREIGN KEY (tx_card_id) REFERENCES vaxiom.tx_cards(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8400 (class 2606 OID 895192)
-- Name: txs txs_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT txs_ibfk_2 FOREIGN KEY (tx_plan_id) REFERENCES vaxiom.tx_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8401 (class 2606 OID 895197)
-- Name: txs txs_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT txs_ibfk_3 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8402 (class 2606 OID 895202)
-- Name: txs txs_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT txs_ibfk_4 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8403 (class 2606 OID 895207)
-- Name: txs_state_changes txs_state_changes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs_state_changes
    ADD CONSTRAINT txs_state_changes_ibfk_1 FOREIGN KEY (treatment_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8449 (class 2606 OID 895437)
-- Name: unapplied_payments unapplied_payments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.unapplied_payments
    ADD CONSTRAINT unapplied_payments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8450 (class 2606 OID 895442)
-- Name: unapplied_payments unapplied_payments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.unapplied_payments
    ADD CONSTRAINT unapplied_payments_ibfk_2 FOREIGN KEY (applied_payment_id) REFERENCES vaxiom.applied_payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8338 (class 2606 OID 894882)
-- Name: selected_appointment_templates user_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id);


--
-- TOC entry 8275 (class 2606 OID 894567)
-- Name: permissions_user_role_permission user_role_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_permission
    ADD CONSTRAINT user_role_fk FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id);


--
-- TOC entry 8451 (class 2606 OID 895447)
-- Name: week_templates week_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_1 FOREIGN KEY (fri) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8452 (class 2606 OID 895452)
-- Name: week_templates week_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8453 (class 2606 OID 895457)
-- Name: week_templates week_templates_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_3 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8454 (class 2606 OID 895462)
-- Name: week_templates week_templates_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_4 FOREIGN KEY (thu) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8455 (class 2606 OID 895467)
-- Name: week_templates week_templates_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_5 FOREIGN KEY (sun) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8456 (class 2606 OID 895472)
-- Name: week_templates week_templates_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_6 FOREIGN KEY (tue) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8457 (class 2606 OID 895477)
-- Name: week_templates week_templates_ibfk_7; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_7 FOREIGN KEY (mon) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8458 (class 2606 OID 895482)
-- Name: week_templates week_templates_ibfk_8; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_8 FOREIGN KEY (wed) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8459 (class 2606 OID 895487)
-- Name: week_templates week_templates_ibfk_9; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_9 FOREIGN KEY (sat) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8463 (class 2606 OID 895507)
-- Name: work_hour_comments work_hour_comments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hour_comments
    ADD CONSTRAINT work_hour_comments_ibfk_1 FOREIGN KEY (work_hours_id) REFERENCES vaxiom.work_hours(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8464 (class 2606 OID 895512)
-- Name: work_hour_comments work_hour_comments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hour_comments
    ADD CONSTRAINT work_hour_comments_ibfk_2 FOREIGN KEY (core_user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8460 (class 2606 OID 895492)
-- Name: work_hours work_hours_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hours
    ADD CONSTRAINT work_hours_ibfk_1 FOREIGN KEY (core_user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8461 (class 2606 OID 895497)
-- Name: work_hours work_hours_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hours
    ADD CONSTRAINT work_hours_ibfk_2 FOREIGN KEY (modified_by_admin) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8462 (class 2606 OID 895502)
-- Name: work_hours work_hours_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hours
    ADD CONSTRAINT work_hours_ibfk_3 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8465 (class 2606 OID 895517)
-- Name: work_schedule_days work_schedule_days_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_schedule_days
    ADD CONSTRAINT work_schedule_days_ibfk_1 FOREIGN KEY (day_schedule_id) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8466 (class 2606 OID 895522)
-- Name: work_schedule_days work_schedule_days_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_schedule_days
    ADD CONSTRAINT work_schedule_days_ibfk_2 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8467 (class 2606 OID 895527)
-- Name: writeoff_adjustments writeoff_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.writeoff_adjustments
    ADD CONSTRAINT writeoff_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2020-03-10 15:12:22 CET

--
-- PostgreSQL database dump complete
--

