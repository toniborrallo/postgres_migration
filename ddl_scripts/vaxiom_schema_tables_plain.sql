--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

-- Started on 2020-03-10 15:12:20 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 889331)
-- Name: vaxiom; Type: SCHEMA; Schema: -; Owner: postgres
--

DROP SCHEMA IF EXISTS vaxiom CASCADE;
CREATE SCHEMA vaxiom;


ALTER SCHEMA vaxiom OWNER TO postgres;

--
-- TOC entry 1063 (class 1255 OID 895532)
-- Name: on_update_current_timestamp_act_evt_log(); Type: FUNCTION; Schema: vaxiom; Owner: postgres
--

CREATE FUNCTION vaxiom.on_update_current_timestamp_act_evt_log() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.time_stamp_ = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION vaxiom.on_update_current_timestamp_act_evt_log() OWNER TO postgres;

--
-- TOC entry 1064 (class 1255 OID 895534)
-- Name: on_update_current_timestamp_act_re_deployment(); Type: FUNCTION; Schema: vaxiom; Owner: postgres
--

CREATE FUNCTION vaxiom.on_update_current_timestamp_act_re_deployment() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.deploy_time_ = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION vaxiom.on_update_current_timestamp_act_re_deployment() OWNER TO postgres;

--
-- TOC entry 1065 (class 1255 OID 895536)
-- Name: on_update_current_timestamp_act_ru_task(); Type: FUNCTION; Schema: vaxiom; Owner: postgres
--

CREATE FUNCTION vaxiom.on_update_current_timestamp_act_ru_task() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.create_time_ = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION vaxiom.on_update_current_timestamp_act_ru_task() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 637 (class 1259 OID 889343)
-- Name: act_evt_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_evt_log (
    log_nr_ bigint NOT NULL,
    type_ character varying(64),
    proc_def_id_ character varying(64),
    proc_inst_id_ character varying(64),
    execution_id_ character varying(64),
    task_id_ character varying(64),
    time_stamp_ timestamp with time zone,
    user_id_ character varying(255),
    data_ bytea,
    lock_owner_ character varying(255),
    lock_time_ timestamp with time zone,
    is_processed_ boolean DEFAULT false
);


ALTER TABLE vaxiom.act_evt_log OWNER TO postgres;

--
-- TOC entry 636 (class 1259 OID 889341)
-- Name: act_evt_log_log_nr__seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.act_evt_log_log_nr__seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.act_evt_log_log_nr__seq OWNER TO postgres;

--
-- TOC entry 9014 (class 0 OID 0)
-- Dependencies: 636
-- Name: act_evt_log_log_nr__seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.act_evt_log_log_nr__seq OWNED BY vaxiom.act_evt_log.log_nr_;


--
-- TOC entry 638 (class 1259 OID 889351)
-- Name: act_ge_bytearray; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ge_bytearray (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    name_ character varying(255),
    deployment_id_ character varying(64),
    bytes_ bytea,
    generated_ boolean
);


ALTER TABLE vaxiom.act_ge_bytearray OWNER TO postgres;

--
-- TOC entry 639 (class 1259 OID 889357)
-- Name: act_ge_property; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ge_property (
    name_ character varying(64) NOT NULL,
    value_ character varying(300),
    rev_ integer
);


ALTER TABLE vaxiom.act_ge_property OWNER TO postgres;

--
-- TOC entry 640 (class 1259 OID 889360)
-- Name: act_hi_actinst; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_actinst (
    id_ character varying(64) NOT NULL,
    proc_def_id_ character varying(64) NOT NULL,
    proc_inst_id_ character varying(64) NOT NULL,
    execution_id_ character varying(64) NOT NULL,
    act_id_ character varying(255) NOT NULL,
    task_id_ character varying(64),
    call_proc_inst_id_ character varying(64),
    act_name_ character varying(255),
    act_type_ character varying(255) NOT NULL,
    assignee_ character varying(255),
    start_time_ timestamp without time zone,
    end_time_ timestamp without time zone,
    duration_ numeric(20,0),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_hi_actinst OWNER TO postgres;

--
-- TOC entry 641 (class 1259 OID 889367)
-- Name: act_hi_attachment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_attachment (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    user_id_ character varying(255),
    name_ character varying(255),
    description_ character varying(4000),
    type_ character varying(255),
    task_id_ character varying(64),
    proc_inst_id_ character varying(64),
    url_ character varying(4000),
    content_id_ character varying(64),
    time_ timestamp without time zone
);


ALTER TABLE vaxiom.act_hi_attachment OWNER TO postgres;

--
-- TOC entry 642 (class 1259 OID 889373)
-- Name: act_hi_comment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_comment (
    id_ character varying(64) NOT NULL,
    type_ character varying(255),
    time_ timestamp without time zone,
    user_id_ character varying(255),
    task_id_ character varying(64),
    proc_inst_id_ character varying(64),
    action_ character varying(255),
    message_ character varying(4000),
    full_msg_ bytea
);


ALTER TABLE vaxiom.act_hi_comment OWNER TO postgres;

--
-- TOC entry 643 (class 1259 OID 889379)
-- Name: act_hi_detail; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_detail (
    id_ character varying(64) NOT NULL,
    type_ character varying(255) NOT NULL,
    proc_inst_id_ character varying(64),
    execution_id_ character varying(64),
    task_id_ character varying(64),
    act_inst_id_ character varying(64),
    name_ character varying(255) NOT NULL,
    var_type_ character varying(255),
    rev_ integer,
    time_ timestamp without time zone,
    bytearray_id_ character varying(64),
    double_ double precision,
    long_ numeric(20,0),
    text_ character varying(4000),
    text2_ character varying(4000)
);


ALTER TABLE vaxiom.act_hi_detail OWNER TO postgres;

--
-- TOC entry 644 (class 1259 OID 889385)
-- Name: act_hi_identitylink; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_identitylink (
    id_ character varying(64) NOT NULL,
    group_id_ character varying(255),
    type_ character varying(255),
    user_id_ character varying(255),
    task_id_ character varying(64),
    proc_inst_id_ character varying(64)
);


ALTER TABLE vaxiom.act_hi_identitylink OWNER TO postgres;

--
-- TOC entry 645 (class 1259 OID 889391)
-- Name: act_hi_procinst; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_procinst (
    id_ character varying(64) NOT NULL,
    proc_inst_id_ character varying(64) NOT NULL,
    business_key_ character varying(255),
    proc_def_id_ character varying(64) NOT NULL,
    start_time_ timestamp without time zone,
    end_time_ timestamp without time zone,
    duration_ numeric(20,0),
    start_user_id_ character varying(255),
    start_act_id_ character varying(255),
    end_act_id_ character varying(255),
    super_process_instance_id_ character varying(64),
    delete_reason_ character varying(4000),
    tenant_id_ character varying(255) DEFAULT ''::character varying,
    name_ character varying(255)
);


ALTER TABLE vaxiom.act_hi_procinst OWNER TO postgres;

--
-- TOC entry 646 (class 1259 OID 889398)
-- Name: act_hi_taskinst; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_taskinst (
    id_ character varying(64) NOT NULL,
    proc_def_id_ character varying(64),
    task_def_key_ character varying(255),
    proc_inst_id_ character varying(64),
    execution_id_ character varying(64),
    name_ character varying(255),
    parent_task_id_ character varying(64),
    description_ character varying(4000),
    owner_ character varying(255),
    assignee_ character varying(255),
    start_time_ timestamp without time zone,
    claim_time_ timestamp without time zone,
    end_time_ timestamp without time zone,
    duration_ numeric(20,0),
    delete_reason_ character varying(4000),
    priority_ integer,
    due_date_ timestamp without time zone,
    form_key_ character varying(255),
    category_ character varying(255),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_hi_taskinst OWNER TO postgres;

--
-- TOC entry 647 (class 1259 OID 889405)
-- Name: act_hi_varinst; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_varinst (
    id_ character varying(64) NOT NULL,
    proc_inst_id_ character varying(64),
    execution_id_ character varying(64),
    task_id_ character varying(64),
    name_ character varying(255) NOT NULL,
    var_type_ character varying(100),
    rev_ integer,
    bytearray_id_ character varying(64),
    double_ double precision,
    long_ numeric(20,0),
    text_ character varying(4000),
    text2_ character varying(4000),
    create_time_ timestamp without time zone,
    last_updated_time_ timestamp without time zone
);


ALTER TABLE vaxiom.act_hi_varinst OWNER TO postgres;

--
-- TOC entry 648 (class 1259 OID 889411)
-- Name: act_id_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_id_group (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    name_ character varying(255),
    type_ character varying(255)
);


ALTER TABLE vaxiom.act_id_group OWNER TO postgres;

--
-- TOC entry 649 (class 1259 OID 889417)
-- Name: act_id_info; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_id_info (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    user_id_ character varying(64),
    type_ character varying(64),
    key_ character varying(255),
    value_ character varying(255),
    password_ bytea,
    parent_id_ character varying(255)
);


ALTER TABLE vaxiom.act_id_info OWNER TO postgres;

--
-- TOC entry 650 (class 1259 OID 889423)
-- Name: act_id_membership; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_id_membership (
    user_id_ character varying(64) NOT NULL,
    group_id_ character varying(64) NOT NULL
);


ALTER TABLE vaxiom.act_id_membership OWNER TO postgres;

--
-- TOC entry 651 (class 1259 OID 889426)
-- Name: act_id_user; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_id_user (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    first_ character varying(255),
    last_ character varying(255),
    email_ character varying(255),
    pwd_ character varying(255),
    picture_id_ character varying(64)
);


ALTER TABLE vaxiom.act_id_user OWNER TO postgres;

--
-- TOC entry 652 (class 1259 OID 889432)
-- Name: act_re_deployment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_re_deployment (
    id_ character varying(64) NOT NULL,
    name_ character varying(255),
    category_ character varying(255),
    tenant_id_ character varying(255) DEFAULT ''::character varying,
    deploy_time_ timestamp with time zone
);


ALTER TABLE vaxiom.act_re_deployment OWNER TO postgres;

--
-- TOC entry 653 (class 1259 OID 889439)
-- Name: act_re_model; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_re_model (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    name_ character varying(255),
    key_ character varying(255),
    category_ character varying(255),
    create_time_ timestamp with time zone,
    last_update_time_ timestamp with time zone,
    version_ integer,
    meta_info_ character varying(4000),
    deployment_id_ character varying(64),
    editor_source_value_id_ character varying(64),
    editor_source_extra_value_id_ character varying(64),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_re_model OWNER TO postgres;

--
-- TOC entry 654 (class 1259 OID 889446)
-- Name: act_re_procdef; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_re_procdef (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    category_ character varying(255),
    name_ character varying(255),
    key_ character varying(255) NOT NULL,
    version_ integer NOT NULL,
    deployment_id_ character varying(64),
    resource_name_ character varying(4000),
    dgrm_resource_name_ character varying(4000),
    description_ character varying(4000),
    has_start_form_key_ boolean,
    has_graphical_notation_ boolean,
    suspension_state_ integer,
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_re_procdef OWNER TO postgres;

--
-- TOC entry 655 (class 1259 OID 889453)
-- Name: act_ru_event_subscr; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_event_subscr (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    event_type_ character varying(255) NOT NULL,
    event_name_ character varying(255),
    execution_id_ character varying(64),
    proc_inst_id_ character varying(64),
    activity_id_ character varying(64),
    configuration_ character varying(255),
    created_ timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    proc_def_id_ character varying(64),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_ru_event_subscr OWNER TO postgres;

--
-- TOC entry 656 (class 1259 OID 889461)
-- Name: act_ru_execution; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_execution (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    proc_inst_id_ character varying(64),
    business_key_ character varying(255),
    parent_id_ character varying(64),
    proc_def_id_ character varying(64),
    super_exec_ character varying(64),
    act_id_ character varying(255),
    is_active_ boolean,
    is_concurrent_ boolean,
    is_scope_ boolean,
    is_event_scope_ boolean,
    suspension_state_ integer,
    cached_ent_state_ integer,
    tenant_id_ character varying(255) DEFAULT ''::character varying,
    name_ character varying(255),
    lock_time_ timestamp with time zone
);


ALTER TABLE vaxiom.act_ru_execution OWNER TO postgres;

--
-- TOC entry 657 (class 1259 OID 889468)
-- Name: act_ru_identitylink; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_identitylink (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    group_id_ character varying(255),
    type_ character varying(255),
    user_id_ character varying(255),
    task_id_ character varying(64),
    proc_inst_id_ character varying(64),
    proc_def_id_ character varying(64)
);


ALTER TABLE vaxiom.act_ru_identitylink OWNER TO postgres;

--
-- TOC entry 658 (class 1259 OID 889474)
-- Name: act_ru_job; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_job (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    type_ character varying(255) NOT NULL,
    lock_exp_time_ timestamp with time zone,
    lock_owner_ character varying(255),
    exclusive_ boolean,
    execution_id_ character varying(64),
    process_instance_id_ character varying(64),
    proc_def_id_ character varying(64),
    retries_ integer,
    exception_stack_id_ character varying(64),
    exception_msg_ character varying(4000),
    duedate_ timestamp with time zone,
    repeat_ character varying(255),
    handler_type_ character varying(255),
    handler_cfg_ character varying(4000),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_ru_job OWNER TO postgres;

--
-- TOC entry 659 (class 1259 OID 889481)
-- Name: act_ru_task; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_task (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    execution_id_ character varying(64),
    proc_inst_id_ character varying(64),
    proc_def_id_ character varying(64),
    name_ character varying(255),
    parent_task_id_ character varying(64),
    description_ character varying(4000),
    task_def_key_ character varying(255),
    owner_ character varying(255),
    assignee_ character varying(255),
    delegation_ character varying(64),
    priority_ integer,
    create_time_ timestamp with time zone,
    due_date_ timestamp without time zone,
    category_ character varying(255),
    suspension_state_ integer,
    tenant_id_ character varying(255) DEFAULT ''::character varying,
    form_key_ character varying(255)
);


ALTER TABLE vaxiom.act_ru_task OWNER TO postgres;

--
-- TOC entry 660 (class 1259 OID 889488)
-- Name: act_ru_variable; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_variable (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    type_ character varying(255) NOT NULL,
    name_ character varying(255) NOT NULL,
    execution_id_ character varying(64),
    proc_inst_id_ character varying(64),
    task_id_ character varying(64),
    bytearray_id_ character varying(64),
    double_ double precision,
    long_ numeric(20,0),
    text_ character varying(4000),
    text2_ character varying(4000)
);


ALTER TABLE vaxiom.act_ru_variable OWNER TO postgres;

--
-- TOC entry 634 (class 1259 OID 889332)
-- Name: activiti_process_settings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.activiti_process_settings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_node_id numeric(20,0) NOT NULL,
    process_id character varying(255)
);


ALTER TABLE vaxiom.activiti_process_settings OWNER TO postgres;

--
-- TOC entry 635 (class 1259 OID 889335)
-- Name: activiti_process_settings_property; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.activiti_process_settings_property (
    activiti_process_settings_id numeric(20,0) NOT NULL,
    property character varying(255) NOT NULL,
    val character varying(255) NOT NULL
);


ALTER TABLE vaxiom.activiti_process_settings_property OWNER TO postgres;

--
-- TOC entry 661 (class 1259 OID 889494)
-- Name: added_report_type; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.added_report_type (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    owner numeric(20,0) NOT NULL,
    report_type character varying(255) NOT NULL
);


ALTER TABLE vaxiom.added_report_type OWNER TO postgres;

--
-- TOC entry 662 (class 1259 OID 889497)
-- Name: answers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.answers (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    question_id numeric(20,0),
    answer character varying(2048)
);


ALTER TABLE vaxiom.answers OWNER TO postgres;

--
-- TOC entry 663 (class 1259 OID 889503)
-- Name: application_properties; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.application_properties (
    organization_id numeric(20,0) NOT NULL,
    message_key character varying(255) NOT NULL,
    message_value character varying(1023)
);


ALTER TABLE vaxiom.application_properties OWNER TO postgres;

--
-- TOC entry 664 (class 1259 OID 889509)
-- Name: applied_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.applied_payments (
    id numeric(20,0) NOT NULL,
    payment_id numeric(20,0),
    applied_type character varying(64)
);


ALTER TABLE vaxiom.applied_payments OWNER TO postgres;

--
-- TOC entry 666 (class 1259 OID 889521)
-- Name: appointment_audit_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_audit_log (
    action character varying(255) NOT NULL,
    id numeric(20,0) NOT NULL,
    appointment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.appointment_audit_log OWNER TO postgres;

--
-- TOC entry 668 (class 1259 OID 889531)
-- Name: appointment_booking_resources; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_booking_resources (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    booking_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    appt_start_offset integer NOT NULL,
    duration integer NOT NULL,
    load_percentage numeric(3,2) DEFAULT 1.00 NOT NULL,
    legacy_doctor character varying(255),
    legacy_assistant character varying(255)
);


ALTER TABLE vaxiom.appointment_booking_resources OWNER TO postgres;

--
-- TOC entry 669 (class 1259 OID 889538)
-- Name: appointment_booking_state_changes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_booking_state_changes (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    change_time timestamp without time zone,
    new_state character varying(255) NOT NULL,
    old_state character varying(255),
    booking_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.appointment_booking_state_changes OWNER TO postgres;

--
-- TOC entry 667 (class 1259 OID 889524)
-- Name: appointment_bookings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_bookings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    confirmation_status character varying(255) NOT NULL,
    start_time timestamp without time zone,
    local_start_date date NOT NULL,
    local_start_time time without time zone NOT NULL,
    duration integer NOT NULL,
    state character varying(255) NOT NULL,
    appointment_id numeric(20,0) NOT NULL,
    chair_id numeric(20,0) NOT NULL,
    provider_id numeric(20,0),
    assistant_id numeric(20,0),
    legacy_doctor character varying(255),
    legacy_assistant character varying(255),
    reminder_sent boolean DEFAULT false NOT NULL,
    seated_chair_id numeric(20,0),
    sys_created_at numeric(20,0),
    check_in_time timestamp without time zone
);


ALTER TABLE vaxiom.appointment_bookings OWNER TO postgres;

--
-- TOC entry 670 (class 1259 OID 889544)
-- Name: appointment_field_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_field_values (
    appointment_id numeric(20,0) NOT NULL,
    field_option_id numeric(20,0) NOT NULL,
    field_definition_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.appointment_field_values OWNER TO postgres;

--
-- TOC entry 671 (class 1259 OID 889547)
-- Name: appointment_procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_procedures (
    appointment_id numeric(20,0) NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    pos integer DEFAULT 0 NOT NULL,
    from_template boolean DEFAULT false NOT NULL,
    added boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.appointment_procedures OWNER TO postgres;

--
-- TOC entry 673 (class 1259 OID 889561)
-- Name: appointment_template_procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_template_procedures (
    appointment_template_id numeric(20,0) NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    pos integer NOT NULL
);


ALTER TABLE vaxiom.appointment_template_procedures OWNER TO postgres;

--
-- TOC entry 672 (class 1259 OID 889554)
-- Name: appointment_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    appointment_type_id numeric(20,0) NOT NULL,
    primary_provider_type_id numeric(20,0),
    primary_assistant_type_id numeric(20,0),
    full_name character varying(255),
    color_id character varying(255),
    medical_plan character varying(45),
    send_medical_form boolean DEFAULT false
);


ALTER TABLE vaxiom.appointment_templates OWNER TO postgres;

--
-- TOC entry 676 (class 1259 OID 889580)
-- Name: appointment_type_daily_restrictions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_type_daily_restrictions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type_id numeric(20,0) NOT NULL,
    local_date date NOT NULL,
    location_id numeric(20,0) NOT NULL,
    max_appts integer DEFAULT 1 NOT NULL
);


ALTER TABLE vaxiom.appointment_type_daily_restrictions OWNER TO postgres;

--
-- TOC entry 674 (class 1259 OID 889564)
-- Name: appointment_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    color_id character varying(255),
    full_name character varying(255) DEFAULT ''::character varying NOT NULL,
    name character varying(255) NOT NULL,
    not_tx boolean DEFAULT false NOT NULL,
    patient_exam boolean DEFAULT false NOT NULL,
    parent_id numeric(20,0),
    migrated boolean NOT NULL,
    emergency boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.appointment_types OWNER TO postgres;

--
-- TOC entry 675 (class 1259 OID 889574)
-- Name: appointment_types_property; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_types_property (
    appointment_type_id numeric(20,0) NOT NULL,
    property character varying(255) NOT NULL,
    val character varying(255) NOT NULL
);


ALTER TABLE vaxiom.appointment_types_property OWNER TO postgres;

--
-- TOC entry 665 (class 1259 OID 889512)
-- Name: appointments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointments (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer,
    has_custom_procedures boolean DEFAULT false NOT NULL,
    interval_to_next integer,
    note character varying(4096) DEFAULT ''::character varying NOT NULL,
    type_id numeric(20,0) NOT NULL,
    next_appointment_id numeric(20,0),
    patient_id numeric(20,0),
    tx_id numeric(20,0),
    tx_plan_appointment_id numeric(20,0),
    unplanned boolean DEFAULT false NOT NULL,
    legacy_procedure_codes character varying(1024),
    sys_created_at numeric(20,0)
);


ALTER TABLE vaxiom.appointments OWNER TO postgres;

--
-- TOC entry 677 (class 1259 OID 889584)
-- Name: appt_type_template_restrictions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appt_type_template_restrictions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    template_id numeric(20,0) NOT NULL,
    type_id numeric(20,0) NOT NULL,
    max_appts integer DEFAULT 1 NOT NULL
);


ALTER TABLE vaxiom.appt_type_template_restrictions OWNER TO postgres;

--
-- TOC entry 678 (class 1259 OID 889588)
-- Name: audit_events; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.audit_events (
    id numeric(20,0) NOT NULL,
    created timestamp without time zone,
    ip_address character varying(50),
    application character varying(255),
    session character varying(255),
    user_id numeric(20,0),
    subject character varying(255),
    subject_id numeric(20,0),
    verb character varying(255),
    object character varying(255),
    object_id numeric(20,0),
    parent_object character varying(255),
    parent_object_id numeric(20,0),
    description character varying(255)
);


ALTER TABLE vaxiom.audit_events OWNER TO postgres;

--
-- TOC entry 679 (class 1259 OID 889594)
-- Name: audit_logs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.audit_logs (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    class_type character varying(255) NOT NULL,
    user_id numeric(20,0)
);


ALTER TABLE vaxiom.audit_logs OWNER TO postgres;

--
-- TOC entry 680 (class 1259 OID 889597)
-- Name: autopay_invoice_changes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.autopay_invoice_changes (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    change_time timestamp without time zone,
    changed_by numeric(20,0),
    changed_field_name character varying(255),
    new_value character varying(255),
    old_value character varying(255),
    receivable_id numeric(20,0) NOT NULL,
    description character varying(1024)
);


ALTER TABLE vaxiom.autopay_invoice_changes OWNER TO postgres;

--
-- TOC entry 681 (class 1259 OID 889603)
-- Name: bank_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.bank_accounts (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    bank_account_type character varying(255),
    account_number character varying(255),
    routing_number character varying(255)
);


ALTER TABLE vaxiom.bank_accounts OWNER TO postgres;

--
-- TOC entry 682 (class 1259 OID 889609)
-- Name: benefit_saving_account_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.benefit_saving_account_values (
    id numeric(20,0) NOT NULL,
    pre_tax_rate numeric(30,10) NOT NULL,
    post_tax_rate numeric(30,10),
    employer_rate numeric(30,10),
    total_rate numeric(30,10),
    employee_benefit_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    insurance_plan_saving_account_id numeric(20,0) NOT NULL,
    employee_annual_rate numeric(30,10),
    employer_annual_rate numeric(30,10),
    waived boolean,
    qle_parent numeric(20,0),
    employee_beneficiary_group_id numeric(20,0)
);


ALTER TABLE vaxiom.benefit_saving_account_values OWNER TO postgres;

--
-- TOC entry 683 (class 1259 OID 889612)
-- Name: bluefin_credentials; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.bluefin_credentials (
    organization_id numeric(20,0) NOT NULL,
    account_id character varying(255) NOT NULL,
    access_key character varying(255) NOT NULL
);


ALTER TABLE vaxiom.bluefin_credentials OWNER TO postgres;

--
-- TOC entry 684 (class 1259 OID 889618)
-- Name: broker; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.broker (
    id numeric(20,0),
    login_email character varying(50),
    name character varying(50)
);


ALTER TABLE vaxiom.broker OWNER TO postgres;

--
-- TOC entry 685 (class 1259 OID 889621)
-- Name: cancelled_receivables; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.cancelled_receivables (
    receivable_id numeric(20,0) NOT NULL,
    date timestamp without time zone
);


ALTER TABLE vaxiom.cancelled_receivables OWNER TO postgres;

--
-- TOC entry 686 (class 1259 OID 889624)
-- Name: cephx_requests; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.cephx_requests (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    transaction_id character varying(255),
    patient_image_id numeric(20,0) NOT NULL,
    status character varying(45) DEFAULT 'SENT'::character varying NOT NULL
);


ALTER TABLE vaxiom.cephx_requests OWNER TO postgres;

--
-- TOC entry 688 (class 1259 OID 889635)
-- Name: chair_allocations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.chair_allocations (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    ca_date date NOT NULL,
    chair_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    primary_resource boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.chair_allocations OWNER TO postgres;

--
-- TOC entry 687 (class 1259 OID 889628)
-- Name: chairs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.chairs (
    code character varying(255) NOT NULL,
    full_name character varying(255) NOT NULL,
    id numeric(20,0) NOT NULL,
    pos integer DEFAULT 1000 NOT NULL
);


ALTER TABLE vaxiom.chairs OWNER TO postgres;

--
-- TOC entry 689 (class 1259 OID 889639)
-- Name: charge_transfer_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.charge_transfer_adjustments (
    id numeric(20,0) NOT NULL,
    transfer_charge_type character varying(255)
);


ALTER TABLE vaxiom.charge_transfer_adjustments OWNER TO postgres;

--
-- TOC entry 691 (class 1259 OID 889645)
-- Name: check_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.check_types (
    id numeric(20,0) NOT NULL,
    name character varying(100) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created date,
    sys_last_modified date,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.check_types OWNER TO postgres;

--
-- TOC entry 690 (class 1259 OID 889642)
-- Name: checks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.checks (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0),
    type_id numeric(20,0),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created date,
    sys_last_modified date,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.checks OWNER TO postgres;

--
-- TOC entry 692 (class 1259 OID 889648)
-- Name: claim_events; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.claim_events (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    receivable_id numeric(20,0),
    event character varying(255),
    amount numeric(30,10),
    description character varying(1024)
);


ALTER TABLE vaxiom.claim_events OWNER TO postgres;

--
-- TOC entry 693 (class 1259 OID 889654)
-- Name: claim_requests; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.claim_requests (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    receivable_id numeric(20,0) NOT NULL,
    location_id numeric(20,0),
    status character varying(255),
    report text,
    claim_request_data text NOT NULL,
    patient_id numeric(20,0),
    treatment_id numeric(20,0),
    money_requested numeric(30,10)
);


ALTER TABLE vaxiom.claim_requests OWNER TO postgres;

--
-- TOC entry 696 (class 1259 OID 889669)
-- Name: collection_label_agencies; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collection_label_agencies (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    phone_numer character varying(25),
    parent_company_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.collection_label_agencies OWNER TO postgres;

--
-- TOC entry 697 (class 1259 OID 889672)
-- Name: collection_label_history; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collection_label_history (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    created_by numeric(20,0) NOT NULL,
    created timestamp without time zone,
    patient_id numeric(20,0) NOT NULL,
    payment_account_id numeric(20,0) NOT NULL,
    previous_collection_label_template_id numeric(20,0)
);


ALTER TABLE vaxiom.collection_label_history OWNER TO postgres;

--
-- TOC entry 698 (class 1259 OID 889675)
-- Name: collection_label_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collection_label_templates (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    parent_company_id numeric(20,0) NOT NULL,
    agency_id numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    payer_type character varying(45) DEFAULT 'PERSON'::character varying,
    is_default boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.collection_label_templates OWNER TO postgres;

--
-- TOC entry 695 (class 1259 OID 889666)
-- Name: collection_labels; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collection_labels (
    patient_id numeric(20,0) NOT NULL,
    payment_account_id numeric(20,0) NOT NULL,
    last_modified_by numeric(20,0) NOT NULL,
    last_modified timestamp without time zone,
    collection_label_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.collection_labels OWNER TO postgres;

--
-- TOC entry 694 (class 1259 OID 889660)
-- Name: collections_cache; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collections_cache (
    location_id numeric(20,0) NOT NULL,
    date date NOT NULL,
    results text
);


ALTER TABLE vaxiom.collections_cache OWNER TO postgres;

--
-- TOC entry 699 (class 1259 OID 889681)
-- Name: communication_preferences; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.communication_preferences (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    contact_method_association_id numeric(20,0) NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE vaxiom.communication_preferences OWNER TO postgres;

--
-- TOC entry 700 (class 1259 OID 889684)
-- Name: contact_emails; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_emails (
    id numeric(20,0) NOT NULL,
    address character varying(255) NOT NULL
);


ALTER TABLE vaxiom.contact_emails OWNER TO postgres;

--
-- TOC entry 702 (class 1259 OID 889691)
-- Name: contact_method_associations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_method_associations (
    id numeric(20,0) NOT NULL,
    person_id numeric(20,0) NOT NULL,
    contact_method_id numeric(20,0) NOT NULL,
    description character varying(255),
    preference character varying(31),
    error_location_id numeric(20,0),
    error_date date,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    contact_order integer
);


ALTER TABLE vaxiom.contact_method_associations OWNER TO postgres;

--
-- TOC entry 701 (class 1259 OID 889687)
-- Name: contact_methods; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_methods (
    dtype character varying(31) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0) DEFAULT '0'::numeric NOT NULL
);


ALTER TABLE vaxiom.contact_methods OWNER TO postgres;

--
-- TOC entry 703 (class 1259 OID 889694)
-- Name: contact_phones; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_phones (
    id numeric(20,0) NOT NULL,
    number character varying(255) NOT NULL,
    type character varying(31) NOT NULL
);


ALTER TABLE vaxiom.contact_phones OWNER TO postgres;

--
-- TOC entry 704 (class 1259 OID 889697)
-- Name: contact_postal_addresses; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_postal_addresses (
    id numeric(20,0) NOT NULL,
    address_line1 character varying(255),
    address_line2 character varying(255),
    city character varying(255),
    zip character varying(255),
    state character varying(255)
);


ALTER TABLE vaxiom.contact_postal_addresses OWNER TO postgres;

--
-- TOC entry 705 (class 1259 OID 889703)
-- Name: contact_recently_opened_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_recently_opened_items (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.contact_recently_opened_items OWNER TO postgres;

--
-- TOC entry 706 (class 1259 OID 889706)
-- Name: contact_website; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_website (
    id numeric(20,0) NOT NULL,
    website_address character varying(255) NOT NULL
);


ALTER TABLE vaxiom.contact_website OWNER TO postgres;

--
-- TOC entry 707 (class 1259 OID 889709)
-- Name: correction_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.correction_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(50) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    parent_company_id numeric(20,0),
    positive boolean DEFAULT true,
    negative boolean DEFAULT true
);


ALTER TABLE vaxiom.correction_types OWNER TO postgres;

--
-- TOC entry 708 (class 1259 OID 889715)
-- Name: correction_types_locations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.correction_types_locations (
    correction_type_id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.correction_types_locations OWNER TO postgres;

--
-- TOC entry 709 (class 1259 OID 889718)
-- Name: correction_types_payment_options; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.correction_types_payment_options (
    correction_types_id numeric(20,0) NOT NULL,
    payment_options_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.correction_types_payment_options OWNER TO postgres;

--
-- TOC entry 710 (class 1259 OID 889721)
-- Name: county; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.county (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    incits_code character varying(15) NOT NULL,
    name character varying(255) NOT NULL,
    state character varying(25) NOT NULL
);


ALTER TABLE vaxiom.county OWNER TO postgres;

--
-- TOC entry 711 (class 1259 OID 889724)
-- Name: daily_transactions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.daily_transactions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    date date,
    location_id numeric(20,0) NOT NULL,
    report_data text NOT NULL,
    report_version integer DEFAULT 0 NOT NULL,
    location_type character varying(20) NOT NULL
);


ALTER TABLE vaxiom.daily_transactions OWNER TO postgres;

--
-- TOC entry 712 (class 1259 OID 889731)
-- Name: data_lock; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.data_lock (
    id numeric(20,0) NOT NULL,
    sys_created timestamp without time zone,
    session_id character varying(255) NOT NULL,
    user_id numeric(20,0) NOT NULL,
    data_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.data_lock OWNER TO postgres;

--
-- TOC entry 714 (class 1259 OID 889741)
-- Name: day_schedule_appt_slots; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.day_schedule_appt_slots (
    day_schedule_id numeric(20,0) NOT NULL,
    appt_type_id numeric(20,0) NOT NULL,
    start_min integer NOT NULL
);


ALTER TABLE vaxiom.day_schedule_appt_slots OWNER TO postgres;

--
-- TOC entry 715 (class 1259 OID 889744)
-- Name: day_schedule_break_hours; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.day_schedule_break_hours (
    day_schedule_id numeric(20,0) NOT NULL,
    end_min integer NOT NULL,
    start_min integer NOT NULL
);


ALTER TABLE vaxiom.day_schedule_break_hours OWNER TO postgres;

--
-- TOC entry 716 (class 1259 OID 889747)
-- Name: day_schedule_office_hours; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.day_schedule_office_hours (
    day_schedule_id numeric(20,0) NOT NULL,
    end_min integer NOT NULL,
    start_min integer NOT NULL
);


ALTER TABLE vaxiom.day_schedule_office_hours OWNER TO postgres;

--
-- TOC entry 713 (class 1259 OID 889734)
-- Name: day_schedules; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.day_schedules (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    color character varying(255) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    name character varying(255) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0),
    valid_from timestamp without time zone,
    day_schedule_nr numeric(20,0) NOT NULL,
    overrides_day_schedule numeric(20,0)
);


ALTER TABLE vaxiom.day_schedules OWNER TO postgres;

--
-- TOC entry 717 (class 1259 OID 889750)
-- Name: default_appointment_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.default_appointment_templates (
    id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    template_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.default_appointment_templates OWNER TO postgres;

--
-- TOC entry 718 (class 1259 OID 889753)
-- Name: dentalchart_additional_markings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_additional_markings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    marking_id numeric(20,0),
    name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_additional_markings OWNER TO postgres;

--
-- TOC entry 719 (class 1259 OID 889756)
-- Name: dentalchart_edit_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_edit_log (
    id numeric(20,0) NOT NULL,
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_edit_log OWNER TO postgres;

--
-- TOC entry 720 (class 1259 OID 889759)
-- Name: dentalchart_marking; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_marking (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    added timestamp without time zone,
    completed timestamp without time zone,
    canceled timestamp without time zone,
    to_tooth_number integer,
    tooth_number integer NOT NULL,
    type character varying(255),
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_marking OWNER TO postgres;

--
-- TOC entry 721 (class 1259 OID 889762)
-- Name: dentalchart_materials; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_materials (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    marking_type character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_materials OWNER TO postgres;

--
-- TOC entry 722 (class 1259 OID 889768)
-- Name: dentalchart_snapshot; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_snapshot (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    comments character varying(255),
    data bytea NOT NULL,
    mime_type character varying(255) NOT NULL,
    appointment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_snapshot OWNER TO postgres;

--
-- TOC entry 723 (class 1259 OID 889774)
-- Name: diagnosis; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.diagnosis (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    appointment_id numeric(20,0) NOT NULL,
    diagnosis_template_id numeric(20,0) NOT NULL,
    techincal_letter_generated timestamp without time zone,
    layperson_letter_generated timestamp without time zone,
    techincal_letter_id character varying(255),
    layperson_letter_id character varying(255)
);


ALTER TABLE vaxiom.diagnosis OWNER TO postgres;

--
-- TOC entry 724 (class 1259 OID 889780)
-- Name: diagnosis_field_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.diagnosis_field_values (
    diagnosis_id numeric(20,0) NOT NULL,
    field_key character varying(255) NOT NULL,
    field_value text
);


ALTER TABLE vaxiom.diagnosis_field_values OWNER TO postgres;

--
-- TOC entry 725 (class 1259 OID 889786)
-- Name: diagnosis_letters; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.diagnosis_letters (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    diagnosis_id numeric(20,0) NOT NULL,
    document_id numeric(20,0) NOT NULL,
    document_template_id numeric(20,0) NOT NULL,
    recipient_id numeric(20,0) NOT NULL,
    address_id numeric(20,0)
);


ALTER TABLE vaxiom.diagnosis_letters OWNER TO postgres;

--
-- TOC entry 726 (class 1259 OID 889789)
-- Name: diagnosis_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.diagnosis_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    version numeric(20,0),
    form_structure_xml text,
    tx_category_id numeric(20,0) NOT NULL,
    appointment_type_id numeric(20,0),
    parent_company_id numeric(20,0)
);


ALTER TABLE vaxiom.diagnosis_templates OWNER TO postgres;

--
-- TOC entry 728 (class 1259 OID 889802)
-- Name: discount_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.discount_adjustments (
    id numeric(20,0) NOT NULL,
    discount_id numeric(20,0),
    discount_adjustment_type character varying(255)
);


ALTER TABLE vaxiom.discount_adjustments OWNER TO postgres;

--
-- TOC entry 729 (class 1259 OID 889805)
-- Name: discount_reverse_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.discount_reverse_adjustments (
    id numeric(20,0) NOT NULL,
    discount_adjustment_id numeric(20,0)
);


ALTER TABLE vaxiom.discount_reverse_adjustments OWNER TO postgres;

--
-- TOC entry 727 (class 1259 OID 889795)
-- Name: discounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.discounts (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255),
    organization_id numeric(20,0) NOT NULL,
    default_amount numeric(30,10),
    default_percentage numeric(3,2),
    fixed boolean,
    valid_from date,
    valid_to date,
    tx_fee boolean DEFAULT false NOT NULL,
    type character varying(20) DEFAULT 'PVT_PAY'::character varying NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    affects_production boolean DEFAULT true NOT NULL
);


ALTER TABLE vaxiom.discounts OWNER TO postgres;

--
-- TOC entry 730 (class 1259 OID 889808)
-- Name: dock_item_descriptors; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_descriptors (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    height integer NOT NULL,
    owner numeric(20,0) NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL,
    width integer NOT NULL
);


ALTER TABLE vaxiom.dock_item_descriptors OWNER TO postgres;

--
-- TOC entry 731 (class 1259 OID 889811)
-- Name: dock_item_features_events; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_events (
    calendar_id character varying(255) NOT NULL,
    event_id character varying(255),
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_events OWNER TO postgres;

--
-- TOC entry 732 (class 1259 OID 889817)
-- Name: dock_item_features_images; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_images (
    image_id character varying(255) NOT NULL,
    edit_mode boolean,
    id numeric(20,0) NOT NULL,
    patient_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_images OWNER TO postgres;

--
-- TOC entry 733 (class 1259 OID 889820)
-- Name: dock_item_features_messages; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_messages (
    thread_id numeric(20,0),
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_messages OWNER TO postgres;

--
-- TOC entry 734 (class 1259 OID 889823)
-- Name: dock_item_features_note; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_note (
    id numeric(20,0) NOT NULL,
    note_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_note OWNER TO postgres;

--
-- TOC entry 735 (class 1259 OID 889826)
-- Name: dock_item_features_tasks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_tasks (
    list_id character varying(255),
    task_id character varying(255),
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_tasks OWNER TO postgres;

--
-- TOC entry 736 (class 1259 OID 889832)
-- Name: document_recently_opened_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.document_recently_opened_items (
    drive_id character varying(255) NOT NULL,
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.document_recently_opened_items OWNER TO postgres;

--
-- TOC entry 737 (class 1259 OID 889835)
-- Name: document_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.document_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    description character varying(255),
    file_key character varying(255) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    mime_type character varying(255) NOT NULL,
    is_contract boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.document_templates OWNER TO postgres;

--
-- TOC entry 738 (class 1259 OID 889842)
-- Name: document_tree_nodes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.document_tree_nodes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    parent_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL,
    file_uid character varying(255),
    name character varying(255) NOT NULL,
    size numeric(20,0),
    icon character varying(255),
    thumbs character varying(255),
    meta text,
    deleted boolean DEFAULT false NOT NULL,
    created_from character varying(45) DEFAULT 'CORE'::character varying
);


ALTER TABLE vaxiom.document_tree_nodes OWNER TO postgres;

--
-- TOC entry 739 (class 1259 OID 889850)
-- Name: eeo; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.eeo (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    jobtitle character varying(60)
);


ALTER TABLE vaxiom.eeo OWNER TO postgres;

--
-- TOC entry 740 (class 1259 OID 889853)
-- Name: employee_beneficiary; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_beneficiary (
    id numeric(20,0) NOT NULL,
    share_percent integer,
    type character varying(45),
    employee_enrollment_person_id numeric(20,0) NOT NULL,
    employee_beneficiary_group_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0)
);


ALTER TABLE vaxiom.employee_beneficiary OWNER TO postgres;

--
-- TOC entry 741 (class 1259 OID 889856)
-- Name: employee_beneficiary_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_beneficiary_group (
    id numeric(20,0) NOT NULL,
    employee_enrollment_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0)
);


ALTER TABLE vaxiom.employee_beneficiary_group OWNER TO postgres;

--
-- TOC entry 742 (class 1259 OID 889859)
-- Name: employee_benefit; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_benefit (
    id numeric(20,0) NOT NULL,
    benefit_type character varying(45),
    pre_tax_rate numeric(30,10),
    post_tax_rate numeric(30,10),
    employer_rate numeric(30,10),
    total_rate numeric(30,10),
    covarage_level character varying(45),
    other_fields character varying(1200),
    employee_enrollment_id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0),
    is_waived boolean,
    benefit_enrollment_status character varying(45) DEFAULT 'NONE'::character varying,
    is_disabled boolean DEFAULT false,
    waive_reason character varying(45),
    payroll_cycle character varying(20),
    biling_start_date date,
    biling_end_date date,
    other_carrier_name character varying(100),
    carrier_person_name character varying(100),
    other_coverage_level character varying(100)
);


ALTER TABLE vaxiom.employee_benefit OWNER TO postgres;

--
-- TOC entry 743 (class 1259 OID 889867)
-- Name: employee_dependent; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_dependent (
    id numeric(20,0) NOT NULL,
    gender character varying(45),
    ssn character(11),
    is_disabled_child boolean,
    is_address_the_same boolean,
    is_fulltime_student boolean,
    street character varying(120),
    city character varying(45),
    zip_code character varying(45),
    state character varying(45),
    country character varying(45),
    employee_enrollment_person_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    employee_enrollment_id numeric(20,0) NOT NULL,
    qle_parent numeric(20,0),
    primary_phone character varying(20),
    primary_phone_type character varying(20),
    secondary_phone character varying(20),
    secondary_phone_type character varying(20),
    primary_email character varying(60),
    primary_email_type character varying(20),
    secondary_email character varying(60),
    secondary_email_type character varying(20)
);


ALTER TABLE vaxiom.employee_dependent OWNER TO postgres;

--
-- TOC entry 744 (class 1259 OID 889873)
-- Name: employee_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_enrollment (
    id numeric(20,0) NOT NULL,
    status character varying(45),
    fill_date timestamp without time zone,
    summary_document_id character varying(45),
    enrollment_id numeric(20,0),
    enrollment_form_source_event_id numeric(20,0) NOT NULL,
    employment_contracts_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_type character varying(64),
    qle_occurrence_date date,
    qle_parent numeric(20,0),
    other_qle_description character varying(500),
    first_name character varying(45),
    last_name character varying(45),
    middle_name character varying(45),
    total_annualized_earnings numeric(30,10),
    gender character varying(11),
    birth_date date,
    effective_coverage_start date,
    fill_date_offset character varying(20),
    confirmed_event_date boolean DEFAULT false,
    deny_acknowledged boolean DEFAULT false,
    confirmed boolean DEFAULT false,
    sys_portal_last_modified timestamp without time zone,
    sys_portal_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.employee_enrollment OWNER TO postgres;

--
-- TOC entry 9015 (class 0 OID 0)
-- Dependencies: 744
-- Name: TABLE employee_enrollment; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON TABLE vaxiom.employee_enrollment IS '		';


--
-- TOC entry 745 (class 1259 OID 889882)
-- Name: employee_enrollment_attachment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_enrollment_attachment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(45) NOT NULL,
    file_id character varying(127),
    file_mime_type character varying(255),
    file_name character varying(127),
    file_size numeric(20,0),
    employee_enrollment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.employee_enrollment_attachment OWNER TO postgres;

--
-- TOC entry 746 (class 1259 OID 889888)
-- Name: employee_enrollment_event; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_enrollment_event (
    id numeric(20,0) NOT NULL,
    employee_enrollment_id numeric(20,0) NOT NULL,
    old_status character varying(45),
    new_status character varying(45),
    username character varying(255),
    comment character varying(255),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.employee_enrollment_event OWNER TO postgres;

--
-- TOC entry 747 (class 1259 OID 889894)
-- Name: employee_enrollment_person; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_enrollment_person (
    id numeric(20,0) NOT NULL,
    first_name character varying(100),
    middle_name character varying(45),
    last_name character varying(45),
    date_of_birth date,
    relationship character varying(45),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    phone_number character varying(45),
    email character varying(100),
    qle_parent numeric(20,0),
    employee_enrollment_id numeric(20,0) NOT NULL,
    organization_name character varying(100),
    organization_form_date date,
    resp_person_name character varying(400),
    resp_person_city character varying(45),
    resp_person_state character varying(20),
    type character varying(10)
);


ALTER TABLE vaxiom.employee_enrollment_person OWNER TO postgres;

--
-- TOC entry 748 (class 1259 OID 889900)
-- Name: employee_insured; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_insured (
    id numeric(20,0) NOT NULL,
    employee_beneficiary_group_id numeric(20,0),
    employee_dependent_id numeric(20,0) NOT NULL,
    employee_benefit_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0),
    smoker boolean DEFAULT false
);


ALTER TABLE vaxiom.employee_insured OWNER TO postgres;

--
-- TOC entry 749 (class 1259 OID 889904)
-- Name: employee_professional_relationships; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_professional_relationships (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    provider_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.employee_professional_relationships OWNER TO postgres;

--
-- TOC entry 750 (class 1259 OID 889907)
-- Name: employee_resources; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_resources (
    id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0) NOT NULL,
    invalid boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.employee_resources OWNER TO postgres;

--
-- TOC entry 751 (class 1259 OID 889911)
-- Name: employers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employers (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    master_id numeric(20,0),
    organization_id numeric(20,0),
    contact_postal_address_id numeric(20,0),
    contact_phone_id numeric(20,0),
    contact_email_id numeric(20,0),
    name character varying(255)
);


ALTER TABLE vaxiom.employers OWNER TO postgres;

--
-- TOC entry 753 (class 1259 OID 889921)
-- Name: employment_contract_emergency_contact; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_emergency_contact (
    id numeric(20,0) NOT NULL,
    first_name character varying(45) NOT NULL,
    middle_name character varying(45),
    last_name character varying(45) NOT NULL,
    email character varying(45),
    phone_number character varying(45) NOT NULL,
    relationship character varying(45) NOT NULL,
    type character varying(45) NOT NULL,
    employment_contracts_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.employment_contract_emergency_contact OWNER TO postgres;

--
-- TOC entry 754 (class 1259 OID 889924)
-- Name: employment_contract_enrollment_group_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_enrollment_group_log (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    employment_contracts_id numeric(20,0) NOT NULL,
    old_enrollment_group_id numeric(20,0),
    new_enrollment_group_id numeric(20,0),
    date_of_occurance date NOT NULL,
    commnet character varying(500)
);


ALTER TABLE vaxiom.employment_contract_enrollment_group_log OWNER TO postgres;

--
-- TOC entry 755 (class 1259 OID 889930)
-- Name: employment_contract_job_status_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_job_status_log (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    employment_contracts_id numeric(20,0) NOT NULL,
    old_job_status character varying(45),
    new_job_status character varying(45) NOT NULL,
    date_of_occurance date
);


ALTER TABLE vaxiom.employment_contract_job_status_log OWNER TO postgres;

--
-- TOC entry 756 (class 1259 OID 889933)
-- Name: employment_contract_location_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_location_group (
    id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0),
    location_group_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0)
);


ALTER TABLE vaxiom.employment_contract_location_group OWNER TO postgres;

--
-- TOC entry 757 (class 1259 OID 889936)
-- Name: employment_contract_permissions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_permissions (
    id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0),
    permissions_permission_id numeric(20,0) NOT NULL,
    access_type character varying(30) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0)
);


ALTER TABLE vaxiom.employment_contract_permissions OWNER TO postgres;

--
-- TOC entry 758 (class 1259 OID 889939)
-- Name: employment_contract_role; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_role (
    id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0),
    permissions_user_role_id numeric(20,0),
    sys_organizations_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.employment_contract_role OWNER TO postgres;

--
-- TOC entry 759 (class 1259 OID 889942)
-- Name: employment_contract_salary_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_salary_log (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    employment_contracts_id numeric(20,0) NOT NULL,
    salary_structure character varying(45),
    salary numeric(30,10),
    day_rate numeric(30,10),
    hourly_rate numeric(30,10),
    avg_hours_per_week numeric(30,10),
    avg_days_per_week numeric(30,10),
    add_annual_compensation numeric(30,10),
    total_annual_earnings numeric(30,10),
    ltd_bonus_up_annualized numeric(30,10),
    annualized_commission numeric(30,10),
    date_of_occurance date NOT NULL,
    payroll_cycle character varying(40),
    commnet character varying(500)
);


ALTER TABLE vaxiom.employment_contract_salary_log OWNER TO postgres;

--
-- TOC entry 752 (class 1259 OID 889914)
-- Name: employment_contracts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contracts (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    legacy_id character varying(255),
    organization_id numeric(20,0),
    person_id numeric(20,0) NOT NULL,
    company character varying(100),
    department character varying(100),
    location character varying(100),
    employment_start_date date NOT NULL,
    employment_end_date date,
    job_status character varying(20) NOT NULL,
    job_class character varying(20),
    job_title character varying(100),
    enrollment_class character varying(20),
    salary numeric(30,10),
    annualized_commission numeric(30,10),
    ltd_bonus_up_annualized numeric(30,10),
    signature bytea,
    human_id numeric(20,0),
    npi character varying(45),
    tin character varying(45),
    medicaid_id character varying(45),
    hourly_rate numeric(30,10),
    day_rate numeric(30,10),
    avg_hours_per_week numeric(30,10),
    avg_days_per_week numeric(30,10),
    salary_structure character varying(45),
    add_annual_compensation numeric(30,10),
    total_annual_earnings numeric(30,10),
    current_employment_start_date date,
    payroll_cycle character varying(45),
    legal_entity_id numeric(20,0) NOT NULL,
    physical_address_id numeric(20,0),
    department_id numeric(20,0),
    division_id numeric(20,0),
    enrollment_group_id numeric(20,0),
    billing_group_id numeric(20,0),
    jobtitle_id numeric(20,0),
    coverage_period character varying(100),
    deleted boolean DEFAULT false
);


ALTER TABLE vaxiom.employment_contracts OWNER TO postgres;

--
-- TOC entry 760 (class 1259 OID 889948)
-- Name: enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    effective_coverage_start date,
    effective_coverage_end date,
    year integer,
    oe_start date,
    oe_end date,
    oe_email_template character varying(100),
    oe_sms_template character varying(100),
    nh_email_template character varying(100),
    nh_sms_template character varying(100),
    legal_entity_id numeric(20,0) NOT NULL,
    email_reminder integer NOT NULL,
    sms_reminder integer NOT NULL,
    new_hire_effective_covearge_definition character varying(100),
    renewal_date date,
    expiration_date date,
    cancellation_date date,
    status character varying(60),
    last_enrollment_id numeric(20,0)
);


ALTER TABLE vaxiom.enrollment OWNER TO postgres;

--
-- TOC entry 762 (class 1259 OID 889956)
-- Name: enrollment_form; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_id numeric(20,0) NOT NULL,
    employee_id numeric(20,0) NOT NULL,
    status character varying(20),
    fill_date timestamp without time zone,
    dependents_confirmation boolean,
    confirmation_document_id character varying(100)
);


ALTER TABLE vaxiom.enrollment_form OWNER TO postgres;

--
-- TOC entry 764 (class 1259 OID 889962)
-- Name: enrollment_form_beneficiary; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_beneficiary (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    beneficiary_data text
);


ALTER TABLE vaxiom.enrollment_form_beneficiary OWNER TO postgres;

--
-- TOC entry 763 (class 1259 OID 889960)
-- Name: enrollment_form_beneficiary_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_beneficiary_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_beneficiary_id_seq OWNER TO postgres;

--
-- TOC entry 9016 (class 0 OID 0)
-- Dependencies: 763
-- Name: enrollment_form_beneficiary_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_beneficiary_id_seq OWNED BY vaxiom.enrollment_form_beneficiary.id;


--
-- TOC entry 766 (class 1259 OID 889971)
-- Name: enrollment_form_benefits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_benefits (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    benefit_type character varying(30),
    benefit_data text
);


ALTER TABLE vaxiom.enrollment_form_benefits OWNER TO postgres;

--
-- TOC entry 765 (class 1259 OID 889969)
-- Name: enrollment_form_benefits_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_benefits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_benefits_id_seq OWNER TO postgres;

--
-- TOC entry 9017 (class 0 OID 0)
-- Dependencies: 765
-- Name: enrollment_form_benefits_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_benefits_id_seq OWNED BY vaxiom.enrollment_form_benefits.id;


--
-- TOC entry 768 (class 1259 OID 889980)
-- Name: enrollment_form_dependents; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_dependents (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    dependent_data text
);


ALTER TABLE vaxiom.enrollment_form_dependents OWNER TO postgres;

--
-- TOC entry 770 (class 1259 OID 889989)
-- Name: enrollment_form_dependents_benefits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_dependents_benefits (
    id bigint NOT NULL,
    enrollment_form_benefit_id numeric(20,0),
    enrollment_form_dependent_id numeric(20,0),
    benefit_data text
);


ALTER TABLE vaxiom.enrollment_form_dependents_benefits OWNER TO postgres;

--
-- TOC entry 769 (class 1259 OID 889987)
-- Name: enrollment_form_dependents_benefits_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_dependents_benefits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_dependents_benefits_id_seq OWNER TO postgres;

--
-- TOC entry 9018 (class 0 OID 0)
-- Dependencies: 769
-- Name: enrollment_form_dependents_benefits_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_dependents_benefits_id_seq OWNED BY vaxiom.enrollment_form_dependents_benefits.id;


--
-- TOC entry 767 (class 1259 OID 889978)
-- Name: enrollment_form_dependents_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_dependents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_dependents_id_seq OWNER TO postgres;

--
-- TOC entry 9019 (class 0 OID 0)
-- Dependencies: 767
-- Name: enrollment_form_dependents_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_dependents_id_seq OWNED BY vaxiom.enrollment_form_dependents.id;


--
-- TOC entry 761 (class 1259 OID 889954)
-- Name: enrollment_form_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_id_seq OWNER TO postgres;

--
-- TOC entry 9020 (class 0 OID 0)
-- Dependencies: 761
-- Name: enrollment_form_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_id_seq OWNED BY vaxiom.enrollment_form.id;


--
-- TOC entry 772 (class 1259 OID 889998)
-- Name: enrollment_form_personal_data; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_personal_data (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    personal_data text
);


ALTER TABLE vaxiom.enrollment_form_personal_data OWNER TO postgres;

--
-- TOC entry 771 (class 1259 OID 889996)
-- Name: enrollment_form_personal_data_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_personal_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_personal_data_id_seq OWNER TO postgres;

--
-- TOC entry 9021 (class 0 OID 0)
-- Dependencies: 771
-- Name: enrollment_form_personal_data_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_personal_data_id_seq OWNED BY vaxiom.enrollment_form_personal_data.id;


--
-- TOC entry 773 (class 1259 OID 890005)
-- Name: enrollment_form_source_event; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_source_event (
    id numeric(20,0) NOT NULL,
    source_type character varying(45),
    sub_type character varying(45),
    configuration character varying(1200),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.enrollment_form_source_event OWNER TO postgres;

--
-- TOC entry 774 (class 1259 OID 890011)
-- Name: enrollment_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_group (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    group_type character varying(45)
);


ALTER TABLE vaxiom.enrollment_group OWNER TO postgres;

--
-- TOC entry 776 (class 1259 OID 890017)
-- Name: enrollment_group_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_group_enrollment (
    enrollment_group_id numeric(20,0) NOT NULL,
    enrollment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.enrollment_group_enrollment OWNER TO postgres;

--
-- TOC entry 775 (class 1259 OID 890014)
-- Name: enrollment_groups_enrollment_legal_entity; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_groups_enrollment_legal_entity (
    enrollment_group_id numeric(20,0) NOT NULL,
    legal_entity_id numeric(20,0) NOT NULL,
    enrollment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.enrollment_groups_enrollment_legal_entity OWNER TO postgres;

--
-- TOC entry 777 (class 1259 OID 890020)
-- Name: enrollment_schedule_billing_groups; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_schedule_billing_groups (
    enrollment_id numeric(20,0) NOT NULL,
    carrier character varying(45) NOT NULL,
    legal_entity_id numeric(20,0) NOT NULL,
    billing_group character varying(50)
);


ALTER TABLE vaxiom.enrollment_schedule_billing_groups OWNER TO postgres;

--
-- TOC entry 9022 (class 0 OID 0)
-- Dependencies: 777
-- Name: COLUMN enrollment_schedule_billing_groups.enrollment_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.enrollment_schedule_billing_groups.enrollment_id IS 'Enrollment primary key';


--
-- TOC entry 9023 (class 0 OID 0)
-- Dependencies: 777
-- Name: COLUMN enrollment_schedule_billing_groups.carrier; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.enrollment_schedule_billing_groups.carrier IS 'Insurance Plan carrier';


--
-- TOC entry 9024 (class 0 OID 0)
-- Dependencies: 777
-- Name: COLUMN enrollment_schedule_billing_groups.legal_entity_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.enrollment_schedule_billing_groups.legal_entity_id IS 'Legal entity primary key (ENROLLMENT | legal_entities_participating_enrollment)';


--
-- TOC entry 9025 (class 0 OID 0)
-- Dependencies: 777
-- Name: COLUMN enrollment_schedule_billing_groups.billing_group; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.enrollment_schedule_billing_groups.billing_group IS 'Manual ID of the carrier';


--
-- TOC entry 778 (class 1259 OID 890023)
-- Name: equipment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.equipment (
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.equipment OWNER TO postgres;

--
-- TOC entry 779 (class 1259 OID 890027)
-- Name: external_offices; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.external_offices (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    postal_address_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.external_offices OWNER TO postgres;

--
-- TOC entry 780 (class 1259 OID 890030)
-- Name: financial_settings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.financial_settings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    payment_term character varying(255),
    autopay_payment_term character varying(255),
    realization character varying(255),
    realization_dom integer
);


ALTER TABLE vaxiom.financial_settings OWNER TO postgres;

--
-- TOC entry 781 (class 1259 OID 890036)
-- Name: fix_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.fix_adjustments (
    id numeric(20,0) NOT NULL,
    from_migration boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.fix_adjustments OWNER TO postgres;

--
-- TOC entry 782 (class 1259 OID 890040)
-- Name: gf10219; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.gf10219 (
    patient_id numeric(20,0),
    appointment_id numeric(20,0),
    tx_id numeric(20,0),
    wrong_next_appointment_id numeric(20,0),
    wrong_tx_id numeric(20,0),
    correct_next_appointment_id numeric(20,0),
    correct_next_tx_id numeric(20,0)
);


ALTER TABLE vaxiom.gf10219 OWNER TO postgres;

--
-- TOC entry 783 (class 1259 OID 890043)
-- Name: gf9412_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.gf9412_payments (
    human_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    patient_name character varying(511),
    insured_id numeric(20,0) NOT NULL,
    payment_account_id_ok numeric(20,0),
    company_ok character varying(255) NOT NULL,
    payment_id numeric(20,0) NOT NULL,
    payment_sys_created timestamp without time zone,
    payer_name character varying(255),
    payment_account_id_wrong numeric(20,0),
    migrated boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.gf9412_payments OWNER TO postgres;

--
-- TOC entry 784 (class 1259 OID 890050)
-- Name: gf9412_receivables; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.gf9412_receivables (
    human_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    patient_name character varying(511),
    receivable_id numeric(20,0) NOT NULL,
    rtype character varying(20) NOT NULL,
    rec_sys_created timestamp without time zone,
    insured_id numeric(20,0) NOT NULL,
    payment_account_id_ok numeric(20,0),
    company_ok character varying(255) NOT NULL,
    receivable_account_id_wrong numeric(20,0),
    company_wrong character varying(255)
);


ALTER TABLE vaxiom.gf9412_receivables OWNER TO postgres;

--
-- TOC entry 785 (class 1259 OID 890056)
-- Name: gf9412_unapplied; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.gf9412_unapplied (
    human_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    patient_name character varying(511),
    payment_id numeric(20,0) NOT NULL,
    payment_account_id_ok numeric(20,0) NOT NULL,
    payment_account_id_wrong numeric(20,0),
    account_type character varying(30) NOT NULL,
    id numeric(20,0) NOT NULL,
    name character varying(50) NOT NULL,
    billing_center_name character varying(255),
    payer_name character varying(255)
);


ALTER TABLE vaxiom.gf9412_unapplied OWNER TO postgres;

--
-- TOC entry 787 (class 1259 OID 890065)
-- Name: hr_admin_config; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.hr_admin_config (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    name character varying(45) NOT NULL,
    value character varying(100)
);


ALTER TABLE vaxiom.hr_admin_config OWNER TO postgres;

--
-- TOC entry 786 (class 1259 OID 890062)
-- Name: hradmin_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.hradmin_enrollment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    start_date date NOT NULL,
    end_date date NOT NULL,
    mode character varying(10) NOT NULL,
    company_name character varying(80) NOT NULL,
    tax_id character varying(20) NOT NULL,
    sic_code character varying(8) NOT NULL,
    street character varying(60) NOT NULL,
    state character varying(40) NOT NULL,
    city character varying(60) NOT NULL,
    zip_code character varying(20) NOT NULL
);


ALTER TABLE vaxiom.hradmin_enrollment OWNER TO postgres;

--
-- TOC entry 788 (class 1259 OID 890068)
-- Name: image_series; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.image_series (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0),
    type_id numeric(20,0),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created date,
    sys_last_modified date,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.image_series OWNER TO postgres;

--
-- TOC entry 789 (class 1259 OID 890071)
-- Name: image_series_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.image_series_types (
    id numeric(20,0) NOT NULL,
    name character varying(100) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created date,
    sys_last_modified date,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.image_series_types OWNER TO postgres;

--
-- TOC entry 840 (class 1259 OID 890265)
-- Name: in_network_discounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.in_network_discounts (
    id numeric(20,0) NOT NULL,
    network_sheet_fee_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.in_network_discounts OWNER TO postgres;

--
-- TOC entry 790 (class 1259 OID 890074)
-- Name: installment_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.installment_charges (
    id numeric(20,0) NOT NULL,
    payment_plan_id numeric(20,0)
);


ALTER TABLE vaxiom.installment_charges OWNER TO postgres;

--
-- TOC entry 791 (class 1259 OID 890077)
-- Name: insurance_billing_centers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_billing_centers (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255),
    payer_id character varying(255),
    contact_postal_address_id numeric(20,0),
    organization_id numeric(20,0),
    contact_phone_id numeric(20,0),
    contact_email_id numeric(20,0),
    master_id numeric(20,0)
);


ALTER TABLE vaxiom.insurance_billing_centers OWNER TO postgres;

--
-- TOC entry 792 (class 1259 OID 890083)
-- Name: insurance_claims_setup; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_claims_setup (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    billing_entity_id numeric(20,0) NOT NULL,
    legal_entity_npi_id numeric(20,0),
    sys_organization_address_id numeric(20,0),
    sys_organization_contact_method_id numeric(20,0),
    license_number_type character varying(45) DEFAULT 'NONE'::character varying NOT NULL,
    license_number_value character varying(45),
    ssn character(11),
    tin character varying(45),
    tin_id numeric(20,0)
);


ALTER TABLE vaxiom.insurance_claims_setup OWNER TO postgres;

--
-- TOC entry 794 (class 1259 OID 890093)
-- Name: insurance_code_val; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_code_val (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    coverage numeric(30,10),
    fee numeric(30,10),
    insurance_code_id numeric(20,0) NOT NULL,
    insurance_company_id numeric(20,0),
    organization_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_code_val OWNER TO postgres;

--
-- TOC entry 793 (class 1259 OID 890087)
-- Name: insurance_codes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_codes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    code character varying(255) NOT NULL,
    title character varying(255)
);


ALTER TABLE vaxiom.insurance_codes OWNER TO postgres;

--
-- TOC entry 795 (class 1259 OID 890096)
-- Name: insurance_companies; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_companies (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0),
    carrier_name character varying(255) NOT NULL,
    electronic boolean,
    website character varying(255),
    installment_interval character varying(255),
    continuation_claims_required boolean DEFAULT false NOT NULL,
    postal_address_id numeric(20,0),
    am_name character varying(255),
    am_phone_number_id numeric(20,0),
    am_email_address_id numeric(20,0),
    master_id numeric(20,0),
    medicaid boolean DEFAULT false NOT NULL,
    medicaid_state character varying(255)
);


ALTER TABLE vaxiom.insurance_companies OWNER TO postgres;

--
-- TOC entry 796 (class 1259 OID 890104)
-- Name: insurance_company_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_company_payments (
    insurance_company_id numeric(20,0) NOT NULL,
    company_name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.insurance_company_payments OWNER TO postgres;

--
-- TOC entry 797 (class 1259 OID 890107)
-- Name: insurance_company_phone_associations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_company_phone_associations (
    id numeric(20,0) NOT NULL,
    insurance_company_id numeric(20,0) NOT NULL,
    contact_method_id numeric(20,0) NOT NULL,
    description character varying(255),
    preferred boolean DEFAULT false NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_company_phone_associations OWNER TO postgres;

--
-- TOC entry 799 (class 1259 OID 890117)
-- Name: insurance_payment_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_accounts (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    insurance_company_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_payment_accounts OWNER TO postgres;

--
-- TOC entry 800 (class 1259 OID 890120)
-- Name: insurance_payment_corrections; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_corrections (
    id numeric(20,0) NOT NULL,
    correction_type_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_payment_corrections OWNER TO postgres;

--
-- TOC entry 9026 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN insurance_payment_corrections.id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_corrections.id IS 'corresponds to a insurance_payment_movements id';


--
-- TOC entry 9027 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN insurance_payment_corrections.correction_type_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_corrections.correction_type_id IS 'Correction types such as  Posting Error,Recoupled Payment,Reversed Orthobanc Payment';


--
-- TOC entry 801 (class 1259 OID 890123)
-- Name: insurance_payment_movements; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_movements (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_created_at numeric(20,0) NOT NULL,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    dtype character varying(31) NOT NULL,
    insurance_payment_id numeric(20,0) NOT NULL,
    moved_on timestamp without time zone,
    moved_at numeric(20,0) NOT NULL,
    amount numeric(30,10) NOT NULL,
    notes character varying(500)
);


ALTER TABLE vaxiom.insurance_payment_movements OWNER TO postgres;

--
-- TOC entry 9028 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.dtype; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.dtype IS 'Indicates the actual movement type';


--
-- TOC entry 9029 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.insurance_payment_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.insurance_payment_id IS 'Insurance payment id';


--
-- TOC entry 9030 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.moved_on; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.moved_on IS 'timestamp when movement was created';


--
-- TOC entry 9031 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.moved_at; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.moved_at IS 'Location id where movement was created';


--
-- TOC entry 9032 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.amount; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.amount IS 'Actual amount moved';


--
-- TOC entry 9033 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.notes; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.notes IS 'Notes related  for movement ';


--
-- TOC entry 802 (class 1259 OID 890129)
-- Name: insurance_payment_refunds; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_refunds (
    id numeric(20,0) NOT NULL,
    refund_type character varying(20) NOT NULL,
    payment_method character varying(50),
    payment_method_number character varying(200)
);


ALTER TABLE vaxiom.insurance_payment_refunds OWNER TO postgres;

--
-- TOC entry 9034 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN insurance_payment_refunds.id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_refunds.id IS 'corresponds to a insurance_payment_movements id';


--
-- TOC entry 9035 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN insurance_payment_refunds.refund_type; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_refunds.refund_type IS 'Refund type such as Overpayment,Posting Error, Transfer Out ';


--
-- TOC entry 9036 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN insurance_payment_refunds.payment_method; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_refunds.payment_method IS 'Refund payment methods , such as Cash,Check or CreditCard';


--
-- TOC entry 9037 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN insurance_payment_refunds.payment_method_number; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_refunds.payment_method_number IS 'Refund payment methods , such as Cash,Check or CreditCard';


--
-- TOC entry 803 (class 1259 OID 890132)
-- Name: insurance_payment_transfers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_transfers (
    id numeric(20,0) NOT NULL,
    payment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_payment_transfers OWNER TO postgres;

--
-- TOC entry 9038 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN insurance_payment_transfers.id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_transfers.id IS 'corresponds to a insurance_payment_movements id';


--
-- TOC entry 9039 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN insurance_payment_transfers.payment_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_transfers.payment_id IS 'Refers to the actual patient  payment created for the transfer';


--
-- TOC entry 798 (class 1259 OID 890111)
-- Name: insurance_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payments (
    id numeric(20,0) NOT NULL,
    human_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_created_at numeric(20,0) NOT NULL,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    insurance_company_id numeric(20,0) NOT NULL,
    dtype character varying(20) NOT NULL,
    posted_on timestamp without time zone,
    posted_at numeric(20,0),
    amount numeric(30,10),
    payment_type_id numeric(20,0),
    payer_name character varying(255),
    check_or_card_number character varying(30),
    cc_expire character varying(4),
    notes character varying(500)
);


ALTER TABLE vaxiom.insurance_payments OWNER TO postgres;

--
-- TOC entry 9040 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.insurance_company_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.insurance_company_id IS 'Insurance company payment';


--
-- TOC entry 9041 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.dtype; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.dtype IS 'insurance payment type such as check,credit, cash, other ';


--
-- TOC entry 9042 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.posted_on; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.posted_on IS 'timestamp when payment was created';


--
-- TOC entry 9043 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.posted_at; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.posted_at IS 'Location id where payment was created';


--
-- TOC entry 9044 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.amount; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.amount IS 'Actual amount posted';


--
-- TOC entry 9045 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.payment_type_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.payment_type_id IS 'Insurance Payment method type used for payment ';


--
-- TOC entry 9046 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.payer_name; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.payer_name IS 'Insurance Payment method payer name ';


--
-- TOC entry 9047 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.check_or_card_number; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.check_or_card_number IS 'Insurance Credit card last four digits or Check number ';


--
-- TOC entry 9048 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.cc_expire; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.cc_expire IS 'Insurance Credit Payment card expire ';


--
-- TOC entry 804 (class 1259 OID 890135)
-- Name: insurance_plan; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan (
    id numeric(20,0) NOT NULL,
    benefit_template_id numeric(20,0),
    record_type character varying(45),
    plan_name character varying(45),
    other_fields character varying(1200),
    benefit_type character varying(45),
    carrier character varying(45),
    summary_plan_pdf character varying(45),
    coverage_levels character varying(100),
    employer character varying(100),
    employee_clasification character varying(45),
    enrollment_class character varying(45),
    payroll_deduction character varying(45) NOT NULL,
    premium_allocation_type character varying(45),
    premium_allocation_employer_value numeric(30,10),
    rate_type character varying(45),
    wave_allowed boolean,
    wave_with_reason boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0),
    renewal_date date,
    policy_number character varying(45),
    inception_date date,
    smoker_definition character varying(500),
    deleted boolean DEFAULT false,
    spouse_premium_calculation character varying(20),
    benefit_amount_type character varying(45),
    parent_insurance_plan_id numeric(20,0),
    summary_plan_pdf_file_name character varying(60),
    prior_policy_number character varying(45)
);


ALTER TABLE vaxiom.insurance_plan OWNER TO postgres;

--
-- TOC entry 9049 (class 0 OID 0)
-- Dependencies: 804
-- Name: TABLE insurance_plan; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON TABLE vaxiom.insurance_plan IS 'table with insuracen plan definition';


--
-- TOC entry 9050 (class 0 OID 0)
-- Dependencies: 804
-- Name: COLUMN insurance_plan.record_type; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_plan.record_type IS 'Definines if record is a template or specyfic plan definition';


--
-- TOC entry 805 (class 1259 OID 890142)
-- Name: insurance_plan_adandd; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_adandd (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL,
    employee_rate numeric(30,10),
    spouse_rate numeric(30,10),
    child_rate numeric(30,10),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_adandd OWNER TO postgres;

--
-- TOC entry 806 (class 1259 OID 890145)
-- Name: insurance_plan_age_and_gender_banded_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_age_and_gender_banded_rate (
    id numeric(20,0) NOT NULL,
    min_age integer NOT NULL,
    max_age integer NOT NULL,
    gender character varying(45) NOT NULL,
    only_me_rate numeric(30,10) NOT NULL,
    me_and_spouse_rate numeric(30,10) NOT NULL,
    me_and_chidren_rate numeric(30,10) NOT NULL,
    family_rate numeric(30,10) NOT NULL,
    tabaco boolean NOT NULL,
    me_and_one_child numeric(30,10) NOT NULL,
    only_me_rate_tobacco numeric(30,10),
    me_and_spouse_rate_tobacco numeric(30,10),
    me_and_chidren_rate_tobacco numeric(30,10),
    family_rate_tobacco numeric(30,10),
    me_and_one_child_tobacco numeric(30,10)
);


ALTER TABLE vaxiom.insurance_plan_age_and_gender_banded_rate OWNER TO postgres;

--
-- TOC entry 807 (class 1259 OID 890148)
-- Name: insurance_plan_age_banded_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_age_banded_rate (
    id numeric(20,0) NOT NULL,
    min_age integer NOT NULL,
    max_age integer NOT NULL,
    rate numeric(30,10) NOT NULL,
    tabaco boolean NOT NULL,
    tobacco_rate numeric(30,10)
);


ALTER TABLE vaxiom.insurance_plan_age_banded_rate OWNER TO postgres;

--
-- TOC entry 808 (class 1259 OID 890151)
-- Name: insurance_plan_age_reduction; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_age_reduction (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL,
    min_reduced_amt numeric(30,10),
    min_reduction_amt numeric(30,10),
    applies_to_employee boolean,
    applies_to_spouse boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_age_reduction OWNER TO postgres;

--
-- TOC entry 809 (class 1259 OID 890154)
-- Name: insurance_plan_age_reduction_detail; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_age_reduction_detail (
    id numeric(20,0) NOT NULL,
    insurance_plan_age_reduction_id numeric(20,0) NOT NULL,
    age bigint NOT NULL,
    reduction double precision NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_age_reduction_detail OWNER TO postgres;

--
-- TOC entry 810 (class 1259 OID 890157)
-- Name: insurance_plan_benefit_amount; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_benefit_amount (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    rounding_rule character varying(45),
    rounding_rule_value numeric(30,10),
    insurance_plan_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_benefit_amount OWNER TO postgres;

--
-- TOC entry 811 (class 1259 OID 890160)
-- Name: insurance_plan_children_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_children_rate (
    id numeric(20,0) NOT NULL,
    rate numeric(30,10),
    tobacco_rate numeric(30,10),
    coverage_type character varying(20)
);


ALTER TABLE vaxiom.insurance_plan_children_rate OWNER TO postgres;

--
-- TOC entry 812 (class 1259 OID 890163)
-- Name: insurance_plan_complex_benefit_amount; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_complex_benefit_amount (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_complex_benefit_amount OWNER TO postgres;

--
-- TOC entry 813 (class 1259 OID 890166)
-- Name: insurance_plan_complex_benefit_amount_item; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_complex_benefit_amount_item (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    insured_type character varying(45) NOT NULL,
    min_amount_value numeric(30,10),
    max_amount_type character varying(45),
    max_amount_money_value numeric(30,10),
    max_amount_percentage_value numeric(30,10),
    max_amount_earnings_multiply_value numeric(30,10),
    increment_amounts numeric(30,10),
    insurance_plan_complex_benefit_amount_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_complex_benefit_amount_item OWNER TO postgres;

--
-- TOC entry 814 (class 1259 OID 890169)
-- Name: insurance_plan_composition_detailed_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_composition_detailed_rate (
    id numeric(20,0) NOT NULL,
    employee_amount numeric(30,10) NOT NULL,
    spouse_amount numeric(30,10) NOT NULL,
    child_amount numeric(30,10) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_composition_detailed_rate OWNER TO postgres;

--
-- TOC entry 815 (class 1259 OID 890172)
-- Name: insurance_plan_composition_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_composition_rate (
    id numeric(20,0) NOT NULL,
    only_me_amount numeric(30,10) NOT NULL,
    me_and_spouse_rate numeric(30,10) NOT NULL,
    me_and_chidren_amount numeric(30,10) NOT NULL,
    family_amount numeric(30,10) NOT NULL,
    me_and_one_child numeric(30,10) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_composition_rate OWNER TO postgres;

--
-- TOC entry 816 (class 1259 OID 890175)
-- Name: insurance_plan_employers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_employers (
    insurance_plan_id numeric(20,0) NOT NULL,
    employer_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_employers OWNER TO postgres;

--
-- TOC entry 817 (class 1259 OID 890178)
-- Name: insurance_plan_fixed_limit; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_fixed_limit (
    id numeric(20,0) NOT NULL,
    amount_min numeric(30,10),
    amount_max numeric(30,10),
    insurance_limits_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    alert_limit numeric(30,10) DEFAULT '0'::numeric NOT NULL,
    step integer DEFAULT 1
);


ALTER TABLE vaxiom.insurance_plan_fixed_limit OWNER TO postgres;

--
-- TOC entry 818 (class 1259 OID 890183)
-- Name: insurance_plan_guaranteed_increase; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_guaranteed_increase (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0),
    enabled boolean,
    increment numeric(30,10),
    applies_to_employee boolean,
    applies_to_spouse boolean,
    applies_to_child boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_guaranteed_increase OWNER TO postgres;

--
-- TOC entry 819 (class 1259 OID 890186)
-- Name: insurance_plan_guaranteed_issue; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_guaranteed_issue (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0),
    enabled boolean,
    amount numeric(30,10),
    availability character varying(20),
    first_time boolean,
    grandfather_rule boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    amount_child numeric(30,10),
    amount_spouse numeric(30,10),
    availability_child character varying(20),
    availability_spouse character varying(20),
    enabled_me boolean,
    enabled_spouse boolean,
    enabled_child boolean
);


ALTER TABLE vaxiom.insurance_plan_guaranteed_issue OWNER TO postgres;

--
-- TOC entry 820 (class 1259 OID 890189)
-- Name: insurance_plan_has_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_has_enrollment (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    insurance_plan_id numeric(20,0) NOT NULL,
    enrollment_id numeric(20,0) NOT NULL,
    status character varying(45),
    removed boolean DEFAULT false
);


ALTER TABLE vaxiom.insurance_plan_has_enrollment OWNER TO postgres;

--
-- TOC entry 821 (class 1259 OID 890193)
-- Name: insurance_plan_has_enrollment_has_enrollment_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_has_enrollment_has_enrollment_group (
    insurance_plan_has_enrollment_id numeric(20,0) NOT NULL,
    enrollment_group_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_has_enrollment_has_enrollment_group OWNER TO postgres;

--
-- TOC entry 822 (class 1259 OID 890196)
-- Name: insurance_plan_limits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_limits (
    id numeric(20,0) NOT NULL,
    type character varying(45),
    insurance_plan_id numeric(20,0) NOT NULL,
    benefit_type character varying(45),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_limits OWNER TO postgres;

--
-- TOC entry 823 (class 1259 OID 890199)
-- Name: insurance_plan_medical_connection; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_medical_connection (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0),
    enabled boolean,
    applies_to_employee boolean,
    applies_to_spouse boolean,
    applies_to_child boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_medical_connection OWNER TO postgres;

--
-- TOC entry 824 (class 1259 OID 890202)
-- Name: insurance_plan_misc; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_misc (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL,
    is_bonus_up boolean,
    offer_adandd boolean,
    payments_begin integer,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_misc OWNER TO postgres;

--
-- TOC entry 825 (class 1259 OID 890205)
-- Name: insurance_plan_rates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_rates (
    id numeric(20,0) NOT NULL,
    type character varying(45),
    insurance_plan_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_rates OWNER TO postgres;

--
-- TOC entry 826 (class 1259 OID 890208)
-- Name: insurance_plan_saving_account; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_saving_account (
    insurance_plan_id numeric(20,0) NOT NULL,
    id numeric(20,0) NOT NULL,
    name character varying(45) NOT NULL,
    limit_amt numeric(30,10),
    employer_cost numeric(30,10) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enabled boolean,
    employer_contributes boolean,
    summary_plan_pdf character varying(255),
    type character varying(45),
    summary_plan_pdf_file_name character varying(255),
    tpa character varying(60)
);


ALTER TABLE vaxiom.insurance_plan_saving_account OWNER TO postgres;

--
-- TOC entry 827 (class 1259 OID 890214)
-- Name: insurance_plan_saving_accounts_limits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_saving_accounts_limits (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    year integer NOT NULL,
    hsa_individual_limit numeric(30,10) NOT NULL,
    hsa_family_limit numeric(30,10) NOT NULL,
    hsa_catch_up numeric(30,10) NOT NULL,
    fsa_health numeric(30,10) NOT NULL,
    fsa_dependent_care numeric(30,10) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_saving_accounts_limits OWNER TO postgres;

--
-- TOC entry 828 (class 1259 OID 890217)
-- Name: insurance_plan_separate_age_banded_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_separate_age_banded_rate (
    id numeric(20,0) NOT NULL,
    min_age integer NOT NULL,
    max_age integer NOT NULL,
    rate numeric(30,10) NOT NULL,
    tabaco boolean NOT NULL,
    tobacco_rate numeric(30,10),
    insured_type character varying(20) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_separate_age_banded_rate OWNER TO postgres;

--
-- TOC entry 829 (class 1259 OID 890220)
-- Name: insurance_plan_simple_benefit_amount; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_simple_benefit_amount (
    employee_amount_type character varying(45) NOT NULL,
    employee_amount_flat_value numeric(30,10),
    employ_amount_multiply_value numeric(30,10),
    employee_min_amount_value numeric(30,10),
    employee_max_amount_value numeric(30,10),
    spouse_amount_value numeric(30,10),
    children_amount_value numeric(30,10),
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_simple_benefit_amount OWNER TO postgres;

--
-- TOC entry 830 (class 1259 OID 890223)
-- Name: insurance_plan_slider_limit; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_slider_limit (
    id numeric(20,0) NOT NULL,
    min_value numeric(30,10),
    max_value_in_percent numeric(30,10),
    step numeric(30,10),
    rounding character varying(45),
    insurance_limits_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    alert_limit numeric(30,10)
);


ALTER TABLE vaxiom.insurance_plan_slider_limit OWNER TO postgres;

--
-- TOC entry 831 (class 1259 OID 890226)
-- Name: insurance_plan_summary_fields; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_summary_fields (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    field_name character varying(150),
    value0 character varying(100),
    value1 character varying(100),
    value2 character varying(100),
    value3 character varying(100),
    item_order integer NOT NULL,
    is_visible boolean NOT NULL,
    ui_type character varying(45) NOT NULL,
    is_default boolean NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_summary_fields OWNER TO postgres;

--
-- TOC entry 832 (class 1259 OID 890232)
-- Name: insurance_plan_summary_fields_default; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_summary_fields_default (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    field_name character varying(150),
    value0 character varying(100),
    value1 character varying(100),
    value2 character varying(100),
    value3 character varying(100),
    item_order integer NOT NULL,
    is_visible boolean,
    ui_type character varying(45),
    insurance_plan_type character varying(45)
);


ALTER TABLE vaxiom.insurance_plan_summary_fields_default OWNER TO postgres;

--
-- TOC entry 833 (class 1259 OID 890238)
-- Name: insurance_plan_term_disability_benefit_amount; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_term_disability_benefit_amount (
    id numeric(20,0) NOT NULL,
    annual_earning_percent numeric(3,0),
    term_disability_benefit_option character varying(45),
    min_amount_value numeric(30,10),
    increment_amount numeric(30,10),
    benefit_dration character varying(150),
    max_amount_value numeric(30,10)
);


ALTER TABLE vaxiom.insurance_plan_term_disability_benefit_amount OWNER TO postgres;

--
-- TOC entry 834 (class 1259 OID 890241)
-- Name: insurance_subscriptions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_subscriptions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    insurance_plan_id numeric(20,0),
    employer_id numeric(20,0),
    notes character varying(255),
    member_id character varying(255),
    active boolean DEFAULT false NOT NULL,
    enrollment_date date,
    person_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_subscriptions OWNER TO postgres;

--
-- TOC entry 835 (class 1259 OID 890249)
-- Name: insured; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insured (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    notes character varying(255),
    insurance_subscription_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insured OWNER TO postgres;

--
-- TOC entry 836 (class 1259 OID 890253)
-- Name: insured_benefit_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insured_benefit_values (
    id numeric(20,0) NOT NULL,
    pre_tax_rate numeric(30,10),
    post_tax_rate numeric(30,10),
    employer_rate numeric(30,10),
    total_rate numeric(30,10),
    benefit_amout numeric(30,10),
    employee_insured_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0),
    reduction numeric(30,10),
    reduced_benefit_amount numeric(30,10)
);


ALTER TABLE vaxiom.insured_benefit_values OWNER TO postgres;

--
-- TOC entry 837 (class 1259 OID 890256)
-- Name: invitations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.invitations (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    event character varying(255),
    invitee numeric(20,0) NOT NULL,
    inviter numeric(20,0) NOT NULL,
    updated boolean
);


ALTER TABLE vaxiom.invitations OWNER TO postgres;

--
-- TOC entry 838 (class 1259 OID 890259)
-- Name: invoice_export_queue; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.invoice_export_queue (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    target character varying(20),
    invoice_id numeric(20,0) NOT NULL,
    account_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.invoice_export_queue OWNER TO postgres;

--
-- TOC entry 839 (class 1259 OID 890262)
-- Name: invoice_import_export_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.invoice_import_export_log (
    id numeric(20,0) NOT NULL,
    type character varying(20),
    account_id numeric(20,0) NOT NULL,
    invoice_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.invoice_import_export_log OWNER TO postgres;

--
-- TOC entry 841 (class 1259 OID 890268)
-- Name: jobtitle; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.jobtitle (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(60),
    sys_organizations_id numeric(20,0) NOT NULL,
    jobtitle_group_id numeric(20,0) NOT NULL,
    mediacal_provider boolean NOT NULL
);


ALTER TABLE vaxiom.jobtitle OWNER TO postgres;

--
-- TOC entry 842 (class 1259 OID 890271)
-- Name: jobtitle_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.jobtitle_group (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(60),
    eeo_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.jobtitle_group OWNER TO postgres;

--
-- TOC entry 843 (class 1259 OID 890274)
-- Name: late_fee_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.late_fee_charges (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.late_fee_charges OWNER TO postgres;

--
-- TOC entry 844 (class 1259 OID 890277)
-- Name: legal_entities_participating_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entities_participating_enrollment (
    legal_entity_id numeric(20,0) NOT NULL,
    enrollment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entities_participating_enrollment OWNER TO postgres;

--
-- TOC entry 845 (class 1259 OID 890280)
-- Name: legal_entity; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0),
    parent_id numeric(20,0),
    relationship character varying(45) NOT NULL,
    structure_type character varying(45),
    structure_subtype character varying(45),
    legal_name character varying(255),
    sic_code character varying(10),
    naics2012_code character varying(20),
    fein character(10),
    county_id numeric(20,0),
    state character varying(25),
    status character varying(20) DEFAULT 'DRAFT'::character varying NOT NULL,
    has_employees boolean DEFAULT true NOT NULL,
    exp_empl_agricultural integer,
    exp_empl_household integer,
    exp_empl_other integer,
    biling_groups_setup character varying(45) DEFAULT 'DEPARTMENT'::character varying,
    available_payroll_cycle character varying(100),
    allow_new_hires boolean DEFAULT true
);


ALTER TABLE vaxiom.legal_entity OWNER TO postgres;

--
-- TOC entry 846 (class 1259 OID 890290)
-- Name: legal_entity_address; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_address (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(45),
    street character varying(45),
    city character varying(45),
    state character varying(45),
    zip_code character varying(45),
    occupacy character varying(45),
    legal_entity_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_address OWNER TO postgres;

--
-- TOC entry 847 (class 1259 OID 890293)
-- Name: legal_entity_address_attachment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_address_attachment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(45) NOT NULL,
    file_id character varying(127),
    file_mime_type character varying(255),
    file_name character varying(127),
    file_size numeric(20,0),
    legal_entity_address_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_address_attachment OWNER TO postgres;

--
-- TOC entry 848 (class 1259 OID 890299)
-- Name: legal_entity_billing_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_billing_group (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    legal_entity_id numeric(20,0) NOT NULL,
    name character varying(45) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_billing_group OWNER TO postgres;

--
-- TOC entry 849 (class 1259 OID 890302)
-- Name: legal_entity_billing_group_has_sys_organizations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_billing_group_has_sys_organizations (
    legal_entity_billing_group_id numeric(20,0) NOT NULL,
    sys_organizations_id numeric(20,0) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.legal_entity_billing_group_has_sys_organizations OWNER TO postgres;

--
-- TOC entry 850 (class 1259 OID 890305)
-- Name: legal_entity_dba; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_dba (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    legal_entity_id numeric(20,0) NOT NULL,
    dba_name character varying(100) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_dba OWNER TO postgres;

--
-- TOC entry 851 (class 1259 OID 890308)
-- Name: legal_entity_insurance_benefit; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_insurance_benefit (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    legal_entity_id numeric(20,0) NOT NULL,
    benefits_holder numeric(20,0) NOT NULL,
    broker_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_insurance_benefit OWNER TO postgres;

--
-- TOC entry 852 (class 1259 OID 890311)
-- Name: legal_entity_insurance_benefit_has_division; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_insurance_benefit_has_division (
    legal_entity_insurance_benefit_id numeric(20,0) NOT NULL,
    division_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_insurance_benefit_has_division OWNER TO postgres;

--
-- TOC entry 853 (class 1259 OID 890314)
-- Name: legal_entity_npi; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_npi (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    name character varying(100) NOT NULL,
    description character varying(200),
    legal_entity_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_npi OWNER TO postgres;

--
-- TOC entry 854 (class 1259 OID 890317)
-- Name: legal_entity_operational_structures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_operational_structures (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    is_hr boolean NOT NULL,
    hr_type character varying(45),
    is_ib boolean NOT NULL,
    is_pm boolean NOT NULL,
    is_acc boolean NOT NULL,
    legal_entity_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_operational_structures OWNER TO postgres;

--
-- TOC entry 9051 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.is_hr; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.is_hr IS 'human resources';


--
-- TOC entry 9052 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.hr_type; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.hr_type IS 'human resources type (basic, enhanced)';


--
-- TOC entry 9053 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.is_ib; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.is_ib IS 'insurance benefits';


--
-- TOC entry 9054 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.is_pm; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.is_pm IS 'practice management';


--
-- TOC entry 9055 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.is_acc; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.is_acc IS 'accounting';


--
-- TOC entry 855 (class 1259 OID 890320)
-- Name: legal_entity_owner; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_owner (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    legal_entity_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL,
    name character varying(45) NOT NULL,
    name_2 character varying(45),
    fein character(10),
    ssn character(11),
    birth_date date,
    percentage numeric(8,5) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_owner OWNER TO postgres;

--
-- TOC entry 856 (class 1259 OID 890323)
-- Name: legal_entity_tin; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_tin (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    name character varying(20) NOT NULL,
    description character varying(20),
    legal_entity_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_tin OWNER TO postgres;

--
-- TOC entry 857 (class 1259 OID 890326)
-- Name: lightbarcalls; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.lightbarcalls (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    calling_since timestamp without time zone,
    urgent boolean NOT NULL,
    booking_id numeric(20,0),
    chair_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.lightbarcalls OWNER TO postgres;

--
-- TOC entry 858 (class 1259 OID 890329)
-- Name: location_access_keys; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.location_access_keys (
    id numeric(20,0) NOT NULL,
    sys_created timestamp without time zone,
    sys_created_by numeric(20,0),
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0) DEFAULT '0'::numeric,
    organization_id numeric(20,0) NOT NULL,
    access_name character varying(100),
    access_key integer NOT NULL,
    enabled boolean DEFAULT false,
    accepts_medicaid boolean DEFAULT false
);


ALTER TABLE vaxiom.location_access_keys OWNER TO postgres;

--
-- TOC entry 859 (class 1259 OID 890335)
-- Name: medical_history; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.medical_history (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    abnormal_breathing boolean,
    additional_info character varying(255),
    aids boolean,
    aids_alert boolean,
    arthritis boolean,
    arthritis_alert boolean,
    asthma boolean,
    asthma_alert boolean,
    blood_disorders boolean,
    blood_disorders_alert boolean,
    brain_injury boolean,
    brain_injury_alert boolean,
    clicking_pain_jaw boolean,
    clicking_pain_jaw_alert boolean,
    diabetes boolean,
    diabetes_alert boolean,
    dizziness_fainting boolean,
    dizziness_fainting_alert boolean,
    epilepsy boolean,
    epilepsy_alert boolean,
    family_member_same_condition character varying(20),
    family_member_same_condition_alert boolean,
    first_tooth_age character varying(255),
    frequent_colds boolean,
    frequent_colds_alert boolean,
    grinding_teeth boolean,
    hearing_difficulties boolean,
    hearing_difficulties_alert boolean,
    heart_trouble boolean,
    heart_trouble_alert boolean,
    hepatitis boolean,
    hepatitis_alert boolean,
    in_good_health boolean,
    in_good_health_alert boolean,
    kidney_liver_disease boolean,
    kidney_liver_disease_alert boolean,
    last_dental_care date,
    lip_biting boolean,
    mouth_breathing boolean,
    nail_biting boolean,
    other_conditions character varying(255),
    other_conditions_alert boolean,
    parents_teeth_removed_crowding boolean,
    parents_teeth_removed_crowding_alert boolean,
    rheumatic_fever boolean,
    rheumatic_fever_alert boolean,
    smoking boolean,
    tobacco boolean DEFAULT false,
    speech_disorders boolean,
    tb boolean,
    tb_alert boolean,
    thumb_sucking boolean,
    tongue_biting boolean,
    tongue_sucking boolean,
    tongue_thrusting boolean,
    under_care_physician character varying(255),
    under_care_physician_alert boolean,
    unfavourable_reactions boolean,
    unfavourable_reactions_alert boolean,
    has_general_dentist boolean DEFAULT false,
    general_dentist_first_name character varying(255),
    general_dentist_phone character varying(100),
    week_floss character varying(255),
    day_brush integer DEFAULT 0,
    toothbrush character varying(100),
    birth_control boolean DEFAULT false,
    birth_control_alert boolean DEFAULT false,
    pregnant boolean DEFAULT false,
    pregnant_alert boolean DEFAULT false,
    pregnancy_week integer DEFAULT 0,
    nursing boolean DEFAULT false,
    nursing_alert boolean DEFAULT false,
    active_dental_insurance boolean DEFAULT false,
    secondary_insurance_card boolean DEFAULT false,
    dental_health character varying(100),
    like_smile boolean DEFAULT false,
    gums boolean DEFAULT false,
    gums_alert boolean DEFAULT false,
    seizures boolean DEFAULT false,
    seizures_alert boolean DEFAULT false,
    antibiotics boolean DEFAULT false,
    antibiotics_alert boolean DEFAULT false,
    problems_previous_procedures boolean DEFAULT false,
    problems_previous_procedures_alert boolean DEFAULT false,
    personal_physician_last_visit timestamp without time zone,
    periodontal_disease boolean DEFAULT false,
    periodontal_disease_alert boolean DEFAULT false,
    general_health character varying(100),
    current_in_pain boolean DEFAULT false,
    clicking_pain boolean DEFAULT false,
    under_care_physician_phone character varying(100),
    physician_active_care boolean DEFAULT false,
    add_or_adhd boolean DEFAULT false,
    add_or_adhd_alert boolean DEFAULT false,
    anemia boolean DEFAULT false,
    anemia_alert boolean DEFAULT false,
    artificial_bones_or_joints_or_valves boolean DEFAULT false,
    artificial_bones_or_joints_or_valves_alert boolean DEFAULT false,
    autism_or_sensory boolean DEFAULT false,
    autism_or_sensory_alert boolean DEFAULT false,
    cancer boolean DEFAULT false,
    cancer_alert boolean DEFAULT false,
    heart_congenital_defect boolean DEFAULT false,
    heart_congenital_defect_alert boolean DEFAULT false,
    drug_alcohol_abuse boolean DEFAULT false,
    drug_alcohol_abuse_alert boolean DEFAULT false,
    emphysema boolean DEFAULT false,
    emphysema_alert boolean DEFAULT false,
    herpes boolean DEFAULT false,
    herpes_alert boolean DEFAULT false,
    frequently_tired boolean DEFAULT false,
    frequently_tired_alert boolean DEFAULT false,
    glaucoma boolean DEFAULT false,
    glaucoma_alert boolean DEFAULT false,
    handicap_or_disabilities boolean DEFAULT false,
    handicap_or_disabilities_alert boolean DEFAULT false,
    hearing_impairment boolean DEFAULT false,
    hearing_impairment_alert boolean DEFAULT false,
    heart_attack boolean DEFAULT false,
    heart_attack_alert boolean DEFAULT false,
    heart_murmur boolean DEFAULT false,
    heart_murmur_alert boolean DEFAULT false,
    heart_surgery boolean DEFAULT false,
    heart_surgery_alert boolean DEFAULT false,
    hemophilia_or_abnormal_bleeding boolean DEFAULT false,
    low_blood_pressure_alert boolean DEFAULT false,
    hemophilia_or_abnormal_bleeding_alert boolean DEFAULT false,
    high_blood_pressure boolean DEFAULT false,
    high_blood_pressure_alert boolean DEFAULT false,
    low_blood_pressure boolean DEFAULT false,
    kidney_diseases boolean DEFAULT false,
    kidney_diseases_alert boolean DEFAULT false,
    mitral_valve_prolapse boolean DEFAULT false,
    mitral_valve_prolapse_alert boolean DEFAULT false,
    psychiatric_problem boolean DEFAULT false,
    psychiatric_problem_alert boolean DEFAULT false,
    radiation_therapy boolean DEFAULT false,
    radiation_therapy_alert boolean DEFAULT false,
    shingles boolean DEFAULT false,
    shingles_alert boolean DEFAULT false,
    sickle_cell_disease boolean DEFAULT false,
    sickle_cell_disease_alert boolean DEFAULT false,
    stomach_troubles_or_ulcers boolean DEFAULT false,
    stomach_troubles_or_ulcers_alert boolean DEFAULT false,
    stroke boolean DEFAULT false,
    stroke_alert boolean DEFAULT false,
    thyroid_problem boolean DEFAULT false,
    thyroid_problem_alert boolean DEFAULT false,
    tuberculosis boolean DEFAULT false,
    tuberculosis_alert boolean DEFAULT false,
    cancer_medication boolean DEFAULT false,
    cancer_medication_alert boolean DEFAULT false,
    respiratory_problems boolean DEFAULT false,
    respiratory_problems_alert boolean DEFAULT false,
    patient_id numeric(20,0) NOT NULL,
    created_from character varying(45) DEFAULT 'CORE'::character varying,
    chief_complaint character varying(100)
);


ALTER TABLE vaxiom.medical_history OWNER TO postgres;

--
-- TOC entry 860 (class 1259 OID 890430)
-- Name: medical_history_medications; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.medical_history_medications (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    alert boolean NOT NULL,
    relation_type character varying(20) NOT NULL,
    medication_id numeric(20,0) NOT NULL,
    medical_history_id numeric(20,0)
);


ALTER TABLE vaxiom.medical_history_medications OWNER TO postgres;

--
-- TOC entry 862 (class 1259 OID 890435)
-- Name: medical_history_patient_relatives_crowding; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.medical_history_patient_relatives_crowding (
    id bigint NOT NULL,
    medical_history_id numeric(20,0),
    patient_relative_crowding character varying(50),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.medical_history_patient_relatives_crowding OWNER TO postgres;

--
-- TOC entry 861 (class 1259 OID 890433)
-- Name: medical_history_patient_relatives_crowding_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.medical_history_patient_relatives_crowding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.medical_history_patient_relatives_crowding_id_seq OWNER TO postgres;

--
-- TOC entry 9056 (class 0 OID 0)
-- Dependencies: 861
-- Name: medical_history_patient_relatives_crowding_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.medical_history_patient_relatives_crowding_id_seq OWNED BY vaxiom.medical_history_patient_relatives_crowding.id;


--
-- TOC entry 863 (class 1259 OID 890439)
-- Name: medications; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.medications (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255),
    deleted bigint DEFAULT '0'::bigint
);


ALTER TABLE vaxiom.medications OWNER TO postgres;

--
-- TOC entry 864 (class 1259 OID 890443)
-- Name: merchant_reports; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.merchant_reports (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    location_id numeric(20,0) NOT NULL,
    check_date date NOT NULL,
    run_date timestamp without time zone,
    success boolean NOT NULL,
    note character varying(1024)
);


ALTER TABLE vaxiom.merchant_reports OWNER TO postgres;

--
-- TOC entry 865 (class 1259 OID 890449)
-- Name: migrated_answers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_answers (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    answer_id numeric(20,0),
    answer_guid character varying(45),
    question_guid character varying(45),
    answer character varying(50)
);


ALTER TABLE vaxiom.migrated_answers OWNER TO postgres;

--
-- TOC entry 867 (class 1259 OID 890455)
-- Name: migrated_appointment_fields; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_appointment_fields (
    appointment_id numeric(20,0) NOT NULL,
    fields_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_appointment_fields OWNER TO postgres;

--
-- TOC entry 868 (class 1259 OID 890458)
-- Name: migrated_appointment_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_appointment_types (
    appointment_type_id numeric(20,0) NOT NULL,
    appointment_type_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_appointment_types OWNER TO postgres;

--
-- TOC entry 866 (class 1259 OID 890452)
-- Name: migrated_appointments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_appointments (
    appointment_id numeric(20,0) NOT NULL,
    appointment_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_appointments OWNER TO postgres;

--
-- TOC entry 869 (class 1259 OID 890461)
-- Name: migrated_chairs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_chairs (
    chair_id numeric(20,0) NOT NULL,
    chair_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_chairs OWNER TO postgres;

--
-- TOC entry 870 (class 1259 OID 890464)
-- Name: migrated_claim; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_claim (
    claim_id numeric(20,0),
    claim_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_claim OWNER TO postgres;

--
-- TOC entry 871 (class 1259 OID 890467)
-- Name: migrated_claim_help; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_claim_help (
    db character varying(8),
    pricestring character varying(50),
    guid character(36) NOT NULL,
    status character varying(12) NOT NULL,
    date date,
    type character varying(50),
    total numeric(30,10),
    patguid character(36)
);


ALTER TABLE vaxiom.migrated_claim_help OWNER TO postgres;

--
-- TOC entry 872 (class 1259 OID 890470)
-- Name: migrated_employees; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_employees (
    employee_id numeric(20,0) NOT NULL,
    employee_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_employees OWNER TO postgres;

--
-- TOC entry 873 (class 1259 OID 890473)
-- Name: migrated_insurance_companies; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_insurance_companies (
    company_id numeric(20,0) NOT NULL,
    company_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_insurance_companies OWNER TO postgres;

--
-- TOC entry 874 (class 1259 OID 890476)
-- Name: migrated_insurance_plans; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_insurance_plans (
    plan_id numeric(20,0) NOT NULL,
    plan_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_insurance_plans OWNER TO postgres;

--
-- TOC entry 875 (class 1259 OID 890479)
-- Name: migrated_locations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_locations (
    location_id numeric(20,0),
    location_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_locations OWNER TO postgres;

--
-- TOC entry 878 (class 1259 OID 890491)
-- Name: migrated_patient_answers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patient_answers (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    patient_answer_id numeric(20,0),
    patient_answer_guid character varying(45),
    patient_guid character varying(45),
    answer_guid character varying(45),
    questionnaire_guid character varying(45),
    question_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_patient_answers OWNER TO postgres;

--
-- TOC entry 879 (class 1259 OID 890494)
-- Name: migrated_patient_documents; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patient_documents (
    id numeric(20,0) NOT NULL,
    person_guid character varying(45) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.migrated_patient_documents OWNER TO postgres;

--
-- TOC entry 880 (class 1259 OID 890497)
-- Name: migrated_patient_image_series; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patient_image_series (
    id numeric(20,0) NOT NULL,
    image_series_id numeric(20,0) NOT NULL,
    image_series_guid character varying(45) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.migrated_patient_image_series OWNER TO postgres;

--
-- TOC entry 876 (class 1259 OID 890482)
-- Name: migrated_patients; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patients (
    patient_id numeric(20,0) NOT NULL,
    patient_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_patients OWNER TO postgres;

--
-- TOC entry 877 (class 1259 OID 890485)
-- Name: migrated_patients_comments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patients_comments (
    id numeric(20,0) NOT NULL,
    comment_id numeric(20,0),
    comment_guid character varying(45),
    note character varying(2048),
    type character varying(50),
    patient_guid character varying(45),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.migrated_patients_comments OWNER TO postgres;

--
-- TOC entry 881 (class 1259 OID 890500)
-- Name: migrated_payment_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_payment_accounts (
    payment_id numeric(20,0) NOT NULL,
    payment_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_payment_accounts OWNER TO postgres;

--
-- TOC entry 882 (class 1259 OID 890503)
-- Name: migrated_persons; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_persons (
    person_id numeric(20,0) NOT NULL,
    person_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_persons OWNER TO postgres;

--
-- TOC entry 883 (class 1259 OID 890506)
-- Name: migrated_questionnaire; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_questionnaire (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    questionnaire_id numeric(20,0),
    questionnaire_guid character varying(45),
    patient_guid character varying(45),
    questionnaire_name character varying(50),
    date timestamp without time zone,
    tx_card_id numeric(20,0)
);


ALTER TABLE vaxiom.migrated_questionnaire OWNER TO postgres;

--
-- TOC entry 884 (class 1259 OID 890509)
-- Name: migrated_questions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_questions (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    question_id numeric(20,0),
    question_guid character varying(45),
    patient_guid character varying(45),
    questionnaire_guid character varying(45),
    question character varying(50),
    question_type character varying(45)
);


ALTER TABLE vaxiom.migrated_questions OWNER TO postgres;

--
-- TOC entry 885 (class 1259 OID 890512)
-- Name: migrated_relations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_relations (
    relation_id numeric(20,0) NOT NULL,
    relation_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_relations OWNER TO postgres;

--
-- TOC entry 886 (class 1259 OID 890515)
-- Name: migrated_schedule_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_schedule_templates (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    template_id numeric(20,0),
    template_guid character varying(45),
    location_name character varying(50),
    minutes_per_row integer,
    location_guid character varying(45),
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    template_order integer,
    time_zone character varying(500),
    color integer
);


ALTER TABLE vaxiom.migrated_schedule_templates OWNER TO postgres;

--
-- TOC entry 887 (class 1259 OID 890521)
-- Name: migrated_transactions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_transactions (
    transaction_id numeric(20,0) NOT NULL,
    transaction_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_transactions OWNER TO postgres;

--
-- TOC entry 888 (class 1259 OID 890524)
-- Name: migrated_treatment_notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_treatment_notes (
    note_id numeric(20,0) NOT NULL,
    note_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_treatment_notes OWNER TO postgres;

--
-- TOC entry 889 (class 1259 OID 890527)
-- Name: migrated_txcards; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_txcards (
    txcard_id numeric(20,0) NOT NULL,
    txcard_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_txcards OWNER TO postgres;

--
-- TOC entry 890 (class 1259 OID 890530)
-- Name: migrated_workschedule; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_workschedule (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    workschedule_id numeric(20,0),
    workschedule_guid character varying(80),
    template_guid character varying(45),
    location_name character varying(50),
    chair_name character varying(50),
    minutes_per_row integer
);


ALTER TABLE vaxiom.migrated_workschedule OWNER TO postgres;

--
-- TOC entry 891 (class 1259 OID 890533)
-- Name: migration_appointment_employee_mapping; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migration_appointment_employee_mapping (
    appointment_source_key character varying(20),
    provider_source_key character varying(20),
    assistant_source_key character varying(20),
    appointment_target_key character varying(20),
    provider_target_key character varying(20),
    assistant_target_key character varying(20)
);


ALTER TABLE vaxiom.migration_appointment_employee_mapping OWNER TO postgres;

--
-- TOC entry 892 (class 1259 OID 890536)
-- Name: migration_fix_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migration_fix_adjustments (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.migration_fix_adjustments OWNER TO postgres;

--
-- TOC entry 894 (class 1259 OID 890541)
-- Name: migration_map; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migration_map (
    id bigint NOT NULL,
    created timestamp without time zone,
    source_key character varying(256),
    source_type character varying(64) NOT NULL,
    source_name character varying(64) NOT NULL,
    target_id numeric(20,0) NOT NULL,
    target_table character varying(64) NOT NULL,
    merged_to_id numeric(20,0),
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.migration_map OWNER TO postgres;

--
-- TOC entry 893 (class 1259 OID 890539)
-- Name: migration_map_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.migration_map_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.migration_map_id_seq OWNER TO postgres;

--
-- TOC entry 9057 (class 0 OID 0)
-- Dependencies: 893
-- Name: migration_map_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.migration_map_id_seq OWNED BY vaxiom.migration_map.id;


--
-- TOC entry 896 (class 1259 OID 890551)
-- Name: misc_fee_charge_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.misc_fee_charge_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    name character varying(255),
    price numeric(30,10) NOT NULL,
    affecting_production_report boolean DEFAULT true NOT NULL
);


ALTER TABLE vaxiom.misc_fee_charge_templates OWNER TO postgres;

--
-- TOC entry 895 (class 1259 OID 890546)
-- Name: misc_fee_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.misc_fee_charges (
    id numeric(20,0) NOT NULL,
    name character varying(255),
    included_in_treatment_fee boolean DEFAULT false NOT NULL,
    affecting_production_report boolean DEFAULT true NOT NULL
);


ALTER TABLE vaxiom.misc_fee_charges OWNER TO postgres;

--
-- TOC entry 897 (class 1259 OID 890555)
-- Name: my_reports; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.my_reports (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255)
);


ALTER TABLE vaxiom.my_reports OWNER TO postgres;

--
-- TOC entry 898 (class 1259 OID 890558)
-- Name: my_reports_columns; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.my_reports_columns (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    column_name character varying(255),
    report_id numeric(20,0) NOT NULL,
    sort_direction character varying(4),
    sort_order integer,
    group_order integer,
    column_order integer
);


ALTER TABLE vaxiom.my_reports_columns OWNER TO postgres;

--
-- TOC entry 899 (class 1259 OID 890561)
-- Name: my_reports_filters; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.my_reports_filters (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(255),
    column_name character varying(255),
    report_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.my_reports_filters OWNER TO postgres;

--
-- TOC entry 900 (class 1259 OID 890567)
-- Name: network_fee_sheets; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.network_fee_sheets (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    valid_from date,
    valid_to date,
    organization_id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.network_fee_sheets OWNER TO postgres;

--
-- TOC entry 901 (class 1259 OID 890570)
-- Name: network_sheet_fee; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.network_sheet_fee (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    insurance_code_id numeric(20,0),
    network_fee_sheet_id numeric(20,0),
    fee numeric(30,10)
);


ALTER TABLE vaxiom.network_sheet_fee OWNER TO postgres;

--
-- TOC entry 903 (class 1259 OID 890576)
-- Name: notebook_notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.notebook_notes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    color character varying(255),
    height integer,
    x integer,
    y integer,
    content text NOT NULL,
    title character varying(255) NOT NULL,
    width integer,
    sys_zindex integer,
    notebook_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.notebook_notes OWNER TO postgres;

--
-- TOC entry 902 (class 1259 OID 890573)
-- Name: notebooks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.notebooks (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    owner numeric(20,0) NOT NULL,
    title character varying(255) NOT NULL
);


ALTER TABLE vaxiom.notebooks OWNER TO postgres;

--
-- TOC entry 904 (class 1259 OID 890582)
-- Name: notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.notes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    author numeric(20,0),
    org_id numeric(20,0),
    ref_id numeric(20,0),
    target_id numeric(20,0) NOT NULL,
    target_type character varying(255) NOT NULL,
    note character varying(6000) NOT NULL,
    description character varying(255),
    alert boolean
);


ALTER TABLE vaxiom.notes OWNER TO postgres;

--
-- TOC entry 907 (class 1259 OID 890596)
-- Name: od_chair_break_hours; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.od_chair_break_hours (
    od_chair_id numeric(20,0) NOT NULL,
    end_min integer NOT NULL,
    start_min integer NOT NULL
);


ALTER TABLE vaxiom.od_chair_break_hours OWNER TO postgres;

--
-- TOC entry 906 (class 1259 OID 890592)
-- Name: odt_chair_allocations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.odt_chair_allocations (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    odt_chair_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    primary_resource boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.odt_chair_allocations OWNER TO postgres;

--
-- TOC entry 905 (class 1259 OID 890588)
-- Name: odt_chairs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.odt_chairs (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    valid_from timestamp without time zone,
    template_id numeric(20,0) NOT NULL,
    day_schedule_id numeric(20,0),
    chair_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.odt_chairs OWNER TO postgres;

--
-- TOC entry 910 (class 1259 OID 890614)
-- Name: office_day_chairs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.office_day_chairs (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    odt_chair_id numeric(20,0),
    office_day_id numeric(20,0) NOT NULL,
    chair_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.office_day_chairs OWNER TO postgres;

--
-- TOC entry 911 (class 1259 OID 890617)
-- Name: office_day_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.office_day_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    location_id numeric(20,0) NOT NULL,
    name character varying(255) NOT NULL,
    color character varying(255) NOT NULL
);


ALTER TABLE vaxiom.office_day_templates OWNER TO postgres;

--
-- TOC entry 909 (class 1259 OID 890607)
-- Name: office_days; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.office_days (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    location_id numeric(20,0) NOT NULL,
    template_id numeric(20,0) NOT NULL,
    o_date date NOT NULL,
    notes character varying(1024)
);


ALTER TABLE vaxiom.office_days OWNER TO postgres;

--
-- TOC entry 908 (class 1259 OID 890599)
-- Name: offices; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.offices (
    city character varying(255) DEFAULT ''::character varying NOT NULL,
    state character varying(255) DEFAULT ''::character varying NOT NULL,
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.offices OWNER TO postgres;

--
-- TOC entry 912 (class 1259 OID 890624)
-- Name: ortho_coverages; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.ortho_coverages (
    id numeric(20,0) NOT NULL,
    max_lifetime_coverage numeric(30,10),
    deductible_amount numeric(30,10),
    reimbursement_percentage numeric(3,2),
    downpayment_percentage numeric(3,2),
    dependent_age_limit integer,
    age_limit_cutoff character varying(255),
    waiting_period integer,
    cob_type character varying(255),
    inprogress boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.ortho_coverages OWNER TO postgres;

--
-- TOC entry 913 (class 1259 OID 890631)
-- Name: ortho_insured; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.ortho_insured (
    id numeric(20,0) NOT NULL,
    used_lifetime_coverage numeric(30,10),
    year_deductible_paid_last integer
);


ALTER TABLE vaxiom.ortho_insured OWNER TO postgres;

--
-- TOC entry 914 (class 1259 OID 890634)
-- Name: other_professional_relationships; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.other_professional_relationships (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    person_id numeric(20,0) NOT NULL,
    external_office_id numeric(20,0) NOT NULL,
    employee_type_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.other_professional_relationships OWNER TO postgres;

--
-- TOC entry 916 (class 1259 OID 890644)
-- Name: patient_images; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_images (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    imageseries_id numeric(20,0) NOT NULL,
    file_key character varying(255) NOT NULL,
    type_key character varying(255),
    slot_key character varying(255),
    file_written timestamp without time zone
);


ALTER TABLE vaxiom.patient_images OWNER TO postgres;

--
-- TOC entry 918 (class 1259 OID 890654)
-- Name: patient_images_layouts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_images_layouts (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_node_id numeric(20,0) NOT NULL,
    xml_content bytea NOT NULL
);


ALTER TABLE vaxiom.patient_images_layouts OWNER TO postgres;

--
-- TOC entry 919 (class 1259 OID 890660)
-- Name: patient_images_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_images_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    xml_content bytea NOT NULL
);


ALTER TABLE vaxiom.patient_images_types OWNER TO postgres;

--
-- TOC entry 917 (class 1259 OID 890650)
-- Name: patient_imageseries; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_imageseries (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    treatment_id numeric(20,0),
    appointment_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    name character varying(255),
    series_date timestamp without time zone,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.patient_imageseries OWNER TO postgres;

--
-- TOC entry 920 (class 1259 OID 890666)
-- Name: patient_insurance_plans; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_insurance_plans (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    master_id numeric(20,0),
    insurance_company_id numeric(20,0),
    insurance_billing_center_id numeric(20,0),
    number character varying(255),
    name character varying(255),
    description character varying(255)
);


ALTER TABLE vaxiom.patient_insurance_plans OWNER TO postgres;

--
-- TOC entry 921 (class 1259 OID 890672)
-- Name: patient_ledger_history; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_ledger_history (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    history_date date NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    tx_location_id numeric(20,0) NOT NULL,
    payment_account_type character varying(50) NOT NULL,
    medicaid boolean NOT NULL,
    payer character varying(255),
    oldest_due_day numeric(20,0) NOT NULL,
    balance numeric(30,10) NOT NULL,
    due numeric(30,10),
    future_due numeric(30,10),
    overdue1 numeric(30,10),
    overdue30 numeric(30,10),
    overdue60 numeric(30,10),
    overdue90 numeric(30,10),
    overdue120 numeric(30,10),
    autopay_source character varying(50),
    payer_phones character varying(1024),
    payer_emails character varying(250),
    label_names character varying(1024),
    label_agencies character varying(1024)
);


ALTER TABLE vaxiom.patient_ledger_history OWNER TO postgres;

--
-- TOC entry 922 (class 1259 OID 890678)
-- Name: patient_ledger_history_overdue_receivables; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_ledger_history_overdue_receivables (
    receivable_id numeric(20,0) NOT NULL,
    patient_ledger_history_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.patient_ledger_history_overdue_receivables OWNER TO postgres;

--
-- TOC entry 923 (class 1259 OID 890681)
-- Name: patient_locations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_locations (
    patient_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.patient_locations OWNER TO postgres;

--
-- TOC entry 924 (class 1259 OID 890684)
-- Name: patient_person_referrals; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_person_referrals (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    person_id numeric(20,0) NOT NULL,
    person_type character varying(255) NOT NULL
);


ALTER TABLE vaxiom.patient_person_referrals OWNER TO postgres;

--
-- TOC entry 925 (class 1259 OID 890687)
-- Name: patient_recently_opened_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_recently_opened_items (
    id numeric(20,0) NOT NULL,
    patient_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.patient_recently_opened_items OWNER TO postgres;

--
-- TOC entry 926 (class 1259 OID 890690)
-- Name: patient_template_referrals; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_template_referrals (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    referral_template_id numeric(20,0) NOT NULL,
    text_value character varying(2000)
);


ALTER TABLE vaxiom.patient_template_referrals OWNER TO postgres;

--
-- TOC entry 915 (class 1259 OID 890637)
-- Name: patients; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patients (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    person_id numeric(20,0) NOT NULL,
    notes character varying(8191),
    legacy_id character varying(255),
    heard_from_patient_id numeric(20,0),
    heard_from_professional_id numeric(20,0),
    heard_from_website character varying(255),
    heard_from_other character varying(255),
    image_file_key character varying(255),
    human_id numeric(20,0),
    primary_location_id numeric(20,0),
    not_generate_statement boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.patients OWNER TO postgres;

--
-- TOC entry 928 (class 1259 OID 890703)
-- Name: payment_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_accounts (
    id numeric(20,0) NOT NULL,
    account_type character varying(30) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    temp boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.payment_accounts OWNER TO postgres;

--
-- TOC entry 929 (class 1259 OID 890707)
-- Name: payment_correction_details; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_correction_details (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    transaction_id numeric(20,0) NOT NULL,
    note character varying(1024),
    correction_type_id numeric(20,0)
);


ALTER TABLE vaxiom.payment_correction_details OWNER TO postgres;

--
-- TOC entry 930 (class 1259 OID 890713)
-- Name: payment_options; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_options (
    id numeric(20,0) NOT NULL,
    name character varying(40) NOT NULL
);


ALTER TABLE vaxiom.payment_options OWNER TO postgres;

--
-- TOC entry 932 (class 1259 OID 890724)
-- Name: payment_plan_rollbacks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_plan_rollbacks (
    id numeric(20,0) NOT NULL,
    payment_plan_id numeric(20,0)
);


ALTER TABLE vaxiom.payment_plan_rollbacks OWNER TO postgres;

--
-- TOC entry 931 (class 1259 OID 890716)
-- Name: payment_plans; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_plans (
    id numeric(20,0) NOT NULL,
    payment_plan_type character varying(255),
    payment_plan_rollback_id numeric(20,0),
    installment_interval character varying(255),
    first_installment_date integer,
    second_installment_date integer,
    installment_amount numeric(30,10),
    first_due_date date NOT NULL,
    installment_locked boolean DEFAULT false NOT NULL,
    plan_length_locked boolean DEFAULT false NOT NULL,
    bank_account_id numeric(20,0),
    token character varying(255),
    signature bytea,
    autopay_source character varying(255),
    last_four_digits character varying(4),
    card_expiration character varying(4),
    invoice_contact_method_id numeric(20,0),
    token_location_id numeric(20,0),
    requested_downpayment_amount numeric(30,10),
    rollback_date timestamp without time zone
);


ALTER TABLE vaxiom.payment_plans OWNER TO postgres;

--
-- TOC entry 933 (class 1259 OID 890727)
-- Name: payment_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(50) NOT NULL,
    type integer NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    parent_company_id numeric(20,0)
);


ALTER TABLE vaxiom.payment_types OWNER TO postgres;

--
-- TOC entry 934 (class 1259 OID 890731)
-- Name: payment_types_locations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_types_locations (
    payment_type_id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.payment_types_locations OWNER TO postgres;

--
-- TOC entry 927 (class 1259 OID 890696)
-- Name: payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payments (
    id numeric(20,0) NOT NULL,
    payment_type character varying(20) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    account_id numeric(20,0),
    amount numeric(30,10),
    signature bytea,
    transaction_id character varying(255),
    receivable_id numeric(20,0),
    payer_name character varying(255),
    check_number character varying(255),
    last_four_digits character varying(4),
    expire character varying(4),
    type_info character varying(255),
    human_id numeric(20,0),
    billing_address_id numeric(20,0),
    credit_card_data character varying(2048),
    applied_payment_id numeric(20,0),
    organization_id numeric(20,0),
    notes character varying(1024),
    patient_id numeric(20,0) NOT NULL,
    sys_created_at numeric(20,0),
    credit_manual boolean,
    payment_type_id numeric(20,0),
    insurance_company_payment_id numeric(20,0),
    migrated boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.payments OWNER TO postgres;

--
-- TOC entry 940 (class 1259 OID 890749)
-- Name: permission_user_role_location; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permission_user_role_location (
    permission_user_role_id numeric(20,0) NOT NULL,
    user_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.permission_user_role_location OWNER TO postgres;

--
-- TOC entry 935 (class 1259 OID 890734)
-- Name: permissions_permission; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_permission (
    id numeric(20,0) NOT NULL,
    name character varying(100),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0),
    permission_group_id numeric(20,0)
);


ALTER TABLE vaxiom.permissions_permission OWNER TO postgres;

--
-- TOC entry 936 (class 1259 OID 890737)
-- Name: permissions_user_role; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_user_role (
    id numeric(20,0) NOT NULL,
    name character varying(45) NOT NULL,
    sys_organizations_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.permissions_user_role OWNER TO postgres;

--
-- TOC entry 937 (class 1259 OID 890740)
-- Name: permissions_user_role_departament; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_user_role_departament (
    permissions_user_role_id numeric(20,0) NOT NULL,
    department_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.permissions_user_role_departament OWNER TO postgres;

--
-- TOC entry 938 (class 1259 OID 890743)
-- Name: permissions_user_role_job_title; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_user_role_job_title (
    permissions_user_role_id numeric(20,0) NOT NULL,
    job_title_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.permissions_user_role_job_title OWNER TO postgres;

--
-- TOC entry 939 (class 1259 OID 890746)
-- Name: permissions_user_role_permission; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_user_role_permission (
    id numeric(20,0) NOT NULL,
    permissions_user_role_id numeric(20,0) NOT NULL,
    permissions_permission_id numeric(20,0) NOT NULL,
    access_type character varying(30) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0)
);


ALTER TABLE vaxiom.permissions_user_role_permission OWNER TO postgres;

--
-- TOC entry 942 (class 1259 OID 890759)
-- Name: person_image_files; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.person_image_files (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    full_size character varying(255) NOT NULL,
    mime_type character varying(255),
    thumbnail character varying(255) NOT NULL,
    person_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.person_image_files OWNER TO postgres;

--
-- TOC entry 943 (class 1259 OID 890765)
-- Name: person_payment_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.person_payment_accounts (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    payer_person_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.person_payment_accounts OWNER TO postgres;

--
-- TOC entry 941 (class 1259 OID 890752)
-- Name: persons; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.persons (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    birth_date date,
    first_name character varying(255) NOT NULL,
    gender character varying(11) NOT NULL,
    greeting character varying(255),
    last_name character varying(255) NOT NULL,
    middle_name character varying(255),
    search_name character varying(255),
    prefix character varying(255),
    suffix character varying(255),
    preferred_means_of_contact character varying(10),
    ssn character(11),
    no_ssn_reason character varying(45),
    id_number character varying(100),
    title character varying(10),
    core_user_id numeric(20,0),
    portal_user_id numeric(20,0),
    school character varying(255),
    marital_status character varying(255),
    organization_id numeric(20,0) NOT NULL,
    global_person_id numeric(20,0),
    patient_portal_user_id numeric(20,0),
    heard_about_us_from character varying(256),
    created_from character varying(45) DEFAULT 'CORE'::character varying
);


ALTER TABLE vaxiom.persons OWNER TO postgres;

--
-- TOC entry 944 (class 1259 OID 890768)
-- Name: previous_enrollment_dependents; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.previous_enrollment_dependents (
    previous_enrollment_id numeric(20,0) NOT NULL,
    dependent_data text
);


ALTER TABLE vaxiom.previous_enrollment_dependents OWNER TO postgres;

--
-- TOC entry 946 (class 1259 OID 890776)
-- Name: previous_enrollment_form; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.previous_enrollment_form (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    employee_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.previous_enrollment_form OWNER TO postgres;

--
-- TOC entry 948 (class 1259 OID 890782)
-- Name: previous_enrollment_form_benefits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.previous_enrollment_form_benefits (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    benefit_type character varying(30),
    benefit_data text
);


ALTER TABLE vaxiom.previous_enrollment_form_benefits OWNER TO postgres;

--
-- TOC entry 947 (class 1259 OID 890780)
-- Name: previous_enrollment_form_benefits_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.previous_enrollment_form_benefits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.previous_enrollment_form_benefits_id_seq OWNER TO postgres;

--
-- TOC entry 9058 (class 0 OID 0)
-- Dependencies: 947
-- Name: previous_enrollment_form_benefits_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.previous_enrollment_form_benefits_id_seq OWNED BY vaxiom.previous_enrollment_form_benefits.id;


--
-- TOC entry 945 (class 1259 OID 890774)
-- Name: previous_enrollment_form_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.previous_enrollment_form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.previous_enrollment_form_id_seq OWNER TO postgres;

--
-- TOC entry 9059 (class 0 OID 0)
-- Dependencies: 945
-- Name: previous_enrollment_form_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.previous_enrollment_form_id_seq OWNED BY vaxiom.previous_enrollment_form.id;


--
-- TOC entry 950 (class 1259 OID 890795)
-- Name: procedure_additional_resource_requirements; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedure_additional_resource_requirements (
    procedure_id numeric(20,0) NOT NULL,
    resource_type_id numeric(20,0) NOT NULL,
    qty integer NOT NULL
);


ALTER TABLE vaxiom.procedure_additional_resource_requirements OWNER TO postgres;

--
-- TOC entry 952 (class 1259 OID 890802)
-- Name: procedure_step_additional_resource_requirements; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedure_step_additional_resource_requirements (
    step_id numeric(20,0) NOT NULL,
    resource_type_id numeric(20,0) NOT NULL,
    qty integer NOT NULL
);


ALTER TABLE vaxiom.procedure_step_additional_resource_requirements OWNER TO postgres;

--
-- TOC entry 953 (class 1259 OID 890805)
-- Name: procedure_step_durations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedure_step_durations (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    step_id numeric(20,0) NOT NULL,
    load_percentage numeric(3,2) DEFAULT 1.00 NOT NULL
);


ALTER TABLE vaxiom.procedure_step_durations OWNER TO postgres;

--
-- TOC entry 951 (class 1259 OID 890798)
-- Name: procedure_steps; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedure_steps (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer NOT NULL,
    name character varying(255) NOT NULL,
    number integer DEFAULT 0 NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    requires_primary_assistant boolean NOT NULL,
    requires_primary_provider boolean NOT NULL
);


ALTER TABLE vaxiom.procedure_steps OWNER TO postgres;

--
-- TOC entry 949 (class 1259 OID 890789)
-- Name: procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedures (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    description character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    insurance_code_id numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    requires_primary_assistant boolean NOT NULL,
    requires_primary_provider boolean NOT NULL,
    requires_diagnosis boolean NOT NULL,
    requires_images boolean NOT NULL,
    dental_marking_type character varying(255)
);


ALTER TABLE vaxiom.procedures OWNER TO postgres;

--
-- TOC entry 954 (class 1259 OID 890809)
-- Name: professional_relationships; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.professional_relationships (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    dtype character varying(31) NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    past boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.professional_relationships OWNER TO postgres;

--
-- TOC entry 955 (class 1259 OID 890813)
-- Name: provider_license; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.provider_license (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    number character varying(45) NOT NULL,
    state character varying(25) NOT NULL,
    provider_specialty_id numeric(20,0) NOT NULL,
    employment_contracts_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.provider_license OWNER TO postgres;

--
-- TOC entry 956 (class 1259 OID 890816)
-- Name: provider_network_insurance_companies; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.provider_network_insurance_companies (
    insurance_company_id numeric(20,0) NOT NULL,
    network_fee_sheet_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.provider_network_insurance_companies OWNER TO postgres;

--
-- TOC entry 957 (class 1259 OID 890819)
-- Name: provider_specialty; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.provider_specialty (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(45) NOT NULL,
    code character varying(45) NOT NULL
);


ALTER TABLE vaxiom.provider_specialty OWNER TO postgres;

--
-- TOC entry 958 (class 1259 OID 890822)
-- Name: question; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.question (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    questionnaire_id numeric(20,0),
    question character varying(50)
);


ALTER TABLE vaxiom.question OWNER TO postgres;

--
-- TOC entry 959 (class 1259 OID 890825)
-- Name: questionnaire; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.questionnaire (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    questionnaire_name character varying(50),
    date timestamp without time zone,
    tx_card_id numeric(20,0),
    patient_id numeric(20,0)
);


ALTER TABLE vaxiom.questionnaire OWNER TO postgres;

--
-- TOC entry 960 (class 1259 OID 890828)
-- Name: questionnaire_answers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.questionnaire_answers (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    questionnaire_id numeric(20,0),
    question_id numeric(20,0),
    answer_id numeric(20,0),
    answer_note character varying(2048),
    quantity character varying(50)
);


ALTER TABLE vaxiom.questionnaire_answers OWNER TO postgres;

--
-- TOC entry 961 (class 1259 OID 890834)
-- Name: reachify_users; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.reachify_users (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    reachify_id character varying(200) NOT NULL,
    email character varying(200) NOT NULL,
    username character varying(200) NOT NULL,
    password character varying(200) NOT NULL
);


ALTER TABLE vaxiom.reachify_users OWNER TO postgres;

--
-- TOC entry 962 (class 1259 OID 890840)
-- Name: receivables; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.receivables (
    id numeric(20,0) NOT NULL,
    rtype character varying(20) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    due_date date,
    realization_date date,
    autopay_date date,
    treatment_id numeric(20,0),
    patient_id numeric(20,0),
    payment_account_id numeric(20,0),
    status character varying(255),
    human_id numeric(20,0),
    insured_id numeric(20,0),
    organization_id numeric(20,0),
    parent_company_id numeric(20,0) NOT NULL,
    init_claim_id numeric(20,0),
    primary_prsn_payer_inv_id numeric(20,0),
    sys_created_at numeric(20,0),
    contract boolean,
    bluefin_organization_id numeric(20,0)
);


ALTER TABLE vaxiom.receivables OWNER TO postgres;

--
-- TOC entry 963 (class 1259 OID 890843)
-- Name: recently_opened_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.recently_opened_items (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    class_name character varying(255) NOT NULL,
    owner numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.recently_opened_items OWNER TO postgres;

--
-- TOC entry 964 (class 1259 OID 890846)
-- Name: referral_list_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.referral_list_values (
    referral_template_id numeric(20,0) NOT NULL,
    pos integer NOT NULL,
    value character varying(2000)
);


ALTER TABLE vaxiom.referral_list_values OWNER TO postgres;

--
-- TOC entry 965 (class 1259 OID 890852)
-- Name: referral_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.referral_templates (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    type character varying(255),
    free_type character varying(255),
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.referral_templates OWNER TO postgres;

--
-- TOC entry 967 (class 1259 OID 890862)
-- Name: refund_details; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.refund_details (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    transaction_id numeric(20,0) NOT NULL,
    refund_type character varying(255),
    refund_method character varying(64),
    money_transaction_id character varying(255),
    check_number character varying(255),
    credit_card_number character varying(255),
    credit_card_expire character varying(4),
    note character varying(1024)
);


ALTER TABLE vaxiom.refund_details OWNER TO postgres;

--
-- TOC entry 966 (class 1259 OID 890859)
-- Name: refunds; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.refunds (
    id numeric(20,0) NOT NULL,
    refund_type character varying(255)
);


ALTER TABLE vaxiom.refunds OWNER TO postgres;

--
-- TOC entry 968 (class 1259 OID 890868)
-- Name: relationships; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.relationships (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    from_person numeric(20,0) NOT NULL,
    to_person numeric(20,0) NOT NULL,
    type character varying(255) NOT NULL,
    role character varying(255) NOT NULL,
    permitted_to_see_info boolean NOT NULL
);


ALTER TABLE vaxiom.relationships OWNER TO postgres;

--
-- TOC entry 969 (class 1259 OID 890874)
-- Name: remote_authentication_tokens; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.remote_authentication_tokens (
    id numeric(20,0) NOT NULL,
    created timestamp without time zone,
    expires timestamp without time zone,
    checksum character varying(64) NOT NULL
);


ALTER TABLE vaxiom.remote_authentication_tokens OWNER TO postgres;

--
-- TOC entry 971 (class 1259 OID 890881)
-- Name: resource_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.resource_types (
    dtype character varying(31) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    abbreviation character varying(100) NOT NULL,
    is_provider boolean DEFAULT false,
    is_assistant boolean DEFAULT false,
    is_filterable boolean DEFAULT false,
    filter_name character varying(255)
);


ALTER TABLE vaxiom.resource_types OWNER TO postgres;

--
-- TOC entry 970 (class 1259 OID 890877)
-- Name: resources; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.resources (
    dtype character varying(31) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    resource_type_id numeric(20,0) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    permissions_user_role_id numeric(20,0)
);


ALTER TABLE vaxiom.resources OWNER TO postgres;

--
-- TOC entry 972 (class 1259 OID 890890)
-- Name: retail_fees; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.retail_fees (
    id numeric(20,0) NOT NULL,
    retail_item_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.retail_fees OWNER TO postgres;

--
-- TOC entry 973 (class 1259 OID 890893)
-- Name: retail_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.retail_items (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    name character varying(255),
    price numeric(30,10) NOT NULL,
    available boolean DEFAULT true NOT NULL
);


ALTER TABLE vaxiom.retail_items OWNER TO postgres;

--
-- TOC entry 974 (class 1259 OID 890897)
-- Name: reversed_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.reversed_payments (
    payment_id numeric(20,0) NOT NULL,
    note character varying(1024),
    date timestamp without time zone
);


ALTER TABLE vaxiom.reversed_payments OWNER TO postgres;

--
-- TOC entry 976 (class 1259 OID 890907)
-- Name: schedule_conflict_permission_bypass; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.schedule_conflict_permission_bypass (
    id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    days integer NOT NULL
);


ALTER TABLE vaxiom.schedule_conflict_permission_bypass OWNER TO postgres;

--
-- TOC entry 977 (class 1259 OID 890910)
-- Name: schedule_notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.schedule_notes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    chair_id numeric(20,0) NOT NULL,
    start_time timestamp without time zone,
    duration integer NOT NULL,
    content character varying(255) NOT NULL,
    alert_this_day boolean DEFAULT false NOT NULL,
    is_blocking_time boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.schedule_notes OWNER TO postgres;

--
-- TOC entry 975 (class 1259 OID 890903)
-- Name: scheduled_task; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.scheduled_task (
    task_type character varying(60) NOT NULL,
    scheduling_cycle character varying(20) NOT NULL,
    parent_company_id numeric(20,0) NOT NULL,
    enabled boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.scheduled_task OWNER TO postgres;

--
-- TOC entry 978 (class 1259 OID 890915)
-- Name: scheduling_preferences; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.scheduling_preferences (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    end_hour integer,
    start_hour integer,
    patient_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.scheduling_preferences OWNER TO postgres;

--
-- TOC entry 979 (class 1259 OID 890918)
-- Name: scheduling_preferences_week_days; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.scheduling_preferences_week_days (
    scheduling_preferences_id numeric(20,0) NOT NULL,
    week_days integer
);


ALTER TABLE vaxiom.scheduling_preferences_week_days OWNER TO postgres;

--
-- TOC entry 980 (class 1259 OID 890921)
-- Name: scheduling_preferred_employees; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.scheduling_preferred_employees (
    preference_id numeric(20,0) NOT NULL,
    employee_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.scheduling_preferred_employees OWNER TO postgres;

--
-- TOC entry 981 (class 1259 OID 890924)
-- Name: schema_version; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.schema_version (
    version_rank integer NOT NULL,
    installed_rank integer NOT NULL,
    version character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE vaxiom.schema_version OWNER TO postgres;

--
-- TOC entry 982 (class 1259 OID 890931)
-- Name: selected_appointment_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.selected_appointment_templates (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    user_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    appointment_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.selected_appointment_templates OWNER TO postgres;

--
-- TOC entry 983 (class 1259 OID 890934)
-- Name: selected_appointment_templates_office_day_calendar_view; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.selected_appointment_templates_office_day_calendar_view (
    user_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    appointment_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.selected_appointment_templates_office_day_calendar_view OWNER TO postgres;

--
-- TOC entry 984 (class 1259 OID 890937)
-- Name: selfcheckin_settings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.selfcheckin_settings (
    location_access_key_id numeric(20,0) NOT NULL,
    max_minutes_late integer NOT NULL,
    max_due_amount integer NOT NULL,
    due_period character varying(20) NOT NULL,
    max_due_amount_insurance integer NOT NULL,
    due_period_insurance character varying(20) NOT NULL
);


ALTER TABLE vaxiom.selfcheckin_settings OWNER TO postgres;

--
-- TOC entry 985 (class 1259 OID 890940)
-- Name: selfcheckin_settings_forbidden_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.selfcheckin_settings_forbidden_types (
    location_access_key_id numeric(20,0) NOT NULL,
    appointment_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.selfcheckin_settings_forbidden_types OWNER TO postgres;

--
-- TOC entry 986 (class 1259 OID 890943)
-- Name: simultaneous_appointment_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.simultaneous_appointment_values (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    denominator integer NOT NULL,
    numerator integer NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    type_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.simultaneous_appointment_values OWNER TO postgres;

--
-- TOC entry 987 (class 1259 OID 890946)
-- Name: sms; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sms (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    logical_message_id character varying(255) NOT NULL,
    processor character varying(255),
    status character varying(255) NOT NULL,
    sender character varying(255) NOT NULL,
    receiver character varying(255) NOT NULL,
    payload character varying(1280),
    status_message character varying(255),
    sending_date timestamp without time zone,
    time_zone_id character varying(255),
    expiry_date timestamp without time zone
);


ALTER TABLE vaxiom.sms OWNER TO postgres;

--
-- TOC entry 988 (class 1259 OID 890952)
-- Name: sms_part; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sms_part (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sms_id numeric(20,0) NOT NULL,
    provider_message_id character varying(255),
    status character varying(255) NOT NULL
);


ALTER TABLE vaxiom.sms_part OWNER TO postgres;

--
-- TOC entry 989 (class 1259 OID 890958)
-- Name: statements; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.statements (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    human_id numeric(20,0) NOT NULL,
    is_pritented boolean DEFAULT false NOT NULL,
    is_emailed boolean DEFAULT false NOT NULL,
    is_scheduled_to_link boolean DEFAULT false NOT NULL,
    is_linked boolean DEFAULT false NOT NULL,
    is_pdf_requested boolean DEFAULT false NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    pdf_file_key character varying(255),
    filter_start_date date,
    filter_end_date date
);


ALTER TABLE vaxiom.statements OWNER TO postgres;

--
-- TOC entry 990 (class 1259 OID 890967)
-- Name: sys_i18n; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_i18n (
    locale character varying(50) NOT NULL,
    key character varying(255) NOT NULL,
    value character varying(1000) NOT NULL
);


ALTER TABLE vaxiom.sys_i18n OWNER TO postgres;

--
-- TOC entry 991 (class 1259 OID 890973)
-- Name: sys_identifiers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_identifiers (
    next_free numeric(20,0) NOT NULL,
    space character varying(100) NOT NULL
);


ALTER TABLE vaxiom.sys_identifiers OWNER TO postgres;

--
-- TOC entry 993 (class 1259 OID 890983)
-- Name: sys_organization_address; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_address (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(45) NOT NULL,
    occupancy character varying(45),
    contact_postal_addresses_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_address OWNER TO postgres;

--
-- TOC entry 994 (class 1259 OID 890986)
-- Name: sys_organization_address_has_sys_organizations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_address_has_sys_organizations (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_organisation_address_id numeric(20,0) NOT NULL,
    sys_organizations_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_address_has_sys_organizations OWNER TO postgres;

--
-- TOC entry 995 (class 1259 OID 890989)
-- Name: sys_organization_attachment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_attachment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL,
    file_id character varying(127),
    file_mime_type character varying(255),
    file_name character varying(127),
    file_size numeric(20,0),
    file_content bytea
);


ALTER TABLE vaxiom.sys_organization_attachment OWNER TO postgres;

--
-- TOC entry 996 (class 1259 OID 890995)
-- Name: sys_organization_contact_method; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_contact_method (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    contact_methods_id numeric(20,0) NOT NULL,
    description character varying(255),
    is_prefered boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.sys_organization_contact_method OWNER TO postgres;

--
-- TOC entry 997 (class 1259 OID 890999)
-- Name: sys_organization_department_data; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_department_data (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_department_data OWNER TO postgres;

--
-- TOC entry 998 (class 1259 OID 891002)
-- Name: sys_organization_division_data; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_division_data (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    medicaid_id character varying(45),
    schedule_appointments boolean,
    accept_payments boolean,
    legal_entity_npi_id numeric(20,0)
);


ALTER TABLE vaxiom.sys_organization_division_data OWNER TO postgres;

--
-- TOC entry 999 (class 1259 OID 891005)
-- Name: sys_organization_division_has_department; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_division_has_department (
    division_tree_node_id numeric(20,0) NOT NULL,
    department_id numeric(20,0) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.sys_organization_division_has_department OWNER TO postgres;

--
-- TOC entry 1000 (class 1259 OID 891008)
-- Name: sys_organization_location_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_location_group (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(45),
    sys_organizations_id numeric(20,0)
);


ALTER TABLE vaxiom.sys_organization_location_group OWNER TO postgres;

--
-- TOC entry 1001 (class 1259 OID 891011)
-- Name: sys_organization_location_group_has_sys_organizations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_location_group_has_sys_organizations (
    sys_organization_location_group_id numeric(20,0) NOT NULL,
    sys_organizations_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_location_group_has_sys_organizations OWNER TO postgres;

--
-- TOC entry 1002 (class 1259 OID 891014)
-- Name: sys_organization_settings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_settings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_settings OWNER TO postgres;

--
-- TOC entry 992 (class 1259 OID 890976)
-- Name: sys_organizations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organizations (
    id numeric(20,0) NOT NULL,
    level character varying(20) NOT NULL,
    name character varying(255) NOT NULL,
    time_zone_id character varying(255),
    parent_id numeric(20,0),
    postal_address_id numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    epoch_date date,
    lat numeric(30,10),
    lng numeric(30,10)
);


ALTER TABLE vaxiom.sys_organizations OWNER TO postgres;

--
-- TOC entry 1003 (class 1259 OID 891020)
-- Name: sys_patient_portal_users; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_patient_portal_users (
    id numeric(20,0) NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    encoded_password character varying(255) NOT NULL,
    login_email character varying(255) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    account_expiration_date date,
    password_expiration_date date,
    token character varying(255),
    token_expiration_date date,
    invitation_sent boolean DEFAULT false NOT NULL,
    completed_first_appointment boolean DEFAULT false,
    completed_profile boolean DEFAULT false
);


ALTER TABLE vaxiom.sys_patient_portal_users OWNER TO postgres;

--
-- TOC entry 1004 (class 1259 OID 891032)
-- Name: sys_portal_users; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_portal_users (
    id numeric(20,0) NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    encoded_password character varying(255) NOT NULL,
    login_email character varying(255) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    account_expiration_date date,
    password_expiration_date date,
    token character varying(255),
    token_expiration_date date,
    invitation_sent boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.sys_portal_users OWNER TO postgres;

--
-- TOC entry 1005 (class 1259 OID 891042)
-- Name: sys_users; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_users (
    id numeric(20,0) NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    encoded_password character varying(255) NOT NULL,
    login_email character varying(255) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    account_expiration_date date,
    password_expiration_date date,
    token character varying(255),
    token_expiration_date date
);


ALTER TABLE vaxiom.sys_users OWNER TO postgres;

--
-- TOC entry 1006 (class 1259 OID 891051)
-- Name: table_statistics; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.table_statistics (
    created timestamp without time zone,
    table_name character varying(64) DEFAULT ''::character varying NOT NULL,
    table_rows numeric,
    avg_row_length numeric,
    data_length numeric,
    index_length numeric
);


ALTER TABLE vaxiom.table_statistics OWNER TO postgres;

--
-- TOC entry 1009 (class 1259 OID 891071)
-- Name: task_basket_subscribers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.task_basket_subscribers (
    task_basket_id numeric(20,0) NOT NULL,
    user_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.task_basket_subscribers OWNER TO postgres;

--
-- TOC entry 1008 (class 1259 OID 891065)
-- Name: task_baskets; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.task_baskets (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    title character varying(255),
    description character varying(1023),
    organization_id numeric(20,0) NOT NULL,
    type character varying(255)
);


ALTER TABLE vaxiom.task_baskets OWNER TO postgres;

--
-- TOC entry 1010 (class 1259 OID 891074)
-- Name: task_contacts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.task_contacts (
    task_id numeric(20,0) NOT NULL,
    contact_method_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.task_contacts OWNER TO postgres;

--
-- TOC entry 1011 (class 1259 OID 891077)
-- Name: task_events; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.task_events (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    task_id numeric(20,0) NOT NULL,
    assignee_id numeric(20,0),
    action character varying(255),
    comment character varying(4095)
);


ALTER TABLE vaxiom.task_events OWNER TO postgres;

--
-- TOC entry 1007 (class 1259 OID 891058)
-- Name: tasks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tasks (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    title character varying(255),
    due_date timestamp without time zone,
    description character varying(1023),
    patient_id numeric(20,0),
    document_template_id numeric(20,0),
    document_tree_node_id numeric(20,0),
    provider_id numeric(20,0),
    organization_id numeric(20,0),
    assignee_id numeric(20,0),
    task_basket_id numeric(20,0) NOT NULL,
    task_status character varying(255),
    recipient_id numeric(20,0),
    created_from character varying(45) DEFAULT 'CORE'::character varying
);


ALTER TABLE vaxiom.tasks OWNER TO postgres;

--
-- TOC entry 1013 (class 1259 OID 891089)
-- Name: temp_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.temp_accounts (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    person_payer_id numeric(20,0),
    insurance_payer_id numeric(20,0),
    tx_plan_id numeric(20,0)
);


ALTER TABLE vaxiom.temp_accounts OWNER TO postgres;

--
-- TOC entry 1012 (class 1259 OID 891083)
-- Name: temporary_images; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.temporary_images (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    file_key character varying(255) NOT NULL,
    organization_node_id numeric(20,0) NOT NULL,
    patient_id numeric(20,0),
    source character varying(255),
    mime_type character varying(255),
    original_name character varying(255),
    patient_image_id numeric(20,0)
);


ALTER TABLE vaxiom.temporary_images OWNER TO postgres;

--
-- TOC entry 1014 (class 1259 OID 891092)
-- Name: third_party_account; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.third_party_account (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    system character varying(20),
    name character varying(255),
    pem_path character varying(255),
    consumer_key character varying(255),
    consumer_secret character varying(255),
    organization_node_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.third_party_account OWNER TO postgres;

--
-- TOC entry 1018 (class 1259 OID 891110)
-- Name: tooth_marking; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tooth_marking (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    completed boolean,
    to_tooth_number integer,
    tooth_number integer NOT NULL,
    type character varying(255),
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tooth_marking OWNER TO postgres;

--
-- TOC entry 1015 (class 1259 OID 891098)
-- Name: toothchart_edit_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.toothchart_edit_log (
    id numeric(20,0) NOT NULL,
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.toothchart_edit_log OWNER TO postgres;

--
-- TOC entry 1016 (class 1259 OID 891101)
-- Name: toothchart_note; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.toothchart_note (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    posx integer,
    posy integer,
    text character varying(255),
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.toothchart_note OWNER TO postgres;

--
-- TOC entry 1017 (class 1259 OID 891104)
-- Name: toothchart_snapshot; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.toothchart_snapshot (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    comments character varying(255),
    data bytea NOT NULL,
    mime_type character varying(255) NOT NULL,
    appointment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.toothchart_snapshot OWNER TO postgres;

--
-- TOC entry 1019 (class 1259 OID 891113)
-- Name: transactions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.transactions (
    dtype character varying(31) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    receivable_id numeric(20,0),
    amount numeric(30,10),
    notes character varying(255),
    affects_due_now boolean DEFAULT true NOT NULL,
    meta_amount numeric(30,10),
    added timestamp without time zone,
    effective_date timestamp without time zone,
    draft boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.transactions OWNER TO postgres;

--
-- TOC entry 1020 (class 1259 OID 891118)
-- Name: transfer_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.transfer_charges (
    id numeric(20,0) NOT NULL,
    charge_transfer_adjustment_id numeric(20,0),
    transfer_charge_type character varying(255)
);


ALTER TABLE vaxiom.transfer_charges OWNER TO postgres;

--
-- TOC entry 1024 (class 1259 OID 891141)
-- Name: tx_card_field_definitions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_card_field_definitions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    alert boolean DEFAULT false NOT NULL,
    hide_for_future boolean DEFAULT false NOT NULL,
    name character varying(255) NOT NULL,
    multi_value boolean DEFAULT false NOT NULL,
    number integer,
    tracked boolean DEFAULT false NOT NULL,
    type_id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    mandatory boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_card_field_definitions OWNER TO postgres;

--
-- TOC entry 1025 (class 1259 OID 891149)
-- Name: tx_card_field_options; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_card_field_options (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    field_type_id numeric(20,0) NOT NULL,
    value character varying(255) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    pos integer DEFAULT 0 NOT NULL
);


ALTER TABLE vaxiom.tx_card_field_options OWNER TO postgres;

--
-- TOC entry 1026 (class 1259 OID 891154)
-- Name: tx_card_field_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_card_field_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    value_type character varying(255) NOT NULL,
    is_broken_brackets boolean DEFAULT false,
    is_hygiene boolean DEFAULT false
);


ALTER TABLE vaxiom.tx_card_field_types OWNER TO postgres;

--
-- TOC entry 1027 (class 1259 OID 891162)
-- Name: tx_card_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_card_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    tx_category_id numeric(20,0) NOT NULL,
    tx_plan_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_card_templates OWNER TO postgres;

--
-- TOC entry 1023 (class 1259 OID 891137)
-- Name: tx_cards; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_cards (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    tx_category_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_cards OWNER TO postgres;

--
-- TOC entry 1028 (class 1259 OID 891165)
-- Name: tx_categories; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_categories (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.tx_categories OWNER TO postgres;

--
-- TOC entry 1029 (class 1259 OID 891168)
-- Name: tx_category_coverages; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_category_coverages (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    tx_category_id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_category_coverages OWNER TO postgres;

--
-- TOC entry 1030 (class 1259 OID 891171)
-- Name: tx_category_insured; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_category_insured (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    tx_category_id numeric(20,0) NOT NULL,
    insured_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_category_insured OWNER TO postgres;

--
-- TOC entry 1031 (class 1259 OID 891174)
-- Name: tx_contracts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_contracts (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    generated_date date,
    signed_date date,
    file character varying(255),
    template_name character varying(255),
    tx_payer_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_contracts OWNER TO postgres;

--
-- TOC entry 1032 (class 1259 OID 891180)
-- Name: tx_fee_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_fee_charges (
    id numeric(20,0) NOT NULL,
    tx_plan_id numeric(20,0) NOT NULL,
    insurance_code_id numeric(20,0)
);


ALTER TABLE vaxiom.tx_fee_charges OWNER TO postgres;

--
-- TOC entry 1033 (class 1259 OID 891183)
-- Name: tx_payers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_payers (
    id numeric(20,0) NOT NULL,
    ptype character varying(20) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    primary_payer boolean DEFAULT false NOT NULL,
    tx_id numeric(20,0),
    person_id numeric(20,0),
    contact_method_id numeric(20,0),
    insured_id numeric(20,0),
    payment_method character varying(255),
    account_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_payers OWNER TO postgres;

--
-- TOC entry 1036 (class 1259 OID 891197)
-- Name: tx_plan_appointment_procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_appointment_procedures (
    appointment_id numeric(20,0) NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    pos integer DEFAULT 0 NOT NULL,
    from_template boolean DEFAULT false NOT NULL,
    added boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_plan_appointment_procedures OWNER TO postgres;

--
-- TOC entry 1035 (class 1259 OID 891192)
-- Name: tx_plan_appointments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_appointments (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer,
    has_custom_procedures boolean DEFAULT false NOT NULL,
    interval_to_next integer,
    note character varying(255) DEFAULT ''::character varying NOT NULL,
    type_id numeric(20,0) NOT NULL,
    next_appointment_id numeric(20,0),
    tx_plan_id numeric(20,0) NOT NULL,
    tx_plan_template_appointment_id numeric(20,0),
    sys_created_at numeric(20,0)
);


ALTER TABLE vaxiom.tx_plan_appointments OWNER TO postgres;

--
-- TOC entry 1037 (class 1259 OID 891204)
-- Name: tx_plan_groups; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_groups (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255),
    pc_id numeric(20,0) NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_plan_groups OWNER TO postgres;

--
-- TOC entry 1039 (class 1259 OID 891215)
-- Name: tx_plan_note_groups; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_note_groups (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    note character varying(8192) DEFAULT ''::character varying NOT NULL,
    group_key character varying(255),
    tx_plan_note_id numeric(20,0)
);


ALTER TABLE vaxiom.tx_plan_note_groups OWNER TO postgres;

--
-- TOC entry 1038 (class 1259 OID 891208)
-- Name: tx_plan_notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_notes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    note character varying(8192) DEFAULT ''::character varying NOT NULL,
    tx_card_id numeric(20,0),
    diagnosis_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_plan_notes OWNER TO postgres;

--
-- TOC entry 1042 (class 1259 OID 891233)
-- Name: tx_plan_template_appointment_procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_template_appointment_procedures (
    appointment_id numeric(20,0) NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    pos integer DEFAULT 0 NOT NULL,
    from_template boolean DEFAULT false NOT NULL,
    added boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_plan_template_appointment_procedures OWNER TO postgres;

--
-- TOC entry 1041 (class 1259 OID 891228)
-- Name: tx_plan_template_appointments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_template_appointments (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer,
    has_custom_procedures boolean DEFAULT false NOT NULL,
    interval_to_next integer,
    note character varying(255) DEFAULT ''::character varying NOT NULL,
    type_id numeric(20,0) NOT NULL,
    next_appointment_id numeric(20,0),
    tx_plan_template_id numeric(20,0) NOT NULL,
    sys_created_at numeric(20,0)
);


ALTER TABLE vaxiom.tx_plan_template_appointments OWNER TO postgres;

--
-- TOC entry 1040 (class 1259 OID 891222)
-- Name: tx_plan_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    insurance_code_id numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    is_pre_treatment boolean DEFAULT false NOT NULL,
    tx_category_id numeric(20,0) NOT NULL,
    fee numeric(30,10),
    length_in_weeks integer,
    deleted boolean DEFAULT false NOT NULL,
    tx_plan_group_id numeric(20,0)
);


ALTER TABLE vaxiom.tx_plan_templates OWNER TO postgres;

--
-- TOC entry 1034 (class 1259 OID 891187)
-- Name: tx_plans; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plans (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    insurance_code_id numeric(20,0),
    tx_id numeric(20,0) NOT NULL,
    tx_plan_template_id numeric(20,0),
    is_candidate boolean DEFAULT true NOT NULL,
    fee numeric(30,10),
    projected_start_date date,
    length_in_weeks integer
);


ALTER TABLE vaxiom.tx_plans OWNER TO postgres;

--
-- TOC entry 1043 (class 1259 OID 891240)
-- Name: tx_statuses; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_statuses (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(45) NOT NULL,
    parent_company_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL,
    hidden boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_statuses OWNER TO postgres;

--
-- TOC entry 1021 (class 1259 OID 891121)
-- Name: txs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.txs (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    notes character varying(8192) DEFAULT ''::character varying NOT NULL,
    status character varying(255) NOT NULL,
    did_not_start boolean DEFAULT false NOT NULL,
    estimated_start_date date,
    estimated_end_date date,
    start_date date,
    end_date date,
    tx_plan_id numeric(20,0),
    tx_card_id numeric(20,0) NOT NULL,
    migrated boolean DEFAULT false NOT NULL,
    sys_created_at numeric(20,0)
);


ALTER TABLE vaxiom.txs OWNER TO postgres;

--
-- TOC entry 1022 (class 1259 OID 891131)
-- Name: txs_state_changes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.txs_state_changes (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    change_time timestamp without time zone,
    new_state character varying(255) NOT NULL,
    old_state character varying(255),
    treatment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.txs_state_changes OWNER TO postgres;

--
-- TOC entry 1044 (class 1259 OID 891244)
-- Name: unapplied_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.unapplied_payments (
    id numeric(20,0) NOT NULL,
    applied_payment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.unapplied_payments OWNER TO postgres;

--
-- TOC entry 1045 (class 1259 OID 891247)
-- Name: update_autopayments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.update_autopayments (
    created timestamp without time zone,
    source_name character varying(50),
    patient_id numeric(20,0),
    receivable_id numeric(20,0),
    payment_plan_id numeric(20,0),
    patient_name character varying(100),
    treatment character varying(50),
    source_token character varying(20),
    last_four_digits character varying(10),
    exp_card_date character varying(20),
    location_id numeric(20,0),
    payconnex_account character varying(20),
    payconnex_token character varying(20),
    result character varying(500)
);


ALTER TABLE vaxiom.update_autopayments OWNER TO postgres;

--
-- TOC entry 1046 (class 1259 OID 891253)
-- Name: week_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.week_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    fri numeric(20,0),
    mon numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0),
    sat numeric(20,0),
    sun numeric(20,0),
    thu numeric(20,0),
    tue numeric(20,0),
    wed numeric(20,0)
);


ALTER TABLE vaxiom.week_templates OWNER TO postgres;

--
-- TOC entry 1048 (class 1259 OID 891260)
-- Name: work_hour_comments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.work_hour_comments (
    id numeric(20,0) NOT NULL,
    work_hours_id numeric(20,0) NOT NULL,
    date timestamp without time zone,
    core_user_id numeric(20,0) NOT NULL,
    comment character varying(512),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.work_hour_comments OWNER TO postgres;

--
-- TOC entry 1047 (class 1259 OID 891256)
-- Name: work_hours; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.work_hours (
    id numeric(20,0) NOT NULL,
    core_user_id numeric(20,0) NOT NULL,
    date timestamp without time zone,
    status character varying(20) NOT NULL,
    state character varying(20) DEFAULT 'NORMAL'::character varying NOT NULL,
    location_id numeric(20,0) NOT NULL,
    modified_by_admin numeric(20,0),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.work_hours OWNER TO postgres;

--
-- TOC entry 1049 (class 1259 OID 891266)
-- Name: work_schedule_days; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.work_schedule_days (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    ws_date date NOT NULL,
    day_schedule_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.work_schedule_days OWNER TO postgres;

--
-- TOC entry 1050 (class 1259 OID 891269)
-- Name: writeoff_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.writeoff_adjustments (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.writeoff_adjustments OWNER TO postgres;

--
-- TOC entry 6232 (class 2604 OID 889346)
-- Name: act_evt_log log_nr_; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_evt_log ALTER COLUMN log_nr_ SET DEFAULT nextval('vaxiom.act_evt_log_log_nr__seq'::regclass);


--
-- TOC entry 6288 (class 2604 OID 889959)
-- Name: enrollment_form id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_id_seq'::regclass);


--
-- TOC entry 6289 (class 2604 OID 889965)
-- Name: enrollment_form_beneficiary id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_beneficiary ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_beneficiary_id_seq'::regclass);


--
-- TOC entry 6290 (class 2604 OID 889974)
-- Name: enrollment_form_benefits id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_benefits ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_benefits_id_seq'::regclass);


--
-- TOC entry 6291 (class 2604 OID 889983)
-- Name: enrollment_form_dependents id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_dependents ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_dependents_id_seq'::regclass);


--
-- TOC entry 6292 (class 2604 OID 889992)
-- Name: enrollment_form_dependents_benefits id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_dependents_benefits ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_dependents_benefits_id_seq'::regclass);


--
-- TOC entry 6293 (class 2604 OID 890001)
-- Name: enrollment_form_personal_data id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_personal_data ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_personal_data_id_seq'::regclass);


--
-- TOC entry 6404 (class 2604 OID 890438)
-- Name: medical_history_patient_relatives_crowding id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_patient_relatives_crowding ALTER COLUMN id SET DEFAULT nextval('vaxiom.medical_history_patient_relatives_crowding_id_seq'::regclass);


--
-- TOC entry 6406 (class 2604 OID 890544)
-- Name: migration_map id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migration_map ALTER COLUMN id SET DEFAULT nextval('vaxiom.migration_map_id_seq'::regclass);


--
-- TOC entry 6426 (class 2604 OID 890779)
-- Name: previous_enrollment_form id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form ALTER COLUMN id SET DEFAULT nextval('vaxiom.previous_enrollment_form_id_seq'::regclass);


--
-- TOC entry 6427 (class 2604 OID 890785)
-- Name: previous_enrollment_form_benefits id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form_benefits ALTER COLUMN id SET DEFAULT nextval('vaxiom.previous_enrollment_form_benefits_id_seq'::regclass);


--
-- TOC entry 9060 (class 0 OID 0)
-- Dependencies: 636
-- Name: act_evt_log_log_nr__seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.act_evt_log_log_nr__seq', 1, true);


--
-- TOC entry 9061 (class 0 OID 0)
-- Dependencies: 763
-- Name: enrollment_form_beneficiary_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_beneficiary_id_seq', 1, true);


--
-- TOC entry 9062 (class 0 OID 0)
-- Dependencies: 765
-- Name: enrollment_form_benefits_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_benefits_id_seq', 1, true);


--
-- TOC entry 9063 (class 0 OID 0)
-- Dependencies: 769
-- Name: enrollment_form_dependents_benefits_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_dependents_benefits_id_seq', 1, true);


--
-- TOC entry 9064 (class 0 OID 0)
-- Dependencies: 767
-- Name: enrollment_form_dependents_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_dependents_id_seq', 1, true);


--
-- TOC entry 9065 (class 0 OID 0)
-- Dependencies: 761
-- Name: enrollment_form_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_id_seq', 1, true);


--
-- TOC entry 9066 (class 0 OID 0)
-- Dependencies: 771
-- Name: enrollment_form_personal_data_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_personal_data_id_seq', 1, true);


--
-- TOC entry 9067 (class 0 OID 0)
-- Dependencies: 861
-- Name: medical_history_patient_relatives_crowding_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.medical_history_patient_relatives_crowding_id_seq', 1, true);


--
-- TOC entry 9068 (class 0 OID 0)
-- Dependencies: 893
-- Name: migration_map_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.migration_map_id_seq', 1, true);


--
-- TOC entry 9069 (class 0 OID 0)
-- Dependencies: 947
-- Name: previous_enrollment_form_benefits_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.previous_enrollment_form_benefits_id_seq', 1, true);


--
-- TOC entry 9070 (class 0 OID 0)
-- Dependencies: 945
-- Name: previous_enrollment_form_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.previous_enrollment_form_id_seq', 1, true);
-- Completed on 2020-03-10 15:12:22 CET

--
-- PostgreSQL database dump complete
--

