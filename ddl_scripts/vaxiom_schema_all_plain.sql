--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

-- Started on 2020-03-10 15:12:20 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 889331)
-- Name: vaxiom; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA vaxiom;


ALTER SCHEMA vaxiom OWNER TO postgres;

--
-- TOC entry 1063 (class 1255 OID 895532)
-- Name: on_update_current_timestamp_act_evt_log(); Type: FUNCTION; Schema: vaxiom; Owner: postgres
--

CREATE FUNCTION vaxiom.on_update_current_timestamp_act_evt_log() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.time_stamp_ = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION vaxiom.on_update_current_timestamp_act_evt_log() OWNER TO postgres;

--
-- TOC entry 1064 (class 1255 OID 895534)
-- Name: on_update_current_timestamp_act_re_deployment(); Type: FUNCTION; Schema: vaxiom; Owner: postgres
--

CREATE FUNCTION vaxiom.on_update_current_timestamp_act_re_deployment() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.deploy_time_ = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION vaxiom.on_update_current_timestamp_act_re_deployment() OWNER TO postgres;

--
-- TOC entry 1065 (class 1255 OID 895536)
-- Name: on_update_current_timestamp_act_ru_task(); Type: FUNCTION; Schema: vaxiom; Owner: postgres
--

CREATE FUNCTION vaxiom.on_update_current_timestamp_act_ru_task() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.create_time_ = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION vaxiom.on_update_current_timestamp_act_ru_task() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 637 (class 1259 OID 889343)
-- Name: act_evt_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_evt_log (
    log_nr_ bigint NOT NULL,
    type_ character varying(64),
    proc_def_id_ character varying(64),
    proc_inst_id_ character varying(64),
    execution_id_ character varying(64),
    task_id_ character varying(64),
    time_stamp_ timestamp with time zone,
    user_id_ character varying(255),
    data_ bytea,
    lock_owner_ character varying(255),
    lock_time_ timestamp with time zone,
    is_processed_ boolean DEFAULT false
);


ALTER TABLE vaxiom.act_evt_log OWNER TO postgres;

--
-- TOC entry 636 (class 1259 OID 889341)
-- Name: act_evt_log_log_nr__seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.act_evt_log_log_nr__seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.act_evt_log_log_nr__seq OWNER TO postgres;

--
-- TOC entry 9014 (class 0 OID 0)
-- Dependencies: 636
-- Name: act_evt_log_log_nr__seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.act_evt_log_log_nr__seq OWNED BY vaxiom.act_evt_log.log_nr_;


--
-- TOC entry 638 (class 1259 OID 889351)
-- Name: act_ge_bytearray; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ge_bytearray (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    name_ character varying(255),
    deployment_id_ character varying(64),
    bytes_ bytea,
    generated_ boolean
);


ALTER TABLE vaxiom.act_ge_bytearray OWNER TO postgres;

--
-- TOC entry 639 (class 1259 OID 889357)
-- Name: act_ge_property; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ge_property (
    name_ character varying(64) NOT NULL,
    value_ character varying(300),
    rev_ integer
);


ALTER TABLE vaxiom.act_ge_property OWNER TO postgres;

--
-- TOC entry 640 (class 1259 OID 889360)
-- Name: act_hi_actinst; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_actinst (
    id_ character varying(64) NOT NULL,
    proc_def_id_ character varying(64) NOT NULL,
    proc_inst_id_ character varying(64) NOT NULL,
    execution_id_ character varying(64) NOT NULL,
    act_id_ character varying(255) NOT NULL,
    task_id_ character varying(64),
    call_proc_inst_id_ character varying(64),
    act_name_ character varying(255),
    act_type_ character varying(255) NOT NULL,
    assignee_ character varying(255),
    start_time_ timestamp without time zone,
    end_time_ timestamp without time zone,
    duration_ numeric(20,0),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_hi_actinst OWNER TO postgres;

--
-- TOC entry 641 (class 1259 OID 889367)
-- Name: act_hi_attachment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_attachment (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    user_id_ character varying(255),
    name_ character varying(255),
    description_ character varying(4000),
    type_ character varying(255),
    task_id_ character varying(64),
    proc_inst_id_ character varying(64),
    url_ character varying(4000),
    content_id_ character varying(64),
    time_ timestamp without time zone
);


ALTER TABLE vaxiom.act_hi_attachment OWNER TO postgres;

--
-- TOC entry 642 (class 1259 OID 889373)
-- Name: act_hi_comment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_comment (
    id_ character varying(64) NOT NULL,
    type_ character varying(255),
    time_ timestamp without time zone,
    user_id_ character varying(255),
    task_id_ character varying(64),
    proc_inst_id_ character varying(64),
    action_ character varying(255),
    message_ character varying(4000),
    full_msg_ bytea
);


ALTER TABLE vaxiom.act_hi_comment OWNER TO postgres;

--
-- TOC entry 643 (class 1259 OID 889379)
-- Name: act_hi_detail; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_detail (
    id_ character varying(64) NOT NULL,
    type_ character varying(255) NOT NULL,
    proc_inst_id_ character varying(64),
    execution_id_ character varying(64),
    task_id_ character varying(64),
    act_inst_id_ character varying(64),
    name_ character varying(255) NOT NULL,
    var_type_ character varying(255),
    rev_ integer,
    time_ timestamp without time zone,
    bytearray_id_ character varying(64),
    double_ double precision,
    long_ numeric(20,0),
    text_ character varying(4000),
    text2_ character varying(4000)
);


ALTER TABLE vaxiom.act_hi_detail OWNER TO postgres;

--
-- TOC entry 644 (class 1259 OID 889385)
-- Name: act_hi_identitylink; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_identitylink (
    id_ character varying(64) NOT NULL,
    group_id_ character varying(255),
    type_ character varying(255),
    user_id_ character varying(255),
    task_id_ character varying(64),
    proc_inst_id_ character varying(64)
);


ALTER TABLE vaxiom.act_hi_identitylink OWNER TO postgres;

--
-- TOC entry 645 (class 1259 OID 889391)
-- Name: act_hi_procinst; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_procinst (
    id_ character varying(64) NOT NULL,
    proc_inst_id_ character varying(64) NOT NULL,
    business_key_ character varying(255),
    proc_def_id_ character varying(64) NOT NULL,
    start_time_ timestamp without time zone,
    end_time_ timestamp without time zone,
    duration_ numeric(20,0),
    start_user_id_ character varying(255),
    start_act_id_ character varying(255),
    end_act_id_ character varying(255),
    super_process_instance_id_ character varying(64),
    delete_reason_ character varying(4000),
    tenant_id_ character varying(255) DEFAULT ''::character varying,
    name_ character varying(255)
);


ALTER TABLE vaxiom.act_hi_procinst OWNER TO postgres;

--
-- TOC entry 646 (class 1259 OID 889398)
-- Name: act_hi_taskinst; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_taskinst (
    id_ character varying(64) NOT NULL,
    proc_def_id_ character varying(64),
    task_def_key_ character varying(255),
    proc_inst_id_ character varying(64),
    execution_id_ character varying(64),
    name_ character varying(255),
    parent_task_id_ character varying(64),
    description_ character varying(4000),
    owner_ character varying(255),
    assignee_ character varying(255),
    start_time_ timestamp without time zone,
    claim_time_ timestamp without time zone,
    end_time_ timestamp without time zone,
    duration_ numeric(20,0),
    delete_reason_ character varying(4000),
    priority_ integer,
    due_date_ timestamp without time zone,
    form_key_ character varying(255),
    category_ character varying(255),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_hi_taskinst OWNER TO postgres;

--
-- TOC entry 647 (class 1259 OID 889405)
-- Name: act_hi_varinst; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_hi_varinst (
    id_ character varying(64) NOT NULL,
    proc_inst_id_ character varying(64),
    execution_id_ character varying(64),
    task_id_ character varying(64),
    name_ character varying(255) NOT NULL,
    var_type_ character varying(100),
    rev_ integer,
    bytearray_id_ character varying(64),
    double_ double precision,
    long_ numeric(20,0),
    text_ character varying(4000),
    text2_ character varying(4000),
    create_time_ timestamp without time zone,
    last_updated_time_ timestamp without time zone
);


ALTER TABLE vaxiom.act_hi_varinst OWNER TO postgres;

--
-- TOC entry 648 (class 1259 OID 889411)
-- Name: act_id_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_id_group (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    name_ character varying(255),
    type_ character varying(255)
);


ALTER TABLE vaxiom.act_id_group OWNER TO postgres;

--
-- TOC entry 649 (class 1259 OID 889417)
-- Name: act_id_info; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_id_info (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    user_id_ character varying(64),
    type_ character varying(64),
    key_ character varying(255),
    value_ character varying(255),
    password_ bytea,
    parent_id_ character varying(255)
);


ALTER TABLE vaxiom.act_id_info OWNER TO postgres;

--
-- TOC entry 650 (class 1259 OID 889423)
-- Name: act_id_membership; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_id_membership (
    user_id_ character varying(64) NOT NULL,
    group_id_ character varying(64) NOT NULL
);


ALTER TABLE vaxiom.act_id_membership OWNER TO postgres;

--
-- TOC entry 651 (class 1259 OID 889426)
-- Name: act_id_user; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_id_user (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    first_ character varying(255),
    last_ character varying(255),
    email_ character varying(255),
    pwd_ character varying(255),
    picture_id_ character varying(64)
);


ALTER TABLE vaxiom.act_id_user OWNER TO postgres;

--
-- TOC entry 652 (class 1259 OID 889432)
-- Name: act_re_deployment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_re_deployment (
    id_ character varying(64) NOT NULL,
    name_ character varying(255),
    category_ character varying(255),
    tenant_id_ character varying(255) DEFAULT ''::character varying,
    deploy_time_ timestamp with time zone
);


ALTER TABLE vaxiom.act_re_deployment OWNER TO postgres;

--
-- TOC entry 653 (class 1259 OID 889439)
-- Name: act_re_model; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_re_model (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    name_ character varying(255),
    key_ character varying(255),
    category_ character varying(255),
    create_time_ timestamp with time zone,
    last_update_time_ timestamp with time zone,
    version_ integer,
    meta_info_ character varying(4000),
    deployment_id_ character varying(64),
    editor_source_value_id_ character varying(64),
    editor_source_extra_value_id_ character varying(64),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_re_model OWNER TO postgres;

--
-- TOC entry 654 (class 1259 OID 889446)
-- Name: act_re_procdef; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_re_procdef (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    category_ character varying(255),
    name_ character varying(255),
    key_ character varying(255) NOT NULL,
    version_ integer NOT NULL,
    deployment_id_ character varying(64),
    resource_name_ character varying(4000),
    dgrm_resource_name_ character varying(4000),
    description_ character varying(4000),
    has_start_form_key_ boolean,
    has_graphical_notation_ boolean,
    suspension_state_ integer,
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_re_procdef OWNER TO postgres;

--
-- TOC entry 655 (class 1259 OID 889453)
-- Name: act_ru_event_subscr; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_event_subscr (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    event_type_ character varying(255) NOT NULL,
    event_name_ character varying(255),
    execution_id_ character varying(64),
    proc_inst_id_ character varying(64),
    activity_id_ character varying(64),
    configuration_ character varying(255),
    created_ timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    proc_def_id_ character varying(64),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_ru_event_subscr OWNER TO postgres;

--
-- TOC entry 656 (class 1259 OID 889461)
-- Name: act_ru_execution; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_execution (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    proc_inst_id_ character varying(64),
    business_key_ character varying(255),
    parent_id_ character varying(64),
    proc_def_id_ character varying(64),
    super_exec_ character varying(64),
    act_id_ character varying(255),
    is_active_ boolean,
    is_concurrent_ boolean,
    is_scope_ boolean,
    is_event_scope_ boolean,
    suspension_state_ integer,
    cached_ent_state_ integer,
    tenant_id_ character varying(255) DEFAULT ''::character varying,
    name_ character varying(255),
    lock_time_ timestamp with time zone
);


ALTER TABLE vaxiom.act_ru_execution OWNER TO postgres;

--
-- TOC entry 657 (class 1259 OID 889468)
-- Name: act_ru_identitylink; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_identitylink (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    group_id_ character varying(255),
    type_ character varying(255),
    user_id_ character varying(255),
    task_id_ character varying(64),
    proc_inst_id_ character varying(64),
    proc_def_id_ character varying(64)
);


ALTER TABLE vaxiom.act_ru_identitylink OWNER TO postgres;

--
-- TOC entry 658 (class 1259 OID 889474)
-- Name: act_ru_job; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_job (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    type_ character varying(255) NOT NULL,
    lock_exp_time_ timestamp with time zone,
    lock_owner_ character varying(255),
    exclusive_ boolean,
    execution_id_ character varying(64),
    process_instance_id_ character varying(64),
    proc_def_id_ character varying(64),
    retries_ integer,
    exception_stack_id_ character varying(64),
    exception_msg_ character varying(4000),
    duedate_ timestamp with time zone,
    repeat_ character varying(255),
    handler_type_ character varying(255),
    handler_cfg_ character varying(4000),
    tenant_id_ character varying(255) DEFAULT ''::character varying
);


ALTER TABLE vaxiom.act_ru_job OWNER TO postgres;

--
-- TOC entry 659 (class 1259 OID 889481)
-- Name: act_ru_task; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_task (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    execution_id_ character varying(64),
    proc_inst_id_ character varying(64),
    proc_def_id_ character varying(64),
    name_ character varying(255),
    parent_task_id_ character varying(64),
    description_ character varying(4000),
    task_def_key_ character varying(255),
    owner_ character varying(255),
    assignee_ character varying(255),
    delegation_ character varying(64),
    priority_ integer,
    create_time_ timestamp with time zone,
    due_date_ timestamp without time zone,
    category_ character varying(255),
    suspension_state_ integer,
    tenant_id_ character varying(255) DEFAULT ''::character varying,
    form_key_ character varying(255)
);


ALTER TABLE vaxiom.act_ru_task OWNER TO postgres;

--
-- TOC entry 660 (class 1259 OID 889488)
-- Name: act_ru_variable; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.act_ru_variable (
    id_ character varying(64) NOT NULL,
    rev_ integer,
    type_ character varying(255) NOT NULL,
    name_ character varying(255) NOT NULL,
    execution_id_ character varying(64),
    proc_inst_id_ character varying(64),
    task_id_ character varying(64),
    bytearray_id_ character varying(64),
    double_ double precision,
    long_ numeric(20,0),
    text_ character varying(4000),
    text2_ character varying(4000)
);


ALTER TABLE vaxiom.act_ru_variable OWNER TO postgres;

--
-- TOC entry 634 (class 1259 OID 889332)
-- Name: activiti_process_settings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.activiti_process_settings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_node_id numeric(20,0) NOT NULL,
    process_id character varying(255)
);


ALTER TABLE vaxiom.activiti_process_settings OWNER TO postgres;

--
-- TOC entry 635 (class 1259 OID 889335)
-- Name: activiti_process_settings_property; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.activiti_process_settings_property (
    activiti_process_settings_id numeric(20,0) NOT NULL,
    property character varying(255) NOT NULL,
    val character varying(255) NOT NULL
);


ALTER TABLE vaxiom.activiti_process_settings_property OWNER TO postgres;

--
-- TOC entry 661 (class 1259 OID 889494)
-- Name: added_report_type; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.added_report_type (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    owner numeric(20,0) NOT NULL,
    report_type character varying(255) NOT NULL
);


ALTER TABLE vaxiom.added_report_type OWNER TO postgres;

--
-- TOC entry 662 (class 1259 OID 889497)
-- Name: answers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.answers (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    question_id numeric(20,0),
    answer character varying(2048)
);


ALTER TABLE vaxiom.answers OWNER TO postgres;

--
-- TOC entry 663 (class 1259 OID 889503)
-- Name: application_properties; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.application_properties (
    organization_id numeric(20,0) NOT NULL,
    message_key character varying(255) NOT NULL,
    message_value character varying(1023)
);


ALTER TABLE vaxiom.application_properties OWNER TO postgres;

--
-- TOC entry 664 (class 1259 OID 889509)
-- Name: applied_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.applied_payments (
    id numeric(20,0) NOT NULL,
    payment_id numeric(20,0),
    applied_type character varying(64)
);


ALTER TABLE vaxiom.applied_payments OWNER TO postgres;

--
-- TOC entry 666 (class 1259 OID 889521)
-- Name: appointment_audit_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_audit_log (
    action character varying(255) NOT NULL,
    id numeric(20,0) NOT NULL,
    appointment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.appointment_audit_log OWNER TO postgres;

--
-- TOC entry 668 (class 1259 OID 889531)
-- Name: appointment_booking_resources; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_booking_resources (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    booking_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    appt_start_offset integer NOT NULL,
    duration integer NOT NULL,
    load_percentage numeric(3,2) DEFAULT 1.00 NOT NULL,
    legacy_doctor character varying(255),
    legacy_assistant character varying(255)
);


ALTER TABLE vaxiom.appointment_booking_resources OWNER TO postgres;

--
-- TOC entry 669 (class 1259 OID 889538)
-- Name: appointment_booking_state_changes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_booking_state_changes (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    change_time timestamp without time zone,
    new_state character varying(255) NOT NULL,
    old_state character varying(255),
    booking_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.appointment_booking_state_changes OWNER TO postgres;

--
-- TOC entry 667 (class 1259 OID 889524)
-- Name: appointment_bookings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_bookings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    confirmation_status character varying(255) NOT NULL,
    start_time timestamp without time zone,
    local_start_date date NOT NULL,
    local_start_time time without time zone NOT NULL,
    duration integer NOT NULL,
    state character varying(255) NOT NULL,
    appointment_id numeric(20,0) NOT NULL,
    chair_id numeric(20,0) NOT NULL,
    provider_id numeric(20,0),
    assistant_id numeric(20,0),
    legacy_doctor character varying(255),
    legacy_assistant character varying(255),
    reminder_sent boolean DEFAULT false NOT NULL,
    seated_chair_id numeric(20,0),
    sys_created_at numeric(20,0),
    check_in_time timestamp without time zone
);


ALTER TABLE vaxiom.appointment_bookings OWNER TO postgres;

--
-- TOC entry 670 (class 1259 OID 889544)
-- Name: appointment_field_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_field_values (
    appointment_id numeric(20,0) NOT NULL,
    field_option_id numeric(20,0) NOT NULL,
    field_definition_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.appointment_field_values OWNER TO postgres;

--
-- TOC entry 671 (class 1259 OID 889547)
-- Name: appointment_procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_procedures (
    appointment_id numeric(20,0) NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    pos integer DEFAULT 0 NOT NULL,
    from_template boolean DEFAULT false NOT NULL,
    added boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.appointment_procedures OWNER TO postgres;

--
-- TOC entry 673 (class 1259 OID 889561)
-- Name: appointment_template_procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_template_procedures (
    appointment_template_id numeric(20,0) NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    pos integer NOT NULL
);


ALTER TABLE vaxiom.appointment_template_procedures OWNER TO postgres;

--
-- TOC entry 672 (class 1259 OID 889554)
-- Name: appointment_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    appointment_type_id numeric(20,0) NOT NULL,
    primary_provider_type_id numeric(20,0),
    primary_assistant_type_id numeric(20,0),
    full_name character varying(255),
    color_id character varying(255),
    medical_plan character varying(45),
    send_medical_form boolean DEFAULT false
);


ALTER TABLE vaxiom.appointment_templates OWNER TO postgres;

--
-- TOC entry 676 (class 1259 OID 889580)
-- Name: appointment_type_daily_restrictions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_type_daily_restrictions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type_id numeric(20,0) NOT NULL,
    local_date date NOT NULL,
    location_id numeric(20,0) NOT NULL,
    max_appts integer DEFAULT 1 NOT NULL
);


ALTER TABLE vaxiom.appointment_type_daily_restrictions OWNER TO postgres;

--
-- TOC entry 674 (class 1259 OID 889564)
-- Name: appointment_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    color_id character varying(255),
    full_name character varying(255) DEFAULT ''::character varying NOT NULL,
    name character varying(255) NOT NULL,
    not_tx boolean DEFAULT false NOT NULL,
    patient_exam boolean DEFAULT false NOT NULL,
    parent_id numeric(20,0),
    migrated boolean NOT NULL,
    emergency boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.appointment_types OWNER TO postgres;

--
-- TOC entry 675 (class 1259 OID 889574)
-- Name: appointment_types_property; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointment_types_property (
    appointment_type_id numeric(20,0) NOT NULL,
    property character varying(255) NOT NULL,
    val character varying(255) NOT NULL
);


ALTER TABLE vaxiom.appointment_types_property OWNER TO postgres;

--
-- TOC entry 665 (class 1259 OID 889512)
-- Name: appointments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appointments (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer,
    has_custom_procedures boolean DEFAULT false NOT NULL,
    interval_to_next integer,
    note character varying(4096) DEFAULT ''::character varying NOT NULL,
    type_id numeric(20,0) NOT NULL,
    next_appointment_id numeric(20,0),
    patient_id numeric(20,0),
    tx_id numeric(20,0),
    tx_plan_appointment_id numeric(20,0),
    unplanned boolean DEFAULT false NOT NULL,
    legacy_procedure_codes character varying(1024),
    sys_created_at numeric(20,0)
);


ALTER TABLE vaxiom.appointments OWNER TO postgres;

--
-- TOC entry 677 (class 1259 OID 889584)
-- Name: appt_type_template_restrictions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.appt_type_template_restrictions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    template_id numeric(20,0) NOT NULL,
    type_id numeric(20,0) NOT NULL,
    max_appts integer DEFAULT 1 NOT NULL
);


ALTER TABLE vaxiom.appt_type_template_restrictions OWNER TO postgres;

--
-- TOC entry 678 (class 1259 OID 889588)
-- Name: audit_events; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.audit_events (
    id numeric(20,0) NOT NULL,
    created timestamp without time zone,
    ip_address character varying(50),
    application character varying(255),
    session character varying(255),
    user_id numeric(20,0),
    subject character varying(255),
    subject_id numeric(20,0),
    verb character varying(255),
    object character varying(255),
    object_id numeric(20,0),
    parent_object character varying(255),
    parent_object_id numeric(20,0),
    description character varying(255)
);


ALTER TABLE vaxiom.audit_events OWNER TO postgres;

--
-- TOC entry 679 (class 1259 OID 889594)
-- Name: audit_logs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.audit_logs (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    class_type character varying(255) NOT NULL,
    user_id numeric(20,0)
);


ALTER TABLE vaxiom.audit_logs OWNER TO postgres;

--
-- TOC entry 680 (class 1259 OID 889597)
-- Name: autopay_invoice_changes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.autopay_invoice_changes (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    change_time timestamp without time zone,
    changed_by numeric(20,0),
    changed_field_name character varying(255),
    new_value character varying(255),
    old_value character varying(255),
    receivable_id numeric(20,0) NOT NULL,
    description character varying(1024)
);


ALTER TABLE vaxiom.autopay_invoice_changes OWNER TO postgres;

--
-- TOC entry 681 (class 1259 OID 889603)
-- Name: bank_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.bank_accounts (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    bank_account_type character varying(255),
    account_number character varying(255),
    routing_number character varying(255)
);


ALTER TABLE vaxiom.bank_accounts OWNER TO postgres;

--
-- TOC entry 682 (class 1259 OID 889609)
-- Name: benefit_saving_account_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.benefit_saving_account_values (
    id numeric(20,0) NOT NULL,
    pre_tax_rate numeric(30,10) NOT NULL,
    post_tax_rate numeric(30,10),
    employer_rate numeric(30,10),
    total_rate numeric(30,10),
    employee_benefit_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    insurance_plan_saving_account_id numeric(20,0) NOT NULL,
    employee_annual_rate numeric(30,10),
    employer_annual_rate numeric(30,10),
    waived boolean,
    qle_parent numeric(20,0),
    employee_beneficiary_group_id numeric(20,0)
);


ALTER TABLE vaxiom.benefit_saving_account_values OWNER TO postgres;

--
-- TOC entry 683 (class 1259 OID 889612)
-- Name: bluefin_credentials; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.bluefin_credentials (
    organization_id numeric(20,0) NOT NULL,
    account_id character varying(255) NOT NULL,
    access_key character varying(255) NOT NULL
);


ALTER TABLE vaxiom.bluefin_credentials OWNER TO postgres;

--
-- TOC entry 684 (class 1259 OID 889618)
-- Name: broker; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.broker (
    id numeric(20,0),
    login_email character varying(50),
    name character varying(50)
);


ALTER TABLE vaxiom.broker OWNER TO postgres;

--
-- TOC entry 685 (class 1259 OID 889621)
-- Name: cancelled_receivables; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.cancelled_receivables (
    receivable_id numeric(20,0) NOT NULL,
    date timestamp without time zone
);


ALTER TABLE vaxiom.cancelled_receivables OWNER TO postgres;

--
-- TOC entry 686 (class 1259 OID 889624)
-- Name: cephx_requests; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.cephx_requests (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    transaction_id character varying(255),
    patient_image_id numeric(20,0) NOT NULL,
    status character varying(45) DEFAULT 'SENT'::character varying NOT NULL
);


ALTER TABLE vaxiom.cephx_requests OWNER TO postgres;

--
-- TOC entry 688 (class 1259 OID 889635)
-- Name: chair_allocations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.chair_allocations (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    ca_date date NOT NULL,
    chair_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    primary_resource boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.chair_allocations OWNER TO postgres;

--
-- TOC entry 687 (class 1259 OID 889628)
-- Name: chairs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.chairs (
    code character varying(255) NOT NULL,
    full_name character varying(255) NOT NULL,
    id numeric(20,0) NOT NULL,
    pos integer DEFAULT 1000 NOT NULL
);


ALTER TABLE vaxiom.chairs OWNER TO postgres;

--
-- TOC entry 689 (class 1259 OID 889639)
-- Name: charge_transfer_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.charge_transfer_adjustments (
    id numeric(20,0) NOT NULL,
    transfer_charge_type character varying(255)
);


ALTER TABLE vaxiom.charge_transfer_adjustments OWNER TO postgres;

--
-- TOC entry 691 (class 1259 OID 889645)
-- Name: check_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.check_types (
    id numeric(20,0) NOT NULL,
    name character varying(100) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created date,
    sys_last_modified date,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.check_types OWNER TO postgres;

--
-- TOC entry 690 (class 1259 OID 889642)
-- Name: checks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.checks (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0),
    type_id numeric(20,0),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created date,
    sys_last_modified date,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.checks OWNER TO postgres;

--
-- TOC entry 692 (class 1259 OID 889648)
-- Name: claim_events; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.claim_events (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    receivable_id numeric(20,0),
    event character varying(255),
    amount numeric(30,10),
    description character varying(1024)
);


ALTER TABLE vaxiom.claim_events OWNER TO postgres;

--
-- TOC entry 693 (class 1259 OID 889654)
-- Name: claim_requests; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.claim_requests (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    receivable_id numeric(20,0) NOT NULL,
    location_id numeric(20,0),
    status character varying(255),
    report text,
    claim_request_data text NOT NULL,
    patient_id numeric(20,0),
    treatment_id numeric(20,0),
    money_requested numeric(30,10)
);


ALTER TABLE vaxiom.claim_requests OWNER TO postgres;

--
-- TOC entry 696 (class 1259 OID 889669)
-- Name: collection_label_agencies; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collection_label_agencies (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    phone_numer character varying(25),
    parent_company_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.collection_label_agencies OWNER TO postgres;

--
-- TOC entry 697 (class 1259 OID 889672)
-- Name: collection_label_history; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collection_label_history (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    created_by numeric(20,0) NOT NULL,
    created timestamp without time zone,
    patient_id numeric(20,0) NOT NULL,
    payment_account_id numeric(20,0) NOT NULL,
    previous_collection_label_template_id numeric(20,0)
);


ALTER TABLE vaxiom.collection_label_history OWNER TO postgres;

--
-- TOC entry 698 (class 1259 OID 889675)
-- Name: collection_label_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collection_label_templates (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    parent_company_id numeric(20,0) NOT NULL,
    agency_id numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    payer_type character varying(45) DEFAULT 'PERSON'::character varying,
    is_default boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.collection_label_templates OWNER TO postgres;

--
-- TOC entry 695 (class 1259 OID 889666)
-- Name: collection_labels; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collection_labels (
    patient_id numeric(20,0) NOT NULL,
    payment_account_id numeric(20,0) NOT NULL,
    last_modified_by numeric(20,0) NOT NULL,
    last_modified timestamp without time zone,
    collection_label_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.collection_labels OWNER TO postgres;

--
-- TOC entry 694 (class 1259 OID 889660)
-- Name: collections_cache; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.collections_cache (
    location_id numeric(20,0) NOT NULL,
    date date NOT NULL,
    results text
);


ALTER TABLE vaxiom.collections_cache OWNER TO postgres;

--
-- TOC entry 699 (class 1259 OID 889681)
-- Name: communication_preferences; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.communication_preferences (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    contact_method_association_id numeric(20,0) NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE vaxiom.communication_preferences OWNER TO postgres;

--
-- TOC entry 700 (class 1259 OID 889684)
-- Name: contact_emails; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_emails (
    id numeric(20,0) NOT NULL,
    address character varying(255) NOT NULL
);


ALTER TABLE vaxiom.contact_emails OWNER TO postgres;

--
-- TOC entry 702 (class 1259 OID 889691)
-- Name: contact_method_associations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_method_associations (
    id numeric(20,0) NOT NULL,
    person_id numeric(20,0) NOT NULL,
    contact_method_id numeric(20,0) NOT NULL,
    description character varying(255),
    preference character varying(31),
    error_location_id numeric(20,0),
    error_date date,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    contact_order integer
);


ALTER TABLE vaxiom.contact_method_associations OWNER TO postgres;

--
-- TOC entry 701 (class 1259 OID 889687)
-- Name: contact_methods; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_methods (
    dtype character varying(31) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0) DEFAULT '0'::numeric NOT NULL
);


ALTER TABLE vaxiom.contact_methods OWNER TO postgres;

--
-- TOC entry 703 (class 1259 OID 889694)
-- Name: contact_phones; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_phones (
    id numeric(20,0) NOT NULL,
    number character varying(255) NOT NULL,
    type character varying(31) NOT NULL
);


ALTER TABLE vaxiom.contact_phones OWNER TO postgres;

--
-- TOC entry 704 (class 1259 OID 889697)
-- Name: contact_postal_addresses; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_postal_addresses (
    id numeric(20,0) NOT NULL,
    address_line1 character varying(255),
    address_line2 character varying(255),
    city character varying(255),
    zip character varying(255),
    state character varying(255)
);


ALTER TABLE vaxiom.contact_postal_addresses OWNER TO postgres;

--
-- TOC entry 705 (class 1259 OID 889703)
-- Name: contact_recently_opened_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_recently_opened_items (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.contact_recently_opened_items OWNER TO postgres;

--
-- TOC entry 706 (class 1259 OID 889706)
-- Name: contact_website; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.contact_website (
    id numeric(20,0) NOT NULL,
    website_address character varying(255) NOT NULL
);


ALTER TABLE vaxiom.contact_website OWNER TO postgres;

--
-- TOC entry 707 (class 1259 OID 889709)
-- Name: correction_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.correction_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(50) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    parent_company_id numeric(20,0),
    positive boolean DEFAULT true,
    negative boolean DEFAULT true
);


ALTER TABLE vaxiom.correction_types OWNER TO postgres;

--
-- TOC entry 708 (class 1259 OID 889715)
-- Name: correction_types_locations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.correction_types_locations (
    correction_type_id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.correction_types_locations OWNER TO postgres;

--
-- TOC entry 709 (class 1259 OID 889718)
-- Name: correction_types_payment_options; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.correction_types_payment_options (
    correction_types_id numeric(20,0) NOT NULL,
    payment_options_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.correction_types_payment_options OWNER TO postgres;

--
-- TOC entry 710 (class 1259 OID 889721)
-- Name: county; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.county (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    incits_code character varying(15) NOT NULL,
    name character varying(255) NOT NULL,
    state character varying(25) NOT NULL
);


ALTER TABLE vaxiom.county OWNER TO postgres;

--
-- TOC entry 711 (class 1259 OID 889724)
-- Name: daily_transactions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.daily_transactions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    date date,
    location_id numeric(20,0) NOT NULL,
    report_data text NOT NULL,
    report_version integer DEFAULT 0 NOT NULL,
    location_type character varying(20) NOT NULL
);


ALTER TABLE vaxiom.daily_transactions OWNER TO postgres;

--
-- TOC entry 712 (class 1259 OID 889731)
-- Name: data_lock; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.data_lock (
    id numeric(20,0) NOT NULL,
    sys_created timestamp without time zone,
    session_id character varying(255) NOT NULL,
    user_id numeric(20,0) NOT NULL,
    data_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.data_lock OWNER TO postgres;

--
-- TOC entry 714 (class 1259 OID 889741)
-- Name: day_schedule_appt_slots; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.day_schedule_appt_slots (
    day_schedule_id numeric(20,0) NOT NULL,
    appt_type_id numeric(20,0) NOT NULL,
    start_min integer NOT NULL
);


ALTER TABLE vaxiom.day_schedule_appt_slots OWNER TO postgres;

--
-- TOC entry 715 (class 1259 OID 889744)
-- Name: day_schedule_break_hours; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.day_schedule_break_hours (
    day_schedule_id numeric(20,0) NOT NULL,
    end_min integer NOT NULL,
    start_min integer NOT NULL
);


ALTER TABLE vaxiom.day_schedule_break_hours OWNER TO postgres;

--
-- TOC entry 716 (class 1259 OID 889747)
-- Name: day_schedule_office_hours; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.day_schedule_office_hours (
    day_schedule_id numeric(20,0) NOT NULL,
    end_min integer NOT NULL,
    start_min integer NOT NULL
);


ALTER TABLE vaxiom.day_schedule_office_hours OWNER TO postgres;

--
-- TOC entry 713 (class 1259 OID 889734)
-- Name: day_schedules; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.day_schedules (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    color character varying(255) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    name character varying(255) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0),
    valid_from timestamp without time zone,
    day_schedule_nr numeric(20,0) NOT NULL,
    overrides_day_schedule numeric(20,0)
);


ALTER TABLE vaxiom.day_schedules OWNER TO postgres;

--
-- TOC entry 717 (class 1259 OID 889750)
-- Name: default_appointment_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.default_appointment_templates (
    id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    template_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.default_appointment_templates OWNER TO postgres;

--
-- TOC entry 718 (class 1259 OID 889753)
-- Name: dentalchart_additional_markings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_additional_markings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    marking_id numeric(20,0),
    name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_additional_markings OWNER TO postgres;

--
-- TOC entry 719 (class 1259 OID 889756)
-- Name: dentalchart_edit_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_edit_log (
    id numeric(20,0) NOT NULL,
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_edit_log OWNER TO postgres;

--
-- TOC entry 720 (class 1259 OID 889759)
-- Name: dentalchart_marking; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_marking (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    added timestamp without time zone,
    completed timestamp without time zone,
    canceled timestamp without time zone,
    to_tooth_number integer,
    tooth_number integer NOT NULL,
    type character varying(255),
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_marking OWNER TO postgres;

--
-- TOC entry 721 (class 1259 OID 889762)
-- Name: dentalchart_materials; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_materials (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    marking_type character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_materials OWNER TO postgres;

--
-- TOC entry 722 (class 1259 OID 889768)
-- Name: dentalchart_snapshot; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dentalchart_snapshot (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    comments character varying(255),
    data bytea NOT NULL,
    mime_type character varying(255) NOT NULL,
    appointment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dentalchart_snapshot OWNER TO postgres;

--
-- TOC entry 723 (class 1259 OID 889774)
-- Name: diagnosis; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.diagnosis (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    appointment_id numeric(20,0) NOT NULL,
    diagnosis_template_id numeric(20,0) NOT NULL,
    techincal_letter_generated timestamp without time zone,
    layperson_letter_generated timestamp without time zone,
    techincal_letter_id character varying(255),
    layperson_letter_id character varying(255)
);


ALTER TABLE vaxiom.diagnosis OWNER TO postgres;

--
-- TOC entry 724 (class 1259 OID 889780)
-- Name: diagnosis_field_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.diagnosis_field_values (
    diagnosis_id numeric(20,0) NOT NULL,
    field_key character varying(255) NOT NULL,
    field_value text
);


ALTER TABLE vaxiom.diagnosis_field_values OWNER TO postgres;

--
-- TOC entry 725 (class 1259 OID 889786)
-- Name: diagnosis_letters; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.diagnosis_letters (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    diagnosis_id numeric(20,0) NOT NULL,
    document_id numeric(20,0) NOT NULL,
    document_template_id numeric(20,0) NOT NULL,
    recipient_id numeric(20,0) NOT NULL,
    address_id numeric(20,0)
);


ALTER TABLE vaxiom.diagnosis_letters OWNER TO postgres;

--
-- TOC entry 726 (class 1259 OID 889789)
-- Name: diagnosis_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.diagnosis_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    version numeric(20,0),
    form_structure_xml text,
    tx_category_id numeric(20,0) NOT NULL,
    appointment_type_id numeric(20,0),
    parent_company_id numeric(20,0)
);


ALTER TABLE vaxiom.diagnosis_templates OWNER TO postgres;

--
-- TOC entry 728 (class 1259 OID 889802)
-- Name: discount_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.discount_adjustments (
    id numeric(20,0) NOT NULL,
    discount_id numeric(20,0),
    discount_adjustment_type character varying(255)
);


ALTER TABLE vaxiom.discount_adjustments OWNER TO postgres;

--
-- TOC entry 729 (class 1259 OID 889805)
-- Name: discount_reverse_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.discount_reverse_adjustments (
    id numeric(20,0) NOT NULL,
    discount_adjustment_id numeric(20,0)
);


ALTER TABLE vaxiom.discount_reverse_adjustments OWNER TO postgres;

--
-- TOC entry 727 (class 1259 OID 889795)
-- Name: discounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.discounts (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255),
    organization_id numeric(20,0) NOT NULL,
    default_amount numeric(30,10),
    default_percentage numeric(3,2),
    fixed boolean,
    valid_from date,
    valid_to date,
    tx_fee boolean DEFAULT false NOT NULL,
    type character varying(20) DEFAULT 'PVT_PAY'::character varying NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    affects_production boolean DEFAULT true NOT NULL
);


ALTER TABLE vaxiom.discounts OWNER TO postgres;

--
-- TOC entry 730 (class 1259 OID 889808)
-- Name: dock_item_descriptors; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_descriptors (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    height integer NOT NULL,
    owner numeric(20,0) NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL,
    width integer NOT NULL
);


ALTER TABLE vaxiom.dock_item_descriptors OWNER TO postgres;

--
-- TOC entry 731 (class 1259 OID 889811)
-- Name: dock_item_features_events; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_events (
    calendar_id character varying(255) NOT NULL,
    event_id character varying(255),
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_events OWNER TO postgres;

--
-- TOC entry 732 (class 1259 OID 889817)
-- Name: dock_item_features_images; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_images (
    image_id character varying(255) NOT NULL,
    edit_mode boolean,
    id numeric(20,0) NOT NULL,
    patient_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_images OWNER TO postgres;

--
-- TOC entry 733 (class 1259 OID 889820)
-- Name: dock_item_features_messages; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_messages (
    thread_id numeric(20,0),
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_messages OWNER TO postgres;

--
-- TOC entry 734 (class 1259 OID 889823)
-- Name: dock_item_features_note; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_note (
    id numeric(20,0) NOT NULL,
    note_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_note OWNER TO postgres;

--
-- TOC entry 735 (class 1259 OID 889826)
-- Name: dock_item_features_tasks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.dock_item_features_tasks (
    list_id character varying(255),
    task_id character varying(255),
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.dock_item_features_tasks OWNER TO postgres;

--
-- TOC entry 736 (class 1259 OID 889832)
-- Name: document_recently_opened_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.document_recently_opened_items (
    drive_id character varying(255) NOT NULL,
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.document_recently_opened_items OWNER TO postgres;

--
-- TOC entry 737 (class 1259 OID 889835)
-- Name: document_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.document_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    description character varying(255),
    file_key character varying(255) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    mime_type character varying(255) NOT NULL,
    is_contract boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.document_templates OWNER TO postgres;

--
-- TOC entry 738 (class 1259 OID 889842)
-- Name: document_tree_nodes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.document_tree_nodes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    parent_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL,
    file_uid character varying(255),
    name character varying(255) NOT NULL,
    size numeric(20,0),
    icon character varying(255),
    thumbs character varying(255),
    meta text,
    deleted boolean DEFAULT false NOT NULL,
    created_from character varying(45) DEFAULT 'CORE'::character varying
);


ALTER TABLE vaxiom.document_tree_nodes OWNER TO postgres;

--
-- TOC entry 739 (class 1259 OID 889850)
-- Name: eeo; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.eeo (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    jobtitle character varying(60)
);


ALTER TABLE vaxiom.eeo OWNER TO postgres;

--
-- TOC entry 740 (class 1259 OID 889853)
-- Name: employee_beneficiary; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_beneficiary (
    id numeric(20,0) NOT NULL,
    share_percent integer,
    type character varying(45),
    employee_enrollment_person_id numeric(20,0) NOT NULL,
    employee_beneficiary_group_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0)
);


ALTER TABLE vaxiom.employee_beneficiary OWNER TO postgres;

--
-- TOC entry 741 (class 1259 OID 889856)
-- Name: employee_beneficiary_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_beneficiary_group (
    id numeric(20,0) NOT NULL,
    employee_enrollment_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0)
);


ALTER TABLE vaxiom.employee_beneficiary_group OWNER TO postgres;

--
-- TOC entry 742 (class 1259 OID 889859)
-- Name: employee_benefit; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_benefit (
    id numeric(20,0) NOT NULL,
    benefit_type character varying(45),
    pre_tax_rate numeric(30,10),
    post_tax_rate numeric(30,10),
    employer_rate numeric(30,10),
    total_rate numeric(30,10),
    covarage_level character varying(45),
    other_fields character varying(1200),
    employee_enrollment_id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0),
    is_waived boolean,
    benefit_enrollment_status character varying(45) DEFAULT 'NONE'::character varying,
    is_disabled boolean DEFAULT false,
    waive_reason character varying(45),
    payroll_cycle character varying(20),
    biling_start_date date,
    biling_end_date date,
    other_carrier_name character varying(100),
    carrier_person_name character varying(100),
    other_coverage_level character varying(100)
);


ALTER TABLE vaxiom.employee_benefit OWNER TO postgres;

--
-- TOC entry 743 (class 1259 OID 889867)
-- Name: employee_dependent; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_dependent (
    id numeric(20,0) NOT NULL,
    gender character varying(45),
    ssn character(11),
    is_disabled_child boolean,
    is_address_the_same boolean,
    is_fulltime_student boolean,
    street character varying(120),
    city character varying(45),
    zip_code character varying(45),
    state character varying(45),
    country character varying(45),
    employee_enrollment_person_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    employee_enrollment_id numeric(20,0) NOT NULL,
    qle_parent numeric(20,0),
    primary_phone character varying(20),
    primary_phone_type character varying(20),
    secondary_phone character varying(20),
    secondary_phone_type character varying(20),
    primary_email character varying(60),
    primary_email_type character varying(20),
    secondary_email character varying(60),
    secondary_email_type character varying(20)
);


ALTER TABLE vaxiom.employee_dependent OWNER TO postgres;

--
-- TOC entry 744 (class 1259 OID 889873)
-- Name: employee_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_enrollment (
    id numeric(20,0) NOT NULL,
    status character varying(45),
    fill_date timestamp without time zone,
    summary_document_id character varying(45),
    enrollment_id numeric(20,0),
    enrollment_form_source_event_id numeric(20,0) NOT NULL,
    employment_contracts_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_type character varying(64),
    qle_occurrence_date date,
    qle_parent numeric(20,0),
    other_qle_description character varying(500),
    first_name character varying(45),
    last_name character varying(45),
    middle_name character varying(45),
    total_annualized_earnings numeric(30,10),
    gender character varying(11),
    birth_date date,
    effective_coverage_start date,
    fill_date_offset character varying(20),
    confirmed_event_date boolean DEFAULT false,
    deny_acknowledged boolean DEFAULT false,
    confirmed boolean DEFAULT false,
    sys_portal_last_modified timestamp without time zone,
    sys_portal_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.employee_enrollment OWNER TO postgres;

--
-- TOC entry 9015 (class 0 OID 0)
-- Dependencies: 744
-- Name: TABLE employee_enrollment; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON TABLE vaxiom.employee_enrollment IS '		';


--
-- TOC entry 745 (class 1259 OID 889882)
-- Name: employee_enrollment_attachment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_enrollment_attachment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(45) NOT NULL,
    file_id character varying(127),
    file_mime_type character varying(255),
    file_name character varying(127),
    file_size numeric(20,0),
    employee_enrollment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.employee_enrollment_attachment OWNER TO postgres;

--
-- TOC entry 746 (class 1259 OID 889888)
-- Name: employee_enrollment_event; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_enrollment_event (
    id numeric(20,0) NOT NULL,
    employee_enrollment_id numeric(20,0) NOT NULL,
    old_status character varying(45),
    new_status character varying(45),
    username character varying(255),
    comment character varying(255),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.employee_enrollment_event OWNER TO postgres;

--
-- TOC entry 747 (class 1259 OID 889894)
-- Name: employee_enrollment_person; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_enrollment_person (
    id numeric(20,0) NOT NULL,
    first_name character varying(100),
    middle_name character varying(45),
    last_name character varying(45),
    date_of_birth date,
    relationship character varying(45),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    phone_number character varying(45),
    email character varying(100),
    qle_parent numeric(20,0),
    employee_enrollment_id numeric(20,0) NOT NULL,
    organization_name character varying(100),
    organization_form_date date,
    resp_person_name character varying(400),
    resp_person_city character varying(45),
    resp_person_state character varying(20),
    type character varying(10)
);


ALTER TABLE vaxiom.employee_enrollment_person OWNER TO postgres;

--
-- TOC entry 748 (class 1259 OID 889900)
-- Name: employee_insured; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_insured (
    id numeric(20,0) NOT NULL,
    employee_beneficiary_group_id numeric(20,0),
    employee_dependent_id numeric(20,0) NOT NULL,
    employee_benefit_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0),
    smoker boolean DEFAULT false
);


ALTER TABLE vaxiom.employee_insured OWNER TO postgres;

--
-- TOC entry 749 (class 1259 OID 889904)
-- Name: employee_professional_relationships; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_professional_relationships (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    provider_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.employee_professional_relationships OWNER TO postgres;

--
-- TOC entry 750 (class 1259 OID 889907)
-- Name: employee_resources; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employee_resources (
    id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0) NOT NULL,
    invalid boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.employee_resources OWNER TO postgres;

--
-- TOC entry 751 (class 1259 OID 889911)
-- Name: employers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employers (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    master_id numeric(20,0),
    organization_id numeric(20,0),
    contact_postal_address_id numeric(20,0),
    contact_phone_id numeric(20,0),
    contact_email_id numeric(20,0),
    name character varying(255)
);


ALTER TABLE vaxiom.employers OWNER TO postgres;

--
-- TOC entry 753 (class 1259 OID 889921)
-- Name: employment_contract_emergency_contact; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_emergency_contact (
    id numeric(20,0) NOT NULL,
    first_name character varying(45) NOT NULL,
    middle_name character varying(45),
    last_name character varying(45) NOT NULL,
    email character varying(45),
    phone_number character varying(45) NOT NULL,
    relationship character varying(45) NOT NULL,
    type character varying(45) NOT NULL,
    employment_contracts_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.employment_contract_emergency_contact OWNER TO postgres;

--
-- TOC entry 754 (class 1259 OID 889924)
-- Name: employment_contract_enrollment_group_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_enrollment_group_log (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    employment_contracts_id numeric(20,0) NOT NULL,
    old_enrollment_group_id numeric(20,0),
    new_enrollment_group_id numeric(20,0),
    date_of_occurance date NOT NULL,
    commnet character varying(500)
);


ALTER TABLE vaxiom.employment_contract_enrollment_group_log OWNER TO postgres;

--
-- TOC entry 755 (class 1259 OID 889930)
-- Name: employment_contract_job_status_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_job_status_log (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    employment_contracts_id numeric(20,0) NOT NULL,
    old_job_status character varying(45),
    new_job_status character varying(45) NOT NULL,
    date_of_occurance date
);


ALTER TABLE vaxiom.employment_contract_job_status_log OWNER TO postgres;

--
-- TOC entry 756 (class 1259 OID 889933)
-- Name: employment_contract_location_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_location_group (
    id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0),
    location_group_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0)
);


ALTER TABLE vaxiom.employment_contract_location_group OWNER TO postgres;

--
-- TOC entry 757 (class 1259 OID 889936)
-- Name: employment_contract_permissions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_permissions (
    id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0),
    permissions_permission_id numeric(20,0) NOT NULL,
    access_type character varying(30) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0)
);


ALTER TABLE vaxiom.employment_contract_permissions OWNER TO postgres;

--
-- TOC entry 758 (class 1259 OID 889939)
-- Name: employment_contract_role; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_role (
    id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0),
    permissions_user_role_id numeric(20,0),
    sys_organizations_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.employment_contract_role OWNER TO postgres;

--
-- TOC entry 759 (class 1259 OID 889942)
-- Name: employment_contract_salary_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contract_salary_log (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    employment_contracts_id numeric(20,0) NOT NULL,
    salary_structure character varying(45),
    salary numeric(30,10),
    day_rate numeric(30,10),
    hourly_rate numeric(30,10),
    avg_hours_per_week numeric(30,10),
    avg_days_per_week numeric(30,10),
    add_annual_compensation numeric(30,10),
    total_annual_earnings numeric(30,10),
    ltd_bonus_up_annualized numeric(30,10),
    annualized_commission numeric(30,10),
    date_of_occurance date NOT NULL,
    payroll_cycle character varying(40),
    commnet character varying(500)
);


ALTER TABLE vaxiom.employment_contract_salary_log OWNER TO postgres;

--
-- TOC entry 752 (class 1259 OID 889914)
-- Name: employment_contracts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.employment_contracts (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    legacy_id character varying(255),
    organization_id numeric(20,0),
    person_id numeric(20,0) NOT NULL,
    company character varying(100),
    department character varying(100),
    location character varying(100),
    employment_start_date date NOT NULL,
    employment_end_date date,
    job_status character varying(20) NOT NULL,
    job_class character varying(20),
    job_title character varying(100),
    enrollment_class character varying(20),
    salary numeric(30,10),
    annualized_commission numeric(30,10),
    ltd_bonus_up_annualized numeric(30,10),
    signature bytea,
    human_id numeric(20,0),
    npi character varying(45),
    tin character varying(45),
    medicaid_id character varying(45),
    hourly_rate numeric(30,10),
    day_rate numeric(30,10),
    avg_hours_per_week numeric(30,10),
    avg_days_per_week numeric(30,10),
    salary_structure character varying(45),
    add_annual_compensation numeric(30,10),
    total_annual_earnings numeric(30,10),
    current_employment_start_date date,
    payroll_cycle character varying(45),
    legal_entity_id numeric(20,0) NOT NULL,
    physical_address_id numeric(20,0),
    department_id numeric(20,0),
    division_id numeric(20,0),
    enrollment_group_id numeric(20,0),
    billing_group_id numeric(20,0),
    jobtitle_id numeric(20,0),
    coverage_period character varying(100),
    deleted boolean DEFAULT false
);


ALTER TABLE vaxiom.employment_contracts OWNER TO postgres;

--
-- TOC entry 760 (class 1259 OID 889948)
-- Name: enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    effective_coverage_start date,
    effective_coverage_end date,
    year integer,
    oe_start date,
    oe_end date,
    oe_email_template character varying(100),
    oe_sms_template character varying(100),
    nh_email_template character varying(100),
    nh_sms_template character varying(100),
    legal_entity_id numeric(20,0) NOT NULL,
    email_reminder integer NOT NULL,
    sms_reminder integer NOT NULL,
    new_hire_effective_covearge_definition character varying(100),
    renewal_date date,
    expiration_date date,
    cancellation_date date,
    status character varying(60),
    last_enrollment_id numeric(20,0)
);


ALTER TABLE vaxiom.enrollment OWNER TO postgres;

--
-- TOC entry 762 (class 1259 OID 889956)
-- Name: enrollment_form; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_id numeric(20,0) NOT NULL,
    employee_id numeric(20,0) NOT NULL,
    status character varying(20),
    fill_date timestamp without time zone,
    dependents_confirmation boolean,
    confirmation_document_id character varying(100)
);


ALTER TABLE vaxiom.enrollment_form OWNER TO postgres;

--
-- TOC entry 764 (class 1259 OID 889962)
-- Name: enrollment_form_beneficiary; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_beneficiary (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    beneficiary_data text
);


ALTER TABLE vaxiom.enrollment_form_beneficiary OWNER TO postgres;

--
-- TOC entry 763 (class 1259 OID 889960)
-- Name: enrollment_form_beneficiary_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_beneficiary_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_beneficiary_id_seq OWNER TO postgres;

--
-- TOC entry 9016 (class 0 OID 0)
-- Dependencies: 763
-- Name: enrollment_form_beneficiary_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_beneficiary_id_seq OWNED BY vaxiom.enrollment_form_beneficiary.id;


--
-- TOC entry 766 (class 1259 OID 889971)
-- Name: enrollment_form_benefits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_benefits (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    benefit_type character varying(30),
    benefit_data text
);


ALTER TABLE vaxiom.enrollment_form_benefits OWNER TO postgres;

--
-- TOC entry 765 (class 1259 OID 889969)
-- Name: enrollment_form_benefits_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_benefits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_benefits_id_seq OWNER TO postgres;

--
-- TOC entry 9017 (class 0 OID 0)
-- Dependencies: 765
-- Name: enrollment_form_benefits_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_benefits_id_seq OWNED BY vaxiom.enrollment_form_benefits.id;


--
-- TOC entry 768 (class 1259 OID 889980)
-- Name: enrollment_form_dependents; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_dependents (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    dependent_data text
);


ALTER TABLE vaxiom.enrollment_form_dependents OWNER TO postgres;

--
-- TOC entry 770 (class 1259 OID 889989)
-- Name: enrollment_form_dependents_benefits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_dependents_benefits (
    id bigint NOT NULL,
    enrollment_form_benefit_id numeric(20,0),
    enrollment_form_dependent_id numeric(20,0),
    benefit_data text
);


ALTER TABLE vaxiom.enrollment_form_dependents_benefits OWNER TO postgres;

--
-- TOC entry 769 (class 1259 OID 889987)
-- Name: enrollment_form_dependents_benefits_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_dependents_benefits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_dependents_benefits_id_seq OWNER TO postgres;

--
-- TOC entry 9018 (class 0 OID 0)
-- Dependencies: 769
-- Name: enrollment_form_dependents_benefits_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_dependents_benefits_id_seq OWNED BY vaxiom.enrollment_form_dependents_benefits.id;


--
-- TOC entry 767 (class 1259 OID 889978)
-- Name: enrollment_form_dependents_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_dependents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_dependents_id_seq OWNER TO postgres;

--
-- TOC entry 9019 (class 0 OID 0)
-- Dependencies: 767
-- Name: enrollment_form_dependents_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_dependents_id_seq OWNED BY vaxiom.enrollment_form_dependents.id;


--
-- TOC entry 761 (class 1259 OID 889954)
-- Name: enrollment_form_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_id_seq OWNER TO postgres;

--
-- TOC entry 9020 (class 0 OID 0)
-- Dependencies: 761
-- Name: enrollment_form_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_id_seq OWNED BY vaxiom.enrollment_form.id;


--
-- TOC entry 772 (class 1259 OID 889998)
-- Name: enrollment_form_personal_data; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_personal_data (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    personal_data text
);


ALTER TABLE vaxiom.enrollment_form_personal_data OWNER TO postgres;

--
-- TOC entry 771 (class 1259 OID 889996)
-- Name: enrollment_form_personal_data_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.enrollment_form_personal_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.enrollment_form_personal_data_id_seq OWNER TO postgres;

--
-- TOC entry 9021 (class 0 OID 0)
-- Dependencies: 771
-- Name: enrollment_form_personal_data_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.enrollment_form_personal_data_id_seq OWNED BY vaxiom.enrollment_form_personal_data.id;


--
-- TOC entry 773 (class 1259 OID 890005)
-- Name: enrollment_form_source_event; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_form_source_event (
    id numeric(20,0) NOT NULL,
    source_type character varying(45),
    sub_type character varying(45),
    configuration character varying(1200),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.enrollment_form_source_event OWNER TO postgres;

--
-- TOC entry 774 (class 1259 OID 890011)
-- Name: enrollment_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_group (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    group_type character varying(45)
);


ALTER TABLE vaxiom.enrollment_group OWNER TO postgres;

--
-- TOC entry 776 (class 1259 OID 890017)
-- Name: enrollment_group_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_group_enrollment (
    enrollment_group_id numeric(20,0) NOT NULL,
    enrollment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.enrollment_group_enrollment OWNER TO postgres;

--
-- TOC entry 775 (class 1259 OID 890014)
-- Name: enrollment_groups_enrollment_legal_entity; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_groups_enrollment_legal_entity (
    enrollment_group_id numeric(20,0) NOT NULL,
    legal_entity_id numeric(20,0) NOT NULL,
    enrollment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.enrollment_groups_enrollment_legal_entity OWNER TO postgres;

--
-- TOC entry 777 (class 1259 OID 890020)
-- Name: enrollment_schedule_billing_groups; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.enrollment_schedule_billing_groups (
    enrollment_id numeric(20,0) NOT NULL,
    carrier character varying(45) NOT NULL,
    legal_entity_id numeric(20,0) NOT NULL,
    billing_group character varying(50)
);


ALTER TABLE vaxiom.enrollment_schedule_billing_groups OWNER TO postgres;

--
-- TOC entry 9022 (class 0 OID 0)
-- Dependencies: 777
-- Name: COLUMN enrollment_schedule_billing_groups.enrollment_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.enrollment_schedule_billing_groups.enrollment_id IS 'Enrollment primary key';


--
-- TOC entry 9023 (class 0 OID 0)
-- Dependencies: 777
-- Name: COLUMN enrollment_schedule_billing_groups.carrier; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.enrollment_schedule_billing_groups.carrier IS 'Insurance Plan carrier';


--
-- TOC entry 9024 (class 0 OID 0)
-- Dependencies: 777
-- Name: COLUMN enrollment_schedule_billing_groups.legal_entity_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.enrollment_schedule_billing_groups.legal_entity_id IS 'Legal entity primary key (ENROLLMENT | legal_entities_participating_enrollment)';


--
-- TOC entry 9025 (class 0 OID 0)
-- Dependencies: 777
-- Name: COLUMN enrollment_schedule_billing_groups.billing_group; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.enrollment_schedule_billing_groups.billing_group IS 'Manual ID of the carrier';


--
-- TOC entry 778 (class 1259 OID 890023)
-- Name: equipment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.equipment (
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.equipment OWNER TO postgres;

--
-- TOC entry 779 (class 1259 OID 890027)
-- Name: external_offices; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.external_offices (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    postal_address_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.external_offices OWNER TO postgres;

--
-- TOC entry 780 (class 1259 OID 890030)
-- Name: financial_settings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.financial_settings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    payment_term character varying(255),
    autopay_payment_term character varying(255),
    realization character varying(255),
    realization_dom integer
);


ALTER TABLE vaxiom.financial_settings OWNER TO postgres;

--
-- TOC entry 781 (class 1259 OID 890036)
-- Name: fix_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.fix_adjustments (
    id numeric(20,0) NOT NULL,
    from_migration boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.fix_adjustments OWNER TO postgres;

--
-- TOC entry 782 (class 1259 OID 890040)
-- Name: gf10219; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.gf10219 (
    patient_id numeric(20,0),
    appointment_id numeric(20,0),
    tx_id numeric(20,0),
    wrong_next_appointment_id numeric(20,0),
    wrong_tx_id numeric(20,0),
    correct_next_appointment_id numeric(20,0),
    correct_next_tx_id numeric(20,0)
);


ALTER TABLE vaxiom.gf10219 OWNER TO postgres;

--
-- TOC entry 783 (class 1259 OID 890043)
-- Name: gf9412_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.gf9412_payments (
    human_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    patient_name character varying(511),
    insured_id numeric(20,0) NOT NULL,
    payment_account_id_ok numeric(20,0),
    company_ok character varying(255) NOT NULL,
    payment_id numeric(20,0) NOT NULL,
    payment_sys_created timestamp without time zone,
    payer_name character varying(255),
    payment_account_id_wrong numeric(20,0),
    migrated boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.gf9412_payments OWNER TO postgres;

--
-- TOC entry 784 (class 1259 OID 890050)
-- Name: gf9412_receivables; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.gf9412_receivables (
    human_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    patient_name character varying(511),
    receivable_id numeric(20,0) NOT NULL,
    rtype character varying(20) NOT NULL,
    rec_sys_created timestamp without time zone,
    insured_id numeric(20,0) NOT NULL,
    payment_account_id_ok numeric(20,0),
    company_ok character varying(255) NOT NULL,
    receivable_account_id_wrong numeric(20,0),
    company_wrong character varying(255)
);


ALTER TABLE vaxiom.gf9412_receivables OWNER TO postgres;

--
-- TOC entry 785 (class 1259 OID 890056)
-- Name: gf9412_unapplied; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.gf9412_unapplied (
    human_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    patient_name character varying(511),
    payment_id numeric(20,0) NOT NULL,
    payment_account_id_ok numeric(20,0) NOT NULL,
    payment_account_id_wrong numeric(20,0),
    account_type character varying(30) NOT NULL,
    id numeric(20,0) NOT NULL,
    name character varying(50) NOT NULL,
    billing_center_name character varying(255),
    payer_name character varying(255)
);


ALTER TABLE vaxiom.gf9412_unapplied OWNER TO postgres;

--
-- TOC entry 787 (class 1259 OID 890065)
-- Name: hr_admin_config; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.hr_admin_config (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    name character varying(45) NOT NULL,
    value character varying(100)
);


ALTER TABLE vaxiom.hr_admin_config OWNER TO postgres;

--
-- TOC entry 786 (class 1259 OID 890062)
-- Name: hradmin_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.hradmin_enrollment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    start_date date NOT NULL,
    end_date date NOT NULL,
    mode character varying(10) NOT NULL,
    company_name character varying(80) NOT NULL,
    tax_id character varying(20) NOT NULL,
    sic_code character varying(8) NOT NULL,
    street character varying(60) NOT NULL,
    state character varying(40) NOT NULL,
    city character varying(60) NOT NULL,
    zip_code character varying(20) NOT NULL
);


ALTER TABLE vaxiom.hradmin_enrollment OWNER TO postgres;

--
-- TOC entry 788 (class 1259 OID 890068)
-- Name: image_series; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.image_series (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0),
    type_id numeric(20,0),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created date,
    sys_last_modified date,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.image_series OWNER TO postgres;

--
-- TOC entry 789 (class 1259 OID 890071)
-- Name: image_series_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.image_series_types (
    id numeric(20,0) NOT NULL,
    name character varying(100) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created date,
    sys_last_modified date,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.image_series_types OWNER TO postgres;

--
-- TOC entry 840 (class 1259 OID 890265)
-- Name: in_network_discounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.in_network_discounts (
    id numeric(20,0) NOT NULL,
    network_sheet_fee_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.in_network_discounts OWNER TO postgres;

--
-- TOC entry 790 (class 1259 OID 890074)
-- Name: installment_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.installment_charges (
    id numeric(20,0) NOT NULL,
    payment_plan_id numeric(20,0)
);


ALTER TABLE vaxiom.installment_charges OWNER TO postgres;

--
-- TOC entry 791 (class 1259 OID 890077)
-- Name: insurance_billing_centers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_billing_centers (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255),
    payer_id character varying(255),
    contact_postal_address_id numeric(20,0),
    organization_id numeric(20,0),
    contact_phone_id numeric(20,0),
    contact_email_id numeric(20,0),
    master_id numeric(20,0)
);


ALTER TABLE vaxiom.insurance_billing_centers OWNER TO postgres;

--
-- TOC entry 792 (class 1259 OID 890083)
-- Name: insurance_claims_setup; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_claims_setup (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    billing_entity_id numeric(20,0) NOT NULL,
    legal_entity_npi_id numeric(20,0),
    sys_organization_address_id numeric(20,0),
    sys_organization_contact_method_id numeric(20,0),
    license_number_type character varying(45) DEFAULT 'NONE'::character varying NOT NULL,
    license_number_value character varying(45),
    ssn character(11),
    tin character varying(45),
    tin_id numeric(20,0)
);


ALTER TABLE vaxiom.insurance_claims_setup OWNER TO postgres;

--
-- TOC entry 794 (class 1259 OID 890093)
-- Name: insurance_code_val; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_code_val (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    coverage numeric(30,10),
    fee numeric(30,10),
    insurance_code_id numeric(20,0) NOT NULL,
    insurance_company_id numeric(20,0),
    organization_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_code_val OWNER TO postgres;

--
-- TOC entry 793 (class 1259 OID 890087)
-- Name: insurance_codes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_codes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    code character varying(255) NOT NULL,
    title character varying(255)
);


ALTER TABLE vaxiom.insurance_codes OWNER TO postgres;

--
-- TOC entry 795 (class 1259 OID 890096)
-- Name: insurance_companies; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_companies (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0),
    carrier_name character varying(255) NOT NULL,
    electronic boolean,
    website character varying(255),
    installment_interval character varying(255),
    continuation_claims_required boolean DEFAULT false NOT NULL,
    postal_address_id numeric(20,0),
    am_name character varying(255),
    am_phone_number_id numeric(20,0),
    am_email_address_id numeric(20,0),
    master_id numeric(20,0),
    medicaid boolean DEFAULT false NOT NULL,
    medicaid_state character varying(255)
);


ALTER TABLE vaxiom.insurance_companies OWNER TO postgres;

--
-- TOC entry 796 (class 1259 OID 890104)
-- Name: insurance_company_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_company_payments (
    insurance_company_id numeric(20,0) NOT NULL,
    company_name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.insurance_company_payments OWNER TO postgres;

--
-- TOC entry 797 (class 1259 OID 890107)
-- Name: insurance_company_phone_associations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_company_phone_associations (
    id numeric(20,0) NOT NULL,
    insurance_company_id numeric(20,0) NOT NULL,
    contact_method_id numeric(20,0) NOT NULL,
    description character varying(255),
    preferred boolean DEFAULT false NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_company_phone_associations OWNER TO postgres;

--
-- TOC entry 799 (class 1259 OID 890117)
-- Name: insurance_payment_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_accounts (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    insurance_company_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_payment_accounts OWNER TO postgres;

--
-- TOC entry 800 (class 1259 OID 890120)
-- Name: insurance_payment_corrections; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_corrections (
    id numeric(20,0) NOT NULL,
    correction_type_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_payment_corrections OWNER TO postgres;

--
-- TOC entry 9026 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN insurance_payment_corrections.id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_corrections.id IS 'corresponds to a insurance_payment_movements id';


--
-- TOC entry 9027 (class 0 OID 0)
-- Dependencies: 800
-- Name: COLUMN insurance_payment_corrections.correction_type_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_corrections.correction_type_id IS 'Correction types such as  Posting Error,Recoupled Payment,Reversed Orthobanc Payment';


--
-- TOC entry 801 (class 1259 OID 890123)
-- Name: insurance_payment_movements; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_movements (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_created_at numeric(20,0) NOT NULL,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    dtype character varying(31) NOT NULL,
    insurance_payment_id numeric(20,0) NOT NULL,
    moved_on timestamp without time zone,
    moved_at numeric(20,0) NOT NULL,
    amount numeric(30,10) NOT NULL,
    notes character varying(500)
);


ALTER TABLE vaxiom.insurance_payment_movements OWNER TO postgres;

--
-- TOC entry 9028 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.dtype; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.dtype IS 'Indicates the actual movement type';


--
-- TOC entry 9029 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.insurance_payment_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.insurance_payment_id IS 'Insurance payment id';


--
-- TOC entry 9030 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.moved_on; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.moved_on IS 'timestamp when movement was created';


--
-- TOC entry 9031 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.moved_at; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.moved_at IS 'Location id where movement was created';


--
-- TOC entry 9032 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.amount; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.amount IS 'Actual amount moved';


--
-- TOC entry 9033 (class 0 OID 0)
-- Dependencies: 801
-- Name: COLUMN insurance_payment_movements.notes; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_movements.notes IS 'Notes related  for movement ';


--
-- TOC entry 802 (class 1259 OID 890129)
-- Name: insurance_payment_refunds; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_refunds (
    id numeric(20,0) NOT NULL,
    refund_type character varying(20) NOT NULL,
    payment_method character varying(50),
    payment_method_number character varying(200)
);


ALTER TABLE vaxiom.insurance_payment_refunds OWNER TO postgres;

--
-- TOC entry 9034 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN insurance_payment_refunds.id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_refunds.id IS 'corresponds to a insurance_payment_movements id';


--
-- TOC entry 9035 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN insurance_payment_refunds.refund_type; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_refunds.refund_type IS 'Refund type such as Overpayment,Posting Error, Transfer Out ';


--
-- TOC entry 9036 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN insurance_payment_refunds.payment_method; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_refunds.payment_method IS 'Refund payment methods , such as Cash,Check or CreditCard';


--
-- TOC entry 9037 (class 0 OID 0)
-- Dependencies: 802
-- Name: COLUMN insurance_payment_refunds.payment_method_number; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_refunds.payment_method_number IS 'Refund payment methods , such as Cash,Check or CreditCard';


--
-- TOC entry 803 (class 1259 OID 890132)
-- Name: insurance_payment_transfers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payment_transfers (
    id numeric(20,0) NOT NULL,
    payment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_payment_transfers OWNER TO postgres;

--
-- TOC entry 9038 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN insurance_payment_transfers.id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_transfers.id IS 'corresponds to a insurance_payment_movements id';


--
-- TOC entry 9039 (class 0 OID 0)
-- Dependencies: 803
-- Name: COLUMN insurance_payment_transfers.payment_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payment_transfers.payment_id IS 'Refers to the actual patient  payment created for the transfer';


--
-- TOC entry 798 (class 1259 OID 890111)
-- Name: insurance_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_payments (
    id numeric(20,0) NOT NULL,
    human_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_created_at numeric(20,0) NOT NULL,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    insurance_company_id numeric(20,0) NOT NULL,
    dtype character varying(20) NOT NULL,
    posted_on timestamp without time zone,
    posted_at numeric(20,0),
    amount numeric(30,10),
    payment_type_id numeric(20,0),
    payer_name character varying(255),
    check_or_card_number character varying(30),
    cc_expire character varying(4),
    notes character varying(500)
);


ALTER TABLE vaxiom.insurance_payments OWNER TO postgres;

--
-- TOC entry 9040 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.insurance_company_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.insurance_company_id IS 'Insurance company payment';


--
-- TOC entry 9041 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.dtype; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.dtype IS 'insurance payment type such as check,credit, cash, other ';


--
-- TOC entry 9042 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.posted_on; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.posted_on IS 'timestamp when payment was created';


--
-- TOC entry 9043 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.posted_at; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.posted_at IS 'Location id where payment was created';


--
-- TOC entry 9044 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.amount; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.amount IS 'Actual amount posted';


--
-- TOC entry 9045 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.payment_type_id; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.payment_type_id IS 'Insurance Payment method type used for payment ';


--
-- TOC entry 9046 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.payer_name; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.payer_name IS 'Insurance Payment method payer name ';


--
-- TOC entry 9047 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.check_or_card_number; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.check_or_card_number IS 'Insurance Credit card last four digits or Check number ';


--
-- TOC entry 9048 (class 0 OID 0)
-- Dependencies: 798
-- Name: COLUMN insurance_payments.cc_expire; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_payments.cc_expire IS 'Insurance Credit Payment card expire ';


--
-- TOC entry 804 (class 1259 OID 890135)
-- Name: insurance_plan; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan (
    id numeric(20,0) NOT NULL,
    benefit_template_id numeric(20,0),
    record_type character varying(45),
    plan_name character varying(45),
    other_fields character varying(1200),
    benefit_type character varying(45),
    carrier character varying(45),
    summary_plan_pdf character varying(45),
    coverage_levels character varying(100),
    employer character varying(100),
    employee_clasification character varying(45),
    enrollment_class character varying(45),
    payroll_deduction character varying(45) NOT NULL,
    premium_allocation_type character varying(45),
    premium_allocation_employer_value numeric(30,10),
    rate_type character varying(45),
    wave_allowed boolean,
    wave_with_reason boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0),
    renewal_date date,
    policy_number character varying(45),
    inception_date date,
    smoker_definition character varying(500),
    deleted boolean DEFAULT false,
    spouse_premium_calculation character varying(20),
    benefit_amount_type character varying(45),
    parent_insurance_plan_id numeric(20,0),
    summary_plan_pdf_file_name character varying(60),
    prior_policy_number character varying(45)
);


ALTER TABLE vaxiom.insurance_plan OWNER TO postgres;

--
-- TOC entry 9049 (class 0 OID 0)
-- Dependencies: 804
-- Name: TABLE insurance_plan; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON TABLE vaxiom.insurance_plan IS 'table with insuracen plan definition';


--
-- TOC entry 9050 (class 0 OID 0)
-- Dependencies: 804
-- Name: COLUMN insurance_plan.record_type; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.insurance_plan.record_type IS 'Definines if record is a template or specyfic plan definition';


--
-- TOC entry 805 (class 1259 OID 890142)
-- Name: insurance_plan_adandd; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_adandd (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL,
    employee_rate numeric(30,10),
    spouse_rate numeric(30,10),
    child_rate numeric(30,10),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_adandd OWNER TO postgres;

--
-- TOC entry 806 (class 1259 OID 890145)
-- Name: insurance_plan_age_and_gender_banded_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_age_and_gender_banded_rate (
    id numeric(20,0) NOT NULL,
    min_age integer NOT NULL,
    max_age integer NOT NULL,
    gender character varying(45) NOT NULL,
    only_me_rate numeric(30,10) NOT NULL,
    me_and_spouse_rate numeric(30,10) NOT NULL,
    me_and_chidren_rate numeric(30,10) NOT NULL,
    family_rate numeric(30,10) NOT NULL,
    tabaco boolean NOT NULL,
    me_and_one_child numeric(30,10) NOT NULL,
    only_me_rate_tobacco numeric(30,10),
    me_and_spouse_rate_tobacco numeric(30,10),
    me_and_chidren_rate_tobacco numeric(30,10),
    family_rate_tobacco numeric(30,10),
    me_and_one_child_tobacco numeric(30,10)
);


ALTER TABLE vaxiom.insurance_plan_age_and_gender_banded_rate OWNER TO postgres;

--
-- TOC entry 807 (class 1259 OID 890148)
-- Name: insurance_plan_age_banded_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_age_banded_rate (
    id numeric(20,0) NOT NULL,
    min_age integer NOT NULL,
    max_age integer NOT NULL,
    rate numeric(30,10) NOT NULL,
    tabaco boolean NOT NULL,
    tobacco_rate numeric(30,10)
);


ALTER TABLE vaxiom.insurance_plan_age_banded_rate OWNER TO postgres;

--
-- TOC entry 808 (class 1259 OID 890151)
-- Name: insurance_plan_age_reduction; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_age_reduction (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL,
    min_reduced_amt numeric(30,10),
    min_reduction_amt numeric(30,10),
    applies_to_employee boolean,
    applies_to_spouse boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_age_reduction OWNER TO postgres;

--
-- TOC entry 809 (class 1259 OID 890154)
-- Name: insurance_plan_age_reduction_detail; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_age_reduction_detail (
    id numeric(20,0) NOT NULL,
    insurance_plan_age_reduction_id numeric(20,0) NOT NULL,
    age bigint NOT NULL,
    reduction double precision NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_age_reduction_detail OWNER TO postgres;

--
-- TOC entry 810 (class 1259 OID 890157)
-- Name: insurance_plan_benefit_amount; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_benefit_amount (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    rounding_rule character varying(45),
    rounding_rule_value numeric(30,10),
    insurance_plan_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_benefit_amount OWNER TO postgres;

--
-- TOC entry 811 (class 1259 OID 890160)
-- Name: insurance_plan_children_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_children_rate (
    id numeric(20,0) NOT NULL,
    rate numeric(30,10),
    tobacco_rate numeric(30,10),
    coverage_type character varying(20)
);


ALTER TABLE vaxiom.insurance_plan_children_rate OWNER TO postgres;

--
-- TOC entry 812 (class 1259 OID 890163)
-- Name: insurance_plan_complex_benefit_amount; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_complex_benefit_amount (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_complex_benefit_amount OWNER TO postgres;

--
-- TOC entry 813 (class 1259 OID 890166)
-- Name: insurance_plan_complex_benefit_amount_item; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_complex_benefit_amount_item (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    insured_type character varying(45) NOT NULL,
    min_amount_value numeric(30,10),
    max_amount_type character varying(45),
    max_amount_money_value numeric(30,10),
    max_amount_percentage_value numeric(30,10),
    max_amount_earnings_multiply_value numeric(30,10),
    increment_amounts numeric(30,10),
    insurance_plan_complex_benefit_amount_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_complex_benefit_amount_item OWNER TO postgres;

--
-- TOC entry 814 (class 1259 OID 890169)
-- Name: insurance_plan_composition_detailed_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_composition_detailed_rate (
    id numeric(20,0) NOT NULL,
    employee_amount numeric(30,10) NOT NULL,
    spouse_amount numeric(30,10) NOT NULL,
    child_amount numeric(30,10) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_composition_detailed_rate OWNER TO postgres;

--
-- TOC entry 815 (class 1259 OID 890172)
-- Name: insurance_plan_composition_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_composition_rate (
    id numeric(20,0) NOT NULL,
    only_me_amount numeric(30,10) NOT NULL,
    me_and_spouse_rate numeric(30,10) NOT NULL,
    me_and_chidren_amount numeric(30,10) NOT NULL,
    family_amount numeric(30,10) NOT NULL,
    me_and_one_child numeric(30,10) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_composition_rate OWNER TO postgres;

--
-- TOC entry 816 (class 1259 OID 890175)
-- Name: insurance_plan_employers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_employers (
    insurance_plan_id numeric(20,0) NOT NULL,
    employer_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_employers OWNER TO postgres;

--
-- TOC entry 817 (class 1259 OID 890178)
-- Name: insurance_plan_fixed_limit; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_fixed_limit (
    id numeric(20,0) NOT NULL,
    amount_min numeric(30,10),
    amount_max numeric(30,10),
    insurance_limits_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    alert_limit numeric(30,10) DEFAULT '0'::numeric NOT NULL,
    step integer DEFAULT 1
);


ALTER TABLE vaxiom.insurance_plan_fixed_limit OWNER TO postgres;

--
-- TOC entry 818 (class 1259 OID 890183)
-- Name: insurance_plan_guaranteed_increase; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_guaranteed_increase (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0),
    enabled boolean,
    increment numeric(30,10),
    applies_to_employee boolean,
    applies_to_spouse boolean,
    applies_to_child boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_guaranteed_increase OWNER TO postgres;

--
-- TOC entry 819 (class 1259 OID 890186)
-- Name: insurance_plan_guaranteed_issue; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_guaranteed_issue (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0),
    enabled boolean,
    amount numeric(30,10),
    availability character varying(20),
    first_time boolean,
    grandfather_rule boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    amount_child numeric(30,10),
    amount_spouse numeric(30,10),
    availability_child character varying(20),
    availability_spouse character varying(20),
    enabled_me boolean,
    enabled_spouse boolean,
    enabled_child boolean
);


ALTER TABLE vaxiom.insurance_plan_guaranteed_issue OWNER TO postgres;

--
-- TOC entry 820 (class 1259 OID 890189)
-- Name: insurance_plan_has_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_has_enrollment (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    insurance_plan_id numeric(20,0) NOT NULL,
    enrollment_id numeric(20,0) NOT NULL,
    status character varying(45),
    removed boolean DEFAULT false
);


ALTER TABLE vaxiom.insurance_plan_has_enrollment OWNER TO postgres;

--
-- TOC entry 821 (class 1259 OID 890193)
-- Name: insurance_plan_has_enrollment_has_enrollment_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_has_enrollment_has_enrollment_group (
    insurance_plan_has_enrollment_id numeric(20,0) NOT NULL,
    enrollment_group_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_has_enrollment_has_enrollment_group OWNER TO postgres;

--
-- TOC entry 822 (class 1259 OID 890196)
-- Name: insurance_plan_limits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_limits (
    id numeric(20,0) NOT NULL,
    type character varying(45),
    insurance_plan_id numeric(20,0) NOT NULL,
    benefit_type character varying(45),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_limits OWNER TO postgres;

--
-- TOC entry 823 (class 1259 OID 890199)
-- Name: insurance_plan_medical_connection; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_medical_connection (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0),
    enabled boolean,
    applies_to_employee boolean,
    applies_to_spouse boolean,
    applies_to_child boolean,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_medical_connection OWNER TO postgres;

--
-- TOC entry 824 (class 1259 OID 890202)
-- Name: insurance_plan_misc; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_misc (
    id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL,
    is_bonus_up boolean,
    offer_adandd boolean,
    payments_begin integer,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_misc OWNER TO postgres;

--
-- TOC entry 825 (class 1259 OID 890205)
-- Name: insurance_plan_rates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_rates (
    id numeric(20,0) NOT NULL,
    type character varying(45),
    insurance_plan_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.insurance_plan_rates OWNER TO postgres;

--
-- TOC entry 826 (class 1259 OID 890208)
-- Name: insurance_plan_saving_account; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_saving_account (
    insurance_plan_id numeric(20,0) NOT NULL,
    id numeric(20,0) NOT NULL,
    name character varying(45) NOT NULL,
    limit_amt numeric(30,10),
    employer_cost numeric(30,10) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enabled boolean,
    employer_contributes boolean,
    summary_plan_pdf character varying(255),
    type character varying(45),
    summary_plan_pdf_file_name character varying(255),
    tpa character varying(60)
);


ALTER TABLE vaxiom.insurance_plan_saving_account OWNER TO postgres;

--
-- TOC entry 827 (class 1259 OID 890214)
-- Name: insurance_plan_saving_accounts_limits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_saving_accounts_limits (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    year integer NOT NULL,
    hsa_individual_limit numeric(30,10) NOT NULL,
    hsa_family_limit numeric(30,10) NOT NULL,
    hsa_catch_up numeric(30,10) NOT NULL,
    fsa_health numeric(30,10) NOT NULL,
    fsa_dependent_care numeric(30,10) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_saving_accounts_limits OWNER TO postgres;

--
-- TOC entry 828 (class 1259 OID 890217)
-- Name: insurance_plan_separate_age_banded_rate; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_separate_age_banded_rate (
    id numeric(20,0) NOT NULL,
    min_age integer NOT NULL,
    max_age integer NOT NULL,
    rate numeric(30,10) NOT NULL,
    tabaco boolean NOT NULL,
    tobacco_rate numeric(30,10),
    insured_type character varying(20) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_separate_age_banded_rate OWNER TO postgres;

--
-- TOC entry 829 (class 1259 OID 890220)
-- Name: insurance_plan_simple_benefit_amount; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_simple_benefit_amount (
    employee_amount_type character varying(45) NOT NULL,
    employee_amount_flat_value numeric(30,10),
    employ_amount_multiply_value numeric(30,10),
    employee_min_amount_value numeric(30,10),
    employee_max_amount_value numeric(30,10),
    spouse_amount_value numeric(30,10),
    children_amount_value numeric(30,10),
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_simple_benefit_amount OWNER TO postgres;

--
-- TOC entry 830 (class 1259 OID 890223)
-- Name: insurance_plan_slider_limit; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_slider_limit (
    id numeric(20,0) NOT NULL,
    min_value numeric(30,10),
    max_value_in_percent numeric(30,10),
    step numeric(30,10),
    rounding character varying(45),
    insurance_limits_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    alert_limit numeric(30,10)
);


ALTER TABLE vaxiom.insurance_plan_slider_limit OWNER TO postgres;

--
-- TOC entry 831 (class 1259 OID 890226)
-- Name: insurance_plan_summary_fields; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_summary_fields (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    field_name character varying(150),
    value0 character varying(100),
    value1 character varying(100),
    value2 character varying(100),
    value3 character varying(100),
    item_order integer NOT NULL,
    is_visible boolean NOT NULL,
    ui_type character varying(45) NOT NULL,
    is_default boolean NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_plan_summary_fields OWNER TO postgres;

--
-- TOC entry 832 (class 1259 OID 890232)
-- Name: insurance_plan_summary_fields_default; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_summary_fields_default (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    field_name character varying(150),
    value0 character varying(100),
    value1 character varying(100),
    value2 character varying(100),
    value3 character varying(100),
    item_order integer NOT NULL,
    is_visible boolean,
    ui_type character varying(45),
    insurance_plan_type character varying(45)
);


ALTER TABLE vaxiom.insurance_plan_summary_fields_default OWNER TO postgres;

--
-- TOC entry 833 (class 1259 OID 890238)
-- Name: insurance_plan_term_disability_benefit_amount; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_plan_term_disability_benefit_amount (
    id numeric(20,0) NOT NULL,
    annual_earning_percent numeric(3,0),
    term_disability_benefit_option character varying(45),
    min_amount_value numeric(30,10),
    increment_amount numeric(30,10),
    benefit_dration character varying(150),
    max_amount_value numeric(30,10)
);


ALTER TABLE vaxiom.insurance_plan_term_disability_benefit_amount OWNER TO postgres;

--
-- TOC entry 834 (class 1259 OID 890241)
-- Name: insurance_subscriptions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insurance_subscriptions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    insurance_plan_id numeric(20,0),
    employer_id numeric(20,0),
    notes character varying(255),
    member_id character varying(255),
    active boolean DEFAULT false NOT NULL,
    enrollment_date date,
    person_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insurance_subscriptions OWNER TO postgres;

--
-- TOC entry 835 (class 1259 OID 890249)
-- Name: insured; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insured (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    notes character varying(255),
    insurance_subscription_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.insured OWNER TO postgres;

--
-- TOC entry 836 (class 1259 OID 890253)
-- Name: insured_benefit_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.insured_benefit_values (
    id numeric(20,0) NOT NULL,
    pre_tax_rate numeric(30,10),
    post_tax_rate numeric(30,10),
    employer_rate numeric(30,10),
    total_rate numeric(30,10),
    benefit_amout numeric(30,10),
    employee_insured_id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    qle_parent numeric(20,0),
    reduction numeric(30,10),
    reduced_benefit_amount numeric(30,10)
);


ALTER TABLE vaxiom.insured_benefit_values OWNER TO postgres;

--
-- TOC entry 837 (class 1259 OID 890256)
-- Name: invitations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.invitations (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    event character varying(255),
    invitee numeric(20,0) NOT NULL,
    inviter numeric(20,0) NOT NULL,
    updated boolean
);


ALTER TABLE vaxiom.invitations OWNER TO postgres;

--
-- TOC entry 838 (class 1259 OID 890259)
-- Name: invoice_export_queue; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.invoice_export_queue (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    target character varying(20),
    invoice_id numeric(20,0) NOT NULL,
    account_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.invoice_export_queue OWNER TO postgres;

--
-- TOC entry 839 (class 1259 OID 890262)
-- Name: invoice_import_export_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.invoice_import_export_log (
    id numeric(20,0) NOT NULL,
    type character varying(20),
    account_id numeric(20,0) NOT NULL,
    invoice_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.invoice_import_export_log OWNER TO postgres;

--
-- TOC entry 841 (class 1259 OID 890268)
-- Name: jobtitle; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.jobtitle (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(60),
    sys_organizations_id numeric(20,0) NOT NULL,
    jobtitle_group_id numeric(20,0) NOT NULL,
    mediacal_provider boolean NOT NULL
);


ALTER TABLE vaxiom.jobtitle OWNER TO postgres;

--
-- TOC entry 842 (class 1259 OID 890271)
-- Name: jobtitle_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.jobtitle_group (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(60),
    eeo_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.jobtitle_group OWNER TO postgres;

--
-- TOC entry 843 (class 1259 OID 890274)
-- Name: late_fee_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.late_fee_charges (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.late_fee_charges OWNER TO postgres;

--
-- TOC entry 844 (class 1259 OID 890277)
-- Name: legal_entities_participating_enrollment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entities_participating_enrollment (
    legal_entity_id numeric(20,0) NOT NULL,
    enrollment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entities_participating_enrollment OWNER TO postgres;

--
-- TOC entry 845 (class 1259 OID 890280)
-- Name: legal_entity; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0),
    parent_id numeric(20,0),
    relationship character varying(45) NOT NULL,
    structure_type character varying(45),
    structure_subtype character varying(45),
    legal_name character varying(255),
    sic_code character varying(10),
    naics2012_code character varying(20),
    fein character(10),
    county_id numeric(20,0),
    state character varying(25),
    status character varying(20) DEFAULT 'DRAFT'::character varying NOT NULL,
    has_employees boolean DEFAULT true NOT NULL,
    exp_empl_agricultural integer,
    exp_empl_household integer,
    exp_empl_other integer,
    biling_groups_setup character varying(45) DEFAULT 'DEPARTMENT'::character varying,
    available_payroll_cycle character varying(100),
    allow_new_hires boolean DEFAULT true
);


ALTER TABLE vaxiom.legal_entity OWNER TO postgres;

--
-- TOC entry 846 (class 1259 OID 890290)
-- Name: legal_entity_address; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_address (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(45),
    street character varying(45),
    city character varying(45),
    state character varying(45),
    zip_code character varying(45),
    occupacy character varying(45),
    legal_entity_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_address OWNER TO postgres;

--
-- TOC entry 847 (class 1259 OID 890293)
-- Name: legal_entity_address_attachment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_address_attachment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(45) NOT NULL,
    file_id character varying(127),
    file_mime_type character varying(255),
    file_name character varying(127),
    file_size numeric(20,0),
    legal_entity_address_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_address_attachment OWNER TO postgres;

--
-- TOC entry 848 (class 1259 OID 890299)
-- Name: legal_entity_billing_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_billing_group (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    legal_entity_id numeric(20,0) NOT NULL,
    name character varying(45) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_billing_group OWNER TO postgres;

--
-- TOC entry 849 (class 1259 OID 890302)
-- Name: legal_entity_billing_group_has_sys_organizations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_billing_group_has_sys_organizations (
    legal_entity_billing_group_id numeric(20,0) NOT NULL,
    sys_organizations_id numeric(20,0) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.legal_entity_billing_group_has_sys_organizations OWNER TO postgres;

--
-- TOC entry 850 (class 1259 OID 890305)
-- Name: legal_entity_dba; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_dba (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    legal_entity_id numeric(20,0) NOT NULL,
    dba_name character varying(100) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_dba OWNER TO postgres;

--
-- TOC entry 851 (class 1259 OID 890308)
-- Name: legal_entity_insurance_benefit; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_insurance_benefit (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    legal_entity_id numeric(20,0) NOT NULL,
    benefits_holder numeric(20,0) NOT NULL,
    broker_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_insurance_benefit OWNER TO postgres;

--
-- TOC entry 852 (class 1259 OID 890311)
-- Name: legal_entity_insurance_benefit_has_division; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_insurance_benefit_has_division (
    legal_entity_insurance_benefit_id numeric(20,0) NOT NULL,
    division_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_insurance_benefit_has_division OWNER TO postgres;

--
-- TOC entry 853 (class 1259 OID 890314)
-- Name: legal_entity_npi; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_npi (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    name character varying(100) NOT NULL,
    description character varying(200),
    legal_entity_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_npi OWNER TO postgres;

--
-- TOC entry 854 (class 1259 OID 890317)
-- Name: legal_entity_operational_structures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_operational_structures (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    is_hr boolean NOT NULL,
    hr_type character varying(45),
    is_ib boolean NOT NULL,
    is_pm boolean NOT NULL,
    is_acc boolean NOT NULL,
    legal_entity_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_operational_structures OWNER TO postgres;

--
-- TOC entry 9051 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.is_hr; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.is_hr IS 'human resources';


--
-- TOC entry 9052 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.hr_type; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.hr_type IS 'human resources type (basic, enhanced)';


--
-- TOC entry 9053 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.is_ib; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.is_ib IS 'insurance benefits';


--
-- TOC entry 9054 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.is_pm; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.is_pm IS 'practice management';


--
-- TOC entry 9055 (class 0 OID 0)
-- Dependencies: 854
-- Name: COLUMN legal_entity_operational_structures.is_acc; Type: COMMENT; Schema: vaxiom; Owner: postgres
--

COMMENT ON COLUMN vaxiom.legal_entity_operational_structures.is_acc IS 'accounting';


--
-- TOC entry 855 (class 1259 OID 890320)
-- Name: legal_entity_owner; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_owner (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    legal_entity_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL,
    name character varying(45) NOT NULL,
    name_2 character varying(45),
    fein character(10),
    ssn character(11),
    birth_date date,
    percentage numeric(8,5) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_owner OWNER TO postgres;

--
-- TOC entry 856 (class 1259 OID 890323)
-- Name: legal_entity_tin; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.legal_entity_tin (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    name character varying(20) NOT NULL,
    description character varying(20),
    legal_entity_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.legal_entity_tin OWNER TO postgres;

--
-- TOC entry 857 (class 1259 OID 890326)
-- Name: lightbarcalls; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.lightbarcalls (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    calling_since timestamp without time zone,
    urgent boolean NOT NULL,
    booking_id numeric(20,0),
    chair_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.lightbarcalls OWNER TO postgres;

--
-- TOC entry 858 (class 1259 OID 890329)
-- Name: location_access_keys; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.location_access_keys (
    id numeric(20,0) NOT NULL,
    sys_created timestamp without time zone,
    sys_created_by numeric(20,0),
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0) DEFAULT '0'::numeric,
    organization_id numeric(20,0) NOT NULL,
    access_name character varying(100),
    access_key integer NOT NULL,
    enabled boolean DEFAULT false,
    accepts_medicaid boolean DEFAULT false
);


ALTER TABLE vaxiom.location_access_keys OWNER TO postgres;

--
-- TOC entry 859 (class 1259 OID 890335)
-- Name: medical_history; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.medical_history (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    abnormal_breathing boolean,
    additional_info character varying(255),
    aids boolean,
    aids_alert boolean,
    arthritis boolean,
    arthritis_alert boolean,
    asthma boolean,
    asthma_alert boolean,
    blood_disorders boolean,
    blood_disorders_alert boolean,
    brain_injury boolean,
    brain_injury_alert boolean,
    clicking_pain_jaw boolean,
    clicking_pain_jaw_alert boolean,
    diabetes boolean,
    diabetes_alert boolean,
    dizziness_fainting boolean,
    dizziness_fainting_alert boolean,
    epilepsy boolean,
    epilepsy_alert boolean,
    family_member_same_condition character varying(20),
    family_member_same_condition_alert boolean,
    first_tooth_age character varying(255),
    frequent_colds boolean,
    frequent_colds_alert boolean,
    grinding_teeth boolean,
    hearing_difficulties boolean,
    hearing_difficulties_alert boolean,
    heart_trouble boolean,
    heart_trouble_alert boolean,
    hepatitis boolean,
    hepatitis_alert boolean,
    in_good_health boolean,
    in_good_health_alert boolean,
    kidney_liver_disease boolean,
    kidney_liver_disease_alert boolean,
    last_dental_care date,
    lip_biting boolean,
    mouth_breathing boolean,
    nail_biting boolean,
    other_conditions character varying(255),
    other_conditions_alert boolean,
    parents_teeth_removed_crowding boolean,
    parents_teeth_removed_crowding_alert boolean,
    rheumatic_fever boolean,
    rheumatic_fever_alert boolean,
    smoking boolean,
    tobacco boolean DEFAULT false,
    speech_disorders boolean,
    tb boolean,
    tb_alert boolean,
    thumb_sucking boolean,
    tongue_biting boolean,
    tongue_sucking boolean,
    tongue_thrusting boolean,
    under_care_physician character varying(255),
    under_care_physician_alert boolean,
    unfavourable_reactions boolean,
    unfavourable_reactions_alert boolean,
    has_general_dentist boolean DEFAULT false,
    general_dentist_first_name character varying(255),
    general_dentist_phone character varying(100),
    week_floss character varying(255),
    day_brush integer DEFAULT 0,
    toothbrush character varying(100),
    birth_control boolean DEFAULT false,
    birth_control_alert boolean DEFAULT false,
    pregnant boolean DEFAULT false,
    pregnant_alert boolean DEFAULT false,
    pregnancy_week integer DEFAULT 0,
    nursing boolean DEFAULT false,
    nursing_alert boolean DEFAULT false,
    active_dental_insurance boolean DEFAULT false,
    secondary_insurance_card boolean DEFAULT false,
    dental_health character varying(100),
    like_smile boolean DEFAULT false,
    gums boolean DEFAULT false,
    gums_alert boolean DEFAULT false,
    seizures boolean DEFAULT false,
    seizures_alert boolean DEFAULT false,
    antibiotics boolean DEFAULT false,
    antibiotics_alert boolean DEFAULT false,
    problems_previous_procedures boolean DEFAULT false,
    problems_previous_procedures_alert boolean DEFAULT false,
    personal_physician_last_visit timestamp without time zone,
    periodontal_disease boolean DEFAULT false,
    periodontal_disease_alert boolean DEFAULT false,
    general_health character varying(100),
    current_in_pain boolean DEFAULT false,
    clicking_pain boolean DEFAULT false,
    under_care_physician_phone character varying(100),
    physician_active_care boolean DEFAULT false,
    add_or_adhd boolean DEFAULT false,
    add_or_adhd_alert boolean DEFAULT false,
    anemia boolean DEFAULT false,
    anemia_alert boolean DEFAULT false,
    artificial_bones_or_joints_or_valves boolean DEFAULT false,
    artificial_bones_or_joints_or_valves_alert boolean DEFAULT false,
    autism_or_sensory boolean DEFAULT false,
    autism_or_sensory_alert boolean DEFAULT false,
    cancer boolean DEFAULT false,
    cancer_alert boolean DEFAULT false,
    heart_congenital_defect boolean DEFAULT false,
    heart_congenital_defect_alert boolean DEFAULT false,
    drug_alcohol_abuse boolean DEFAULT false,
    drug_alcohol_abuse_alert boolean DEFAULT false,
    emphysema boolean DEFAULT false,
    emphysema_alert boolean DEFAULT false,
    herpes boolean DEFAULT false,
    herpes_alert boolean DEFAULT false,
    frequently_tired boolean DEFAULT false,
    frequently_tired_alert boolean DEFAULT false,
    glaucoma boolean DEFAULT false,
    glaucoma_alert boolean DEFAULT false,
    handicap_or_disabilities boolean DEFAULT false,
    handicap_or_disabilities_alert boolean DEFAULT false,
    hearing_impairment boolean DEFAULT false,
    hearing_impairment_alert boolean DEFAULT false,
    heart_attack boolean DEFAULT false,
    heart_attack_alert boolean DEFAULT false,
    heart_murmur boolean DEFAULT false,
    heart_murmur_alert boolean DEFAULT false,
    heart_surgery boolean DEFAULT false,
    heart_surgery_alert boolean DEFAULT false,
    hemophilia_or_abnormal_bleeding boolean DEFAULT false,
    low_blood_pressure_alert boolean DEFAULT false,
    hemophilia_or_abnormal_bleeding_alert boolean DEFAULT false,
    high_blood_pressure boolean DEFAULT false,
    high_blood_pressure_alert boolean DEFAULT false,
    low_blood_pressure boolean DEFAULT false,
    kidney_diseases boolean DEFAULT false,
    kidney_diseases_alert boolean DEFAULT false,
    mitral_valve_prolapse boolean DEFAULT false,
    mitral_valve_prolapse_alert boolean DEFAULT false,
    psychiatric_problem boolean DEFAULT false,
    psychiatric_problem_alert boolean DEFAULT false,
    radiation_therapy boolean DEFAULT false,
    radiation_therapy_alert boolean DEFAULT false,
    shingles boolean DEFAULT false,
    shingles_alert boolean DEFAULT false,
    sickle_cell_disease boolean DEFAULT false,
    sickle_cell_disease_alert boolean DEFAULT false,
    stomach_troubles_or_ulcers boolean DEFAULT false,
    stomach_troubles_or_ulcers_alert boolean DEFAULT false,
    stroke boolean DEFAULT false,
    stroke_alert boolean DEFAULT false,
    thyroid_problem boolean DEFAULT false,
    thyroid_problem_alert boolean DEFAULT false,
    tuberculosis boolean DEFAULT false,
    tuberculosis_alert boolean DEFAULT false,
    cancer_medication boolean DEFAULT false,
    cancer_medication_alert boolean DEFAULT false,
    respiratory_problems boolean DEFAULT false,
    respiratory_problems_alert boolean DEFAULT false,
    patient_id numeric(20,0) NOT NULL,
    created_from character varying(45) DEFAULT 'CORE'::character varying,
    chief_complaint character varying(100)
);


ALTER TABLE vaxiom.medical_history OWNER TO postgres;

--
-- TOC entry 860 (class 1259 OID 890430)
-- Name: medical_history_medications; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.medical_history_medications (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    alert boolean NOT NULL,
    relation_type character varying(20) NOT NULL,
    medication_id numeric(20,0) NOT NULL,
    medical_history_id numeric(20,0)
);


ALTER TABLE vaxiom.medical_history_medications OWNER TO postgres;

--
-- TOC entry 862 (class 1259 OID 890435)
-- Name: medical_history_patient_relatives_crowding; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.medical_history_patient_relatives_crowding (
    id bigint NOT NULL,
    medical_history_id numeric(20,0),
    patient_relative_crowding character varying(50),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.medical_history_patient_relatives_crowding OWNER TO postgres;

--
-- TOC entry 861 (class 1259 OID 890433)
-- Name: medical_history_patient_relatives_crowding_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.medical_history_patient_relatives_crowding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.medical_history_patient_relatives_crowding_id_seq OWNER TO postgres;

--
-- TOC entry 9056 (class 0 OID 0)
-- Dependencies: 861
-- Name: medical_history_patient_relatives_crowding_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.medical_history_patient_relatives_crowding_id_seq OWNED BY vaxiom.medical_history_patient_relatives_crowding.id;


--
-- TOC entry 863 (class 1259 OID 890439)
-- Name: medications; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.medications (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255),
    deleted bigint DEFAULT '0'::bigint
);


ALTER TABLE vaxiom.medications OWNER TO postgres;

--
-- TOC entry 864 (class 1259 OID 890443)
-- Name: merchant_reports; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.merchant_reports (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    location_id numeric(20,0) NOT NULL,
    check_date date NOT NULL,
    run_date timestamp without time zone,
    success boolean NOT NULL,
    note character varying(1024)
);


ALTER TABLE vaxiom.merchant_reports OWNER TO postgres;

--
-- TOC entry 865 (class 1259 OID 890449)
-- Name: migrated_answers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_answers (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    answer_id numeric(20,0),
    answer_guid character varying(45),
    question_guid character varying(45),
    answer character varying(50)
);


ALTER TABLE vaxiom.migrated_answers OWNER TO postgres;

--
-- TOC entry 867 (class 1259 OID 890455)
-- Name: migrated_appointment_fields; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_appointment_fields (
    appointment_id numeric(20,0) NOT NULL,
    fields_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_appointment_fields OWNER TO postgres;

--
-- TOC entry 868 (class 1259 OID 890458)
-- Name: migrated_appointment_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_appointment_types (
    appointment_type_id numeric(20,0) NOT NULL,
    appointment_type_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_appointment_types OWNER TO postgres;

--
-- TOC entry 866 (class 1259 OID 890452)
-- Name: migrated_appointments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_appointments (
    appointment_id numeric(20,0) NOT NULL,
    appointment_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_appointments OWNER TO postgres;

--
-- TOC entry 869 (class 1259 OID 890461)
-- Name: migrated_chairs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_chairs (
    chair_id numeric(20,0) NOT NULL,
    chair_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_chairs OWNER TO postgres;

--
-- TOC entry 870 (class 1259 OID 890464)
-- Name: migrated_claim; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_claim (
    claim_id numeric(20,0),
    claim_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_claim OWNER TO postgres;

--
-- TOC entry 871 (class 1259 OID 890467)
-- Name: migrated_claim_help; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_claim_help (
    db character varying(8),
    pricestring character varying(50),
    guid character(36) NOT NULL,
    status character varying(12) NOT NULL,
    date date,
    type character varying(50),
    total numeric(30,10),
    patguid character(36)
);


ALTER TABLE vaxiom.migrated_claim_help OWNER TO postgres;

--
-- TOC entry 872 (class 1259 OID 890470)
-- Name: migrated_employees; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_employees (
    employee_id numeric(20,0) NOT NULL,
    employee_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_employees OWNER TO postgres;

--
-- TOC entry 873 (class 1259 OID 890473)
-- Name: migrated_insurance_companies; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_insurance_companies (
    company_id numeric(20,0) NOT NULL,
    company_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_insurance_companies OWNER TO postgres;

--
-- TOC entry 874 (class 1259 OID 890476)
-- Name: migrated_insurance_plans; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_insurance_plans (
    plan_id numeric(20,0) NOT NULL,
    plan_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_insurance_plans OWNER TO postgres;

--
-- TOC entry 875 (class 1259 OID 890479)
-- Name: migrated_locations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_locations (
    location_id numeric(20,0),
    location_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_locations OWNER TO postgres;

--
-- TOC entry 878 (class 1259 OID 890491)
-- Name: migrated_patient_answers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patient_answers (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    patient_answer_id numeric(20,0),
    patient_answer_guid character varying(45),
    patient_guid character varying(45),
    answer_guid character varying(45),
    questionnaire_guid character varying(45),
    question_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_patient_answers OWNER TO postgres;

--
-- TOC entry 879 (class 1259 OID 890494)
-- Name: migrated_patient_documents; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patient_documents (
    id numeric(20,0) NOT NULL,
    person_guid character varying(45) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.migrated_patient_documents OWNER TO postgres;

--
-- TOC entry 880 (class 1259 OID 890497)
-- Name: migrated_patient_image_series; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patient_image_series (
    id numeric(20,0) NOT NULL,
    image_series_id numeric(20,0) NOT NULL,
    image_series_guid character varying(45) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.migrated_patient_image_series OWNER TO postgres;

--
-- TOC entry 876 (class 1259 OID 890482)
-- Name: migrated_patients; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patients (
    patient_id numeric(20,0) NOT NULL,
    patient_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_patients OWNER TO postgres;

--
-- TOC entry 877 (class 1259 OID 890485)
-- Name: migrated_patients_comments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_patients_comments (
    id numeric(20,0) NOT NULL,
    comment_id numeric(20,0),
    comment_guid character varying(45),
    note character varying(2048),
    type character varying(50),
    patient_guid character varying(45),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.migrated_patients_comments OWNER TO postgres;

--
-- TOC entry 881 (class 1259 OID 890500)
-- Name: migrated_payment_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_payment_accounts (
    payment_id numeric(20,0) NOT NULL,
    payment_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_payment_accounts OWNER TO postgres;

--
-- TOC entry 882 (class 1259 OID 890503)
-- Name: migrated_persons; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_persons (
    person_id numeric(20,0) NOT NULL,
    person_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_persons OWNER TO postgres;

--
-- TOC entry 883 (class 1259 OID 890506)
-- Name: migrated_questionnaire; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_questionnaire (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    questionnaire_id numeric(20,0),
    questionnaire_guid character varying(45),
    patient_guid character varying(45),
    questionnaire_name character varying(50),
    date timestamp without time zone,
    tx_card_id numeric(20,0)
);


ALTER TABLE vaxiom.migrated_questionnaire OWNER TO postgres;

--
-- TOC entry 884 (class 1259 OID 890509)
-- Name: migrated_questions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_questions (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    question_id numeric(20,0),
    question_guid character varying(45),
    patient_guid character varying(45),
    questionnaire_guid character varying(45),
    question character varying(50),
    question_type character varying(45)
);


ALTER TABLE vaxiom.migrated_questions OWNER TO postgres;

--
-- TOC entry 885 (class 1259 OID 890512)
-- Name: migrated_relations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_relations (
    relation_id numeric(20,0) NOT NULL,
    relation_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_relations OWNER TO postgres;

--
-- TOC entry 886 (class 1259 OID 890515)
-- Name: migrated_schedule_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_schedule_templates (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    template_id numeric(20,0),
    template_guid character varying(45),
    location_name character varying(50),
    minutes_per_row integer,
    location_guid character varying(45),
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    template_order integer,
    time_zone character varying(500),
    color integer
);


ALTER TABLE vaxiom.migrated_schedule_templates OWNER TO postgres;

--
-- TOC entry 887 (class 1259 OID 890521)
-- Name: migrated_transactions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_transactions (
    transaction_id numeric(20,0) NOT NULL,
    transaction_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_transactions OWNER TO postgres;

--
-- TOC entry 888 (class 1259 OID 890524)
-- Name: migrated_treatment_notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_treatment_notes (
    note_id numeric(20,0) NOT NULL,
    note_guid character varying(45)
);


ALTER TABLE vaxiom.migrated_treatment_notes OWNER TO postgres;

--
-- TOC entry 889 (class 1259 OID 890527)
-- Name: migrated_txcards; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_txcards (
    txcard_id numeric(20,0) NOT NULL,
    txcard_guid character varying(45) NOT NULL
);


ALTER TABLE vaxiom.migrated_txcards OWNER TO postgres;

--
-- TOC entry 890 (class 1259 OID 890530)
-- Name: migrated_workschedule; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migrated_workschedule (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    workschedule_id numeric(20,0),
    workschedule_guid character varying(80),
    template_guid character varying(45),
    location_name character varying(50),
    chair_name character varying(50),
    minutes_per_row integer
);


ALTER TABLE vaxiom.migrated_workschedule OWNER TO postgres;

--
-- TOC entry 891 (class 1259 OID 890533)
-- Name: migration_appointment_employee_mapping; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migration_appointment_employee_mapping (
    appointment_source_key character varying(20),
    provider_source_key character varying(20),
    assistant_source_key character varying(20),
    appointment_target_key character varying(20),
    provider_target_key character varying(20),
    assistant_target_key character varying(20)
);


ALTER TABLE vaxiom.migration_appointment_employee_mapping OWNER TO postgres;

--
-- TOC entry 892 (class 1259 OID 890536)
-- Name: migration_fix_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migration_fix_adjustments (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.migration_fix_adjustments OWNER TO postgres;

--
-- TOC entry 894 (class 1259 OID 890541)
-- Name: migration_map; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.migration_map (
    id bigint NOT NULL,
    created timestamp without time zone,
    source_key character varying(256),
    source_type character varying(64) NOT NULL,
    source_name character varying(64) NOT NULL,
    target_id numeric(20,0) NOT NULL,
    target_table character varying(64) NOT NULL,
    merged_to_id numeric(20,0),
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.migration_map OWNER TO postgres;

--
-- TOC entry 893 (class 1259 OID 890539)
-- Name: migration_map_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.migration_map_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.migration_map_id_seq OWNER TO postgres;

--
-- TOC entry 9057 (class 0 OID 0)
-- Dependencies: 893
-- Name: migration_map_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.migration_map_id_seq OWNED BY vaxiom.migration_map.id;


--
-- TOC entry 896 (class 1259 OID 890551)
-- Name: misc_fee_charge_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.misc_fee_charge_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    name character varying(255),
    price numeric(30,10) NOT NULL,
    affecting_production_report boolean DEFAULT true NOT NULL
);


ALTER TABLE vaxiom.misc_fee_charge_templates OWNER TO postgres;

--
-- TOC entry 895 (class 1259 OID 890546)
-- Name: misc_fee_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.misc_fee_charges (
    id numeric(20,0) NOT NULL,
    name character varying(255),
    included_in_treatment_fee boolean DEFAULT false NOT NULL,
    affecting_production_report boolean DEFAULT true NOT NULL
);


ALTER TABLE vaxiom.misc_fee_charges OWNER TO postgres;

--
-- TOC entry 897 (class 1259 OID 890555)
-- Name: my_reports; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.my_reports (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255)
);


ALTER TABLE vaxiom.my_reports OWNER TO postgres;

--
-- TOC entry 898 (class 1259 OID 890558)
-- Name: my_reports_columns; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.my_reports_columns (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    column_name character varying(255),
    report_id numeric(20,0) NOT NULL,
    sort_direction character varying(4),
    sort_order integer,
    group_order integer,
    column_order integer
);


ALTER TABLE vaxiom.my_reports_columns OWNER TO postgres;

--
-- TOC entry 899 (class 1259 OID 890561)
-- Name: my_reports_filters; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.my_reports_filters (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(255),
    column_name character varying(255),
    report_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.my_reports_filters OWNER TO postgres;

--
-- TOC entry 900 (class 1259 OID 890567)
-- Name: network_fee_sheets; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.network_fee_sheets (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    valid_from date,
    valid_to date,
    organization_id numeric(20,0) NOT NULL,
    employment_contract_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.network_fee_sheets OWNER TO postgres;

--
-- TOC entry 901 (class 1259 OID 890570)
-- Name: network_sheet_fee; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.network_sheet_fee (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    insurance_code_id numeric(20,0),
    network_fee_sheet_id numeric(20,0),
    fee numeric(30,10)
);


ALTER TABLE vaxiom.network_sheet_fee OWNER TO postgres;

--
-- TOC entry 903 (class 1259 OID 890576)
-- Name: notebook_notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.notebook_notes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    color character varying(255),
    height integer,
    x integer,
    y integer,
    content text NOT NULL,
    title character varying(255) NOT NULL,
    width integer,
    sys_zindex integer,
    notebook_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.notebook_notes OWNER TO postgres;

--
-- TOC entry 902 (class 1259 OID 890573)
-- Name: notebooks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.notebooks (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    owner numeric(20,0) NOT NULL,
    title character varying(255) NOT NULL
);


ALTER TABLE vaxiom.notebooks OWNER TO postgres;

--
-- TOC entry 904 (class 1259 OID 890582)
-- Name: notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.notes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    author numeric(20,0),
    org_id numeric(20,0),
    ref_id numeric(20,0),
    target_id numeric(20,0) NOT NULL,
    target_type character varying(255) NOT NULL,
    note character varying(6000) NOT NULL,
    description character varying(255),
    alert boolean
);


ALTER TABLE vaxiom.notes OWNER TO postgres;

--
-- TOC entry 907 (class 1259 OID 890596)
-- Name: od_chair_break_hours; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.od_chair_break_hours (
    od_chair_id numeric(20,0) NOT NULL,
    end_min integer NOT NULL,
    start_min integer NOT NULL
);


ALTER TABLE vaxiom.od_chair_break_hours OWNER TO postgres;

--
-- TOC entry 906 (class 1259 OID 890592)
-- Name: odt_chair_allocations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.odt_chair_allocations (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    odt_chair_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    primary_resource boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.odt_chair_allocations OWNER TO postgres;

--
-- TOC entry 905 (class 1259 OID 890588)
-- Name: odt_chairs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.odt_chairs (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    valid_from timestamp without time zone,
    template_id numeric(20,0) NOT NULL,
    day_schedule_id numeric(20,0),
    chair_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.odt_chairs OWNER TO postgres;

--
-- TOC entry 910 (class 1259 OID 890614)
-- Name: office_day_chairs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.office_day_chairs (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    odt_chair_id numeric(20,0),
    office_day_id numeric(20,0) NOT NULL,
    chair_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.office_day_chairs OWNER TO postgres;

--
-- TOC entry 911 (class 1259 OID 890617)
-- Name: office_day_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.office_day_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    location_id numeric(20,0) NOT NULL,
    name character varying(255) NOT NULL,
    color character varying(255) NOT NULL
);


ALTER TABLE vaxiom.office_day_templates OWNER TO postgres;

--
-- TOC entry 909 (class 1259 OID 890607)
-- Name: office_days; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.office_days (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    location_id numeric(20,0) NOT NULL,
    template_id numeric(20,0) NOT NULL,
    o_date date NOT NULL,
    notes character varying(1024)
);


ALTER TABLE vaxiom.office_days OWNER TO postgres;

--
-- TOC entry 908 (class 1259 OID 890599)
-- Name: offices; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.offices (
    city character varying(255) DEFAULT ''::character varying NOT NULL,
    state character varying(255) DEFAULT ''::character varying NOT NULL,
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.offices OWNER TO postgres;

--
-- TOC entry 912 (class 1259 OID 890624)
-- Name: ortho_coverages; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.ortho_coverages (
    id numeric(20,0) NOT NULL,
    max_lifetime_coverage numeric(30,10),
    deductible_amount numeric(30,10),
    reimbursement_percentage numeric(3,2),
    downpayment_percentage numeric(3,2),
    dependent_age_limit integer,
    age_limit_cutoff character varying(255),
    waiting_period integer,
    cob_type character varying(255),
    inprogress boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.ortho_coverages OWNER TO postgres;

--
-- TOC entry 913 (class 1259 OID 890631)
-- Name: ortho_insured; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.ortho_insured (
    id numeric(20,0) NOT NULL,
    used_lifetime_coverage numeric(30,10),
    year_deductible_paid_last integer
);


ALTER TABLE vaxiom.ortho_insured OWNER TO postgres;

--
-- TOC entry 914 (class 1259 OID 890634)
-- Name: other_professional_relationships; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.other_professional_relationships (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    person_id numeric(20,0) NOT NULL,
    external_office_id numeric(20,0) NOT NULL,
    employee_type_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.other_professional_relationships OWNER TO postgres;

--
-- TOC entry 916 (class 1259 OID 890644)
-- Name: patient_images; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_images (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    imageseries_id numeric(20,0) NOT NULL,
    file_key character varying(255) NOT NULL,
    type_key character varying(255),
    slot_key character varying(255),
    file_written timestamp without time zone
);


ALTER TABLE vaxiom.patient_images OWNER TO postgres;

--
-- TOC entry 918 (class 1259 OID 890654)
-- Name: patient_images_layouts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_images_layouts (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_node_id numeric(20,0) NOT NULL,
    xml_content bytea NOT NULL
);


ALTER TABLE vaxiom.patient_images_layouts OWNER TO postgres;

--
-- TOC entry 919 (class 1259 OID 890660)
-- Name: patient_images_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_images_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    xml_content bytea NOT NULL
);


ALTER TABLE vaxiom.patient_images_types OWNER TO postgres;

--
-- TOC entry 917 (class 1259 OID 890650)
-- Name: patient_imageseries; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_imageseries (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    treatment_id numeric(20,0),
    appointment_id numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    name character varying(255),
    series_date timestamp without time zone,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.patient_imageseries OWNER TO postgres;

--
-- TOC entry 920 (class 1259 OID 890666)
-- Name: patient_insurance_plans; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_insurance_plans (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    master_id numeric(20,0),
    insurance_company_id numeric(20,0),
    insurance_billing_center_id numeric(20,0),
    number character varying(255),
    name character varying(255),
    description character varying(255)
);


ALTER TABLE vaxiom.patient_insurance_plans OWNER TO postgres;

--
-- TOC entry 921 (class 1259 OID 890672)
-- Name: patient_ledger_history; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_ledger_history (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    history_date date NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    tx_location_id numeric(20,0) NOT NULL,
    payment_account_type character varying(50) NOT NULL,
    medicaid boolean NOT NULL,
    payer character varying(255),
    oldest_due_day numeric(20,0) NOT NULL,
    balance numeric(30,10) NOT NULL,
    due numeric(30,10),
    future_due numeric(30,10),
    overdue1 numeric(30,10),
    overdue30 numeric(30,10),
    overdue60 numeric(30,10),
    overdue90 numeric(30,10),
    overdue120 numeric(30,10),
    autopay_source character varying(50),
    payer_phones character varying(1024),
    payer_emails character varying(250),
    label_names character varying(1024),
    label_agencies character varying(1024)
);


ALTER TABLE vaxiom.patient_ledger_history OWNER TO postgres;

--
-- TOC entry 922 (class 1259 OID 890678)
-- Name: patient_ledger_history_overdue_receivables; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_ledger_history_overdue_receivables (
    receivable_id numeric(20,0) NOT NULL,
    patient_ledger_history_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.patient_ledger_history_overdue_receivables OWNER TO postgres;

--
-- TOC entry 923 (class 1259 OID 890681)
-- Name: patient_locations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_locations (
    patient_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.patient_locations OWNER TO postgres;

--
-- TOC entry 924 (class 1259 OID 890684)
-- Name: patient_person_referrals; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_person_referrals (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    person_id numeric(20,0) NOT NULL,
    person_type character varying(255) NOT NULL
);


ALTER TABLE vaxiom.patient_person_referrals OWNER TO postgres;

--
-- TOC entry 925 (class 1259 OID 890687)
-- Name: patient_recently_opened_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_recently_opened_items (
    id numeric(20,0) NOT NULL,
    patient_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.patient_recently_opened_items OWNER TO postgres;

--
-- TOC entry 926 (class 1259 OID 890690)
-- Name: patient_template_referrals; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patient_template_referrals (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    referral_template_id numeric(20,0) NOT NULL,
    text_value character varying(2000)
);


ALTER TABLE vaxiom.patient_template_referrals OWNER TO postgres;

--
-- TOC entry 915 (class 1259 OID 890637)
-- Name: patients; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.patients (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    person_id numeric(20,0) NOT NULL,
    notes character varying(8191),
    legacy_id character varying(255),
    heard_from_patient_id numeric(20,0),
    heard_from_professional_id numeric(20,0),
    heard_from_website character varying(255),
    heard_from_other character varying(255),
    image_file_key character varying(255),
    human_id numeric(20,0),
    primary_location_id numeric(20,0),
    not_generate_statement boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.patients OWNER TO postgres;

--
-- TOC entry 928 (class 1259 OID 890703)
-- Name: payment_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_accounts (
    id numeric(20,0) NOT NULL,
    account_type character varying(30) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    temp boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.payment_accounts OWNER TO postgres;

--
-- TOC entry 929 (class 1259 OID 890707)
-- Name: payment_correction_details; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_correction_details (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    transaction_id numeric(20,0) NOT NULL,
    note character varying(1024),
    correction_type_id numeric(20,0)
);


ALTER TABLE vaxiom.payment_correction_details OWNER TO postgres;

--
-- TOC entry 930 (class 1259 OID 890713)
-- Name: payment_options; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_options (
    id numeric(20,0) NOT NULL,
    name character varying(40) NOT NULL
);


ALTER TABLE vaxiom.payment_options OWNER TO postgres;

--
-- TOC entry 932 (class 1259 OID 890724)
-- Name: payment_plan_rollbacks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_plan_rollbacks (
    id numeric(20,0) NOT NULL,
    payment_plan_id numeric(20,0)
);


ALTER TABLE vaxiom.payment_plan_rollbacks OWNER TO postgres;

--
-- TOC entry 931 (class 1259 OID 890716)
-- Name: payment_plans; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_plans (
    id numeric(20,0) NOT NULL,
    payment_plan_type character varying(255),
    payment_plan_rollback_id numeric(20,0),
    installment_interval character varying(255),
    first_installment_date integer,
    second_installment_date integer,
    installment_amount numeric(30,10),
    first_due_date date NOT NULL,
    installment_locked boolean DEFAULT false NOT NULL,
    plan_length_locked boolean DEFAULT false NOT NULL,
    bank_account_id numeric(20,0),
    token character varying(255),
    signature bytea,
    autopay_source character varying(255),
    last_four_digits character varying(4),
    card_expiration character varying(4),
    invoice_contact_method_id numeric(20,0),
    token_location_id numeric(20,0),
    requested_downpayment_amount numeric(30,10),
    rollback_date timestamp without time zone
);


ALTER TABLE vaxiom.payment_plans OWNER TO postgres;

--
-- TOC entry 933 (class 1259 OID 890727)
-- Name: payment_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(50) NOT NULL,
    type integer NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    parent_company_id numeric(20,0)
);


ALTER TABLE vaxiom.payment_types OWNER TO postgres;

--
-- TOC entry 934 (class 1259 OID 890731)
-- Name: payment_types_locations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payment_types_locations (
    payment_type_id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.payment_types_locations OWNER TO postgres;

--
-- TOC entry 927 (class 1259 OID 890696)
-- Name: payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.payments (
    id numeric(20,0) NOT NULL,
    payment_type character varying(20) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    account_id numeric(20,0),
    amount numeric(30,10),
    signature bytea,
    transaction_id character varying(255),
    receivable_id numeric(20,0),
    payer_name character varying(255),
    check_number character varying(255),
    last_four_digits character varying(4),
    expire character varying(4),
    type_info character varying(255),
    human_id numeric(20,0),
    billing_address_id numeric(20,0),
    credit_card_data character varying(2048),
    applied_payment_id numeric(20,0),
    organization_id numeric(20,0),
    notes character varying(1024),
    patient_id numeric(20,0) NOT NULL,
    sys_created_at numeric(20,0),
    credit_manual boolean,
    payment_type_id numeric(20,0),
    insurance_company_payment_id numeric(20,0),
    migrated boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.payments OWNER TO postgres;

--
-- TOC entry 940 (class 1259 OID 890749)
-- Name: permission_user_role_location; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permission_user_role_location (
    permission_user_role_id numeric(20,0) NOT NULL,
    user_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.permission_user_role_location OWNER TO postgres;

--
-- TOC entry 935 (class 1259 OID 890734)
-- Name: permissions_permission; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_permission (
    id numeric(20,0) NOT NULL,
    name character varying(100),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0),
    permission_group_id numeric(20,0)
);


ALTER TABLE vaxiom.permissions_permission OWNER TO postgres;

--
-- TOC entry 936 (class 1259 OID 890737)
-- Name: permissions_user_role; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_user_role (
    id numeric(20,0) NOT NULL,
    name character varying(45) NOT NULL,
    sys_organizations_id numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.permissions_user_role OWNER TO postgres;

--
-- TOC entry 937 (class 1259 OID 890740)
-- Name: permissions_user_role_departament; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_user_role_departament (
    permissions_user_role_id numeric(20,0) NOT NULL,
    department_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.permissions_user_role_departament OWNER TO postgres;

--
-- TOC entry 938 (class 1259 OID 890743)
-- Name: permissions_user_role_job_title; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_user_role_job_title (
    permissions_user_role_id numeric(20,0) NOT NULL,
    job_title_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.permissions_user_role_job_title OWNER TO postgres;

--
-- TOC entry 939 (class 1259 OID 890746)
-- Name: permissions_user_role_permission; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.permissions_user_role_permission (
    id numeric(20,0) NOT NULL,
    permissions_user_role_id numeric(20,0) NOT NULL,
    permissions_permission_id numeric(20,0) NOT NULL,
    access_type character varying(30) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0)
);


ALTER TABLE vaxiom.permissions_user_role_permission OWNER TO postgres;

--
-- TOC entry 942 (class 1259 OID 890759)
-- Name: person_image_files; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.person_image_files (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    full_size character varying(255) NOT NULL,
    mime_type character varying(255),
    thumbnail character varying(255) NOT NULL,
    person_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.person_image_files OWNER TO postgres;

--
-- TOC entry 943 (class 1259 OID 890765)
-- Name: person_payment_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.person_payment_accounts (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    payer_person_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.person_payment_accounts OWNER TO postgres;

--
-- TOC entry 941 (class 1259 OID 890752)
-- Name: persons; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.persons (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    birth_date date,
    first_name character varying(255) NOT NULL,
    gender character varying(11) NOT NULL,
    greeting character varying(255),
    last_name character varying(255) NOT NULL,
    middle_name character varying(255),
    search_name character varying(255),
    prefix character varying(255),
    suffix character varying(255),
    preferred_means_of_contact character varying(10),
    ssn character(11),
    no_ssn_reason character varying(45),
    id_number character varying(100),
    title character varying(10),
    core_user_id numeric(20,0),
    portal_user_id numeric(20,0),
    school character varying(255),
    marital_status character varying(255),
    organization_id numeric(20,0) NOT NULL,
    global_person_id numeric(20,0),
    patient_portal_user_id numeric(20,0),
    heard_about_us_from character varying(256),
    created_from character varying(45) DEFAULT 'CORE'::character varying
);


ALTER TABLE vaxiom.persons OWNER TO postgres;

--
-- TOC entry 944 (class 1259 OID 890768)
-- Name: previous_enrollment_dependents; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.previous_enrollment_dependents (
    previous_enrollment_id numeric(20,0) NOT NULL,
    dependent_data text
);


ALTER TABLE vaxiom.previous_enrollment_dependents OWNER TO postgres;

--
-- TOC entry 946 (class 1259 OID 890776)
-- Name: previous_enrollment_form; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.previous_enrollment_form (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    employee_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.previous_enrollment_form OWNER TO postgres;

--
-- TOC entry 948 (class 1259 OID 890782)
-- Name: previous_enrollment_form_benefits; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.previous_enrollment_form_benefits (
    id bigint NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    enrollment_form_id numeric(20,0),
    benefit_type character varying(30),
    benefit_data text
);


ALTER TABLE vaxiom.previous_enrollment_form_benefits OWNER TO postgres;

--
-- TOC entry 947 (class 1259 OID 890780)
-- Name: previous_enrollment_form_benefits_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.previous_enrollment_form_benefits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.previous_enrollment_form_benefits_id_seq OWNER TO postgres;

--
-- TOC entry 9058 (class 0 OID 0)
-- Dependencies: 947
-- Name: previous_enrollment_form_benefits_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.previous_enrollment_form_benefits_id_seq OWNED BY vaxiom.previous_enrollment_form_benefits.id;


--
-- TOC entry 945 (class 1259 OID 890774)
-- Name: previous_enrollment_form_id_seq; Type: SEQUENCE; Schema: vaxiom; Owner: postgres
--

CREATE SEQUENCE vaxiom.previous_enrollment_form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vaxiom.previous_enrollment_form_id_seq OWNER TO postgres;

--
-- TOC entry 9059 (class 0 OID 0)
-- Dependencies: 945
-- Name: previous_enrollment_form_id_seq; Type: SEQUENCE OWNED BY; Schema: vaxiom; Owner: postgres
--

ALTER SEQUENCE vaxiom.previous_enrollment_form_id_seq OWNED BY vaxiom.previous_enrollment_form.id;


--
-- TOC entry 950 (class 1259 OID 890795)
-- Name: procedure_additional_resource_requirements; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedure_additional_resource_requirements (
    procedure_id numeric(20,0) NOT NULL,
    resource_type_id numeric(20,0) NOT NULL,
    qty integer NOT NULL
);


ALTER TABLE vaxiom.procedure_additional_resource_requirements OWNER TO postgres;

--
-- TOC entry 952 (class 1259 OID 890802)
-- Name: procedure_step_additional_resource_requirements; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedure_step_additional_resource_requirements (
    step_id numeric(20,0) NOT NULL,
    resource_type_id numeric(20,0) NOT NULL,
    qty integer NOT NULL
);


ALTER TABLE vaxiom.procedure_step_additional_resource_requirements OWNER TO postgres;

--
-- TOC entry 953 (class 1259 OID 890805)
-- Name: procedure_step_durations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedure_step_durations (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    step_id numeric(20,0) NOT NULL,
    load_percentage numeric(3,2) DEFAULT 1.00 NOT NULL
);


ALTER TABLE vaxiom.procedure_step_durations OWNER TO postgres;

--
-- TOC entry 951 (class 1259 OID 890798)
-- Name: procedure_steps; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedure_steps (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer NOT NULL,
    name character varying(255) NOT NULL,
    number integer DEFAULT 0 NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    requires_primary_assistant boolean NOT NULL,
    requires_primary_provider boolean NOT NULL
);


ALTER TABLE vaxiom.procedure_steps OWNER TO postgres;

--
-- TOC entry 949 (class 1259 OID 890789)
-- Name: procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.procedures (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    description character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    insurance_code_id numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    requires_primary_assistant boolean NOT NULL,
    requires_primary_provider boolean NOT NULL,
    requires_diagnosis boolean NOT NULL,
    requires_images boolean NOT NULL,
    dental_marking_type character varying(255)
);


ALTER TABLE vaxiom.procedures OWNER TO postgres;

--
-- TOC entry 954 (class 1259 OID 890809)
-- Name: professional_relationships; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.professional_relationships (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    dtype character varying(31) NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    past boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.professional_relationships OWNER TO postgres;

--
-- TOC entry 955 (class 1259 OID 890813)
-- Name: provider_license; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.provider_license (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    number character varying(45) NOT NULL,
    state character varying(25) NOT NULL,
    provider_specialty_id numeric(20,0) NOT NULL,
    employment_contracts_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.provider_license OWNER TO postgres;

--
-- TOC entry 956 (class 1259 OID 890816)
-- Name: provider_network_insurance_companies; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.provider_network_insurance_companies (
    insurance_company_id numeric(20,0) NOT NULL,
    network_fee_sheet_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.provider_network_insurance_companies OWNER TO postgres;

--
-- TOC entry 957 (class 1259 OID 890819)
-- Name: provider_specialty; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.provider_specialty (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(45) NOT NULL,
    code character varying(45) NOT NULL
);


ALTER TABLE vaxiom.provider_specialty OWNER TO postgres;

--
-- TOC entry 958 (class 1259 OID 890822)
-- Name: question; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.question (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    questionnaire_id numeric(20,0),
    question character varying(50)
);


ALTER TABLE vaxiom.question OWNER TO postgres;

--
-- TOC entry 959 (class 1259 OID 890825)
-- Name: questionnaire; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.questionnaire (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    questionnaire_name character varying(50),
    date timestamp without time zone,
    tx_card_id numeric(20,0),
    patient_id numeric(20,0)
);


ALTER TABLE vaxiom.questionnaire OWNER TO postgres;

--
-- TOC entry 960 (class 1259 OID 890828)
-- Name: questionnaire_answers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.questionnaire_answers (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    questionnaire_id numeric(20,0),
    question_id numeric(20,0),
    answer_id numeric(20,0),
    answer_note character varying(2048),
    quantity character varying(50)
);


ALTER TABLE vaxiom.questionnaire_answers OWNER TO postgres;

--
-- TOC entry 961 (class 1259 OID 890834)
-- Name: reachify_users; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.reachify_users (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    patient_id numeric(20,0) NOT NULL,
    reachify_id character varying(200) NOT NULL,
    email character varying(200) NOT NULL,
    username character varying(200) NOT NULL,
    password character varying(200) NOT NULL
);


ALTER TABLE vaxiom.reachify_users OWNER TO postgres;

--
-- TOC entry 962 (class 1259 OID 890840)
-- Name: receivables; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.receivables (
    id numeric(20,0) NOT NULL,
    rtype character varying(20) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    due_date date,
    realization_date date,
    autopay_date date,
    treatment_id numeric(20,0),
    patient_id numeric(20,0),
    payment_account_id numeric(20,0),
    status character varying(255),
    human_id numeric(20,0),
    insured_id numeric(20,0),
    organization_id numeric(20,0),
    parent_company_id numeric(20,0) NOT NULL,
    init_claim_id numeric(20,0),
    primary_prsn_payer_inv_id numeric(20,0),
    sys_created_at numeric(20,0),
    contract boolean,
    bluefin_organization_id numeric(20,0)
);


ALTER TABLE vaxiom.receivables OWNER TO postgres;

--
-- TOC entry 963 (class 1259 OID 890843)
-- Name: recently_opened_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.recently_opened_items (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    class_name character varying(255) NOT NULL,
    owner numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.recently_opened_items OWNER TO postgres;

--
-- TOC entry 964 (class 1259 OID 890846)
-- Name: referral_list_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.referral_list_values (
    referral_template_id numeric(20,0) NOT NULL,
    pos integer NOT NULL,
    value character varying(2000)
);


ALTER TABLE vaxiom.referral_list_values OWNER TO postgres;

--
-- TOC entry 965 (class 1259 OID 890852)
-- Name: referral_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.referral_templates (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    type character varying(255),
    free_type character varying(255),
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.referral_templates OWNER TO postgres;

--
-- TOC entry 967 (class 1259 OID 890862)
-- Name: refund_details; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.refund_details (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    transaction_id numeric(20,0) NOT NULL,
    refund_type character varying(255),
    refund_method character varying(64),
    money_transaction_id character varying(255),
    check_number character varying(255),
    credit_card_number character varying(255),
    credit_card_expire character varying(4),
    note character varying(1024)
);


ALTER TABLE vaxiom.refund_details OWNER TO postgres;

--
-- TOC entry 966 (class 1259 OID 890859)
-- Name: refunds; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.refunds (
    id numeric(20,0) NOT NULL,
    refund_type character varying(255)
);


ALTER TABLE vaxiom.refunds OWNER TO postgres;

--
-- TOC entry 968 (class 1259 OID 890868)
-- Name: relationships; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.relationships (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    from_person numeric(20,0) NOT NULL,
    to_person numeric(20,0) NOT NULL,
    type character varying(255) NOT NULL,
    role character varying(255) NOT NULL,
    permitted_to_see_info boolean NOT NULL
);


ALTER TABLE vaxiom.relationships OWNER TO postgres;

--
-- TOC entry 969 (class 1259 OID 890874)
-- Name: remote_authentication_tokens; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.remote_authentication_tokens (
    id numeric(20,0) NOT NULL,
    created timestamp without time zone,
    expires timestamp without time zone,
    checksum character varying(64) NOT NULL
);


ALTER TABLE vaxiom.remote_authentication_tokens OWNER TO postgres;

--
-- TOC entry 971 (class 1259 OID 890881)
-- Name: resource_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.resource_types (
    dtype character varying(31) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    abbreviation character varying(100) NOT NULL,
    is_provider boolean DEFAULT false,
    is_assistant boolean DEFAULT false,
    is_filterable boolean DEFAULT false,
    filter_name character varying(255)
);


ALTER TABLE vaxiom.resource_types OWNER TO postgres;

--
-- TOC entry 970 (class 1259 OID 890877)
-- Name: resources; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.resources (
    dtype character varying(31) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    resource_type_id numeric(20,0) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    permissions_user_role_id numeric(20,0)
);


ALTER TABLE vaxiom.resources OWNER TO postgres;

--
-- TOC entry 972 (class 1259 OID 890890)
-- Name: retail_fees; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.retail_fees (
    id numeric(20,0) NOT NULL,
    retail_item_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.retail_fees OWNER TO postgres;

--
-- TOC entry 973 (class 1259 OID 890893)
-- Name: retail_items; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.retail_items (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    name character varying(255),
    price numeric(30,10) NOT NULL,
    available boolean DEFAULT true NOT NULL
);


ALTER TABLE vaxiom.retail_items OWNER TO postgres;

--
-- TOC entry 974 (class 1259 OID 890897)
-- Name: reversed_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.reversed_payments (
    payment_id numeric(20,0) NOT NULL,
    note character varying(1024),
    date timestamp without time zone
);


ALTER TABLE vaxiom.reversed_payments OWNER TO postgres;

--
-- TOC entry 976 (class 1259 OID 890907)
-- Name: schedule_conflict_permission_bypass; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.schedule_conflict_permission_bypass (
    id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    days integer NOT NULL
);


ALTER TABLE vaxiom.schedule_conflict_permission_bypass OWNER TO postgres;

--
-- TOC entry 977 (class 1259 OID 890910)
-- Name: schedule_notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.schedule_notes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    chair_id numeric(20,0) NOT NULL,
    start_time timestamp without time zone,
    duration integer NOT NULL,
    content character varying(255) NOT NULL,
    alert_this_day boolean DEFAULT false NOT NULL,
    is_blocking_time boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.schedule_notes OWNER TO postgres;

--
-- TOC entry 975 (class 1259 OID 890903)
-- Name: scheduled_task; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.scheduled_task (
    task_type character varying(60) NOT NULL,
    scheduling_cycle character varying(20) NOT NULL,
    parent_company_id numeric(20,0) NOT NULL,
    enabled boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.scheduled_task OWNER TO postgres;

--
-- TOC entry 978 (class 1259 OID 890915)
-- Name: scheduling_preferences; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.scheduling_preferences (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    end_hour integer,
    start_hour integer,
    patient_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.scheduling_preferences OWNER TO postgres;

--
-- TOC entry 979 (class 1259 OID 890918)
-- Name: scheduling_preferences_week_days; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.scheduling_preferences_week_days (
    scheduling_preferences_id numeric(20,0) NOT NULL,
    week_days integer
);


ALTER TABLE vaxiom.scheduling_preferences_week_days OWNER TO postgres;

--
-- TOC entry 980 (class 1259 OID 890921)
-- Name: scheduling_preferred_employees; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.scheduling_preferred_employees (
    preference_id numeric(20,0) NOT NULL,
    employee_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.scheduling_preferred_employees OWNER TO postgres;

--
-- TOC entry 981 (class 1259 OID 890924)
-- Name: schema_version; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.schema_version (
    version_rank integer NOT NULL,
    installed_rank integer NOT NULL,
    version character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE vaxiom.schema_version OWNER TO postgres;

--
-- TOC entry 982 (class 1259 OID 890931)
-- Name: selected_appointment_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.selected_appointment_templates (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    user_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    appointment_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.selected_appointment_templates OWNER TO postgres;

--
-- TOC entry 983 (class 1259 OID 890934)
-- Name: selected_appointment_templates_office_day_calendar_view; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.selected_appointment_templates_office_day_calendar_view (
    user_id numeric(20,0) NOT NULL,
    location_id numeric(20,0) NOT NULL,
    appointment_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.selected_appointment_templates_office_day_calendar_view OWNER TO postgres;

--
-- TOC entry 984 (class 1259 OID 890937)
-- Name: selfcheckin_settings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.selfcheckin_settings (
    location_access_key_id numeric(20,0) NOT NULL,
    max_minutes_late integer NOT NULL,
    max_due_amount integer NOT NULL,
    due_period character varying(20) NOT NULL,
    max_due_amount_insurance integer NOT NULL,
    due_period_insurance character varying(20) NOT NULL
);


ALTER TABLE vaxiom.selfcheckin_settings OWNER TO postgres;

--
-- TOC entry 985 (class 1259 OID 890940)
-- Name: selfcheckin_settings_forbidden_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.selfcheckin_settings_forbidden_types (
    location_access_key_id numeric(20,0) NOT NULL,
    appointment_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.selfcheckin_settings_forbidden_types OWNER TO postgres;

--
-- TOC entry 986 (class 1259 OID 890943)
-- Name: simultaneous_appointment_values; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.simultaneous_appointment_values (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    denominator integer NOT NULL,
    numerator integer NOT NULL,
    resource_id numeric(20,0) NOT NULL,
    type_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.simultaneous_appointment_values OWNER TO postgres;

--
-- TOC entry 987 (class 1259 OID 890946)
-- Name: sms; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sms (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    logical_message_id character varying(255) NOT NULL,
    processor character varying(255),
    status character varying(255) NOT NULL,
    sender character varying(255) NOT NULL,
    receiver character varying(255) NOT NULL,
    payload character varying(1280),
    status_message character varying(255),
    sending_date timestamp without time zone,
    time_zone_id character varying(255),
    expiry_date timestamp without time zone
);


ALTER TABLE vaxiom.sms OWNER TO postgres;

--
-- TOC entry 988 (class 1259 OID 890952)
-- Name: sms_part; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sms_part (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sms_id numeric(20,0) NOT NULL,
    provider_message_id character varying(255),
    status character varying(255) NOT NULL
);


ALTER TABLE vaxiom.sms_part OWNER TO postgres;

--
-- TOC entry 989 (class 1259 OID 890958)
-- Name: statements; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.statements (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    human_id numeric(20,0) NOT NULL,
    is_pritented boolean DEFAULT false NOT NULL,
    is_emailed boolean DEFAULT false NOT NULL,
    is_scheduled_to_link boolean DEFAULT false NOT NULL,
    is_linked boolean DEFAULT false NOT NULL,
    is_pdf_requested boolean DEFAULT false NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    pdf_file_key character varying(255),
    filter_start_date date,
    filter_end_date date
);


ALTER TABLE vaxiom.statements OWNER TO postgres;

--
-- TOC entry 990 (class 1259 OID 890967)
-- Name: sys_i18n; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_i18n (
    locale character varying(50) NOT NULL,
    key character varying(255) NOT NULL,
    value character varying(1000) NOT NULL
);


ALTER TABLE vaxiom.sys_i18n OWNER TO postgres;

--
-- TOC entry 991 (class 1259 OID 890973)
-- Name: sys_identifiers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_identifiers (
    next_free numeric(20,0) NOT NULL,
    space character varying(100) NOT NULL
);


ALTER TABLE vaxiom.sys_identifiers OWNER TO postgres;

--
-- TOC entry 993 (class 1259 OID 890983)
-- Name: sys_organization_address; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_address (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    type character varying(45) NOT NULL,
    occupancy character varying(45),
    contact_postal_addresses_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_address OWNER TO postgres;

--
-- TOC entry 994 (class 1259 OID 890986)
-- Name: sys_organization_address_has_sys_organizations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_address_has_sys_organizations (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_organisation_address_id numeric(20,0) NOT NULL,
    sys_organizations_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_address_has_sys_organizations OWNER TO postgres;

--
-- TOC entry 995 (class 1259 OID 890989)
-- Name: sys_organization_attachment; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_attachment (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL,
    file_id character varying(127),
    file_mime_type character varying(255),
    file_name character varying(127),
    file_size numeric(20,0),
    file_content bytea
);


ALTER TABLE vaxiom.sys_organization_attachment OWNER TO postgres;

--
-- TOC entry 996 (class 1259 OID 890995)
-- Name: sys_organization_contact_method; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_contact_method (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    contact_methods_id numeric(20,0) NOT NULL,
    description character varying(255),
    is_prefered boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.sys_organization_contact_method OWNER TO postgres;

--
-- TOC entry 997 (class 1259 OID 890999)
-- Name: sys_organization_department_data; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_department_data (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_department_data OWNER TO postgres;

--
-- TOC entry 998 (class 1259 OID 891002)
-- Name: sys_organization_division_data; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_division_data (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_organizations_id numeric(20,0) NOT NULL,
    medicaid_id character varying(45),
    schedule_appointments boolean,
    accept_payments boolean,
    legal_entity_npi_id numeric(20,0)
);


ALTER TABLE vaxiom.sys_organization_division_data OWNER TO postgres;

--
-- TOC entry 999 (class 1259 OID 891005)
-- Name: sys_organization_division_has_department; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_division_has_department (
    division_tree_node_id numeric(20,0) NOT NULL,
    department_id numeric(20,0) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0)
);


ALTER TABLE vaxiom.sys_organization_division_has_department OWNER TO postgres;

--
-- TOC entry 1000 (class 1259 OID 891008)
-- Name: sys_organization_location_group; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_location_group (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(45),
    sys_organizations_id numeric(20,0)
);


ALTER TABLE vaxiom.sys_organization_location_group OWNER TO postgres;

--
-- TOC entry 1001 (class 1259 OID 891011)
-- Name: sys_organization_location_group_has_sys_organizations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_location_group_has_sys_organizations (
    sys_organization_location_group_id numeric(20,0) NOT NULL,
    sys_organizations_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_location_group_has_sys_organizations OWNER TO postgres;

--
-- TOC entry 1002 (class 1259 OID 891014)
-- Name: sys_organization_settings; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organization_settings (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE vaxiom.sys_organization_settings OWNER TO postgres;

--
-- TOC entry 992 (class 1259 OID 890976)
-- Name: sys_organizations; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_organizations (
    id numeric(20,0) NOT NULL,
    level character varying(20) NOT NULL,
    name character varying(255) NOT NULL,
    time_zone_id character varying(255),
    parent_id numeric(20,0),
    postal_address_id numeric(20,0),
    deleted boolean DEFAULT false NOT NULL,
    epoch_date date,
    lat numeric(30,10),
    lng numeric(30,10)
);


ALTER TABLE vaxiom.sys_organizations OWNER TO postgres;

--
-- TOC entry 1003 (class 1259 OID 891020)
-- Name: sys_patient_portal_users; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_patient_portal_users (
    id numeric(20,0) NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    encoded_password character varying(255) NOT NULL,
    login_email character varying(255) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    account_expiration_date date,
    password_expiration_date date,
    token character varying(255),
    token_expiration_date date,
    invitation_sent boolean DEFAULT false NOT NULL,
    completed_first_appointment boolean DEFAULT false,
    completed_profile boolean DEFAULT false
);


ALTER TABLE vaxiom.sys_patient_portal_users OWNER TO postgres;

--
-- TOC entry 1004 (class 1259 OID 891032)
-- Name: sys_portal_users; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_portal_users (
    id numeric(20,0) NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    encoded_password character varying(255) NOT NULL,
    login_email character varying(255) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    account_expiration_date date,
    password_expiration_date date,
    token character varying(255),
    token_expiration_date date,
    invitation_sent boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.sys_portal_users OWNER TO postgres;

--
-- TOC entry 1005 (class 1259 OID 891042)
-- Name: sys_users; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.sys_users (
    id numeric(20,0) NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    encoded_password character varying(255) NOT NULL,
    login_email character varying(255) NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    account_expiration_date date,
    password_expiration_date date,
    token character varying(255),
    token_expiration_date date
);


ALTER TABLE vaxiom.sys_users OWNER TO postgres;

--
-- TOC entry 1006 (class 1259 OID 891051)
-- Name: table_statistics; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.table_statistics (
    created timestamp without time zone,
    table_name character varying(64) DEFAULT ''::character varying NOT NULL,
    table_rows numeric,
    avg_row_length numeric,
    data_length numeric,
    index_length numeric
);


ALTER TABLE vaxiom.table_statistics OWNER TO postgres;

--
-- TOC entry 1009 (class 1259 OID 891071)
-- Name: task_basket_subscribers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.task_basket_subscribers (
    task_basket_id numeric(20,0) NOT NULL,
    user_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.task_basket_subscribers OWNER TO postgres;

--
-- TOC entry 1008 (class 1259 OID 891065)
-- Name: task_baskets; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.task_baskets (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    title character varying(255),
    description character varying(1023),
    organization_id numeric(20,0) NOT NULL,
    type character varying(255)
);


ALTER TABLE vaxiom.task_baskets OWNER TO postgres;

--
-- TOC entry 1010 (class 1259 OID 891074)
-- Name: task_contacts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.task_contacts (
    task_id numeric(20,0) NOT NULL,
    contact_method_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.task_contacts OWNER TO postgres;

--
-- TOC entry 1011 (class 1259 OID 891077)
-- Name: task_events; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.task_events (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    task_id numeric(20,0) NOT NULL,
    assignee_id numeric(20,0),
    action character varying(255),
    comment character varying(4095)
);


ALTER TABLE vaxiom.task_events OWNER TO postgres;

--
-- TOC entry 1007 (class 1259 OID 891058)
-- Name: tasks; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tasks (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    title character varying(255),
    due_date timestamp without time zone,
    description character varying(1023),
    patient_id numeric(20,0),
    document_template_id numeric(20,0),
    document_tree_node_id numeric(20,0),
    provider_id numeric(20,0),
    organization_id numeric(20,0),
    assignee_id numeric(20,0),
    task_basket_id numeric(20,0) NOT NULL,
    task_status character varying(255),
    recipient_id numeric(20,0),
    created_from character varying(45) DEFAULT 'CORE'::character varying
);


ALTER TABLE vaxiom.tasks OWNER TO postgres;

--
-- TOC entry 1013 (class 1259 OID 891089)
-- Name: temp_accounts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.temp_accounts (
    id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    person_payer_id numeric(20,0),
    insurance_payer_id numeric(20,0),
    tx_plan_id numeric(20,0)
);


ALTER TABLE vaxiom.temp_accounts OWNER TO postgres;

--
-- TOC entry 1012 (class 1259 OID 891083)
-- Name: temporary_images; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.temporary_images (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    file_key character varying(255) NOT NULL,
    organization_node_id numeric(20,0) NOT NULL,
    patient_id numeric(20,0),
    source character varying(255),
    mime_type character varying(255),
    original_name character varying(255),
    patient_image_id numeric(20,0)
);


ALTER TABLE vaxiom.temporary_images OWNER TO postgres;

--
-- TOC entry 1014 (class 1259 OID 891092)
-- Name: third_party_account; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.third_party_account (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    system character varying(20),
    name character varying(255),
    pem_path character varying(255),
    consumer_key character varying(255),
    consumer_secret character varying(255),
    organization_node_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.third_party_account OWNER TO postgres;

--
-- TOC entry 1018 (class 1259 OID 891110)
-- Name: tooth_marking; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tooth_marking (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    completed boolean,
    to_tooth_number integer,
    tooth_number integer NOT NULL,
    type character varying(255),
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tooth_marking OWNER TO postgres;

--
-- TOC entry 1015 (class 1259 OID 891098)
-- Name: toothchart_edit_log; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.toothchart_edit_log (
    id numeric(20,0) NOT NULL,
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.toothchart_edit_log OWNER TO postgres;

--
-- TOC entry 1016 (class 1259 OID 891101)
-- Name: toothchart_note; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.toothchart_note (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    posx integer,
    posy integer,
    text character varying(255),
    snapshot_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.toothchart_note OWNER TO postgres;

--
-- TOC entry 1017 (class 1259 OID 891104)
-- Name: toothchart_snapshot; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.toothchart_snapshot (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    comments character varying(255),
    data bytea NOT NULL,
    mime_type character varying(255) NOT NULL,
    appointment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.toothchart_snapshot OWNER TO postgres;

--
-- TOC entry 1019 (class 1259 OID 891113)
-- Name: transactions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.transactions (
    dtype character varying(31) NOT NULL,
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    receivable_id numeric(20,0),
    amount numeric(30,10),
    notes character varying(255),
    affects_due_now boolean DEFAULT true NOT NULL,
    meta_amount numeric(30,10),
    added timestamp without time zone,
    effective_date timestamp without time zone,
    draft boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.transactions OWNER TO postgres;

--
-- TOC entry 1020 (class 1259 OID 891118)
-- Name: transfer_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.transfer_charges (
    id numeric(20,0) NOT NULL,
    charge_transfer_adjustment_id numeric(20,0),
    transfer_charge_type character varying(255)
);


ALTER TABLE vaxiom.transfer_charges OWNER TO postgres;

--
-- TOC entry 1024 (class 1259 OID 891141)
-- Name: tx_card_field_definitions; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_card_field_definitions (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    alert boolean DEFAULT false NOT NULL,
    hide_for_future boolean DEFAULT false NOT NULL,
    name character varying(255) NOT NULL,
    multi_value boolean DEFAULT false NOT NULL,
    number integer,
    tracked boolean DEFAULT false NOT NULL,
    type_id numeric(20,0) NOT NULL,
    organization_id numeric(20,0) NOT NULL,
    mandatory boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_card_field_definitions OWNER TO postgres;

--
-- TOC entry 1025 (class 1259 OID 891149)
-- Name: tx_card_field_options; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_card_field_options (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    field_type_id numeric(20,0) NOT NULL,
    value character varying(255) NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    pos integer DEFAULT 0 NOT NULL
);


ALTER TABLE vaxiom.tx_card_field_options OWNER TO postgres;

--
-- TOC entry 1026 (class 1259 OID 891154)
-- Name: tx_card_field_types; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_card_field_types (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    value_type character varying(255) NOT NULL,
    is_broken_brackets boolean DEFAULT false,
    is_hygiene boolean DEFAULT false
);


ALTER TABLE vaxiom.tx_card_field_types OWNER TO postgres;

--
-- TOC entry 1027 (class 1259 OID 891162)
-- Name: tx_card_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_card_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    tx_category_id numeric(20,0) NOT NULL,
    tx_plan_template_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_card_templates OWNER TO postgres;

--
-- TOC entry 1023 (class 1259 OID 891137)
-- Name: tx_cards; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_cards (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    patient_id numeric(20,0) NOT NULL,
    tx_category_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_cards OWNER TO postgres;

--
-- TOC entry 1028 (class 1259 OID 891165)
-- Name: tx_categories; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_categories (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL
);


ALTER TABLE vaxiom.tx_categories OWNER TO postgres;

--
-- TOC entry 1029 (class 1259 OID 891168)
-- Name: tx_category_coverages; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_category_coverages (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    tx_category_id numeric(20,0) NOT NULL,
    insurance_plan_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_category_coverages OWNER TO postgres;

--
-- TOC entry 1030 (class 1259 OID 891171)
-- Name: tx_category_insured; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_category_insured (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    tx_category_id numeric(20,0) NOT NULL,
    insured_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_category_insured OWNER TO postgres;

--
-- TOC entry 1031 (class 1259 OID 891174)
-- Name: tx_contracts; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_contracts (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    generated_date date,
    signed_date date,
    file character varying(255),
    template_name character varying(255),
    tx_payer_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_contracts OWNER TO postgres;

--
-- TOC entry 1032 (class 1259 OID 891180)
-- Name: tx_fee_charges; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_fee_charges (
    id numeric(20,0) NOT NULL,
    tx_plan_id numeric(20,0) NOT NULL,
    insurance_code_id numeric(20,0)
);


ALTER TABLE vaxiom.tx_fee_charges OWNER TO postgres;

--
-- TOC entry 1033 (class 1259 OID 891183)
-- Name: tx_payers; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_payers (
    id numeric(20,0) NOT NULL,
    ptype character varying(20) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    primary_payer boolean DEFAULT false NOT NULL,
    tx_id numeric(20,0),
    person_id numeric(20,0),
    contact_method_id numeric(20,0),
    insured_id numeric(20,0),
    payment_method character varying(255),
    account_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_payers OWNER TO postgres;

--
-- TOC entry 1036 (class 1259 OID 891197)
-- Name: tx_plan_appointment_procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_appointment_procedures (
    appointment_id numeric(20,0) NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    pos integer DEFAULT 0 NOT NULL,
    from_template boolean DEFAULT false NOT NULL,
    added boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_plan_appointment_procedures OWNER TO postgres;

--
-- TOC entry 1035 (class 1259 OID 891192)
-- Name: tx_plan_appointments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_appointments (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer,
    has_custom_procedures boolean DEFAULT false NOT NULL,
    interval_to_next integer,
    note character varying(255) DEFAULT ''::character varying NOT NULL,
    type_id numeric(20,0) NOT NULL,
    next_appointment_id numeric(20,0),
    tx_plan_id numeric(20,0) NOT NULL,
    tx_plan_template_appointment_id numeric(20,0),
    sys_created_at numeric(20,0)
);


ALTER TABLE vaxiom.tx_plan_appointments OWNER TO postgres;

--
-- TOC entry 1037 (class 1259 OID 891204)
-- Name: tx_plan_groups; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_groups (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255),
    pc_id numeric(20,0) NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_plan_groups OWNER TO postgres;

--
-- TOC entry 1039 (class 1259 OID 891215)
-- Name: tx_plan_note_groups; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_note_groups (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    note character varying(8192) DEFAULT ''::character varying NOT NULL,
    group_key character varying(255),
    tx_plan_note_id numeric(20,0)
);


ALTER TABLE vaxiom.tx_plan_note_groups OWNER TO postgres;

--
-- TOC entry 1038 (class 1259 OID 891208)
-- Name: tx_plan_notes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_notes (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    note character varying(8192) DEFAULT ''::character varying NOT NULL,
    tx_card_id numeric(20,0),
    diagnosis_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.tx_plan_notes OWNER TO postgres;

--
-- TOC entry 1042 (class 1259 OID 891233)
-- Name: tx_plan_template_appointment_procedures; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_template_appointment_procedures (
    appointment_id numeric(20,0) NOT NULL,
    procedure_id numeric(20,0) NOT NULL,
    pos integer DEFAULT 0 NOT NULL,
    from_template boolean DEFAULT false NOT NULL,
    added boolean DEFAULT false NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_plan_template_appointment_procedures OWNER TO postgres;

--
-- TOC entry 1041 (class 1259 OID 891228)
-- Name: tx_plan_template_appointments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_template_appointments (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    duration integer,
    has_custom_procedures boolean DEFAULT false NOT NULL,
    interval_to_next integer,
    note character varying(255) DEFAULT ''::character varying NOT NULL,
    type_id numeric(20,0) NOT NULL,
    next_appointment_id numeric(20,0),
    tx_plan_template_id numeric(20,0) NOT NULL,
    sys_created_at numeric(20,0)
);


ALTER TABLE vaxiom.tx_plan_template_appointments OWNER TO postgres;

--
-- TOC entry 1040 (class 1259 OID 891222)
-- Name: tx_plan_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plan_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    insurance_code_id numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    is_pre_treatment boolean DEFAULT false NOT NULL,
    tx_category_id numeric(20,0) NOT NULL,
    fee numeric(30,10),
    length_in_weeks integer,
    deleted boolean DEFAULT false NOT NULL,
    tx_plan_group_id numeric(20,0)
);


ALTER TABLE vaxiom.tx_plan_templates OWNER TO postgres;

--
-- TOC entry 1034 (class 1259 OID 891187)
-- Name: tx_plans; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_plans (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    insurance_code_id numeric(20,0),
    tx_id numeric(20,0) NOT NULL,
    tx_plan_template_id numeric(20,0),
    is_candidate boolean DEFAULT true NOT NULL,
    fee numeric(30,10),
    projected_start_date date,
    length_in_weeks integer
);


ALTER TABLE vaxiom.tx_plans OWNER TO postgres;

--
-- TOC entry 1043 (class 1259 OID 891240)
-- Name: tx_statuses; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.tx_statuses (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(45) NOT NULL,
    parent_company_id numeric(20,0) NOT NULL,
    type character varying(45) NOT NULL,
    hidden boolean DEFAULT false NOT NULL
);


ALTER TABLE vaxiom.tx_statuses OWNER TO postgres;

--
-- TOC entry 1021 (class 1259 OID 891121)
-- Name: txs; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.txs (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    organization_id numeric(20,0),
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    notes character varying(8192) DEFAULT ''::character varying NOT NULL,
    status character varying(255) NOT NULL,
    did_not_start boolean DEFAULT false NOT NULL,
    estimated_start_date date,
    estimated_end_date date,
    start_date date,
    end_date date,
    tx_plan_id numeric(20,0),
    tx_card_id numeric(20,0) NOT NULL,
    migrated boolean DEFAULT false NOT NULL,
    sys_created_at numeric(20,0)
);


ALTER TABLE vaxiom.txs OWNER TO postgres;

--
-- TOC entry 1022 (class 1259 OID 891131)
-- Name: txs_state_changes; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.txs_state_changes (
    id numeric(20,0) NOT NULL,
    sys_version numeric(20,0),
    change_time timestamp without time zone,
    new_state character varying(255) NOT NULL,
    old_state character varying(255),
    treatment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.txs_state_changes OWNER TO postgres;

--
-- TOC entry 1044 (class 1259 OID 891244)
-- Name: unapplied_payments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.unapplied_payments (
    id numeric(20,0) NOT NULL,
    applied_payment_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.unapplied_payments OWNER TO postgres;

--
-- TOC entry 1045 (class 1259 OID 891247)
-- Name: update_autopayments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.update_autopayments (
    created timestamp without time zone,
    source_name character varying(50),
    patient_id numeric(20,0),
    receivable_id numeric(20,0),
    payment_plan_id numeric(20,0),
    patient_name character varying(100),
    treatment character varying(50),
    source_token character varying(20),
    last_four_digits character varying(10),
    exp_card_date character varying(20),
    location_id numeric(20,0),
    payconnex_account character varying(20),
    payconnex_token character varying(20),
    result character varying(500)
);


ALTER TABLE vaxiom.update_autopayments OWNER TO postgres;

--
-- TOC entry 1046 (class 1259 OID 891253)
-- Name: week_templates; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.week_templates (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    name character varying(255) NOT NULL,
    fri numeric(20,0),
    mon numeric(20,0),
    organization_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0),
    sat numeric(20,0),
    sun numeric(20,0),
    thu numeric(20,0),
    tue numeric(20,0),
    wed numeric(20,0)
);


ALTER TABLE vaxiom.week_templates OWNER TO postgres;

--
-- TOC entry 1048 (class 1259 OID 891260)
-- Name: work_hour_comments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.work_hour_comments (
    id numeric(20,0) NOT NULL,
    work_hours_id numeric(20,0) NOT NULL,
    date timestamp without time zone,
    core_user_id numeric(20,0) NOT NULL,
    comment character varying(512),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.work_hour_comments OWNER TO postgres;

--
-- TOC entry 1047 (class 1259 OID 891256)
-- Name: work_hours; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.work_hours (
    id numeric(20,0) NOT NULL,
    core_user_id numeric(20,0) NOT NULL,
    date timestamp without time zone,
    status character varying(20) NOT NULL,
    state character varying(20) DEFAULT 'NORMAL'::character varying NOT NULL,
    location_id numeric(20,0) NOT NULL,
    modified_by_admin numeric(20,0),
    sys_version numeric(20,0),
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0)
);


ALTER TABLE vaxiom.work_hours OWNER TO postgres;

--
-- TOC entry 1049 (class 1259 OID 891266)
-- Name: work_schedule_days; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.work_schedule_days (
    id numeric(20,0) NOT NULL,
    sys_created_by numeric(20,0),
    sys_created timestamp without time zone,
    sys_last_modified timestamp without time zone,
    sys_last_modified_by numeric(20,0),
    sys_version numeric(20,0),
    ws_date date NOT NULL,
    day_schedule_id numeric(20,0) NOT NULL,
    resource_id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.work_schedule_days OWNER TO postgres;

--
-- TOC entry 1050 (class 1259 OID 891269)
-- Name: writeoff_adjustments; Type: TABLE; Schema: vaxiom; Owner: postgres
--

CREATE TABLE vaxiom.writeoff_adjustments (
    id numeric(20,0) NOT NULL
);


ALTER TABLE vaxiom.writeoff_adjustments OWNER TO postgres;

--
-- TOC entry 6232 (class 2604 OID 889346)
-- Name: act_evt_log log_nr_; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_evt_log ALTER COLUMN log_nr_ SET DEFAULT nextval('vaxiom.act_evt_log_log_nr__seq'::regclass);


--
-- TOC entry 6288 (class 2604 OID 889959)
-- Name: enrollment_form id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_id_seq'::regclass);


--
-- TOC entry 6289 (class 2604 OID 889965)
-- Name: enrollment_form_beneficiary id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_beneficiary ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_beneficiary_id_seq'::regclass);


--
-- TOC entry 6290 (class 2604 OID 889974)
-- Name: enrollment_form_benefits id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_benefits ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_benefits_id_seq'::regclass);


--
-- TOC entry 6291 (class 2604 OID 889983)
-- Name: enrollment_form_dependents id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_dependents ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_dependents_id_seq'::regclass);


--
-- TOC entry 6292 (class 2604 OID 889992)
-- Name: enrollment_form_dependents_benefits id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_dependents_benefits ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_dependents_benefits_id_seq'::regclass);


--
-- TOC entry 6293 (class 2604 OID 890001)
-- Name: enrollment_form_personal_data id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_personal_data ALTER COLUMN id SET DEFAULT nextval('vaxiom.enrollment_form_personal_data_id_seq'::regclass);


--
-- TOC entry 6404 (class 2604 OID 890438)
-- Name: medical_history_patient_relatives_crowding id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_patient_relatives_crowding ALTER COLUMN id SET DEFAULT nextval('vaxiom.medical_history_patient_relatives_crowding_id_seq'::regclass);


--
-- TOC entry 6406 (class 2604 OID 890544)
-- Name: migration_map id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migration_map ALTER COLUMN id SET DEFAULT nextval('vaxiom.migration_map_id_seq'::regclass);


--
-- TOC entry 6426 (class 2604 OID 890779)
-- Name: previous_enrollment_form id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form ALTER COLUMN id SET DEFAULT nextval('vaxiom.previous_enrollment_form_id_seq'::regclass);


--
-- TOC entry 6427 (class 2604 OID 890785)
-- Name: previous_enrollment_form_benefits id; Type: DEFAULT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form_benefits ALTER COLUMN id SET DEFAULT nextval('vaxiom.previous_enrollment_form_benefits_id_seq'::regclass);


--
-- TOC entry 8595 (class 0 OID 889343)
-- Dependencies: 637
-- Data for Name: act_evt_log; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_evt_log (log_nr_, type_, proc_def_id_, proc_inst_id_, execution_id_, task_id_, time_stamp_, user_id_, data_, lock_owner_, lock_time_, is_processed_) FROM stdin;
\.


--
-- TOC entry 8596 (class 0 OID 889351)
-- Dependencies: 638
-- Data for Name: act_ge_bytearray; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_ge_bytearray (id_, rev_, name_, deployment_id_, bytes_, generated_) FROM stdin;
\.


--
-- TOC entry 8597 (class 0 OID 889357)
-- Dependencies: 639
-- Data for Name: act_ge_property; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_ge_property (name_, value_, rev_) FROM stdin;
\.


--
-- TOC entry 8598 (class 0 OID 889360)
-- Dependencies: 640
-- Data for Name: act_hi_actinst; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_hi_actinst (id_, proc_def_id_, proc_inst_id_, execution_id_, act_id_, task_id_, call_proc_inst_id_, act_name_, act_type_, assignee_, start_time_, end_time_, duration_, tenant_id_) FROM stdin;
\.


--
-- TOC entry 8599 (class 0 OID 889367)
-- Dependencies: 641
-- Data for Name: act_hi_attachment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_hi_attachment (id_, rev_, user_id_, name_, description_, type_, task_id_, proc_inst_id_, url_, content_id_, time_) FROM stdin;
\.


--
-- TOC entry 8600 (class 0 OID 889373)
-- Dependencies: 642
-- Data for Name: act_hi_comment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_hi_comment (id_, type_, time_, user_id_, task_id_, proc_inst_id_, action_, message_, full_msg_) FROM stdin;
\.


--
-- TOC entry 8601 (class 0 OID 889379)
-- Dependencies: 643
-- Data for Name: act_hi_detail; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_hi_detail (id_, type_, proc_inst_id_, execution_id_, task_id_, act_inst_id_, name_, var_type_, rev_, time_, bytearray_id_, double_, long_, text_, text2_) FROM stdin;
\.


--
-- TOC entry 8602 (class 0 OID 889385)
-- Dependencies: 644
-- Data for Name: act_hi_identitylink; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_hi_identitylink (id_, group_id_, type_, user_id_, task_id_, proc_inst_id_) FROM stdin;
\.


--
-- TOC entry 8603 (class 0 OID 889391)
-- Dependencies: 645
-- Data for Name: act_hi_procinst; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_hi_procinst (id_, proc_inst_id_, business_key_, proc_def_id_, start_time_, end_time_, duration_, start_user_id_, start_act_id_, end_act_id_, super_process_instance_id_, delete_reason_, tenant_id_, name_) FROM stdin;
\.


--
-- TOC entry 8604 (class 0 OID 889398)
-- Dependencies: 646
-- Data for Name: act_hi_taskinst; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_hi_taskinst (id_, proc_def_id_, task_def_key_, proc_inst_id_, execution_id_, name_, parent_task_id_, description_, owner_, assignee_, start_time_, claim_time_, end_time_, duration_, delete_reason_, priority_, due_date_, form_key_, category_, tenant_id_) FROM stdin;
\.


--
-- TOC entry 8605 (class 0 OID 889405)
-- Dependencies: 647
-- Data for Name: act_hi_varinst; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_hi_varinst (id_, proc_inst_id_, execution_id_, task_id_, name_, var_type_, rev_, bytearray_id_, double_, long_, text_, text2_, create_time_, last_updated_time_) FROM stdin;
\.


--
-- TOC entry 8606 (class 0 OID 889411)
-- Dependencies: 648
-- Data for Name: act_id_group; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_id_group (id_, rev_, name_, type_) FROM stdin;
\.


--
-- TOC entry 8607 (class 0 OID 889417)
-- Dependencies: 649
-- Data for Name: act_id_info; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_id_info (id_, rev_, user_id_, type_, key_, value_, password_, parent_id_) FROM stdin;
\.


--
-- TOC entry 8608 (class 0 OID 889423)
-- Dependencies: 650
-- Data for Name: act_id_membership; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_id_membership (user_id_, group_id_) FROM stdin;
\.


--
-- TOC entry 8609 (class 0 OID 889426)
-- Dependencies: 651
-- Data for Name: act_id_user; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_id_user (id_, rev_, first_, last_, email_, pwd_, picture_id_) FROM stdin;
\.


--
-- TOC entry 8610 (class 0 OID 889432)
-- Dependencies: 652
-- Data for Name: act_re_deployment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_re_deployment (id_, name_, category_, tenant_id_, deploy_time_) FROM stdin;
\.


--
-- TOC entry 8611 (class 0 OID 889439)
-- Dependencies: 653
-- Data for Name: act_re_model; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_re_model (id_, rev_, name_, key_, category_, create_time_, last_update_time_, version_, meta_info_, deployment_id_, editor_source_value_id_, editor_source_extra_value_id_, tenant_id_) FROM stdin;
\.


--
-- TOC entry 8612 (class 0 OID 889446)
-- Dependencies: 654
-- Data for Name: act_re_procdef; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_re_procdef (id_, rev_, category_, name_, key_, version_, deployment_id_, resource_name_, dgrm_resource_name_, description_, has_start_form_key_, has_graphical_notation_, suspension_state_, tenant_id_) FROM stdin;
\.


--
-- TOC entry 8613 (class 0 OID 889453)
-- Dependencies: 655
-- Data for Name: act_ru_event_subscr; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_ru_event_subscr (id_, rev_, event_type_, event_name_, execution_id_, proc_inst_id_, activity_id_, configuration_, created_, proc_def_id_, tenant_id_) FROM stdin;
\.


--
-- TOC entry 8614 (class 0 OID 889461)
-- Dependencies: 656
-- Data for Name: act_ru_execution; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_ru_execution (id_, rev_, proc_inst_id_, business_key_, parent_id_, proc_def_id_, super_exec_, act_id_, is_active_, is_concurrent_, is_scope_, is_event_scope_, suspension_state_, cached_ent_state_, tenant_id_, name_, lock_time_) FROM stdin;
\.


--
-- TOC entry 8615 (class 0 OID 889468)
-- Dependencies: 657
-- Data for Name: act_ru_identitylink; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_ru_identitylink (id_, rev_, group_id_, type_, user_id_, task_id_, proc_inst_id_, proc_def_id_) FROM stdin;
\.


--
-- TOC entry 8616 (class 0 OID 889474)
-- Dependencies: 658
-- Data for Name: act_ru_job; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_ru_job (id_, rev_, type_, lock_exp_time_, lock_owner_, exclusive_, execution_id_, process_instance_id_, proc_def_id_, retries_, exception_stack_id_, exception_msg_, duedate_, repeat_, handler_type_, handler_cfg_, tenant_id_) FROM stdin;
\.


--
-- TOC entry 8617 (class 0 OID 889481)
-- Dependencies: 659
-- Data for Name: act_ru_task; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_ru_task (id_, rev_, execution_id_, proc_inst_id_, proc_def_id_, name_, parent_task_id_, description_, task_def_key_, owner_, assignee_, delegation_, priority_, create_time_, due_date_, category_, suspension_state_, tenant_id_, form_key_) FROM stdin;
\.


--
-- TOC entry 8618 (class 0 OID 889488)
-- Dependencies: 660
-- Data for Name: act_ru_variable; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.act_ru_variable (id_, rev_, type_, name_, execution_id_, proc_inst_id_, task_id_, bytearray_id_, double_, long_, text_, text2_) FROM stdin;
\.


--
-- TOC entry 8592 (class 0 OID 889332)
-- Dependencies: 634
-- Data for Name: activiti_process_settings; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.activiti_process_settings (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_node_id, process_id) FROM stdin;
\.


--
-- TOC entry 8593 (class 0 OID 889335)
-- Dependencies: 635
-- Data for Name: activiti_process_settings_property; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.activiti_process_settings_property (activiti_process_settings_id, property, val) FROM stdin;
\.


--
-- TOC entry 8619 (class 0 OID 889494)
-- Dependencies: 661
-- Data for Name: added_report_type; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.added_report_type (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, owner, report_type) FROM stdin;
\.


--
-- TOC entry 8620 (class 0 OID 889497)
-- Dependencies: 662
-- Data for Name: answers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.answers (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, question_id, answer) FROM stdin;
\.


--
-- TOC entry 8621 (class 0 OID 889503)
-- Dependencies: 663
-- Data for Name: application_properties; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.application_properties (organization_id, message_key, message_value) FROM stdin;
\.


--
-- TOC entry 8622 (class 0 OID 889509)
-- Dependencies: 664
-- Data for Name: applied_payments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.applied_payments (id, payment_id, applied_type) FROM stdin;
\.


--
-- TOC entry 8624 (class 0 OID 889521)
-- Dependencies: 666
-- Data for Name: appointment_audit_log; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_audit_log (action, id, appointment_id) FROM stdin;
\.


--
-- TOC entry 8626 (class 0 OID 889531)
-- Dependencies: 668
-- Data for Name: appointment_booking_resources; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_booking_resources (id, sys_version, booking_id, resource_id, appt_start_offset, duration, load_percentage, legacy_doctor, legacy_assistant) FROM stdin;
\.


--
-- TOC entry 8627 (class 0 OID 889538)
-- Dependencies: 669
-- Data for Name: appointment_booking_state_changes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_booking_state_changes (id, sys_version, change_time, new_state, old_state, booking_id) FROM stdin;
\.


--
-- TOC entry 8625 (class 0 OID 889524)
-- Dependencies: 667
-- Data for Name: appointment_bookings; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_bookings (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, confirmation_status, start_time, local_start_date, local_start_time, duration, state, appointment_id, chair_id, provider_id, assistant_id, legacy_doctor, legacy_assistant, reminder_sent, seated_chair_id, sys_created_at, check_in_time) FROM stdin;
\.


--
-- TOC entry 8628 (class 0 OID 889544)
-- Dependencies: 670
-- Data for Name: appointment_field_values; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_field_values (appointment_id, field_option_id, field_definition_id) FROM stdin;
\.


--
-- TOC entry 8629 (class 0 OID 889547)
-- Dependencies: 671
-- Data for Name: appointment_procedures; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_procedures (appointment_id, procedure_id, pos, from_template, added, deleted) FROM stdin;
\.


--
-- TOC entry 8631 (class 0 OID 889561)
-- Dependencies: 673
-- Data for Name: appointment_template_procedures; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_template_procedures (appointment_template_id, procedure_id, pos) FROM stdin;
\.


--
-- TOC entry 8630 (class 0 OID 889554)
-- Dependencies: 672
-- Data for Name: appointment_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_templates (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, appointment_type_id, primary_provider_type_id, primary_assistant_type_id, full_name, color_id, medical_plan, send_medical_form) FROM stdin;
\.


--
-- TOC entry 8634 (class 0 OID 889580)
-- Dependencies: 676
-- Data for Name: appointment_type_daily_restrictions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_type_daily_restrictions (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, type_id, local_date, location_id, max_appts) FROM stdin;
\.


--
-- TOC entry 8632 (class 0 OID 889564)
-- Dependencies: 674
-- Data for Name: appointment_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_types (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, color_id, full_name, name, not_tx, patient_exam, parent_id, migrated, emergency) FROM stdin;
\.


--
-- TOC entry 8633 (class 0 OID 889574)
-- Dependencies: 675
-- Data for Name: appointment_types_property; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointment_types_property (appointment_type_id, property, val) FROM stdin;
\.


--
-- TOC entry 8623 (class 0 OID 889512)
-- Dependencies: 665
-- Data for Name: appointments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appointments (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, duration, has_custom_procedures, interval_to_next, note, type_id, next_appointment_id, patient_id, tx_id, tx_plan_appointment_id, unplanned, legacy_procedure_codes, sys_created_at) FROM stdin;
\.


--
-- TOC entry 8635 (class 0 OID 889584)
-- Dependencies: 677
-- Data for Name: appt_type_template_restrictions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.appt_type_template_restrictions (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, template_id, type_id, max_appts) FROM stdin;
\.


--
-- TOC entry 8636 (class 0 OID 889588)
-- Dependencies: 678
-- Data for Name: audit_events; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.audit_events (id, created, ip_address, application, session, user_id, subject, subject_id, verb, object, object_id, parent_object, parent_object_id, description) FROM stdin;
\.


--
-- TOC entry 8637 (class 0 OID 889594)
-- Dependencies: 679
-- Data for Name: audit_logs; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.audit_logs (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, class_type, user_id) FROM stdin;
\.


--
-- TOC entry 8638 (class 0 OID 889597)
-- Dependencies: 680
-- Data for Name: autopay_invoice_changes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.autopay_invoice_changes (id, sys_version, change_time, changed_by, changed_field_name, new_value, old_value, receivable_id, description) FROM stdin;
\.


--
-- TOC entry 8639 (class 0 OID 889603)
-- Dependencies: 681
-- Data for Name: bank_accounts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.bank_accounts (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, bank_account_type, account_number, routing_number) FROM stdin;
\.


--
-- TOC entry 8640 (class 0 OID 889609)
-- Dependencies: 682
-- Data for Name: benefit_saving_account_values; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.benefit_saving_account_values (id, pre_tax_rate, post_tax_rate, employer_rate, total_rate, employee_benefit_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, insurance_plan_saving_account_id, employee_annual_rate, employer_annual_rate, waived, qle_parent, employee_beneficiary_group_id) FROM stdin;
\.


--
-- TOC entry 8641 (class 0 OID 889612)
-- Dependencies: 683
-- Data for Name: bluefin_credentials; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.bluefin_credentials (organization_id, account_id, access_key) FROM stdin;
\.


--
-- TOC entry 8642 (class 0 OID 889618)
-- Dependencies: 684
-- Data for Name: broker; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.broker (id, login_email, name) FROM stdin;
\.


--
-- TOC entry 8643 (class 0 OID 889621)
-- Dependencies: 685
-- Data for Name: cancelled_receivables; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.cancelled_receivables (receivable_id, date) FROM stdin;
\.


--
-- TOC entry 8644 (class 0 OID 889624)
-- Dependencies: 686
-- Data for Name: cephx_requests; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.cephx_requests (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, transaction_id, patient_image_id, status) FROM stdin;
\.


--
-- TOC entry 8646 (class 0 OID 889635)
-- Dependencies: 688
-- Data for Name: chair_allocations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.chair_allocations (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, ca_date, chair_id, resource_id, primary_resource) FROM stdin;
\.


--
-- TOC entry 8645 (class 0 OID 889628)
-- Dependencies: 687
-- Data for Name: chairs; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.chairs (code, full_name, id, pos) FROM stdin;
\.


--
-- TOC entry 8647 (class 0 OID 889639)
-- Dependencies: 689
-- Data for Name: charge_transfer_adjustments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.charge_transfer_adjustments (id, transfer_charge_type) FROM stdin;
\.


--
-- TOC entry 8649 (class 0 OID 889645)
-- Dependencies: 691
-- Data for Name: check_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.check_types (id, name, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8648 (class 0 OID 889642)
-- Dependencies: 690
-- Data for Name: checks; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.checks (id, organization_id, type_id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8650 (class 0 OID 889648)
-- Dependencies: 692
-- Data for Name: claim_events; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.claim_events (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, receivable_id, event, amount, description) FROM stdin;
\.


--
-- TOC entry 8651 (class 0 OID 889654)
-- Dependencies: 693
-- Data for Name: claim_requests; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.claim_requests (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, receivable_id, location_id, status, report, claim_request_data, patient_id, treatment_id, money_requested) FROM stdin;
\.


--
-- TOC entry 8654 (class 0 OID 889669)
-- Dependencies: 696
-- Data for Name: collection_label_agencies; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.collection_label_agencies (id, sys_version, name, phone_numer, parent_company_id) FROM stdin;
\.


--
-- TOC entry 8655 (class 0 OID 889672)
-- Dependencies: 697
-- Data for Name: collection_label_history; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.collection_label_history (id, sys_version, created_by, created, patient_id, payment_account_id, previous_collection_label_template_id) FROM stdin;
\.


--
-- TOC entry 8656 (class 0 OID 889675)
-- Dependencies: 698
-- Data for Name: collection_label_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.collection_label_templates (id, sys_version, name, parent_company_id, agency_id, deleted, payer_type, is_default) FROM stdin;
\.


--
-- TOC entry 8653 (class 0 OID 889666)
-- Dependencies: 695
-- Data for Name: collection_labels; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.collection_labels (patient_id, payment_account_id, last_modified_by, last_modified, collection_label_template_id) FROM stdin;
\.


--
-- TOC entry 8652 (class 0 OID 889660)
-- Dependencies: 694
-- Data for Name: collections_cache; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.collections_cache (location_id, date, results) FROM stdin;
\.


--
-- TOC entry 8657 (class 0 OID 889681)
-- Dependencies: 699
-- Data for Name: communication_preferences; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.communication_preferences (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, contact_method_association_id, patient_id, type) FROM stdin;
\.


--
-- TOC entry 8658 (class 0 OID 889684)
-- Dependencies: 700
-- Data for Name: contact_emails; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.contact_emails (id, address) FROM stdin;
\.


--
-- TOC entry 8660 (class 0 OID 889691)
-- Dependencies: 702
-- Data for Name: contact_method_associations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.contact_method_associations (id, person_id, contact_method_id, description, preference, error_location_id, error_date, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, contact_order) FROM stdin;
\.


--
-- TOC entry 8659 (class 0 OID 889687)
-- Dependencies: 701
-- Data for Name: contact_methods; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.contact_methods (dtype, id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8661 (class 0 OID 889694)
-- Dependencies: 703
-- Data for Name: contact_phones; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.contact_phones (id, number, type) FROM stdin;
\.


--
-- TOC entry 8662 (class 0 OID 889697)
-- Dependencies: 704
-- Data for Name: contact_postal_addresses; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.contact_postal_addresses (id, address_line1, address_line2, city, zip, state) FROM stdin;
\.


--
-- TOC entry 8663 (class 0 OID 889703)
-- Dependencies: 705
-- Data for Name: contact_recently_opened_items; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.contact_recently_opened_items (id) FROM stdin;
\.


--
-- TOC entry 8664 (class 0 OID 889706)
-- Dependencies: 706
-- Data for Name: contact_website; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.contact_website (id, website_address) FROM stdin;
\.


--
-- TOC entry 8665 (class 0 OID 889709)
-- Dependencies: 707
-- Data for Name: correction_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.correction_types (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, deleted, parent_company_id, positive, negative) FROM stdin;
\.


--
-- TOC entry 8666 (class 0 OID 889715)
-- Dependencies: 708
-- Data for Name: correction_types_locations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.correction_types_locations (correction_type_id, organization_id) FROM stdin;
\.


--
-- TOC entry 8667 (class 0 OID 889718)
-- Dependencies: 709
-- Data for Name: correction_types_payment_options; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.correction_types_payment_options (correction_types_id, payment_options_id) FROM stdin;
\.


--
-- TOC entry 8668 (class 0 OID 889721)
-- Dependencies: 710
-- Data for Name: county; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.county (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, incits_code, name, state) FROM stdin;
\.


--
-- TOC entry 8669 (class 0 OID 889724)
-- Dependencies: 711
-- Data for Name: daily_transactions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.daily_transactions (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, date, location_id, report_data, report_version, location_type) FROM stdin;
\.


--
-- TOC entry 8670 (class 0 OID 889731)
-- Dependencies: 712
-- Data for Name: data_lock; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.data_lock (id, sys_created, session_id, user_id, data_id) FROM stdin;
\.


--
-- TOC entry 8672 (class 0 OID 889741)
-- Dependencies: 714
-- Data for Name: day_schedule_appt_slots; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.day_schedule_appt_slots (day_schedule_id, appt_type_id, start_min) FROM stdin;
\.


--
-- TOC entry 8673 (class 0 OID 889744)
-- Dependencies: 715
-- Data for Name: day_schedule_break_hours; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.day_schedule_break_hours (day_schedule_id, end_min, start_min) FROM stdin;
\.


--
-- TOC entry 8674 (class 0 OID 889747)
-- Dependencies: 716
-- Data for Name: day_schedule_office_hours; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.day_schedule_office_hours (day_schedule_id, end_min, start_min) FROM stdin;
\.


--
-- TOC entry 8671 (class 0 OID 889734)
-- Dependencies: 713
-- Data for Name: day_schedules; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.day_schedules (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, color, deleted, name, organization_id, resource_id, valid_from, day_schedule_nr, overrides_day_schedule) FROM stdin;
\.


--
-- TOC entry 8675 (class 0 OID 889750)
-- Dependencies: 717
-- Data for Name: default_appointment_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.default_appointment_templates (id, location_id, template_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8676 (class 0 OID 889753)
-- Dependencies: 718
-- Data for Name: dentalchart_additional_markings; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dentalchart_additional_markings (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, marking_id, name) FROM stdin;
\.


--
-- TOC entry 8677 (class 0 OID 889756)
-- Dependencies: 719
-- Data for Name: dentalchart_edit_log; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dentalchart_edit_log (id, snapshot_id) FROM stdin;
\.


--
-- TOC entry 8678 (class 0 OID 889759)
-- Dependencies: 720
-- Data for Name: dentalchart_marking; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dentalchart_marking (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, added, completed, canceled, to_tooth_number, tooth_number, type, snapshot_id) FROM stdin;
\.


--
-- TOC entry 8679 (class 0 OID 889762)
-- Dependencies: 721
-- Data for Name: dentalchart_materials; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dentalchart_materials (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, marking_type, name) FROM stdin;
\.


--
-- TOC entry 8680 (class 0 OID 889768)
-- Dependencies: 722
-- Data for Name: dentalchart_snapshot; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dentalchart_snapshot (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, comments, data, mime_type, appointment_id) FROM stdin;
\.


--
-- TOC entry 8681 (class 0 OID 889774)
-- Dependencies: 723
-- Data for Name: diagnosis; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.diagnosis (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, appointment_id, diagnosis_template_id, techincal_letter_generated, layperson_letter_generated, techincal_letter_id, layperson_letter_id) FROM stdin;
\.


--
-- TOC entry 8682 (class 0 OID 889780)
-- Dependencies: 724
-- Data for Name: diagnosis_field_values; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.diagnosis_field_values (diagnosis_id, field_key, field_value) FROM stdin;
\.


--
-- TOC entry 8683 (class 0 OID 889786)
-- Dependencies: 725
-- Data for Name: diagnosis_letters; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.diagnosis_letters (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, diagnosis_id, document_id, document_template_id, recipient_id, address_id) FROM stdin;
\.


--
-- TOC entry 8684 (class 0 OID 889789)
-- Dependencies: 726
-- Data for Name: diagnosis_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.diagnosis_templates (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, version, form_structure_xml, tx_category_id, appointment_type_id, parent_company_id) FROM stdin;
\.


--
-- TOC entry 8686 (class 0 OID 889802)
-- Dependencies: 728
-- Data for Name: discount_adjustments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.discount_adjustments (id, discount_id, discount_adjustment_type) FROM stdin;
\.


--
-- TOC entry 8687 (class 0 OID 889805)
-- Dependencies: 729
-- Data for Name: discount_reverse_adjustments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.discount_reverse_adjustments (id, discount_adjustment_id) FROM stdin;
\.


--
-- TOC entry 8685 (class 0 OID 889795)
-- Dependencies: 727
-- Data for Name: discounts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.discounts (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, organization_id, default_amount, default_percentage, fixed, valid_from, valid_to, tx_fee, type, deleted, affects_production) FROM stdin;
\.


--
-- TOC entry 8688 (class 0 OID 889808)
-- Dependencies: 730
-- Data for Name: dock_item_descriptors; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dock_item_descriptors (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, height, owner, x, y, width) FROM stdin;
\.


--
-- TOC entry 8689 (class 0 OID 889811)
-- Dependencies: 731
-- Data for Name: dock_item_features_events; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dock_item_features_events (calendar_id, event_id, id) FROM stdin;
\.


--
-- TOC entry 8690 (class 0 OID 889817)
-- Dependencies: 732
-- Data for Name: dock_item_features_images; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dock_item_features_images (image_id, edit_mode, id, patient_id) FROM stdin;
\.


--
-- TOC entry 8691 (class 0 OID 889820)
-- Dependencies: 733
-- Data for Name: dock_item_features_messages; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dock_item_features_messages (thread_id, id) FROM stdin;
\.


--
-- TOC entry 8692 (class 0 OID 889823)
-- Dependencies: 734
-- Data for Name: dock_item_features_note; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dock_item_features_note (id, note_id) FROM stdin;
\.


--
-- TOC entry 8693 (class 0 OID 889826)
-- Dependencies: 735
-- Data for Name: dock_item_features_tasks; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.dock_item_features_tasks (list_id, task_id, id) FROM stdin;
\.


--
-- TOC entry 8694 (class 0 OID 889832)
-- Dependencies: 736
-- Data for Name: document_recently_opened_items; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.document_recently_opened_items (drive_id, id) FROM stdin;
\.


--
-- TOC entry 8695 (class 0 OID 889835)
-- Dependencies: 737
-- Data for Name: document_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.document_templates (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, description, file_key, organization_id, mime_type, is_contract) FROM stdin;
\.


--
-- TOC entry 8696 (class 0 OID 889842)
-- Dependencies: 738
-- Data for Name: document_tree_nodes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.document_tree_nodes (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, parent_id, patient_id, type, file_uid, name, size, icon, thumbs, meta, deleted, created_from) FROM stdin;
\.


--
-- TOC entry 8697 (class 0 OID 889850)
-- Dependencies: 739
-- Data for Name: eeo; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.eeo (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, jobtitle) FROM stdin;
\.


--
-- TOC entry 8698 (class 0 OID 889853)
-- Dependencies: 740
-- Data for Name: employee_beneficiary; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_beneficiary (id, share_percent, type, employee_enrollment_person_id, employee_beneficiary_group_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, qle_parent) FROM stdin;
\.


--
-- TOC entry 8699 (class 0 OID 889856)
-- Dependencies: 741
-- Data for Name: employee_beneficiary_group; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_beneficiary_group (id, employee_enrollment_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, qle_parent) FROM stdin;
\.


--
-- TOC entry 8700 (class 0 OID 889859)
-- Dependencies: 742
-- Data for Name: employee_benefit; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_benefit (id, benefit_type, pre_tax_rate, post_tax_rate, employer_rate, total_rate, covarage_level, other_fields, employee_enrollment_id, insurance_plan_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, qle_parent, is_waived, benefit_enrollment_status, is_disabled, waive_reason, payroll_cycle, biling_start_date, biling_end_date, other_carrier_name, carrier_person_name, other_coverage_level) FROM stdin;
\.


--
-- TOC entry 8701 (class 0 OID 889867)
-- Dependencies: 743
-- Data for Name: employee_dependent; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_dependent (id, gender, ssn, is_disabled_child, is_address_the_same, is_fulltime_student, street, city, zip_code, state, country, employee_enrollment_person_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, employee_enrollment_id, qle_parent, primary_phone, primary_phone_type, secondary_phone, secondary_phone_type, primary_email, primary_email_type, secondary_email, secondary_email_type) FROM stdin;
\.


--
-- TOC entry 8702 (class 0 OID 889873)
-- Dependencies: 744
-- Data for Name: employee_enrollment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_enrollment (id, status, fill_date, summary_document_id, enrollment_id, enrollment_form_source_event_id, employment_contracts_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, qle_type, qle_occurrence_date, qle_parent, other_qle_description, first_name, last_name, middle_name, total_annualized_earnings, gender, birth_date, effective_coverage_start, fill_date_offset, confirmed_event_date, deny_acknowledged, confirmed, sys_portal_last_modified, sys_portal_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8703 (class 0 OID 889882)
-- Dependencies: 745
-- Data for Name: employee_enrollment_attachment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_enrollment_attachment (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, type, file_id, file_mime_type, file_name, file_size, employee_enrollment_id) FROM stdin;
\.


--
-- TOC entry 8704 (class 0 OID 889888)
-- Dependencies: 746
-- Data for Name: employee_enrollment_event; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_enrollment_event (id, employee_enrollment_id, old_status, new_status, username, comment, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8705 (class 0 OID 889894)
-- Dependencies: 747
-- Data for Name: employee_enrollment_person; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_enrollment_person (id, first_name, middle_name, last_name, date_of_birth, relationship, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, phone_number, email, qle_parent, employee_enrollment_id, organization_name, organization_form_date, resp_person_name, resp_person_city, resp_person_state, type) FROM stdin;
\.


--
-- TOC entry 8706 (class 0 OID 889900)
-- Dependencies: 748
-- Data for Name: employee_insured; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_insured (id, employee_beneficiary_group_id, employee_dependent_id, employee_benefit_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, qle_parent, smoker) FROM stdin;
\.


--
-- TOC entry 8707 (class 0 OID 889904)
-- Dependencies: 749
-- Data for Name: employee_professional_relationships; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_professional_relationships (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, provider_id) FROM stdin;
\.


--
-- TOC entry 8708 (class 0 OID 889907)
-- Dependencies: 750
-- Data for Name: employee_resources; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employee_resources (id, employment_contract_id, invalid) FROM stdin;
\.


--
-- TOC entry 8709 (class 0 OID 889911)
-- Dependencies: 751
-- Data for Name: employers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employers (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, master_id, organization_id, contact_postal_address_id, contact_phone_id, contact_email_id, name) FROM stdin;
\.


--
-- TOC entry 8711 (class 0 OID 889921)
-- Dependencies: 753
-- Data for Name: employment_contract_emergency_contact; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employment_contract_emergency_contact (id, first_name, middle_name, last_name, email, phone_number, relationship, type, employment_contracts_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8712 (class 0 OID 889924)
-- Dependencies: 754
-- Data for Name: employment_contract_enrollment_group_log; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employment_contract_enrollment_group_log (id, sys_version, employment_contracts_id, old_enrollment_group_id, new_enrollment_group_id, date_of_occurance, commnet) FROM stdin;
\.


--
-- TOC entry 8713 (class 0 OID 889930)
-- Dependencies: 755
-- Data for Name: employment_contract_job_status_log; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employment_contract_job_status_log (id, sys_version, employment_contracts_id, old_job_status, new_job_status, date_of_occurance) FROM stdin;
\.


--
-- TOC entry 8714 (class 0 OID 889933)
-- Dependencies: 756
-- Data for Name: employment_contract_location_group; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employment_contract_location_group (id, employment_contract_id, location_group_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, sys_organizations_id) FROM stdin;
\.


--
-- TOC entry 8715 (class 0 OID 889936)
-- Dependencies: 757
-- Data for Name: employment_contract_permissions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employment_contract_permissions (id, employment_contract_id, permissions_permission_id, access_type, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, sys_organizations_id) FROM stdin;
\.


--
-- TOC entry 8716 (class 0 OID 889939)
-- Dependencies: 758
-- Data for Name: employment_contract_role; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employment_contract_role (id, employment_contract_id, permissions_user_role_id, sys_organizations_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8717 (class 0 OID 889942)
-- Dependencies: 759
-- Data for Name: employment_contract_salary_log; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employment_contract_salary_log (id, sys_version, employment_contracts_id, salary_structure, salary, day_rate, hourly_rate, avg_hours_per_week, avg_days_per_week, add_annual_compensation, total_annual_earnings, ltd_bonus_up_annualized, annualized_commission, date_of_occurance, payroll_cycle, commnet) FROM stdin;
\.


--
-- TOC entry 8710 (class 0 OID 889914)
-- Dependencies: 752
-- Data for Name: employment_contracts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.employment_contracts (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, legacy_id, organization_id, person_id, company, department, location, employment_start_date, employment_end_date, job_status, job_class, job_title, enrollment_class, salary, annualized_commission, ltd_bonus_up_annualized, signature, human_id, npi, tin, medicaid_id, hourly_rate, day_rate, avg_hours_per_week, avg_days_per_week, salary_structure, add_annual_compensation, total_annual_earnings, current_employment_start_date, payroll_cycle, legal_entity_id, physical_address_id, department_id, division_id, enrollment_group_id, billing_group_id, jobtitle_id, coverage_period, deleted) FROM stdin;
\.


--
-- TOC entry 8718 (class 0 OID 889948)
-- Dependencies: 760
-- Data for Name: enrollment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, effective_coverage_start, effective_coverage_end, year, oe_start, oe_end, oe_email_template, oe_sms_template, nh_email_template, nh_sms_template, legal_entity_id, email_reminder, sms_reminder, new_hire_effective_covearge_definition, renewal_date, expiration_date, cancellation_date, status, last_enrollment_id) FROM stdin;
\.


--
-- TOC entry 8720 (class 0 OID 889956)
-- Dependencies: 762
-- Data for Name: enrollment_form; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_form (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, enrollment_id, employee_id, status, fill_date, dependents_confirmation, confirmation_document_id) FROM stdin;
\.


--
-- TOC entry 8722 (class 0 OID 889962)
-- Dependencies: 764
-- Data for Name: enrollment_form_beneficiary; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_form_beneficiary (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, enrollment_form_id, beneficiary_data) FROM stdin;
\.


--
-- TOC entry 8724 (class 0 OID 889971)
-- Dependencies: 766
-- Data for Name: enrollment_form_benefits; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_form_benefits (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, enrollment_form_id, benefit_type, benefit_data) FROM stdin;
\.


--
-- TOC entry 8726 (class 0 OID 889980)
-- Dependencies: 768
-- Data for Name: enrollment_form_dependents; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_form_dependents (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, enrollment_form_id, dependent_data) FROM stdin;
\.


--
-- TOC entry 8728 (class 0 OID 889989)
-- Dependencies: 770
-- Data for Name: enrollment_form_dependents_benefits; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_form_dependents_benefits (id, enrollment_form_benefit_id, enrollment_form_dependent_id, benefit_data) FROM stdin;
\.


--
-- TOC entry 8730 (class 0 OID 889998)
-- Dependencies: 772
-- Data for Name: enrollment_form_personal_data; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_form_personal_data (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, enrollment_form_id, personal_data) FROM stdin;
\.


--
-- TOC entry 8731 (class 0 OID 890005)
-- Dependencies: 773
-- Data for Name: enrollment_form_source_event; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_form_source_event (id, source_type, sub_type, configuration, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8732 (class 0 OID 890011)
-- Dependencies: 774
-- Data for Name: enrollment_group; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_group (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, group_type) FROM stdin;
\.


--
-- TOC entry 8734 (class 0 OID 890017)
-- Dependencies: 776
-- Data for Name: enrollment_group_enrollment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_group_enrollment (enrollment_group_id, enrollment_id) FROM stdin;
\.


--
-- TOC entry 8733 (class 0 OID 890014)
-- Dependencies: 775
-- Data for Name: enrollment_groups_enrollment_legal_entity; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_groups_enrollment_legal_entity (enrollment_group_id, legal_entity_id, enrollment_id) FROM stdin;
\.


--
-- TOC entry 8735 (class 0 OID 890020)
-- Dependencies: 777
-- Data for Name: enrollment_schedule_billing_groups; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.enrollment_schedule_billing_groups (enrollment_id, carrier, legal_entity_id, billing_group) FROM stdin;
\.


--
-- TOC entry 8736 (class 0 OID 890023)
-- Dependencies: 778
-- Data for Name: equipment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.equipment (name, id) FROM stdin;
\.


--
-- TOC entry 8737 (class 0 OID 890027)
-- Dependencies: 779
-- Data for Name: external_offices; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.external_offices (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, postal_address_id) FROM stdin;
\.


--
-- TOC entry 8738 (class 0 OID 890030)
-- Dependencies: 780
-- Data for Name: financial_settings; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.financial_settings (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, payment_term, autopay_payment_term, realization, realization_dom) FROM stdin;
\.


--
-- TOC entry 8739 (class 0 OID 890036)
-- Dependencies: 781
-- Data for Name: fix_adjustments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.fix_adjustments (id, from_migration) FROM stdin;
\.


--
-- TOC entry 8740 (class 0 OID 890040)
-- Dependencies: 782
-- Data for Name: gf10219; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.gf10219 (patient_id, appointment_id, tx_id, wrong_next_appointment_id, wrong_tx_id, correct_next_appointment_id, correct_next_tx_id) FROM stdin;
\.


--
-- TOC entry 8741 (class 0 OID 890043)
-- Dependencies: 783
-- Data for Name: gf9412_payments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.gf9412_payments (human_id, patient_id, patient_name, insured_id, payment_account_id_ok, company_ok, payment_id, payment_sys_created, payer_name, payment_account_id_wrong, migrated) FROM stdin;
\.


--
-- TOC entry 8742 (class 0 OID 890050)
-- Dependencies: 784
-- Data for Name: gf9412_receivables; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.gf9412_receivables (human_id, patient_id, patient_name, receivable_id, rtype, rec_sys_created, insured_id, payment_account_id_ok, company_ok, receivable_account_id_wrong, company_wrong) FROM stdin;
\.


--
-- TOC entry 8743 (class 0 OID 890056)
-- Dependencies: 785
-- Data for Name: gf9412_unapplied; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.gf9412_unapplied (human_id, patient_id, patient_name, payment_id, payment_account_id_ok, payment_account_id_wrong, account_type, id, name, billing_center_name, payer_name) FROM stdin;
\.


--
-- TOC entry 8745 (class 0 OID 890065)
-- Dependencies: 787
-- Data for Name: hr_admin_config; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.hr_admin_config (id, sys_version, name, value) FROM stdin;
\.


--
-- TOC entry 8744 (class 0 OID 890062)
-- Dependencies: 786
-- Data for Name: hradmin_enrollment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.hradmin_enrollment (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, start_date, end_date, mode, company_name, tax_id, sic_code, street, state, city, zip_code) FROM stdin;
\.


--
-- TOC entry 8746 (class 0 OID 890068)
-- Dependencies: 788
-- Data for Name: image_series; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.image_series (id, organization_id, type_id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8747 (class 0 OID 890071)
-- Dependencies: 789
-- Data for Name: image_series_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.image_series_types (id, name, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8798 (class 0 OID 890265)
-- Dependencies: 840
-- Data for Name: in_network_discounts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.in_network_discounts (id, network_sheet_fee_id) FROM stdin;
\.


--
-- TOC entry 8748 (class 0 OID 890074)
-- Dependencies: 790
-- Data for Name: installment_charges; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.installment_charges (id, payment_plan_id) FROM stdin;
\.


--
-- TOC entry 8749 (class 0 OID 890077)
-- Dependencies: 791
-- Data for Name: insurance_billing_centers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_billing_centers (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, payer_id, contact_postal_address_id, organization_id, contact_phone_id, contact_email_id, master_id) FROM stdin;
\.


--
-- TOC entry 8750 (class 0 OID 890083)
-- Dependencies: 792
-- Data for Name: insurance_claims_setup; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_claims_setup (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_organizations_id, billing_entity_id, legal_entity_npi_id, sys_organization_address_id, sys_organization_contact_method_id, license_number_type, license_number_value, ssn, tin, tin_id) FROM stdin;
\.


--
-- TOC entry 8752 (class 0 OID 890093)
-- Dependencies: 794
-- Data for Name: insurance_code_val; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_code_val (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, coverage, fee, insurance_code_id, insurance_company_id, organization_id) FROM stdin;
\.


--
-- TOC entry 8751 (class 0 OID 890087)
-- Dependencies: 793
-- Data for Name: insurance_codes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_codes (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, code, title) FROM stdin;
\.


--
-- TOC entry 8753 (class 0 OID 890096)
-- Dependencies: 795
-- Data for Name: insurance_companies; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_companies (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, carrier_name, electronic, website, installment_interval, continuation_claims_required, postal_address_id, am_name, am_phone_number_id, am_email_address_id, master_id, medicaid, medicaid_state) FROM stdin;
\.


--
-- TOC entry 8754 (class 0 OID 890104)
-- Dependencies: 796
-- Data for Name: insurance_company_payments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_company_payments (insurance_company_id, company_name) FROM stdin;
\.


--
-- TOC entry 8755 (class 0 OID 890107)
-- Dependencies: 797
-- Data for Name: insurance_company_phone_associations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_company_phone_associations (id, insurance_company_id, contact_method_id, description, preferred, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8757 (class 0 OID 890117)
-- Dependencies: 799
-- Data for Name: insurance_payment_accounts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_payment_accounts (id, organization_id, insurance_company_id) FROM stdin;
\.


--
-- TOC entry 8758 (class 0 OID 890120)
-- Dependencies: 800
-- Data for Name: insurance_payment_corrections; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_payment_corrections (id, correction_type_id) FROM stdin;
\.


--
-- TOC entry 8759 (class 0 OID 890123)
-- Dependencies: 801
-- Data for Name: insurance_payment_movements; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_payment_movements (id, sys_created_by, sys_created, sys_created_at, sys_last_modified, sys_last_modified_by, sys_version, dtype, insurance_payment_id, moved_on, moved_at, amount, notes) FROM stdin;
\.


--
-- TOC entry 8760 (class 0 OID 890129)
-- Dependencies: 802
-- Data for Name: insurance_payment_refunds; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_payment_refunds (id, refund_type, payment_method, payment_method_number) FROM stdin;
\.


--
-- TOC entry 8761 (class 0 OID 890132)
-- Dependencies: 803
-- Data for Name: insurance_payment_transfers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_payment_transfers (id, payment_id) FROM stdin;
\.


--
-- TOC entry 8756 (class 0 OID 890111)
-- Dependencies: 798
-- Data for Name: insurance_payments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_payments (id, human_id, sys_created_by, sys_created, sys_created_at, sys_last_modified, sys_last_modified_by, sys_version, insurance_company_id, dtype, posted_on, posted_at, amount, payment_type_id, payer_name, check_or_card_number, cc_expire, notes) FROM stdin;
\.


--
-- TOC entry 8762 (class 0 OID 890135)
-- Dependencies: 804
-- Data for Name: insurance_plan; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan (id, benefit_template_id, record_type, plan_name, other_fields, benefit_type, carrier, summary_plan_pdf, coverage_levels, employer, employee_clasification, enrollment_class, payroll_deduction, premium_allocation_type, premium_allocation_employer_value, rate_type, wave_allowed, wave_with_reason, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, sys_organizations_id, renewal_date, policy_number, inception_date, smoker_definition, deleted, spouse_premium_calculation, benefit_amount_type, parent_insurance_plan_id, summary_plan_pdf_file_name, prior_policy_number) FROM stdin;
\.


--
-- TOC entry 8763 (class 0 OID 890142)
-- Dependencies: 805
-- Data for Name: insurance_plan_adandd; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_adandd (id, insurance_plan_id, employee_rate, spouse_rate, child_rate, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8764 (class 0 OID 890145)
-- Dependencies: 806
-- Data for Name: insurance_plan_age_and_gender_banded_rate; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_age_and_gender_banded_rate (id, min_age, max_age, gender, only_me_rate, me_and_spouse_rate, me_and_chidren_rate, family_rate, tabaco, me_and_one_child, only_me_rate_tobacco, me_and_spouse_rate_tobacco, me_and_chidren_rate_tobacco, family_rate_tobacco, me_and_one_child_tobacco) FROM stdin;
\.


--
-- TOC entry 8765 (class 0 OID 890148)
-- Dependencies: 807
-- Data for Name: insurance_plan_age_banded_rate; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_age_banded_rate (id, min_age, max_age, rate, tabaco, tobacco_rate) FROM stdin;
\.


--
-- TOC entry 8766 (class 0 OID 890151)
-- Dependencies: 808
-- Data for Name: insurance_plan_age_reduction; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_age_reduction (id, insurance_plan_id, min_reduced_amt, min_reduction_amt, applies_to_employee, applies_to_spouse, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8767 (class 0 OID 890154)
-- Dependencies: 809
-- Data for Name: insurance_plan_age_reduction_detail; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_age_reduction_detail (id, insurance_plan_age_reduction_id, age, reduction, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8768 (class 0 OID 890157)
-- Dependencies: 810
-- Data for Name: insurance_plan_benefit_amount; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_benefit_amount (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, rounding_rule, rounding_rule_value, insurance_plan_id, type) FROM stdin;
\.


--
-- TOC entry 8769 (class 0 OID 890160)
-- Dependencies: 811
-- Data for Name: insurance_plan_children_rate; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_children_rate (id, rate, tobacco_rate, coverage_type) FROM stdin;
\.


--
-- TOC entry 8770 (class 0 OID 890163)
-- Dependencies: 812
-- Data for Name: insurance_plan_complex_benefit_amount; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_complex_benefit_amount (id) FROM stdin;
\.


--
-- TOC entry 8771 (class 0 OID 890166)
-- Dependencies: 813
-- Data for Name: insurance_plan_complex_benefit_amount_item; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_complex_benefit_amount_item (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, insured_type, min_amount_value, max_amount_type, max_amount_money_value, max_amount_percentage_value, max_amount_earnings_multiply_value, increment_amounts, insurance_plan_complex_benefit_amount_id) FROM stdin;
\.


--
-- TOC entry 8772 (class 0 OID 890169)
-- Dependencies: 814
-- Data for Name: insurance_plan_composition_detailed_rate; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_composition_detailed_rate (id, employee_amount, spouse_amount, child_amount) FROM stdin;
\.


--
-- TOC entry 8773 (class 0 OID 890172)
-- Dependencies: 815
-- Data for Name: insurance_plan_composition_rate; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_composition_rate (id, only_me_amount, me_and_spouse_rate, me_and_chidren_amount, family_amount, me_and_one_child) FROM stdin;
\.


--
-- TOC entry 8774 (class 0 OID 890175)
-- Dependencies: 816
-- Data for Name: insurance_plan_employers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_employers (insurance_plan_id, employer_id) FROM stdin;
\.


--
-- TOC entry 8775 (class 0 OID 890178)
-- Dependencies: 817
-- Data for Name: insurance_plan_fixed_limit; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_fixed_limit (id, amount_min, amount_max, insurance_limits_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, alert_limit, step) FROM stdin;
\.


--
-- TOC entry 8776 (class 0 OID 890183)
-- Dependencies: 818
-- Data for Name: insurance_plan_guaranteed_increase; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_guaranteed_increase (id, insurance_plan_id, enabled, increment, applies_to_employee, applies_to_spouse, applies_to_child, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8777 (class 0 OID 890186)
-- Dependencies: 819
-- Data for Name: insurance_plan_guaranteed_issue; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_guaranteed_issue (id, insurance_plan_id, enabled, amount, availability, first_time, grandfather_rule, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, amount_child, amount_spouse, availability_child, availability_spouse, enabled_me, enabled_spouse, enabled_child) FROM stdin;
\.


--
-- TOC entry 8778 (class 0 OID 890189)
-- Dependencies: 820
-- Data for Name: insurance_plan_has_enrollment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_has_enrollment (id, sys_version, insurance_plan_id, enrollment_id, status, removed) FROM stdin;
\.


--
-- TOC entry 8779 (class 0 OID 890193)
-- Dependencies: 821
-- Data for Name: insurance_plan_has_enrollment_has_enrollment_group; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_has_enrollment_has_enrollment_group (insurance_plan_has_enrollment_id, enrollment_group_id) FROM stdin;
\.


--
-- TOC entry 8780 (class 0 OID 890196)
-- Dependencies: 822
-- Data for Name: insurance_plan_limits; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_limits (id, type, insurance_plan_id, benefit_type, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8781 (class 0 OID 890199)
-- Dependencies: 823
-- Data for Name: insurance_plan_medical_connection; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_medical_connection (id, insurance_plan_id, enabled, applies_to_employee, applies_to_spouse, applies_to_child, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8782 (class 0 OID 890202)
-- Dependencies: 824
-- Data for Name: insurance_plan_misc; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_misc (id, insurance_plan_id, is_bonus_up, offer_adandd, payments_begin, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8783 (class 0 OID 890205)
-- Dependencies: 825
-- Data for Name: insurance_plan_rates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_rates (id, type, insurance_plan_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8784 (class 0 OID 890208)
-- Dependencies: 826
-- Data for Name: insurance_plan_saving_account; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_saving_account (insurance_plan_id, id, name, limit_amt, employer_cost, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, enabled, employer_contributes, summary_plan_pdf, type, summary_plan_pdf_file_name, tpa) FROM stdin;
\.


--
-- TOC entry 8785 (class 0 OID 890214)
-- Dependencies: 827
-- Data for Name: insurance_plan_saving_accounts_limits; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_saving_accounts_limits (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, year, hsa_individual_limit, hsa_family_limit, hsa_catch_up, fsa_health, fsa_dependent_care) FROM stdin;
\.


--
-- TOC entry 8786 (class 0 OID 890217)
-- Dependencies: 828
-- Data for Name: insurance_plan_separate_age_banded_rate; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_separate_age_banded_rate (id, min_age, max_age, rate, tabaco, tobacco_rate, insured_type) FROM stdin;
\.


--
-- TOC entry 8787 (class 0 OID 890220)
-- Dependencies: 829
-- Data for Name: insurance_plan_simple_benefit_amount; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_simple_benefit_amount (employee_amount_type, employee_amount_flat_value, employ_amount_multiply_value, employee_min_amount_value, employee_max_amount_value, spouse_amount_value, children_amount_value, id) FROM stdin;
\.


--
-- TOC entry 8788 (class 0 OID 890223)
-- Dependencies: 830
-- Data for Name: insurance_plan_slider_limit; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_slider_limit (id, min_value, max_value_in_percent, step, rounding, insurance_limits_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, alert_limit) FROM stdin;
\.


--
-- TOC entry 8789 (class 0 OID 890226)
-- Dependencies: 831
-- Data for Name: insurance_plan_summary_fields; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_summary_fields (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, field_name, value0, value1, value2, value3, item_order, is_visible, ui_type, is_default, insurance_plan_id) FROM stdin;
\.


--
-- TOC entry 8790 (class 0 OID 890232)
-- Dependencies: 832
-- Data for Name: insurance_plan_summary_fields_default; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_summary_fields_default (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, field_name, value0, value1, value2, value3, item_order, is_visible, ui_type, insurance_plan_type) FROM stdin;
\.


--
-- TOC entry 8791 (class 0 OID 890238)
-- Dependencies: 833
-- Data for Name: insurance_plan_term_disability_benefit_amount; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_plan_term_disability_benefit_amount (id, annual_earning_percent, term_disability_benefit_option, min_amount_value, increment_amount, benefit_dration, max_amount_value) FROM stdin;
\.


--
-- TOC entry 8792 (class 0 OID 890241)
-- Dependencies: 834
-- Data for Name: insurance_subscriptions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insurance_subscriptions (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, deleted, insurance_plan_id, employer_id, notes, member_id, active, enrollment_date, person_id) FROM stdin;
\.


--
-- TOC entry 8793 (class 0 OID 890249)
-- Dependencies: 835
-- Data for Name: insured; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insured (id, sys_version, deleted, patient_id, notes, insurance_subscription_id) FROM stdin;
\.


--
-- TOC entry 8794 (class 0 OID 890253)
-- Dependencies: 836
-- Data for Name: insured_benefit_values; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.insured_benefit_values (id, pre_tax_rate, post_tax_rate, employer_rate, total_rate, benefit_amout, employee_insured_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, qle_parent, reduction, reduced_benefit_amount) FROM stdin;
\.


--
-- TOC entry 8795 (class 0 OID 890256)
-- Dependencies: 837
-- Data for Name: invitations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.invitations (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, event, invitee, inviter, updated) FROM stdin;
\.


--
-- TOC entry 8796 (class 0 OID 890259)
-- Dependencies: 838
-- Data for Name: invoice_export_queue; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.invoice_export_queue (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, target, invoice_id, account_id) FROM stdin;
\.


--
-- TOC entry 8797 (class 0 OID 890262)
-- Dependencies: 839
-- Data for Name: invoice_import_export_log; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.invoice_import_export_log (id, type, account_id, invoice_id) FROM stdin;
\.


--
-- TOC entry 8799 (class 0 OID 890268)
-- Dependencies: 841
-- Data for Name: jobtitle; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.jobtitle (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, sys_organizations_id, jobtitle_group_id, mediacal_provider) FROM stdin;
\.


--
-- TOC entry 8800 (class 0 OID 890271)
-- Dependencies: 842
-- Data for Name: jobtitle_group; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.jobtitle_group (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, eeo_id) FROM stdin;
\.


--
-- TOC entry 8801 (class 0 OID 890274)
-- Dependencies: 843
-- Data for Name: late_fee_charges; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.late_fee_charges (id) FROM stdin;
\.


--
-- TOC entry 8802 (class 0 OID 890277)
-- Dependencies: 844
-- Data for Name: legal_entities_participating_enrollment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entities_participating_enrollment (legal_entity_id, enrollment_id) FROM stdin;
\.


--
-- TOC entry 8803 (class 0 OID 890280)
-- Dependencies: 845
-- Data for Name: legal_entity; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_organizations_id, parent_id, relationship, structure_type, structure_subtype, legal_name, sic_code, naics2012_code, fein, county_id, state, status, has_employees, exp_empl_agricultural, exp_empl_household, exp_empl_other, biling_groups_setup, available_payroll_cycle, allow_new_hires) FROM stdin;
\.


--
-- TOC entry 8804 (class 0 OID 890290)
-- Dependencies: 846
-- Data for Name: legal_entity_address; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_address (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, type, street, city, state, zip_code, occupacy, legal_entity_id) FROM stdin;
\.


--
-- TOC entry 8805 (class 0 OID 890293)
-- Dependencies: 847
-- Data for Name: legal_entity_address_attachment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_address_attachment (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, type, file_id, file_mime_type, file_name, file_size, legal_entity_address_id) FROM stdin;
\.


--
-- TOC entry 8806 (class 0 OID 890299)
-- Dependencies: 848
-- Data for Name: legal_entity_billing_group; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_billing_group (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, legal_entity_id, name) FROM stdin;
\.


--
-- TOC entry 8807 (class 0 OID 890302)
-- Dependencies: 849
-- Data for Name: legal_entity_billing_group_has_sys_organizations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_billing_group_has_sys_organizations (legal_entity_billing_group_id, sys_organizations_id, id, sys_version) FROM stdin;
\.


--
-- TOC entry 8808 (class 0 OID 890305)
-- Dependencies: 850
-- Data for Name: legal_entity_dba; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_dba (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, legal_entity_id, dba_name) FROM stdin;
\.


--
-- TOC entry 8809 (class 0 OID 890308)
-- Dependencies: 851
-- Data for Name: legal_entity_insurance_benefit; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_insurance_benefit (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, legal_entity_id, benefits_holder, broker_id) FROM stdin;
\.


--
-- TOC entry 8810 (class 0 OID 890311)
-- Dependencies: 852
-- Data for Name: legal_entity_insurance_benefit_has_division; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_insurance_benefit_has_division (legal_entity_insurance_benefit_id, division_id) FROM stdin;
\.


--
-- TOC entry 8811 (class 0 OID 890314)
-- Dependencies: 853
-- Data for Name: legal_entity_npi; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_npi (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, name, description, legal_entity_id) FROM stdin;
\.


--
-- TOC entry 8812 (class 0 OID 890317)
-- Dependencies: 854
-- Data for Name: legal_entity_operational_structures; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_operational_structures (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, is_hr, hr_type, is_ib, is_pm, is_acc, legal_entity_id) FROM stdin;
\.


--
-- TOC entry 8813 (class 0 OID 890320)
-- Dependencies: 855
-- Data for Name: legal_entity_owner; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_owner (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, legal_entity_id, type, name, name_2, fein, ssn, birth_date, percentage) FROM stdin;
\.


--
-- TOC entry 8814 (class 0 OID 890323)
-- Dependencies: 856
-- Data for Name: legal_entity_tin; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.legal_entity_tin (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, name, description, legal_entity_id) FROM stdin;
\.


--
-- TOC entry 8815 (class 0 OID 890326)
-- Dependencies: 857
-- Data for Name: lightbarcalls; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.lightbarcalls (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, calling_since, urgent, booking_id, chair_id, resource_id) FROM stdin;
\.


--
-- TOC entry 8816 (class 0 OID 890329)
-- Dependencies: 858
-- Data for Name: location_access_keys; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.location_access_keys (id, sys_created, sys_created_by, sys_last_modified, sys_last_modified_by, sys_version, organization_id, access_name, access_key, enabled, accepts_medicaid) FROM stdin;
\.


--
-- TOC entry 8817 (class 0 OID 890335)
-- Dependencies: 859
-- Data for Name: medical_history; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.medical_history (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, abnormal_breathing, additional_info, aids, aids_alert, arthritis, arthritis_alert, asthma, asthma_alert, blood_disorders, blood_disorders_alert, brain_injury, brain_injury_alert, clicking_pain_jaw, clicking_pain_jaw_alert, diabetes, diabetes_alert, dizziness_fainting, dizziness_fainting_alert, epilepsy, epilepsy_alert, family_member_same_condition, family_member_same_condition_alert, first_tooth_age, frequent_colds, frequent_colds_alert, grinding_teeth, hearing_difficulties, hearing_difficulties_alert, heart_trouble, heart_trouble_alert, hepatitis, hepatitis_alert, in_good_health, in_good_health_alert, kidney_liver_disease, kidney_liver_disease_alert, last_dental_care, lip_biting, mouth_breathing, nail_biting, other_conditions, other_conditions_alert, parents_teeth_removed_crowding, parents_teeth_removed_crowding_alert, rheumatic_fever, rheumatic_fever_alert, smoking, tobacco, speech_disorders, tb, tb_alert, thumb_sucking, tongue_biting, tongue_sucking, tongue_thrusting, under_care_physician, under_care_physician_alert, unfavourable_reactions, unfavourable_reactions_alert, has_general_dentist, general_dentist_first_name, general_dentist_phone, week_floss, day_brush, toothbrush, birth_control, birth_control_alert, pregnant, pregnant_alert, pregnancy_week, nursing, nursing_alert, active_dental_insurance, secondary_insurance_card, dental_health, like_smile, gums, gums_alert, seizures, seizures_alert, antibiotics, antibiotics_alert, problems_previous_procedures, problems_previous_procedures_alert, personal_physician_last_visit, periodontal_disease, periodontal_disease_alert, general_health, current_in_pain, clicking_pain, under_care_physician_phone, physician_active_care, add_or_adhd, add_or_adhd_alert, anemia, anemia_alert, artificial_bones_or_joints_or_valves, artificial_bones_or_joints_or_valves_alert, autism_or_sensory, autism_or_sensory_alert, cancer, cancer_alert, heart_congenital_defect, heart_congenital_defect_alert, drug_alcohol_abuse, drug_alcohol_abuse_alert, emphysema, emphysema_alert, herpes, herpes_alert, frequently_tired, frequently_tired_alert, glaucoma, glaucoma_alert, handicap_or_disabilities, handicap_or_disabilities_alert, hearing_impairment, hearing_impairment_alert, heart_attack, heart_attack_alert, heart_murmur, heart_murmur_alert, heart_surgery, heart_surgery_alert, hemophilia_or_abnormal_bleeding, low_blood_pressure_alert, hemophilia_or_abnormal_bleeding_alert, high_blood_pressure, high_blood_pressure_alert, low_blood_pressure, kidney_diseases, kidney_diseases_alert, mitral_valve_prolapse, mitral_valve_prolapse_alert, psychiatric_problem, psychiatric_problem_alert, radiation_therapy, radiation_therapy_alert, shingles, shingles_alert, sickle_cell_disease, sickle_cell_disease_alert, stomach_troubles_or_ulcers, stomach_troubles_or_ulcers_alert, stroke, stroke_alert, thyroid_problem, thyroid_problem_alert, tuberculosis, tuberculosis_alert, cancer_medication, cancer_medication_alert, respiratory_problems, respiratory_problems_alert, patient_id, created_from, chief_complaint) FROM stdin;
\.


--
-- TOC entry 8818 (class 0 OID 890430)
-- Dependencies: 860
-- Data for Name: medical_history_medications; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.medical_history_medications (id, sys_version, alert, relation_type, medication_id, medical_history_id) FROM stdin;
\.


--
-- TOC entry 8820 (class 0 OID 890435)
-- Dependencies: 862
-- Data for Name: medical_history_patient_relatives_crowding; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.medical_history_patient_relatives_crowding (id, medical_history_id, patient_relative_crowding, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8821 (class 0 OID 890439)
-- Dependencies: 863
-- Data for Name: medications; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.medications (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, deleted) FROM stdin;
\.


--
-- TOC entry 8822 (class 0 OID 890443)
-- Dependencies: 864
-- Data for Name: merchant_reports; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.merchant_reports (id, sys_version, location_id, check_date, run_date, success, note) FROM stdin;
\.


--
-- TOC entry 8823 (class 0 OID 890449)
-- Dependencies: 865
-- Data for Name: migrated_answers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_answers (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, answer_id, answer_guid, question_guid, answer) FROM stdin;
\.


--
-- TOC entry 8825 (class 0 OID 890455)
-- Dependencies: 867
-- Data for Name: migrated_appointment_fields; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_appointment_fields (appointment_id, fields_guid) FROM stdin;
\.


--
-- TOC entry 8826 (class 0 OID 890458)
-- Dependencies: 868
-- Data for Name: migrated_appointment_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_appointment_types (appointment_type_id, appointment_type_guid) FROM stdin;
\.


--
-- TOC entry 8824 (class 0 OID 890452)
-- Dependencies: 866
-- Data for Name: migrated_appointments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_appointments (appointment_id, appointment_guid) FROM stdin;
\.


--
-- TOC entry 8827 (class 0 OID 890461)
-- Dependencies: 869
-- Data for Name: migrated_chairs; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_chairs (chair_id, chair_guid) FROM stdin;
\.


--
-- TOC entry 8828 (class 0 OID 890464)
-- Dependencies: 870
-- Data for Name: migrated_claim; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_claim (claim_id, claim_guid) FROM stdin;
\.


--
-- TOC entry 8829 (class 0 OID 890467)
-- Dependencies: 871
-- Data for Name: migrated_claim_help; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_claim_help (db, pricestring, guid, status, date, type, total, patguid) FROM stdin;
\.


--
-- TOC entry 8830 (class 0 OID 890470)
-- Dependencies: 872
-- Data for Name: migrated_employees; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_employees (employee_id, employee_guid) FROM stdin;
\.


--
-- TOC entry 8831 (class 0 OID 890473)
-- Dependencies: 873
-- Data for Name: migrated_insurance_companies; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_insurance_companies (company_id, company_guid) FROM stdin;
\.


--
-- TOC entry 8832 (class 0 OID 890476)
-- Dependencies: 874
-- Data for Name: migrated_insurance_plans; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_insurance_plans (plan_id, plan_guid) FROM stdin;
\.


--
-- TOC entry 8833 (class 0 OID 890479)
-- Dependencies: 875
-- Data for Name: migrated_locations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_locations (location_id, location_guid) FROM stdin;
\.


--
-- TOC entry 8836 (class 0 OID 890491)
-- Dependencies: 878
-- Data for Name: migrated_patient_answers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_patient_answers (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, patient_answer_id, patient_answer_guid, patient_guid, answer_guid, questionnaire_guid, question_guid) FROM stdin;
\.


--
-- TOC entry 8837 (class 0 OID 890494)
-- Dependencies: 879
-- Data for Name: migrated_patient_documents; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_patient_documents (id, person_guid, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8838 (class 0 OID 890497)
-- Dependencies: 880
-- Data for Name: migrated_patient_image_series; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_patient_image_series (id, image_series_id, image_series_guid, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8834 (class 0 OID 890482)
-- Dependencies: 876
-- Data for Name: migrated_patients; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_patients (patient_id, patient_guid) FROM stdin;
\.


--
-- TOC entry 8835 (class 0 OID 890485)
-- Dependencies: 877
-- Data for Name: migrated_patients_comments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_patients_comments (id, comment_id, comment_guid, note, type, patient_guid, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 8839 (class 0 OID 890500)
-- Dependencies: 881
-- Data for Name: migrated_payment_accounts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_payment_accounts (payment_id, payment_guid) FROM stdin;
\.


--
-- TOC entry 8840 (class 0 OID 890503)
-- Dependencies: 882
-- Data for Name: migrated_persons; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_persons (person_id, person_guid) FROM stdin;
\.


--
-- TOC entry 8841 (class 0 OID 890506)
-- Dependencies: 883
-- Data for Name: migrated_questionnaire; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_questionnaire (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, questionnaire_id, questionnaire_guid, patient_guid, questionnaire_name, date, tx_card_id) FROM stdin;
\.


--
-- TOC entry 8842 (class 0 OID 890509)
-- Dependencies: 884
-- Data for Name: migrated_questions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_questions (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, question_id, question_guid, patient_guid, questionnaire_guid, question, question_type) FROM stdin;
\.


--
-- TOC entry 8843 (class 0 OID 890512)
-- Dependencies: 885
-- Data for Name: migrated_relations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_relations (relation_id, relation_guid) FROM stdin;
\.


--
-- TOC entry 8844 (class 0 OID 890515)
-- Dependencies: 886
-- Data for Name: migrated_schedule_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_schedule_templates (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, template_id, template_guid, location_name, minutes_per_row, location_guid, start_time, end_time, template_order, time_zone, color) FROM stdin;
\.


--
-- TOC entry 8845 (class 0 OID 890521)
-- Dependencies: 887
-- Data for Name: migrated_transactions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_transactions (transaction_id, transaction_guid) FROM stdin;
\.


--
-- TOC entry 8846 (class 0 OID 890524)
-- Dependencies: 888
-- Data for Name: migrated_treatment_notes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_treatment_notes (note_id, note_guid) FROM stdin;
\.


--
-- TOC entry 8847 (class 0 OID 890527)
-- Dependencies: 889
-- Data for Name: migrated_txcards; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_txcards (txcard_id, txcard_guid) FROM stdin;
\.


--
-- TOC entry 8848 (class 0 OID 890530)
-- Dependencies: 890
-- Data for Name: migrated_workschedule; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migrated_workschedule (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, workschedule_id, workschedule_guid, template_guid, location_name, chair_name, minutes_per_row) FROM stdin;
\.


--
-- TOC entry 8849 (class 0 OID 890533)
-- Dependencies: 891
-- Data for Name: migration_appointment_employee_mapping; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migration_appointment_employee_mapping (appointment_source_key, provider_source_key, assistant_source_key, appointment_target_key, provider_target_key, assistant_target_key) FROM stdin;
\.


--
-- TOC entry 8850 (class 0 OID 890536)
-- Dependencies: 892
-- Data for Name: migration_fix_adjustments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migration_fix_adjustments (id) FROM stdin;
\.


--
-- TOC entry 8852 (class 0 OID 890541)
-- Dependencies: 894
-- Data for Name: migration_map; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.migration_map (id, created, source_key, source_type, source_name, target_id, target_table, merged_to_id, deleted) FROM stdin;
\.


--
-- TOC entry 8854 (class 0 OID 890551)
-- Dependencies: 896
-- Data for Name: misc_fee_charge_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.misc_fee_charge_templates (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, name, price, affecting_production_report) FROM stdin;
\.


--
-- TOC entry 8853 (class 0 OID 890546)
-- Dependencies: 895
-- Data for Name: misc_fee_charges; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.misc_fee_charges (id, name, included_in_treatment_fee, affecting_production_report) FROM stdin;
\.


--
-- TOC entry 8855 (class 0 OID 890555)
-- Dependencies: 897
-- Data for Name: my_reports; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.my_reports (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name) FROM stdin;
\.


--
-- TOC entry 8856 (class 0 OID 890558)
-- Dependencies: 898
-- Data for Name: my_reports_columns; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.my_reports_columns (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, column_name, report_id, sort_direction, sort_order, group_order, column_order) FROM stdin;
\.


--
-- TOC entry 8857 (class 0 OID 890561)
-- Dependencies: 899
-- Data for Name: my_reports_filters; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.my_reports_filters (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, type, column_name, report_id) FROM stdin;
\.


--
-- TOC entry 8858 (class 0 OID 890567)
-- Dependencies: 900
-- Data for Name: network_fee_sheets; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.network_fee_sheets (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, valid_from, valid_to, organization_id, employment_contract_id) FROM stdin;
\.


--
-- TOC entry 8859 (class 0 OID 890570)
-- Dependencies: 901
-- Data for Name: network_sheet_fee; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.network_sheet_fee (id, sys_version, insurance_code_id, network_fee_sheet_id, fee) FROM stdin;
\.


--
-- TOC entry 8861 (class 0 OID 890576)
-- Dependencies: 903
-- Data for Name: notebook_notes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.notebook_notes (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, color, height, x, y, content, title, width, sys_zindex, notebook_id) FROM stdin;
\.


--
-- TOC entry 8860 (class 0 OID 890573)
-- Dependencies: 902
-- Data for Name: notebooks; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.notebooks (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, owner, title) FROM stdin;
\.


--
-- TOC entry 8862 (class 0 OID 890582)
-- Dependencies: 904
-- Data for Name: notes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.notes (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, author, org_id, ref_id, target_id, target_type, note, description, alert) FROM stdin;
\.


--
-- TOC entry 8865 (class 0 OID 890596)
-- Dependencies: 907
-- Data for Name: od_chair_break_hours; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.od_chair_break_hours (od_chair_id, end_min, start_min) FROM stdin;
\.


--
-- TOC entry 8864 (class 0 OID 890592)
-- Dependencies: 906
-- Data for Name: odt_chair_allocations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.odt_chair_allocations (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, odt_chair_id, resource_id, primary_resource) FROM stdin;
\.


--
-- TOC entry 8863 (class 0 OID 890588)
-- Dependencies: 905
-- Data for Name: odt_chairs; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.odt_chairs (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, deleted, valid_from, template_id, day_schedule_id, chair_id) FROM stdin;
\.


--
-- TOC entry 8868 (class 0 OID 890614)
-- Dependencies: 910
-- Data for Name: office_day_chairs; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.office_day_chairs (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, odt_chair_id, office_day_id, chair_id) FROM stdin;
\.


--
-- TOC entry 8869 (class 0 OID 890617)
-- Dependencies: 911
-- Data for Name: office_day_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.office_day_templates (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, deleted, location_id, name, color) FROM stdin;
\.


--
-- TOC entry 8867 (class 0 OID 890607)
-- Dependencies: 909
-- Data for Name: office_days; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.office_days (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, deleted, location_id, template_id, o_date, notes) FROM stdin;
\.


--
-- TOC entry 8866 (class 0 OID 890599)
-- Dependencies: 908
-- Data for Name: offices; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.offices (city, state, id) FROM stdin;
\.


--
-- TOC entry 8870 (class 0 OID 890624)
-- Dependencies: 912
-- Data for Name: ortho_coverages; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.ortho_coverages (id, max_lifetime_coverage, deductible_amount, reimbursement_percentage, downpayment_percentage, dependent_age_limit, age_limit_cutoff, waiting_period, cob_type, inprogress) FROM stdin;
\.


--
-- TOC entry 8871 (class 0 OID 890631)
-- Dependencies: 913
-- Data for Name: ortho_insured; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.ortho_insured (id, used_lifetime_coverage, year_deductible_paid_last) FROM stdin;
\.


--
-- TOC entry 8872 (class 0 OID 890634)
-- Dependencies: 914
-- Data for Name: other_professional_relationships; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.other_professional_relationships (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, person_id, external_office_id, employee_type_id) FROM stdin;
\.


--
-- TOC entry 8874 (class 0 OID 890644)
-- Dependencies: 916
-- Data for Name: patient_images; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_images (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, patient_id, imageseries_id, file_key, type_key, slot_key, file_written) FROM stdin;
\.


--
-- TOC entry 8876 (class 0 OID 890654)
-- Dependencies: 918
-- Data for Name: patient_images_layouts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_images_layouts (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_node_id, xml_content) FROM stdin;
\.


--
-- TOC entry 8877 (class 0 OID 890660)
-- Dependencies: 919
-- Data for Name: patient_images_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_images_types (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, xml_content) FROM stdin;
\.


--
-- TOC entry 8875 (class 0 OID 890650)
-- Dependencies: 917
-- Data for Name: patient_imageseries; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_imageseries (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, treatment_id, appointment_id, patient_id, name, series_date, deleted) FROM stdin;
\.


--
-- TOC entry 8878 (class 0 OID 890666)
-- Dependencies: 920
-- Data for Name: patient_insurance_plans; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_insurance_plans (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, master_id, insurance_company_id, insurance_billing_center_id, number, name, description) FROM stdin;
\.


--
-- TOC entry 8879 (class 0 OID 890672)
-- Dependencies: 921
-- Data for Name: patient_ledger_history; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_ledger_history (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, history_date, patient_id, tx_location_id, payment_account_type, medicaid, payer, oldest_due_day, balance, due, future_due, overdue1, overdue30, overdue60, overdue90, overdue120, autopay_source, payer_phones, payer_emails, label_names, label_agencies) FROM stdin;
\.


--
-- TOC entry 8880 (class 0 OID 890678)
-- Dependencies: 922
-- Data for Name: patient_ledger_history_overdue_receivables; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_ledger_history_overdue_receivables (receivable_id, patient_ledger_history_id) FROM stdin;
\.


--
-- TOC entry 8881 (class 0 OID 890681)
-- Dependencies: 923
-- Data for Name: patient_locations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_locations (patient_id, location_id) FROM stdin;
\.


--
-- TOC entry 8882 (class 0 OID 890684)
-- Dependencies: 924
-- Data for Name: patient_person_referrals; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_person_referrals (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, patient_id, person_id, person_type) FROM stdin;
\.


--
-- TOC entry 8883 (class 0 OID 890687)
-- Dependencies: 925
-- Data for Name: patient_recently_opened_items; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_recently_opened_items (id, patient_id) FROM stdin;
\.


--
-- TOC entry 8884 (class 0 OID 890690)
-- Dependencies: 926
-- Data for Name: patient_template_referrals; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patient_template_referrals (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, patient_id, referral_template_id, text_value) FROM stdin;
\.


--
-- TOC entry 8873 (class 0 OID 890637)
-- Dependencies: 915
-- Data for Name: patients; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.patients (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, person_id, notes, legacy_id, heard_from_patient_id, heard_from_professional_id, heard_from_website, heard_from_other, image_file_key, human_id, primary_location_id, not_generate_statement) FROM stdin;
\.


--
-- TOC entry 8886 (class 0 OID 890703)
-- Dependencies: 928
-- Data for Name: payment_accounts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.payment_accounts (id, account_type, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, temp) FROM stdin;
\.


--
-- TOC entry 8887 (class 0 OID 890707)
-- Dependencies: 929
-- Data for Name: payment_correction_details; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.payment_correction_details (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, transaction_id, note, correction_type_id) FROM stdin;
\.


--
-- TOC entry 8888 (class 0 OID 890713)
-- Dependencies: 930
-- Data for Name: payment_options; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.payment_options (id, name) FROM stdin;
\.


--
-- TOC entry 8890 (class 0 OID 890724)
-- Dependencies: 932
-- Data for Name: payment_plan_rollbacks; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.payment_plan_rollbacks (id, payment_plan_id) FROM stdin;
\.


--
-- TOC entry 8889 (class 0 OID 890716)
-- Dependencies: 931
-- Data for Name: payment_plans; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.payment_plans (id, payment_plan_type, payment_plan_rollback_id, installment_interval, first_installment_date, second_installment_date, installment_amount, first_due_date, installment_locked, plan_length_locked, bank_account_id, token, signature, autopay_source, last_four_digits, card_expiration, invoice_contact_method_id, token_location_id, requested_downpayment_amount, rollback_date) FROM stdin;
\.


--
-- TOC entry 8891 (class 0 OID 890727)
-- Dependencies: 933
-- Data for Name: payment_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.payment_types (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, type, deleted, parent_company_id) FROM stdin;
\.


--
-- TOC entry 8892 (class 0 OID 890731)
-- Dependencies: 934
-- Data for Name: payment_types_locations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.payment_types_locations (payment_type_id, organization_id) FROM stdin;
\.


--
-- TOC entry 8885 (class 0 OID 890696)
-- Dependencies: 927
-- Data for Name: payments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.payments (id, payment_type, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, account_id, amount, signature, transaction_id, receivable_id, payer_name, check_number, last_four_digits, expire, type_info, human_id, billing_address_id, credit_card_data, applied_payment_id, organization_id, notes, patient_id, sys_created_at, credit_manual, payment_type_id, insurance_company_payment_id, migrated) FROM stdin;
\.


--
-- TOC entry 8898 (class 0 OID 890749)
-- Dependencies: 940
-- Data for Name: permission_user_role_location; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.permission_user_role_location (permission_user_role_id, user_id, location_id) FROM stdin;
\.


--
-- TOC entry 8893 (class 0 OID 890734)
-- Dependencies: 935
-- Data for Name: permissions_permission; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.permissions_permission (id, name, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, sys_organizations_id, permission_group_id) FROM stdin;
\.


--
-- TOC entry 8894 (class 0 OID 890737)
-- Dependencies: 936
-- Data for Name: permissions_user_role; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.permissions_user_role (id, name, sys_organizations_id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version) FROM stdin;
\.


--
-- TOC entry 8895 (class 0 OID 890740)
-- Dependencies: 937
-- Data for Name: permissions_user_role_departament; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.permissions_user_role_departament (permissions_user_role_id, department_id) FROM stdin;
\.


--
-- TOC entry 8896 (class 0 OID 890743)
-- Dependencies: 938
-- Data for Name: permissions_user_role_job_title; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.permissions_user_role_job_title (permissions_user_role_id, job_title_id) FROM stdin;
\.


--
-- TOC entry 8897 (class 0 OID 890746)
-- Dependencies: 939
-- Data for Name: permissions_user_role_permission; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.permissions_user_role_permission (id, permissions_user_role_id, permissions_permission_id, access_type, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, sys_organizations_id) FROM stdin;
\.


--
-- TOC entry 8900 (class 0 OID 890759)
-- Dependencies: 942
-- Data for Name: person_image_files; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.person_image_files (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, full_size, mime_type, thumbnail, person_id) FROM stdin;
\.


--
-- TOC entry 8901 (class 0 OID 890765)
-- Dependencies: 943
-- Data for Name: person_payment_accounts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.person_payment_accounts (id, organization_id, payer_person_id) FROM stdin;
\.


--
-- TOC entry 8899 (class 0 OID 890752)
-- Dependencies: 941
-- Data for Name: persons; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.persons (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, birth_date, first_name, gender, greeting, last_name, middle_name, search_name, prefix, suffix, preferred_means_of_contact, ssn, no_ssn_reason, id_number, title, core_user_id, portal_user_id, school, marital_status, organization_id, global_person_id, patient_portal_user_id, heard_about_us_from, created_from) FROM stdin;
\.


--
-- TOC entry 8902 (class 0 OID 890768)
-- Dependencies: 944
-- Data for Name: previous_enrollment_dependents; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.previous_enrollment_dependents (previous_enrollment_id, dependent_data) FROM stdin;
\.


--
-- TOC entry 8904 (class 0 OID 890776)
-- Dependencies: 946
-- Data for Name: previous_enrollment_form; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.previous_enrollment_form (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, employee_id) FROM stdin;
\.


--
-- TOC entry 8906 (class 0 OID 890782)
-- Dependencies: 948
-- Data for Name: previous_enrollment_form_benefits; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.previous_enrollment_form_benefits (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, enrollment_form_id, benefit_type, benefit_data) FROM stdin;
\.


--
-- TOC entry 8908 (class 0 OID 890795)
-- Dependencies: 950
-- Data for Name: procedure_additional_resource_requirements; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.procedure_additional_resource_requirements (procedure_id, resource_type_id, qty) FROM stdin;
\.


--
-- TOC entry 8910 (class 0 OID 890802)
-- Dependencies: 952
-- Data for Name: procedure_step_additional_resource_requirements; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.procedure_step_additional_resource_requirements (step_id, resource_type_id, qty) FROM stdin;
\.


--
-- TOC entry 8911 (class 0 OID 890805)
-- Dependencies: 953
-- Data for Name: procedure_step_durations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.procedure_step_durations (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, duration, resource_id, step_id, load_percentage) FROM stdin;
\.


--
-- TOC entry 8909 (class 0 OID 890798)
-- Dependencies: 951
-- Data for Name: procedure_steps; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.procedure_steps (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, duration, name, number, procedure_id, requires_primary_assistant, requires_primary_provider) FROM stdin;
\.


--
-- TOC entry 8907 (class 0 OID 890789)
-- Dependencies: 949
-- Data for Name: procedures; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.procedures (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, description, name, insurance_code_id, organization_id, requires_primary_assistant, requires_primary_provider, requires_diagnosis, requires_images, dental_marking_type) FROM stdin;
\.


--
-- TOC entry 8912 (class 0 OID 890809)
-- Dependencies: 954
-- Data for Name: professional_relationships; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.professional_relationships (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, dtype, patient_id, past) FROM stdin;
\.


--
-- TOC entry 8913 (class 0 OID 890813)
-- Dependencies: 955
-- Data for Name: provider_license; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.provider_license (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, number, state, provider_specialty_id, employment_contracts_id) FROM stdin;
\.


--
-- TOC entry 8914 (class 0 OID 890816)
-- Dependencies: 956
-- Data for Name: provider_network_insurance_companies; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.provider_network_insurance_companies (insurance_company_id, network_fee_sheet_id) FROM stdin;
\.


--
-- TOC entry 8915 (class 0 OID 890819)
-- Dependencies: 957
-- Data for Name: provider_specialty; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.provider_specialty (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, code) FROM stdin;
\.


--
-- TOC entry 8916 (class 0 OID 890822)
-- Dependencies: 958
-- Data for Name: question; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.question (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, questionnaire_id, question) FROM stdin;
\.


--
-- TOC entry 8917 (class 0 OID 890825)
-- Dependencies: 959
-- Data for Name: questionnaire; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.questionnaire (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, questionnaire_name, date, tx_card_id, patient_id) FROM stdin;
\.


--
-- TOC entry 8918 (class 0 OID 890828)
-- Dependencies: 960
-- Data for Name: questionnaire_answers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.questionnaire_answers (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, questionnaire_id, question_id, answer_id, answer_note, quantity) FROM stdin;
\.


--
-- TOC entry 8919 (class 0 OID 890834)
-- Dependencies: 961
-- Data for Name: reachify_users; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.reachify_users (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, patient_id, reachify_id, email, username, password) FROM stdin;
\.


--
-- TOC entry 8920 (class 0 OID 890840)
-- Dependencies: 962
-- Data for Name: receivables; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.receivables (id, rtype, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, due_date, realization_date, autopay_date, treatment_id, patient_id, payment_account_id, status, human_id, insured_id, organization_id, parent_company_id, init_claim_id, primary_prsn_payer_inv_id, sys_created_at, contract, bluefin_organization_id) FROM stdin;
\.


--
-- TOC entry 8921 (class 0 OID 890843)
-- Dependencies: 963
-- Data for Name: recently_opened_items; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.recently_opened_items (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, class_name, owner) FROM stdin;
\.


--
-- TOC entry 8922 (class 0 OID 890846)
-- Dependencies: 964
-- Data for Name: referral_list_values; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.referral_list_values (referral_template_id, pos, value) FROM stdin;
\.


--
-- TOC entry 8923 (class 0 OID 890852)
-- Dependencies: 965
-- Data for Name: referral_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.referral_templates (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, organization_id, location_id, type, free_type, deleted) FROM stdin;
\.


--
-- TOC entry 8925 (class 0 OID 890862)
-- Dependencies: 967
-- Data for Name: refund_details; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.refund_details (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, transaction_id, refund_type, refund_method, money_transaction_id, check_number, credit_card_number, credit_card_expire, note) FROM stdin;
\.


--
-- TOC entry 8924 (class 0 OID 890859)
-- Dependencies: 966
-- Data for Name: refunds; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.refunds (id, refund_type) FROM stdin;
\.


--
-- TOC entry 8926 (class 0 OID 890868)
-- Dependencies: 968
-- Data for Name: relationships; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.relationships (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, from_person, to_person, type, role, permitted_to_see_info) FROM stdin;
\.


--
-- TOC entry 8927 (class 0 OID 890874)
-- Dependencies: 969
-- Data for Name: remote_authentication_tokens; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.remote_authentication_tokens (id, created, expires, checksum) FROM stdin;
\.


--
-- TOC entry 8929 (class 0 OID 890881)
-- Dependencies: 971
-- Data for Name: resource_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.resource_types (dtype, id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, abbreviation, is_provider, is_assistant, is_filterable, filter_name) FROM stdin;
\.


--
-- TOC entry 8928 (class 0 OID 890877)
-- Dependencies: 970
-- Data for Name: resources; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.resources (dtype, id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, resource_type_id, deleted, permissions_user_role_id) FROM stdin;
\.


--
-- TOC entry 8930 (class 0 OID 890890)
-- Dependencies: 972
-- Data for Name: retail_fees; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.retail_fees (id, retail_item_id) FROM stdin;
\.


--
-- TOC entry 8931 (class 0 OID 890893)
-- Dependencies: 973
-- Data for Name: retail_items; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.retail_items (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, name, price, available) FROM stdin;
\.


--
-- TOC entry 8932 (class 0 OID 890897)
-- Dependencies: 974
-- Data for Name: reversed_payments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.reversed_payments (payment_id, note, date) FROM stdin;
\.


--
-- TOC entry 8934 (class 0 OID 890907)
-- Dependencies: 976
-- Data for Name: schedule_conflict_permission_bypass; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.schedule_conflict_permission_bypass (id, location_id, days) FROM stdin;
\.


--
-- TOC entry 8935 (class 0 OID 890910)
-- Dependencies: 977
-- Data for Name: schedule_notes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.schedule_notes (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, chair_id, start_time, duration, content, alert_this_day, is_blocking_time) FROM stdin;
\.


--
-- TOC entry 8933 (class 0 OID 890903)
-- Dependencies: 975
-- Data for Name: scheduled_task; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.scheduled_task (task_type, scheduling_cycle, parent_company_id, enabled) FROM stdin;
\.


--
-- TOC entry 8936 (class 0 OID 890915)
-- Dependencies: 978
-- Data for Name: scheduling_preferences; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.scheduling_preferences (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, end_hour, start_hour, patient_id) FROM stdin;
\.


--
-- TOC entry 8937 (class 0 OID 890918)
-- Dependencies: 979
-- Data for Name: scheduling_preferences_week_days; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.scheduling_preferences_week_days (scheduling_preferences_id, week_days) FROM stdin;
\.


--
-- TOC entry 8938 (class 0 OID 890921)
-- Dependencies: 980
-- Data for Name: scheduling_preferred_employees; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.scheduling_preferred_employees (preference_id, employee_id) FROM stdin;
\.


--
-- TOC entry 8939 (class 0 OID 890924)
-- Dependencies: 981
-- Data for Name: schema_version; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
\.


--
-- TOC entry 8940 (class 0 OID 890931)
-- Dependencies: 982
-- Data for Name: selected_appointment_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.selected_appointment_templates (id, sys_version, user_id, location_id, appointment_template_id) FROM stdin;
\.


--
-- TOC entry 8941 (class 0 OID 890934)
-- Dependencies: 983
-- Data for Name: selected_appointment_templates_office_day_calendar_view; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.selected_appointment_templates_office_day_calendar_view (user_id, location_id, appointment_template_id) FROM stdin;
\.


--
-- TOC entry 8942 (class 0 OID 890937)
-- Dependencies: 984
-- Data for Name: selfcheckin_settings; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.selfcheckin_settings (location_access_key_id, max_minutes_late, max_due_amount, due_period, max_due_amount_insurance, due_period_insurance) FROM stdin;
\.


--
-- TOC entry 8943 (class 0 OID 890940)
-- Dependencies: 985
-- Data for Name: selfcheckin_settings_forbidden_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.selfcheckin_settings_forbidden_types (location_access_key_id, appointment_template_id) FROM stdin;
\.


--
-- TOC entry 8944 (class 0 OID 890943)
-- Dependencies: 986
-- Data for Name: simultaneous_appointment_values; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.simultaneous_appointment_values (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, denominator, numerator, resource_id, type_id) FROM stdin;
\.


--
-- TOC entry 8945 (class 0 OID 890946)
-- Dependencies: 987
-- Data for Name: sms; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sms (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, logical_message_id, processor, status, sender, receiver, payload, status_message, sending_date, time_zone_id, expiry_date) FROM stdin;
\.


--
-- TOC entry 8946 (class 0 OID 890952)
-- Dependencies: 988
-- Data for Name: sms_part; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sms_part (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, sms_id, provider_message_id, status) FROM stdin;
\.


--
-- TOC entry 8947 (class 0 OID 890958)
-- Dependencies: 989
-- Data for Name: statements; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.statements (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, human_id, is_pritented, is_emailed, is_scheduled_to_link, is_linked, is_pdf_requested, is_deleted, organization_id, patient_id, pdf_file_key, filter_start_date, filter_end_date) FROM stdin;
\.


--
-- TOC entry 8948 (class 0 OID 890967)
-- Dependencies: 990
-- Data for Name: sys_i18n; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_i18n (locale, key, value) FROM stdin;
\.


--
-- TOC entry 8949 (class 0 OID 890973)
-- Dependencies: 991
-- Data for Name: sys_identifiers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_identifiers (next_free, space) FROM stdin;
\.


--
-- TOC entry 8951 (class 0 OID 890983)
-- Dependencies: 993
-- Data for Name: sys_organization_address; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_address (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, type, occupancy, contact_postal_addresses_id) FROM stdin;
\.


--
-- TOC entry 8952 (class 0 OID 890986)
-- Dependencies: 994
-- Data for Name: sys_organization_address_has_sys_organizations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_address_has_sys_organizations (id, sys_version, sys_organisation_address_id, sys_organizations_id) FROM stdin;
\.


--
-- TOC entry 8953 (class 0 OID 890989)
-- Dependencies: 995
-- Data for Name: sys_organization_attachment; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_attachment (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, sys_organizations_id, type, file_id, file_mime_type, file_name, file_size, file_content) FROM stdin;
\.


--
-- TOC entry 8954 (class 0 OID 890995)
-- Dependencies: 996
-- Data for Name: sys_organization_contact_method; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_contact_method (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_organizations_id, contact_methods_id, description, is_prefered) FROM stdin;
\.


--
-- TOC entry 8955 (class 0 OID 890999)
-- Dependencies: 997
-- Data for Name: sys_organization_department_data; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_department_data (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_organizations_id, name) FROM stdin;
\.


--
-- TOC entry 8956 (class 0 OID 891002)
-- Dependencies: 998
-- Data for Name: sys_organization_division_data; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_division_data (id, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_organizations_id, medicaid_id, schedule_appointments, accept_payments, legal_entity_npi_id) FROM stdin;
\.


--
-- TOC entry 8957 (class 0 OID 891005)
-- Dependencies: 999
-- Data for Name: sys_organization_division_has_department; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_division_has_department (division_tree_node_id, department_id, id, sys_version) FROM stdin;
\.


--
-- TOC entry 8958 (class 0 OID 891008)
-- Dependencies: 1000
-- Data for Name: sys_organization_location_group; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_location_group (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, sys_organizations_id) FROM stdin;
\.


--
-- TOC entry 8959 (class 0 OID 891011)
-- Dependencies: 1001
-- Data for Name: sys_organization_location_group_has_sys_organizations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_location_group_has_sys_organizations (sys_organization_location_group_id, sys_organizations_id) FROM stdin;
\.


--
-- TOC entry 8960 (class 0 OID 891014)
-- Dependencies: 1002
-- Data for Name: sys_organization_settings; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organization_settings (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, name, value) FROM stdin;
\.


--
-- TOC entry 8950 (class 0 OID 890976)
-- Dependencies: 992
-- Data for Name: sys_organizations; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_organizations (id, level, name, time_zone_id, parent_id, postal_address_id, deleted, epoch_date, lat, lng) FROM stdin;
\.


--
-- TOC entry 8961 (class 0 OID 891020)
-- Dependencies: 1003
-- Data for Name: sys_patient_portal_users; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_patient_portal_users (id, disabled, encoded_password, login_email, name, locked, account_expiration_date, password_expiration_date, token, token_expiration_date, invitation_sent, completed_first_appointment, completed_profile) FROM stdin;
\.


--
-- TOC entry 8962 (class 0 OID 891032)
-- Dependencies: 1004
-- Data for Name: sys_portal_users; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_portal_users (id, disabled, encoded_password, login_email, name, locked, account_expiration_date, password_expiration_date, token, token_expiration_date, invitation_sent) FROM stdin;
\.


--
-- TOC entry 8963 (class 0 OID 891042)
-- Dependencies: 1005
-- Data for Name: sys_users; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.sys_users (id, disabled, encoded_password, login_email, name, locked, account_expiration_date, password_expiration_date, token, token_expiration_date) FROM stdin;
\.


--
-- TOC entry 8964 (class 0 OID 891051)
-- Dependencies: 1006
-- Data for Name: table_statistics; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.table_statistics (created, table_name, table_rows, avg_row_length, data_length, index_length) FROM stdin;
\.


--
-- TOC entry 8967 (class 0 OID 891071)
-- Dependencies: 1009
-- Data for Name: task_basket_subscribers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.task_basket_subscribers (task_basket_id, user_id) FROM stdin;
\.


--
-- TOC entry 8966 (class 0 OID 891065)
-- Dependencies: 1008
-- Data for Name: task_baskets; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.task_baskets (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, title, description, organization_id, type) FROM stdin;
\.


--
-- TOC entry 8968 (class 0 OID 891074)
-- Dependencies: 1010
-- Data for Name: task_contacts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.task_contacts (task_id, contact_method_id) FROM stdin;
\.


--
-- TOC entry 8969 (class 0 OID 891077)
-- Dependencies: 1011
-- Data for Name: task_events; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.task_events (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, task_id, assignee_id, action, comment) FROM stdin;
\.


--
-- TOC entry 8965 (class 0 OID 891058)
-- Dependencies: 1007
-- Data for Name: tasks; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tasks (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, title, due_date, description, patient_id, document_template_id, document_tree_node_id, provider_id, organization_id, assignee_id, task_basket_id, task_status, recipient_id, created_from) FROM stdin;
\.


--
-- TOC entry 8971 (class 0 OID 891089)
-- Dependencies: 1013
-- Data for Name: temp_accounts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.temp_accounts (id, organization_id, person_payer_id, insurance_payer_id, tx_plan_id) FROM stdin;
\.


--
-- TOC entry 8970 (class 0 OID 891083)
-- Dependencies: 1012
-- Data for Name: temporary_images; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.temporary_images (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, file_key, organization_node_id, patient_id, source, mime_type, original_name, patient_image_id) FROM stdin;
\.


--
-- TOC entry 8972 (class 0 OID 891092)
-- Dependencies: 1014
-- Data for Name: third_party_account; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.third_party_account (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, system, name, pem_path, consumer_key, consumer_secret, organization_node_id) FROM stdin;
\.


--
-- TOC entry 8976 (class 0 OID 891110)
-- Dependencies: 1018
-- Data for Name: tooth_marking; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tooth_marking (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, completed, to_tooth_number, tooth_number, type, snapshot_id) FROM stdin;
\.


--
-- TOC entry 8973 (class 0 OID 891098)
-- Dependencies: 1015
-- Data for Name: toothchart_edit_log; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.toothchart_edit_log (id, snapshot_id) FROM stdin;
\.


--
-- TOC entry 8974 (class 0 OID 891101)
-- Dependencies: 1016
-- Data for Name: toothchart_note; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.toothchart_note (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, posx, posy, text, snapshot_id) FROM stdin;
\.


--
-- TOC entry 8975 (class 0 OID 891104)
-- Dependencies: 1017
-- Data for Name: toothchart_snapshot; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.toothchart_snapshot (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, comments, data, mime_type, appointment_id) FROM stdin;
\.


--
-- TOC entry 8977 (class 0 OID 891113)
-- Dependencies: 1019
-- Data for Name: transactions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.transactions (dtype, id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, receivable_id, amount, notes, affects_due_now, meta_amount, added, effective_date, draft) FROM stdin;
\.


--
-- TOC entry 8978 (class 0 OID 891118)
-- Dependencies: 1020
-- Data for Name: transfer_charges; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.transfer_charges (id, charge_transfer_adjustment_id, transfer_charge_type) FROM stdin;
\.


--
-- TOC entry 8982 (class 0 OID 891141)
-- Dependencies: 1024
-- Data for Name: tx_card_field_definitions; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_card_field_definitions (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, alert, hide_for_future, name, multi_value, number, tracked, type_id, organization_id, mandatory) FROM stdin;
\.


--
-- TOC entry 8983 (class 0 OID 891149)
-- Dependencies: 1025
-- Data for Name: tx_card_field_options; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_card_field_options (id, sys_version, field_type_id, value, deleted, pos) FROM stdin;
\.


--
-- TOC entry 8984 (class 0 OID 891154)
-- Dependencies: 1026
-- Data for Name: tx_card_field_types; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_card_field_types (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, value_type, is_broken_brackets, is_hygiene) FROM stdin;
\.


--
-- TOC entry 8985 (class 0 OID 891162)
-- Dependencies: 1027
-- Data for Name: tx_card_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_card_templates (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, tx_category_id, tx_plan_template_id) FROM stdin;
\.


--
-- TOC entry 8981 (class 0 OID 891137)
-- Dependencies: 1023
-- Data for Name: tx_cards; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_cards (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, patient_id, tx_category_id) FROM stdin;
\.


--
-- TOC entry 8986 (class 0 OID 891165)
-- Dependencies: 1028
-- Data for Name: tx_categories; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_categories (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name) FROM stdin;
\.


--
-- TOC entry 8987 (class 0 OID 891168)
-- Dependencies: 1029
-- Data for Name: tx_category_coverages; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_category_coverages (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, tx_category_id, insurance_plan_id) FROM stdin;
\.


--
-- TOC entry 8988 (class 0 OID 891171)
-- Dependencies: 1030
-- Data for Name: tx_category_insured; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_category_insured (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, tx_category_id, insured_id) FROM stdin;
\.


--
-- TOC entry 8989 (class 0 OID 891174)
-- Dependencies: 1031
-- Data for Name: tx_contracts; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_contracts (id, sys_version, generated_date, signed_date, file, template_name, tx_payer_id) FROM stdin;
\.


--
-- TOC entry 8990 (class 0 OID 891180)
-- Dependencies: 1032
-- Data for Name: tx_fee_charges; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_fee_charges (id, tx_plan_id, insurance_code_id) FROM stdin;
\.


--
-- TOC entry 8991 (class 0 OID 891183)
-- Dependencies: 1033
-- Data for Name: tx_payers; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_payers (id, ptype, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, primary_payer, tx_id, person_id, contact_method_id, insured_id, payment_method, account_id) FROM stdin;
\.


--
-- TOC entry 8994 (class 0 OID 891197)
-- Dependencies: 1036
-- Data for Name: tx_plan_appointment_procedures; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plan_appointment_procedures (appointment_id, procedure_id, pos, from_template, added, deleted) FROM stdin;
\.


--
-- TOC entry 8993 (class 0 OID 891192)
-- Dependencies: 1035
-- Data for Name: tx_plan_appointments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plan_appointments (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, duration, has_custom_procedures, interval_to_next, note, type_id, next_appointment_id, tx_plan_id, tx_plan_template_appointment_id, sys_created_at) FROM stdin;
\.


--
-- TOC entry 8995 (class 0 OID 891204)
-- Dependencies: 1037
-- Data for Name: tx_plan_groups; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plan_groups (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, pc_id, deleted) FROM stdin;
\.


--
-- TOC entry 8997 (class 0 OID 891215)
-- Dependencies: 1039
-- Data for Name: tx_plan_note_groups; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plan_note_groups (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, note, group_key, tx_plan_note_id) FROM stdin;
\.


--
-- TOC entry 8996 (class 0 OID 891208)
-- Dependencies: 1038
-- Data for Name: tx_plan_notes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plan_notes (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, note, tx_card_id, diagnosis_id) FROM stdin;
\.


--
-- TOC entry 9000 (class 0 OID 891233)
-- Dependencies: 1042
-- Data for Name: tx_plan_template_appointment_procedures; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plan_template_appointment_procedures (appointment_id, procedure_id, pos, from_template, added, deleted) FROM stdin;
\.


--
-- TOC entry 8999 (class 0 OID 891228)
-- Dependencies: 1041
-- Data for Name: tx_plan_template_appointments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plan_template_appointments (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, duration, has_custom_procedures, interval_to_next, note, type_id, next_appointment_id, tx_plan_template_id, sys_created_at) FROM stdin;
\.


--
-- TOC entry 8998 (class 0 OID 891222)
-- Dependencies: 1040
-- Data for Name: tx_plan_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plan_templates (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, insurance_code_id, organization_id, is_pre_treatment, tx_category_id, fee, length_in_weeks, deleted, tx_plan_group_id) FROM stdin;
\.


--
-- TOC entry 8992 (class 0 OID 891187)
-- Dependencies: 1034
-- Data for Name: tx_plans; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_plans (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, insurance_code_id, tx_id, tx_plan_template_id, is_candidate, fee, projected_start_date, length_in_weeks) FROM stdin;
\.


--
-- TOC entry 9001 (class 0 OID 891240)
-- Dependencies: 1043
-- Data for Name: tx_statuses; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.tx_statuses (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, parent_company_id, type, hidden) FROM stdin;
\.


--
-- TOC entry 8979 (class 0 OID 891121)
-- Dependencies: 1021
-- Data for Name: txs; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.txs (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, organization_id, name, notes, status, did_not_start, estimated_start_date, estimated_end_date, start_date, end_date, tx_plan_id, tx_card_id, migrated, sys_created_at) FROM stdin;
\.


--
-- TOC entry 8980 (class 0 OID 891131)
-- Dependencies: 1022
-- Data for Name: txs_state_changes; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.txs_state_changes (id, sys_version, change_time, new_state, old_state, treatment_id) FROM stdin;
\.


--
-- TOC entry 9002 (class 0 OID 891244)
-- Dependencies: 1044
-- Data for Name: unapplied_payments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.unapplied_payments (id, applied_payment_id) FROM stdin;
\.


--
-- TOC entry 9003 (class 0 OID 891247)
-- Dependencies: 1045
-- Data for Name: update_autopayments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.update_autopayments (created, source_name, patient_id, receivable_id, payment_plan_id, patient_name, treatment, source_token, last_four_digits, exp_card_date, location_id, payconnex_account, payconnex_token, result) FROM stdin;
\.


--
-- TOC entry 9004 (class 0 OID 891253)
-- Dependencies: 1046
-- Data for Name: week_templates; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.week_templates (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, name, fri, mon, organization_id, resource_id, sat, sun, thu, tue, wed) FROM stdin;
\.


--
-- TOC entry 9006 (class 0 OID 891260)
-- Dependencies: 1048
-- Data for Name: work_hour_comments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.work_hour_comments (id, work_hours_id, date, core_user_id, comment, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 9005 (class 0 OID 891256)
-- Dependencies: 1047
-- Data for Name: work_hours; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.work_hours (id, core_user_id, date, status, state, location_id, modified_by_admin, sys_version, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by) FROM stdin;
\.


--
-- TOC entry 9007 (class 0 OID 891266)
-- Dependencies: 1049
-- Data for Name: work_schedule_days; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.work_schedule_days (id, sys_created_by, sys_created, sys_last_modified, sys_last_modified_by, sys_version, ws_date, day_schedule_id, resource_id) FROM stdin;
\.


--
-- TOC entry 9008 (class 0 OID 891269)
-- Dependencies: 1050
-- Data for Name: writeoff_adjustments; Type: TABLE DATA; Schema: vaxiom; Owner: postgres
--

COPY vaxiom.writeoff_adjustments (id) FROM stdin;
\.


--
-- TOC entry 9060 (class 0 OID 0)
-- Dependencies: 636
-- Name: act_evt_log_log_nr__seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.act_evt_log_log_nr__seq', 1, true);


--
-- TOC entry 9061 (class 0 OID 0)
-- Dependencies: 763
-- Name: enrollment_form_beneficiary_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_beneficiary_id_seq', 1, true);


--
-- TOC entry 9062 (class 0 OID 0)
-- Dependencies: 765
-- Name: enrollment_form_benefits_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_benefits_id_seq', 1, true);


--
-- TOC entry 9063 (class 0 OID 0)
-- Dependencies: 769
-- Name: enrollment_form_dependents_benefits_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_dependents_benefits_id_seq', 1, true);


--
-- TOC entry 9064 (class 0 OID 0)
-- Dependencies: 767
-- Name: enrollment_form_dependents_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_dependents_id_seq', 1, true);


--
-- TOC entry 9065 (class 0 OID 0)
-- Dependencies: 761
-- Name: enrollment_form_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_id_seq', 1, true);


--
-- TOC entry 9066 (class 0 OID 0)
-- Dependencies: 771
-- Name: enrollment_form_personal_data_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.enrollment_form_personal_data_id_seq', 1, true);


--
-- TOC entry 9067 (class 0 OID 0)
-- Dependencies: 861
-- Name: medical_history_patient_relatives_crowding_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.medical_history_patient_relatives_crowding_id_seq', 1, true);


--
-- TOC entry 9068 (class 0 OID 0)
-- Dependencies: 893
-- Name: migration_map_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.migration_map_id_seq', 1, true);


--
-- TOC entry 9069 (class 0 OID 0)
-- Dependencies: 947
-- Name: previous_enrollment_form_benefits_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.previous_enrollment_form_benefits_id_seq', 1, true);


--
-- TOC entry 9070 (class 0 OID 0)
-- Dependencies: 945
-- Name: previous_enrollment_form_id_seq; Type: SEQUENCE SET; Schema: vaxiom; Owner: postgres
--

SELECT pg_catalog.setval('vaxiom.previous_enrollment_form_id_seq', 1, true);


--
-- TOC entry 6505 (class 2606 OID 892291)
-- Name: activiti_process_settings idx_889332_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.activiti_process_settings
    ADD CONSTRAINT idx_889332_primary PRIMARY KEY (id);


--
-- TOC entry 6507 (class 2606 OID 892292)
-- Name: activiti_process_settings_property idx_889335_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.activiti_process_settings_property
    ADD CONSTRAINT idx_889335_primary PRIMARY KEY (activiti_process_settings_id, property);


--
-- TOC entry 6509 (class 2606 OID 892293)
-- Name: act_evt_log idx_889343_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_evt_log
    ADD CONSTRAINT idx_889343_primary PRIMARY KEY (log_nr_);


--
-- TOC entry 6512 (class 2606 OID 892294)
-- Name: act_ge_bytearray idx_889351_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ge_bytearray
    ADD CONSTRAINT idx_889351_primary PRIMARY KEY (id_);


--
-- TOC entry 6514 (class 2606 OID 892295)
-- Name: act_ge_property idx_889357_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ge_property
    ADD CONSTRAINT idx_889357_primary PRIMARY KEY (name_);


--
-- TOC entry 6520 (class 2606 OID 892296)
-- Name: act_hi_actinst idx_889360_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_actinst
    ADD CONSTRAINT idx_889360_primary PRIMARY KEY (id_);


--
-- TOC entry 6522 (class 2606 OID 892297)
-- Name: act_hi_attachment idx_889367_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_attachment
    ADD CONSTRAINT idx_889367_primary PRIMARY KEY (id_);


--
-- TOC entry 6524 (class 2606 OID 892298)
-- Name: act_hi_comment idx_889373_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_comment
    ADD CONSTRAINT idx_889373_primary PRIMARY KEY (id_);


--
-- TOC entry 6531 (class 2606 OID 892299)
-- Name: act_hi_detail idx_889379_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_detail
    ADD CONSTRAINT idx_889379_primary PRIMARY KEY (id_);


--
-- TOC entry 6536 (class 2606 OID 892300)
-- Name: act_hi_identitylink idx_889385_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_identitylink
    ADD CONSTRAINT idx_889385_primary PRIMARY KEY (id_);


--
-- TOC entry 6540 (class 2606 OID 892301)
-- Name: act_hi_procinst idx_889391_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_procinst
    ADD CONSTRAINT idx_889391_primary PRIMARY KEY (id_);


--
-- TOC entry 6544 (class 2606 OID 892302)
-- Name: act_hi_taskinst idx_889398_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_taskinst
    ADD CONSTRAINT idx_889398_primary PRIMARY KEY (id_);


--
-- TOC entry 6549 (class 2606 OID 892303)
-- Name: act_hi_varinst idx_889405_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_hi_varinst
    ADD CONSTRAINT idx_889405_primary PRIMARY KEY (id_);


--
-- TOC entry 6551 (class 2606 OID 892304)
-- Name: act_id_group idx_889411_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_group
    ADD CONSTRAINT idx_889411_primary PRIMARY KEY (id_);


--
-- TOC entry 6553 (class 2606 OID 892305)
-- Name: act_id_info idx_889417_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_info
    ADD CONSTRAINT idx_889417_primary PRIMARY KEY (id_);


--
-- TOC entry 6556 (class 2606 OID 892306)
-- Name: act_id_membership idx_889423_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_membership
    ADD CONSTRAINT idx_889423_primary PRIMARY KEY (user_id_, group_id_);


--
-- TOC entry 6558 (class 2606 OID 892307)
-- Name: act_id_user idx_889426_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_user
    ADD CONSTRAINT idx_889426_primary PRIMARY KEY (id_);


--
-- TOC entry 6560 (class 2606 OID 892308)
-- Name: act_re_deployment idx_889432_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_deployment
    ADD CONSTRAINT idx_889432_primary PRIMARY KEY (id_);


--
-- TOC entry 6565 (class 2606 OID 892309)
-- Name: act_re_model idx_889439_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_model
    ADD CONSTRAINT idx_889439_primary PRIMARY KEY (id_);


--
-- TOC entry 6568 (class 2606 OID 892310)
-- Name: act_re_procdef idx_889446_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_procdef
    ADD CONSTRAINT idx_889446_primary PRIMARY KEY (id_);


--
-- TOC entry 6572 (class 2606 OID 892311)
-- Name: act_ru_event_subscr idx_889453_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_event_subscr
    ADD CONSTRAINT idx_889453_primary PRIMARY KEY (id_);


--
-- TOC entry 6579 (class 2606 OID 892312)
-- Name: act_ru_execution idx_889461_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT idx_889461_primary PRIMARY KEY (id_);


--
-- TOC entry 6586 (class 2606 OID 892313)
-- Name: act_ru_identitylink idx_889468_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_identitylink
    ADD CONSTRAINT idx_889468_primary PRIMARY KEY (id_);


--
-- TOC entry 6589 (class 2606 OID 892314)
-- Name: act_ru_job idx_889474_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_job
    ADD CONSTRAINT idx_889474_primary PRIMARY KEY (id_);


--
-- TOC entry 6595 (class 2606 OID 892315)
-- Name: act_ru_task idx_889481_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_task
    ADD CONSTRAINT idx_889481_primary PRIMARY KEY (id_);


--
-- TOC entry 6601 (class 2606 OID 892316)
-- Name: act_ru_variable idx_889488_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_variable
    ADD CONSTRAINT idx_889488_primary PRIMARY KEY (id_);


--
-- TOC entry 6604 (class 2606 OID 892317)
-- Name: added_report_type idx_889494_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.added_report_type
    ADD CONSTRAINT idx_889494_primary PRIMARY KEY (id);


--
-- TOC entry 6606 (class 2606 OID 892318)
-- Name: answers idx_889497_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.answers
    ADD CONSTRAINT idx_889497_primary PRIMARY KEY (id);


--
-- TOC entry 6609 (class 2606 OID 892319)
-- Name: application_properties idx_889503_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.application_properties
    ADD CONSTRAINT idx_889503_primary PRIMARY KEY (organization_id, message_key);


--
-- TOC entry 6612 (class 2606 OID 892320)
-- Name: applied_payments idx_889509_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.applied_payments
    ADD CONSTRAINT idx_889509_primary PRIMARY KEY (id);


--
-- TOC entry 6616 (class 2606 OID 892321)
-- Name: appointments idx_889512_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT idx_889512_primary PRIMARY KEY (id);


--
-- TOC entry 6623 (class 2606 OID 892322)
-- Name: appointment_audit_log idx_889521_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_audit_log
    ADD CONSTRAINT idx_889521_primary PRIMARY KEY (id);


--
-- TOC entry 6632 (class 2606 OID 892323)
-- Name: appointment_bookings idx_889524_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT idx_889524_primary PRIMARY KEY (id);


--
-- TOC entry 6638 (class 2606 OID 892324)
-- Name: appointment_booking_resources idx_889531_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_resources
    ADD CONSTRAINT idx_889531_primary PRIMARY KEY (id);


--
-- TOC entry 6642 (class 2606 OID 892325)
-- Name: appointment_booking_state_changes idx_889538_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_state_changes
    ADD CONSTRAINT idx_889538_primary PRIMARY KEY (id);


--
-- TOC entry 6646 (class 2606 OID 892326)
-- Name: appointment_field_values idx_889544_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_field_values
    ADD CONSTRAINT idx_889544_primary PRIMARY KEY (appointment_id, field_option_id, field_definition_id);


--
-- TOC entry 6648 (class 2606 OID 892327)
-- Name: appointment_procedures idx_889547_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_procedures
    ADD CONSTRAINT idx_889547_primary PRIMARY KEY (appointment_id, procedure_id, pos);


--
-- TOC entry 6653 (class 2606 OID 892328)
-- Name: appointment_templates idx_889554_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT idx_889554_primary PRIMARY KEY (id);


--
-- TOC entry 6657 (class 2606 OID 892329)
-- Name: appointment_template_procedures idx_889561_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_template_procedures
    ADD CONSTRAINT idx_889561_primary PRIMARY KEY (appointment_template_id, pos);


--
-- TOC entry 6664 (class 2606 OID 892330)
-- Name: appointment_types idx_889564_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_types
    ADD CONSTRAINT idx_889564_primary PRIMARY KEY (id);


--
-- TOC entry 6666 (class 2606 OID 892331)
-- Name: appointment_types_property idx_889574_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_types_property
    ADD CONSTRAINT idx_889574_primary PRIMARY KEY (appointment_type_id, property);


--
-- TOC entry 6670 (class 2606 OID 892332)
-- Name: appointment_type_daily_restrictions idx_889580_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_type_daily_restrictions
    ADD CONSTRAINT idx_889580_primary PRIMARY KEY (id);


--
-- TOC entry 6673 (class 2606 OID 892333)
-- Name: appt_type_template_restrictions idx_889584_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appt_type_template_restrictions
    ADD CONSTRAINT idx_889584_primary PRIMARY KEY (id);


--
-- TOC entry 6677 (class 2606 OID 892334)
-- Name: audit_events idx_889588_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.audit_events
    ADD CONSTRAINT idx_889588_primary PRIMARY KEY (id);


--
-- TOC entry 6679 (class 2606 OID 892335)
-- Name: audit_logs idx_889594_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.audit_logs
    ADD CONSTRAINT idx_889594_primary PRIMARY KEY (id);


--
-- TOC entry 6682 (class 2606 OID 892336)
-- Name: autopay_invoice_changes idx_889597_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.autopay_invoice_changes
    ADD CONSTRAINT idx_889597_primary PRIMARY KEY (id);


--
-- TOC entry 6685 (class 2606 OID 892337)
-- Name: bank_accounts idx_889603_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.bank_accounts
    ADD CONSTRAINT idx_889603_primary PRIMARY KEY (id);


--
-- TOC entry 6691 (class 2606 OID 892338)
-- Name: benefit_saving_account_values idx_889609_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.benefit_saving_account_values
    ADD CONSTRAINT idx_889609_primary PRIMARY KEY (id);


--
-- TOC entry 6693 (class 2606 OID 892339)
-- Name: bluefin_credentials idx_889612_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.bluefin_credentials
    ADD CONSTRAINT idx_889612_primary PRIMARY KEY (organization_id);


--
-- TOC entry 6695 (class 2606 OID 892340)
-- Name: cancelled_receivables idx_889621_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.cancelled_receivables
    ADD CONSTRAINT idx_889621_primary PRIMARY KEY (receivable_id);


--
-- TOC entry 6698 (class 2606 OID 892341)
-- Name: cephx_requests idx_889624_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.cephx_requests
    ADD CONSTRAINT idx_889624_primary PRIMARY KEY (id);


--
-- TOC entry 6700 (class 2606 OID 892342)
-- Name: chairs idx_889628_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chairs
    ADD CONSTRAINT idx_889628_primary PRIMARY KEY (id);


--
-- TOC entry 6704 (class 2606 OID 892343)
-- Name: chair_allocations idx_889635_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chair_allocations
    ADD CONSTRAINT idx_889635_primary PRIMARY KEY (id);


--
-- TOC entry 6707 (class 2606 OID 892344)
-- Name: charge_transfer_adjustments idx_889639_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.charge_transfer_adjustments
    ADD CONSTRAINT idx_889639_primary PRIMARY KEY (id);


--
-- TOC entry 6710 (class 2606 OID 892345)
-- Name: checks idx_889642_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.checks
    ADD CONSTRAINT idx_889642_primary PRIMARY KEY (id);


--
-- TOC entry 6713 (class 2606 OID 892346)
-- Name: check_types idx_889645_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.check_types
    ADD CONSTRAINT idx_889645_primary PRIMARY KEY (id);


--
-- TOC entry 6715 (class 2606 OID 892347)
-- Name: claim_events idx_889648_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_events
    ADD CONSTRAINT idx_889648_primary PRIMARY KEY (id);


--
-- TOC entry 6720 (class 2606 OID 892348)
-- Name: claim_requests idx_889654_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT idx_889654_primary PRIMARY KEY (id);


--
-- TOC entry 6728 (class 2606 OID 892349)
-- Name: collection_labels idx_889666_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT idx_889666_primary PRIMARY KEY (patient_id, payment_account_id);


--
-- TOC entry 6731 (class 2606 OID 892350)
-- Name: collection_label_agencies idx_889669_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_agencies
    ADD CONSTRAINT idx_889669_primary PRIMARY KEY (id);


--
-- TOC entry 6737 (class 2606 OID 892351)
-- Name: collection_label_history idx_889672_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT idx_889672_primary PRIMARY KEY (id);


--
-- TOC entry 6741 (class 2606 OID 892352)
-- Name: collection_label_templates idx_889675_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_templates
    ADD CONSTRAINT idx_889675_primary PRIMARY KEY (id);


--
-- TOC entry 6745 (class 2606 OID 892353)
-- Name: communication_preferences idx_889681_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.communication_preferences
    ADD CONSTRAINT idx_889681_primary PRIMARY KEY (id);


--
-- TOC entry 6748 (class 2606 OID 892354)
-- Name: contact_emails idx_889684_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_emails
    ADD CONSTRAINT idx_889684_primary PRIMARY KEY (id);


--
-- TOC entry 6750 (class 2606 OID 892355)
-- Name: contact_methods idx_889687_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_methods
    ADD CONSTRAINT idx_889687_primary PRIMARY KEY (id);


--
-- TOC entry 6755 (class 2606 OID 892356)
-- Name: contact_method_associations idx_889691_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_method_associations
    ADD CONSTRAINT idx_889691_primary PRIMARY KEY (id);


--
-- TOC entry 6758 (class 2606 OID 892357)
-- Name: contact_phones idx_889694_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_phones
    ADD CONSTRAINT idx_889694_primary PRIMARY KEY (id);


--
-- TOC entry 6762 (class 2606 OID 892358)
-- Name: contact_postal_addresses idx_889697_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_postal_addresses
    ADD CONSTRAINT idx_889697_primary PRIMARY KEY (id);


--
-- TOC entry 6764 (class 2606 OID 892359)
-- Name: contact_recently_opened_items idx_889703_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_recently_opened_items
    ADD CONSTRAINT idx_889703_primary PRIMARY KEY (id);


--
-- TOC entry 6766 (class 2606 OID 892360)
-- Name: contact_website idx_889706_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_website
    ADD CONSTRAINT idx_889706_primary PRIMARY KEY (id);


--
-- TOC entry 6770 (class 2606 OID 892361)
-- Name: correction_types idx_889709_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types
    ADD CONSTRAINT idx_889709_primary PRIMARY KEY (id);


--
-- TOC entry 6773 (class 2606 OID 892362)
-- Name: correction_types_locations idx_889715_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_locations
    ADD CONSTRAINT idx_889715_primary PRIMARY KEY (correction_type_id, organization_id);


--
-- TOC entry 6776 (class 2606 OID 892363)
-- Name: correction_types_payment_options idx_889718_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_payment_options
    ADD CONSTRAINT idx_889718_primary PRIMARY KEY (correction_types_id, payment_options_id);


--
-- TOC entry 6779 (class 2606 OID 892364)
-- Name: county idx_889721_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.county
    ADD CONSTRAINT idx_889721_primary PRIMARY KEY (id);


--
-- TOC entry 6782 (class 2606 OID 892365)
-- Name: daily_transactions idx_889724_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.daily_transactions
    ADD CONSTRAINT idx_889724_primary PRIMARY KEY (id);


--
-- TOC entry 6785 (class 2606 OID 892366)
-- Name: data_lock idx_889731_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.data_lock
    ADD CONSTRAINT idx_889731_primary PRIMARY KEY (id);


--
-- TOC entry 6789 (class 2606 OID 892367)
-- Name: day_schedules idx_889734_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedules
    ADD CONSTRAINT idx_889734_primary PRIMARY KEY (id);


--
-- TOC entry 6793 (class 2606 OID 892368)
-- Name: day_schedule_appt_slots idx_889741_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_appt_slots
    ADD CONSTRAINT idx_889741_primary PRIMARY KEY (day_schedule_id, appt_type_id, start_min);


--
-- TOC entry 6795 (class 2606 OID 892369)
-- Name: day_schedule_break_hours idx_889744_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_break_hours
    ADD CONSTRAINT idx_889744_primary PRIMARY KEY (day_schedule_id, end_min, start_min);


--
-- TOC entry 6797 (class 2606 OID 892370)
-- Name: day_schedule_office_hours idx_889747_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_office_hours
    ADD CONSTRAINT idx_889747_primary PRIMARY KEY (day_schedule_id, end_min, start_min);


--
-- TOC entry 6800 (class 2606 OID 892371)
-- Name: default_appointment_templates idx_889750_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.default_appointment_templates
    ADD CONSTRAINT idx_889750_primary PRIMARY KEY (id);


--
-- TOC entry 6804 (class 2606 OID 892372)
-- Name: dentalchart_additional_markings idx_889753_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_additional_markings
    ADD CONSTRAINT idx_889753_primary PRIMARY KEY (id);


--
-- TOC entry 6806 (class 2606 OID 892373)
-- Name: dentalchart_edit_log idx_889756_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_edit_log
    ADD CONSTRAINT idx_889756_primary PRIMARY KEY (id);


--
-- TOC entry 6809 (class 2606 OID 892374)
-- Name: dentalchart_marking idx_889759_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_marking
    ADD CONSTRAINT idx_889759_primary PRIMARY KEY (id);


--
-- TOC entry 6812 (class 2606 OID 892375)
-- Name: dentalchart_materials idx_889762_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_materials
    ADD CONSTRAINT idx_889762_primary PRIMARY KEY (id);


--
-- TOC entry 6815 (class 2606 OID 892376)
-- Name: dentalchart_snapshot idx_889768_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_snapshot
    ADD CONSTRAINT idx_889768_primary PRIMARY KEY (id);


--
-- TOC entry 6819 (class 2606 OID 892377)
-- Name: diagnosis idx_889774_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis
    ADD CONSTRAINT idx_889774_primary PRIMARY KEY (id);


--
-- TOC entry 6821 (class 2606 OID 892378)
-- Name: diagnosis_field_values idx_889780_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_field_values
    ADD CONSTRAINT idx_889780_primary PRIMARY KEY (diagnosis_id, field_key);


--
-- TOC entry 6826 (class 2606 OID 892379)
-- Name: diagnosis_letters idx_889786_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT idx_889786_primary PRIMARY KEY (id);


--
-- TOC entry 6831 (class 2606 OID 892380)
-- Name: diagnosis_templates idx_889789_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_templates
    ADD CONSTRAINT idx_889789_primary PRIMARY KEY (id);


--
-- TOC entry 6835 (class 2606 OID 892381)
-- Name: discounts idx_889795_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discounts
    ADD CONSTRAINT idx_889795_primary PRIMARY KEY (id);


--
-- TOC entry 6838 (class 2606 OID 892382)
-- Name: discount_adjustments idx_889802_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_adjustments
    ADD CONSTRAINT idx_889802_primary PRIMARY KEY (id);


--
-- TOC entry 6841 (class 2606 OID 892383)
-- Name: discount_reverse_adjustments idx_889805_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_reverse_adjustments
    ADD CONSTRAINT idx_889805_primary PRIMARY KEY (id);


--
-- TOC entry 6843 (class 2606 OID 892384)
-- Name: dock_item_descriptors idx_889808_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_descriptors
    ADD CONSTRAINT idx_889808_primary PRIMARY KEY (id);


--
-- TOC entry 6845 (class 2606 OID 892385)
-- Name: dock_item_features_events idx_889811_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_events
    ADD CONSTRAINT idx_889811_primary PRIMARY KEY (id);


--
-- TOC entry 6848 (class 2606 OID 892386)
-- Name: dock_item_features_images idx_889817_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_images
    ADD CONSTRAINT idx_889817_primary PRIMARY KEY (id);


--
-- TOC entry 6850 (class 2606 OID 892387)
-- Name: dock_item_features_messages idx_889820_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_messages
    ADD CONSTRAINT idx_889820_primary PRIMARY KEY (id);


--
-- TOC entry 6853 (class 2606 OID 892388)
-- Name: dock_item_features_note idx_889823_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_note
    ADD CONSTRAINT idx_889823_primary PRIMARY KEY (id);


--
-- TOC entry 6855 (class 2606 OID 892389)
-- Name: dock_item_features_tasks idx_889826_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_tasks
    ADD CONSTRAINT idx_889826_primary PRIMARY KEY (id);


--
-- TOC entry 6857 (class 2606 OID 892390)
-- Name: document_recently_opened_items idx_889832_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_recently_opened_items
    ADD CONSTRAINT idx_889832_primary PRIMARY KEY (id);


--
-- TOC entry 6860 (class 2606 OID 892391)
-- Name: document_templates idx_889835_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_templates
    ADD CONSTRAINT idx_889835_primary PRIMARY KEY (id);


--
-- TOC entry 6865 (class 2606 OID 892392)
-- Name: document_tree_nodes idx_889842_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_tree_nodes
    ADD CONSTRAINT idx_889842_primary PRIMARY KEY (id);


--
-- TOC entry 6867 (class 2606 OID 892393)
-- Name: eeo idx_889850_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.eeo
    ADD CONSTRAINT idx_889850_primary PRIMARY KEY (id);


--
-- TOC entry 6871 (class 2606 OID 892394)
-- Name: employee_beneficiary idx_889853_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary
    ADD CONSTRAINT idx_889853_primary PRIMARY KEY (id);


--
-- TOC entry 6874 (class 2606 OID 892395)
-- Name: employee_beneficiary_group idx_889856_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary_group
    ADD CONSTRAINT idx_889856_primary PRIMARY KEY (id);


--
-- TOC entry 6879 (class 2606 OID 892396)
-- Name: employee_benefit idx_889859_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_benefit
    ADD CONSTRAINT idx_889859_primary PRIMARY KEY (id);


--
-- TOC entry 6883 (class 2606 OID 892397)
-- Name: employee_dependent idx_889867_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_dependent
    ADD CONSTRAINT idx_889867_primary PRIMARY KEY (id);


--
-- TOC entry 6888 (class 2606 OID 892398)
-- Name: employee_enrollment idx_889873_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment
    ADD CONSTRAINT idx_889873_primary PRIMARY KEY (id);


--
-- TOC entry 6891 (class 2606 OID 892399)
-- Name: employee_enrollment_attachment idx_889882_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_attachment
    ADD CONSTRAINT idx_889882_primary PRIMARY KEY (id);


--
-- TOC entry 6894 (class 2606 OID 892400)
-- Name: employee_enrollment_event idx_889888_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_event
    ADD CONSTRAINT idx_889888_primary PRIMARY KEY (id);


--
-- TOC entry 6897 (class 2606 OID 892401)
-- Name: employee_enrollment_person idx_889894_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_person
    ADD CONSTRAINT idx_889894_primary PRIMARY KEY (id);


--
-- TOC entry 6902 (class 2606 OID 892402)
-- Name: employee_insured idx_889900_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_insured
    ADD CONSTRAINT idx_889900_primary PRIMARY KEY (id);


--
-- TOC entry 6904 (class 2606 OID 892403)
-- Name: employee_professional_relationships idx_889904_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_professional_relationships
    ADD CONSTRAINT idx_889904_primary PRIMARY KEY (id);


--
-- TOC entry 6908 (class 2606 OID 892404)
-- Name: employee_resources idx_889907_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_resources
    ADD CONSTRAINT idx_889907_primary PRIMARY KEY (id);


--
-- TOC entry 6915 (class 2606 OID 892405)
-- Name: employers idx_889911_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT idx_889911_primary PRIMARY KEY (id);


--
-- TOC entry 6927 (class 2606 OID 892406)
-- Name: employment_contracts idx_889914_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT idx_889914_primary PRIMARY KEY (id);


--
-- TOC entry 6930 (class 2606 OID 892407)
-- Name: employment_contract_emergency_contact idx_889921_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_emergency_contact
    ADD CONSTRAINT idx_889921_primary PRIMARY KEY (id);


--
-- TOC entry 6933 (class 2606 OID 892408)
-- Name: employment_contract_enrollment_group_log idx_889924_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_enrollment_group_log
    ADD CONSTRAINT idx_889924_primary PRIMARY KEY (id);


--
-- TOC entry 6936 (class 2606 OID 892409)
-- Name: employment_contract_job_status_log idx_889930_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_job_status_log
    ADD CONSTRAINT idx_889930_primary PRIMARY KEY (id);


--
-- TOC entry 6940 (class 2606 OID 892410)
-- Name: employment_contract_location_group idx_889933_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_location_group
    ADD CONSTRAINT idx_889933_primary PRIMARY KEY (id);


--
-- TOC entry 6944 (class 2606 OID 892411)
-- Name: employment_contract_permissions idx_889936_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_permissions
    ADD CONSTRAINT idx_889936_primary PRIMARY KEY (id);


--
-- TOC entry 6948 (class 2606 OID 892412)
-- Name: employment_contract_role idx_889939_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_role
    ADD CONSTRAINT idx_889939_primary PRIMARY KEY (id);


--
-- TOC entry 6951 (class 2606 OID 892413)
-- Name: employment_contract_salary_log idx_889942_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_salary_log
    ADD CONSTRAINT idx_889942_primary PRIMARY KEY (id);


--
-- TOC entry 6954 (class 2606 OID 892414)
-- Name: enrollment idx_889948_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment
    ADD CONSTRAINT idx_889948_primary PRIMARY KEY (id);


--
-- TOC entry 6959 (class 2606 OID 892415)
-- Name: enrollment_form idx_889956_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form
    ADD CONSTRAINT idx_889956_primary PRIMARY KEY (id);


--
-- TOC entry 6962 (class 2606 OID 892416)
-- Name: enrollment_form_beneficiary idx_889962_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_beneficiary
    ADD CONSTRAINT idx_889962_primary PRIMARY KEY (id);


--
-- TOC entry 6965 (class 2606 OID 892417)
-- Name: enrollment_form_benefits idx_889971_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_benefits
    ADD CONSTRAINT idx_889971_primary PRIMARY KEY (id);


--
-- TOC entry 6968 (class 2606 OID 892418)
-- Name: enrollment_form_dependents idx_889980_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_dependents
    ADD CONSTRAINT idx_889980_primary PRIMARY KEY (id);


--
-- TOC entry 6972 (class 2606 OID 892419)
-- Name: enrollment_form_dependents_benefits idx_889989_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_dependents_benefits
    ADD CONSTRAINT idx_889989_primary PRIMARY KEY (id);


--
-- TOC entry 6975 (class 2606 OID 892420)
-- Name: enrollment_form_personal_data idx_889998_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_personal_data
    ADD CONSTRAINT idx_889998_primary PRIMARY KEY (id);


--
-- TOC entry 6977 (class 2606 OID 892421)
-- Name: enrollment_form_source_event idx_890005_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form_source_event
    ADD CONSTRAINT idx_890005_primary PRIMARY KEY (id);


--
-- TOC entry 6979 (class 2606 OID 892422)
-- Name: enrollment_group idx_890011_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_group
    ADD CONSTRAINT idx_890011_primary PRIMARY KEY (id);


--
-- TOC entry 6983 (class 2606 OID 892423)
-- Name: enrollment_groups_enrollment_legal_entity idx_890014_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_groups_enrollment_legal_entity
    ADD CONSTRAINT idx_890014_primary PRIMARY KEY (enrollment_group_id, legal_entity_id, enrollment_id);


--
-- TOC entry 6986 (class 2606 OID 892424)
-- Name: enrollment_group_enrollment idx_890017_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_group_enrollment
    ADD CONSTRAINT idx_890017_primary PRIMARY KEY (enrollment_group_id, enrollment_id);


--
-- TOC entry 6989 (class 2606 OID 892425)
-- Name: enrollment_schedule_billing_groups idx_890020_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_schedule_billing_groups
    ADD CONSTRAINT idx_890020_primary PRIMARY KEY (enrollment_id, legal_entity_id, carrier);


--
-- TOC entry 6991 (class 2606 OID 892426)
-- Name: equipment idx_890023_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.equipment
    ADD CONSTRAINT idx_890023_primary PRIMARY KEY (id);


--
-- TOC entry 6994 (class 2606 OID 892427)
-- Name: external_offices idx_890027_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.external_offices
    ADD CONSTRAINT idx_890027_primary PRIMARY KEY (id);


--
-- TOC entry 6997 (class 2606 OID 892428)
-- Name: financial_settings idx_890030_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.financial_settings
    ADD CONSTRAINT idx_890030_primary PRIMARY KEY (id);


--
-- TOC entry 6999 (class 2606 OID 892429)
-- Name: fix_adjustments idx_890036_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.fix_adjustments
    ADD CONSTRAINT idx_890036_primary PRIMARY KEY (id);


--
-- TOC entry 7002 (class 2606 OID 892430)
-- Name: hradmin_enrollment idx_890062_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.hradmin_enrollment
    ADD CONSTRAINT idx_890062_primary PRIMARY KEY (id);


--
-- TOC entry 7004 (class 2606 OID 892431)
-- Name: hr_admin_config idx_890065_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.hr_admin_config
    ADD CONSTRAINT idx_890065_primary PRIMARY KEY (id);


--
-- TOC entry 7007 (class 2606 OID 892432)
-- Name: image_series idx_890068_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.image_series
    ADD CONSTRAINT idx_890068_primary PRIMARY KEY (id);


--
-- TOC entry 7010 (class 2606 OID 892433)
-- Name: image_series_types idx_890071_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.image_series_types
    ADD CONSTRAINT idx_890071_primary PRIMARY KEY (id);


--
-- TOC entry 7013 (class 2606 OID 892434)
-- Name: installment_charges idx_890074_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.installment_charges
    ADD CONSTRAINT idx_890074_primary PRIMARY KEY (id);


--
-- TOC entry 7020 (class 2606 OID 892435)
-- Name: insurance_billing_centers idx_890077_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT idx_890077_primary PRIMARY KEY (id);


--
-- TOC entry 7028 (class 2606 OID 892436)
-- Name: insurance_claims_setup idx_890083_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT idx_890083_primary PRIMARY KEY (id);


--
-- TOC entry 7030 (class 2606 OID 892437)
-- Name: insurance_codes idx_890087_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_codes
    ADD CONSTRAINT idx_890087_primary PRIMARY KEY (id);


--
-- TOC entry 7035 (class 2606 OID 892438)
-- Name: insurance_code_val idx_890093_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_code_val
    ADD CONSTRAINT idx_890093_primary PRIMARY KEY (id);


--
-- TOC entry 7042 (class 2606 OID 892439)
-- Name: insurance_companies idx_890096_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT idx_890096_primary PRIMARY KEY (id);


--
-- TOC entry 7044 (class 2606 OID 892440)
-- Name: insurance_company_payments idx_890104_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_payments
    ADD CONSTRAINT idx_890104_primary PRIMARY KEY (insurance_company_id);


--
-- TOC entry 7048 (class 2606 OID 892441)
-- Name: insurance_company_phone_associations idx_890107_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_phone_associations
    ADD CONSTRAINT idx_890107_primary PRIMARY KEY (id);


--
-- TOC entry 7052 (class 2606 OID 892442)
-- Name: insurance_payments idx_890111_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payments
    ADD CONSTRAINT idx_890111_primary PRIMARY KEY (id);


--
-- TOC entry 7056 (class 2606 OID 892443)
-- Name: insurance_payment_accounts idx_890117_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_accounts
    ADD CONSTRAINT idx_890117_primary PRIMARY KEY (id);


--
-- TOC entry 7059 (class 2606 OID 892444)
-- Name: insurance_payment_corrections idx_890120_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_corrections
    ADD CONSTRAINT idx_890120_primary PRIMARY KEY (id);


--
-- TOC entry 7062 (class 2606 OID 892445)
-- Name: insurance_payment_movements idx_890123_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_movements
    ADD CONSTRAINT idx_890123_primary PRIMARY KEY (id);


--
-- TOC entry 7064 (class 2606 OID 892446)
-- Name: insurance_payment_refunds idx_890129_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_refunds
    ADD CONSTRAINT idx_890129_primary PRIMARY KEY (id);


--
-- TOC entry 7067 (class 2606 OID 892447)
-- Name: insurance_payment_transfers idx_890132_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_transfers
    ADD CONSTRAINT idx_890132_primary PRIMARY KEY (id);


--
-- TOC entry 7070 (class 2606 OID 892448)
-- Name: insurance_plan idx_890135_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan
    ADD CONSTRAINT idx_890135_primary PRIMARY KEY (id);


--
-- TOC entry 7072 (class 2606 OID 892449)
-- Name: insurance_plan_adandd idx_890142_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_adandd
    ADD CONSTRAINT idx_890142_primary PRIMARY KEY (id);


--
-- TOC entry 7075 (class 2606 OID 892450)
-- Name: insurance_plan_age_and_gender_banded_rate idx_890145_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_and_gender_banded_rate
    ADD CONSTRAINT idx_890145_primary PRIMARY KEY (id);


--
-- TOC entry 7078 (class 2606 OID 892451)
-- Name: insurance_plan_age_banded_rate idx_890148_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_banded_rate
    ADD CONSTRAINT idx_890148_primary PRIMARY KEY (id);


--
-- TOC entry 7080 (class 2606 OID 892452)
-- Name: insurance_plan_age_reduction idx_890151_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_reduction
    ADD CONSTRAINT idx_890151_primary PRIMARY KEY (id);


--
-- TOC entry 7082 (class 2606 OID 892453)
-- Name: insurance_plan_age_reduction_detail idx_890154_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_reduction_detail
    ADD CONSTRAINT idx_890154_primary PRIMARY KEY (id);


--
-- TOC entry 7085 (class 2606 OID 892454)
-- Name: insurance_plan_benefit_amount idx_890157_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_benefit_amount
    ADD CONSTRAINT idx_890157_primary PRIMARY KEY (id);


--
-- TOC entry 7087 (class 2606 OID 892455)
-- Name: insurance_plan_children_rate idx_890160_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_children_rate
    ADD CONSTRAINT idx_890160_primary PRIMARY KEY (id);


--
-- TOC entry 7089 (class 2606 OID 892456)
-- Name: insurance_plan_complex_benefit_amount idx_890163_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_complex_benefit_amount
    ADD CONSTRAINT idx_890163_primary PRIMARY KEY (id);


--
-- TOC entry 7092 (class 2606 OID 892457)
-- Name: insurance_plan_complex_benefit_amount_item idx_890166_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_complex_benefit_amount_item
    ADD CONSTRAINT idx_890166_primary PRIMARY KEY (id);


--
-- TOC entry 7094 (class 2606 OID 892458)
-- Name: insurance_plan_composition_detailed_rate idx_890169_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_composition_detailed_rate
    ADD CONSTRAINT idx_890169_primary PRIMARY KEY (id);


--
-- TOC entry 7097 (class 2606 OID 892459)
-- Name: insurance_plan_composition_rate idx_890172_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_composition_rate
    ADD CONSTRAINT idx_890172_primary PRIMARY KEY (id);


--
-- TOC entry 7100 (class 2606 OID 892460)
-- Name: insurance_plan_employers idx_890175_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_employers
    ADD CONSTRAINT idx_890175_primary PRIMARY KEY (insurance_plan_id, employer_id);


--
-- TOC entry 7103 (class 2606 OID 892461)
-- Name: insurance_plan_fixed_limit idx_890178_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_fixed_limit
    ADD CONSTRAINT idx_890178_primary PRIMARY KEY (id, alert_limit);


--
-- TOC entry 7105 (class 2606 OID 892462)
-- Name: insurance_plan_guaranteed_increase idx_890183_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_guaranteed_increase
    ADD CONSTRAINT idx_890183_primary PRIMARY KEY (id);


--
-- TOC entry 7107 (class 2606 OID 892463)
-- Name: insurance_plan_guaranteed_issue idx_890186_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_guaranteed_issue
    ADD CONSTRAINT idx_890186_primary PRIMARY KEY (id);


--
-- TOC entry 7111 (class 2606 OID 892464)
-- Name: insurance_plan_has_enrollment idx_890189_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment
    ADD CONSTRAINT idx_890189_primary PRIMARY KEY (id);


--
-- TOC entry 7115 (class 2606 OID 892465)
-- Name: insurance_plan_has_enrollment_has_enrollment_group idx_890193_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment_has_enrollment_group
    ADD CONSTRAINT idx_890193_primary PRIMARY KEY (insurance_plan_has_enrollment_id, enrollment_group_id);


--
-- TOC entry 7118 (class 2606 OID 892466)
-- Name: insurance_plan_limits idx_890196_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_limits
    ADD CONSTRAINT idx_890196_primary PRIMARY KEY (id);


--
-- TOC entry 7120 (class 2606 OID 892467)
-- Name: insurance_plan_medical_connection idx_890199_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_medical_connection
    ADD CONSTRAINT idx_890199_primary PRIMARY KEY (id);


--
-- TOC entry 7122 (class 2606 OID 892468)
-- Name: insurance_plan_misc idx_890202_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_misc
    ADD CONSTRAINT idx_890202_primary PRIMARY KEY (id);


--
-- TOC entry 7125 (class 2606 OID 892469)
-- Name: insurance_plan_rates idx_890205_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_rates
    ADD CONSTRAINT idx_890205_primary PRIMARY KEY (id);


--
-- TOC entry 7128 (class 2606 OID 892470)
-- Name: insurance_plan_saving_account idx_890208_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_saving_account
    ADD CONSTRAINT idx_890208_primary PRIMARY KEY (id);


--
-- TOC entry 7132 (class 2606 OID 892471)
-- Name: insurance_plan_separate_age_banded_rate idx_890217_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_separate_age_banded_rate
    ADD CONSTRAINT idx_890217_primary PRIMARY KEY (id);


--
-- TOC entry 7134 (class 2606 OID 892472)
-- Name: insurance_plan_simple_benefit_amount idx_890220_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_simple_benefit_amount
    ADD CONSTRAINT idx_890220_primary PRIMARY KEY (id);


--
-- TOC entry 7137 (class 2606 OID 892473)
-- Name: insurance_plan_slider_limit idx_890223_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_slider_limit
    ADD CONSTRAINT idx_890223_primary PRIMARY KEY (id);


--
-- TOC entry 7140 (class 2606 OID 892474)
-- Name: insurance_plan_summary_fields idx_890226_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_summary_fields
    ADD CONSTRAINT idx_890226_primary PRIMARY KEY (id);


--
-- TOC entry 7142 (class 2606 OID 892475)
-- Name: insurance_plan_summary_fields_default idx_890232_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_summary_fields_default
    ADD CONSTRAINT idx_890232_primary PRIMARY KEY (id);


--
-- TOC entry 7144 (class 2606 OID 892476)
-- Name: insurance_plan_term_disability_benefit_amount idx_890238_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_term_disability_benefit_amount
    ADD CONSTRAINT idx_890238_primary PRIMARY KEY (id);


--
-- TOC entry 7149 (class 2606 OID 892477)
-- Name: insurance_subscriptions idx_890241_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_subscriptions
    ADD CONSTRAINT idx_890241_primary PRIMARY KEY (id);


--
-- TOC entry 7153 (class 2606 OID 892478)
-- Name: insured idx_890249_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured
    ADD CONSTRAINT idx_890249_primary PRIMARY KEY (id);


--
-- TOC entry 7156 (class 2606 OID 892479)
-- Name: insured_benefit_values idx_890253_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured_benefit_values
    ADD CONSTRAINT idx_890253_primary PRIMARY KEY (id);


--
-- TOC entry 7158 (class 2606 OID 892480)
-- Name: invitations idx_890256_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invitations
    ADD CONSTRAINT idx_890256_primary PRIMARY KEY (id);


--
-- TOC entry 7162 (class 2606 OID 892481)
-- Name: invoice_export_queue idx_890259_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_export_queue
    ADD CONSTRAINT idx_890259_primary PRIMARY KEY (id);


--
-- TOC entry 7166 (class 2606 OID 892482)
-- Name: invoice_import_export_log idx_890262_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_import_export_log
    ADD CONSTRAINT idx_890262_primary PRIMARY KEY (id);


--
-- TOC entry 7169 (class 2606 OID 892483)
-- Name: in_network_discounts idx_890265_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.in_network_discounts
    ADD CONSTRAINT idx_890265_primary PRIMARY KEY (id);


--
-- TOC entry 7174 (class 2606 OID 892484)
-- Name: jobtitle idx_890268_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle
    ADD CONSTRAINT idx_890268_primary PRIMARY KEY (id);


--
-- TOC entry 7178 (class 2606 OID 892485)
-- Name: jobtitle_group idx_890271_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle_group
    ADD CONSTRAINT idx_890271_primary PRIMARY KEY (id);


--
-- TOC entry 7180 (class 2606 OID 892486)
-- Name: late_fee_charges idx_890274_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.late_fee_charges
    ADD CONSTRAINT idx_890274_primary PRIMARY KEY (id);


--
-- TOC entry 7184 (class 2606 OID 892487)
-- Name: legal_entities_participating_enrollment idx_890277_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entities_participating_enrollment
    ADD CONSTRAINT idx_890277_primary PRIMARY KEY (legal_entity_id, enrollment_id);


--
-- TOC entry 7189 (class 2606 OID 892488)
-- Name: legal_entity idx_890280_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity
    ADD CONSTRAINT idx_890280_primary PRIMARY KEY (id);


--
-- TOC entry 7192 (class 2606 OID 892489)
-- Name: legal_entity_address idx_890290_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_address
    ADD CONSTRAINT idx_890290_primary PRIMARY KEY (id);


--
-- TOC entry 7195 (class 2606 OID 892490)
-- Name: legal_entity_address_attachment idx_890293_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_address_attachment
    ADD CONSTRAINT idx_890293_primary PRIMARY KEY (id);


--
-- TOC entry 7198 (class 2606 OID 892491)
-- Name: legal_entity_billing_group idx_890299_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group
    ADD CONSTRAINT idx_890299_primary PRIMARY KEY (id);


--
-- TOC entry 7201 (class 2606 OID 892492)
-- Name: legal_entity_billing_group_has_sys_organizations idx_890302_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group_has_sys_organizations
    ADD CONSTRAINT idx_890302_primary PRIMARY KEY (id);


--
-- TOC entry 7204 (class 2606 OID 892493)
-- Name: legal_entity_dba idx_890305_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_dba
    ADD CONSTRAINT idx_890305_primary PRIMARY KEY (id);


--
-- TOC entry 7209 (class 2606 OID 892494)
-- Name: legal_entity_insurance_benefit idx_890308_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit
    ADD CONSTRAINT idx_890308_primary PRIMARY KEY (id);


--
-- TOC entry 7212 (class 2606 OID 892495)
-- Name: legal_entity_insurance_benefit_has_division idx_890311_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit_has_division
    ADD CONSTRAINT idx_890311_primary PRIMARY KEY (legal_entity_insurance_benefit_id, division_id);


--
-- TOC entry 7215 (class 2606 OID 892496)
-- Name: legal_entity_npi idx_890314_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_npi
    ADD CONSTRAINT idx_890314_primary PRIMARY KEY (id);


--
-- TOC entry 7218 (class 2606 OID 892497)
-- Name: legal_entity_operational_structures idx_890317_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_operational_structures
    ADD CONSTRAINT idx_890317_primary PRIMARY KEY (id);


--
-- TOC entry 7221 (class 2606 OID 892498)
-- Name: legal_entity_owner idx_890320_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_owner
    ADD CONSTRAINT idx_890320_primary PRIMARY KEY (id);


--
-- TOC entry 7225 (class 2606 OID 892499)
-- Name: legal_entity_tin idx_890323_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_tin
    ADD CONSTRAINT idx_890323_primary PRIMARY KEY (id);


--
-- TOC entry 7229 (class 2606 OID 892500)
-- Name: lightbarcalls idx_890326_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.lightbarcalls
    ADD CONSTRAINT idx_890326_primary PRIMARY KEY (id);


--
-- TOC entry 7234 (class 2606 OID 892501)
-- Name: location_access_keys idx_890329_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.location_access_keys
    ADD CONSTRAINT idx_890329_primary PRIMARY KEY (id);


--
-- TOC entry 7237 (class 2606 OID 892502)
-- Name: medical_history idx_890335_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history
    ADD CONSTRAINT idx_890335_primary PRIMARY KEY (id);


--
-- TOC entry 7241 (class 2606 OID 892503)
-- Name: medical_history_medications idx_890430_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_medications
    ADD CONSTRAINT idx_890430_primary PRIMARY KEY (id);


--
-- TOC entry 7244 (class 2606 OID 892504)
-- Name: medical_history_patient_relatives_crowding idx_890435_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_patient_relatives_crowding
    ADD CONSTRAINT idx_890435_primary PRIMARY KEY (id);


--
-- TOC entry 7246 (class 2606 OID 892505)
-- Name: medications idx_890439_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medications
    ADD CONSTRAINT idx_890439_primary PRIMARY KEY (id);


--
-- TOC entry 7251 (class 2606 OID 892506)
-- Name: merchant_reports idx_890443_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.merchant_reports
    ADD CONSTRAINT idx_890443_primary PRIMARY KEY (id);


--
-- TOC entry 7255 (class 2606 OID 892507)
-- Name: migrated_answers idx_890449_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_answers
    ADD CONSTRAINT idx_890449_primary PRIMARY KEY (id);


--
-- TOC entry 7278 (class 2606 OID 892508)
-- Name: migrated_patients_comments idx_890485_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_patients_comments
    ADD CONSTRAINT idx_890485_primary PRIMARY KEY (id);


--
-- TOC entry 7282 (class 2606 OID 892509)
-- Name: migrated_patient_answers idx_890491_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_patient_answers
    ADD CONSTRAINT idx_890491_primary PRIMARY KEY (id);


--
-- TOC entry 7284 (class 2606 OID 892510)
-- Name: migrated_patient_documents idx_890494_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_patient_documents
    ADD CONSTRAINT idx_890494_primary PRIMARY KEY (id);


--
-- TOC entry 7288 (class 2606 OID 892511)
-- Name: migrated_patient_image_series idx_890497_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_patient_image_series
    ADD CONSTRAINT idx_890497_primary PRIMARY KEY (id);


--
-- TOC entry 7297 (class 2606 OID 892512)
-- Name: migrated_questionnaire idx_890506_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_questionnaire
    ADD CONSTRAINT idx_890506_primary PRIMARY KEY (id);


--
-- TOC entry 7303 (class 2606 OID 892513)
-- Name: migrated_questions idx_890509_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_questions
    ADD CONSTRAINT idx_890509_primary PRIMARY KEY (id);


--
-- TOC entry 7308 (class 2606 OID 892514)
-- Name: migrated_relations idx_890512_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_relations
    ADD CONSTRAINT idx_890512_primary PRIMARY KEY (relation_id);


--
-- TOC entry 7311 (class 2606 OID 892515)
-- Name: migrated_schedule_templates idx_890515_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_schedule_templates
    ADD CONSTRAINT idx_890515_primary PRIMARY KEY (id);


--
-- TOC entry 7321 (class 2606 OID 892516)
-- Name: migrated_workschedule idx_890530_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migrated_workschedule
    ADD CONSTRAINT idx_890530_primary PRIMARY KEY (id);


--
-- TOC entry 7325 (class 2606 OID 892517)
-- Name: migration_fix_adjustments idx_890536_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migration_fix_adjustments
    ADD CONSTRAINT idx_890536_primary PRIMARY KEY (id);


--
-- TOC entry 7330 (class 2606 OID 892518)
-- Name: migration_map idx_890541_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migration_map
    ADD CONSTRAINT idx_890541_primary PRIMARY KEY (id);


--
-- TOC entry 7334 (class 2606 OID 892519)
-- Name: misc_fee_charges idx_890546_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.misc_fee_charges
    ADD CONSTRAINT idx_890546_primary PRIMARY KEY (id);


--
-- TOC entry 7337 (class 2606 OID 892520)
-- Name: misc_fee_charge_templates idx_890551_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.misc_fee_charge_templates
    ADD CONSTRAINT idx_890551_primary PRIMARY KEY (id);


--
-- TOC entry 7339 (class 2606 OID 892521)
-- Name: my_reports idx_890555_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports
    ADD CONSTRAINT idx_890555_primary PRIMARY KEY (id);


--
-- TOC entry 7341 (class 2606 OID 892522)
-- Name: my_reports_columns idx_890558_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports_columns
    ADD CONSTRAINT idx_890558_primary PRIMARY KEY (id);


--
-- TOC entry 7344 (class 2606 OID 892523)
-- Name: my_reports_filters idx_890561_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports_filters
    ADD CONSTRAINT idx_890561_primary PRIMARY KEY (id);


--
-- TOC entry 7349 (class 2606 OID 892524)
-- Name: network_fee_sheets idx_890567_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_fee_sheets
    ADD CONSTRAINT idx_890567_primary PRIMARY KEY (id);


--
-- TOC entry 7353 (class 2606 OID 892525)
-- Name: network_sheet_fee idx_890570_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_sheet_fee
    ADD CONSTRAINT idx_890570_primary PRIMARY KEY (id);


--
-- TOC entry 7355 (class 2606 OID 892526)
-- Name: notebooks idx_890573_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.notebooks
    ADD CONSTRAINT idx_890573_primary PRIMARY KEY (id);


--
-- TOC entry 7358 (class 2606 OID 892527)
-- Name: notebook_notes idx_890576_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.notebook_notes
    ADD CONSTRAINT idx_890576_primary PRIMARY KEY (id);


--
-- TOC entry 7360 (class 2606 OID 892528)
-- Name: notes idx_890582_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.notes
    ADD CONSTRAINT idx_890582_primary PRIMARY KEY (id);


--
-- TOC entry 7365 (class 2606 OID 892529)
-- Name: odt_chairs idx_890588_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chairs
    ADD CONSTRAINT idx_890588_primary PRIMARY KEY (id);


--
-- TOC entry 7369 (class 2606 OID 892530)
-- Name: odt_chair_allocations idx_890592_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chair_allocations
    ADD CONSTRAINT idx_890592_primary PRIMARY KEY (id);


--
-- TOC entry 7372 (class 2606 OID 892531)
-- Name: od_chair_break_hours idx_890596_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.od_chair_break_hours
    ADD CONSTRAINT idx_890596_primary PRIMARY KEY (od_chair_id, end_min, start_min);


--
-- TOC entry 7374 (class 2606 OID 892532)
-- Name: offices idx_890599_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.offices
    ADD CONSTRAINT idx_890599_primary PRIMARY KEY (id);


--
-- TOC entry 7377 (class 2606 OID 892533)
-- Name: office_days idx_890607_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_days
    ADD CONSTRAINT idx_890607_primary PRIMARY KEY (id);


--
-- TOC entry 7383 (class 2606 OID 892534)
-- Name: office_day_chairs idx_890614_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_chairs
    ADD CONSTRAINT idx_890614_primary PRIMARY KEY (id);


--
-- TOC entry 7386 (class 2606 OID 892535)
-- Name: office_day_templates idx_890617_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_templates
    ADD CONSTRAINT idx_890617_primary PRIMARY KEY (id);


--
-- TOC entry 7388 (class 2606 OID 892536)
-- Name: ortho_coverages idx_890624_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.ortho_coverages
    ADD CONSTRAINT idx_890624_primary PRIMARY KEY (id);


--
-- TOC entry 7390 (class 2606 OID 892537)
-- Name: ortho_insured idx_890631_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.ortho_insured
    ADD CONSTRAINT idx_890631_primary PRIMARY KEY (id);


--
-- TOC entry 7395 (class 2606 OID 892538)
-- Name: other_professional_relationships idx_890634_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.other_professional_relationships
    ADD CONSTRAINT idx_890634_primary PRIMARY KEY (id);


--
-- TOC entry 7401 (class 2606 OID 892539)
-- Name: patients idx_890637_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT idx_890637_primary PRIMARY KEY (id);


--
-- TOC entry 7405 (class 2606 OID 892540)
-- Name: patient_images idx_890644_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images
    ADD CONSTRAINT idx_890644_primary PRIMARY KEY (id);


--
-- TOC entry 7410 (class 2606 OID 892541)
-- Name: patient_imageseries idx_890650_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_imageseries
    ADD CONSTRAINT idx_890650_primary PRIMARY KEY (id);


--
-- TOC entry 7414 (class 2606 OID 892542)
-- Name: patient_images_layouts idx_890654_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images_layouts
    ADD CONSTRAINT idx_890654_primary PRIMARY KEY (id);


--
-- TOC entry 7416 (class 2606 OID 892543)
-- Name: patient_images_types idx_890660_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images_types
    ADD CONSTRAINT idx_890660_primary PRIMARY KEY (id);


--
-- TOC entry 7421 (class 2606 OID 892544)
-- Name: patient_insurance_plans idx_890666_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_insurance_plans
    ADD CONSTRAINT idx_890666_primary PRIMARY KEY (id);


--
-- TOC entry 7424 (class 2606 OID 892545)
-- Name: patient_ledger_history idx_890672_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history
    ADD CONSTRAINT idx_890672_primary PRIMARY KEY (id);


--
-- TOC entry 7428 (class 2606 OID 892546)
-- Name: patient_ledger_history_overdue_receivables idx_890678_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history_overdue_receivables
    ADD CONSTRAINT idx_890678_primary PRIMARY KEY (receivable_id, patient_ledger_history_id);


--
-- TOC entry 7431 (class 2606 OID 892547)
-- Name: patient_locations idx_890681_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_locations
    ADD CONSTRAINT idx_890681_primary PRIMARY KEY (patient_id, location_id);


--
-- TOC entry 7435 (class 2606 OID 892548)
-- Name: patient_person_referrals idx_890684_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_person_referrals
    ADD CONSTRAINT idx_890684_primary PRIMARY KEY (id, patient_id, person_id);


--
-- TOC entry 7438 (class 2606 OID 892549)
-- Name: patient_recently_opened_items idx_890687_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_recently_opened_items
    ADD CONSTRAINT idx_890687_primary PRIMARY KEY (id);


--
-- TOC entry 7442 (class 2606 OID 892550)
-- Name: patient_template_referrals idx_890690_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_template_referrals
    ADD CONSTRAINT idx_890690_primary PRIMARY KEY (id);


--
-- TOC entry 7451 (class 2606 OID 892551)
-- Name: payments idx_890696_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT idx_890696_primary PRIMARY KEY (id);


--
-- TOC entry 7456 (class 2606 OID 892552)
-- Name: payment_accounts idx_890703_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_accounts
    ADD CONSTRAINT idx_890703_primary PRIMARY KEY (id);


--
-- TOC entry 7459 (class 2606 OID 892553)
-- Name: payment_correction_details idx_890707_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_correction_details
    ADD CONSTRAINT idx_890707_primary PRIMARY KEY (id);


--
-- TOC entry 7462 (class 2606 OID 892554)
-- Name: payment_options idx_890713_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_options
    ADD CONSTRAINT idx_890713_primary PRIMARY KEY (id);


--
-- TOC entry 7466 (class 2606 OID 892555)
-- Name: payment_plans idx_890716_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT idx_890716_primary PRIMARY KEY (id);


--
-- TOC entry 7470 (class 2606 OID 892556)
-- Name: payment_plan_rollbacks idx_890724_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plan_rollbacks
    ADD CONSTRAINT idx_890724_primary PRIMARY KEY (id);


--
-- TOC entry 7474 (class 2606 OID 892557)
-- Name: payment_types idx_890727_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types
    ADD CONSTRAINT idx_890727_primary PRIMARY KEY (id);


--
-- TOC entry 7477 (class 2606 OID 892558)
-- Name: payment_types_locations idx_890731_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types_locations
    ADD CONSTRAINT idx_890731_primary PRIMARY KEY (payment_type_id, organization_id);


--
-- TOC entry 7480 (class 2606 OID 892559)
-- Name: permissions_permission idx_890734_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_permission
    ADD CONSTRAINT idx_890734_primary PRIMARY KEY (id);


--
-- TOC entry 7482 (class 2606 OID 892560)
-- Name: permissions_user_role idx_890737_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role
    ADD CONSTRAINT idx_890737_primary PRIMARY KEY (id);


--
-- TOC entry 7485 (class 2606 OID 892561)
-- Name: permissions_user_role_departament idx_890740_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_departament
    ADD CONSTRAINT idx_890740_primary PRIMARY KEY (permissions_user_role_id, department_id);


--
-- TOC entry 7488 (class 2606 OID 892562)
-- Name: permissions_user_role_job_title idx_890743_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_job_title
    ADD CONSTRAINT idx_890743_primary PRIMARY KEY (permissions_user_role_id, job_title_id);


--
-- TOC entry 7491 (class 2606 OID 892563)
-- Name: permissions_user_role_permission idx_890746_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_permission
    ADD CONSTRAINT idx_890746_primary PRIMARY KEY (id, permissions_permission_id);


--
-- TOC entry 7496 (class 2606 OID 892564)
-- Name: permission_user_role_location idx_890749_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permission_user_role_location
    ADD CONSTRAINT idx_890749_primary PRIMARY KEY (user_id, location_id);


--
-- TOC entry 7504 (class 2606 OID 892565)
-- Name: persons idx_890752_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT idx_890752_primary PRIMARY KEY (id);


--
-- TOC entry 7508 (class 2606 OID 892566)
-- Name: person_image_files idx_890759_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_image_files
    ADD CONSTRAINT idx_890759_primary PRIMARY KEY (id);


--
-- TOC entry 7512 (class 2606 OID 892567)
-- Name: person_payment_accounts idx_890765_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_payment_accounts
    ADD CONSTRAINT idx_890765_primary PRIMARY KEY (id);


--
-- TOC entry 7516 (class 2606 OID 892568)
-- Name: previous_enrollment_form idx_890776_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form
    ADD CONSTRAINT idx_890776_primary PRIMARY KEY (id);


--
-- TOC entry 7519 (class 2606 OID 892569)
-- Name: previous_enrollment_form_benefits idx_890782_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form_benefits
    ADD CONSTRAINT idx_890782_primary PRIMARY KEY (id);


--
-- TOC entry 7523 (class 2606 OID 892570)
-- Name: procedures idx_890789_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedures
    ADD CONSTRAINT idx_890789_primary PRIMARY KEY (id);


--
-- TOC entry 7525 (class 2606 OID 892571)
-- Name: procedure_additional_resource_requirements idx_890795_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_additional_resource_requirements
    ADD CONSTRAINT idx_890795_primary PRIMARY KEY (procedure_id, resource_type_id, qty);


--
-- TOC entry 7528 (class 2606 OID 892572)
-- Name: procedure_steps idx_890798_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_steps
    ADD CONSTRAINT idx_890798_primary PRIMARY KEY (id);


--
-- TOC entry 7531 (class 2606 OID 892573)
-- Name: procedure_step_additional_resource_requirements idx_890802_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_additional_resource_requirements
    ADD CONSTRAINT idx_890802_primary PRIMARY KEY (step_id, resource_type_id, qty);


--
-- TOC entry 7534 (class 2606 OID 892574)
-- Name: procedure_step_durations idx_890805_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_durations
    ADD CONSTRAINT idx_890805_primary PRIMARY KEY (id);


--
-- TOC entry 7539 (class 2606 OID 892575)
-- Name: professional_relationships idx_890809_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.professional_relationships
    ADD CONSTRAINT idx_890809_primary PRIMARY KEY (id);


--
-- TOC entry 7543 (class 2606 OID 892576)
-- Name: provider_license idx_890813_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_license
    ADD CONSTRAINT idx_890813_primary PRIMARY KEY (id);


--
-- TOC entry 7546 (class 2606 OID 892577)
-- Name: provider_network_insurance_companies idx_890816_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_network_insurance_companies
    ADD CONSTRAINT idx_890816_primary PRIMARY KEY (insurance_company_id, network_fee_sheet_id);


--
-- TOC entry 7548 (class 2606 OID 892578)
-- Name: provider_specialty idx_890819_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_specialty
    ADD CONSTRAINT idx_890819_primary PRIMARY KEY (id);


--
-- TOC entry 7550 (class 2606 OID 892579)
-- Name: question idx_890822_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.question
    ADD CONSTRAINT idx_890822_primary PRIMARY KEY (id);


--
-- TOC entry 7554 (class 2606 OID 892580)
-- Name: questionnaire idx_890825_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.questionnaire
    ADD CONSTRAINT idx_890825_primary PRIMARY KEY (id);


--
-- TOC entry 7558 (class 2606 OID 892581)
-- Name: questionnaire_answers idx_890828_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.questionnaire_answers
    ADD CONSTRAINT idx_890828_primary PRIMARY KEY (id);


--
-- TOC entry 7562 (class 2606 OID 892582)
-- Name: reachify_users idx_890834_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.reachify_users
    ADD CONSTRAINT idx_890834_primary PRIMARY KEY (id);


--
-- TOC entry 7569 (class 2606 OID 892583)
-- Name: receivables idx_890840_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT idx_890840_primary PRIMARY KEY (id);


--
-- TOC entry 7574 (class 2606 OID 892584)
-- Name: recently_opened_items idx_890843_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.recently_opened_items
    ADD CONSTRAINT idx_890843_primary PRIMARY KEY (id);


--
-- TOC entry 7576 (class 2606 OID 892585)
-- Name: referral_list_values idx_890846_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_list_values
    ADD CONSTRAINT idx_890846_primary PRIMARY KEY (referral_template_id, pos);


--
-- TOC entry 7578 (class 2606 OID 892586)
-- Name: referral_templates idx_890852_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_templates
    ADD CONSTRAINT idx_890852_primary PRIMARY KEY (id);


--
-- TOC entry 7582 (class 2606 OID 892587)
-- Name: refunds idx_890859_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.refunds
    ADD CONSTRAINT idx_890859_primary PRIMARY KEY (id);


--
-- TOC entry 7584 (class 2606 OID 892588)
-- Name: refund_details idx_890862_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.refund_details
    ADD CONSTRAINT idx_890862_primary PRIMARY KEY (id);


--
-- TOC entry 7588 (class 2606 OID 892589)
-- Name: relationships idx_890868_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.relationships
    ADD CONSTRAINT idx_890868_primary PRIMARY KEY (id);


--
-- TOC entry 7594 (class 2606 OID 892590)
-- Name: remote_authentication_tokens idx_890874_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.remote_authentication_tokens
    ADD CONSTRAINT idx_890874_primary PRIMARY KEY (id);


--
-- TOC entry 7597 (class 2606 OID 892591)
-- Name: resources idx_890877_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resources
    ADD CONSTRAINT idx_890877_primary PRIMARY KEY (id);


--
-- TOC entry 7603 (class 2606 OID 892592)
-- Name: resource_types idx_890881_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resource_types
    ADD CONSTRAINT idx_890881_primary PRIMARY KEY (id);


--
-- TOC entry 7605 (class 2606 OID 892593)
-- Name: retail_fees idx_890890_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_fees
    ADD CONSTRAINT idx_890890_primary PRIMARY KEY (id);


--
-- TOC entry 7609 (class 2606 OID 892594)
-- Name: retail_items idx_890893_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_items
    ADD CONSTRAINT idx_890893_primary PRIMARY KEY (id);


--
-- TOC entry 7611 (class 2606 OID 892595)
-- Name: reversed_payments idx_890897_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.reversed_payments
    ADD CONSTRAINT idx_890897_primary PRIMARY KEY (payment_id);


--
-- TOC entry 7613 (class 2606 OID 892596)
-- Name: scheduled_task idx_890903_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduled_task
    ADD CONSTRAINT idx_890903_primary PRIMARY KEY (task_type, parent_company_id);


--
-- TOC entry 7617 (class 2606 OID 892597)
-- Name: schedule_conflict_permission_bypass idx_890907_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_conflict_permission_bypass
    ADD CONSTRAINT idx_890907_primary PRIMARY KEY (id);


--
-- TOC entry 7621 (class 2606 OID 892598)
-- Name: schedule_notes idx_890910_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_notes
    ADD CONSTRAINT idx_890910_primary PRIMARY KEY (id);


--
-- TOC entry 7624 (class 2606 OID 892599)
-- Name: scheduling_preferences idx_890915_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferences
    ADD CONSTRAINT idx_890915_primary PRIMARY KEY (id);


--
-- TOC entry 7628 (class 2606 OID 892600)
-- Name: scheduling_preferred_employees idx_890921_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferred_employees
    ADD CONSTRAINT idx_890921_primary PRIMARY KEY (preference_id, employee_id);


--
-- TOC entry 7630 (class 2606 OID 892601)
-- Name: schema_version idx_890924_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schema_version
    ADD CONSTRAINT idx_890924_primary PRIMARY KEY (version);


--
-- TOC entry 7638 (class 2606 OID 892602)
-- Name: selected_appointment_templates idx_890931_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates
    ADD CONSTRAINT idx_890931_primary PRIMARY KEY (id);


--
-- TOC entry 7643 (class 2606 OID 892603)
-- Name: selected_appointment_templates_office_day_calendar_view idx_890934_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates_office_day_calendar_view
    ADD CONSTRAINT idx_890934_primary PRIMARY KEY (user_id, location_id, appointment_template_id);


--
-- TOC entry 7648 (class 2606 OID 892604)
-- Name: simultaneous_appointment_values idx_890943_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.simultaneous_appointment_values
    ADD CONSTRAINT idx_890943_primary PRIMARY KEY (id);


--
-- TOC entry 7652 (class 2606 OID 892605)
-- Name: sms idx_890946_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sms
    ADD CONSTRAINT idx_890946_primary PRIMARY KEY (id);


--
-- TOC entry 7654 (class 2606 OID 892606)
-- Name: sms_part idx_890952_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sms_part
    ADD CONSTRAINT idx_890952_primary PRIMARY KEY (id);


--
-- TOC entry 7660 (class 2606 OID 892607)
-- Name: statements idx_890958_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.statements
    ADD CONSTRAINT idx_890958_primary PRIMARY KEY (id);


--
-- TOC entry 7662 (class 2606 OID 892608)
-- Name: sys_i18n idx_890967_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_i18n
    ADD CONSTRAINT idx_890967_primary PRIMARY KEY (locale, key);


--
-- TOC entry 7664 (class 2606 OID 892609)
-- Name: sys_identifiers idx_890973_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_identifiers
    ADD CONSTRAINT idx_890973_primary PRIMARY KEY (space);


--
-- TOC entry 7669 (class 2606 OID 892610)
-- Name: sys_organizations idx_890976_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organizations
    ADD CONSTRAINT idx_890976_primary PRIMARY KEY (id);


--
-- TOC entry 7672 (class 2606 OID 892611)
-- Name: sys_organization_address idx_890983_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address
    ADD CONSTRAINT idx_890983_primary PRIMARY KEY (id);


--
-- TOC entry 7675 (class 2606 OID 892612)
-- Name: sys_organization_address_has_sys_organizations idx_890986_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address_has_sys_organizations
    ADD CONSTRAINT idx_890986_primary PRIMARY KEY (id);


--
-- TOC entry 7678 (class 2606 OID 892613)
-- Name: sys_organization_attachment idx_890989_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_attachment
    ADD CONSTRAINT idx_890989_primary PRIMARY KEY (id);


--
-- TOC entry 7682 (class 2606 OID 892614)
-- Name: sys_organization_contact_method idx_890995_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_contact_method
    ADD CONSTRAINT idx_890995_primary PRIMARY KEY (id);


--
-- TOC entry 7685 (class 2606 OID 892615)
-- Name: sys_organization_department_data idx_890999_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_department_data
    ADD CONSTRAINT idx_890999_primary PRIMARY KEY (id);


--
-- TOC entry 7689 (class 2606 OID 892616)
-- Name: sys_organization_division_data idx_891002_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_data
    ADD CONSTRAINT idx_891002_primary PRIMARY KEY (id);


--
-- TOC entry 7692 (class 2606 OID 892617)
-- Name: sys_organization_division_has_department idx_891005_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_has_department
    ADD CONSTRAINT idx_891005_primary PRIMARY KEY (id);


--
-- TOC entry 7696 (class 2606 OID 892618)
-- Name: sys_organization_location_group idx_891008_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group
    ADD CONSTRAINT idx_891008_primary PRIMARY KEY (id);


--
-- TOC entry 7699 (class 2606 OID 892619)
-- Name: sys_organization_location_group_has_sys_organizations idx_891011_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group_has_sys_organizations
    ADD CONSTRAINT idx_891011_primary PRIMARY KEY (sys_organization_location_group_id, sys_organizations_id);


--
-- TOC entry 7702 (class 2606 OID 892620)
-- Name: sys_organization_settings idx_891014_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_settings
    ADD CONSTRAINT idx_891014_primary PRIMARY KEY (id);


--
-- TOC entry 7705 (class 2606 OID 892621)
-- Name: sys_patient_portal_users idx_891020_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_patient_portal_users
    ADD CONSTRAINT idx_891020_primary PRIMARY KEY (id);


--
-- TOC entry 7709 (class 2606 OID 892622)
-- Name: sys_portal_users idx_891032_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_portal_users
    ADD CONSTRAINT idx_891032_primary PRIMARY KEY (id);


--
-- TOC entry 7713 (class 2606 OID 892623)
-- Name: sys_users idx_891042_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_users
    ADD CONSTRAINT idx_891042_primary PRIMARY KEY (id);


--
-- TOC entry 7720 (class 2606 OID 892624)
-- Name: tasks idx_891058_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT idx_891058_primary PRIMARY KEY (id);


--
-- TOC entry 7726 (class 2606 OID 892625)
-- Name: task_baskets idx_891065_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_baskets
    ADD CONSTRAINT idx_891065_primary PRIMARY KEY (id);


--
-- TOC entry 7728 (class 2606 OID 892626)
-- Name: task_basket_subscribers idx_891071_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_basket_subscribers
    ADD CONSTRAINT idx_891071_primary PRIMARY KEY (task_basket_id, user_id);


--
-- TOC entry 7732 (class 2606 OID 892627)
-- Name: task_contacts idx_891074_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_contacts
    ADD CONSTRAINT idx_891074_primary PRIMARY KEY (task_id, contact_method_id);


--
-- TOC entry 7735 (class 2606 OID 892628)
-- Name: task_events idx_891077_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_events
    ADD CONSTRAINT idx_891077_primary PRIMARY KEY (id);


--
-- TOC entry 7741 (class 2606 OID 892629)
-- Name: temporary_images idx_891083_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temporary_images
    ADD CONSTRAINT idx_891083_primary PRIMARY KEY (id);


--
-- TOC entry 7746 (class 2606 OID 892630)
-- Name: temp_accounts idx_891089_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT idx_891089_primary PRIMARY KEY (id);


--
-- TOC entry 7750 (class 2606 OID 892631)
-- Name: third_party_account idx_891092_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.third_party_account
    ADD CONSTRAINT idx_891092_primary PRIMARY KEY (id);


--
-- TOC entry 7752 (class 2606 OID 892632)
-- Name: toothchart_edit_log idx_891098_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_edit_log
    ADD CONSTRAINT idx_891098_primary PRIMARY KEY (id);


--
-- TOC entry 7755 (class 2606 OID 892633)
-- Name: toothchart_note idx_891101_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_note
    ADD CONSTRAINT idx_891101_primary PRIMARY KEY (id);


--
-- TOC entry 7759 (class 2606 OID 892634)
-- Name: toothchart_snapshot idx_891104_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_snapshot
    ADD CONSTRAINT idx_891104_primary PRIMARY KEY (id);


--
-- TOC entry 7761 (class 2606 OID 892635)
-- Name: tooth_marking idx_891110_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tooth_marking
    ADD CONSTRAINT idx_891110_primary PRIMARY KEY (id);


--
-- TOC entry 7765 (class 2606 OID 892636)
-- Name: transactions idx_891113_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transactions
    ADD CONSTRAINT idx_891113_primary PRIMARY KEY (id);


--
-- TOC entry 7770 (class 2606 OID 892637)
-- Name: transfer_charges idx_891118_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transfer_charges
    ADD CONSTRAINT idx_891118_primary PRIMARY KEY (id);


--
-- TOC entry 7774 (class 2606 OID 892638)
-- Name: txs idx_891121_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT idx_891121_primary PRIMARY KEY (id);


--
-- TOC entry 7779 (class 2606 OID 892639)
-- Name: txs_state_changes idx_891131_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs_state_changes
    ADD CONSTRAINT idx_891131_primary PRIMARY KEY (id);


--
-- TOC entry 7783 (class 2606 OID 892640)
-- Name: tx_cards idx_891137_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_cards
    ADD CONSTRAINT idx_891137_primary PRIMARY KEY (id);


--
-- TOC entry 7787 (class 2606 OID 892641)
-- Name: tx_card_field_definitions idx_891141_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_definitions
    ADD CONSTRAINT idx_891141_primary PRIMARY KEY (id);


--
-- TOC entry 7791 (class 2606 OID 892642)
-- Name: tx_card_field_options idx_891149_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_options
    ADD CONSTRAINT idx_891149_primary PRIMARY KEY (id);


--
-- TOC entry 7793 (class 2606 OID 892643)
-- Name: tx_card_field_types idx_891154_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_types
    ADD CONSTRAINT idx_891154_primary PRIMARY KEY (id);


--
-- TOC entry 7795 (class 2606 OID 892644)
-- Name: tx_card_templates idx_891162_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_templates
    ADD CONSTRAINT idx_891162_primary PRIMARY KEY (id);


--
-- TOC entry 7799 (class 2606 OID 892645)
-- Name: tx_categories idx_891165_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_categories
    ADD CONSTRAINT idx_891165_primary PRIMARY KEY (id);


--
-- TOC entry 7802 (class 2606 OID 892646)
-- Name: tx_category_coverages idx_891168_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_coverages
    ADD CONSTRAINT idx_891168_primary PRIMARY KEY (id);


--
-- TOC entry 7806 (class 2606 OID 892647)
-- Name: tx_category_insured idx_891171_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_insured
    ADD CONSTRAINT idx_891171_primary PRIMARY KEY (id);


--
-- TOC entry 7809 (class 2606 OID 892648)
-- Name: tx_contracts idx_891174_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_contracts
    ADD CONSTRAINT idx_891174_primary PRIMARY KEY (id);


--
-- TOC entry 7813 (class 2606 OID 892649)
-- Name: tx_fee_charges idx_891180_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_fee_charges
    ADD CONSTRAINT idx_891180_primary PRIMARY KEY (id);


--
-- TOC entry 7820 (class 2606 OID 892650)
-- Name: tx_payers idx_891183_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT idx_891183_primary PRIMARY KEY (id);


--
-- TOC entry 7824 (class 2606 OID 892651)
-- Name: tx_plans idx_891187_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plans
    ADD CONSTRAINT idx_891187_primary PRIMARY KEY (id);


--
-- TOC entry 7829 (class 2606 OID 892652)
-- Name: tx_plan_appointments idx_891192_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT idx_891192_primary PRIMARY KEY (id);


--
-- TOC entry 7835 (class 2606 OID 892653)
-- Name: tx_plan_appointment_procedures idx_891197_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointment_procedures
    ADD CONSTRAINT idx_891197_primary PRIMARY KEY (appointment_id, procedure_id, pos);


--
-- TOC entry 7839 (class 2606 OID 892654)
-- Name: tx_plan_groups idx_891204_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_groups
    ADD CONSTRAINT idx_891204_primary PRIMARY KEY (id);


--
-- TOC entry 7842 (class 2606 OID 892655)
-- Name: tx_plan_notes idx_891208_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_notes
    ADD CONSTRAINT idx_891208_primary PRIMARY KEY (id);


--
-- TOC entry 7845 (class 2606 OID 892656)
-- Name: tx_plan_note_groups idx_891215_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_note_groups
    ADD CONSTRAINT idx_891215_primary PRIMARY KEY (id);


--
-- TOC entry 7850 (class 2606 OID 892657)
-- Name: tx_plan_templates idx_891222_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT idx_891222_primary PRIMARY KEY (id);


--
-- TOC entry 7855 (class 2606 OID 892658)
-- Name: tx_plan_template_appointments idx_891228_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT idx_891228_primary PRIMARY KEY (id);


--
-- TOC entry 7860 (class 2606 OID 892659)
-- Name: tx_plan_template_appointment_procedures idx_891233_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointment_procedures
    ADD CONSTRAINT idx_891233_primary PRIMARY KEY (appointment_id, procedure_id, pos);


--
-- TOC entry 7863 (class 2606 OID 892660)
-- Name: tx_statuses idx_891240_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_statuses
    ADD CONSTRAINT idx_891240_primary PRIMARY KEY (id);


--
-- TOC entry 7867 (class 2606 OID 892661)
-- Name: unapplied_payments idx_891244_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.unapplied_payments
    ADD CONSTRAINT idx_891244_primary PRIMARY KEY (id);


--
-- TOC entry 7872 (class 2606 OID 892662)
-- Name: week_templates idx_891253_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT idx_891253_primary PRIMARY KEY (id);


--
-- TOC entry 7883 (class 2606 OID 892663)
-- Name: work_hours idx_891256_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hours
    ADD CONSTRAINT idx_891256_primary PRIMARY KEY (id);


--
-- TOC entry 7886 (class 2606 OID 892664)
-- Name: work_hour_comments idx_891260_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hour_comments
    ADD CONSTRAINT idx_891260_primary PRIMARY KEY (id);


--
-- TOC entry 7890 (class 2606 OID 892665)
-- Name: work_schedule_days idx_891266_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_schedule_days
    ADD CONSTRAINT idx_891266_primary PRIMARY KEY (id);


--
-- TOC entry 7894 (class 2606 OID 892666)
-- Name: writeoff_adjustments idx_891269_primary; Type: CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.writeoff_adjustments
    ADD CONSTRAINT idx_891269_primary PRIMARY KEY (id);


--
-- TOC entry 6503 (class 1259 OID 891279)
-- Name: idx_889332_organization_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889332_organization_node_id ON vaxiom.activiti_process_settings USING btree (organization_node_id);


--
-- TOC entry 6510 (class 1259 OID 891304)
-- Name: idx_889351_act_fk_bytearr_depl; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889351_act_fk_bytearr_depl ON vaxiom.act_ge_bytearray USING btree (deployment_id_);


--
-- TOC entry 6515 (class 1259 OID 891276)
-- Name: idx_889360_act_idx_hi_act_inst_end; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889360_act_idx_hi_act_inst_end ON vaxiom.act_hi_actinst USING btree (end_time_);


--
-- TOC entry 6516 (class 1259 OID 891272)
-- Name: idx_889360_act_idx_hi_act_inst_exec; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889360_act_idx_hi_act_inst_exec ON vaxiom.act_hi_actinst USING btree (execution_id_, act_id_);


--
-- TOC entry 6517 (class 1259 OID 891277)
-- Name: idx_889360_act_idx_hi_act_inst_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889360_act_idx_hi_act_inst_procinst ON vaxiom.act_hi_actinst USING btree (proc_inst_id_, act_id_);


--
-- TOC entry 6518 (class 1259 OID 891273)
-- Name: idx_889360_act_idx_hi_act_inst_start; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889360_act_idx_hi_act_inst_start ON vaxiom.act_hi_actinst USING btree (start_time_);


--
-- TOC entry 6525 (class 1259 OID 891290)
-- Name: idx_889379_act_idx_hi_detail_act_inst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_act_inst ON vaxiom.act_hi_detail USING btree (act_inst_id_);


--
-- TOC entry 6526 (class 1259 OID 891313)
-- Name: idx_889379_act_idx_hi_detail_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_name ON vaxiom.act_hi_detail USING btree (name_);


--
-- TOC entry 6527 (class 1259 OID 891291)
-- Name: idx_889379_act_idx_hi_detail_proc_inst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_proc_inst ON vaxiom.act_hi_detail USING btree (proc_inst_id_);


--
-- TOC entry 6528 (class 1259 OID 891288)
-- Name: idx_889379_act_idx_hi_detail_task_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_task_id ON vaxiom.act_hi_detail USING btree (task_id_);


--
-- TOC entry 6529 (class 1259 OID 891285)
-- Name: idx_889379_act_idx_hi_detail_time; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889379_act_idx_hi_detail_time ON vaxiom.act_hi_detail USING btree (time_);


--
-- TOC entry 6532 (class 1259 OID 891287)
-- Name: idx_889385_act_idx_hi_ident_lnk_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889385_act_idx_hi_ident_lnk_procinst ON vaxiom.act_hi_identitylink USING btree (proc_inst_id_);


--
-- TOC entry 6533 (class 1259 OID 891283)
-- Name: idx_889385_act_idx_hi_ident_lnk_task; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889385_act_idx_hi_ident_lnk_task ON vaxiom.act_hi_identitylink USING btree (task_id_);


--
-- TOC entry 6534 (class 1259 OID 891299)
-- Name: idx_889385_act_idx_hi_ident_lnk_user; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889385_act_idx_hi_ident_lnk_user ON vaxiom.act_hi_identitylink USING btree (user_id_);


--
-- TOC entry 6537 (class 1259 OID 891300)
-- Name: idx_889391_act_idx_hi_pro_i_buskey; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889391_act_idx_hi_pro_i_buskey ON vaxiom.act_hi_procinst USING btree (business_key_);


--
-- TOC entry 6538 (class 1259 OID 891294)
-- Name: idx_889391_act_idx_hi_pro_inst_end; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889391_act_idx_hi_pro_inst_end ON vaxiom.act_hi_procinst USING btree (end_time_);


--
-- TOC entry 6541 (class 1259 OID 891301)
-- Name: idx_889391_proc_inst_id_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889391_proc_inst_id_ ON vaxiom.act_hi_procinst USING btree (proc_inst_id_);


--
-- TOC entry 6542 (class 1259 OID 891297)
-- Name: idx_889398_act_idx_hi_task_inst_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889398_act_idx_hi_task_inst_procinst ON vaxiom.act_hi_taskinst USING btree (proc_inst_id_);


--
-- TOC entry 6545 (class 1259 OID 891296)
-- Name: idx_889405_act_idx_hi_procvar_name_type; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889405_act_idx_hi_procvar_name_type ON vaxiom.act_hi_varinst USING btree (name_, var_type_);


--
-- TOC entry 6546 (class 1259 OID 891298)
-- Name: idx_889405_act_idx_hi_procvar_proc_inst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889405_act_idx_hi_procvar_proc_inst ON vaxiom.act_hi_varinst USING btree (proc_inst_id_);


--
-- TOC entry 6547 (class 1259 OID 891292)
-- Name: idx_889405_act_idx_hi_procvar_task_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889405_act_idx_hi_procvar_task_id ON vaxiom.act_hi_varinst USING btree (task_id_);


--
-- TOC entry 6554 (class 1259 OID 891310)
-- Name: idx_889423_act_fk_memb_group; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889423_act_fk_memb_group ON vaxiom.act_id_membership USING btree (group_id_);


--
-- TOC entry 6561 (class 1259 OID 891306)
-- Name: idx_889439_act_fk_model_deployment; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889439_act_fk_model_deployment ON vaxiom.act_re_model USING btree (deployment_id_);


--
-- TOC entry 6562 (class 1259 OID 891307)
-- Name: idx_889439_act_fk_model_source; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889439_act_fk_model_source ON vaxiom.act_re_model USING btree (editor_source_value_id_);


--
-- TOC entry 6563 (class 1259 OID 891309)
-- Name: idx_889439_act_fk_model_source_extra; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889439_act_fk_model_source_extra ON vaxiom.act_re_model USING btree (editor_source_extra_value_id_);


--
-- TOC entry 6566 (class 1259 OID 891322)
-- Name: idx_889446_act_uniq_procdef; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889446_act_uniq_procdef ON vaxiom.act_re_procdef USING btree (key_, version_, tenant_id_);


--
-- TOC entry 6569 (class 1259 OID 891316)
-- Name: idx_889453_act_fk_event_exec; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889453_act_fk_event_exec ON vaxiom.act_ru_event_subscr USING btree (execution_id_);


--
-- TOC entry 6570 (class 1259 OID 891321)
-- Name: idx_889453_act_idx_event_subscr_config_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889453_act_idx_event_subscr_config_ ON vaxiom.act_ru_event_subscr USING btree (configuration_);


--
-- TOC entry 6573 (class 1259 OID 891323)
-- Name: idx_889461_act_fk_exe_parent; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_fk_exe_parent ON vaxiom.act_ru_execution USING btree (parent_id_);


--
-- TOC entry 6574 (class 1259 OID 891319)
-- Name: idx_889461_act_fk_exe_procdef; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_fk_exe_procdef ON vaxiom.act_ru_execution USING btree (proc_def_id_);


--
-- TOC entry 6575 (class 1259 OID 891317)
-- Name: idx_889461_act_fk_exe_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_fk_exe_procinst ON vaxiom.act_ru_execution USING btree (proc_inst_id_);


--
-- TOC entry 6576 (class 1259 OID 891318)
-- Name: idx_889461_act_fk_exe_super; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_fk_exe_super ON vaxiom.act_ru_execution USING btree (super_exec_);


--
-- TOC entry 6577 (class 1259 OID 891320)
-- Name: idx_889461_act_idx_exec_buskey; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889461_act_idx_exec_buskey ON vaxiom.act_ru_execution USING btree (business_key_);


--
-- TOC entry 6580 (class 1259 OID 891332)
-- Name: idx_889468_act_fk_idl_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_fk_idl_procinst ON vaxiom.act_ru_identitylink USING btree (proc_inst_id_);


--
-- TOC entry 6581 (class 1259 OID 891326)
-- Name: idx_889468_act_fk_tskass_task; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_fk_tskass_task ON vaxiom.act_ru_identitylink USING btree (task_id_);


--
-- TOC entry 6582 (class 1259 OID 891327)
-- Name: idx_889468_act_idx_athrz_procedef; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_idx_athrz_procedef ON vaxiom.act_ru_identitylink USING btree (proc_def_id_);


--
-- TOC entry 6583 (class 1259 OID 891331)
-- Name: idx_889468_act_idx_ident_lnk_group; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_idx_ident_lnk_group ON vaxiom.act_ru_identitylink USING btree (group_id_);


--
-- TOC entry 6584 (class 1259 OID 891355)
-- Name: idx_889468_act_idx_ident_lnk_user; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889468_act_idx_ident_lnk_user ON vaxiom.act_ru_identitylink USING btree (user_id_);


--
-- TOC entry 6587 (class 1259 OID 891333)
-- Name: idx_889474_act_fk_job_exception; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889474_act_fk_job_exception ON vaxiom.act_ru_job USING btree (exception_stack_id_);


--
-- TOC entry 6590 (class 1259 OID 891329)
-- Name: idx_889481_act_fk_task_exe; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889481_act_fk_task_exe ON vaxiom.act_ru_task USING btree (execution_id_);


--
-- TOC entry 6591 (class 1259 OID 891330)
-- Name: idx_889481_act_fk_task_procdef; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889481_act_fk_task_procdef ON vaxiom.act_ru_task USING btree (proc_def_id_);


--
-- TOC entry 6592 (class 1259 OID 891325)
-- Name: idx_889481_act_fk_task_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889481_act_fk_task_procinst ON vaxiom.act_ru_task USING btree (proc_inst_id_);


--
-- TOC entry 6593 (class 1259 OID 891344)
-- Name: idx_889481_act_idx_task_create; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889481_act_idx_task_create ON vaxiom.act_ru_task USING btree (create_time_);


--
-- TOC entry 6596 (class 1259 OID 891338)
-- Name: idx_889488_act_fk_var_bytearray; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889488_act_fk_var_bytearray ON vaxiom.act_ru_variable USING btree (bytearray_id_);


--
-- TOC entry 6597 (class 1259 OID 891342)
-- Name: idx_889488_act_fk_var_exe; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889488_act_fk_var_exe ON vaxiom.act_ru_variable USING btree (execution_id_);


--
-- TOC entry 6598 (class 1259 OID 891366)
-- Name: idx_889488_act_fk_var_procinst; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889488_act_fk_var_procinst ON vaxiom.act_ru_variable USING btree (proc_inst_id_);


--
-- TOC entry 6599 (class 1259 OID 891348)
-- Name: idx_889488_act_idx_variable_task_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889488_act_idx_variable_task_id ON vaxiom.act_ru_variable USING btree (task_id_);


--
-- TOC entry 6602 (class 1259 OID 891339)
-- Name: idx_889494_owner; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889494_owner ON vaxiom.added_report_type USING btree (owner, report_type);


--
-- TOC entry 6607 (class 1259 OID 891335)
-- Name: idx_889497_questionid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889497_questionid ON vaxiom.answers USING btree (question_id);


--
-- TOC entry 6610 (class 1259 OID 891347)
-- Name: idx_889509_payment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889509_payment_id ON vaxiom.applied_payments USING btree (payment_id);


--
-- TOC entry 6613 (class 1259 OID 891353)
-- Name: idx_889512_next_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889512_next_appointment_id ON vaxiom.appointments USING btree (next_appointment_id);


--
-- TOC entry 6614 (class 1259 OID 891375)
-- Name: idx_889512_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_patient_id ON vaxiom.appointments USING btree (patient_id);


--
-- TOC entry 6617 (class 1259 OID 891354)
-- Name: idx_889512_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_sys_created_at ON vaxiom.appointments USING btree (sys_created_at);


--
-- TOC entry 6618 (class 1259 OID 891350)
-- Name: idx_889512_tx_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_tx_id ON vaxiom.appointments USING btree (tx_id);


--
-- TOC entry 6619 (class 1259 OID 891351)
-- Name: idx_889512_tx_plan_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_tx_plan_appointment_id ON vaxiom.appointments USING btree (tx_plan_appointment_id);


--
-- TOC entry 6620 (class 1259 OID 891357)
-- Name: idx_889512_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889512_type_id ON vaxiom.appointments USING btree (type_id);


--
-- TOC entry 6621 (class 1259 OID 891346)
-- Name: idx_889521_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889521_appointment_id ON vaxiom.appointment_audit_log USING btree (appointment_id);


--
-- TOC entry 6624 (class 1259 OID 891358)
-- Name: idx_889524_appointment_bookings_local_start_date; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_appointment_bookings_local_start_date ON vaxiom.appointment_bookings USING btree (local_start_date);


--
-- TOC entry 6625 (class 1259 OID 891359)
-- Name: idx_889524_appointment_bookings_state; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_appointment_bookings_state ON vaxiom.appointment_bookings USING btree (state);


--
-- TOC entry 6626 (class 1259 OID 891365)
-- Name: idx_889524_appointment_bookings_state_provider; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_appointment_bookings_state_provider ON vaxiom.appointment_bookings USING btree (state, provider_id);


--
-- TOC entry 6627 (class 1259 OID 891383)
-- Name: idx_889524_appointment_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_appointment_index ON vaxiom.appointment_bookings USING btree (appointment_id);


--
-- TOC entry 6628 (class 1259 OID 891373)
-- Name: idx_889524_assistant_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_assistant_id ON vaxiom.appointment_bookings USING btree (assistant_id);


--
-- TOC entry 6629 (class 1259 OID 891364)
-- Name: idx_889524_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_chair_id ON vaxiom.appointment_bookings USING btree (chair_id);


--
-- TOC entry 6630 (class 1259 OID 891361)
-- Name: idx_889524_fk_appointment_bookings_chairs; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_fk_appointment_bookings_chairs ON vaxiom.appointment_bookings USING btree (seated_chair_id);


--
-- TOC entry 6633 (class 1259 OID 891367)
-- Name: idx_889524_provider_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_provider_id ON vaxiom.appointment_bookings USING btree (provider_id);


--
-- TOC entry 6634 (class 1259 OID 891356)
-- Name: idx_889524_start_time_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_start_time_index ON vaxiom.appointment_bookings USING btree (start_time);


--
-- TOC entry 6635 (class 1259 OID 891371)
-- Name: idx_889524_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889524_sys_created_at ON vaxiom.appointment_bookings USING btree (sys_created_at);


--
-- TOC entry 6636 (class 1259 OID 891370)
-- Name: idx_889531_appointment_booking_resources_ibfk_2; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889531_appointment_booking_resources_ibfk_2 ON vaxiom.appointment_booking_resources USING btree (booking_id);


--
-- TOC entry 6639 (class 1259 OID 891374)
-- Name: idx_889531_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889531_resource_id ON vaxiom.appointment_booking_resources USING btree (resource_id);


--
-- TOC entry 6640 (class 1259 OID 891392)
-- Name: idx_889538_booking_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889538_booking_id ON vaxiom.appointment_booking_state_changes USING btree (booking_id);


--
-- TOC entry 6643 (class 1259 OID 891376)
-- Name: idx_889544_field_definition_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889544_field_definition_id ON vaxiom.appointment_field_values USING btree (field_definition_id);


--
-- TOC entry 6644 (class 1259 OID 891372)
-- Name: idx_889544_field_option_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889544_field_option_id ON vaxiom.appointment_field_values USING btree (field_option_id);


--
-- TOC entry 6649 (class 1259 OID 891368)
-- Name: idx_889547_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889547_procedure_id ON vaxiom.appointment_procedures USING btree (procedure_id);


--
-- TOC entry 6650 (class 1259 OID 891382)
-- Name: idx_889554_appointment_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889554_appointment_type_id ON vaxiom.appointment_templates USING btree (appointment_type_id);


--
-- TOC entry 6651 (class 1259 OID 891380)
-- Name: idx_889554_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889554_organization_id ON vaxiom.appointment_templates USING btree (organization_id, appointment_type_id);


--
-- TOC entry 6654 (class 1259 OID 891384)
-- Name: idx_889554_primary_assistant_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889554_primary_assistant_type_id ON vaxiom.appointment_templates USING btree (primary_assistant_type_id);


--
-- TOC entry 6655 (class 1259 OID 891401)
-- Name: idx_889554_primary_provider_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889554_primary_provider_type_id ON vaxiom.appointment_templates USING btree (primary_provider_type_id);


--
-- TOC entry 6658 (class 1259 OID 891388)
-- Name: idx_889561_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889561_procedure_id ON vaxiom.appointment_template_procedures USING btree (procedure_id);


--
-- TOC entry 6659 (class 1259 OID 891386)
-- Name: idx_889564_appointment_types_full_name_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889564_appointment_types_full_name_idx ON vaxiom.appointment_types USING btree (full_name);


--
-- TOC entry 6660 (class 1259 OID 891385)
-- Name: idx_889564_appointment_types_migrated; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889564_appointment_types_migrated ON vaxiom.appointment_types USING btree (migrated);


--
-- TOC entry 6661 (class 1259 OID 891387)
-- Name: idx_889564_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889564_name ON vaxiom.appointment_types USING btree (name);


--
-- TOC entry 6662 (class 1259 OID 891379)
-- Name: idx_889564_parent_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889564_parent_id ON vaxiom.appointment_types USING btree (parent_id);


--
-- TOC entry 6667 (class 1259 OID 891391)
-- Name: idx_889580_appointment_type_daily_restrictions_date_location_ty; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889580_appointment_type_daily_restrictions_date_location_ty ON vaxiom.appointment_type_daily_restrictions USING btree (local_date, location_id, type_id);


--
-- TOC entry 6668 (class 1259 OID 891395)
-- Name: idx_889580_location_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889580_location_id_fk ON vaxiom.appointment_type_daily_restrictions USING btree (location_id);


--
-- TOC entry 6671 (class 1259 OID 891419)
-- Name: idx_889580_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889580_type_id ON vaxiom.appointment_type_daily_restrictions USING btree (type_id, local_date, location_id);


--
-- TOC entry 6674 (class 1259 OID 891396)
-- Name: idx_889584_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889584_template_id ON vaxiom.appt_type_template_restrictions USING btree (template_id, type_id);


--
-- TOC entry 6675 (class 1259 OID 891393)
-- Name: idx_889584_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889584_type_id ON vaxiom.appt_type_template_restrictions USING btree (type_id);


--
-- TOC entry 6680 (class 1259 OID 891408)
-- Name: idx_889594_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889594_user_id ON vaxiom.audit_logs USING btree (user_id);


--
-- TOC entry 6683 (class 1259 OID 891405)
-- Name: idx_889597_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889597_receivable_id ON vaxiom.autopay_invoice_changes USING btree (receivable_id);


--
-- TOC entry 6686 (class 1259 OID 891425)
-- Name: idx_889609_employee_benefit_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889609_employee_benefit_id ON vaxiom.benefit_saving_account_values USING btree (employee_benefit_id, insurance_plan_saving_account_id);


--
-- TOC entry 6687 (class 1259 OID 891428)
-- Name: idx_889609_fk_benefit_saving_account_values_employee_beneficiar; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889609_fk_benefit_saving_account_values_employee_beneficiar ON vaxiom.benefit_saving_account_values USING btree (employee_beneficiary_group_id);


--
-- TOC entry 6688 (class 1259 OID 891409)
-- Name: idx_889609_fk_benefit_saving_accout_values_employee_benefit1_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889609_fk_benefit_saving_accout_values_employee_benefit1_id ON vaxiom.benefit_saving_account_values USING btree (employee_benefit_id);


--
-- TOC entry 6689 (class 1259 OID 891404)
-- Name: idx_889609_fk_benefit_saving_accout_values_insurance_plan_savin; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889609_fk_benefit_saving_accout_values_insurance_plan_savin ON vaxiom.benefit_saving_account_values USING btree (insurance_plan_saving_account_id);


--
-- TOC entry 6696 (class 1259 OID 891418)
-- Name: idx_889624_patient_image_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889624_patient_image_id ON vaxiom.cephx_requests USING btree (patient_image_id);


--
-- TOC entry 6701 (class 1259 OID 891416)
-- Name: idx_889635_chair_allocations_chair_id_primary_resource_ca_date; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889635_chair_allocations_chair_id_primary_resource_ca_date ON vaxiom.chair_allocations USING btree (chair_id, ca_date, primary_resource);


--
-- TOC entry 6702 (class 1259 OID 891435)
-- Name: idx_889635_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889635_chair_id ON vaxiom.chair_allocations USING btree (chair_id);


--
-- TOC entry 6705 (class 1259 OID 891420)
-- Name: idx_889635_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889635_resource_id ON vaxiom.chair_allocations USING btree (resource_id);


--
-- TOC entry 6708 (class 1259 OID 891412)
-- Name: idx_889642_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889642_organization_id ON vaxiom.checks USING btree (organization_id);


--
-- TOC entry 6711 (class 1259 OID 891417)
-- Name: idx_889642_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889642_type_id ON vaxiom.checks USING btree (type_id);


--
-- TOC entry 6716 (class 1259 OID 891422)
-- Name: idx_889648_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889648_receivable_id ON vaxiom.claim_events USING btree (receivable_id);


--
-- TOC entry 6717 (class 1259 OID 891423)
-- Name: idx_889654_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889654_location_id ON vaxiom.claim_requests USING btree (location_id);


--
-- TOC entry 6718 (class 1259 OID 891446)
-- Name: idx_889654_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889654_patient_id ON vaxiom.claim_requests USING btree (patient_id);


--
-- TOC entry 6721 (class 1259 OID 891431)
-- Name: idx_889654_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889654_receivable_id ON vaxiom.claim_requests USING btree (receivable_id);


--
-- TOC entry 6722 (class 1259 OID 891430)
-- Name: idx_889654_treatment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889654_treatment_id ON vaxiom.claim_requests USING btree (treatment_id);


--
-- TOC entry 6723 (class 1259 OID 891426)
-- Name: idx_889660_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889660_location_id ON vaxiom.collections_cache USING btree (location_id, date);


--
-- TOC entry 6724 (class 1259 OID 891439)
-- Name: idx_889666_collection_label_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889666_collection_label_template_id ON vaxiom.collection_labels USING btree (collection_label_template_id);


--
-- TOC entry 6725 (class 1259 OID 891424)
-- Name: idx_889666_last_modified_by; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889666_last_modified_by ON vaxiom.collection_labels USING btree (last_modified_by);


--
-- TOC entry 6726 (class 1259 OID 891438)
-- Name: idx_889666_payment_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889666_payment_account_id ON vaxiom.collection_labels USING btree (payment_account_id);


--
-- TOC entry 6729 (class 1259 OID 891434)
-- Name: idx_889669_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889669_parent_company_id ON vaxiom.collection_label_agencies USING btree (parent_company_id);


--
-- TOC entry 6732 (class 1259 OID 891458)
-- Name: idx_889672_created_by; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889672_created_by ON vaxiom.collection_label_history USING btree (created_by);


--
-- TOC entry 6733 (class 1259 OID 891457)
-- Name: idx_889672_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889672_patient_id ON vaxiom.collection_label_history USING btree (patient_id);


--
-- TOC entry 6734 (class 1259 OID 891440)
-- Name: idx_889672_payment_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889672_payment_account_id ON vaxiom.collection_label_history USING btree (payment_account_id);


--
-- TOC entry 6735 (class 1259 OID 891444)
-- Name: idx_889672_previous_collection_label_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889672_previous_collection_label_template_id ON vaxiom.collection_label_history USING btree (previous_collection_label_template_id);


--
-- TOC entry 6738 (class 1259 OID 891450)
-- Name: idx_889675_agency_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889675_agency_id ON vaxiom.collection_label_templates USING btree (agency_id);


--
-- TOC entry 6739 (class 1259 OID 891436)
-- Name: idx_889675_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889675_parent_company_id ON vaxiom.collection_label_templates USING btree (parent_company_id);


--
-- TOC entry 6742 (class 1259 OID 891442)
-- Name: idx_889681_contact_method_association_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889681_contact_method_association_id ON vaxiom.communication_preferences USING btree (contact_method_association_id);


--
-- TOC entry 6743 (class 1259 OID 891445)
-- Name: idx_889681_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889681_patient_id ON vaxiom.communication_preferences USING btree (patient_id);


--
-- TOC entry 6746 (class 1259 OID 891468)
-- Name: idx_889684_address; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889684_address ON vaxiom.contact_emails USING btree (address);


--
-- TOC entry 6751 (class 1259 OID 891454)
-- Name: idx_889691_contact_method_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889691_contact_method_id ON vaxiom.contact_method_associations USING btree (contact_method_id);


--
-- TOC entry 6752 (class 1259 OID 891453)
-- Name: idx_889691_error_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889691_error_location_id ON vaxiom.contact_method_associations USING btree (error_location_id);


--
-- TOC entry 6753 (class 1259 OID 891461)
-- Name: idx_889691_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889691_person_id ON vaxiom.contact_method_associations USING btree (person_id);


--
-- TOC entry 6756 (class 1259 OID 891459)
-- Name: idx_889694_number; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889694_number ON vaxiom.contact_phones USING btree (number);


--
-- TOC entry 6759 (class 1259 OID 891456)
-- Name: idx_889697_address_line1_city_zip_state; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889697_address_line1_city_zip_state ON vaxiom.contact_postal_addresses USING btree (address_line1, city, zip, state);


--
-- TOC entry 6760 (class 1259 OID 891455)
-- Name: idx_889697_contact_postal_addresses_city; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889697_contact_postal_addresses_city ON vaxiom.contact_postal_addresses USING btree (city);


--
-- TOC entry 6767 (class 1259 OID 891464)
-- Name: idx_889709_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889709_name ON vaxiom.correction_types USING btree (name, parent_company_id);


--
-- TOC entry 6768 (class 1259 OID 891463)
-- Name: idx_889709_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889709_parent_company_id ON vaxiom.correction_types USING btree (parent_company_id);


--
-- TOC entry 6771 (class 1259 OID 891460)
-- Name: idx_889715_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889715_organization_id ON vaxiom.correction_types_locations USING btree (organization_id);


--
-- TOC entry 6774 (class 1259 OID 891465)
-- Name: idx_889718_correction_types_payment_options_ibfk_2; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889718_correction_types_payment_options_ibfk_2 ON vaxiom.correction_types_payment_options USING btree (payment_options_id);


--
-- TOC entry 6777 (class 1259 OID 891467)
-- Name: idx_889721_county_state_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889721_county_state_index ON vaxiom.county USING btree (state);


--
-- TOC entry 6780 (class 1259 OID 891493)
-- Name: idx_889724_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889724_location_id ON vaxiom.daily_transactions USING btree (location_id, date);


--
-- TOC entry 6783 (class 1259 OID 891475)
-- Name: idx_889731_data_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889731_data_id ON vaxiom.data_lock USING btree (data_id);


--
-- TOC entry 6786 (class 1259 OID 891481)
-- Name: idx_889731_session_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889731_session_id ON vaxiom.data_lock USING btree (session_id, user_id, data_id);


--
-- TOC entry 6787 (class 1259 OID 891471)
-- Name: idx_889734_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889734_organization_id_fk ON vaxiom.day_schedules USING btree (organization_id);


--
-- TOC entry 6790 (class 1259 OID 891478)
-- Name: idx_889734_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889734_resource_id ON vaxiom.day_schedules USING btree (resource_id);


--
-- TOC entry 6791 (class 1259 OID 891476)
-- Name: idx_889741_appt_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889741_appt_type_id ON vaxiom.day_schedule_appt_slots USING btree (appt_type_id);


--
-- TOC entry 6798 (class 1259 OID 891483)
-- Name: idx_889750_location_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889750_location_unique ON vaxiom.default_appointment_templates USING btree (location_id);


--
-- TOC entry 6801 (class 1259 OID 891485)
-- Name: idx_889750_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889750_template_id ON vaxiom.default_appointment_templates USING btree (template_id);


--
-- TOC entry 6802 (class 1259 OID 891490)
-- Name: idx_889753_marking_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889753_marking_id_fk ON vaxiom.dentalchart_additional_markings USING btree (marking_id);


--
-- TOC entry 6807 (class 1259 OID 891489)
-- Name: idx_889756_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889756_snapshot_id ON vaxiom.dentalchart_edit_log USING btree (snapshot_id);


--
-- TOC entry 6810 (class 1259 OID 891492)
-- Name: idx_889759_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889759_snapshot_id ON vaxiom.dentalchart_marking USING btree (snapshot_id);


--
-- TOC entry 6813 (class 1259 OID 891517)
-- Name: idx_889768_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889768_appointment_id ON vaxiom.dentalchart_snapshot USING btree (appointment_id);


--
-- TOC entry 6816 (class 1259 OID 891497)
-- Name: idx_889774_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889774_appointment_id ON vaxiom.diagnosis USING btree (appointment_id);


--
-- TOC entry 6817 (class 1259 OID 891496)
-- Name: idx_889774_diagnosis_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889774_diagnosis_template_id ON vaxiom.diagnosis USING btree (diagnosis_template_id);


--
-- TOC entry 6822 (class 1259 OID 891507)
-- Name: idx_889786_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889786_address_id ON vaxiom.diagnosis_letters USING btree (address_id);


--
-- TOC entry 6823 (class 1259 OID 891501)
-- Name: idx_889786_diagnosis_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889786_diagnosis_id ON vaxiom.diagnosis_letters USING btree (diagnosis_id);


--
-- TOC entry 6824 (class 1259 OID 891498)
-- Name: idx_889786_document_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889786_document_template_id ON vaxiom.diagnosis_letters USING btree (document_template_id);


--
-- TOC entry 6827 (class 1259 OID 891530)
-- Name: idx_889786_recipient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889786_recipient_id ON vaxiom.diagnosis_letters USING btree (recipient_id);


--
-- TOC entry 6828 (class 1259 OID 891528)
-- Name: idx_889789_appointment_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889789_appointment_type_id ON vaxiom.diagnosis_templates USING btree (appointment_type_id);


--
-- TOC entry 6829 (class 1259 OID 891504)
-- Name: idx_889789_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889789_parent_company_id ON vaxiom.diagnosis_templates USING btree (parent_company_id);


--
-- TOC entry 6832 (class 1259 OID 891506)
-- Name: idx_889789_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889789_tx_category_id ON vaxiom.diagnosis_templates USING btree (tx_category_id);


--
-- TOC entry 6833 (class 1259 OID 891510)
-- Name: idx_889795_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889795_organization_id ON vaxiom.discounts USING btree (organization_id);


--
-- TOC entry 6836 (class 1259 OID 891519)
-- Name: idx_889802_discount_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889802_discount_id ON vaxiom.discount_adjustments USING btree (discount_id);


--
-- TOC entry 6839 (class 1259 OID 891509)
-- Name: idx_889805_discount_adjustment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889805_discount_adjustment_id ON vaxiom.discount_reverse_adjustments USING btree (discount_adjustment_id);


--
-- TOC entry 6846 (class 1259 OID 891514)
-- Name: idx_889817_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889817_patient_id ON vaxiom.dock_item_features_images USING btree (patient_id);


--
-- TOC entry 6851 (class 1259 OID 891521)
-- Name: idx_889823_note_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889823_note_id ON vaxiom.dock_item_features_note USING btree (note_id);


--
-- TOC entry 6858 (class 1259 OID 891520)
-- Name: idx_889835_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889835_organization_id ON vaxiom.document_templates USING btree (organization_id, name);


--
-- TOC entry 6861 (class 1259 OID 891551)
-- Name: idx_889842_ix_document_tree_nodes_file_uid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889842_ix_document_tree_nodes_file_uid ON vaxiom.document_tree_nodes USING btree (file_uid);


--
-- TOC entry 6862 (class 1259 OID 891548)
-- Name: idx_889842_ix_document_tree_nodes_parent_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889842_ix_document_tree_nodes_parent_id ON vaxiom.document_tree_nodes USING btree (parent_id);


--
-- TOC entry 6863 (class 1259 OID 891525)
-- Name: idx_889842_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889842_patient_id ON vaxiom.document_tree_nodes USING btree (patient_id);


--
-- TOC entry 6868 (class 1259 OID 891532)
-- Name: idx_889853_fk_employee_benebitiary_employee_benefitiary_group1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889853_fk_employee_benebitiary_employee_benefitiary_group1_ ON vaxiom.employee_beneficiary USING btree (employee_beneficiary_group_id);


--
-- TOC entry 6869 (class 1259 OID 891523)
-- Name: idx_889853_fk_employee_benebitiary_employee_enrollment_person1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889853_fk_employee_benebitiary_employee_enrollment_person1_ ON vaxiom.employee_beneficiary USING btree (employee_enrollment_person_id);


--
-- TOC entry 6872 (class 1259 OID 891533)
-- Name: idx_889856_fk_employee_benefitiary_group_employee_enrollment1_i; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889856_fk_employee_benefitiary_group_employee_enrollment1_i ON vaxiom.employee_beneficiary_group USING btree (employee_enrollment_id);


--
-- TOC entry 6875 (class 1259 OID 891537)
-- Name: idx_889859_benefit_type; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889859_benefit_type ON vaxiom.employee_benefit USING btree (benefit_type, employee_enrollment_id);


--
-- TOC entry 6876 (class 1259 OID 891558)
-- Name: idx_889859_fk_employee_benefit_employee_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889859_fk_employee_benefit_employee_enrollment1_idx ON vaxiom.employee_benefit USING btree (employee_enrollment_id);


--
-- TOC entry 6877 (class 1259 OID 891559)
-- Name: idx_889859_fk_employee_benefit_insurance_palan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889859_fk_employee_benefit_insurance_palan1_idx ON vaxiom.employee_benefit USING btree (insurance_plan_id);


--
-- TOC entry 6880 (class 1259 OID 891535)
-- Name: idx_889867_fk_employee_dependent_employee_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889867_fk_employee_dependent_employee_enrollment1_idx ON vaxiom.employee_dependent USING btree (employee_enrollment_id);


--
-- TOC entry 6881 (class 1259 OID 891540)
-- Name: idx_889867_fk_employee_dependet_employee_enrollment_person1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889867_fk_employee_dependet_employee_enrollment_person1_idx ON vaxiom.employee_dependent USING btree (employee_enrollment_person_id);


--
-- TOC entry 6884 (class 1259 OID 891536)
-- Name: idx_889873_fk_employee_enrollment_employment_contracts1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889873_fk_employee_enrollment_employment_contracts1_idx ON vaxiom.employee_enrollment USING btree (employment_contracts_id);


--
-- TOC entry 6885 (class 1259 OID 891555)
-- Name: idx_889873_fk_employee_enrollment_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889873_fk_employee_enrollment_enrollment1_idx ON vaxiom.employee_enrollment USING btree (enrollment_id);


--
-- TOC entry 6886 (class 1259 OID 891549)
-- Name: idx_889873_fk_employee_enrollment_enrollment_form_source_event1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889873_fk_employee_enrollment_enrollment_form_source_event1 ON vaxiom.employee_enrollment USING btree (enrollment_form_source_event_id);


--
-- TOC entry 6889 (class 1259 OID 891545)
-- Name: idx_889882_fk_employee_enrollment_attachment_employee_enrollmen; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889882_fk_employee_enrollment_attachment_employee_enrollmen ON vaxiom.employee_enrollment_attachment USING btree (employee_enrollment_id);


--
-- TOC entry 6892 (class 1259 OID 891570)
-- Name: idx_889888_fk_employee_enrollment_event_employee_enrollment1_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889888_fk_employee_enrollment_event_employee_enrollment1_id ON vaxiom.employee_enrollment_event USING btree (employee_enrollment_id);


--
-- TOC entry 6895 (class 1259 OID 891546)
-- Name: idx_889894_fk_employee_enrollment_person_employee_enrollment1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889894_fk_employee_enrollment_person_employee_enrollment1 ON vaxiom.employee_enrollment_person USING btree (employee_enrollment_id);


--
-- TOC entry 6898 (class 1259 OID 891553)
-- Name: idx_889900_fk_employee_insured_employee_benefit1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889900_fk_employee_insured_employee_benefit1_idx ON vaxiom.employee_insured USING btree (employee_benefit_id);


--
-- TOC entry 6899 (class 1259 OID 891547)
-- Name: idx_889900_fk_employee_insured_employee_benefitiary_group1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889900_fk_employee_insured_employee_benefitiary_group1_idx ON vaxiom.employee_insured USING btree (employee_beneficiary_group_id);


--
-- TOC entry 6900 (class 1259 OID 891565)
-- Name: idx_889900_fk_employee_insured_employee_dependet1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889900_fk_employee_insured_employee_dependet1_idx ON vaxiom.employee_insured USING btree (employee_dependent_id);


--
-- TOC entry 6905 (class 1259 OID 891557)
-- Name: idx_889904_provider_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889904_provider_id ON vaxiom.employee_professional_relationships USING btree (provider_id);


--
-- TOC entry 6906 (class 1259 OID 891576)
-- Name: idx_889907_employment_contract_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889907_employment_contract_id ON vaxiom.employee_resources USING btree (employment_contract_id);


--
-- TOC entry 6909 (class 1259 OID 891552)
-- Name: idx_889911_contact_email_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_contact_email_id ON vaxiom.employers USING btree (contact_email_id);


--
-- TOC entry 6910 (class 1259 OID 891562)
-- Name: idx_889911_contact_phone_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_contact_phone_id ON vaxiom.employers USING btree (contact_phone_id);


--
-- TOC entry 6911 (class 1259 OID 891560)
-- Name: idx_889911_contact_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_contact_postal_address_id ON vaxiom.employers USING btree (contact_postal_address_id);


--
-- TOC entry 6912 (class 1259 OID 891569)
-- Name: idx_889911_master_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_master_id ON vaxiom.employers USING btree (master_id);


--
-- TOC entry 6913 (class 1259 OID 891554)
-- Name: idx_889911_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889911_organization_id ON vaxiom.employers USING btree (organization_id);


--
-- TOC entry 6916 (class 1259 OID 891571)
-- Name: idx_889914_fk_employment_contracts_enrollment_group1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_enrollment_group1_idx ON vaxiom.employment_contracts USING btree (enrollment_group_id);


--
-- TOC entry 6917 (class 1259 OID 891566)
-- Name: idx_889914_fk_employment_contracts_jobtitle1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_jobtitle1_idx ON vaxiom.employment_contracts USING btree (jobtitle_id);


--
-- TOC entry 6918 (class 1259 OID 891567)
-- Name: idx_889914_fk_employment_contracts_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_legal_entity1_idx ON vaxiom.employment_contracts USING btree (legal_entity_id);


--
-- TOC entry 6919 (class 1259 OID 891585)
-- Name: idx_889914_fk_employment_contracts_legal_entity_billing_group1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_legal_entity_billing_group1_ ON vaxiom.employment_contracts USING btree (billing_group_id);


--
-- TOC entry 6920 (class 1259 OID 891575)
-- Name: idx_889914_fk_employment_contracts_sys_organization_address1_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_sys_organization_address1_id ON vaxiom.employment_contracts USING btree (physical_address_id);


--
-- TOC entry 6921 (class 1259 OID 891587)
-- Name: idx_889914_fk_employment_contracts_sys_organizations1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_sys_organizations1_idx ON vaxiom.employment_contracts USING btree (department_id);


--
-- TOC entry 6922 (class 1259 OID 891561)
-- Name: idx_889914_fk_employment_contracts_sys_organizations2_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_fk_employment_contracts_sys_organizations2_idx ON vaxiom.employment_contracts USING btree (division_id);


--
-- TOC entry 6923 (class 1259 OID 891572)
-- Name: idx_889914_human_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889914_human_id ON vaxiom.employment_contracts USING btree (legacy_id);


--
-- TOC entry 6924 (class 1259 OID 891578)
-- Name: idx_889914_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_organization_id_fk ON vaxiom.employment_contracts USING btree (organization_id);


--
-- TOC entry 6925 (class 1259 OID 891563)
-- Name: idx_889914_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889914_person_id ON vaxiom.employment_contracts USING btree (person_id);


--
-- TOC entry 6928 (class 1259 OID 891581)
-- Name: idx_889921_fk_employee_emergency_contact_employment_contracts1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889921_fk_employee_emergency_contact_employment_contracts1_ ON vaxiom.employment_contract_emergency_contact USING btree (employment_contracts_id);


--
-- TOC entry 6931 (class 1259 OID 891574)
-- Name: idx_889924_fk_employment_contract_enrollment_group_log_employme; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889924_fk_employment_contract_enrollment_group_log_employme ON vaxiom.employment_contract_enrollment_group_log USING btree (employment_contracts_id);


--
-- TOC entry 6934 (class 1259 OID 891598)
-- Name: idx_889930_fk_employement_contract_job_status_log_employment_co; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889930_fk_employement_contract_job_status_log_employment_co ON vaxiom.employment_contract_job_status_log USING btree (employment_contracts_id);


--
-- TOC entry 6937 (class 1259 OID 891594)
-- Name: idx_889933_emploment_contract_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889933_emploment_contract_fk ON vaxiom.employment_contract_location_group USING btree (employment_contract_id);


--
-- TOC entry 6938 (class 1259 OID 891582)
-- Name: idx_889933_location_group_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889933_location_group_fk ON vaxiom.employment_contract_location_group USING btree (location_group_id);


--
-- TOC entry 6941 (class 1259 OID 891583)
-- Name: idx_889936_ecperm_emploment_contract_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889936_ecperm_emploment_contract_fk ON vaxiom.employment_contract_permissions USING btree (employment_contract_id);


--
-- TOC entry 6942 (class 1259 OID 891597)
-- Name: idx_889936_ecperm_permission_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889936_ecperm_permission_fk ON vaxiom.employment_contract_permissions USING btree (permissions_permission_id);


--
-- TOC entry 6945 (class 1259 OID 891589)
-- Name: idx_889939_ecrole_user_role_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889939_ecrole_user_role_fk ON vaxiom.employment_contract_role USING btree (permissions_user_role_id);


--
-- TOC entry 6946 (class 1259 OID 891584)
-- Name: idx_889939_emploment_contract_fk_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889939_emploment_contract_fk_idx ON vaxiom.employment_contract_role USING btree (employment_contract_id);


--
-- TOC entry 6949 (class 1259 OID 891609)
-- Name: idx_889942_fk_employment_contract_salary_log_employment_cont_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889942_fk_employment_contract_salary_log_employment_cont_id ON vaxiom.employment_contract_salary_log USING btree (employment_contracts_id);


--
-- TOC entry 6952 (class 1259 OID 891603)
-- Name: idx_889948_fk_enrollment_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889948_fk_enrollment_legal_entity1_idx ON vaxiom.enrollment USING btree (legal_entity_id);


--
-- TOC entry 6955 (class 1259 OID 891596)
-- Name: idx_889948_uq_year_lagal_entity; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_889948_uq_year_lagal_entity ON vaxiom.enrollment USING btree (year, legal_entity_id);


--
-- TOC entry 6956 (class 1259 OID 891591)
-- Name: idx_889956_employee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889956_employee_id ON vaxiom.enrollment_form USING btree (employee_id);


--
-- TOC entry 6957 (class 1259 OID 891605)
-- Name: idx_889956_enrollment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889956_enrollment_id ON vaxiom.enrollment_form USING btree (enrollment_id);


--
-- TOC entry 6960 (class 1259 OID 891599)
-- Name: idx_889962_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889962_enrollment_form_id ON vaxiom.enrollment_form_beneficiary USING btree (enrollment_form_id);


--
-- TOC entry 6963 (class 1259 OID 891623)
-- Name: idx_889971_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889971_enrollment_form_id ON vaxiom.enrollment_form_benefits USING btree (enrollment_form_id);


--
-- TOC entry 6966 (class 1259 OID 891606)
-- Name: idx_889980_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889980_enrollment_form_id ON vaxiom.enrollment_form_dependents USING btree (enrollment_form_id);


--
-- TOC entry 6969 (class 1259 OID 891604)
-- Name: idx_889989_enrollment_form_benefit_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889989_enrollment_form_benefit_id ON vaxiom.enrollment_form_dependents_benefits USING btree (enrollment_form_benefit_id);


--
-- TOC entry 6970 (class 1259 OID 891610)
-- Name: idx_889989_enrollment_form_dependent_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889989_enrollment_form_dependent_id ON vaxiom.enrollment_form_dependents_benefits USING btree (enrollment_form_dependent_id);


--
-- TOC entry 6973 (class 1259 OID 891617)
-- Name: idx_889998_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_889998_enrollment_form_id ON vaxiom.enrollment_form_personal_data USING btree (enrollment_form_id);


--
-- TOC entry 6980 (class 1259 OID 891632)
-- Name: idx_890014_fk_eg_enrollment; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890014_fk_eg_enrollment ON vaxiom.enrollment_groups_enrollment_legal_entity USING btree (enrollment_id);


--
-- TOC entry 6981 (class 1259 OID 891634)
-- Name: idx_890014_fk_egle_legal_entity; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890014_fk_egle_legal_entity ON vaxiom.enrollment_groups_enrollment_legal_entity USING btree (legal_entity_id);


--
-- TOC entry 6984 (class 1259 OID 891622)
-- Name: idx_890017_enrollment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890017_enrollment_id ON vaxiom.enrollment_group_enrollment USING btree (enrollment_id);


--
-- TOC entry 6987 (class 1259 OID 891619)
-- Name: idx_890020_legal_entity_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890020_legal_entity_id ON vaxiom.enrollment_schedule_billing_groups USING btree (legal_entity_id);


--
-- TOC entry 6992 (class 1259 OID 891624)
-- Name: idx_890027_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890027_postal_address_id ON vaxiom.external_offices USING btree (postal_address_id);


--
-- TOC entry 6995 (class 1259 OID 891616)
-- Name: idx_890030_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890030_organization_id ON vaxiom.financial_settings USING btree (organization_id);


--
-- TOC entry 7000 (class 1259 OID 891692)
-- Name: idx_890040_idx_gf10219; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890040_idx_gf10219 ON vaxiom.gf10219 USING btree (appointment_id);


--
-- TOC entry 7005 (class 1259 OID 891629)
-- Name: idx_890068_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890068_organization_id ON vaxiom.image_series USING btree (organization_id);


--
-- TOC entry 7008 (class 1259 OID 891636)
-- Name: idx_890068_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890068_type_id ON vaxiom.image_series USING btree (type_id);


--
-- TOC entry 7011 (class 1259 OID 891628)
-- Name: idx_890074_payment_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890074_payment_plan_id ON vaxiom.installment_charges USING btree (payment_plan_id);


--
-- TOC entry 7014 (class 1259 OID 891653)
-- Name: idx_890077_contact_email_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_contact_email_id ON vaxiom.insurance_billing_centers USING btree (contact_email_id);


--
-- TOC entry 7015 (class 1259 OID 891654)
-- Name: idx_890077_contact_phone_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_contact_phone_id ON vaxiom.insurance_billing_centers USING btree (contact_phone_id);


--
-- TOC entry 7016 (class 1259 OID 891701)
-- Name: idx_890077_contact_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_contact_postal_address_id ON vaxiom.insurance_billing_centers USING btree (contact_postal_address_id);


--
-- TOC entry 7017 (class 1259 OID 891640)
-- Name: idx_890077_master_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_master_id ON vaxiom.insurance_billing_centers USING btree (master_id);


--
-- TOC entry 7018 (class 1259 OID 891635)
-- Name: idx_890077_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890077_organization_id ON vaxiom.insurance_billing_centers USING btree (organization_id);


--
-- TOC entry 7021 (class 1259 OID 891631)
-- Name: idx_890083_fk_billing_entity_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_billing_entity_idx ON vaxiom.insurance_claims_setup USING btree (billing_entity_id);


--
-- TOC entry 7022 (class 1259 OID 891646)
-- Name: idx_890083_fk_insurance_claims_setup_legal_entity_npi1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_legal_entity_npi1_idx ON vaxiom.insurance_claims_setup USING btree (legal_entity_npi_id);


--
-- TOC entry 7023 (class 1259 OID 891642)
-- Name: idx_890083_fk_insurance_claims_setup_legal_entity_tin1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_legal_entity_tin1_idx ON vaxiom.insurance_claims_setup USING btree (tin_id);


--
-- TOC entry 7024 (class 1259 OID 891637)
-- Name: idx_890083_fk_insurance_claims_setup_sys_organization_address1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_sys_organization_address1_ ON vaxiom.insurance_claims_setup USING btree (sys_organization_address_id);


--
-- TOC entry 7025 (class 1259 OID 891663)
-- Name: idx_890083_fk_insurance_claims_setup_sys_organization_contact_m; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_sys_organization_contact_m ON vaxiom.insurance_claims_setup USING btree (sys_organization_contact_method_id);


--
-- TOC entry 7026 (class 1259 OID 891638)
-- Name: idx_890083_fk_insurance_claims_setup_sys_organizations_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890083_fk_insurance_claims_setup_sys_organizations_idx ON vaxiom.insurance_claims_setup USING btree (sys_organizations_id);


--
-- TOC entry 7031 (class 1259 OID 891650)
-- Name: idx_890093_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890093_insurance_code_id ON vaxiom.insurance_code_val USING btree (insurance_code_id);


--
-- TOC entry 7032 (class 1259 OID 891645)
-- Name: idx_890093_insurance_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890093_insurance_company_id ON vaxiom.insurance_code_val USING btree (insurance_company_id);


--
-- TOC entry 7033 (class 1259 OID 891647)
-- Name: idx_890093_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890093_organization_id_fk ON vaxiom.insurance_code_val USING btree (organization_id);


--
-- TOC entry 7036 (class 1259 OID 891656)
-- Name: idx_890096_am_email_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_am_email_address_id ON vaxiom.insurance_companies USING btree (am_email_address_id);


--
-- TOC entry 7037 (class 1259 OID 891652)
-- Name: idx_890096_am_phone_number_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_am_phone_number_id ON vaxiom.insurance_companies USING btree (am_phone_number_id);


--
-- TOC entry 7038 (class 1259 OID 891649)
-- Name: idx_890096_master_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_master_id ON vaxiom.insurance_companies USING btree (master_id);


--
-- TOC entry 7039 (class 1259 OID 891648)
-- Name: idx_890096_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_organization_id ON vaxiom.insurance_companies USING btree (organization_id);


--
-- TOC entry 7040 (class 1259 OID 891673)
-- Name: idx_890096_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890096_postal_address_id ON vaxiom.insurance_companies USING btree (postal_address_id);


--
-- TOC entry 7045 (class 1259 OID 891660)
-- Name: idx_890107_contact_method_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890107_contact_method_id ON vaxiom.insurance_company_phone_associations USING btree (contact_method_id);


--
-- TOC entry 7046 (class 1259 OID 891655)
-- Name: idx_890107_insurance_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890107_insurance_company_id ON vaxiom.insurance_company_phone_associations USING btree (insurance_company_id);


--
-- TOC entry 7049 (class 1259 OID 891651)
-- Name: idx_890111_fk_insurancepaymentid_insurancecompanies; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890111_fk_insurancepaymentid_insurancecompanies ON vaxiom.insurance_payments USING btree (insurance_company_id);


--
-- TOC entry 7050 (class 1259 OID 891666)
-- Name: idx_890111_fk_paymenttypeid_paymenttypes; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890111_fk_paymenttypeid_paymenttypes ON vaxiom.insurance_payments USING btree (payment_type_id);


--
-- TOC entry 7053 (class 1259 OID 891661)
-- Name: idx_890117_insurance_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890117_insurance_company_id ON vaxiom.insurance_payment_accounts USING btree (insurance_company_id);


--
-- TOC entry 7054 (class 1259 OID 891657)
-- Name: idx_890117_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890117_organization_id ON vaxiom.insurance_payment_accounts USING btree (organization_id, insurance_company_id);


--
-- TOC entry 7057 (class 1259 OID 891685)
-- Name: idx_890120_fk_insurancepaymentcorrectionscorrectiontypeid_corre; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890120_fk_insurancepaymentcorrectionscorrectiontypeid_corre ON vaxiom.insurance_payment_corrections USING btree (correction_type_id);


--
-- TOC entry 7060 (class 1259 OID 891670)
-- Name: idx_890123_fk_insurancepaymentmovementid_insurancepaymentid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890123_fk_insurancepaymentmovementid_insurancepaymentid ON vaxiom.insurance_payment_movements USING btree (insurance_payment_id);


--
-- TOC entry 7065 (class 1259 OID 891659)
-- Name: idx_890132_fk_insurancepaymenttransferpaymentid_paymentid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890132_fk_insurancepaymenttransferpaymentid_paymentid ON vaxiom.insurance_payment_transfers USING btree (payment_id);


--
-- TOC entry 7068 (class 1259 OID 891671)
-- Name: idx_890135_fk_insurance_plan_sys_organizations1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890135_fk_insurance_plan_sys_organizations1_idx ON vaxiom.insurance_plan USING btree (sys_organizations_id);


--
-- TOC entry 7073 (class 1259 OID 891693)
-- Name: idx_890145_fk_insurance_plan_age_and_gender_banded_rate_insuran; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890145_fk_insurance_plan_age_and_gender_banded_rate_insuran ON vaxiom.insurance_plan_age_and_gender_banded_rate USING btree (id);


--
-- TOC entry 7076 (class 1259 OID 891739)
-- Name: idx_890148_fk_insurance_plan_age_banded_rate_insurance_plan_rat; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890148_fk_insurance_plan_age_banded_rate_insurance_plan_rat ON vaxiom.insurance_plan_age_banded_rate USING btree (id);


--
-- TOC entry 7083 (class 1259 OID 891668)
-- Name: idx_890157_fk_insurance_plan_benefit_amount_insurance_plan2_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890157_fk_insurance_plan_benefit_amount_insurance_plan2_idx ON vaxiom.insurance_plan_benefit_amount USING btree (insurance_plan_id);


--
-- TOC entry 7090 (class 1259 OID 891677)
-- Name: idx_890166_fk_insurance_plan_complex_benefit_amount_item_insura; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890166_fk_insurance_plan_complex_benefit_amount_item_insura ON vaxiom.insurance_plan_complex_benefit_amount_item USING btree (insurance_plan_complex_benefit_amount_id);


--
-- TOC entry 7095 (class 1259 OID 891750)
-- Name: idx_890172_fk_insurance_plan_composition_rate_insurance_plan_ra; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890172_fk_insurance_plan_composition_rate_insurance_plan_ra ON vaxiom.insurance_plan_composition_rate USING btree (id);


--
-- TOC entry 7098 (class 1259 OID 891686)
-- Name: idx_890175_employer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890175_employer_id ON vaxiom.insurance_plan_employers USING btree (employer_id);


--
-- TOC entry 7101 (class 1259 OID 891680)
-- Name: idx_890178_fk_fixedlimits_insurance_limits1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890178_fk_fixedlimits_insurance_limits1_idx ON vaxiom.insurance_plan_fixed_limit USING btree (insurance_limits_id);


--
-- TOC entry 7108 (class 1259 OID 891691)
-- Name: idx_890189_fk_insurance_plan_has_enrollment_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890189_fk_insurance_plan_has_enrollment_enrollment1_idx ON vaxiom.insurance_plan_has_enrollment USING btree (enrollment_id);


--
-- TOC entry 7109 (class 1259 OID 891714)
-- Name: idx_890189_fk_insurance_plan_has_enrollment_insurance_plan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890189_fk_insurance_plan_has_enrollment_insurance_plan1_idx ON vaxiom.insurance_plan_has_enrollment USING btree (insurance_plan_id);


--
-- TOC entry 7112 (class 1259 OID 891761)
-- Name: idx_890189_uq_insurance_plan_enrollment; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890189_uq_insurance_plan_enrollment ON vaxiom.insurance_plan_has_enrollment USING btree (insurance_plan_id, enrollment_id);


--
-- TOC entry 7113 (class 1259 OID 891694)
-- Name: idx_890193_fk_insurance_plan_has_enrollment_has_enrollment_grou; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890193_fk_insurance_plan_has_enrollment_has_enrollment_grou ON vaxiom.insurance_plan_has_enrollment_has_enrollment_group USING btree (insurance_plan_has_enrollment_id);


--
-- TOC entry 7116 (class 1259 OID 891690)
-- Name: idx_890196_fk_insurance_limits_insurance_palan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890196_fk_insurance_limits_insurance_palan1_idx ON vaxiom.insurance_plan_limits USING btree (insurance_plan_id);


--
-- TOC entry 7123 (class 1259 OID 891702)
-- Name: idx_890205_fk_insurance_plan_rates_insurance_plan_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890205_fk_insurance_plan_rates_insurance_plan_idx ON vaxiom.insurance_plan_rates USING btree (insurance_plan_id);


--
-- TOC entry 7126 (class 1259 OID 891730)
-- Name: idx_890208_fk_insurace_plan_saving_account_insurance_plan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890208_fk_insurace_plan_saving_account_insurance_plan1_idx ON vaxiom.insurance_plan_saving_account USING btree (insurance_plan_id);


--
-- TOC entry 7129 (class 1259 OID 891713)
-- Name: idx_890214_year_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890214_year_index ON vaxiom.insurance_plan_saving_accounts_limits USING btree (year);


--
-- TOC entry 7130 (class 1259 OID 891706)
-- Name: idx_890217_fk_separate_age_banded_rate_insurance_plan_rates_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890217_fk_separate_age_banded_rate_insurance_plan_rates_idx ON vaxiom.insurance_plan_separate_age_banded_rate USING btree (id);


--
-- TOC entry 7135 (class 1259 OID 891721)
-- Name: idx_890223_fk_slider_limits_insurance_limits1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890223_fk_slider_limits_insurance_limits1_idx ON vaxiom.insurance_plan_slider_limit USING btree (insurance_limits_id);


--
-- TOC entry 7138 (class 1259 OID 891715)
-- Name: idx_890226_fk_insurance_plan_summary_fields_insurance_plan1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890226_fk_insurance_plan_summary_fields_insurance_plan1_idx ON vaxiom.insurance_plan_summary_fields USING btree (insurance_plan_id);


--
-- TOC entry 7145 (class 1259 OID 891778)
-- Name: idx_890241_employer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890241_employer_id ON vaxiom.insurance_subscriptions USING btree (employer_id);


--
-- TOC entry 7146 (class 1259 OID 891724)
-- Name: idx_890241_insurance_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890241_insurance_plan_id ON vaxiom.insurance_subscriptions USING btree (insurance_plan_id);


--
-- TOC entry 7147 (class 1259 OID 891717)
-- Name: idx_890241_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890241_person_id ON vaxiom.insurance_subscriptions USING btree (person_id);


--
-- TOC entry 7150 (class 1259 OID 891712)
-- Name: idx_890249_insurance_subscription_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890249_insurance_subscription_id ON vaxiom.insured USING btree (insurance_subscription_id);


--
-- TOC entry 7151 (class 1259 OID 891732)
-- Name: idx_890249_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890249_patient_id ON vaxiom.insured USING btree (patient_id);


--
-- TOC entry 7154 (class 1259 OID 891727)
-- Name: idx_890253_fk_insured_rate_amount_employee_insured1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890253_fk_insured_rate_amount_employee_insured1_idx ON vaxiom.insured_benefit_values USING btree (employee_insured_id);


--
-- TOC entry 7159 (class 1259 OID 891751)
-- Name: idx_890259_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890259_account_id ON vaxiom.invoice_export_queue USING btree (account_id);


--
-- TOC entry 7160 (class 1259 OID 891787)
-- Name: idx_890259_invoice_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890259_invoice_id ON vaxiom.invoice_export_queue USING btree (invoice_id);


--
-- TOC entry 7163 (class 1259 OID 891728)
-- Name: idx_890262_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890262_account_id ON vaxiom.invoice_import_export_log USING btree (account_id);


--
-- TOC entry 7164 (class 1259 OID 891731)
-- Name: idx_890262_invoice_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890262_invoice_id ON vaxiom.invoice_import_export_log USING btree (invoice_id);


--
-- TOC entry 7167 (class 1259 OID 891743)
-- Name: idx_890265_network_sheet_fee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890265_network_sheet_fee_id ON vaxiom.in_network_discounts USING btree (network_sheet_fee_id);


--
-- TOC entry 7170 (class 1259 OID 891737)
-- Name: idx_890268_fk_jobtitle_jobtitle_group1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890268_fk_jobtitle_jobtitle_group1_idx ON vaxiom.jobtitle USING btree (jobtitle_group_id);


--
-- TOC entry 7171 (class 1259 OID 891734)
-- Name: idx_890268_fk_jobtitle_sys_organizations1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890268_fk_jobtitle_sys_organizations1_idx ON vaxiom.jobtitle USING btree (sys_organizations_id);


--
-- TOC entry 7172 (class 1259 OID 891758)
-- Name: idx_890268_idx_name_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890268_idx_name_unique ON vaxiom.jobtitle USING btree (name, sys_organizations_id, jobtitle_group_id);


--
-- TOC entry 7175 (class 1259 OID 891799)
-- Name: idx_890271_fk_jobtitle_group_eeo1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890271_fk_jobtitle_group_eeo1_idx ON vaxiom.jobtitle_group USING btree (eeo_id);


--
-- TOC entry 7176 (class 1259 OID 891744)
-- Name: idx_890271_idx_name_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890271_idx_name_unique ON vaxiom.jobtitle_group USING btree (name, eeo_id);


--
-- TOC entry 7181 (class 1259 OID 891735)
-- Name: idx_890277_fk_legal_entity_has_enrollment_enrollment1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890277_fk_legal_entity_has_enrollment_enrollment1_idx ON vaxiom.legal_entities_participating_enrollment USING btree (enrollment_id);


--
-- TOC entry 7182 (class 1259 OID 891754)
-- Name: idx_890277_fk_legal_entity_has_enrollment_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890277_fk_legal_entity_has_enrollment_legal_entity1_idx ON vaxiom.legal_entities_participating_enrollment USING btree (legal_entity_id);


--
-- TOC entry 7185 (class 1259 OID 891747)
-- Name: idx_890280_fk_employer_sys_organizations1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890280_fk_employer_sys_organizations1_idx ON vaxiom.legal_entity USING btree (sys_organizations_id);


--
-- TOC entry 7186 (class 1259 OID 891746)
-- Name: idx_890280_fk_legal_entity_county1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890280_fk_legal_entity_county1_idx ON vaxiom.legal_entity USING btree (county_id);


--
-- TOC entry 7187 (class 1259 OID 891767)
-- Name: idx_890280_fk_legal_entity_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890280_fk_legal_entity_legal_entity1_idx ON vaxiom.legal_entity USING btree (parent_id);


--
-- TOC entry 7190 (class 1259 OID 891808)
-- Name: idx_890290_fk_legal_entity_address_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890290_fk_legal_entity_address_legal_entity1_idx ON vaxiom.legal_entity_address USING btree (legal_entity_id);


--
-- TOC entry 7193 (class 1259 OID 891749)
-- Name: idx_890293_fk_legal_entity_address_attachment_legal_entity_addr; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890293_fk_legal_entity_address_attachment_legal_entity_addr ON vaxiom.legal_entity_address_attachment USING btree (legal_entity_address_id);


--
-- TOC entry 7196 (class 1259 OID 891745)
-- Name: idx_890299_fk_legal_entity_billing_group_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890299_fk_legal_entity_billing_group_legal_entity1_idx ON vaxiom.legal_entity_billing_group USING btree (legal_entity_id);


--
-- TOC entry 7199 (class 1259 OID 891760)
-- Name: idx_890302_fk_legal_entity_billing_group_has_sys_organizations_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890302_fk_legal_entity_billing_group_has_sys_organizations_ ON vaxiom.legal_entity_billing_group_has_sys_organizations USING btree (sys_organizations_id);


--
-- TOC entry 7202 (class 1259 OID 891777)
-- Name: idx_890305_fk_legal_entity_dba_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890305_fk_legal_entity_dba_legal_entity1_idx ON vaxiom.legal_entity_dba USING btree (legal_entity_id);


--
-- TOC entry 7205 (class 1259 OID 891818)
-- Name: idx_890308_fk_legal_entity_insurance_benefit_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890308_fk_legal_entity_insurance_benefit_legal_entity1_idx ON vaxiom.legal_entity_insurance_benefit USING btree (benefits_holder);


--
-- TOC entry 7206 (class 1259 OID 891764)
-- Name: idx_890308_fk_legal_entity_insurance_benefit_legal_entity2_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890308_fk_legal_entity_insurance_benefit_legal_entity2_idx ON vaxiom.legal_entity_insurance_benefit USING btree (legal_entity_id);


--
-- TOC entry 7207 (class 1259 OID 891759)
-- Name: idx_890308_fk_legal_entity_insurance_benefit_sys_users1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890308_fk_legal_entity_insurance_benefit_sys_users1_idx ON vaxiom.legal_entity_insurance_benefit USING btree (broker_id);


--
-- TOC entry 7210 (class 1259 OID 891757)
-- Name: idx_890311_fk_legal_entity_insurance_benefit_has_sys_organizati; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890311_fk_legal_entity_insurance_benefit_has_sys_organizati ON vaxiom.legal_entity_insurance_benefit_has_division USING btree (legal_entity_insurance_benefit_id);


--
-- TOC entry 7213 (class 1259 OID 891770)
-- Name: idx_890314_fk_legal_entity_npi_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890314_fk_legal_entity_npi_legal_entity1_idx ON vaxiom.legal_entity_npi USING btree (legal_entity_id);


--
-- TOC entry 7216 (class 1259 OID 891788)
-- Name: idx_890317_fk_legal_entity_operational_structures_legal_entity1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890317_fk_legal_entity_operational_structures_legal_entity1 ON vaxiom.legal_entity_operational_structures USING btree (legal_entity_id);


--
-- TOC entry 7219 (class 1259 OID 891828)
-- Name: idx_890320_fk_legal_entity_owner_legal_entity1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890320_fk_legal_entity_owner_legal_entity1_idx ON vaxiom.legal_entity_owner USING btree (legal_entity_id);


--
-- TOC entry 7222 (class 1259 OID 891769)
-- Name: idx_890323_legal_entity_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890323_legal_entity_id ON vaxiom.legal_entity_tin USING btree (legal_entity_id);


--
-- TOC entry 7223 (class 1259 OID 891775)
-- Name: idx_890323_legal_entity_id_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890323_legal_entity_id_idx ON vaxiom.legal_entity_tin USING btree (id);


--
-- TOC entry 7226 (class 1259 OID 891793)
-- Name: idx_890326_booking_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890326_booking_id ON vaxiom.lightbarcalls USING btree (booking_id);


--
-- TOC entry 7227 (class 1259 OID 891782)
-- Name: idx_890326_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890326_chair_id ON vaxiom.lightbarcalls USING btree (chair_id);


--
-- TOC entry 7230 (class 1259 OID 891776)
-- Name: idx_890326_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890326_resource_id ON vaxiom.lightbarcalls USING btree (resource_id);


--
-- TOC entry 7231 (class 1259 OID 891798)
-- Name: idx_890329_access_key; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890329_access_key ON vaxiom.location_access_keys USING btree (access_key);


--
-- TOC entry 7232 (class 1259 OID 891805)
-- Name: idx_890329_organization_id_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890329_organization_id_unique ON vaxiom.location_access_keys USING btree (organization_id);


--
-- TOC entry 7235 (class 1259 OID 891784)
-- Name: idx_890335_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890335_patient_id ON vaxiom.medical_history USING btree (patient_id);


--
-- TOC entry 7238 (class 1259 OID 891786)
-- Name: idx_890430_medical_history_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890430_medical_history_id ON vaxiom.medical_history_medications USING btree (medical_history_id);


--
-- TOC entry 7239 (class 1259 OID 891781)
-- Name: idx_890430_medication_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890430_medication_id ON vaxiom.medical_history_medications USING btree (medication_id);


--
-- TOC entry 7242 (class 1259 OID 891792)
-- Name: idx_890435_fk_medicalhistoryid_medicalhistory; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890435_fk_medicalhistoryid_medicalhistory ON vaxiom.medical_history_patient_relatives_crowding USING btree (medical_history_id);


--
-- TOC entry 7247 (class 1259 OID 891809)
-- Name: idx_890443_check_date_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890443_check_date_index ON vaxiom.merchant_reports USING btree (check_date);


--
-- TOC entry 7248 (class 1259 OID 891814)
-- Name: idx_890443_fk_merchant_reports_sys_organizations; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890443_fk_merchant_reports_sys_organizations ON vaxiom.merchant_reports USING btree (location_id);


--
-- TOC entry 7249 (class 1259 OID 891846)
-- Name: idx_890443_merchant_report_success; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890443_merchant_report_success ON vaxiom.merchant_reports USING btree (success);


--
-- TOC entry 7252 (class 1259 OID 891791)
-- Name: idx_890449_answerguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890449_answerguid ON vaxiom.migrated_answers USING btree (answer_guid);


--
-- TOC entry 7253 (class 1259 OID 891797)
-- Name: idx_890449_answerid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890449_answerid ON vaxiom.migrated_answers USING btree (answer_id);


--
-- TOC entry 7256 (class 1259 OID 891815)
-- Name: idx_890449_questionguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890449_questionguid ON vaxiom.migrated_answers USING btree (question_guid);


--
-- TOC entry 7257 (class 1259 OID 891803)
-- Name: idx_890452_guidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890452_guidindex ON vaxiom.migrated_appointments USING btree (appointment_guid);


--
-- TOC entry 7258 (class 1259 OID 891802)
-- Name: idx_890452_idindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890452_idindex ON vaxiom.migrated_appointments USING btree (appointment_id);


--
-- TOC entry 7259 (class 1259 OID 891796)
-- Name: idx_890455_fieldguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890455_fieldguidindex ON vaxiom.migrated_appointment_fields USING btree (fields_guid);


--
-- TOC entry 7260 (class 1259 OID 891820)
-- Name: idx_890455_fieldidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890455_fieldidindex ON vaxiom.migrated_appointment_fields USING btree (appointment_id);


--
-- TOC entry 7261 (class 1259 OID 891825)
-- Name: idx_890458_typeguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890458_typeguidindex ON vaxiom.migrated_appointment_types USING btree (appointment_type_guid);


--
-- TOC entry 7262 (class 1259 OID 891858)
-- Name: idx_890458_typeidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890458_typeidindex ON vaxiom.migrated_appointment_types USING btree (appointment_type_id);


--
-- TOC entry 7263 (class 1259 OID 891806)
-- Name: idx_890461_chairguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890461_chairguidindex ON vaxiom.migrated_chairs USING btree (chair_guid);


--
-- TOC entry 7264 (class 1259 OID 891800)
-- Name: idx_890461_chairidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890461_chairidindex ON vaxiom.migrated_chairs USING btree (chair_id);


--
-- TOC entry 7265 (class 1259 OID 891816)
-- Name: idx_890464_claimguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890464_claimguid ON vaxiom.migrated_claim USING btree (claim_guid);


--
-- TOC entry 7266 (class 1259 OID 891801)
-- Name: idx_890470_employeeguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890470_employeeguidindex ON vaxiom.migrated_employees USING btree (employee_guid);


--
-- TOC entry 7267 (class 1259 OID 891824)
-- Name: idx_890470_employeeidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890470_employeeidindex ON vaxiom.migrated_employees USING btree (employee_id);


--
-- TOC entry 7268 (class 1259 OID 891813)
-- Name: idx_890473_companyguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890473_companyguidindex ON vaxiom.migrated_insurance_companies USING btree (company_guid);


--
-- TOC entry 7269 (class 1259 OID 891811)
-- Name: idx_890473_companyidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890473_companyidindex ON vaxiom.migrated_insurance_companies USING btree (company_id);


--
-- TOC entry 7270 (class 1259 OID 891807)
-- Name: idx_890476_planguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890476_planguid ON vaxiom.migrated_insurance_plans USING btree (plan_guid);


--
-- TOC entry 7271 (class 1259 OID 891830)
-- Name: idx_890476_planid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890476_planid ON vaxiom.migrated_insurance_plans USING btree (plan_id);


--
-- TOC entry 7272 (class 1259 OID 891840)
-- Name: idx_890479_locationguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890479_locationguid ON vaxiom.migrated_locations USING btree (location_guid);


--
-- TOC entry 7273 (class 1259 OID 891867)
-- Name: idx_890479_locationid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890479_locationid ON vaxiom.migrated_locations USING btree (location_id);


--
-- TOC entry 7274 (class 1259 OID 891817)
-- Name: idx_890482_patientguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890482_patientguidindex ON vaxiom.migrated_patients USING btree (patient_guid);


--
-- TOC entry 7275 (class 1259 OID 891810)
-- Name: idx_890485_commentguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890485_commentguidindex ON vaxiom.migrated_patients_comments USING btree (comment_guid);


--
-- TOC entry 7276 (class 1259 OID 891829)
-- Name: idx_890485_commentidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890485_commentidindex ON vaxiom.migrated_patients_comments USING btree (comment_id);


--
-- TOC entry 7279 (class 1259 OID 891835)
-- Name: idx_890491_patientanswerguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890491_patientanswerguid ON vaxiom.migrated_patient_answers USING btree (patient_answer_guid);


--
-- TOC entry 7280 (class 1259 OID 891823)
-- Name: idx_890491_patientanswerid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890491_patientanswerid ON vaxiom.migrated_patient_answers USING btree (patient_answer_id);


--
-- TOC entry 7285 (class 1259 OID 891841)
-- Name: idx_890497_imageseriesguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890497_imageseriesguidindex ON vaxiom.migrated_patient_image_series USING btree (image_series_guid);


--
-- TOC entry 7286 (class 1259 OID 891850)
-- Name: idx_890497_imageseriesidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890497_imageseriesidindex ON vaxiom.migrated_patient_image_series USING btree (image_series_id);


--
-- TOC entry 7289 (class 1259 OID 891827)
-- Name: idx_890500_paymentguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890500_paymentguidindex ON vaxiom.migrated_payment_accounts USING btree (payment_guid);


--
-- TOC entry 7290 (class 1259 OID 891822)
-- Name: idx_890500_paymentidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890500_paymentidindex ON vaxiom.migrated_payment_accounts USING btree (payment_id);


--
-- TOC entry 7291 (class 1259 OID 891839)
-- Name: idx_890503_migrated_persons_person_guid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890503_migrated_persons_person_guid ON vaxiom.migrated_persons USING btree (person_guid);


--
-- TOC entry 7292 (class 1259 OID 891826)
-- Name: idx_890503_migrated_persons_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890503_migrated_persons_person_id ON vaxiom.migrated_persons USING btree (person_id);


--
-- TOC entry 7293 (class 1259 OID 891843)
-- Name: idx_890503_person_guid_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890503_person_guid_index ON vaxiom.migrated_persons USING btree (person_guid);


--
-- TOC entry 7294 (class 1259 OID 891833)
-- Name: idx_890503_person_id_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890503_person_id_index ON vaxiom.migrated_persons USING btree (person_id);


--
-- TOC entry 7295 (class 1259 OID 891832)
-- Name: idx_890506_patientguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890506_patientguid ON vaxiom.migrated_questionnaire USING btree (patient_guid);


--
-- TOC entry 7298 (class 1259 OID 891852)
-- Name: idx_890506_questionnaireguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890506_questionnaireguid ON vaxiom.migrated_questionnaire USING btree (questionnaire_guid);


--
-- TOC entry 7299 (class 1259 OID 891859)
-- Name: idx_890506_questionnaireid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890506_questionnaireid ON vaxiom.migrated_questionnaire USING btree (questionnaire_id);


--
-- TOC entry 7300 (class 1259 OID 891890)
-- Name: idx_890506_tx_card_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890506_tx_card_id ON vaxiom.migrated_questionnaire USING btree (tx_card_id);


--
-- TOC entry 7301 (class 1259 OID 891836)
-- Name: idx_890509_patient_guid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890509_patient_guid ON vaxiom.migrated_questions USING btree (patient_guid);


--
-- TOC entry 7304 (class 1259 OID 891837)
-- Name: idx_890509_question_guid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890509_question_guid ON vaxiom.migrated_questions USING btree (question_guid);


--
-- TOC entry 7305 (class 1259 OID 891854)
-- Name: idx_890509_question_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890509_question_id ON vaxiom.migrated_questions USING btree (question_id);


--
-- TOC entry 7306 (class 1259 OID 891849)
-- Name: idx_890509_questionnaire_guid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890509_questionnaire_guid ON vaxiom.migrated_questions USING btree (questionnaire_guid);


--
-- TOC entry 7309 (class 1259 OID 891844)
-- Name: idx_890512_relationguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890512_relationguidindex ON vaxiom.migrated_relations USING btree (relation_guid);


--
-- TOC entry 7312 (class 1259 OID 891863)
-- Name: idx_890515_templateguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890515_templateguid ON vaxiom.migrated_schedule_templates USING btree (template_guid);


--
-- TOC entry 7313 (class 1259 OID 891872)
-- Name: idx_890515_templateid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890515_templateid ON vaxiom.migrated_schedule_templates USING btree (template_id);


--
-- TOC entry 7314 (class 1259 OID 891899)
-- Name: idx_890521_invoiceguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890521_invoiceguidindex ON vaxiom.migrated_transactions USING btree (transaction_guid);


--
-- TOC entry 7315 (class 1259 OID 891848)
-- Name: idx_890521_invoiceidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890521_invoiceidindex ON vaxiom.migrated_transactions USING btree (transaction_id);


--
-- TOC entry 7316 (class 1259 OID 891845)
-- Name: idx_890524_nodeguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890524_nodeguid ON vaxiom.migrated_treatment_notes USING btree (note_guid);


--
-- TOC entry 7317 (class 1259 OID 891856)
-- Name: idx_890524_nodeid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890524_nodeid ON vaxiom.migrated_treatment_notes USING btree (note_id);


--
-- TOC entry 7318 (class 1259 OID 891847)
-- Name: idx_890527_txcardguidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890527_txcardguidindex ON vaxiom.migrated_txcards USING btree (txcard_guid);


--
-- TOC entry 7319 (class 1259 OID 891864)
-- Name: idx_890527_txcardidindex; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890527_txcardidindex ON vaxiom.migrated_txcards USING btree (txcard_id);


--
-- TOC entry 7322 (class 1259 OID 891861)
-- Name: idx_890530_workscheduleguid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890530_workscheduleguid ON vaxiom.migrated_workschedule USING btree (workschedule_guid);


--
-- TOC entry 7323 (class 1259 OID 891853)
-- Name: idx_890530_workscheduleid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890530_workscheduleid ON vaxiom.migrated_workschedule USING btree (workschedule_id);


--
-- TOC entry 7326 (class 1259 OID 891885)
-- Name: idx_890541_loose_search_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890541_loose_search_index ON vaxiom.migration_map USING btree (source_key, source_name, target_table);


--
-- TOC entry 7327 (class 1259 OID 891910)
-- Name: idx_890541_merged_to_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890541_merged_to_id ON vaxiom.migration_map USING btree (merged_to_id);


--
-- TOC entry 7328 (class 1259 OID 891860)
-- Name: idx_890541_migration_map_search_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890541_migration_map_search_index ON vaxiom.migration_map USING btree (source_key, source_type, source_name, target_table);


--
-- TOC entry 7331 (class 1259 OID 891868)
-- Name: idx_890541_source_key; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890541_source_key ON vaxiom.migration_map USING btree (source_key, source_name, target_id, target_table);


--
-- TOC entry 7332 (class 1259 OID 891855)
-- Name: idx_890541_target_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890541_target_id ON vaxiom.migration_map USING btree (target_id);


--
-- TOC entry 7335 (class 1259 OID 891871)
-- Name: idx_890551_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890551_organization_id ON vaxiom.misc_fee_charge_templates USING btree (organization_id);


--
-- TOC entry 7342 (class 1259 OID 891897)
-- Name: idx_890558_report_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890558_report_id ON vaxiom.my_reports_columns USING btree (report_id);


--
-- TOC entry 7345 (class 1259 OID 891874)
-- Name: idx_890561_report_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890561_report_id ON vaxiom.my_reports_filters USING btree (report_id);


--
-- TOC entry 7346 (class 1259 OID 891869)
-- Name: idx_890567_employment_contract_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890567_employment_contract_id ON vaxiom.network_fee_sheets USING btree (employment_contract_id);


--
-- TOC entry 7347 (class 1259 OID 891878)
-- Name: idx_890567_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890567_organization_id ON vaxiom.network_fee_sheets USING btree (organization_id, employment_contract_id);


--
-- TOC entry 7350 (class 1259 OID 891886)
-- Name: idx_890570_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890570_insurance_code_id ON vaxiom.network_sheet_fee USING btree (insurance_code_id);


--
-- TOC entry 7351 (class 1259 OID 891882)
-- Name: idx_890570_network_fee_sheet_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890570_network_fee_sheet_id ON vaxiom.network_sheet_fee USING btree (network_fee_sheet_id);


--
-- TOC entry 7356 (class 1259 OID 891894)
-- Name: idx_890576_notebook_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890576_notebook_id ON vaxiom.notebook_notes USING btree (notebook_id);


--
-- TOC entry 7361 (class 1259 OID 891884)
-- Name: idx_890582_target_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890582_target_id ON vaxiom.notes USING btree (target_id);


--
-- TOC entry 7362 (class 1259 OID 891880)
-- Name: idx_890588_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890588_chair_id ON vaxiom.odt_chairs USING btree (chair_id);


--
-- TOC entry 7363 (class 1259 OID 891889)
-- Name: idx_890588_day_schedule_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890588_day_schedule_id ON vaxiom.odt_chairs USING btree (day_schedule_id);


--
-- TOC entry 7366 (class 1259 OID 891896)
-- Name: idx_890588_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890588_template_id ON vaxiom.odt_chairs USING btree (template_id, chair_id, day_schedule_id, valid_from);


--
-- TOC entry 7367 (class 1259 OID 891893)
-- Name: idx_890592_odt_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890592_odt_chair_id ON vaxiom.odt_chair_allocations USING btree (odt_chair_id, resource_id);


--
-- TOC entry 7370 (class 1259 OID 891888)
-- Name: idx_890592_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890592_resource_id ON vaxiom.odt_chair_allocations USING btree (resource_id);


--
-- TOC entry 7375 (class 1259 OID 891941)
-- Name: idx_890607_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890607_location_id ON vaxiom.office_days USING btree (location_id, o_date);


--
-- TOC entry 7378 (class 1259 OID 891891)
-- Name: idx_890607_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890607_template_id ON vaxiom.office_days USING btree (template_id);


--
-- TOC entry 7379 (class 1259 OID 891898)
-- Name: idx_890614_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890614_chair_id ON vaxiom.office_day_chairs USING btree (chair_id);


--
-- TOC entry 7380 (class 1259 OID 891887)
-- Name: idx_890614_odt_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890614_odt_chair_id ON vaxiom.office_day_chairs USING btree (odt_chair_id);


--
-- TOC entry 7381 (class 1259 OID 891907)
-- Name: idx_890614_office_day_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890614_office_day_id ON vaxiom.office_day_chairs USING btree (office_day_id, chair_id);


--
-- TOC entry 7384 (class 1259 OID 891903)
-- Name: idx_890617_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890617_location_id ON vaxiom.office_day_templates USING btree (location_id);


--
-- TOC entry 7391 (class 1259 OID 891952)
-- Name: idx_890634_employee_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890634_employee_type_id ON vaxiom.other_professional_relationships USING btree (employee_type_id);


--
-- TOC entry 7392 (class 1259 OID 891906)
-- Name: idx_890634_external_office_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890634_external_office_id ON vaxiom.other_professional_relationships USING btree (external_office_id);


--
-- TOC entry 7393 (class 1259 OID 891902)
-- Name: idx_890634_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890634_person_id ON vaxiom.other_professional_relationships USING btree (person_id);


--
-- TOC entry 7396 (class 1259 OID 891901)
-- Name: idx_890637_heard_from_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890637_heard_from_patient_id ON vaxiom.patients USING btree (heard_from_patient_id);


--
-- TOC entry 7397 (class 1259 OID 891918)
-- Name: idx_890637_heard_from_professional_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890637_heard_from_professional_id ON vaxiom.patients USING btree (heard_from_professional_id);


--
-- TOC entry 7398 (class 1259 OID 891916)
-- Name: idx_890637_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890637_organization_id ON vaxiom.patients USING btree (organization_id, human_id);


--
-- TOC entry 7399 (class 1259 OID 891912)
-- Name: idx_890637_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890637_person_id ON vaxiom.patients USING btree (person_id);


--
-- TOC entry 7402 (class 1259 OID 891925)
-- Name: idx_890644_imageseries_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890644_imageseries_id ON vaxiom.patient_images USING btree (imageseries_id);


--
-- TOC entry 7403 (class 1259 OID 891942)
-- Name: idx_890644_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890644_patient_id ON vaxiom.patient_images USING btree (patient_id);


--
-- TOC entry 7406 (class 1259 OID 891919)
-- Name: idx_890644_slot_key; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890644_slot_key ON vaxiom.patient_images USING btree (slot_key);


--
-- TOC entry 7407 (class 1259 OID 891915)
-- Name: idx_890650_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890650_appointment_id ON vaxiom.patient_imageseries USING btree (appointment_id);


--
-- TOC entry 7408 (class 1259 OID 891917)
-- Name: idx_890650_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890650_patient_id ON vaxiom.patient_imageseries USING btree (patient_id);


--
-- TOC entry 7411 (class 1259 OID 891932)
-- Name: idx_890650_treatment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890650_treatment_id ON vaxiom.patient_imageseries USING btree (treatment_id);


--
-- TOC entry 7412 (class 1259 OID 891928)
-- Name: idx_890654_organization_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890654_organization_node_id ON vaxiom.patient_images_layouts USING btree (organization_node_id);


--
-- TOC entry 7417 (class 1259 OID 891939)
-- Name: idx_890666_insurance_billing_center_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890666_insurance_billing_center_id ON vaxiom.patient_insurance_plans USING btree (insurance_billing_center_id);


--
-- TOC entry 7418 (class 1259 OID 891962)
-- Name: idx_890666_insurance_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890666_insurance_company_id ON vaxiom.patient_insurance_plans USING btree (insurance_company_id);


--
-- TOC entry 7419 (class 1259 OID 891971)
-- Name: idx_890666_master_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890666_master_id ON vaxiom.patient_insurance_plans USING btree (master_id);


--
-- TOC entry 7422 (class 1259 OID 891926)
-- Name: idx_890672_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890672_patient_id ON vaxiom.patient_ledger_history USING btree (patient_id);


--
-- TOC entry 7425 (class 1259 OID 891924)
-- Name: idx_890672_tx_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890672_tx_location_id ON vaxiom.patient_ledger_history USING btree (tx_location_id);


--
-- TOC entry 7426 (class 1259 OID 891943)
-- Name: idx_890678_patient_ledger_history_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890678_patient_ledger_history_id ON vaxiom.patient_ledger_history_overdue_receivables USING btree (patient_ledger_history_id);


--
-- TOC entry 7429 (class 1259 OID 891934)
-- Name: idx_890681_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890681_location_id ON vaxiom.patient_locations USING btree (location_id);


--
-- TOC entry 7432 (class 1259 OID 891951)
-- Name: idx_890684_patient_person_referrals_ibfk_1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890684_patient_person_referrals_ibfk_1 ON vaxiom.patient_person_referrals USING btree (patient_id);


--
-- TOC entry 7433 (class 1259 OID 891978)
-- Name: idx_890684_patient_person_referrals_ibfk_2; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890684_patient_person_referrals_ibfk_2 ON vaxiom.patient_person_referrals USING btree (person_id);


--
-- TOC entry 7436 (class 1259 OID 891940)
-- Name: idx_890687_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890687_patient_id ON vaxiom.patient_recently_opened_items USING btree (patient_id);


--
-- TOC entry 7439 (class 1259 OID 891936)
-- Name: idx_890690_patient_template_referrals_ibfk_1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890690_patient_template_referrals_ibfk_1 ON vaxiom.patient_template_referrals USING btree (patient_id);


--
-- TOC entry 7440 (class 1259 OID 891935)
-- Name: idx_890690_patient_template_referrals_ibfk_3; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890690_patient_template_referrals_ibfk_3 ON vaxiom.patient_template_referrals USING btree (referral_template_id);


--
-- TOC entry 7443 (class 1259 OID 891949)
-- Name: idx_890696_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_account_id ON vaxiom.payments USING btree (account_id);


--
-- TOC entry 7444 (class 1259 OID 891945)
-- Name: idx_890696_applied_payment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_applied_payment_id ON vaxiom.payments USING btree (applied_payment_id);


--
-- TOC entry 7445 (class 1259 OID 891944)
-- Name: idx_890696_billing_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_billing_address_id ON vaxiom.payments USING btree (billing_address_id);


--
-- TOC entry 7446 (class 1259 OID 891963)
-- Name: idx_890696_fk_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_fk_patient_id ON vaxiom.payments USING btree (patient_id);


--
-- TOC entry 7447 (class 1259 OID 891993)
-- Name: idx_890696_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_organization_id ON vaxiom.payments USING btree (organization_id);


--
-- TOC entry 7448 (class 1259 OID 891950)
-- Name: idx_890696_payment_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_payment_type_id ON vaxiom.payments USING btree (payment_type_id);


--
-- TOC entry 7449 (class 1259 OID 891990)
-- Name: idx_890696_payments_ibfk_8; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_payments_ibfk_8 ON vaxiom.payments USING btree (insurance_company_payment_id);


--
-- TOC entry 7452 (class 1259 OID 891947)
-- Name: idx_890696_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_receivable_id ON vaxiom.payments USING btree (receivable_id);


--
-- TOC entry 7453 (class 1259 OID 891946)
-- Name: idx_890696_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890696_sys_created_at ON vaxiom.payments USING btree (sys_created_at);


--
-- TOC entry 7454 (class 1259 OID 891964)
-- Name: idx_890703_account_type; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890703_account_type ON vaxiom.payment_accounts USING btree (account_type);


--
-- TOC entry 7457 (class 1259 OID 891960)
-- Name: idx_890707_correction_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890707_correction_type_id ON vaxiom.payment_correction_details USING btree (correction_type_id);


--
-- TOC entry 7460 (class 1259 OID 891973)
-- Name: idx_890707_transaction_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890707_transaction_id ON vaxiom.payment_correction_details USING btree (transaction_id);


--
-- TOC entry 7463 (class 1259 OID 892000)
-- Name: idx_890716_bank_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890716_bank_account_id ON vaxiom.payment_plans USING btree (bank_account_id);


--
-- TOC entry 7464 (class 1259 OID 891958)
-- Name: idx_890716_invoice_contact_method_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890716_invoice_contact_method_fk ON vaxiom.payment_plans USING btree (invoice_contact_method_id);


--
-- TOC entry 7467 (class 1259 OID 891955)
-- Name: idx_890716_token_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890716_token_location_id ON vaxiom.payment_plans USING btree (token_location_id);


--
-- TOC entry 7468 (class 1259 OID 891956)
-- Name: idx_890724_payment_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890724_payment_plan_id ON vaxiom.payment_plan_rollbacks USING btree (payment_plan_id);


--
-- TOC entry 7471 (class 1259 OID 891970)
-- Name: idx_890727_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890727_name ON vaxiom.payment_types USING btree (name, parent_company_id);


--
-- TOC entry 7472 (class 1259 OID 891972)
-- Name: idx_890727_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890727_parent_company_id ON vaxiom.payment_types USING btree (parent_company_id);


--
-- TOC entry 7475 (class 1259 OID 891986)
-- Name: idx_890731_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890731_organization_id ON vaxiom.payment_types_locations USING btree (organization_id);


--
-- TOC entry 7478 (class 1259 OID 892009)
-- Name: idx_890734_permissions_ibfk_1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890734_permissions_ibfk_1_idx ON vaxiom.permissions_permission USING btree (permission_group_id);


--
-- TOC entry 7483 (class 1259 OID 891969)
-- Name: idx_890740_department_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890740_department_fk ON vaxiom.permissions_user_role_departament USING btree (department_id);


--
-- TOC entry 7486 (class 1259 OID 891984)
-- Name: idx_890743_jobtitle_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890743_jobtitle_fk ON vaxiom.permissions_user_role_job_title USING btree (job_title_id);


--
-- TOC entry 7489 (class 1259 OID 891981)
-- Name: idx_890746_permission_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890746_permission_fk ON vaxiom.permissions_user_role_permission USING btree (permissions_permission_id);


--
-- TOC entry 7492 (class 1259 OID 891999)
-- Name: idx_890746_user_role_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890746_user_role_fk ON vaxiom.permissions_user_role_permission USING btree (permissions_user_role_id);


--
-- TOC entry 7493 (class 1259 OID 892025)
-- Name: idx_890749_fk_permission_user_role_location_permissions_user_ro; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890749_fk_permission_user_role_location_permissions_user_ro ON vaxiom.permission_user_role_location USING btree (permission_user_role_id);


--
-- TOC entry 7494 (class 1259 OID 892018)
-- Name: idx_890749_fk_permission_user_role_location_sys_organizations; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890749_fk_permission_user_role_location_sys_organizations ON vaxiom.permission_user_role_location USING btree (location_id);


--
-- TOC entry 7497 (class 1259 OID 891975)
-- Name: idx_890752_core_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890752_core_user_id ON vaxiom.persons USING btree (core_user_id);


--
-- TOC entry 7498 (class 1259 OID 891979)
-- Name: idx_890752_fullname_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_fullname_id ON vaxiom.persons USING btree (first_name, last_name);


--
-- TOC entry 7499 (class 1259 OID 891982)
-- Name: idx_890752_global_person_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_global_person_id_fk ON vaxiom.persons USING btree (global_person_id);


--
-- TOC entry 7500 (class 1259 OID 891995)
-- Name: idx_890752_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_organization_id_fk ON vaxiom.persons USING btree (organization_id);


--
-- TOC entry 7501 (class 1259 OID 891994)
-- Name: idx_890752_patient_portal_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_patient_portal_user_id ON vaxiom.persons USING btree (patient_portal_user_id);


--
-- TOC entry 7502 (class 1259 OID 891992)
-- Name: idx_890752_portal_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890752_portal_user_id ON vaxiom.persons USING btree (portal_user_id);


--
-- TOC entry 7505 (class 1259 OID 892010)
-- Name: idx_890752_search_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890752_search_name ON vaxiom.persons USING gin (to_tsvector('simple'::regconfig, (search_name)::text));


--
-- TOC entry 7506 (class 1259 OID 892035)
-- Name: idx_890759_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890759_person_id ON vaxiom.person_image_files USING btree (person_id);


--
-- TOC entry 7509 (class 1259 OID 891987)
-- Name: idx_890765_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890765_organization_id ON vaxiom.person_payment_accounts USING btree (organization_id, payer_person_id);


--
-- TOC entry 7510 (class 1259 OID 891985)
-- Name: idx_890765_payer_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890765_payer_person_id ON vaxiom.person_payment_accounts USING btree (payer_person_id);


--
-- TOC entry 7513 (class 1259 OID 891991)
-- Name: idx_890768_previous_enrollment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890768_previous_enrollment_id ON vaxiom.previous_enrollment_dependents USING btree (previous_enrollment_id);


--
-- TOC entry 7514 (class 1259 OID 892008)
-- Name: idx_890776_employee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890776_employee_id ON vaxiom.previous_enrollment_form USING btree (employee_id);


--
-- TOC entry 7517 (class 1259 OID 892005)
-- Name: idx_890782_enrollment_form_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890782_enrollment_form_id ON vaxiom.previous_enrollment_form_benefits USING btree (enrollment_form_id);


--
-- TOC entry 7520 (class 1259 OID 892019)
-- Name: idx_890789_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890789_insurance_code_id ON vaxiom.procedures USING btree (insurance_code_id);


--
-- TOC entry 7521 (class 1259 OID 892046)
-- Name: idx_890789_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890789_organization_id_fk ON vaxiom.procedures USING btree (organization_id);


--
-- TOC entry 7526 (class 1259 OID 892002)
-- Name: idx_890795_resource_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890795_resource_type_id ON vaxiom.procedure_additional_resource_requirements USING btree (resource_type_id);


--
-- TOC entry 7529 (class 1259 OID 892001)
-- Name: idx_890798_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890798_procedure_id ON vaxiom.procedure_steps USING btree (procedure_id);


--
-- TOC entry 7532 (class 1259 OID 892015)
-- Name: idx_890802_resource_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890802_resource_type_id ON vaxiom.procedure_step_additional_resource_requirements USING btree (resource_type_id);


--
-- TOC entry 7535 (class 1259 OID 892011)
-- Name: idx_890805_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890805_resource_id ON vaxiom.procedure_step_durations USING btree (resource_id);


--
-- TOC entry 7536 (class 1259 OID 892033)
-- Name: idx_890805_step_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890805_step_id ON vaxiom.procedure_step_durations USING btree (step_id);


--
-- TOC entry 7537 (class 1259 OID 892056)
-- Name: idx_890809_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890809_patient_id ON vaxiom.professional_relationships USING btree (patient_id);


--
-- TOC entry 7540 (class 1259 OID 892007)
-- Name: idx_890813_fk_provider_license_employment_contracts1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890813_fk_provider_license_employment_contracts1_idx ON vaxiom.provider_license USING btree (employment_contracts_id);


--
-- TOC entry 7541 (class 1259 OID 892013)
-- Name: idx_890813_fk_provider_license_provider_specialty1_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890813_fk_provider_license_provider_specialty1_idx ON vaxiom.provider_license USING btree (provider_specialty_id);


--
-- TOC entry 7544 (class 1259 OID 892012)
-- Name: idx_890816_network_fee_sheet_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890816_network_fee_sheet_id ON vaxiom.provider_network_insurance_companies USING btree (network_fee_sheet_id);


--
-- TOC entry 7551 (class 1259 OID 892021)
-- Name: idx_890822_questionnaireid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890822_questionnaireid ON vaxiom.question USING btree (questionnaire_id);


--
-- TOC entry 7552 (class 1259 OID 892044)
-- Name: idx_890825_patientid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890825_patientid ON vaxiom.questionnaire USING btree (patient_id);


--
-- TOC entry 7555 (class 1259 OID 892062)
-- Name: idx_890825_txcardid; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890825_txcardid ON vaxiom.questionnaire USING btree (tx_card_id);


--
-- TOC entry 7556 (class 1259 OID 892017)
-- Name: idx_890828_answer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890828_answer_id ON vaxiom.questionnaire_answers USING btree (answer_id);


--
-- TOC entry 7559 (class 1259 OID 892020)
-- Name: idx_890828_question_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890828_question_id ON vaxiom.questionnaire_answers USING btree (question_id);


--
-- TOC entry 7560 (class 1259 OID 892022)
-- Name: idx_890834_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890834_patient_id ON vaxiom.reachify_users USING btree (patient_id);


--
-- TOC entry 7563 (class 1259 OID 892034)
-- Name: idx_890840_insured_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_insured_id ON vaxiom.receivables USING btree (insured_id);


--
-- TOC entry 7564 (class 1259 OID 892036)
-- Name: idx_890840_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_organization_id ON vaxiom.receivables USING btree (organization_id);


--
-- TOC entry 7565 (class 1259 OID 892031)
-- Name: idx_890840_parent_company_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890840_parent_company_id ON vaxiom.receivables USING btree (parent_company_id, human_id);


--
-- TOC entry 7566 (class 1259 OID 892057)
-- Name: idx_890840_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_patient_id ON vaxiom.receivables USING btree (patient_id);


--
-- TOC entry 7567 (class 1259 OID 892079)
-- Name: idx_890840_payment_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_payment_account_id ON vaxiom.receivables USING btree (payment_account_id);


--
-- TOC entry 7570 (class 1259 OID 892029)
-- Name: idx_890840_primary_prsn_payer_inv_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_primary_prsn_payer_inv_id ON vaxiom.receivables USING btree (primary_prsn_payer_inv_id);


--
-- TOC entry 7571 (class 1259 OID 892032)
-- Name: idx_890840_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_sys_created_at ON vaxiom.receivables USING btree (sys_created_at);


--
-- TOC entry 7572 (class 1259 OID 892045)
-- Name: idx_890840_treatment_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890840_treatment_index ON vaxiom.receivables USING btree (treatment_id);


--
-- TOC entry 7579 (class 1259 OID 892047)
-- Name: idx_890852_referral_templates_ibfk_1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890852_referral_templates_ibfk_1 ON vaxiom.referral_templates USING btree (organization_id);


--
-- TOC entry 7580 (class 1259 OID 892041)
-- Name: idx_890852_referral_templates_ibfk_2; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890852_referral_templates_ibfk_2 ON vaxiom.referral_templates USING btree (location_id);


--
-- TOC entry 7585 (class 1259 OID 892091)
-- Name: idx_890862_transaction_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890862_transaction_id ON vaxiom.refund_details USING btree (transaction_id);


--
-- TOC entry 7586 (class 1259 OID 892039)
-- Name: idx_890868_from_person; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890868_from_person ON vaxiom.relationships USING btree (from_person, to_person, type);


--
-- TOC entry 7589 (class 1259 OID 892055)
-- Name: idx_890868_relationships_from_to_person_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890868_relationships_from_to_person_idx ON vaxiom.relationships USING btree (from_person, to_person);


--
-- TOC entry 7590 (class 1259 OID 892040)
-- Name: idx_890868_role; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890868_role ON vaxiom.relationships USING btree (role);


--
-- TOC entry 7591 (class 1259 OID 892069)
-- Name: idx_890868_to_person; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890868_to_person ON vaxiom.relationships USING btree (to_person);


--
-- TOC entry 7592 (class 1259 OID 892053)
-- Name: idx_890868_uc_relationship; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890868_uc_relationship ON vaxiom.relationships USING btree (from_person, to_person);


--
-- TOC entry 7595 (class 1259 OID 892052)
-- Name: idx_890877_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890877_organization_id_fk ON vaxiom.resources USING btree (organization_id);


--
-- TOC entry 7598 (class 1259 OID 892108)
-- Name: idx_890877_resource_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890877_resource_type_id ON vaxiom.resources USING btree (resource_type_id);


--
-- TOC entry 7599 (class 1259 OID 892100)
-- Name: idx_890877_resources_ifbk_3; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890877_resources_ifbk_3 ON vaxiom.resources USING btree (permissions_user_role_id);


--
-- TOC entry 7600 (class 1259 OID 892051)
-- Name: idx_890881_abbreviation; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890881_abbreviation ON vaxiom.resource_types USING btree (abbreviation, dtype);


--
-- TOC entry 7601 (class 1259 OID 892054)
-- Name: idx_890881_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890881_name ON vaxiom.resource_types USING btree (name, dtype);


--
-- TOC entry 7606 (class 1259 OID 892077)
-- Name: idx_890890_retail_item_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890890_retail_item_id ON vaxiom.retail_fees USING btree (retail_item_id);


--
-- TOC entry 7607 (class 1259 OID 892063)
-- Name: idx_890893_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890893_organization_id ON vaxiom.retail_items USING btree (organization_id);


--
-- TOC entry 7614 (class 1259 OID 892110)
-- Name: idx_890903_scheduled_task_ibfk_1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890903_scheduled_task_ibfk_1 ON vaxiom.scheduled_task USING btree (parent_company_id);


--
-- TOC entry 7615 (class 1259 OID 892121)
-- Name: idx_890907_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890907_location_id ON vaxiom.schedule_conflict_permission_bypass USING btree (location_id);


--
-- TOC entry 7618 (class 1259 OID 892064)
-- Name: idx_890910_chair_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890910_chair_id ON vaxiom.schedule_notes USING btree (chair_id);


--
-- TOC entry 7619 (class 1259 OID 892075)
-- Name: idx_890910_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890910_organization_id_fk ON vaxiom.schedule_notes USING btree (organization_id);


--
-- TOC entry 7622 (class 1259 OID 892087)
-- Name: idx_890915_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890915_patient_id ON vaxiom.scheduling_preferences USING btree (patient_id);


--
-- TOC entry 7625 (class 1259 OID 892081)
-- Name: idx_890918_scheduling_preferences_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890918_scheduling_preferences_id ON vaxiom.scheduling_preferences_week_days USING btree (scheduling_preferences_id);


--
-- TOC entry 7626 (class 1259 OID 892072)
-- Name: idx_890921_employee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890921_employee_id ON vaxiom.scheduling_preferred_employees USING btree (employee_id);


--
-- TOC entry 7631 (class 1259 OID 892130)
-- Name: idx_890924_schema_version_ir_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890924_schema_version_ir_idx ON vaxiom.schema_version USING btree (installed_rank);


--
-- TOC entry 7632 (class 1259 OID 892086)
-- Name: idx_890924_schema_version_s_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890924_schema_version_s_idx ON vaxiom.schema_version USING btree (success);


--
-- TOC entry 7633 (class 1259 OID 892074)
-- Name: idx_890924_schema_version_vr_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890924_schema_version_vr_idx ON vaxiom.schema_version USING btree (version_rank);


--
-- TOC entry 7634 (class 1259 OID 892085)
-- Name: idx_890931_appointment_template_id_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890931_appointment_template_id_idx ON vaxiom.selected_appointment_templates USING btree (appointment_template_id);


--
-- TOC entry 7635 (class 1259 OID 892071)
-- Name: idx_890931_location_id_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890931_location_id_idx ON vaxiom.selected_appointment_templates USING btree (location_id);


--
-- TOC entry 7636 (class 1259 OID 892097)
-- Name: idx_890931_location_id_unique; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890931_location_id_unique ON vaxiom.selected_appointment_templates USING btree (user_id, location_id, appointment_template_id);


--
-- TOC entry 7639 (class 1259 OID 892092)
-- Name: idx_890934_fk_appointment_template; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890934_fk_appointment_template ON vaxiom.selected_appointment_templates_office_day_calendar_view USING btree (appointment_template_id);


--
-- TOC entry 7640 (class 1259 OID 892082)
-- Name: idx_890934_fk_location_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890934_fk_location_idx ON vaxiom.selected_appointment_templates_office_day_calendar_view USING btree (location_id);


--
-- TOC entry 7641 (class 1259 OID 892111)
-- Name: idx_890934_fk_user_idx; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890934_fk_user_idx ON vaxiom.selected_appointment_templates_office_day_calendar_view USING btree (user_id);


--
-- TOC entry 7644 (class 1259 OID 892139)
-- Name: idx_890937_location_access_key_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890937_location_access_key_id ON vaxiom.selfcheckin_settings USING btree (location_access_key_id);


--
-- TOC entry 7645 (class 1259 OID 892098)
-- Name: idx_890940_appointment_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890940_appointment_template_id ON vaxiom.selfcheckin_settings_forbidden_types USING btree (appointment_template_id);


--
-- TOC entry 7646 (class 1259 OID 892084)
-- Name: idx_890940_location_access_key_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890940_location_access_key_id ON vaxiom.selfcheckin_settings_forbidden_types USING btree (location_access_key_id, appointment_template_id);


--
-- TOC entry 7649 (class 1259 OID 892080)
-- Name: idx_890943_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890943_resource_id ON vaxiom.simultaneous_appointment_values USING btree (resource_id, type_id);


--
-- TOC entry 7650 (class 1259 OID 892106)
-- Name: idx_890943_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890943_type_id ON vaxiom.simultaneous_appointment_values USING btree (type_id);


--
-- TOC entry 7655 (class 1259 OID 892093)
-- Name: idx_890952_provider_message_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890952_provider_message_id ON vaxiom.sms_part USING btree (provider_message_id);


--
-- TOC entry 7656 (class 1259 OID 892124)
-- Name: idx_890952_sms_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890952_sms_id ON vaxiom.sms_part USING btree (sms_id);


--
-- TOC entry 7657 (class 1259 OID 892141)
-- Name: idx_890958_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890958_organization_id ON vaxiom.statements USING btree (organization_id);


--
-- TOC entry 7658 (class 1259 OID 892149)
-- Name: idx_890958_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890958_patient_id ON vaxiom.statements USING btree (patient_id);


--
-- TOC entry 7665 (class 1259 OID 892090)
-- Name: idx_890976_name; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_890976_name ON vaxiom.sys_organizations USING btree (name, parent_id);


--
-- TOC entry 7666 (class 1259 OID 892120)
-- Name: idx_890976_parent_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890976_parent_id ON vaxiom.sys_organizations USING btree (parent_id);


--
-- TOC entry 7667 (class 1259 OID 892103)
-- Name: idx_890976_postal_address_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890976_postal_address_id ON vaxiom.sys_organizations USING btree (postal_address_id);


--
-- TOC entry 7670 (class 1259 OID 892105)
-- Name: idx_890983_fk_sys_organisation_address_contact_postal_addresses; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890983_fk_sys_organisation_address_contact_postal_addresses ON vaxiom.sys_organization_address USING btree (contact_postal_addresses_id);


--
-- TOC entry 7673 (class 1259 OID 892152)
-- Name: idx_890986_fk_sys_organisation_address_has_sys_organizations_sy; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890986_fk_sys_organisation_address_has_sys_organizations_sy ON vaxiom.sys_organization_address_has_sys_organizations USING btree (sys_organizations_id);


--
-- TOC entry 7676 (class 1259 OID 892104)
-- Name: idx_890989_fk_sys_organization_attachment_sys_organizations1_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890989_fk_sys_organization_attachment_sys_organizations1_id ON vaxiom.sys_organization_attachment USING btree (sys_organizations_id);


--
-- TOC entry 7679 (class 1259 OID 892102)
-- Name: idx_890995_fk_sys_organisation_contact_method_contact_methods1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890995_fk_sys_organisation_contact_method_contact_methods1_ ON vaxiom.sys_organization_contact_method USING btree (contact_methods_id);


--
-- TOC entry 7680 (class 1259 OID 892129)
-- Name: idx_890995_fk_sys_organisation_contact_method_sys_organizations; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890995_fk_sys_organisation_contact_method_sys_organizations ON vaxiom.sys_organization_contact_method USING btree (sys_organizations_id);


--
-- TOC entry 7683 (class 1259 OID 892122)
-- Name: idx_890999_fk_sys_organisation_departament_data_sys_organizatio; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_890999_fk_sys_organisation_departament_data_sys_organizatio ON vaxiom.sys_organization_department_data USING btree (sys_organizations_id);


--
-- TOC entry 7686 (class 1259 OID 892143)
-- Name: idx_891002_fk_sys_organisation_devision_data_sys_organizations1; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891002_fk_sys_organisation_devision_data_sys_organizations1 ON vaxiom.sys_organization_division_data USING btree (sys_organizations_id);


--
-- TOC entry 7687 (class 1259 OID 892162)
-- Name: idx_891002_fk_sys_organization_division_data_legal_entity_npi1_; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891002_fk_sys_organization_division_data_legal_entity_npi1_ ON vaxiom.sys_organization_division_data USING btree (legal_entity_npi_id);


--
-- TOC entry 7690 (class 1259 OID 892113)
-- Name: idx_891005_fk_sys_organizations_has_sys_organizations_sys_organ; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891005_fk_sys_organizations_has_sys_organizations_sys_organ ON vaxiom.sys_organization_division_has_department USING btree (division_tree_node_id);


--
-- TOC entry 7693 (class 1259 OID 892109)
-- Name: idx_891005_uk_sys_organization_division_has_department; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891005_uk_sys_organization_division_has_department ON vaxiom.sys_organization_division_has_department USING btree (division_tree_node_id, department_id);


--
-- TOC entry 7694 (class 1259 OID 892138)
-- Name: idx_891008_fk_sys_organization_location_group_sys_organizations; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891008_fk_sys_organization_location_group_sys_organizations ON vaxiom.sys_organization_location_group USING btree (sys_organizations_id);


--
-- TOC entry 7697 (class 1259 OID 892127)
-- Name: idx_891011_fk_sys_organization_location_group_has_sys_organizat; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891011_fk_sys_organization_location_group_has_sys_organizat ON vaxiom.sys_organization_location_group_has_sys_organizations USING btree (sys_organization_location_group_id);


--
-- TOC entry 7700 (class 1259 OID 892173)
-- Name: idx_891014_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891014_organization_id ON vaxiom.sys_organization_settings USING btree (organization_id);


--
-- TOC entry 7703 (class 1259 OID 892137)
-- Name: idx_891020_login_email; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891020_login_email ON vaxiom.sys_patient_portal_users USING btree (login_email);


--
-- TOC entry 7706 (class 1259 OID 892146)
-- Name: idx_891020_token; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891020_token ON vaxiom.sys_patient_portal_users USING btree (token);


--
-- TOC entry 7707 (class 1259 OID 892118)
-- Name: idx_891032_login_email; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891032_login_email ON vaxiom.sys_portal_users USING btree (login_email);


--
-- TOC entry 7710 (class 1259 OID 892134)
-- Name: idx_891032_token; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891032_token ON vaxiom.sys_portal_users USING btree (token);


--
-- TOC entry 7711 (class 1259 OID 892142)
-- Name: idx_891042_login_email; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891042_login_email ON vaxiom.sys_users USING btree (login_email);


--
-- TOC entry 7714 (class 1259 OID 892163)
-- Name: idx_891058_assignee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_assignee_id ON vaxiom.tasks USING btree (assignee_id);


--
-- TOC entry 7715 (class 1259 OID 892184)
-- Name: idx_891058_document_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_document_template_id ON vaxiom.tasks USING btree (document_template_id);


--
-- TOC entry 7716 (class 1259 OID 892189)
-- Name: idx_891058_document_tree_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_document_tree_node_id ON vaxiom.tasks USING btree (document_tree_node_id);


--
-- TOC entry 7717 (class 1259 OID 892148)
-- Name: idx_891058_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_organization_id ON vaxiom.tasks USING btree (organization_id);


--
-- TOC entry 7718 (class 1259 OID 892135)
-- Name: idx_891058_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_patient_id ON vaxiom.tasks USING btree (patient_id);


--
-- TOC entry 7721 (class 1259 OID 892128)
-- Name: idx_891058_provider_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_provider_id ON vaxiom.tasks USING btree (provider_id);


--
-- TOC entry 7722 (class 1259 OID 892159)
-- Name: idx_891058_recipient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_recipient_id ON vaxiom.tasks USING btree (recipient_id);


--
-- TOC entry 7723 (class 1259 OID 892144)
-- Name: idx_891058_task_basket_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891058_task_basket_id ON vaxiom.tasks USING btree (task_basket_id);


--
-- TOC entry 7724 (class 1259 OID 892151)
-- Name: idx_891065_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891065_organization_id ON vaxiom.task_baskets USING btree (organization_id);


--
-- TOC entry 7729 (class 1259 OID 892194)
-- Name: idx_891071_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891071_user_id ON vaxiom.task_basket_subscribers USING btree (user_id);


--
-- TOC entry 7730 (class 1259 OID 892202)
-- Name: idx_891074_contact_method_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891074_contact_method_id ON vaxiom.task_contacts USING btree (contact_method_id);


--
-- TOC entry 7733 (class 1259 OID 892145)
-- Name: idx_891077_assignee_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891077_assignee_id ON vaxiom.task_events USING btree (assignee_id);


--
-- TOC entry 7736 (class 1259 OID 892140)
-- Name: idx_891077_task_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891077_task_id ON vaxiom.task_events USING btree (task_id);


--
-- TOC entry 7737 (class 1259 OID 892170)
-- Name: idx_891083_organization_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891083_organization_node_id ON vaxiom.temporary_images USING btree (organization_node_id);


--
-- TOC entry 7738 (class 1259 OID 892155)
-- Name: idx_891083_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891083_patient_id ON vaxiom.temporary_images USING btree (patient_id);


--
-- TOC entry 7739 (class 1259 OID 892161)
-- Name: idx_891083_patient_image_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891083_patient_image_id ON vaxiom.temporary_images USING btree (patient_image_id);


--
-- TOC entry 7742 (class 1259 OID 892185)
-- Name: idx_891089_insurance_payer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891089_insurance_payer_id ON vaxiom.temp_accounts USING btree (insurance_payer_id);


--
-- TOC entry 7743 (class 1259 OID 892204)
-- Name: idx_891089_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891089_organization_id ON vaxiom.temp_accounts USING btree (organization_id);


--
-- TOC entry 7744 (class 1259 OID 892213)
-- Name: idx_891089_person_payer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891089_person_payer_id ON vaxiom.temp_accounts USING btree (person_payer_id);


--
-- TOC entry 7747 (class 1259 OID 892157)
-- Name: idx_891089_tx_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891089_tx_plan_id ON vaxiom.temp_accounts USING btree (tx_plan_id);


--
-- TOC entry 7748 (class 1259 OID 892176)
-- Name: idx_891092_organization_node_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891092_organization_node_id ON vaxiom.third_party_account USING btree (organization_node_id);


--
-- TOC entry 7753 (class 1259 OID 892164)
-- Name: idx_891098_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891098_snapshot_id ON vaxiom.toothchart_edit_log USING btree (snapshot_id);


--
-- TOC entry 7756 (class 1259 OID 892172)
-- Name: idx_891101_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891101_snapshot_id ON vaxiom.toothchart_note USING btree (snapshot_id);


--
-- TOC entry 7757 (class 1259 OID 892199)
-- Name: idx_891104_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891104_appointment_id ON vaxiom.toothchart_snapshot USING btree (appointment_id);


--
-- TOC entry 7762 (class 1259 OID 892182)
-- Name: idx_891110_snapshot_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891110_snapshot_id ON vaxiom.tooth_marking USING btree (snapshot_id);


--
-- TOC entry 7763 (class 1259 OID 892167)
-- Name: idx_891113_dtype; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891113_dtype ON vaxiom.transactions USING btree (dtype);


--
-- TOC entry 7766 (class 1259 OID 892166)
-- Name: idx_891113_receivable_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891113_receivable_id ON vaxiom.transactions USING btree (receivable_id);


--
-- TOC entry 7767 (class 1259 OID 892191)
-- Name: idx_891113_sys_created; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891113_sys_created ON vaxiom.transactions USING btree (sys_created);


--
-- TOC entry 7768 (class 1259 OID 892175)
-- Name: idx_891118_charge_transfer_adjustment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891118_charge_transfer_adjustment_id ON vaxiom.transfer_charges USING btree (charge_transfer_adjustment_id);


--
-- TOC entry 7771 (class 1259 OID 892181)
-- Name: idx_891121_migrated; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_migrated ON vaxiom.txs USING btree (migrated);


--
-- TOC entry 7772 (class 1259 OID 892209)
-- Name: idx_891121_organization_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_organization_id ON vaxiom.txs USING btree (organization_id);


--
-- TOC entry 7775 (class 1259 OID 892234)
-- Name: idx_891121_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_sys_created_at ON vaxiom.txs USING btree (sys_created_at);


--
-- TOC entry 7776 (class 1259 OID 892195)
-- Name: idx_891121_tx_card_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_tx_card_id ON vaxiom.txs USING btree (tx_card_id);


--
-- TOC entry 7777 (class 1259 OID 892179)
-- Name: idx_891121_tx_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891121_tx_plan_id ON vaxiom.txs USING btree (tx_plan_id);


--
-- TOC entry 7780 (class 1259 OID 892177)
-- Name: idx_891131_treatment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891131_treatment_id ON vaxiom.txs_state_changes USING btree (treatment_id);


--
-- TOC entry 7781 (class 1259 OID 892200)
-- Name: idx_891137_patient_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891137_patient_id ON vaxiom.tx_cards USING btree (patient_id);


--
-- TOC entry 7784 (class 1259 OID 892192)
-- Name: idx_891137_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891137_tx_category_id ON vaxiom.tx_cards USING btree (tx_category_id);


--
-- TOC entry 7785 (class 1259 OID 892193)
-- Name: idx_891141_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891141_organization_id_fk ON vaxiom.tx_card_field_definitions USING btree (organization_id);


--
-- TOC entry 7788 (class 1259 OID 892241)
-- Name: idx_891141_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891141_type_id ON vaxiom.tx_card_field_definitions USING btree (type_id);


--
-- TOC entry 7789 (class 1259 OID 892243)
-- Name: idx_891149_field_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891149_field_type_id ON vaxiom.tx_card_field_options USING btree (field_type_id);


--
-- TOC entry 7796 (class 1259 OID 892186)
-- Name: idx_891162_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891162_tx_category_id ON vaxiom.tx_card_templates USING btree (tx_category_id);


--
-- TOC entry 7797 (class 1259 OID 892211)
-- Name: idx_891162_tx_plan_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891162_tx_plan_template_id ON vaxiom.tx_card_templates USING btree (tx_plan_template_id);


--
-- TOC entry 7800 (class 1259 OID 892203)
-- Name: idx_891168_insurance_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891168_insurance_plan_id ON vaxiom.tx_category_coverages USING btree (insurance_plan_id);


--
-- TOC entry 7803 (class 1259 OID 892231)
-- Name: idx_891168_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891168_tx_category_id ON vaxiom.tx_category_coverages USING btree (tx_category_id);


--
-- TOC entry 7804 (class 1259 OID 892252)
-- Name: idx_891171_insured_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891171_insured_id ON vaxiom.tx_category_insured USING btree (insured_id);


--
-- TOC entry 7807 (class 1259 OID 892215)
-- Name: idx_891171_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891171_tx_category_id ON vaxiom.tx_category_insured USING btree (tx_category_id);


--
-- TOC entry 7810 (class 1259 OID 892220)
-- Name: idx_891174_tx_payer_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891174_tx_payer_id ON vaxiom.tx_contracts USING btree (tx_payer_id);


--
-- TOC entry 7811 (class 1259 OID 892198)
-- Name: idx_891180_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891180_insurance_code_id ON vaxiom.tx_fee_charges USING btree (insurance_code_id);


--
-- TOC entry 7814 (class 1259 OID 892207)
-- Name: idx_891180_tx_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891180_tx_plan_id ON vaxiom.tx_fee_charges USING btree (tx_plan_id);


--
-- TOC entry 7815 (class 1259 OID 892214)
-- Name: idx_891183_account_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_account_id ON vaxiom.tx_payers USING btree (account_id);


--
-- TOC entry 7816 (class 1259 OID 892217)
-- Name: idx_891183_contact_method_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_contact_method_id ON vaxiom.tx_payers USING btree (contact_method_id);


--
-- TOC entry 7817 (class 1259 OID 892239)
-- Name: idx_891183_insured_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_insured_id ON vaxiom.tx_payers USING btree (insured_id);


--
-- TOC entry 7818 (class 1259 OID 892266)
-- Name: idx_891183_person_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_person_id ON vaxiom.tx_payers USING btree (person_id);


--
-- TOC entry 7821 (class 1259 OID 892226)
-- Name: idx_891183_tx_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891183_tx_id ON vaxiom.tx_payers USING btree (tx_id);


--
-- TOC entry 7822 (class 1259 OID 892212)
-- Name: idx_891187_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891187_insurance_code_id ON vaxiom.tx_plans USING btree (insurance_code_id);


--
-- TOC entry 7825 (class 1259 OID 892208)
-- Name: idx_891187_tx_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891187_tx_id ON vaxiom.tx_plans USING btree (tx_id);


--
-- TOC entry 7826 (class 1259 OID 892230)
-- Name: idx_891187_tx_plan_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891187_tx_plan_template_id ON vaxiom.tx_plans USING btree (tx_plan_template_id);


--
-- TOC entry 7827 (class 1259 OID 892218)
-- Name: idx_891192_next_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_next_appointment_id ON vaxiom.tx_plan_appointments USING btree (next_appointment_id);


--
-- TOC entry 7830 (class 1259 OID 892229)
-- Name: idx_891192_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_sys_created_at ON vaxiom.tx_plan_appointments USING btree (sys_created_at);


--
-- TOC entry 7831 (class 1259 OID 892249)
-- Name: idx_891192_tx_plan_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_tx_plan_id ON vaxiom.tx_plan_appointments USING btree (tx_plan_id);


--
-- TOC entry 7832 (class 1259 OID 892275)
-- Name: idx_891192_tx_plan_template_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_tx_plan_template_appointment_id ON vaxiom.tx_plan_appointments USING btree (tx_plan_template_appointment_id);


--
-- TOC entry 7833 (class 1259 OID 892280)
-- Name: idx_891192_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891192_type_id ON vaxiom.tx_plan_appointments USING btree (type_id);


--
-- TOC entry 7836 (class 1259 OID 892223)
-- Name: idx_891197_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891197_procedure_id ON vaxiom.tx_plan_appointment_procedures USING btree (procedure_id);


--
-- TOC entry 7837 (class 1259 OID 892237)
-- Name: idx_891204_pc_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891204_pc_id ON vaxiom.tx_plan_groups USING btree (pc_id);


--
-- TOC entry 7840 (class 1259 OID 892238)
-- Name: idx_891208_diagnosis_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891208_diagnosis_id ON vaxiom.tx_plan_notes USING btree (diagnosis_id);


--
-- TOC entry 7843 (class 1259 OID 892236)
-- Name: idx_891208_tx_card_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891208_tx_card_id ON vaxiom.tx_plan_notes USING btree (tx_card_id);


--
-- TOC entry 7846 (class 1259 OID 892259)
-- Name: idx_891215_tx_plan_note_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891215_tx_plan_note_id ON vaxiom.tx_plan_note_groups USING btree (tx_plan_note_id);


--
-- TOC entry 7847 (class 1259 OID 892283)
-- Name: idx_891222_insurance_code_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891222_insurance_code_id ON vaxiom.tx_plan_templates USING btree (insurance_code_id);


--
-- TOC entry 7848 (class 1259 OID 892281)
-- Name: idx_891222_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891222_organization_id_fk ON vaxiom.tx_plan_templates USING btree (organization_id);


--
-- TOC entry 7851 (class 1259 OID 892235)
-- Name: idx_891222_tx_category_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891222_tx_category_id ON vaxiom.tx_plan_templates USING btree (tx_category_id);


--
-- TOC entry 7852 (class 1259 OID 892248)
-- Name: idx_891222_tx_plan_templates_ibfk_4; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891222_tx_plan_templates_ibfk_4 ON vaxiom.tx_plan_templates USING btree (tx_plan_group_id);


--
-- TOC entry 7853 (class 1259 OID 892233)
-- Name: idx_891228_next_appointment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891228_next_appointment_id ON vaxiom.tx_plan_template_appointments USING btree (next_appointment_id);


--
-- TOC entry 7856 (class 1259 OID 892244)
-- Name: idx_891228_sys_created_at; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891228_sys_created_at ON vaxiom.tx_plan_template_appointments USING btree (sys_created_at);


--
-- TOC entry 7857 (class 1259 OID 892250)
-- Name: idx_891228_tx_plan_template_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891228_tx_plan_template_id ON vaxiom.tx_plan_template_appointments USING btree (tx_plan_template_id);


--
-- TOC entry 7858 (class 1259 OID 892254)
-- Name: idx_891228_type_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891228_type_id ON vaxiom.tx_plan_template_appointments USING btree (type_id);


--
-- TOC entry 7861 (class 1259 OID 892286)
-- Name: idx_891233_procedure_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891233_procedure_id ON vaxiom.tx_plan_template_appointment_procedures USING btree (procedure_id);


--
-- TOC entry 7864 (class 1259 OID 892262)
-- Name: idx_891240_unique_index; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE UNIQUE INDEX idx_891240_unique_index ON vaxiom.tx_statuses USING btree (parent_company_id, type);


--
-- TOC entry 7865 (class 1259 OID 892246)
-- Name: idx_891244_applied_payment_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891244_applied_payment_id ON vaxiom.unapplied_payments USING btree (applied_payment_id);


--
-- TOC entry 7868 (class 1259 OID 892245)
-- Name: idx_891253_fri; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_fri ON vaxiom.week_templates USING btree (fri);


--
-- TOC entry 7869 (class 1259 OID 892255)
-- Name: idx_891253_mon; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_mon ON vaxiom.week_templates USING btree (mon);


--
-- TOC entry 7870 (class 1259 OID 892258)
-- Name: idx_891253_organization_id_fk; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_organization_id_fk ON vaxiom.week_templates USING btree (organization_id);


--
-- TOC entry 7873 (class 1259 OID 892263)
-- Name: idx_891253_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_resource_id ON vaxiom.week_templates USING btree (resource_id);


--
-- TOC entry 7874 (class 1259 OID 892278)
-- Name: idx_891253_sat; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_sat ON vaxiom.week_templates USING btree (sat);


--
-- TOC entry 7875 (class 1259 OID 892284)
-- Name: idx_891253_sun; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_sun ON vaxiom.week_templates USING btree (sun);


--
-- TOC entry 7876 (class 1259 OID 892279)
-- Name: idx_891253_thu; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_thu ON vaxiom.week_templates USING btree (thu);


--
-- TOC entry 7877 (class 1259 OID 892274)
-- Name: idx_891253_tue; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_tue ON vaxiom.week_templates USING btree (tue);


--
-- TOC entry 7878 (class 1259 OID 892260)
-- Name: idx_891253_wed; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891253_wed ON vaxiom.week_templates USING btree (wed);


--
-- TOC entry 7879 (class 1259 OID 892267)
-- Name: idx_891256_core_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891256_core_user_id ON vaxiom.work_hours USING btree (core_user_id);


--
-- TOC entry 7880 (class 1259 OID 892256)
-- Name: idx_891256_location_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891256_location_id ON vaxiom.work_hours USING btree (location_id);


--
-- TOC entry 7881 (class 1259 OID 892264)
-- Name: idx_891256_modified_by_admin; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891256_modified_by_admin ON vaxiom.work_hours USING btree (modified_by_admin);


--
-- TOC entry 7884 (class 1259 OID 892272)
-- Name: idx_891260_core_user_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891260_core_user_id ON vaxiom.work_hour_comments USING btree (core_user_id);


--
-- TOC entry 7887 (class 1259 OID 892285)
-- Name: idx_891260_work_hours_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891260_work_hours_id ON vaxiom.work_hour_comments USING btree (work_hours_id);


--
-- TOC entry 7888 (class 1259 OID 892287)
-- Name: idx_891266_day_schedule_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891266_day_schedule_id ON vaxiom.work_schedule_days USING btree (day_schedule_id);


--
-- TOC entry 7891 (class 1259 OID 892276)
-- Name: idx_891266_resource_id; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891266_resource_id ON vaxiom.work_schedule_days USING btree (resource_id);


--
-- TOC entry 7892 (class 1259 OID 892271)
-- Name: idx_891266_work_schedule_days_date_resource; Type: INDEX; Schema: vaxiom; Owner: postgres
--

CREATE INDEX idx_891266_work_schedule_days_date_resource ON vaxiom.work_schedule_days USING btree (ws_date, resource_id);


--
-- TOC entry 8468 (class 2620 OID 895533)
-- Name: act_evt_log on_update_current_timestamp; Type: TRIGGER; Schema: vaxiom; Owner: postgres
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON vaxiom.act_evt_log FOR EACH ROW EXECUTE PROCEDURE vaxiom.on_update_current_timestamp_act_evt_log();


--
-- TOC entry 8469 (class 2620 OID 895535)
-- Name: act_re_deployment on_update_current_timestamp; Type: TRIGGER; Schema: vaxiom; Owner: postgres
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON vaxiom.act_re_deployment FOR EACH ROW EXECUTE PROCEDURE vaxiom.on_update_current_timestamp_act_re_deployment();


--
-- TOC entry 8470 (class 2620 OID 895537)
-- Name: act_ru_task on_update_current_timestamp; Type: TRIGGER; Schema: vaxiom; Owner: postgres
--

CREATE TRIGGER on_update_current_timestamp BEFORE UPDATE ON vaxiom.act_ru_task FOR EACH ROW EXECUTE PROCEDURE vaxiom.on_update_current_timestamp_act_ru_task();


--
-- TOC entry 7908 (class 2606 OID 892732)
-- Name: act_ru_identitylink act_fk_athrz_procedef; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_identitylink
    ADD CONSTRAINT act_fk_athrz_procedef FOREIGN KEY (proc_def_id_) REFERENCES vaxiom.act_re_procdef(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7897 (class 2606 OID 892677)
-- Name: act_ge_bytearray act_fk_bytearr_depl; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ge_bytearray
    ADD CONSTRAINT act_fk_bytearr_depl FOREIGN KEY (deployment_id_) REFERENCES vaxiom.act_re_deployment(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7903 (class 2606 OID 892707)
-- Name: act_ru_event_subscr act_fk_event_exec; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_event_subscr
    ADD CONSTRAINT act_fk_event_exec FOREIGN KEY (execution_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7904 (class 2606 OID 892712)
-- Name: act_ru_execution act_fk_exe_parent; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT act_fk_exe_parent FOREIGN KEY (parent_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7905 (class 2606 OID 892717)
-- Name: act_ru_execution act_fk_exe_procdef; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT act_fk_exe_procdef FOREIGN KEY (proc_def_id_) REFERENCES vaxiom.act_re_procdef(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7906 (class 2606 OID 892722)
-- Name: act_ru_execution act_fk_exe_procinst; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT act_fk_exe_procinst FOREIGN KEY (proc_inst_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 7907 (class 2606 OID 892727)
-- Name: act_ru_execution act_fk_exe_super; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_execution
    ADD CONSTRAINT act_fk_exe_super FOREIGN KEY (super_exec_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7909 (class 2606 OID 892737)
-- Name: act_ru_identitylink act_fk_idl_procinst; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_identitylink
    ADD CONSTRAINT act_fk_idl_procinst FOREIGN KEY (proc_inst_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7911 (class 2606 OID 892747)
-- Name: act_ru_job act_fk_job_exception; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_job
    ADD CONSTRAINT act_fk_job_exception FOREIGN KEY (exception_stack_id_) REFERENCES vaxiom.act_ge_bytearray(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7898 (class 2606 OID 892682)
-- Name: act_id_membership act_fk_memb_group; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_membership
    ADD CONSTRAINT act_fk_memb_group FOREIGN KEY (group_id_) REFERENCES vaxiom.act_id_group(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7899 (class 2606 OID 892687)
-- Name: act_id_membership act_fk_memb_user; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_id_membership
    ADD CONSTRAINT act_fk_memb_user FOREIGN KEY (user_id_) REFERENCES vaxiom.act_id_user(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7900 (class 2606 OID 892692)
-- Name: act_re_model act_fk_model_deployment; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_model
    ADD CONSTRAINT act_fk_model_deployment FOREIGN KEY (deployment_id_) REFERENCES vaxiom.act_re_deployment(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7901 (class 2606 OID 892697)
-- Name: act_re_model act_fk_model_source; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_model
    ADD CONSTRAINT act_fk_model_source FOREIGN KEY (editor_source_value_id_) REFERENCES vaxiom.act_ge_bytearray(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7902 (class 2606 OID 892702)
-- Name: act_re_model act_fk_model_source_extra; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_re_model
    ADD CONSTRAINT act_fk_model_source_extra FOREIGN KEY (editor_source_extra_value_id_) REFERENCES vaxiom.act_ge_bytearray(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7912 (class 2606 OID 892752)
-- Name: act_ru_task act_fk_task_exe; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_task
    ADD CONSTRAINT act_fk_task_exe FOREIGN KEY (execution_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7913 (class 2606 OID 892757)
-- Name: act_ru_task act_fk_task_procdef; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_task
    ADD CONSTRAINT act_fk_task_procdef FOREIGN KEY (proc_def_id_) REFERENCES vaxiom.act_re_procdef(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7914 (class 2606 OID 892762)
-- Name: act_ru_task act_fk_task_procinst; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_task
    ADD CONSTRAINT act_fk_task_procinst FOREIGN KEY (proc_inst_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7910 (class 2606 OID 892742)
-- Name: act_ru_identitylink act_fk_tskass_task; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_identitylink
    ADD CONSTRAINT act_fk_tskass_task FOREIGN KEY (task_id_) REFERENCES vaxiom.act_ru_task(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7915 (class 2606 OID 892767)
-- Name: act_ru_variable act_fk_var_bytearray; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_variable
    ADD CONSTRAINT act_fk_var_bytearray FOREIGN KEY (bytearray_id_) REFERENCES vaxiom.act_ge_bytearray(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7916 (class 2606 OID 892772)
-- Name: act_ru_variable act_fk_var_exe; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_variable
    ADD CONSTRAINT act_fk_var_exe FOREIGN KEY (execution_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7917 (class 2606 OID 892777)
-- Name: act_ru_variable act_fk_var_procinst; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.act_ru_variable
    ADD CONSTRAINT act_fk_var_procinst FOREIGN KEY (proc_inst_id_) REFERENCES vaxiom.act_ru_execution(id_) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7895 (class 2606 OID 892667)
-- Name: activiti_process_settings activiti_process_settings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.activiti_process_settings
    ADD CONSTRAINT activiti_process_settings_ibfk_1 FOREIGN KEY (organization_node_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7896 (class 2606 OID 892672)
-- Name: activiti_process_settings_property activiti_process_settings_property_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.activiti_process_settings_property
    ADD CONSTRAINT activiti_process_settings_property_ibfk_1 FOREIGN KEY (activiti_process_settings_id) REFERENCES vaxiom.activiti_process_settings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7918 (class 2606 OID 892782)
-- Name: application_properties application_properties_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.application_properties
    ADD CONSTRAINT application_properties_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7919 (class 2606 OID 892787)
-- Name: applied_payments applied_payments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.applied_payments
    ADD CONSTRAINT applied_payments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7920 (class 2606 OID 892792)
-- Name: applied_payments applied_payments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.applied_payments
    ADD CONSTRAINT applied_payments_ibfk_2 FOREIGN KEY (payment_id) REFERENCES vaxiom.payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7927 (class 2606 OID 892827)
-- Name: appointment_audit_log appointment_audit_log_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_audit_log
    ADD CONSTRAINT appointment_audit_log_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7928 (class 2606 OID 892832)
-- Name: appointment_audit_log appointment_audit_log_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_audit_log
    ADD CONSTRAINT appointment_audit_log_ibfk_2 FOREIGN KEY (id) REFERENCES vaxiom.audit_logs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7935 (class 2606 OID 892867)
-- Name: appointment_booking_resources appointment_booking_resources_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_resources
    ADD CONSTRAINT appointment_booking_resources_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7936 (class 2606 OID 892872)
-- Name: appointment_booking_resources appointment_booking_resources_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_resources
    ADD CONSTRAINT appointment_booking_resources_ibfk_2 FOREIGN KEY (booking_id) REFERENCES vaxiom.appointment_bookings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7937 (class 2606 OID 892877)
-- Name: appointment_booking_state_changes appointment_booking_state_changes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_booking_state_changes
    ADD CONSTRAINT appointment_booking_state_changes_ibfk_1 FOREIGN KEY (booking_id) REFERENCES vaxiom.appointment_bookings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7929 (class 2606 OID 892837)
-- Name: appointment_bookings appointment_bookings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_1 FOREIGN KEY (chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7930 (class 2606 OID 892842)
-- Name: appointment_bookings appointment_bookings_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_2 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7931 (class 2606 OID 892847)
-- Name: appointment_bookings appointment_bookings_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_3 FOREIGN KEY (provider_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7932 (class 2606 OID 892852)
-- Name: appointment_bookings appointment_bookings_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_4 FOREIGN KEY (assistant_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7933 (class 2606 OID 892857)
-- Name: appointment_bookings appointment_bookings_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT appointment_bookings_ibfk_5 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7938 (class 2606 OID 892882)
-- Name: appointment_field_values appointment_field_values_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_field_values
    ADD CONSTRAINT appointment_field_values_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7939 (class 2606 OID 892887)
-- Name: appointment_field_values appointment_field_values_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_field_values
    ADD CONSTRAINT appointment_field_values_ibfk_2 FOREIGN KEY (field_option_id) REFERENCES vaxiom.tx_card_field_options(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7940 (class 2606 OID 892892)
-- Name: appointment_field_values appointment_field_values_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_field_values
    ADD CONSTRAINT appointment_field_values_ibfk_3 FOREIGN KEY (field_definition_id) REFERENCES vaxiom.tx_card_field_definitions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7941 (class 2606 OID 892897)
-- Name: appointment_procedures appointment_procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_procedures
    ADD CONSTRAINT appointment_procedures_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7942 (class 2606 OID 892902)
-- Name: appointment_procedures appointment_procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_procedures
    ADD CONSTRAINT appointment_procedures_ibfk_2 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8336 (class 2606 OID 894872)
-- Name: selected_appointment_templates appointment_template_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates
    ADD CONSTRAINT appointment_template_id FOREIGN KEY (appointment_template_id) REFERENCES vaxiom.appointment_templates(id);


--
-- TOC entry 7947 (class 2606 OID 892927)
-- Name: appointment_template_procedures appointment_template_procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_template_procedures
    ADD CONSTRAINT appointment_template_procedures_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7948 (class 2606 OID 892932)
-- Name: appointment_template_procedures appointment_template_procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_template_procedures
    ADD CONSTRAINT appointment_template_procedures_ibfk_2 FOREIGN KEY (appointment_template_id) REFERENCES vaxiom.appointment_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7943 (class 2606 OID 892907)
-- Name: appointment_templates appointment_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT appointment_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7944 (class 2606 OID 892912)
-- Name: appointment_templates appointment_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT appointment_templates_ibfk_2 FOREIGN KEY (appointment_type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7945 (class 2606 OID 892917)
-- Name: appointment_templates appointment_templates_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT appointment_templates_ibfk_3 FOREIGN KEY (primary_provider_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7946 (class 2606 OID 892922)
-- Name: appointment_templates appointment_templates_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_templates
    ADD CONSTRAINT appointment_templates_ibfk_4 FOREIGN KEY (primary_assistant_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7951 (class 2606 OID 892947)
-- Name: appointment_type_daily_restrictions appointment_type_daily_restrictions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_type_daily_restrictions
    ADD CONSTRAINT appointment_type_daily_restrictions_ibfk_1 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7952 (class 2606 OID 892952)
-- Name: appointment_type_daily_restrictions appointment_type_daily_restrictions_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_type_daily_restrictions
    ADD CONSTRAINT appointment_type_daily_restrictions_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7949 (class 2606 OID 892937)
-- Name: appointment_types appointment_types_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_types
    ADD CONSTRAINT appointment_types_ibfk_1 FOREIGN KEY (parent_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7950 (class 2606 OID 892942)
-- Name: appointment_types_property appointment_types_property_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_types_property
    ADD CONSTRAINT appointment_types_property_ibfk_1 FOREIGN KEY (appointment_type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7921 (class 2606 OID 892797)
-- Name: appointments appointments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7922 (class 2606 OID 892802)
-- Name: appointments appointments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7923 (class 2606 OID 892807)
-- Name: appointments appointments_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_3 FOREIGN KEY (next_appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7924 (class 2606 OID 892812)
-- Name: appointments appointments_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_4 FOREIGN KEY (tx_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7925 (class 2606 OID 892817)
-- Name: appointments appointments_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_5 FOREIGN KEY (tx_plan_appointment_id) REFERENCES vaxiom.tx_plan_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7926 (class 2606 OID 892822)
-- Name: appointments appointments_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointments
    ADD CONSTRAINT appointments_ibfk_6 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7953 (class 2606 OID 892957)
-- Name: appt_type_template_restrictions appt_type_template_restrictions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appt_type_template_restrictions
    ADD CONSTRAINT appt_type_template_restrictions_ibfk_1 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7954 (class 2606 OID 892962)
-- Name: appt_type_template_restrictions appt_type_template_restrictions_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appt_type_template_restrictions
    ADD CONSTRAINT appt_type_template_restrictions_ibfk_2 FOREIGN KEY (template_id) REFERENCES vaxiom.office_day_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7955 (class 2606 OID 892967)
-- Name: audit_logs audit_logs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.audit_logs
    ADD CONSTRAINT audit_logs_ibfk_1 FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7956 (class 2606 OID 892972)
-- Name: autopay_invoice_changes autopay_invoice_changes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.autopay_invoice_changes
    ADD CONSTRAINT autopay_invoice_changes_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7960 (class 2606 OID 892992)
-- Name: bluefin_credentials bluefin_credentials_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.bluefin_credentials
    ADD CONSTRAINT bluefin_credentials_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7961 (class 2606 OID 892997)
-- Name: cancelled_receivables cancelled_receivables_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.cancelled_receivables
    ADD CONSTRAINT cancelled_receivables_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7962 (class 2606 OID 893002)
-- Name: cephx_requests cephx_requests_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.cephx_requests
    ADD CONSTRAINT cephx_requests_ibfk_1 FOREIGN KEY (patient_image_id) REFERENCES vaxiom.patient_images(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7964 (class 2606 OID 893012)
-- Name: chair_allocations chair_allocations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chair_allocations
    ADD CONSTRAINT chair_allocations_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7965 (class 2606 OID 893017)
-- Name: chair_allocations chair_allocations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chair_allocations
    ADD CONSTRAINT chair_allocations_ibfk_2 FOREIGN KEY (chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7963 (class 2606 OID 893007)
-- Name: chairs chairs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.chairs
    ADD CONSTRAINT chairs_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7966 (class 2606 OID 893022)
-- Name: charge_transfer_adjustments charge_transfer_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.charge_transfer_adjustments
    ADD CONSTRAINT charge_transfer_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7967 (class 2606 OID 893027)
-- Name: checks checks_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.checks
    ADD CONSTRAINT checks_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7968 (class 2606 OID 893032)
-- Name: checks checks_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.checks
    ADD CONSTRAINT checks_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.check_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7969 (class 2606 OID 893037)
-- Name: claim_events claim_events_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_events
    ADD CONSTRAINT claim_events_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7970 (class 2606 OID 893042)
-- Name: claim_requests claim_requests_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT claim_requests_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7971 (class 2606 OID 893047)
-- Name: claim_requests claim_requests_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT claim_requests_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7972 (class 2606 OID 893052)
-- Name: claim_requests claim_requests_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT claim_requests_ibfk_3 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7973 (class 2606 OID 893057)
-- Name: claim_requests claim_requests_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.claim_requests
    ADD CONSTRAINT claim_requests_ibfk_4 FOREIGN KEY (treatment_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7978 (class 2606 OID 893082)
-- Name: collection_label_agencies collection_label_agencies_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_agencies
    ADD CONSTRAINT collection_label_agencies_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7979 (class 2606 OID 893087)
-- Name: collection_label_history collection_label_history_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT collection_label_history_ibfk_1 FOREIGN KEY (created_by) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7980 (class 2606 OID 893092)
-- Name: collection_label_history collection_label_history_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT collection_label_history_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7981 (class 2606 OID 893097)
-- Name: collection_label_history collection_label_history_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT collection_label_history_ibfk_3 FOREIGN KEY (payment_account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7982 (class 2606 OID 893102)
-- Name: collection_label_history collection_label_history_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_history
    ADD CONSTRAINT collection_label_history_ibfk_4 FOREIGN KEY (previous_collection_label_template_id) REFERENCES vaxiom.collection_label_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7983 (class 2606 OID 893107)
-- Name: collection_label_templates collection_label_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_templates
    ADD CONSTRAINT collection_label_templates_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7984 (class 2606 OID 893112)
-- Name: collection_label_templates collection_label_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_label_templates
    ADD CONSTRAINT collection_label_templates_ibfk_2 FOREIGN KEY (agency_id) REFERENCES vaxiom.collection_label_agencies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7974 (class 2606 OID 893062)
-- Name: collection_labels collection_labels_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT collection_labels_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7975 (class 2606 OID 893067)
-- Name: collection_labels collection_labels_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT collection_labels_ibfk_2 FOREIGN KEY (payment_account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7976 (class 2606 OID 893072)
-- Name: collection_labels collection_labels_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT collection_labels_ibfk_3 FOREIGN KEY (collection_label_template_id) REFERENCES vaxiom.collection_label_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7977 (class 2606 OID 893077)
-- Name: collection_labels collection_labels_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.collection_labels
    ADD CONSTRAINT collection_labels_ibfk_4 FOREIGN KEY (last_modified_by) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7985 (class 2606 OID 893117)
-- Name: communication_preferences communication_preferences_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.communication_preferences
    ADD CONSTRAINT communication_preferences_ibfk_1 FOREIGN KEY (contact_method_association_id) REFERENCES vaxiom.contact_method_associations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7986 (class 2606 OID 893122)
-- Name: communication_preferences communication_preferences_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.communication_preferences
    ADD CONSTRAINT communication_preferences_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7987 (class 2606 OID 893127)
-- Name: contact_emails contact_emails_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_emails
    ADD CONSTRAINT contact_emails_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7988 (class 2606 OID 893132)
-- Name: contact_method_associations contact_method_associations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_method_associations
    ADD CONSTRAINT contact_method_associations_ibfk_1 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7989 (class 2606 OID 893137)
-- Name: contact_method_associations contact_method_associations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_method_associations
    ADD CONSTRAINT contact_method_associations_ibfk_2 FOREIGN KEY (contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7990 (class 2606 OID 893142)
-- Name: contact_method_associations contact_method_associations_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_method_associations
    ADD CONSTRAINT contact_method_associations_ibfk_3 FOREIGN KEY (error_location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7991 (class 2606 OID 893147)
-- Name: contact_phones contact_phones_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_phones
    ADD CONSTRAINT contact_phones_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7992 (class 2606 OID 893152)
-- Name: contact_postal_addresses contact_postal_addresses_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_postal_addresses
    ADD CONSTRAINT contact_postal_addresses_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7993 (class 2606 OID 893157)
-- Name: contact_recently_opened_items contact_recently_opened_items_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_recently_opened_items
    ADD CONSTRAINT contact_recently_opened_items_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.recently_opened_items(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7995 (class 2606 OID 893167)
-- Name: correction_types correction_types_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types
    ADD CONSTRAINT correction_types_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7996 (class 2606 OID 893172)
-- Name: correction_types_locations correction_types_locations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_locations
    ADD CONSTRAINT correction_types_locations_ibfk_1 FOREIGN KEY (correction_type_id) REFERENCES vaxiom.correction_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7997 (class 2606 OID 893177)
-- Name: correction_types_locations correction_types_locations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_locations
    ADD CONSTRAINT correction_types_locations_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7998 (class 2606 OID 893182)
-- Name: correction_types_payment_options correction_types_payment_options_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_payment_options
    ADD CONSTRAINT correction_types_payment_options_ibfk_1 FOREIGN KEY (correction_types_id) REFERENCES vaxiom.correction_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7999 (class 2606 OID 893187)
-- Name: correction_types_payment_options correction_types_payment_options_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.correction_types_payment_options
    ADD CONSTRAINT correction_types_payment_options_ibfk_2 FOREIGN KEY (payment_options_id) REFERENCES vaxiom.payment_options(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8000 (class 2606 OID 893192)
-- Name: daily_transactions daily_transactions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.daily_transactions
    ADD CONSTRAINT daily_transactions_ibfk_1 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8003 (class 2606 OID 893207)
-- Name: day_schedule_break_hours day_schedule_break_hours_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_break_hours
    ADD CONSTRAINT day_schedule_break_hours_ibfk_1 FOREIGN KEY (day_schedule_id) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8004 (class 2606 OID 893212)
-- Name: day_schedule_office_hours day_schedule_office_hours_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedule_office_hours
    ADD CONSTRAINT day_schedule_office_hours_ibfk_1 FOREIGN KEY (day_schedule_id) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8001 (class 2606 OID 893197)
-- Name: day_schedules day_schedules_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedules
    ADD CONSTRAINT day_schedules_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8002 (class 2606 OID 893202)
-- Name: day_schedules day_schedules_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.day_schedules
    ADD CONSTRAINT day_schedules_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8005 (class 2606 OID 893217)
-- Name: default_appointment_templates default_appointment_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.default_appointment_templates
    ADD CONSTRAINT default_appointment_templates_ibfk_1 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8006 (class 2606 OID 893222)
-- Name: default_appointment_templates default_appointment_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.default_appointment_templates
    ADD CONSTRAINT default_appointment_templates_ibfk_2 FOREIGN KEY (template_id) REFERENCES vaxiom.appointment_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8007 (class 2606 OID 893227)
-- Name: dentalchart_additional_markings dentalchart_additional_markings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_additional_markings
    ADD CONSTRAINT dentalchart_additional_markings_ibfk_1 FOREIGN KEY (marking_id) REFERENCES vaxiom.dentalchart_marking(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8008 (class 2606 OID 893232)
-- Name: dentalchart_edit_log dentalchart_edit_log_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_edit_log
    ADD CONSTRAINT dentalchart_edit_log_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.audit_logs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8009 (class 2606 OID 893237)
-- Name: dentalchart_edit_log dentalchart_edit_log_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_edit_log
    ADD CONSTRAINT dentalchart_edit_log_ibfk_2 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.dentalchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8010 (class 2606 OID 893242)
-- Name: dentalchart_marking dentalchart_marking_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_marking
    ADD CONSTRAINT dentalchart_marking_ibfk_1 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.dentalchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8011 (class 2606 OID 893247)
-- Name: dentalchart_snapshot dentalchart_snapshot_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dentalchart_snapshot
    ADD CONSTRAINT dentalchart_snapshot_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8270 (class 2606 OID 894542)
-- Name: permissions_user_role_departament department_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_departament
    ADD CONSTRAINT department_fk FOREIGN KEY (department_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8014 (class 2606 OID 893262)
-- Name: diagnosis_field_values diagnosis_field_values_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_field_values
    ADD CONSTRAINT diagnosis_field_values_ibfk_1 FOREIGN KEY (diagnosis_id) REFERENCES vaxiom.diagnosis(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8012 (class 2606 OID 893252)
-- Name: diagnosis diagnosis_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis
    ADD CONSTRAINT diagnosis_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8013 (class 2606 OID 893257)
-- Name: diagnosis diagnosis_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis
    ADD CONSTRAINT diagnosis_ibfk_2 FOREIGN KEY (diagnosis_template_id) REFERENCES vaxiom.diagnosis_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8015 (class 2606 OID 893267)
-- Name: diagnosis_letters diagnosis_letters_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT diagnosis_letters_ibfk_1 FOREIGN KEY (diagnosis_id) REFERENCES vaxiom.diagnosis(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8016 (class 2606 OID 893272)
-- Name: diagnosis_letters diagnosis_letters_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT diagnosis_letters_ibfk_2 FOREIGN KEY (document_template_id) REFERENCES vaxiom.document_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8017 (class 2606 OID 893277)
-- Name: diagnosis_letters diagnosis_letters_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT diagnosis_letters_ibfk_3 FOREIGN KEY (recipient_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8018 (class 2606 OID 893282)
-- Name: diagnosis_letters diagnosis_letters_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_letters
    ADD CONSTRAINT diagnosis_letters_ibfk_4 FOREIGN KEY (address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8019 (class 2606 OID 893287)
-- Name: diagnosis_templates diagnosis_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_templates
    ADD CONSTRAINT diagnosis_templates_ibfk_1 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8020 (class 2606 OID 893292)
-- Name: diagnosis_templates diagnosis_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_templates
    ADD CONSTRAINT diagnosis_templates_ibfk_2 FOREIGN KEY (appointment_type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8021 (class 2606 OID 893297)
-- Name: diagnosis_templates diagnosis_templates_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.diagnosis_templates
    ADD CONSTRAINT diagnosis_templates_ibfk_3 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8023 (class 2606 OID 893307)
-- Name: discount_adjustments discount_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_adjustments
    ADD CONSTRAINT discount_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8024 (class 2606 OID 893312)
-- Name: discount_adjustments discount_adjustments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_adjustments
    ADD CONSTRAINT discount_adjustments_ibfk_2 FOREIGN KEY (discount_id) REFERENCES vaxiom.discounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8025 (class 2606 OID 893317)
-- Name: discount_reverse_adjustments discount_reverse_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_reverse_adjustments
    ADD CONSTRAINT discount_reverse_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8026 (class 2606 OID 893322)
-- Name: discount_reverse_adjustments discount_reverse_adjustments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discount_reverse_adjustments
    ADD CONSTRAINT discount_reverse_adjustments_ibfk_2 FOREIGN KEY (discount_adjustment_id) REFERENCES vaxiom.discount_adjustments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8022 (class 2606 OID 893302)
-- Name: discounts discounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.discounts
    ADD CONSTRAINT discounts_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8027 (class 2606 OID 893327)
-- Name: dock_item_features_events dock_item_features_events_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_events
    ADD CONSTRAINT dock_item_features_events_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8028 (class 2606 OID 893332)
-- Name: dock_item_features_images dock_item_features_images_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_images
    ADD CONSTRAINT dock_item_features_images_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8029 (class 2606 OID 893337)
-- Name: dock_item_features_images dock_item_features_images_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_images
    ADD CONSTRAINT dock_item_features_images_ibfk_2 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8030 (class 2606 OID 893342)
-- Name: dock_item_features_messages dock_item_features_messages_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_messages
    ADD CONSTRAINT dock_item_features_messages_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8031 (class 2606 OID 893347)
-- Name: dock_item_features_note dock_item_features_note_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_note
    ADD CONSTRAINT dock_item_features_note_ibfk_1 FOREIGN KEY (note_id) REFERENCES vaxiom.notebook_notes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8032 (class 2606 OID 893352)
-- Name: dock_item_features_note dock_item_features_note_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_note
    ADD CONSTRAINT dock_item_features_note_ibfk_2 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8033 (class 2606 OID 893357)
-- Name: dock_item_features_tasks dock_item_features_tasks_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.dock_item_features_tasks
    ADD CONSTRAINT dock_item_features_tasks_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.dock_item_descriptors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8034 (class 2606 OID 893362)
-- Name: document_recently_opened_items document_recently_opened_items_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_recently_opened_items
    ADD CONSTRAINT document_recently_opened_items_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.recently_opened_items(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8035 (class 2606 OID 893367)
-- Name: document_templates document_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_templates
    ADD CONSTRAINT document_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8036 (class 2606 OID 893372)
-- Name: document_tree_nodes document_tree_nodes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.document_tree_nodes
    ADD CONSTRAINT document_tree_nodes_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8075 (class 2606 OID 893567)
-- Name: employment_contract_permissions ecperm_emploment_contract_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_permissions
    ADD CONSTRAINT ecperm_emploment_contract_fk FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8076 (class 2606 OID 893572)
-- Name: employment_contract_permissions ecperm_permission_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_permissions
    ADD CONSTRAINT ecperm_permission_fk FOREIGN KEY (permissions_permission_id) REFERENCES vaxiom.permissions_permission(id);


--
-- TOC entry 8077 (class 2606 OID 893577)
-- Name: employment_contract_role ecrole_emploment_contract_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_role
    ADD CONSTRAINT ecrole_emploment_contract_fk FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8078 (class 2606 OID 893582)
-- Name: employment_contract_role ecrole_user_role_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_role
    ADD CONSTRAINT ecrole_user_role_fk FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id);


--
-- TOC entry 8073 (class 2606 OID 893557)
-- Name: employment_contract_location_group emploment_contract_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_location_group
    ADD CONSTRAINT emploment_contract_fk FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8053 (class 2606 OID 893457)
-- Name: employee_professional_relationships employee_professional_relationships_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_professional_relationships
    ADD CONSTRAINT employee_professional_relationships_ibfk_1 FOREIGN KEY (provider_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8054 (class 2606 OID 893462)
-- Name: employee_resources employee_resources_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_resources
    ADD CONSTRAINT employee_resources_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 8055 (class 2606 OID 893467)
-- Name: employee_resources employee_resources_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_resources
    ADD CONSTRAINT employee_resources_ibfk_2 FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8056 (class 2606 OID 893472)
-- Name: employers employers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_1 FOREIGN KEY (contact_postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8057 (class 2606 OID 893477)
-- Name: employers employers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_2 FOREIGN KEY (contact_phone_id) REFERENCES vaxiom.contact_phones(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8058 (class 2606 OID 893482)
-- Name: employers employers_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_3 FOREIGN KEY (contact_email_id) REFERENCES vaxiom.contact_emails(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8059 (class 2606 OID 893487)
-- Name: employers employers_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_4 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8060 (class 2606 OID 893492)
-- Name: employers employers_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employers
    ADD CONSTRAINT employers_ibfk_5 FOREIGN KEY (master_id) REFERENCES vaxiom.employers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8061 (class 2606 OID 893497)
-- Name: employment_contracts employment_contracts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT employment_contracts_ibfk_1 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8062 (class 2606 OID 893502)
-- Name: employment_contracts employment_contracts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT employment_contracts_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8081 (class 2606 OID 893597)
-- Name: enrollment_form enrollment_form_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form
    ADD CONSTRAINT enrollment_form_ibfk_1 FOREIGN KEY (employee_id) REFERENCES vaxiom.employment_contracts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8082 (class 2606 OID 893602)
-- Name: enrollment_form enrollment_form_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_form
    ADD CONSTRAINT enrollment_form_ibfk_2 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.hradmin_enrollment(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8086 (class 2606 OID 893622)
-- Name: enrollment_group_enrollment enrollment_group_enrollment_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_group_enrollment
    ADD CONSTRAINT enrollment_group_enrollment_ibfk_1 FOREIGN KEY (enrollment_group_id) REFERENCES vaxiom.enrollment_group(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8087 (class 2606 OID 893627)
-- Name: enrollment_group_enrollment enrollment_group_enrollment_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_group_enrollment
    ADD CONSTRAINT enrollment_group_enrollment_ibfk_2 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8083 (class 2606 OID 893607)
-- Name: enrollment_groups_enrollment_legal_entity enrollment_groups_enrollment_legal_entity_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_groups_enrollment_legal_entity
    ADD CONSTRAINT enrollment_groups_enrollment_legal_entity_ibfk_1 FOREIGN KEY (enrollment_group_id) REFERENCES vaxiom.enrollment_group(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8084 (class 2606 OID 893612)
-- Name: enrollment_groups_enrollment_legal_entity enrollment_groups_enrollment_legal_entity_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_groups_enrollment_legal_entity
    ADD CONSTRAINT enrollment_groups_enrollment_legal_entity_ibfk_2 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8085 (class 2606 OID 893617)
-- Name: enrollment_groups_enrollment_legal_entity enrollment_groups_enrollment_legal_entity_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_groups_enrollment_legal_entity
    ADD CONSTRAINT enrollment_groups_enrollment_legal_entity_ibfk_3 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8088 (class 2606 OID 893632)
-- Name: enrollment_schedule_billing_groups enrollment_schedule_billing_groups_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_schedule_billing_groups
    ADD CONSTRAINT enrollment_schedule_billing_groups_ibfk_1 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8089 (class 2606 OID 893637)
-- Name: enrollment_schedule_billing_groups enrollment_schedule_billing_groups_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment_schedule_billing_groups
    ADD CONSTRAINT enrollment_schedule_billing_groups_ibfk_2 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8090 (class 2606 OID 893642)
-- Name: equipment equipment_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.equipment
    ADD CONSTRAINT equipment_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8091 (class 2606 OID 893647)
-- Name: external_offices external_offices_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.external_offices
    ADD CONSTRAINT external_offices_ibfk_1 FOREIGN KEY (postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8092 (class 2606 OID 893652)
-- Name: financial_settings financial_settings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.financial_settings
    ADD CONSTRAINT financial_settings_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8093 (class 2606 OID 893657)
-- Name: fix_adjustments fix_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.fix_adjustments
    ADD CONSTRAINT fix_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 7934 (class 2606 OID 892862)
-- Name: appointment_bookings fk_appointment_bookings_chairs; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.appointment_bookings
    ADD CONSTRAINT fk_appointment_bookings_chairs FOREIGN KEY (seated_chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8339 (class 2606 OID 894887)
-- Name: selected_appointment_templates_office_day_calendar_view fk_appointment_template; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates_office_day_calendar_view
    ADD CONSTRAINT fk_appointment_template FOREIGN KEY (appointment_template_id) REFERENCES vaxiom.appointment_templates(id);


--
-- TOC entry 7957 (class 2606 OID 892977)
-- Name: benefit_saving_account_values fk_benefit_saving_account_values_employee_beneficiary_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.benefit_saving_account_values
    ADD CONSTRAINT fk_benefit_saving_account_values_employee_beneficiary_group1 FOREIGN KEY (employee_beneficiary_group_id) REFERENCES vaxiom.employee_beneficiary_group(id);


--
-- TOC entry 7958 (class 2606 OID 892982)
-- Name: benefit_saving_account_values fk_benefit_saving_accout_values_employee_benefit1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.benefit_saving_account_values
    ADD CONSTRAINT fk_benefit_saving_accout_values_employee_benefit1 FOREIGN KEY (employee_benefit_id) REFERENCES vaxiom.employee_benefit(id);


--
-- TOC entry 7959 (class 2606 OID 892987)
-- Name: benefit_saving_account_values fk_benefit_saving_accout_values_insurance_plan_saving_account1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.benefit_saving_account_values
    ADD CONSTRAINT fk_benefit_saving_accout_values_insurance_plan_saving_account1 FOREIGN KEY (insurance_plan_saving_account_id) REFERENCES vaxiom.insurance_plan_saving_account(id);


--
-- TOC entry 8103 (class 2606 OID 893707)
-- Name: insurance_claims_setup fk_billing_entity; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_billing_entity FOREIGN KEY (billing_entity_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 7994 (class 2606 OID 893162)
-- Name: contact_website fk_contact_website_contact_methods1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.contact_website
    ADD CONSTRAINT fk_contact_website_contact_methods1 FOREIGN KEY (id) REFERENCES vaxiom.contact_methods(id);


--
-- TOC entry 8037 (class 2606 OID 893377)
-- Name: employee_beneficiary fk_employee_benebitiary_employee_benefitiary_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary
    ADD CONSTRAINT fk_employee_benebitiary_employee_benefitiary_group1 FOREIGN KEY (employee_beneficiary_group_id) REFERENCES vaxiom.employee_beneficiary_group(id);


--
-- TOC entry 8038 (class 2606 OID 893382)
-- Name: employee_beneficiary fk_employee_benebitiary_employee_enrollment_person1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary
    ADD CONSTRAINT fk_employee_benebitiary_employee_enrollment_person1 FOREIGN KEY (employee_enrollment_person_id) REFERENCES vaxiom.employee_enrollment_person(id);


--
-- TOC entry 8040 (class 2606 OID 893392)
-- Name: employee_benefit fk_employee_benefit_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_benefit
    ADD CONSTRAINT fk_employee_benefit_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8041 (class 2606 OID 893397)
-- Name: employee_benefit fk_employee_benefit_insurance_palan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_benefit
    ADD CONSTRAINT fk_employee_benefit_insurance_palan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8039 (class 2606 OID 893387)
-- Name: employee_beneficiary_group fk_employee_benefitiary_group_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_beneficiary_group
    ADD CONSTRAINT fk_employee_benefitiary_group_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8042 (class 2606 OID 893402)
-- Name: employee_dependent fk_employee_dependent_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_dependent
    ADD CONSTRAINT fk_employee_dependent_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8043 (class 2606 OID 893407)
-- Name: employee_dependent fk_employee_dependet_employee_enrollment_person1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_dependent
    ADD CONSTRAINT fk_employee_dependet_employee_enrollment_person1 FOREIGN KEY (employee_enrollment_person_id) REFERENCES vaxiom.employee_enrollment_person(id);


--
-- TOC entry 8070 (class 2606 OID 893542)
-- Name: employment_contract_emergency_contact fk_employee_emergency_contact_employment_contracts1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_emergency_contact
    ADD CONSTRAINT fk_employee_emergency_contact_employment_contracts1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8047 (class 2606 OID 893427)
-- Name: employee_enrollment_attachment fk_employee_enrollment_attachment_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_attachment
    ADD CONSTRAINT fk_employee_enrollment_attachment_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8044 (class 2606 OID 893412)
-- Name: employee_enrollment fk_employee_enrollment_employment_contracts1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment
    ADD CONSTRAINT fk_employee_enrollment_employment_contracts1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8045 (class 2606 OID 893417)
-- Name: employee_enrollment fk_employee_enrollment_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment
    ADD CONSTRAINT fk_employee_enrollment_enrollment1 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id);


--
-- TOC entry 8046 (class 2606 OID 893422)
-- Name: employee_enrollment fk_employee_enrollment_enrollment_form_source_event1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment
    ADD CONSTRAINT fk_employee_enrollment_enrollment_form_source_event1 FOREIGN KEY (enrollment_form_source_event_id) REFERENCES vaxiom.enrollment_form_source_event(id);


--
-- TOC entry 8048 (class 2606 OID 893432)
-- Name: employee_enrollment_event fk_employee_enrollment_event_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_event
    ADD CONSTRAINT fk_employee_enrollment_event_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8049 (class 2606 OID 893437)
-- Name: employee_enrollment_person fk_employee_enrollment_person_employee_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_enrollment_person
    ADD CONSTRAINT fk_employee_enrollment_person_employee_enrollment1 FOREIGN KEY (employee_enrollment_id) REFERENCES vaxiom.employee_enrollment(id);


--
-- TOC entry 8050 (class 2606 OID 893442)
-- Name: employee_insured fk_employee_insured_employee_benefit1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_insured
    ADD CONSTRAINT fk_employee_insured_employee_benefit1 FOREIGN KEY (employee_benefit_id) REFERENCES vaxiom.employee_benefit(id);


--
-- TOC entry 8051 (class 2606 OID 893447)
-- Name: employee_insured fk_employee_insured_employee_benefitiary_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_insured
    ADD CONSTRAINT fk_employee_insured_employee_benefitiary_group1 FOREIGN KEY (employee_beneficiary_group_id) REFERENCES vaxiom.employee_beneficiary_group(id);


--
-- TOC entry 8052 (class 2606 OID 893452)
-- Name: employee_insured fk_employee_insured_employee_dependet1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employee_insured
    ADD CONSTRAINT fk_employee_insured_employee_dependet1 FOREIGN KEY (employee_dependent_id) REFERENCES vaxiom.employee_dependent(id);


--
-- TOC entry 8072 (class 2606 OID 893552)
-- Name: employment_contract_job_status_log fk_employement_contract_job_status_log_employment_contracts1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_job_status_log
    ADD CONSTRAINT fk_employement_contract_job_status_log_employment_contracts1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8170 (class 2606 OID 894042)
-- Name: legal_entity fk_employer_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity
    ADD CONSTRAINT fk_employer_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8071 (class 2606 OID 893547)
-- Name: employment_contract_enrollment_group_log fk_employment_contract_enrollment_group_log_employment_contra1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_enrollment_group_log
    ADD CONSTRAINT fk_employment_contract_enrollment_group_log_employment_contra1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8079 (class 2606 OID 893587)
-- Name: employment_contract_salary_log fk_employment_contract_salary_log_employment_contra1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_salary_log
    ADD CONSTRAINT fk_employment_contract_salary_log_employment_contra1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8063 (class 2606 OID 893507)
-- Name: employment_contracts fk_employment_contracts_enrollment_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_enrollment_group1 FOREIGN KEY (enrollment_group_id) REFERENCES vaxiom.enrollment_group(id);


--
-- TOC entry 8064 (class 2606 OID 893512)
-- Name: employment_contracts fk_employment_contracts_jobtitle1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_jobtitle1 FOREIGN KEY (jobtitle_id) REFERENCES vaxiom.jobtitle(id);


--
-- TOC entry 8065 (class 2606 OID 893517)
-- Name: employment_contracts fk_employment_contracts_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8066 (class 2606 OID 893522)
-- Name: employment_contracts fk_employment_contracts_legal_entity_billing_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_legal_entity_billing_group1 FOREIGN KEY (billing_group_id) REFERENCES vaxiom.legal_entity_billing_group(id);


--
-- TOC entry 8069 (class 2606 OID 893537)
-- Name: employment_contracts fk_employment_contracts_sys_organization_address1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_sys_organization_address1 FOREIGN KEY (physical_address_id) REFERENCES vaxiom.sys_organization_address(id);


--
-- TOC entry 8067 (class 2606 OID 893527)
-- Name: employment_contracts fk_employment_contracts_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_sys_organizations1 FOREIGN KEY (department_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8068 (class 2606 OID 893532)
-- Name: employment_contracts fk_employment_contracts_sys_organizations2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contracts
    ADD CONSTRAINT fk_employment_contracts_sys_organizations2 FOREIGN KEY (division_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8080 (class 2606 OID 893592)
-- Name: enrollment fk_enrollment_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.enrollment
    ADD CONSTRAINT fk_enrollment_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8140 (class 2606 OID 893892)
-- Name: insurance_plan_fixed_limit fk_fixedlimits_insurance_limits1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_fixed_limit
    ADD CONSTRAINT fk_fixedlimits_insurance_limits1 FOREIGN KEY (insurance_limits_id) REFERENCES vaxiom.insurance_plan_limits(id);


--
-- TOC entry 8147 (class 2606 OID 893927)
-- Name: insurance_plan_saving_account fk_insurace_plan_saving_account_insurance_plan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_saving_account
    ADD CONSTRAINT fk_insurace_plan_saving_account_insurance_plan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8104 (class 2606 OID 893712)
-- Name: insurance_claims_setup fk_insurance_claims_setup_legal_entity_npi1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_legal_entity_npi1 FOREIGN KEY (legal_entity_npi_id) REFERENCES vaxiom.legal_entity_npi(id);


--
-- TOC entry 8105 (class 2606 OID 893717)
-- Name: insurance_claims_setup fk_insurance_claims_setup_legal_entity_tin1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_legal_entity_tin1 FOREIGN KEY (tin_id) REFERENCES vaxiom.legal_entity_tin(id);


--
-- TOC entry 8107 (class 2606 OID 893727)
-- Name: insurance_claims_setup fk_insurance_claims_setup_sys_organization_address1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_sys_organization_address1 FOREIGN KEY (sys_organization_address_id) REFERENCES vaxiom.sys_organization_address(id);


--
-- TOC entry 8108 (class 2606 OID 893732)
-- Name: insurance_claims_setup fk_insurance_claims_setup_sys_organization_contact_method1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_sys_organization_contact_method1 FOREIGN KEY (sys_organization_contact_method_id) REFERENCES vaxiom.sys_organization_contact_method(id);


--
-- TOC entry 8106 (class 2606 OID 893722)
-- Name: insurance_claims_setup fk_insurance_claims_setup_sys_organizations; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_claims_setup
    ADD CONSTRAINT fk_insurance_claims_setup_sys_organizations FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8145 (class 2606 OID 893917)
-- Name: insurance_plan_limits fk_insurance_limits_insurance_palan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_limits
    ADD CONSTRAINT fk_insurance_limits_insurance_palan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8131 (class 2606 OID 893847)
-- Name: insurance_plan_age_and_gender_banded_rate fk_insurance_plan_age_and_gender_banded_rate_insurance_plan_r1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_and_gender_banded_rate
    ADD CONSTRAINT fk_insurance_plan_age_and_gender_banded_rate_insurance_plan_r1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8132 (class 2606 OID 893852)
-- Name: insurance_plan_age_banded_rate fk_insurance_plan_age_banded_rate_insurance_plan_rates1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_age_banded_rate
    ADD CONSTRAINT fk_insurance_plan_age_banded_rate_insurance_plan_rates1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8133 (class 2606 OID 893857)
-- Name: insurance_plan_benefit_amount fk_insurance_plan_benefit_amount_insurance_plan2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_benefit_amount
    ADD CONSTRAINT fk_insurance_plan_benefit_amount_insurance_plan2 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8134 (class 2606 OID 893862)
-- Name: insurance_plan_complex_benefit_amount fk_insurance_plan_complex_benefit_amount_insurance_plan_benef2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_complex_benefit_amount
    ADD CONSTRAINT fk_insurance_plan_complex_benefit_amount_insurance_plan_benef2 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_benefit_amount(id);


--
-- TOC entry 8135 (class 2606 OID 893867)
-- Name: insurance_plan_complex_benefit_amount_item fk_insurance_plan_complex_benefit_amount_item_insurance_plan_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_complex_benefit_amount_item
    ADD CONSTRAINT fk_insurance_plan_complex_benefit_amount_item_insurance_plan_1 FOREIGN KEY (insurance_plan_complex_benefit_amount_id) REFERENCES vaxiom.insurance_plan_complex_benefit_amount(id);


--
-- TOC entry 8136 (class 2606 OID 893872)
-- Name: insurance_plan_composition_detailed_rate fk_insurance_plan_composition_detailed_rate_insurance_plan_rate; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_composition_detailed_rate
    ADD CONSTRAINT fk_insurance_plan_composition_detailed_rate_insurance_plan_rate FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8137 (class 2606 OID 893877)
-- Name: insurance_plan_composition_rate fk_insurance_plan_composition_rate_insurance_plan_rates1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_composition_rate
    ADD CONSTRAINT fk_insurance_plan_composition_rate_insurance_plan_rates1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8141 (class 2606 OID 893897)
-- Name: insurance_plan_has_enrollment fk_insurance_plan_has_enrollment_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment
    ADD CONSTRAINT fk_insurance_plan_has_enrollment_enrollment1 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id);


--
-- TOC entry 8143 (class 2606 OID 893907)
-- Name: insurance_plan_has_enrollment_has_enrollment_group fk_insurance_plan_has_enrollment_has_enrollment_group_enrollm1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment_has_enrollment_group
    ADD CONSTRAINT fk_insurance_plan_has_enrollment_has_enrollment_group_enrollm1 FOREIGN KEY (enrollment_group_id) REFERENCES vaxiom.enrollment_group(id);


--
-- TOC entry 8144 (class 2606 OID 893912)
-- Name: insurance_plan_has_enrollment_has_enrollment_group fk_insurance_plan_has_enrollment_has_enrollment_group_insuran1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment_has_enrollment_group
    ADD CONSTRAINT fk_insurance_plan_has_enrollment_has_enrollment_group_insuran1 FOREIGN KEY (insurance_plan_has_enrollment_id) REFERENCES vaxiom.insurance_plan_has_enrollment(id);


--
-- TOC entry 8142 (class 2606 OID 893902)
-- Name: insurance_plan_has_enrollment fk_insurance_plan_has_enrollment_insurance_plan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_has_enrollment
    ADD CONSTRAINT fk_insurance_plan_has_enrollment_insurance_plan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8146 (class 2606 OID 893922)
-- Name: insurance_plan_rates fk_insurance_plan_rates_insurance_plan; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_rates
    ADD CONSTRAINT fk_insurance_plan_rates_insurance_plan FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8149 (class 2606 OID 893937)
-- Name: insurance_plan_simple_benefit_amount fk_insurance_plan_simple_benefit_amount_insurance_plan_benefi1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_simple_benefit_amount
    ADD CONSTRAINT fk_insurance_plan_simple_benefit_amount_insurance_plan_benefi1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_benefit_amount(id);


--
-- TOC entry 8151 (class 2606 OID 893947)
-- Name: insurance_plan_summary_fields fk_insurance_plan_summary_fields_insurance_plan1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_summary_fields
    ADD CONSTRAINT fk_insurance_plan_summary_fields_insurance_plan1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.insurance_plan(id);


--
-- TOC entry 8152 (class 2606 OID 893952)
-- Name: insurance_plan_term_disability_benefit_amount fk_insurance_plan_term_disabilility_benefit_amount_insurance_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_term_disability_benefit_amount
    ADD CONSTRAINT fk_insurance_plan_term_disabilility_benefit_amount_insurance_1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_benefit_amount(id);


--
-- TOC entry 8125 (class 2606 OID 893817)
-- Name: insurance_payment_corrections fk_insurancepaymentcorrectionscorrectiontypeid_correctiontypesi; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_corrections
    ADD CONSTRAINT fk_insurancepaymentcorrectionscorrectiontypeid_correctiontypesi FOREIGN KEY (correction_type_id) REFERENCES vaxiom.correction_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8126 (class 2606 OID 893822)
-- Name: insurance_payment_corrections fk_insurancepaymentcorrectionsid_insurancepaymentmovementsid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_corrections
    ADD CONSTRAINT fk_insurancepaymentcorrectionsid_insurancepaymentmovementsid FOREIGN KEY (id) REFERENCES vaxiom.insurance_payment_movements(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8120 (class 2606 OID 893792)
-- Name: insurance_payments fk_insurancepaymentid_insurancecompanies; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payments
    ADD CONSTRAINT fk_insurancepaymentid_insurancecompanies FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8127 (class 2606 OID 893827)
-- Name: insurance_payment_movements fk_insurancepaymentmovementid_insurancepaymentid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_movements
    ADD CONSTRAINT fk_insurancepaymentmovementid_insurancepaymentid FOREIGN KEY (insurance_payment_id) REFERENCES vaxiom.insurance_payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8128 (class 2606 OID 893832)
-- Name: insurance_payment_refunds fk_insurancepaymentrefundsid_insurancepaymentmovementsid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_refunds
    ADD CONSTRAINT fk_insurancepaymentrefundsid_insurancepaymentmovementsid FOREIGN KEY (id) REFERENCES vaxiom.insurance_payment_movements(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8129 (class 2606 OID 893837)
-- Name: insurance_payment_transfers fk_insurancepaymenttransferid_insurancepaymentmovementsid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_transfers
    ADD CONSTRAINT fk_insurancepaymenttransferid_insurancepaymentmovementsid FOREIGN KEY (id) REFERENCES vaxiom.insurance_payment_movements(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8130 (class 2606 OID 893842)
-- Name: insurance_payment_transfers fk_insurancepaymenttransferpaymentid_paymentid; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_transfers
    ADD CONSTRAINT fk_insurancepaymenttransferpaymentid_paymentid FOREIGN KEY (payment_id) REFERENCES vaxiom.payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8158 (class 2606 OID 893982)
-- Name: insured_benefit_values fk_insured_rate_amount_employee_insured1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured_benefit_values
    ADD CONSTRAINT fk_insured_rate_amount_employee_insured1 FOREIGN KEY (employee_insured_id) REFERENCES vaxiom.employee_insured(id);


--
-- TOC entry 8166 (class 2606 OID 894022)
-- Name: jobtitle_group fk_jobtitle_group_eeo1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle_group
    ADD CONSTRAINT fk_jobtitle_group_eeo1 FOREIGN KEY (eeo_id) REFERENCES vaxiom.eeo(id);


--
-- TOC entry 8164 (class 2606 OID 894012)
-- Name: jobtitle fk_jobtitle_jobtitle_group1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle
    ADD CONSTRAINT fk_jobtitle_jobtitle_group1 FOREIGN KEY (jobtitle_group_id) REFERENCES vaxiom.jobtitle_group(id);


--
-- TOC entry 8165 (class 2606 OID 894017)
-- Name: jobtitle fk_jobtitle_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.jobtitle
    ADD CONSTRAINT fk_jobtitle_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8174 (class 2606 OID 894062)
-- Name: legal_entity_address_attachment fk_legal_entity_address_attachment_legal_entity_address1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_address_attachment
    ADD CONSTRAINT fk_legal_entity_address_attachment_legal_entity_address1 FOREIGN KEY (legal_entity_address_id) REFERENCES vaxiom.legal_entity_address(id);


--
-- TOC entry 8173 (class 2606 OID 894057)
-- Name: legal_entity_address fk_legal_entity_address_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_address
    ADD CONSTRAINT fk_legal_entity_address_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8176 (class 2606 OID 894072)
-- Name: legal_entity_billing_group_has_sys_organizations fk_legal_entity_billing_group_has_sys_organizations_legal_enti1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group_has_sys_organizations
    ADD CONSTRAINT fk_legal_entity_billing_group_has_sys_organizations_legal_enti1 FOREIGN KEY (legal_entity_billing_group_id) REFERENCES vaxiom.legal_entity_billing_group(id);


--
-- TOC entry 8177 (class 2606 OID 894077)
-- Name: legal_entity_billing_group_has_sys_organizations fk_legal_entity_billing_group_has_sys_organizations_sys_organi1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group_has_sys_organizations
    ADD CONSTRAINT fk_legal_entity_billing_group_has_sys_organizations_sys_organi1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8175 (class 2606 OID 894067)
-- Name: legal_entity_billing_group fk_legal_entity_billing_group_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_billing_group
    ADD CONSTRAINT fk_legal_entity_billing_group_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8171 (class 2606 OID 894047)
-- Name: legal_entity fk_legal_entity_county1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity
    ADD CONSTRAINT fk_legal_entity_county1 FOREIGN KEY (county_id) REFERENCES vaxiom.county(id);


--
-- TOC entry 8178 (class 2606 OID 894082)
-- Name: legal_entity_dba fk_legal_entity_dba_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_dba
    ADD CONSTRAINT fk_legal_entity_dba_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8168 (class 2606 OID 894032)
-- Name: legal_entities_participating_enrollment fk_legal_entity_has_enrollment_enrollment1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entities_participating_enrollment
    ADD CONSTRAINT fk_legal_entity_has_enrollment_enrollment1 FOREIGN KEY (enrollment_id) REFERENCES vaxiom.enrollment(id);


--
-- TOC entry 8169 (class 2606 OID 894037)
-- Name: legal_entities_participating_enrollment fk_legal_entity_has_enrollment_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entities_participating_enrollment
    ADD CONSTRAINT fk_legal_entity_has_enrollment_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8182 (class 2606 OID 894102)
-- Name: legal_entity_insurance_benefit_has_division fk_legal_entity_insurance_benefit_has_sys_organizations_legal1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit_has_division
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_has_sys_organizations_legal1 FOREIGN KEY (legal_entity_insurance_benefit_id) REFERENCES vaxiom.legal_entity_insurance_benefit(id);


--
-- TOC entry 8183 (class 2606 OID 894107)
-- Name: legal_entity_insurance_benefit_has_division fk_legal_entity_insurance_benefit_has_sys_organizations_sys_o1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit_has_division
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_has_sys_organizations_sys_o1 FOREIGN KEY (division_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8179 (class 2606 OID 894087)
-- Name: legal_entity_insurance_benefit fk_legal_entity_insurance_benefit_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_legal_entity1 FOREIGN KEY (benefits_holder) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8180 (class 2606 OID 894092)
-- Name: legal_entity_insurance_benefit fk_legal_entity_insurance_benefit_legal_entity2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_legal_entity2 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8181 (class 2606 OID 894097)
-- Name: legal_entity_insurance_benefit fk_legal_entity_insurance_benefit_sys_users1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_insurance_benefit
    ADD CONSTRAINT fk_legal_entity_insurance_benefit_sys_users1 FOREIGN KEY (broker_id) REFERENCES vaxiom.sys_users(id);


--
-- TOC entry 8172 (class 2606 OID 894052)
-- Name: legal_entity fk_legal_entity_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity
    ADD CONSTRAINT fk_legal_entity_legal_entity1 FOREIGN KEY (parent_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8184 (class 2606 OID 894112)
-- Name: legal_entity_npi fk_legal_entity_npi_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_npi
    ADD CONSTRAINT fk_legal_entity_npi_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8185 (class 2606 OID 894117)
-- Name: legal_entity_operational_structures fk_legal_entity_operational_structures_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_operational_structures
    ADD CONSTRAINT fk_legal_entity_operational_structures_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8186 (class 2606 OID 894122)
-- Name: legal_entity_owner fk_legal_entity_owner_legal_entity1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_owner
    ADD CONSTRAINT fk_legal_entity_owner_legal_entity1 FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8340 (class 2606 OID 894892)
-- Name: selected_appointment_templates_office_day_calendar_view fk_location; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates_office_day_calendar_view
    ADD CONSTRAINT fk_location FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8195 (class 2606 OID 894167)
-- Name: medical_history_patient_relatives_crowding fk_medicalhistoryid_medicalhistory; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_patient_relatives_crowding
    ADD CONSTRAINT fk_medicalhistoryid_medicalhistory FOREIGN KEY (medical_history_id) REFERENCES vaxiom.medical_history(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8196 (class 2606 OID 894172)
-- Name: merchant_reports fk_merchant_reports_sys_organizations; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.merchant_reports
    ADD CONSTRAINT fk_merchant_reports_sys_organizations FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8250 (class 2606 OID 894442)
-- Name: payments fk_patient_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT fk_patient_id FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8121 (class 2606 OID 893797)
-- Name: insurance_payments fk_paymenttypeid_paymenttypes; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payments
    ADD CONSTRAINT fk_paymenttypeid_paymenttypes FOREIGN KEY (payment_type_id) REFERENCES vaxiom.payment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8276 (class 2606 OID 894572)
-- Name: permission_user_role_location fk_permission_user_role_location_permissions_user_role; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permission_user_role_location
    ADD CONSTRAINT fk_permission_user_role_location_permissions_user_role FOREIGN KEY (permission_user_role_id) REFERENCES vaxiom.permissions_user_role(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 8277 (class 2606 OID 894577)
-- Name: permission_user_role_location fk_permission_user_role_location_sys_organizations; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permission_user_role_location
    ADD CONSTRAINT fk_permission_user_role_location_sys_organizations FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 8278 (class 2606 OID 894582)
-- Name: permission_user_role_location fk_permission_user_role_location_sys_users; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permission_user_role_location
    ADD CONSTRAINT fk_permission_user_role_location_sys_users FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 8299 (class 2606 OID 894687)
-- Name: provider_license fk_provider_license_employment_contracts1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_license
    ADD CONSTRAINT fk_provider_license_employment_contracts1 FOREIGN KEY (employment_contracts_id) REFERENCES vaxiom.employment_contracts(id);


--
-- TOC entry 8300 (class 2606 OID 894692)
-- Name: provider_license fk_provider_license_provider_specialty1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_license
    ADD CONSTRAINT fk_provider_license_provider_specialty1 FOREIGN KEY (provider_specialty_id) REFERENCES vaxiom.provider_specialty(id);


--
-- TOC entry 8148 (class 2606 OID 893932)
-- Name: insurance_plan_separate_age_banded_rate fk_separate_age_banded_rate_insurance_plan_rates1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_separate_age_banded_rate
    ADD CONSTRAINT fk_separate_age_banded_rate_insurance_plan_rates1 FOREIGN KEY (id) REFERENCES vaxiom.insurance_plan_rates(id);


--
-- TOC entry 8150 (class 2606 OID 893942)
-- Name: insurance_plan_slider_limit fk_slider_limits_insurance_limits1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_slider_limit
    ADD CONSTRAINT fk_slider_limits_insurance_limits1 FOREIGN KEY (insurance_limits_id) REFERENCES vaxiom.insurance_plan_limits(id);


--
-- TOC entry 8352 (class 2606 OID 894952)
-- Name: sys_organization_address fk_sys_organisation_address_contact_postal_addresses1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address
    ADD CONSTRAINT fk_sys_organisation_address_contact_postal_addresses1 FOREIGN KEY (contact_postal_addresses_id) REFERENCES vaxiom.contact_postal_addresses(id);


--
-- TOC entry 8353 (class 2606 OID 894957)
-- Name: sys_organization_address_has_sys_organizations fk_sys_organisation_address_has_sys_organizations_sys_organis1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address_has_sys_organizations
    ADD CONSTRAINT fk_sys_organisation_address_has_sys_organizations_sys_organis1 FOREIGN KEY (sys_organisation_address_id) REFERENCES vaxiom.sys_organization_address(id);


--
-- TOC entry 8354 (class 2606 OID 894962)
-- Name: sys_organization_address_has_sys_organizations fk_sys_organisation_address_has_sys_organizations_sys_organiz1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_address_has_sys_organizations
    ADD CONSTRAINT fk_sys_organisation_address_has_sys_organizations_sys_organiz1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8356 (class 2606 OID 894972)
-- Name: sys_organization_contact_method fk_sys_organisation_contact_method_contact_methods1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_contact_method
    ADD CONSTRAINT fk_sys_organisation_contact_method_contact_methods1 FOREIGN KEY (contact_methods_id) REFERENCES vaxiom.contact_methods(id);


--
-- TOC entry 8357 (class 2606 OID 894977)
-- Name: sys_organization_contact_method fk_sys_organisation_contact_method_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_contact_method
    ADD CONSTRAINT fk_sys_organisation_contact_method_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8358 (class 2606 OID 894982)
-- Name: sys_organization_department_data fk_sys_organisation_departament_data_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_department_data
    ADD CONSTRAINT fk_sys_organisation_departament_data_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8359 (class 2606 OID 894987)
-- Name: sys_organization_division_data fk_sys_organisation_devision_data_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_data
    ADD CONSTRAINT fk_sys_organisation_devision_data_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8355 (class 2606 OID 894967)
-- Name: sys_organization_attachment fk_sys_organization_attachment_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_attachment
    ADD CONSTRAINT fk_sys_organization_attachment_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8360 (class 2606 OID 894992)
-- Name: sys_organization_division_data fk_sys_organization_division_data_legal_entity_npi1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_data
    ADD CONSTRAINT fk_sys_organization_division_data_legal_entity_npi1 FOREIGN KEY (legal_entity_npi_id) REFERENCES vaxiom.legal_entity_npi(id);


--
-- TOC entry 8364 (class 2606 OID 895012)
-- Name: sys_organization_location_group_has_sys_organizations fk_sys_organization_location_group_has_sys_organizations_sys_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group_has_sys_organizations
    ADD CONSTRAINT fk_sys_organization_location_group_has_sys_organizations_sys_1 FOREIGN KEY (sys_organization_location_group_id) REFERENCES vaxiom.sys_organization_location_group(id);


--
-- TOC entry 8365 (class 2606 OID 895017)
-- Name: sys_organization_location_group_has_sys_organizations fk_sys_organization_location_group_has_sys_organizations_sys_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group_has_sys_organizations
    ADD CONSTRAINT fk_sys_organization_location_group_has_sys_organizations_sys_2 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8363 (class 2606 OID 895007)
-- Name: sys_organization_location_group fk_sys_organization_location_group_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_location_group
    ADD CONSTRAINT fk_sys_organization_location_group_sys_organizations1 FOREIGN KEY (sys_organizations_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8361 (class 2606 OID 894997)
-- Name: sys_organization_division_has_department fk_sys_organizations_has_sys_organizations_sys_organizations1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_has_department
    ADD CONSTRAINT fk_sys_organizations_has_sys_organizations_sys_organizations1 FOREIGN KEY (division_tree_node_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8362 (class 2606 OID 895002)
-- Name: sys_organization_division_has_department fk_sys_organizations_has_sys_organizations_sys_organizations2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_division_has_department
    ADD CONSTRAINT fk_sys_organizations_has_sys_organizations_sys_organizations2 FOREIGN KEY (department_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8341 (class 2606 OID 894897)
-- Name: selected_appointment_templates_office_day_calendar_view fk_user; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates_office_day_calendar_view
    ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id);


--
-- TOC entry 8094 (class 2606 OID 893662)
-- Name: image_series image_series_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.image_series
    ADD CONSTRAINT image_series_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8095 (class 2606 OID 893667)
-- Name: image_series image_series_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.image_series
    ADD CONSTRAINT image_series_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.image_series_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8162 (class 2606 OID 894002)
-- Name: in_network_discounts in_network_discounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.in_network_discounts
    ADD CONSTRAINT in_network_discounts_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8163 (class 2606 OID 894007)
-- Name: in_network_discounts in_network_discounts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.in_network_discounts
    ADD CONSTRAINT in_network_discounts_ibfk_2 FOREIGN KEY (network_sheet_fee_id) REFERENCES vaxiom.network_sheet_fee(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8096 (class 2606 OID 893672)
-- Name: installment_charges installment_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.installment_charges
    ADD CONSTRAINT installment_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8097 (class 2606 OID 893677)
-- Name: installment_charges installment_charges_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.installment_charges
    ADD CONSTRAINT installment_charges_ibfk_2 FOREIGN KEY (payment_plan_id) REFERENCES vaxiom.payment_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8098 (class 2606 OID 893682)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_1 FOREIGN KEY (contact_postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8099 (class 2606 OID 893687)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_2 FOREIGN KEY (contact_phone_id) REFERENCES vaxiom.contact_phones(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8100 (class 2606 OID 893692)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_3 FOREIGN KEY (contact_email_id) REFERENCES vaxiom.contact_emails(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8101 (class 2606 OID 893697)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_4 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8102 (class 2606 OID 893702)
-- Name: insurance_billing_centers insurance_billing_centers_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_billing_centers
    ADD CONSTRAINT insurance_billing_centers_ibfk_5 FOREIGN KEY (master_id) REFERENCES vaxiom.insurance_billing_centers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8109 (class 2606 OID 893737)
-- Name: insurance_code_val insurance_code_val_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_code_val
    ADD CONSTRAINT insurance_code_val_ibfk_1 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8110 (class 2606 OID 893742)
-- Name: insurance_code_val insurance_code_val_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_code_val
    ADD CONSTRAINT insurance_code_val_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8111 (class 2606 OID 893747)
-- Name: insurance_code_val insurance_code_val_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_code_val
    ADD CONSTRAINT insurance_code_val_ibfk_3 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8117 (class 2606 OID 893777)
-- Name: insurance_company_payments insurance_companies_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_payments
    ADD CONSTRAINT insurance_companies_fk FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8112 (class 2606 OID 893752)
-- Name: insurance_companies insurance_companies_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_1 FOREIGN KEY (am_phone_number_id) REFERENCES vaxiom.contact_phones(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8113 (class 2606 OID 893757)
-- Name: insurance_companies insurance_companies_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_2 FOREIGN KEY (am_email_address_id) REFERENCES vaxiom.contact_emails(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8114 (class 2606 OID 893762)
-- Name: insurance_companies insurance_companies_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_3 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8115 (class 2606 OID 893767)
-- Name: insurance_companies insurance_companies_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_4 FOREIGN KEY (postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8116 (class 2606 OID 893772)
-- Name: insurance_companies insurance_companies_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_companies
    ADD CONSTRAINT insurance_companies_ibfk_5 FOREIGN KEY (master_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8118 (class 2606 OID 893782)
-- Name: insurance_company_phone_associations insurance_company_phone_associations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_phone_associations
    ADD CONSTRAINT insurance_company_phone_associations_ibfk_1 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8119 (class 2606 OID 893787)
-- Name: insurance_company_phone_associations insurance_company_phone_associations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_company_phone_associations
    ADD CONSTRAINT insurance_company_phone_associations_ibfk_2 FOREIGN KEY (contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8122 (class 2606 OID 893802)
-- Name: insurance_payment_accounts insurance_payment_accounts_fk_pay_acount; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_accounts
    ADD CONSTRAINT insurance_payment_accounts_fk_pay_acount FOREIGN KEY (id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8123 (class 2606 OID 893807)
-- Name: insurance_payment_accounts insurance_payment_accounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_accounts
    ADD CONSTRAINT insurance_payment_accounts_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8124 (class 2606 OID 893812)
-- Name: insurance_payment_accounts insurance_payment_accounts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_payment_accounts
    ADD CONSTRAINT insurance_payment_accounts_ibfk_2 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8138 (class 2606 OID 893882)
-- Name: insurance_plan_employers insurance_plan_employers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_employers
    ADD CONSTRAINT insurance_plan_employers_ibfk_1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.patient_insurance_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8139 (class 2606 OID 893887)
-- Name: insurance_plan_employers insurance_plan_employers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_plan_employers
    ADD CONSTRAINT insurance_plan_employers_ibfk_2 FOREIGN KEY (employer_id) REFERENCES vaxiom.employers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8153 (class 2606 OID 893957)
-- Name: insurance_subscriptions insurance_subscriptions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_subscriptions
    ADD CONSTRAINT insurance_subscriptions_ibfk_1 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.patient_insurance_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8154 (class 2606 OID 893962)
-- Name: insurance_subscriptions insurance_subscriptions_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_subscriptions
    ADD CONSTRAINT insurance_subscriptions_ibfk_2 FOREIGN KEY (employer_id) REFERENCES vaxiom.employers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8155 (class 2606 OID 893967)
-- Name: insurance_subscriptions insurance_subscriptions_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insurance_subscriptions
    ADD CONSTRAINT insurance_subscriptions_ibfk_3 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8156 (class 2606 OID 893972)
-- Name: insured insured_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured
    ADD CONSTRAINT insured_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8157 (class 2606 OID 893977)
-- Name: insured insured_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.insured
    ADD CONSTRAINT insured_ibfk_2 FOREIGN KEY (insurance_subscription_id) REFERENCES vaxiom.insurance_subscriptions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8159 (class 2606 OID 893987)
-- Name: invoice_export_queue invoice_export_queue_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_export_queue
    ADD CONSTRAINT invoice_export_queue_ibfk_2 FOREIGN KEY (account_id) REFERENCES vaxiom.third_party_account(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8160 (class 2606 OID 893992)
-- Name: invoice_import_export_log invoice_import_export_log_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_import_export_log
    ADD CONSTRAINT invoice_import_export_log_ibfk_1 FOREIGN KEY (account_id) REFERENCES vaxiom.third_party_account(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8161 (class 2606 OID 893997)
-- Name: invoice_import_export_log invoice_import_export_log_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.invoice_import_export_log
    ADD CONSTRAINT invoice_import_export_log_ibfk_3 FOREIGN KEY (id) REFERENCES vaxiom.audit_logs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8272 (class 2606 OID 894552)
-- Name: permissions_user_role_job_title jobtitle_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_job_title
    ADD CONSTRAINT jobtitle_fk FOREIGN KEY (job_title_id) REFERENCES vaxiom.jobtitle(id);


--
-- TOC entry 8167 (class 2606 OID 894027)
-- Name: late_fee_charges late_fee_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.late_fee_charges
    ADD CONSTRAINT late_fee_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8187 (class 2606 OID 894127)
-- Name: legal_entity_tin legal_entity_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.legal_entity_tin
    ADD CONSTRAINT legal_entity_id FOREIGN KEY (legal_entity_id) REFERENCES vaxiom.legal_entity(id);


--
-- TOC entry 8188 (class 2606 OID 894132)
-- Name: lightbarcalls lightbarcalls_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.lightbarcalls
    ADD CONSTRAINT lightbarcalls_ibfk_1 FOREIGN KEY (chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8189 (class 2606 OID 894137)
-- Name: lightbarcalls lightbarcalls_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.lightbarcalls
    ADD CONSTRAINT lightbarcalls_ibfk_2 FOREIGN KEY (booking_id) REFERENCES vaxiom.appointment_bookings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8190 (class 2606 OID 894142)
-- Name: lightbarcalls lightbarcalls_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.lightbarcalls
    ADD CONSTRAINT lightbarcalls_ibfk_3 FOREIGN KEY (resource_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8191 (class 2606 OID 894147)
-- Name: location_access_keys location_access_keys_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.location_access_keys
    ADD CONSTRAINT location_access_keys_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8074 (class 2606 OID 893562)
-- Name: employment_contract_location_group location_group_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.employment_contract_location_group
    ADD CONSTRAINT location_group_fk FOREIGN KEY (location_group_id) REFERENCES vaxiom.sys_organization_location_group(id);


--
-- TOC entry 8337 (class 2606 OID 894877)
-- Name: selected_appointment_templates location_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates
    ADD CONSTRAINT location_id FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id);


--
-- TOC entry 8192 (class 2606 OID 894152)
-- Name: medical_history medical_history_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history
    ADD CONSTRAINT medical_history_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8193 (class 2606 OID 894157)
-- Name: medical_history_medications medical_history_medications_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_medications
    ADD CONSTRAINT medical_history_medications_ibfk_1 FOREIGN KEY (medication_id) REFERENCES vaxiom.medications(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8194 (class 2606 OID 894162)
-- Name: medical_history_medications medical_history_medications_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.medical_history_medications
    ADD CONSTRAINT medical_history_medications_ibfk_2 FOREIGN KEY (medical_history_id) REFERENCES vaxiom.medical_history(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8197 (class 2606 OID 894177)
-- Name: migration_fix_adjustments migration_fix_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.migration_fix_adjustments
    ADD CONSTRAINT migration_fix_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8199 (class 2606 OID 894187)
-- Name: misc_fee_charge_templates misc_fee_charge_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.misc_fee_charge_templates
    ADD CONSTRAINT misc_fee_charge_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8198 (class 2606 OID 894182)
-- Name: misc_fee_charges misc_fee_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.misc_fee_charges
    ADD CONSTRAINT misc_fee_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8200 (class 2606 OID 894192)
-- Name: my_reports_columns my_reports_columns_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports_columns
    ADD CONSTRAINT my_reports_columns_ibfk_1 FOREIGN KEY (report_id) REFERENCES vaxiom.my_reports(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8201 (class 2606 OID 894197)
-- Name: my_reports_filters my_reports_filters_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.my_reports_filters
    ADD CONSTRAINT my_reports_filters_ibfk_1 FOREIGN KEY (report_id) REFERENCES vaxiom.my_reports(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8202 (class 2606 OID 894202)
-- Name: network_fee_sheets network_fee_sheets_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_fee_sheets
    ADD CONSTRAINT network_fee_sheets_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8203 (class 2606 OID 894207)
-- Name: network_fee_sheets network_fee_sheets_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_fee_sheets
    ADD CONSTRAINT network_fee_sheets_ibfk_2 FOREIGN KEY (employment_contract_id) REFERENCES vaxiom.employment_contracts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8204 (class 2606 OID 894212)
-- Name: network_sheet_fee network_sheet_fee_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_sheet_fee
    ADD CONSTRAINT network_sheet_fee_ibfk_1 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8205 (class 2606 OID 894217)
-- Name: network_sheet_fee network_sheet_fee_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.network_sheet_fee
    ADD CONSTRAINT network_sheet_fee_ibfk_2 FOREIGN KEY (network_fee_sheet_id) REFERENCES vaxiom.network_fee_sheets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8206 (class 2606 OID 894222)
-- Name: notebook_notes notebook_notes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.notebook_notes
    ADD CONSTRAINT notebook_notes_ibfk_1 FOREIGN KEY (notebook_id) REFERENCES vaxiom.notebooks(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8212 (class 2606 OID 894252)
-- Name: od_chair_break_hours od_chair_break_hours_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.od_chair_break_hours
    ADD CONSTRAINT od_chair_break_hours_ibfk_1 FOREIGN KEY (od_chair_id) REFERENCES vaxiom.office_day_chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8210 (class 2606 OID 894242)
-- Name: odt_chair_allocations odt_chair_allocations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chair_allocations
    ADD CONSTRAINT odt_chair_allocations_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8211 (class 2606 OID 894247)
-- Name: odt_chair_allocations odt_chair_allocations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chair_allocations
    ADD CONSTRAINT odt_chair_allocations_ibfk_2 FOREIGN KEY (odt_chair_id) REFERENCES vaxiom.odt_chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8207 (class 2606 OID 894227)
-- Name: odt_chairs odt_chairs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chairs
    ADD CONSTRAINT odt_chairs_ibfk_1 FOREIGN KEY (template_id) REFERENCES vaxiom.office_day_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8208 (class 2606 OID 894232)
-- Name: odt_chairs odt_chairs_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chairs
    ADD CONSTRAINT odt_chairs_ibfk_2 FOREIGN KEY (day_schedule_id) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8209 (class 2606 OID 894237)
-- Name: odt_chairs odt_chairs_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.odt_chairs
    ADD CONSTRAINT odt_chairs_ibfk_3 FOREIGN KEY (chair_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8216 (class 2606 OID 894272)
-- Name: office_day_chairs office_day_chairs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_chairs
    ADD CONSTRAINT office_day_chairs_ibfk_1 FOREIGN KEY (office_day_id) REFERENCES vaxiom.office_days(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8217 (class 2606 OID 894277)
-- Name: office_day_chairs office_day_chairs_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_chairs
    ADD CONSTRAINT office_day_chairs_ibfk_2 FOREIGN KEY (odt_chair_id) REFERENCES vaxiom.odt_chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8218 (class 2606 OID 894282)
-- Name: office_day_chairs office_day_chairs_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_chairs
    ADD CONSTRAINT office_day_chairs_ibfk_3 FOREIGN KEY (chair_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8219 (class 2606 OID 894287)
-- Name: office_day_templates office_day_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_day_templates
    ADD CONSTRAINT office_day_templates_ibfk_1 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8214 (class 2606 OID 894262)
-- Name: office_days office_days_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_days
    ADD CONSTRAINT office_days_ibfk_1 FOREIGN KEY (template_id) REFERENCES vaxiom.office_day_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8215 (class 2606 OID 894267)
-- Name: office_days office_days_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.office_days
    ADD CONSTRAINT office_days_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8213 (class 2606 OID 894257)
-- Name: offices offices_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.offices
    ADD CONSTRAINT offices_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8220 (class 2606 OID 894292)
-- Name: ortho_coverages ortho_coverages_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.ortho_coverages
    ADD CONSTRAINT ortho_coverages_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.tx_category_coverages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8221 (class 2606 OID 894297)
-- Name: ortho_insured ortho_insured_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.ortho_insured
    ADD CONSTRAINT ortho_insured_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.tx_category_insured(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8222 (class 2606 OID 894302)
-- Name: other_professional_relationships other_professional_relationships_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.other_professional_relationships
    ADD CONSTRAINT other_professional_relationships_ibfk_1 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8223 (class 2606 OID 894307)
-- Name: other_professional_relationships other_professional_relationships_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.other_professional_relationships
    ADD CONSTRAINT other_professional_relationships_ibfk_2 FOREIGN KEY (external_office_id) REFERENCES vaxiom.external_offices(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8224 (class 2606 OID 894312)
-- Name: other_professional_relationships other_professional_relationships_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.other_professional_relationships
    ADD CONSTRAINT other_professional_relationships_ibfk_3 FOREIGN KEY (employee_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8448 (class 2606 OID 895432)
-- Name: tx_statuses parent_company_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_statuses
    ADD CONSTRAINT parent_company_id FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8229 (class 2606 OID 894337)
-- Name: patient_images patient_images_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images
    ADD CONSTRAINT patient_images_ibfk_1 FOREIGN KEY (imageseries_id) REFERENCES vaxiom.patient_imageseries(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8230 (class 2606 OID 894342)
-- Name: patient_images patient_images_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images
    ADD CONSTRAINT patient_images_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8234 (class 2606 OID 894362)
-- Name: patient_images_layouts patient_images_layouts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_images_layouts
    ADD CONSTRAINT patient_images_layouts_ibfk_1 FOREIGN KEY (organization_node_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8231 (class 2606 OID 894347)
-- Name: patient_imageseries patient_imageseries_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_imageseries
    ADD CONSTRAINT patient_imageseries_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8232 (class 2606 OID 894352)
-- Name: patient_imageseries patient_imageseries_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_imageseries
    ADD CONSTRAINT patient_imageseries_ibfk_2 FOREIGN KEY (treatment_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8233 (class 2606 OID 894357)
-- Name: patient_imageseries patient_imageseries_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_imageseries
    ADD CONSTRAINT patient_imageseries_ibfk_3 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8235 (class 2606 OID 894367)
-- Name: patient_insurance_plans patient_insurance_plans_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_insurance_plans
    ADD CONSTRAINT patient_insurance_plans_ibfk_2 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8236 (class 2606 OID 894372)
-- Name: patient_insurance_plans patient_insurance_plans_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_insurance_plans
    ADD CONSTRAINT patient_insurance_plans_ibfk_3 FOREIGN KEY (insurance_billing_center_id) REFERENCES vaxiom.insurance_billing_centers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8237 (class 2606 OID 894377)
-- Name: patient_insurance_plans patient_insurance_plans_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_insurance_plans
    ADD CONSTRAINT patient_insurance_plans_ibfk_4 FOREIGN KEY (master_id) REFERENCES vaxiom.patient_insurance_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8238 (class 2606 OID 894382)
-- Name: patient_ledger_history patient_ledger_history_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history
    ADD CONSTRAINT patient_ledger_history_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8239 (class 2606 OID 894387)
-- Name: patient_ledger_history patient_ledger_history_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history
    ADD CONSTRAINT patient_ledger_history_ibfk_2 FOREIGN KEY (tx_location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8240 (class 2606 OID 894392)
-- Name: patient_ledger_history_overdue_receivables patient_ledger_history_overdue_receivables_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history_overdue_receivables
    ADD CONSTRAINT patient_ledger_history_overdue_receivables_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8241 (class 2606 OID 894397)
-- Name: patient_ledger_history_overdue_receivables patient_ledger_history_overdue_receivables_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_ledger_history_overdue_receivables
    ADD CONSTRAINT patient_ledger_history_overdue_receivables_ibfk_2 FOREIGN KEY (patient_ledger_history_id) REFERENCES vaxiom.patient_ledger_history(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8242 (class 2606 OID 894402)
-- Name: patient_locations patient_locations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_locations
    ADD CONSTRAINT patient_locations_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8243 (class 2606 OID 894407)
-- Name: patient_locations patient_locations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_locations
    ADD CONSTRAINT patient_locations_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8244 (class 2606 OID 894412)
-- Name: patient_person_referrals patient_person_referrals_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_person_referrals
    ADD CONSTRAINT patient_person_referrals_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8245 (class 2606 OID 894417)
-- Name: patient_person_referrals patient_person_referrals_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_person_referrals
    ADD CONSTRAINT patient_person_referrals_ibfk_2 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8246 (class 2606 OID 894422)
-- Name: patient_recently_opened_items patient_recently_opened_items_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_recently_opened_items
    ADD CONSTRAINT patient_recently_opened_items_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.recently_opened_items(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8247 (class 2606 OID 894427)
-- Name: patient_recently_opened_items patient_recently_opened_items_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_recently_opened_items
    ADD CONSTRAINT patient_recently_opened_items_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8248 (class 2606 OID 894432)
-- Name: patient_template_referrals patient_template_referrals_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_template_referrals
    ADD CONSTRAINT patient_template_referrals_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8249 (class 2606 OID 894437)
-- Name: patient_template_referrals patient_template_referrals_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patient_template_referrals
    ADD CONSTRAINT patient_template_referrals_ibfk_3 FOREIGN KEY (referral_template_id) REFERENCES vaxiom.referral_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8225 (class 2606 OID 894317)
-- Name: patients patients_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT patients_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8226 (class 2606 OID 894322)
-- Name: patients patients_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT patients_ibfk_2 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8227 (class 2606 OID 894327)
-- Name: patients patients_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT patients_ibfk_3 FOREIGN KEY (heard_from_patient_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8228 (class 2606 OID 894332)
-- Name: patients patients_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.patients
    ADD CONSTRAINT patients_ibfk_4 FOREIGN KEY (heard_from_professional_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8259 (class 2606 OID 894487)
-- Name: payment_correction_details payment_correction_details_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_correction_details
    ADD CONSTRAINT payment_correction_details_ibfk_1 FOREIGN KEY (transaction_id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8260 (class 2606 OID 894492)
-- Name: payment_correction_details payment_correction_details_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_correction_details
    ADD CONSTRAINT payment_correction_details_ibfk_2 FOREIGN KEY (correction_type_id) REFERENCES vaxiom.correction_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8265 (class 2606 OID 894517)
-- Name: payment_plan_rollbacks payment_plan_rollbacks_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plan_rollbacks
    ADD CONSTRAINT payment_plan_rollbacks_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8266 (class 2606 OID 894522)
-- Name: payment_plan_rollbacks payment_plan_rollbacks_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plan_rollbacks
    ADD CONSTRAINT payment_plan_rollbacks_ibfk_2 FOREIGN KEY (payment_plan_id) REFERENCES vaxiom.payment_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8261 (class 2606 OID 894497)
-- Name: payment_plans payment_plans_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT payment_plans_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8262 (class 2606 OID 894502)
-- Name: payment_plans payment_plans_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT payment_plans_ibfk_2 FOREIGN KEY (bank_account_id) REFERENCES vaxiom.bank_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8263 (class 2606 OID 894507)
-- Name: payment_plans payment_plans_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT payment_plans_ibfk_3 FOREIGN KEY (invoice_contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8264 (class 2606 OID 894512)
-- Name: payment_plans payment_plans_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_plans
    ADD CONSTRAINT payment_plans_ibfk_4 FOREIGN KEY (token_location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8267 (class 2606 OID 894527)
-- Name: payment_types payment_types_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types
    ADD CONSTRAINT payment_types_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8268 (class 2606 OID 894532)
-- Name: payment_types_locations payment_types_locations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types_locations
    ADD CONSTRAINT payment_types_locations_ibfk_1 FOREIGN KEY (payment_type_id) REFERENCES vaxiom.payment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8269 (class 2606 OID 894537)
-- Name: payment_types_locations payment_types_locations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payment_types_locations
    ADD CONSTRAINT payment_types_locations_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8251 (class 2606 OID 894447)
-- Name: payments payments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_1 FOREIGN KEY (account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8252 (class 2606 OID 894452)
-- Name: payments payments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_2 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8253 (class 2606 OID 894457)
-- Name: payments payments_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_3 FOREIGN KEY (billing_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8254 (class 2606 OID 894462)
-- Name: payments payments_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_4 FOREIGN KEY (applied_payment_id) REFERENCES vaxiom.applied_payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8255 (class 2606 OID 894467)
-- Name: payments payments_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_5 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8256 (class 2606 OID 894472)
-- Name: payments payments_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_6 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8257 (class 2606 OID 894477)
-- Name: payments payments_ibfk_7; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_7 FOREIGN KEY (payment_type_id) REFERENCES vaxiom.payment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8258 (class 2606 OID 894482)
-- Name: payments payments_ibfk_8; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.payments
    ADD CONSTRAINT payments_ibfk_8 FOREIGN KEY (insurance_company_payment_id) REFERENCES vaxiom.insurance_company_payments(insurance_company_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8274 (class 2606 OID 894562)
-- Name: permissions_user_role_permission permission_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_permission
    ADD CONSTRAINT permission_fk FOREIGN KEY (permissions_permission_id) REFERENCES vaxiom.permissions_permission(id);


--
-- TOC entry 8271 (class 2606 OID 894547)
-- Name: permissions_user_role_departament permissions_user_role_dep_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_departament
    ADD CONSTRAINT permissions_user_role_dep_fk FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id);


--
-- TOC entry 8273 (class 2606 OID 894557)
-- Name: permissions_user_role_job_title permissions_user_role_jt_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_job_title
    ADD CONSTRAINT permissions_user_role_jt_fk FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id);


--
-- TOC entry 8284 (class 2606 OID 894612)
-- Name: person_image_files person_image_files_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_image_files
    ADD CONSTRAINT person_image_files_ibfk_1 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8285 (class 2606 OID 894617)
-- Name: person_payment_accounts person_payment_accounts_fk_pay_acount; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_payment_accounts
    ADD CONSTRAINT person_payment_accounts_fk_pay_acount FOREIGN KEY (id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8286 (class 2606 OID 894622)
-- Name: person_payment_accounts person_payment_accounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_payment_accounts
    ADD CONSTRAINT person_payment_accounts_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8287 (class 2606 OID 894627)
-- Name: person_payment_accounts person_payment_accounts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.person_payment_accounts
    ADD CONSTRAINT person_payment_accounts_ibfk_2 FOREIGN KEY (payer_person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8279 (class 2606 OID 894587)
-- Name: persons persons_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_1 FOREIGN KEY (core_user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8280 (class 2606 OID 894592)
-- Name: persons persons_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_2 FOREIGN KEY (portal_user_id) REFERENCES vaxiom.sys_portal_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8281 (class 2606 OID 894597)
-- Name: persons persons_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_3 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8282 (class 2606 OID 894602)
-- Name: persons persons_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_4 FOREIGN KEY (global_person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8283 (class 2606 OID 894607)
-- Name: persons persons_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.persons
    ADD CONSTRAINT persons_ibfk_5 FOREIGN KEY (patient_portal_user_id) REFERENCES vaxiom.sys_patient_portal_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8288 (class 2606 OID 894632)
-- Name: previous_enrollment_form previous_enrollment_form_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.previous_enrollment_form
    ADD CONSTRAINT previous_enrollment_form_ibfk_1 FOREIGN KEY (employee_id) REFERENCES vaxiom.employment_contracts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8291 (class 2606 OID 894647)
-- Name: procedure_additional_resource_requirements procedure_additional_resource_requirements_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_additional_resource_requirements
    ADD CONSTRAINT procedure_additional_resource_requirements_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8292 (class 2606 OID 894652)
-- Name: procedure_additional_resource_requirements procedure_additional_resource_requirements_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_additional_resource_requirements
    ADD CONSTRAINT procedure_additional_resource_requirements_ibfk_2 FOREIGN KEY (resource_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8294 (class 2606 OID 894662)
-- Name: procedure_step_additional_resource_requirements procedure_step_additional_resource_requirements_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_additional_resource_requirements
    ADD CONSTRAINT procedure_step_additional_resource_requirements_ibfk_1 FOREIGN KEY (step_id) REFERENCES vaxiom.procedure_steps(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8295 (class 2606 OID 894667)
-- Name: procedure_step_additional_resource_requirements procedure_step_additional_resource_requirements_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_additional_resource_requirements
    ADD CONSTRAINT procedure_step_additional_resource_requirements_ibfk_2 FOREIGN KEY (resource_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8296 (class 2606 OID 894672)
-- Name: procedure_step_durations procedure_step_durations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_durations
    ADD CONSTRAINT procedure_step_durations_ibfk_1 FOREIGN KEY (step_id) REFERENCES vaxiom.procedure_steps(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8297 (class 2606 OID 894677)
-- Name: procedure_step_durations procedure_step_durations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_step_durations
    ADD CONSTRAINT procedure_step_durations_ibfk_2 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8293 (class 2606 OID 894657)
-- Name: procedure_steps procedure_steps_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedure_steps
    ADD CONSTRAINT procedure_steps_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8289 (class 2606 OID 894637)
-- Name: procedures procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedures
    ADD CONSTRAINT procedures_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8290 (class 2606 OID 894642)
-- Name: procedures procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.procedures
    ADD CONSTRAINT procedures_ibfk_2 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8298 (class 2606 OID 894682)
-- Name: professional_relationships professional_relationships_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.professional_relationships
    ADD CONSTRAINT professional_relationships_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8301 (class 2606 OID 894697)
-- Name: provider_network_insurance_companies provider_network_insurance_companies_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_network_insurance_companies
    ADD CONSTRAINT provider_network_insurance_companies_ibfk_1 FOREIGN KEY (network_fee_sheet_id) REFERENCES vaxiom.network_fee_sheets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8302 (class 2606 OID 894702)
-- Name: provider_network_insurance_companies provider_network_insurance_companies_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.provider_network_insurance_companies
    ADD CONSTRAINT provider_network_insurance_companies_ibfk_2 FOREIGN KEY (insurance_company_id) REFERENCES vaxiom.insurance_companies(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8303 (class 2606 OID 894707)
-- Name: questionnaire_answers questionnaire_answers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.questionnaire_answers
    ADD CONSTRAINT questionnaire_answers_ibfk_1 FOREIGN KEY (question_id) REFERENCES vaxiom.question(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8304 (class 2606 OID 894712)
-- Name: questionnaire_answers questionnaire_answers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.questionnaire_answers
    ADD CONSTRAINT questionnaire_answers_ibfk_2 FOREIGN KEY (answer_id) REFERENCES vaxiom.answers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8305 (class 2606 OID 894717)
-- Name: reachify_users reachify_users_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.reachify_users
    ADD CONSTRAINT reachify_users_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8306 (class 2606 OID 894722)
-- Name: receivables receivables_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_1 FOREIGN KEY (treatment_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8307 (class 2606 OID 894727)
-- Name: receivables receivables_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_2 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8308 (class 2606 OID 894732)
-- Name: receivables receivables_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_3 FOREIGN KEY (payment_account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8309 (class 2606 OID 894737)
-- Name: receivables receivables_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_4 FOREIGN KEY (insured_id) REFERENCES vaxiom.insured(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8310 (class 2606 OID 894742)
-- Name: receivables receivables_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_5 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8311 (class 2606 OID 894747)
-- Name: receivables receivables_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_6 FOREIGN KEY (primary_prsn_payer_inv_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8312 (class 2606 OID 894752)
-- Name: receivables receivables_ibfk_7; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_7 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8313 (class 2606 OID 894757)
-- Name: receivables receivables_ibfk_8; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.receivables
    ADD CONSTRAINT receivables_ibfk_8 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8367 (class 2606 OID 895027)
-- Name: tasks recipient_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT recipient_id FOREIGN KEY (recipient_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8314 (class 2606 OID 894762)
-- Name: referral_list_values referral_list_values_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_list_values
    ADD CONSTRAINT referral_list_values_ibfk_1 FOREIGN KEY (referral_template_id) REFERENCES vaxiom.referral_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8315 (class 2606 OID 894767)
-- Name: referral_templates referral_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_templates
    ADD CONSTRAINT referral_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8316 (class 2606 OID 894772)
-- Name: referral_templates referral_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.referral_templates
    ADD CONSTRAINT referral_templates_ibfk_2 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8318 (class 2606 OID 894782)
-- Name: refund_details refund_details_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.refund_details
    ADD CONSTRAINT refund_details_ibfk_1 FOREIGN KEY (transaction_id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8317 (class 2606 OID 894777)
-- Name: refunds refunds_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.refunds
    ADD CONSTRAINT refunds_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8319 (class 2606 OID 894787)
-- Name: relationships relationships_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.relationships
    ADD CONSTRAINT relationships_ibfk_1 FOREIGN KEY (from_person) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8320 (class 2606 OID 894792)
-- Name: relationships relationships_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.relationships
    ADD CONSTRAINT relationships_ibfk_2 FOREIGN KEY (to_person) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8321 (class 2606 OID 894797)
-- Name: resources resources_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resources
    ADD CONSTRAINT resources_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8322 (class 2606 OID 894802)
-- Name: resources resources_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resources
    ADD CONSTRAINT resources_ibfk_2 FOREIGN KEY (resource_type_id) REFERENCES vaxiom.resource_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8323 (class 2606 OID 894807)
-- Name: resources resources_ifbk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.resources
    ADD CONSTRAINT resources_ifbk_3 FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8324 (class 2606 OID 894812)
-- Name: retail_fees retail_fees_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_fees
    ADD CONSTRAINT retail_fees_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8325 (class 2606 OID 894817)
-- Name: retail_fees retail_fees_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_fees
    ADD CONSTRAINT retail_fees_ibfk_2 FOREIGN KEY (retail_item_id) REFERENCES vaxiom.retail_items(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8326 (class 2606 OID 894822)
-- Name: retail_items retail_items_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.retail_items
    ADD CONSTRAINT retail_items_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8327 (class 2606 OID 894827)
-- Name: reversed_payments reversed_payments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.reversed_payments
    ADD CONSTRAINT reversed_payments_ibfk_1 FOREIGN KEY (payment_id) REFERENCES vaxiom.payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8329 (class 2606 OID 894837)
-- Name: schedule_conflict_permission_bypass schedule_conflict_permission_bypass_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_conflict_permission_bypass
    ADD CONSTRAINT schedule_conflict_permission_bypass_ibfk_1 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8330 (class 2606 OID 894842)
-- Name: schedule_notes schedule_notes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_notes
    ADD CONSTRAINT schedule_notes_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8331 (class 2606 OID 894847)
-- Name: schedule_notes schedule_notes_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.schedule_notes
    ADD CONSTRAINT schedule_notes_ibfk_2 FOREIGN KEY (chair_id) REFERENCES vaxiom.chairs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8328 (class 2606 OID 894832)
-- Name: scheduled_task scheduled_task_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduled_task
    ADD CONSTRAINT scheduled_task_ibfk_1 FOREIGN KEY (parent_company_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8332 (class 2606 OID 894852)
-- Name: scheduling_preferences scheduling_preferences_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferences
    ADD CONSTRAINT scheduling_preferences_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8333 (class 2606 OID 894857)
-- Name: scheduling_preferences_week_days scheduling_preferences_week_days_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferences_week_days
    ADD CONSTRAINT scheduling_preferences_week_days_ibfk_1 FOREIGN KEY (scheduling_preferences_id) REFERENCES vaxiom.scheduling_preferences(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8334 (class 2606 OID 894862)
-- Name: scheduling_preferred_employees scheduling_preferred_employees_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferred_employees
    ADD CONSTRAINT scheduling_preferred_employees_ibfk_1 FOREIGN KEY (preference_id) REFERENCES vaxiom.scheduling_preferences(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8335 (class 2606 OID 894867)
-- Name: scheduling_preferred_employees scheduling_preferred_employees_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.scheduling_preferred_employees
    ADD CONSTRAINT scheduling_preferred_employees_ibfk_2 FOREIGN KEY (employee_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8343 (class 2606 OID 894907)
-- Name: selfcheckin_settings_forbidden_types selfcheckin_settings_forbidden_types_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selfcheckin_settings_forbidden_types
    ADD CONSTRAINT selfcheckin_settings_forbidden_types_ibfk_1 FOREIGN KEY (location_access_key_id) REFERENCES vaxiom.location_access_keys(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 8344 (class 2606 OID 894912)
-- Name: selfcheckin_settings_forbidden_types selfcheckin_settings_forbidden_types_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selfcheckin_settings_forbidden_types
    ADD CONSTRAINT selfcheckin_settings_forbidden_types_ibfk_2 FOREIGN KEY (appointment_template_id) REFERENCES vaxiom.appointment_templates(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 8342 (class 2606 OID 894902)
-- Name: selfcheckin_settings selfcheckin_settings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selfcheckin_settings
    ADD CONSTRAINT selfcheckin_settings_ibfk_1 FOREIGN KEY (location_access_key_id) REFERENCES vaxiom.location_access_keys(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 8345 (class 2606 OID 894917)
-- Name: simultaneous_appointment_values simultaneous_appointment_values_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.simultaneous_appointment_values
    ADD CONSTRAINT simultaneous_appointment_values_ibfk_1 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8346 (class 2606 OID 894922)
-- Name: simultaneous_appointment_values simultaneous_appointment_values_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.simultaneous_appointment_values
    ADD CONSTRAINT simultaneous_appointment_values_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8347 (class 2606 OID 894927)
-- Name: sms_part sms_part_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sms_part
    ADD CONSTRAINT sms_part_ibfk_1 FOREIGN KEY (sms_id) REFERENCES vaxiom.sms(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8348 (class 2606 OID 894932)
-- Name: statements statements_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.statements
    ADD CONSTRAINT statements_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8349 (class 2606 OID 894937)
-- Name: statements statements_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.statements
    ADD CONSTRAINT statements_ibfk_3 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8366 (class 2606 OID 895022)
-- Name: sys_organization_settings sys_organization_settings_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organization_settings
    ADD CONSTRAINT sys_organization_settings_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8350 (class 2606 OID 894942)
-- Name: sys_organizations sys_organizations_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organizations
    ADD CONSTRAINT sys_organizations_ibfk_1 FOREIGN KEY (postal_address_id) REFERENCES vaxiom.contact_postal_addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8351 (class 2606 OID 894947)
-- Name: sys_organizations sys_organizations_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.sys_organizations
    ADD CONSTRAINT sys_organizations_ibfk_2 FOREIGN KEY (parent_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8376 (class 2606 OID 895072)
-- Name: task_basket_subscribers task_basket_subscribers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_basket_subscribers
    ADD CONSTRAINT task_basket_subscribers_ibfk_1 FOREIGN KEY (task_basket_id) REFERENCES vaxiom.task_baskets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8377 (class 2606 OID 895077)
-- Name: task_basket_subscribers task_basket_subscribers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_basket_subscribers
    ADD CONSTRAINT task_basket_subscribers_ibfk_2 FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8375 (class 2606 OID 895067)
-- Name: task_baskets task_baskets_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_baskets
    ADD CONSTRAINT task_baskets_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8378 (class 2606 OID 895082)
-- Name: task_contacts task_contacts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_contacts
    ADD CONSTRAINT task_contacts_ibfk_1 FOREIGN KEY (task_id) REFERENCES vaxiom.tasks(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8379 (class 2606 OID 895087)
-- Name: task_contacts task_contacts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_contacts
    ADD CONSTRAINT task_contacts_ibfk_2 FOREIGN KEY (contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8380 (class 2606 OID 895092)
-- Name: task_events task_events_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_events
    ADD CONSTRAINT task_events_ibfk_1 FOREIGN KEY (assignee_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8381 (class 2606 OID 895097)
-- Name: task_events task_events_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.task_events
    ADD CONSTRAINT task_events_ibfk_2 FOREIGN KEY (task_id) REFERENCES vaxiom.tasks(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8368 (class 2606 OID 895032)
-- Name: tasks tasks_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8369 (class 2606 OID 895037)
-- Name: tasks tasks_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_2 FOREIGN KEY (document_template_id) REFERENCES vaxiom.document_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8370 (class 2606 OID 895042)
-- Name: tasks tasks_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_3 FOREIGN KEY (document_tree_node_id) REFERENCES vaxiom.document_tree_nodes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8371 (class 2606 OID 895047)
-- Name: tasks tasks_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_4 FOREIGN KEY (provider_id) REFERENCES vaxiom.employee_resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8372 (class 2606 OID 895052)
-- Name: tasks tasks_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_5 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8373 (class 2606 OID 895057)
-- Name: tasks tasks_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_6 FOREIGN KEY (assignee_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8374 (class 2606 OID 895062)
-- Name: tasks tasks_ibfk_7; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tasks
    ADD CONSTRAINT tasks_ibfk_7 FOREIGN KEY (task_basket_id) REFERENCES vaxiom.task_baskets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8385 (class 2606 OID 895117)
-- Name: temp_accounts temp_accounts_fk_pay_acount; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_fk_pay_acount FOREIGN KEY (id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8386 (class 2606 OID 895122)
-- Name: temp_accounts temp_accounts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8387 (class 2606 OID 895127)
-- Name: temp_accounts temp_accounts_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_ibfk_2 FOREIGN KEY (person_payer_id) REFERENCES vaxiom.tx_payers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8388 (class 2606 OID 895132)
-- Name: temp_accounts temp_accounts_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_ibfk_3 FOREIGN KEY (insurance_payer_id) REFERENCES vaxiom.tx_payers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8389 (class 2606 OID 895137)
-- Name: temp_accounts temp_accounts_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temp_accounts
    ADD CONSTRAINT temp_accounts_ibfk_4 FOREIGN KEY (tx_plan_id) REFERENCES vaxiom.tx_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8382 (class 2606 OID 895102)
-- Name: temporary_images temporary_images_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temporary_images
    ADD CONSTRAINT temporary_images_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8383 (class 2606 OID 895107)
-- Name: temporary_images temporary_images_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temporary_images
    ADD CONSTRAINT temporary_images_ibfk_2 FOREIGN KEY (patient_image_id) REFERENCES vaxiom.patient_images(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8384 (class 2606 OID 895112)
-- Name: temporary_images temporary_images_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.temporary_images
    ADD CONSTRAINT temporary_images_ibfk_3 FOREIGN KEY (organization_node_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8390 (class 2606 OID 895142)
-- Name: third_party_account third_party_account_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.third_party_account
    ADD CONSTRAINT third_party_account_ibfk_1 FOREIGN KEY (organization_node_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8395 (class 2606 OID 895167)
-- Name: tooth_marking tooth_marking_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tooth_marking
    ADD CONSTRAINT tooth_marking_ibfk_1 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.toothchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8391 (class 2606 OID 895147)
-- Name: toothchart_edit_log toothchart_edit_log_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_edit_log
    ADD CONSTRAINT toothchart_edit_log_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.audit_logs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8392 (class 2606 OID 895152)
-- Name: toothchart_edit_log toothchart_edit_log_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_edit_log
    ADD CONSTRAINT toothchart_edit_log_ibfk_2 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.toothchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8393 (class 2606 OID 895157)
-- Name: toothchart_note toothchart_note_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_note
    ADD CONSTRAINT toothchart_note_ibfk_1 FOREIGN KEY (snapshot_id) REFERENCES vaxiom.toothchart_snapshot(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8394 (class 2606 OID 895162)
-- Name: toothchart_snapshot toothchart_snapshot_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.toothchart_snapshot
    ADD CONSTRAINT toothchart_snapshot_ibfk_1 FOREIGN KEY (appointment_id) REFERENCES vaxiom.appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8396 (class 2606 OID 895172)
-- Name: transactions transactions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transactions
    ADD CONSTRAINT transactions_ibfk_1 FOREIGN KEY (receivable_id) REFERENCES vaxiom.receivables(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8397 (class 2606 OID 895177)
-- Name: transfer_charges transfer_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transfer_charges
    ADD CONSTRAINT transfer_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8398 (class 2606 OID 895182)
-- Name: transfer_charges transfer_charges_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.transfer_charges
    ADD CONSTRAINT transfer_charges_ibfk_2 FOREIGN KEY (charge_transfer_adjustment_id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8406 (class 2606 OID 895222)
-- Name: tx_card_field_definitions tx_card_field_definitions_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_definitions
    ADD CONSTRAINT tx_card_field_definitions_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8407 (class 2606 OID 895227)
-- Name: tx_card_field_definitions tx_card_field_definitions_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_definitions
    ADD CONSTRAINT tx_card_field_definitions_ibfk_2 FOREIGN KEY (type_id) REFERENCES vaxiom.tx_card_field_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8408 (class 2606 OID 895232)
-- Name: tx_card_field_options tx_card_field_options_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_field_options
    ADD CONSTRAINT tx_card_field_options_ibfk_1 FOREIGN KEY (field_type_id) REFERENCES vaxiom.tx_card_field_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8409 (class 2606 OID 895237)
-- Name: tx_card_templates tx_card_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_templates
    ADD CONSTRAINT tx_card_templates_ibfk_1 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8410 (class 2606 OID 895242)
-- Name: tx_card_templates tx_card_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_card_templates
    ADD CONSTRAINT tx_card_templates_ibfk_2 FOREIGN KEY (tx_plan_template_id) REFERENCES vaxiom.tx_plan_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8404 (class 2606 OID 895212)
-- Name: tx_cards tx_cards_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_cards
    ADD CONSTRAINT tx_cards_ibfk_1 FOREIGN KEY (patient_id) REFERENCES vaxiom.patients(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8405 (class 2606 OID 895217)
-- Name: tx_cards tx_cards_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_cards
    ADD CONSTRAINT tx_cards_ibfk_2 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8411 (class 2606 OID 895247)
-- Name: tx_category_coverages tx_category_coverages_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_coverages
    ADD CONSTRAINT tx_category_coverages_ibfk_1 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8412 (class 2606 OID 895252)
-- Name: tx_category_coverages tx_category_coverages_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_coverages
    ADD CONSTRAINT tx_category_coverages_ibfk_2 FOREIGN KEY (insurance_plan_id) REFERENCES vaxiom.patient_insurance_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8413 (class 2606 OID 895257)
-- Name: tx_category_insured tx_category_insured_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_insured
    ADD CONSTRAINT tx_category_insured_ibfk_1 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8414 (class 2606 OID 895262)
-- Name: tx_category_insured tx_category_insured_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_category_insured
    ADD CONSTRAINT tx_category_insured_ibfk_2 FOREIGN KEY (insured_id) REFERENCES vaxiom.insured(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8415 (class 2606 OID 895267)
-- Name: tx_contracts tx_contracts_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_contracts
    ADD CONSTRAINT tx_contracts_ibfk_1 FOREIGN KEY (tx_payer_id) REFERENCES vaxiom.tx_payers(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8416 (class 2606 OID 895272)
-- Name: tx_fee_charges tx_fee_charges_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_fee_charges
    ADD CONSTRAINT tx_fee_charges_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8417 (class 2606 OID 895277)
-- Name: tx_fee_charges tx_fee_charges_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_fee_charges
    ADD CONSTRAINT tx_fee_charges_ibfk_2 FOREIGN KEY (tx_plan_id) REFERENCES vaxiom.tx_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8418 (class 2606 OID 895282)
-- Name: tx_fee_charges tx_fee_charges_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_fee_charges
    ADD CONSTRAINT tx_fee_charges_ibfk_3 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8419 (class 2606 OID 895287)
-- Name: tx_payers tx_payers_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_1 FOREIGN KEY (tx_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8420 (class 2606 OID 895292)
-- Name: tx_payers tx_payers_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_2 FOREIGN KEY (person_id) REFERENCES vaxiom.persons(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8421 (class 2606 OID 895297)
-- Name: tx_payers tx_payers_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_3 FOREIGN KEY (contact_method_id) REFERENCES vaxiom.contact_methods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8422 (class 2606 OID 895302)
-- Name: tx_payers tx_payers_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_4 FOREIGN KEY (insured_id) REFERENCES vaxiom.insured(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8423 (class 2606 OID 895307)
-- Name: tx_payers tx_payers_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_payers
    ADD CONSTRAINT tx_payers_ibfk_5 FOREIGN KEY (account_id) REFERENCES vaxiom.payment_accounts(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8432 (class 2606 OID 895352)
-- Name: tx_plan_appointment_procedures tx_plan_appointment_procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointment_procedures
    ADD CONSTRAINT tx_plan_appointment_procedures_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8433 (class 2606 OID 895357)
-- Name: tx_plan_appointment_procedures tx_plan_appointment_procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointment_procedures
    ADD CONSTRAINT tx_plan_appointment_procedures_ibfk_2 FOREIGN KEY (appointment_id) REFERENCES vaxiom.tx_plan_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8427 (class 2606 OID 895327)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_1 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8428 (class 2606 OID 895332)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_2 FOREIGN KEY (tx_plan_id) REFERENCES vaxiom.tx_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8429 (class 2606 OID 895337)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_3 FOREIGN KEY (next_appointment_id) REFERENCES vaxiom.tx_plan_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8430 (class 2606 OID 895342)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_4 FOREIGN KEY (tx_plan_template_appointment_id) REFERENCES vaxiom.tx_plan_template_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8431 (class 2606 OID 895347)
-- Name: tx_plan_appointments tx_plan_appointments_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_appointments
    ADD CONSTRAINT tx_plan_appointments_ibfk_5 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8434 (class 2606 OID 895362)
-- Name: tx_plan_groups tx_plan_groups_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_groups
    ADD CONSTRAINT tx_plan_groups_ibfk_1 FOREIGN KEY (pc_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8437 (class 2606 OID 895377)
-- Name: tx_plan_note_groups tx_plan_note_groups_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_note_groups
    ADD CONSTRAINT tx_plan_note_groups_ibfk_1 FOREIGN KEY (tx_plan_note_id) REFERENCES vaxiom.tx_plan_notes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8435 (class 2606 OID 895367)
-- Name: tx_plan_notes tx_plan_notes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_notes
    ADD CONSTRAINT tx_plan_notes_ibfk_1 FOREIGN KEY (tx_card_id) REFERENCES vaxiom.tx_cards(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8436 (class 2606 OID 895372)
-- Name: tx_plan_notes tx_plan_notes_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_notes
    ADD CONSTRAINT tx_plan_notes_ibfk_2 FOREIGN KEY (diagnosis_id) REFERENCES vaxiom.diagnosis(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8446 (class 2606 OID 895422)
-- Name: tx_plan_template_appointment_procedures tx_plan_template_appointment_procedures_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointment_procedures
    ADD CONSTRAINT tx_plan_template_appointment_procedures_ibfk_1 FOREIGN KEY (procedure_id) REFERENCES vaxiom.procedures(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8447 (class 2606 OID 895427)
-- Name: tx_plan_template_appointment_procedures tx_plan_template_appointment_procedures_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointment_procedures
    ADD CONSTRAINT tx_plan_template_appointment_procedures_ibfk_2 FOREIGN KEY (appointment_id) REFERENCES vaxiom.tx_plan_template_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8442 (class 2606 OID 895402)
-- Name: tx_plan_template_appointments tx_plan_template_appointments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT tx_plan_template_appointments_ibfk_1 FOREIGN KEY (type_id) REFERENCES vaxiom.appointment_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8443 (class 2606 OID 895407)
-- Name: tx_plan_template_appointments tx_plan_template_appointments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT tx_plan_template_appointments_ibfk_2 FOREIGN KEY (tx_plan_template_id) REFERENCES vaxiom.tx_plan_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8444 (class 2606 OID 895412)
-- Name: tx_plan_template_appointments tx_plan_template_appointments_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT tx_plan_template_appointments_ibfk_3 FOREIGN KEY (next_appointment_id) REFERENCES vaxiom.tx_plan_template_appointments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8445 (class 2606 OID 895417)
-- Name: tx_plan_template_appointments tx_plan_template_appointments_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_template_appointments
    ADD CONSTRAINT tx_plan_template_appointments_ibfk_4 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8438 (class 2606 OID 895382)
-- Name: tx_plan_templates tx_plan_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT tx_plan_templates_ibfk_1 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8439 (class 2606 OID 895387)
-- Name: tx_plan_templates tx_plan_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT tx_plan_templates_ibfk_2 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8440 (class 2606 OID 895392)
-- Name: tx_plan_templates tx_plan_templates_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT tx_plan_templates_ibfk_3 FOREIGN KEY (tx_category_id) REFERENCES vaxiom.tx_categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8441 (class 2606 OID 895397)
-- Name: tx_plan_templates tx_plan_templates_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plan_templates
    ADD CONSTRAINT tx_plan_templates_ibfk_4 FOREIGN KEY (tx_plan_group_id) REFERENCES vaxiom.tx_plan_groups(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8424 (class 2606 OID 895312)
-- Name: tx_plans tx_plans_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plans
    ADD CONSTRAINT tx_plans_ibfk_1 FOREIGN KEY (insurance_code_id) REFERENCES vaxiom.insurance_codes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8425 (class 2606 OID 895317)
-- Name: tx_plans tx_plans_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plans
    ADD CONSTRAINT tx_plans_ibfk_2 FOREIGN KEY (tx_plan_template_id) REFERENCES vaxiom.tx_plan_templates(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8426 (class 2606 OID 895322)
-- Name: tx_plans tx_plans_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.tx_plans
    ADD CONSTRAINT tx_plans_ibfk_3 FOREIGN KEY (tx_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8399 (class 2606 OID 895187)
-- Name: txs txs_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT txs_ibfk_1 FOREIGN KEY (tx_card_id) REFERENCES vaxiom.tx_cards(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8400 (class 2606 OID 895192)
-- Name: txs txs_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT txs_ibfk_2 FOREIGN KEY (tx_plan_id) REFERENCES vaxiom.tx_plans(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8401 (class 2606 OID 895197)
-- Name: txs txs_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT txs_ibfk_3 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8402 (class 2606 OID 895202)
-- Name: txs txs_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs
    ADD CONSTRAINT txs_ibfk_4 FOREIGN KEY (sys_created_at) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8403 (class 2606 OID 895207)
-- Name: txs_state_changes txs_state_changes_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.txs_state_changes
    ADD CONSTRAINT txs_state_changes_ibfk_1 FOREIGN KEY (treatment_id) REFERENCES vaxiom.txs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8449 (class 2606 OID 895437)
-- Name: unapplied_payments unapplied_payments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.unapplied_payments
    ADD CONSTRAINT unapplied_payments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8450 (class 2606 OID 895442)
-- Name: unapplied_payments unapplied_payments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.unapplied_payments
    ADD CONSTRAINT unapplied_payments_ibfk_2 FOREIGN KEY (applied_payment_id) REFERENCES vaxiom.applied_payments(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8338 (class 2606 OID 894882)
-- Name: selected_appointment_templates user_id; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.selected_appointment_templates
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES vaxiom.sys_users(id);


--
-- TOC entry 8275 (class 2606 OID 894567)
-- Name: permissions_user_role_permission user_role_fk; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.permissions_user_role_permission
    ADD CONSTRAINT user_role_fk FOREIGN KEY (permissions_user_role_id) REFERENCES vaxiom.permissions_user_role(id);


--
-- TOC entry 8451 (class 2606 OID 895447)
-- Name: week_templates week_templates_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_1 FOREIGN KEY (fri) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8452 (class 2606 OID 895452)
-- Name: week_templates week_templates_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_2 FOREIGN KEY (organization_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8453 (class 2606 OID 895457)
-- Name: week_templates week_templates_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_3 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8454 (class 2606 OID 895462)
-- Name: week_templates week_templates_ibfk_4; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_4 FOREIGN KEY (thu) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8455 (class 2606 OID 895467)
-- Name: week_templates week_templates_ibfk_5; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_5 FOREIGN KEY (sun) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8456 (class 2606 OID 895472)
-- Name: week_templates week_templates_ibfk_6; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_6 FOREIGN KEY (tue) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8457 (class 2606 OID 895477)
-- Name: week_templates week_templates_ibfk_7; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_7 FOREIGN KEY (mon) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8458 (class 2606 OID 895482)
-- Name: week_templates week_templates_ibfk_8; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_8 FOREIGN KEY (wed) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8459 (class 2606 OID 895487)
-- Name: week_templates week_templates_ibfk_9; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.week_templates
    ADD CONSTRAINT week_templates_ibfk_9 FOREIGN KEY (sat) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8463 (class 2606 OID 895507)
-- Name: work_hour_comments work_hour_comments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hour_comments
    ADD CONSTRAINT work_hour_comments_ibfk_1 FOREIGN KEY (work_hours_id) REFERENCES vaxiom.work_hours(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8464 (class 2606 OID 895512)
-- Name: work_hour_comments work_hour_comments_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hour_comments
    ADD CONSTRAINT work_hour_comments_ibfk_2 FOREIGN KEY (core_user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8460 (class 2606 OID 895492)
-- Name: work_hours work_hours_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hours
    ADD CONSTRAINT work_hours_ibfk_1 FOREIGN KEY (core_user_id) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8461 (class 2606 OID 895497)
-- Name: work_hours work_hours_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hours
    ADD CONSTRAINT work_hours_ibfk_2 FOREIGN KEY (modified_by_admin) REFERENCES vaxiom.sys_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8462 (class 2606 OID 895502)
-- Name: work_hours work_hours_ibfk_3; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_hours
    ADD CONSTRAINT work_hours_ibfk_3 FOREIGN KEY (location_id) REFERENCES vaxiom.sys_organizations(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8465 (class 2606 OID 895517)
-- Name: work_schedule_days work_schedule_days_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_schedule_days
    ADD CONSTRAINT work_schedule_days_ibfk_1 FOREIGN KEY (day_schedule_id) REFERENCES vaxiom.day_schedules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8466 (class 2606 OID 895522)
-- Name: work_schedule_days work_schedule_days_ibfk_2; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.work_schedule_days
    ADD CONSTRAINT work_schedule_days_ibfk_2 FOREIGN KEY (resource_id) REFERENCES vaxiom.resources(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 8467 (class 2606 OID 895527)
-- Name: writeoff_adjustments writeoff_adjustments_ibfk_1; Type: FK CONSTRAINT; Schema: vaxiom; Owner: postgres
--

ALTER TABLE ONLY vaxiom.writeoff_adjustments
    ADD CONSTRAINT writeoff_adjustments_ibfk_1 FOREIGN KEY (id) REFERENCES vaxiom.transactions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2020-03-10 15:12:22 CET

--
-- PostgreSQL database dump complete
--

